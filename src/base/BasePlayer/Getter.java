package base.BasePlayer;

import java.util.ArrayList;
import java.util.List;

import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.control.Control;
import base.BasePlayer.control.ControlFile;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.variants.VarMaster;
import base.BasePlayer.variants.VarNode;

public class Getter {
  private static Getter instance = null;

  private Getter() {
      // TODO Auto-generated constructor stub
  }

  public static Getter getInstance() {
    if (instance == null) {
        instance = new Getter();
    }
    return instance;
  }
  public ArrayList<Sample> getSampleList() {
    return Draw.getSampleList.get();
  }
  public VarNode getVariantHead() {
    return VarMaster.getInstance().getHead();
  }
  public List<ControlFile> getControlList() {
    return Control.controlData.fileArray;
  }
  public boolean loading() {
    return Loader.isLoading;
  }
}
