/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseMotionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.UIManager;

import base.BasePlayer.GUI.Launcher;
import base.BasePlayer.GUI.MainPane;

public class Main {
	public static JFrame frame = new JFrame("BasePlayer");
	private static void createAndShowGUI() {
		
	JFrame.setDefaultLookAndFeelDecorated(false);
	
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	frame.setResizable(true);
	
	final JComponent contentPane = new MainPane();
	contentPane.setOpaque(true);
	
	htsjdk.samtools.util.Log.setGlobalLogLevel(htsjdk.samtools.util.Log.LogLevel.ERROR);
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(final java.awt.event.WindowEvent windowEvent) {
				if (MainPane.configChanged) {
					try {
						final BufferedWriter fileWriter = new BufferedWriter(new FileWriter(Launcher.configfile));
						for (int i = 0; i < Launcher.config.size(); i++) {
							fileWriter.write(Launcher.config.get(i) + MainPane.lineseparator);
						}
						fileWriter.close();
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
		Image iconImage = Toolkit.getDefaultToolkit().getImage(Main.class.getResource("icons/icon.png"));
			
		frame.setIconImage(iconImage);
		frame.setContentPane(contentPane);
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(final String[] args) {
		try {

			UIManager.put("Slider.paintValue", false);
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			System.setProperty("sun.java2d.d3d", "false");
		} catch (final Exception e) {
			e.printStackTrace();
		}
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Logo.main(argsit);
				createAndShowGUI();
			}
		});
	}
}