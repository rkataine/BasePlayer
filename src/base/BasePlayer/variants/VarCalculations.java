package base.BasePlayer.variants;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.BedCanvas;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.SampleDialog;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.genome.Gene;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.io.FileWrite;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.sample.SampleNode;
import base.BasePlayer.tracks.BedNode;
import htsjdk.tribble.Feature;
import htsjdk.tribble.index.Index;

public class VarCalculations {
	private static final VarCalculations INSTANCE = new VarCalculations();
	public VariantCaller varc = null;
	public VariantCaller.VarCaller varcal = null;
	public HashMap<String, Integer[]> contexts = null;
  public static VarNode lastVar = null, lastWriteVar = null, returnnode = null;
	public static boolean caller = false;
	public static boolean cancelvarcount = false;
	public static int affected = 0;
	public int mutcount = 0;
	public static VarCalculations getInstance() {
		return INSTANCE;
	} 
	public static void removeNonListVariants() {
		VarNode node = Getter.getInstance().getVariantHead().getNext();
		VarNode tempNode;
		int mutcount = 0;
		
		while (node != null) {

			if (!node.inVarList || node.hideNode()) {
				tempNode = node.getNext();
				node.removeNode();
				node = tempNode;
			} else {
				for (Map.Entry<String, ArrayList<SampleNode>> entry: node.vars.entrySet()) {
					
					mutcount = 0;
					for (int m = 0; m < entry.getValue().size(); m++) {
						if (entry.getValue().get(m).alleles != null) {
							break;
						}
						if (!entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
							if (VariantHandler.onlyselected.isSelected()) {
								if (!entry.getValue().get(m).getSample().equals(Sample.selectedSample)) {
									entry.getValue().remove(m);
									m--;
									continue;
								}
							}
							mutcount++;
						} else {
							entry.getValue().remove(m);
							m--;
						}
					}
				}
				if (mutcount == 0) {
					tempNode = node.getNext();
					node.removeNode();
					node = tempNode;
				} else {
					node = node.getNext();
				}
			}
		}
		node = null;
		tempNode = null;
	
		VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());

		
		MainPane.drawCanvas.repaint();
	}
	public void varCalc() {
		
		try {
			FileRead.novars = false;
			final boolean isfreeze = VariantHandler.freeze.isSelected();
			if (!VariantHandler.none.isSelected()) {
				SampleDialog.checkFiles();

				if (affected == 0) {
					MainPane.showError("Set at least one individual as affected. (click sample name or right click sample sidebar)", "Note");
					return;
				}
			} else {
				VariantHandler.freeze.setSelected(true);
			}
			contexts = null;
		
			Draw.variantcalculator = true;

			if (VariantHandler.writetofile.isSelected()) {
				lastWriteVar = Getter.getInstance().getVariantHead();
			}
			
			clearVariantsFromGenes();
			SplitClass split = Draw.getSplits().get(0);
			int chromcounter = 0;
			VariantHandler.outputStrings.clear();
			if (VarMaster.getInstance().clusterNodes == null) {
				VarMaster.getInstance().clusterNodes = new ArrayList<ClusterNode>();
			} else {
				VarMaster.getInstance().clusterNodes.clear();

			}
			VarMaster.clusterId = 1;
			VariantHandler.table.variants = 0;
			VariantHandler.stattable.variants = 0;
			
			for (int i = 0; i < VariantHandler.tables.size(); i++) {
				VariantHandler.tables.get(i).variants = 0;
			}

			cancelvarcount = false;

			VariantHandler.table.genearray.clear();
			VariantHandler.table.aminoarray.clear();
			VariantHandler.table.controlarray = null;
			VariantHandler.stattable.sampleArray.clear();

			for (int i = 0; i < VariantHandler.tables.size(); i++) {
				VariantHandler.tables.get(i).bedarray = null;
				VariantHandler.tables.get(i).aminoarray = null;
				VariantHandler.tables.get(i).vararray = null;
				VariantHandler.tables.get(i).controlarray = null;
				VariantHandler.tables.get(i).variants = 0;
			}

			VarNode vardraw = VarMaster.getInstance().getCurrent();
			if (vardraw == null) vardraw = Getter.getInstance().getVariantHead().getNext();
			Object[] addobject;
			
			for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {

				if (Getter.getInstance().getSampleList().get(i).multiVCF
						|| (Getter.getInstance().getSampleList().get(i).getTabixFile() == null
								&& !Getter.getInstance().getSampleList().get(i).multipart)
								&& (caller && Getter.getInstance().getSampleList().get(i).samFile == null)
								&& !Getter.getInstance().getSampleList().get(i).calledvariants) {
					continue;
				}
				addobject = new Object[VariantHandler.stattable.headerlengths.length];
				addobject[0] = Getter.getInstance().getSampleList().get(i);
				for (int j = 1; j < addobject.length; j++) {
					addobject[j] = 0;
				}
				Getter.getInstance().getSampleList().get(i).mutationTypes = new double[6];
				for (int j = 0; j < 6; j++) {
					Getter.getInstance().getSampleList().get(i).mutationTypes[j] = 0.0;
				}
				VariantHandler.stattable.sampleArray.add(addobject);
				Getter.getInstance().getSampleList().get(i).heterozygotes = 0;
				Getter.getInstance().getSampleList().get(i).homozygotes = 0;
				Getter.getInstance().getSampleList().get(i).varcount = 0;
				Getter.getInstance().getSampleList().get(i).indels = 0;
				Getter.getInstance().getSampleList().get(i).snvs = 0;
				Getter.getInstance().getSampleList().get(i).sitioRate = 0;
				Getter.getInstance().getSampleList().get(i).versioRate = 0;
				Getter.getInstance().getSampleList().get(i).ins = 0;
				Getter.getInstance().getSampleList().get(i).callrates = 0.0;
				Getter.getInstance().getSampleList().get(i).coding = 0;
				Getter.getInstance().getSampleList().get(i).syn = 0;
				Getter.getInstance().getSampleList().get(i).nonsyn = 0;
				Getter.getInstance().getSampleList().get(i).missense = 0;
				Getter.getInstance().getSampleList().get(i).splice = 0;
				Getter.getInstance().getSampleList().get(i).nonsense = 0;
				Getter.getInstance().getSampleList().get(i).fshift = 0;
				Getter.getInstance().getSampleList().get(i).inframe = 0;
				Getter.getInstance().getSampleList().get(i).coverages = 0;

			}

			VariantHandler.stattable.setPreferredSize(new Dimension(VariantHandler.statsScroll.getViewport().getWidth(),
					(VariantHandler.stattable.sampleArray.size() + 1) * VariantHandler.stattable.rowHeight));
			VariantHandler.stattable.revalidate();
			VariantHandler.stattable.repaint();

			for (int g = 0; g < split.getGenes().size(); g++) {
				split.getGenes().get(g).mutations = 0;
				split.getGenes().get(g).missense = 0;
				split.getGenes().get(g).nonsense = 0;
				split.getGenes().get(g).synonymous = 0;
				split.getGenes().get(g).intronic = 0;
				split.getGenes().get(g).utr = 0;
				split.getGenes().get(g).samples.clear();
				split.getGenes().get(g).varnodes.clear();
				split.getGenes().get(g).intergenic = false;
				split.getGenes().get(g).transcriptString = new StringBuffer();
			}

			if (VariantHandler.allChroms.isSelected() && !VariantHandler.xLinked.isSelected()) {
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					if (MainPane.bedCanvas.bedTrack.get(i).getVarcalc().isSelected()) {
						MainPane.bedCanvas.bedTrack.get(i).intersect = true;

					}
				}
				if (VariantHandler.allChromsfrom.isSelected()) {
					chromcounter = MenuBar.chromosomeDropdown.getSelectedIndex();
					MainPane.nothread = true;
					MenuBar.chromosomeDropdown.setSelectedIndex(MenuBar.chromosomeDropdown.getSelectedIndex());
					MainPane.nothread = false;
					vardraw = Getter.getInstance().getVariantHead().getNext();
					VarMaster.getInstance().clusterNodes.clear();
					VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
				} else if (Draw.getSplits().get(0).start != 1
						|| MenuBar.chromosomeDropdown.getSelectedIndex() != 0) {
					MainPane.nothread = true;
					MenuBar.chromosomeDropdown.setSelectedIndex(0);
					MainPane.nothread = false;
					vardraw = Getter.getInstance().getVariantHead().getNext();
					VarMaster.getInstance().clusterNodes.clear();
					VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
				} else {
					vardraw = Getter.getInstance().getVariantHead().getNext();
					VarMaster.getInstance().clusterNodes.clear();
					VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
				}
			} else {
				if (VariantHandler.xLinked.isSelected()) {
					if (MenuBar.chromModel.getIndexOf("X") != -1) {
						if (Draw.getSplits().get(0).start != 1
								|| MenuBar.chromosomeDropdown.getSelectedIndex() != 0) {
							MainPane.nothread = true;
							MenuBar.chromosomeDropdown.setSelectedItem("X");
							MainPane.nothread = false;
							vardraw = Getter.getInstance().getVariantHead().getNext();
							VarMaster.getInstance().clusterNodes.clear();
							VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
						}
					} else {
						JOptionPane.showMessageDialog(MainPane.drawScroll, "No chromosome X available.", "Note",
								JOptionPane.INFORMATION_MESSAGE);
						return;
					}
				}
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					if (MainPane.bedCanvas.bedTrack.get(i).getVarcalc().isSelected()) {
						MainPane.bedCanvas.bedTrack.get(i).intersect = true;

						if (MainPane.bedCanvas.bedTrack.get(i).small && !MainPane.bedCanvas.bedTrack.get(i).bigWig) {

							MainPane.bedCanvas.annotate(MainPane.bedCanvas.bedTrack.get(i).getHead(), Getter.getInstance().getVariantHead().getNext());
							MainPane.bedCanvas.intersected = true;

						} else {

							final BedCanvas.Annotator annotator = MainPane.bedCanvas.new Annotator(
									MainPane.bedCanvas.bedTrack.get(i));
							annotator.annotateVars();
							MainPane.bedCanvas.intersected = true;

						}
					}

				}
			}

			if (caller && varc == null) {
				varc = new VariantCaller(true);
				varcal = varc.new VarCaller();
			}
			MainPane.drawCanvas.loadbarAll = 0;
			MainPane.drawCanvas.loadBarSample = 0;
			if (caller && Getter.getInstance().getVariantHead().getNext() == null) {
				varcal.callVariants();
				vardraw = Getter.getInstance().getVariantHead().getNext();
			}
			
			while (true) {
				if (cancelvarcount || !Getter.getInstance().loading()) {
					VcfReader.cancelFileRead();
					vardraw = null;
					break;
				}
				
				if (vardraw == null) {
					if (VariantHandler.writetofile.isSelected()) {
						for (int i = 0; i < VariantHandler.table.genearray.size(); i++) {
							FileWrite.writeTranscriptToFile(VariantHandler.table.genearray.get(i), FileWrite.output);
						}
						FileRead.lastpos = 0;
						VarMaster.getInstance().clusterNodes.clear();
					}
					VariantHandler.table
							.setPreferredSize(new Dimension(VariantHandler.tableScroll.getViewport().getWidth(),
									(VariantHandler.table.getTableSize() + 1) * (VariantHandler.table.rowHeight)));
					if (VariantHandler.tabs.getSelectedIndex() == 0) {
						VariantHandler.aminoCount.setText(VariantHandler.table.variants + " variants");
					} else if (VariantHandler.tabs.getSelectedIndex() == 1) {
						VariantHandler.aminoCount.setText(VariantHandler.stattable.variants + " variants");
					} else {
						VariantHandler.aminoCount.setText(VariantHandler.tables.get(VariantHandler.tabs.getSelectedIndex() - 3).variants + " variants");
					}
					VariantHandler.table.revalidate();
					VariantHandler.table.repaint();
					try {						
						
						for (int i = 0; i < VariantHandler.stattable.sampleArray.size(); i++) {
							Sample sample = (Sample) VariantHandler.stattable.sampleArray.get(i)[0];
							VariantHandler.stattable.sampleArray.get(i)[1] = sample.varcount;
							VariantHandler.stattable.sampleArray.get(i)[2] = sample.snvs;
							VariantHandler.stattable.sampleArray.get(i)[3] = sample.indels;
							VariantHandler.stattable.sampleArray.get(i)[4] = sample.ins;
							VariantHandler.stattable.sampleArray.get(i)[5] = sample.coding;
							VariantHandler.stattable.sampleArray.get(i)[6] = MethodLibrary
									.round(sample.heterozygotes / (double) sample.homozygotes, 2);
							VariantHandler.stattable.sampleArray.get(i)[7] = MethodLibrary
									.round((int) sample.sitioRate / (double) sample.versioRate, 2);

							for (int j = 0; j < 6; j++) {
								VariantHandler.stattable.sampleArray.get(i)[8 + j] = MethodLibrary
										.round(sample.mutationTypes[j] / (double) sample.snvs, 2);
							}

							VariantHandler.stattable.sampleArray.get(i)[14] = MethodLibrary
									.round(sample.callrates / (double) sample.varcount, 2);
							VariantHandler.stattable.sampleArray.get(i)[15] = MethodLibrary
									.round(sample.coverages / (double) sample.varcount, 2);
							VariantHandler.stattable.repaint();
						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
					
					if (VariantHandler.allChroms.isSelected() && !VariantHandler.xLinked.isSelected()) {
						for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
							FileRead.removeNonListBeds(MainPane.bedCanvas.bedTrack.get(i).getHead(), Draw.getSplits().get(0).chromEnd);
						}

						if (VariantHandler.writetofile.isSelected()) {
							Getter.getInstance().getVariantHead().putNext(null);
							nullifyVarNodes();
							clearVariantsFromGenes();
						}

						if (cancelvarcount || !Getter.getInstance().loading()) {
							VcfReader.cancelFileRead();
							break;
						}
						FileRead.lastpos = 0;

						if (chromcounter < MenuBar.chromosomeDropdown.getItemCount()) {
							chromcounter++;
							if (chromcounter == MenuBar.chromosomeDropdown.getItemCount()) {// ||
																							// MenuBar.chromosomeDropdown.getItemAt(chromcounter).toString().contains("X"))
																							// {
								break;
							}
							if (VariantHandler.onlyAutosomes.isSelected()) {
								if (MenuBar.chromosomeDropdown.getItemAt(chromcounter).toString().equals("X")
										|| MenuBar.chromosomeDropdown.getItemAt(chromcounter).toString().equals("Y")) {
									break;
								}
							}
							MainPane.nothread = true;
							MenuBar.chromosomeDropdown.setSelectedIndex(chromcounter);
							MainPane.nothread = false;
							if (FileRead.nobeds) {

								continue;
							}

							for (int g = 0; g < split.getGenes().size(); g++) {
								split.getGenes().get(g).mutations = 0;
								split.getGenes().get(g).missense = 0;
								split.getGenes().get(g).nonsense = 0;
								split.getGenes().get(g).synonymous = 0;
								split.getGenes().get(g).intronic = 0;
								split.getGenes().get(g).utr = 0;
								split.getGenes().get(g).samples.clear();
								split.getGenes().get(g).varnodes.clear();
								split.getGenes().get(g).intergenic = false;
								split.getGenes().get(g).transcriptString = new StringBuffer();
							}
							if (caller) {
								varcal.callVariants();
							}
							VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
							vardraw = Getter.getInstance().getVariantHead().getNext();
							Getter.getInstance().getVariantHead().putNext(null);
							if (vardraw == null) {
								continue;
							}
							if (lastVar == null) {
								lastVar = Getter.getInstance().getVariantHead();
							}
							if (lastWriteVar == null) {
								lastWriteVar = Getter.getInstance().getVariantHead();
							}

							vardraw.putPrev(lastVar);
							lastVar.putNext(vardraw);

						} else {
							Loader.ready("all");
							break;
						}
					} else {
						break;
					}
				}
				try {
					
					if (!VariantHandler.allChroms.isSelected()) {
						
						if (vardraw != null && vardraw.getPosition() < Draw.getSplits().get(0).start) {
							vardraw = vardraw.getNext();
							continue;
						}
						if (vardraw == null || vardraw.getPosition() > Draw.getSplits().get(0).end) {
							vardraw = null;
							continue;
						}
					}
					// STATS

					vardraw = annotateVariant(vardraw);

				} catch (final Exception ex) {
					ErrorLog.addError(ex.getStackTrace());
					ex.printStackTrace();
				}
			}

			nullifyVarNodes();
			vardraw = null;

			
			
			VariantHandler.table.revalidate();
			VariantHandler.table.repaint();
			for (int i = 0; i < VariantHandler.tables.size(); i++) {

				VariantHandler.tables.get(i).setPreferredSize(new Dimension(
						VariantHandler.tableScroll.getViewport().getWidth(),
						(VariantHandler.tables.get(i).getTableSize() + 1) * (VariantHandler.tables.get(i).rowHeight)));
				VariantHandler.tables.get(i).revalidate();
				VariantHandler.tables.get(i).repaint();
			}
			
			VariantHandler.table.setPreferredSize(new Dimension(VariantHandler.tableScroll.getViewport().getWidth(),
					(VariantHandler.table.getTableSize() + 1) * (VariantHandler.table.rowHeight)));

			if (VariantHandler.tabs.getSelectedIndex() == 0) {
				VariantHandler.aminoCount.setText(VariantHandler.table.variants + " variants");
			} else if (VariantHandler.tabs.getSelectedIndex() == 1) {
				VariantHandler.aminoCount.setText(VariantHandler.stattable.variants + " variants");
			} else {
				VariantHandler.aminoCount.setText(VariantHandler.tables.get(VariantHandler.tabs.getSelectedIndex() - 3).variants + " variants");
			}
			try {

				if (FileWrite.output != null) {
					if (VariantHandler.onlyStats.isSelected() && VariantHandler.outputContexts.isSelected()) {
						for (int i = 0; i < VariantHandler.stattable.sampleArray.size(); i++) {
							Sample sample = (Sample) VariantHandler.stattable.sampleArray.get(i)[0];
							FileWrite.output.write(sample.getName());

							for (int j = 1; j < VariantHandler.stattable.headerlengths.length; j++) {
								FileWrite.output.write("\t" + VariantHandler.stattable.sampleArray.get(i)[j]);
							}
							FileWrite.output.write("\t" + sample.syn + "\t" + sample.nonsyn + "\t" + sample.missense + "\t" + sample.splice + "\t" + sample.nonsense + "\t" + sample.fshift + "\t"
									+ sample.inframe);
									FileWrite.output.write(MainPane.lineseparator);
						}
						if (contexts != null) {
							String[] contextArray = BaseConstants.contexts.split(" ");
							//final Iterator<Entry<String, Integer[]>> iter = contexts.entrySet().iterator();
							FileWrite.sigOutput.write("MutationType");
							for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
								if (Getter.getInstance().getSampleList().get(i).multiVCF) continue;

								FileWrite.sigOutput.write("\t" + Getter.getInstance().getSampleList().get(i).getName());								
							}
							FileWrite.sigOutput.write("\n"); // MainPane.lineseparator);
							//while (iter.hasNext()) {
							for (String context : contextArray) {							
								//final Entry<String, Integer[]> entry = iter.next();
								//final Integer[] samples = entry.getValue();
								final Integer[] samples = contexts.get(context);
								//String mutField = entry.getKey().charAt(1) +"[" +entry.getKey().charAt(2) +">" +entry.getKey().charAt(0) +"]" +entry.getKey().charAt(3);

								FileWrite.sigOutput.write(context);
								for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
									if (Getter.getInstance().getSampleList().get(i).multiVCF) continue;
									if (samples == null || samples[i] == null) {
										FileWrite.sigOutput.write("\t0");
										continue;
									} 
											
									FileWrite.sigOutput.write("\t" + samples[i]);										
									
								}
								FileWrite.sigOutput.write("\n"); // MainPane.lineseparator);
							}
							FileWrite.sigOutput.close();
						}
					}
					FileWrite.output.close();
				}
				if (FileWrite.outputgz != null) {
					for (int i = 0; i < VariantHandler.outputStrings.size(); i++) {
						FileWrite.outputgz.write(VariantHandler.outputStrings.get(i).getBytes());
						final Feature vcf = VariantHandler.vcfCodec.decode(VariantHandler.outputStrings.get(i));
						FileRead.indexCreator.addFeature(vcf, FileRead.filepointer);
						FileRead.filepointer = FileWrite.outputgz.getFilePointer();
					}
					VariantHandler.outputStrings.clear();
					FileWrite.outputgz.flush();
					final Index index = FileRead.indexCreator.finalizeIndex(FileWrite.outputgz.getFilePointer());
					index.writeBasedOnFeatureFile(FileWrite.outFile);
					FileWrite.outputgz.close();
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
			split = null;

			if (!isfreeze) {
				VariantHandler.freeze.setSelected(false);
			}
			Loader.ready("all");
			if (!Getter.getInstance().loading()) {
				
			}
			Draw.variantcalculator = false;
			MainPane.drawCanvas.loadbarAll = 0;
			MainPane.drawCanvas.loadBarSample = 0;
			if (VariantHandler.allChroms.isSelected()) {
				MainPane.showError("Variant annotation ready!", "Note");
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
  public void varCalcBig() {
		final boolean isfreeze = VariantHandler.freeze.isSelected();
		FileRead.novars = false;
		if (!VariantHandler.none.isSelected()) {
			SampleDialog.checkFiles();

			if (affected == 0) {
				MainPane.showError("Set at least one individual as affected. (right click sample sidebar)", "Note");
				return;
			}
		} else {
			VariantHandler.freeze.setSelected(true);
		}
		FileRead.bigcalc = true;
		Draw.variantcalculator = true;
		final int adder = Settings.windowSize;
		int presearchpos = 1;
		clearVariantsFromGenes();
		int chromcounter = 0;
		clearVariantsFromBeds();
		if (VarMaster.getInstance().clusterNodes == null)
			VarMaster.getInstance().clusterNodes = new ArrayList<ClusterNode>();
		else 
			VarMaster.getInstance().clusterNodes.clear();
		
		for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
			if (MainPane.bedCanvas.bedTrack.get(i).getVarcalc().isSelected()) {
				MainPane.bedCanvas.bedTrack.get(i).intersect = true;
			}
		}
		VarMaster.clusterId = 1;
		VariantHandler.table.variants = 0;
		VariantHandler.stattable.variants = 0;
		
		for (int i = 0; i < VariantHandler.tables.size(); i++) {
			VariantHandler.tables.get(i).variants = 0;
		}

		cancelvarcount = false;
		VariantHandler.table.genearray.clear();
		VariantHandler.table.aminoarray.clear();
		VariantHandler.table.controlarray = null;
		VariantHandler.stattable.sampleArray.clear();

		for (int i = 0; i < VariantHandler.tables.size(); i++) {
			VariantHandler.tables.get(i).bedarray = null;
			VariantHandler.tables.get(i).aminoarray = null;
			VariantHandler.tables.get(i).vararray = null;
			VariantHandler.tables.get(i).controlarray = null;
			VariantHandler.tables.get(i).variants = 0;

		}
		VarMaster.getInstance().reset();
		VarNode vardraw;

		Object[] addobject;

		for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
			if (Getter.getInstance().getSampleList().get(i).multiVCF || (Getter.getInstance().getSampleList().get(i).getTabixFile() == null
					&& !Getter.getInstance().getSampleList().get(i).multipart)) {
				continue;
			}
			addobject = new Object[VariantHandler.stattable.headerlengths.length];
			addobject[0] = Getter.getInstance().getSampleList().get(i);
			for (int j = 1; j < addobject.length; j++) {
				addobject[j] = 0;
			}
			Getter.getInstance().getSampleList().get(i).mutationTypes = new double[6];
			for (int j = 0; j < 6; j++) {
				Getter.getInstance().getSampleList().get(i).mutationTypes[j] = 0.0;
			}
			VariantHandler.stattable.sampleArray.add(addobject);
			Sample sample = Getter.getInstance().getSampleList().get(i);
			sample.heterozygotes = 0;
			sample.varcount = 0;
			sample.homozygotes = 0;
			sample.indels = 0;
			sample.snvs = 0;
			sample.sitioRate = 0;
			sample.versioRate = 0;
			sample.ins = 0;
			sample.callrates = 0.0;
			sample.coding = 0;
			sample.syn = 0;
			sample.nonsyn = 0;
			sample.missense = 0;
			sample.splice = 0;
			sample.nonsense = 0;
			sample.fshift = 0;
			sample.inframe = 0;

		}
		VariantHandler.stattable.setPreferredSize(new Dimension(VariantHandler.statsScroll.getViewport().getWidth(),
				(VariantHandler.stattable.sampleArray.size() + 1) * (BaseVariables.defaultFontSize + 5) + 2));
		VariantHandler.stattable.revalidate();
		VariantHandler.stattable.repaint();
		clearVariantsFromGenes();
		int searchStart = 0;
		int searchEnd = 0;
		if (VariantHandler.allChroms.isSelected()) {
			if (VariantHandler.allChromsfrom.isSelected()) {
				chromcounter = MenuBar.chromosomeDropdown.getSelectedIndex();
				MainPane.nothread = true;
				FileRead.search = true;
				searchStart = (int) Draw.getSplits().get(0).start;
				searchEnd = (int) Draw.getSplits().get(0).start + Settings.windowSize;
				MenuBar.chromosomeDropdown.setSelectedIndex(MenuBar.chromosomeDropdown.getSelectedIndex());
				MainPane.nothread = false;

				VarMaster.getInstance().clusterNodes.clear();
				VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
				vardraw = Getter.getInstance().getVariantHead().getNext();
			} else if (Draw.getSplits().get(0).start > 10 || MenuBar.chromosomeDropdown.getSelectedIndex() != 0) {
				MainPane.nothread = true;
				FileRead.search = true;
				MenuBar.chromosomeDropdown.setSelectedIndex(0);
				MainPane.nothread = false;
				vardraw = Getter.getInstance().getVariantHead().getNext();
				VarMaster.getInstance().clusterNodes.clear();
				VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());

			} else {
				searchStart = 0;
				searchEnd = searchStart + Settings.windowSize;
				FileRead.vcfReader.getVariantWindow(Draw.getSplits().get(0).chrom, searchStart, searchEnd);
				VarMaster.getInstance().clusterNodes.clear();
				VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
				vardraw = Getter.getInstance().getVariantHead().getNext();
			}

		} else {

			searchStart = (int) Draw.getSplits().get(0).start;

			searchEnd = getNextIntergenic(searchStart + adder);
			if (searchEnd > (int) Draw.getSplits().get(0).end) {
				searchEnd = (int) Draw.getSplits().get(0).end;

			}

			FileRead.vcfReader.getVariantWindow(Draw.getSplits().get(0).chrom, searchStart, searchEnd);
			vardraw = Getter.getInstance().getVariantHead().getNext();
		}
		presearchpos = searchEnd;
		lastVar = Getter.getInstance().getVariantHead();

		if (VariantHandler.writetofile.isSelected()) {

			lastWriteVar = Getter.getInstance().getVariantHead();
		}
		MainPane.drawCanvas.loadbarAll = 0;
		MainPane.drawCanvas.loadBarSample = 0;
		while (true) {

			if (cancelvarcount || FileRead.cancelfileread || !Getter.getInstance().loading()) {
				VcfReader.cancelFileRead();
				nullifyVarNodes();
				vardraw = null;
				break;
			}
			if (vardraw == null) {
				if (VariantHandler.writetofile.isSelected()) {

					if (MainPane.projectValues.variantHandlerValues.compareGene > 1) {

						FileWrite.flushVars(vardraw);

					} else {
						clearVariantsFromGenes();
					}
					lastVar.putPrev(null);
					lastWriteVar.putPrev(null);
				}
				try {
					if (MainPane.projectValues.variantHandlerValues.commonVariantsMin > 1 && VariantHandler.clusterSize > 0
							&& VarMaster.getInstance().clusterNodes.size() > 0) {
						VarMaster.getInstance().clusterNodes
								.get(VarMaster.getInstance().clusterNodes.size() - 1).width = VarMaster.getInstance().clusterNodes
										.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes
												.get(VarMaster.getInstance().clusterNodes
														.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes.size()
														- 1)
												.getPosition()
										- VarMaster.getInstance().clusterNodes
												.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes.get(0)
														.getPosition()
										+ 1;
						
					}

					for (int i = 0; i < VariantHandler.stattable.sampleArray.size(); i++) {
						Sample sample = (Sample) VariantHandler.stattable.sampleArray.get(i)[0];
						VariantHandler.stattable.sampleArray.get(i)[1] = sample.varcount;
						VariantHandler.stattable.sampleArray.get(i)[2] = sample.snvs;
						VariantHandler.stattable.sampleArray.get(i)[3] = sample.indels;
						VariantHandler.stattable.sampleArray.get(i)[4] = sample.ins;
						VariantHandler.stattable.sampleArray.get(i)[5] = sample.coding;
						VariantHandler.stattable.sampleArray.get(i)[6] = MethodLibrary
								.round(sample.heterozygotes / (double) sample.homozygotes, 2);
						VariantHandler.stattable.sampleArray.get(i)[7] = MethodLibrary
								.round((int) sample.sitioRate / (double) sample.versioRate, 2);

						for (int j = 0; j < 6; j++) {
							VariantHandler.stattable.sampleArray.get(i)[8 + j] = MethodLibrary
									.round(sample.mutationTypes[j] / (double) sample.snvs, 2);
						}

						VariantHandler.stattable.sampleArray.get(i)[14] = MethodLibrary
								.round(sample.callrates / (double) sample.varcount, 2);
						VariantHandler.stattable.repaint();
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
				if (VariantHandler.allChroms.isSelected() && searchEnd > Draw.getSplits().get(0).chromEnd) {

					for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
						FileRead.removeNonListBeds(MainPane.bedCanvas.bedTrack.get(i).getHead(), Draw.getSplits().get(0).chromEnd);
					}

					if (cancelvarcount) {
						break;
					}
					if (MenuBar.selectedChrom < MenuBar.chromosomeDropdown.getItemCount()) {

						chromcounter++;
						if (chromcounter == MenuBar.chromosomeDropdown.getItemCount()) {
							break;
						}
						if (VariantHandler.onlyAutosomes.isSelected()) {
							if (MenuBar.chromosomeDropdown.getItemAt(chromcounter).toString().equals("X")
									|| MenuBar.chromosomeDropdown.getItemAt(chromcounter).toString().equals("Y")) {
								continue;
							}
						}
						// clearVariantsFromGenes();
						MainPane.nothread = true;
						// search = true;

						MenuBar.chromosomeDropdown.setSelectedIndex(chromcounter);
						searchStart = 1;
						searchEnd = adder;

						// search = false;
						MainPane.nothread = false;

						presearchpos = searchEnd;
						FileRead.vcfReader.getVariantWindow(Draw.getSplits().get(0).chrom, searchStart, searchEnd);

						VarMaster.calcClusters(Getter.getInstance().getVariantHead().getNext());
						vardraw = Getter.getInstance().getVariantHead().getNext();
						Getter.getInstance().getVariantHead().putNext(null);
						if (vardraw == null) {
							continue;
						}

						vardraw.putPrev(lastVar);
						lastVar.putNext(vardraw);

						VariantHandler.table
								.setPreferredSize(new Dimension(VariantHandler.tableScroll.getViewport().getWidth(),
										VariantHandler.table.getTableSize() * VariantHandler.table.rowHeight));
						if (VariantHandler.tabs.getSelectedIndex() == 0) {
							VariantHandler.aminoCount.setText(VariantHandler.table.variants + " variants");
						} else if (VariantHandler.tabs.getSelectedIndex() == 1) {
							VariantHandler.aminoCount.setText(VariantHandler.stattable.variants + " variants");
						} else {
							VariantHandler.aminoCount.setText(
									VariantHandler.tables.get(VariantHandler.tabs.getSelectedIndex() - 3).variants
											+ " variants");
						}
						VariantHandler.table.revalidate();
						VariantHandler.table.repaint();

					}

				} else {

					VariantHandler.table
							.setPreferredSize(new Dimension(VariantHandler.tableScroll.getViewport().getWidth(),
									VariantHandler.table.getTableSize() * VariantHandler.table.rowHeight));
					if (VariantHandler.tabs.getSelectedIndex() == 0) {
						VariantHandler.aminoCount.setText(VariantHandler.table.variants + " variants");
					} else if (VariantHandler.tabs.getSelectedIndex() == 1) {
						VariantHandler.aminoCount.setText(VariantHandler.stattable.variants + " variants");
					} else {
						VariantHandler.aminoCount
								.setText(VariantHandler.tables.get(VariantHandler.tabs.getSelectedIndex() - 3).variants
										+ " variants");
					}

					VariantHandler.table.revalidate();
					VariantHandler.table.repaint();
					MainPane.chromDraw.vardraw = null;

					if (searchEnd >= (int) Draw.getSplits().get(0).end) {
						break;
					}

					searchStart = presearchpos;
					searchEnd = searchStart + adder; // getNextIntergenic(presearchpos+adder);
					presearchpos = searchEnd;
					for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
						FileRead.removeNonListBeds(MainPane.bedCanvas.bedTrack.get(i).getHead(), searchStart);
					}
					FileRead.vcfReader.getVariantWindow(Draw.getSplits().get(0).chrom, searchStart, searchEnd);

					vardraw = Getter.getInstance().getVariantHead().getNext();
					Getter.getInstance().getVariantHead().putNext(null);
					if (vardraw == null) {
						continue;
					}
					vardraw.putPrev(lastVar);
					lastVar.putNext(vardraw);
				}
			}
			try {

				if (!VariantHandler.allChroms.isSelected()) {

					if (vardraw != null && vardraw.getPosition() < Draw.getSplits().get(0).start) {
						vardraw = vardraw.getNext();
					}
					if (vardraw == null || vardraw.getPosition() > Draw.getSplits().get(0).end) {
						vardraw = null;
						continue;
					}
				}

				vardraw = annotateVariant(vardraw);

			} catch (final Exception ex) {
				ErrorLog.addError(ex.getStackTrace());
				ex.printStackTrace();
			}
		}
		
		
		MainPane.drawCanvas.repaint();
		vardraw = null;
		for (int i = 0; i < VariantHandler.tables.size(); i++) {
			VariantHandler.tables.get(i).setPreferredSize(new Dimension(
					VariantHandler.tableScroll.getViewport().getWidth(),
					(VariantHandler.tables.get(i).getTableSize() + 1) * VariantHandler.tables.get(i).rowHeight));
			VariantHandler.tables.get(i).revalidate();
			VariantHandler.tables.get(i).repaint();
		}
		VariantHandler.stattable.repaint();
		VariantHandler.table.setPreferredSize(new Dimension(VariantHandler.tableScroll.getViewport().getWidth(),
				(VariantHandler.table.getTableSize() + 1) * VariantHandler.table.rowHeight));

		if (VariantHandler.tabs.getSelectedIndex() == 0) {
			VariantHandler.aminoCount.setText(VariantHandler.table.variants + " variants");
		} else if (VariantHandler.tabs.getSelectedIndex() == 1) {
			VariantHandler.aminoCount.setText(VariantHandler.stattable.variants + " variants");
		} else {
			VariantHandler.aminoCount.setText(
					VariantHandler.tables.get(VariantHandler.tabs.getSelectedIndex() - 3).variants + " variants");
		}
		try {
			if (FileWrite.output != null) {
				FileWrite.output.close();
			}
			if (FileWrite.outputgz != null) {
				for (int i = 0; i < VariantHandler.outputStrings.size(); i++) {
					FileWrite.outputgz.write(VariantHandler.outputStrings.get(i).getBytes());

					final Feature vcf = VariantHandler.vcfCodec.decode(VariantHandler.outputStrings.get(i));
					FileRead.indexCreator.addFeature(vcf, FileRead.filepointer);
					FileRead.filepointer = FileWrite.outputgz.getFilePointer();

				}

				VariantHandler.outputStrings.clear();
				FileWrite.outputgz.flush();
				final Index index = FileRead.indexCreator.finalizeIndex(FileWrite.outputgz.getFilePointer());
				index.writeBasedOnFeatureFile(FileWrite.outFile);
				FileWrite.outputgz.close();
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		VariantHandler.table.revalidate();
		VariantHandler.table.repaint();
		if (!isfreeze) VariantHandler.freeze.setSelected(false);		

		VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());
		
		Loader.ready("all");
		nullifyVarNodes();
		FileRead.bigcalc = false;
		Draw.variantcalculator = false;
		MainPane.drawCanvas.loadbarAll = 0;
		MainPane.drawCanvas.loadBarSample = 0;
		if (VariantHandler.allChroms.isSelected()) {
			MainPane.showError("Variant annotation ready!", "Note");
		}
	}
  
	static boolean checkRecessiveHomo(final VarNode node, final Entry<String, ArrayList<SampleNode>> entry) {
		try {

			boolean passed = true;
			int samples = 0;

			for (int m = 0; m < entry.getValue().size(); m++) {
				if (entry.getValue().get(m).getSample() == null) {
					continue;
				}
				if (entry.getValue().get(m).getSample().annotation) {
					entry.getValue().get(m).inheritance = true;
					continue;
				}
				if (entry.getValue().get(m).getSample().affected
						&& entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
					continue;
				}
				if (VariantHandler.freeze.isSelected()) {
					if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
						continue;
					}
				}
				if (entry.getValue().get(m).getSample().affected) {

					if (!entry.getValue().get(m).isHomozygous()) {
						passed = false;
						break;
					}

					samples++;

				} else {
					if (entry.getValue().get(m).isHomozygous()) {
						passed = false;
						break;
					}
				}

				entry.getValue().get(m).inheritance = true;

			}

			if (samples != affected) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}
			
			if (!passed) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return true;

	}

	static boolean checkDominant(final VarNode node, final Entry<String, ArrayList<SampleNode>> entry) {
		try {

			boolean passed = true;
			int samples = 0;

			for (int m = 0; m < entry.getValue().size(); m++) {
				if (entry.getValue().get(m).getSample() == null) {
					continue;
				}
				if (entry.getValue().get(m).getSample().annotation) {
					entry.getValue().get(m).inheritance = true;
					continue;
				}
				if (entry.getValue().get(m).getSample().affected
						&& entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
					continue;
				}
				if (VariantHandler.freeze.isSelected()) {
					if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
						continue;
					}
				}
				if (entry.getValue().get(m).isHomozygous()) {
					passed = false;
					break;
				}

				if (!entry.getValue().get(m).getSample().affected) {
					passed = false;
					break;
				}

				entry.getValue().get(m).inheritance = true;
				samples++;
			}

			if (samples != affected) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}
			if (!passed) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return true;

	}

	static boolean checkDeNovo(final VarNode node, final Entry<String, ArrayList<SampleNode>> entry) {
		try {

			int samples = 0;

			for (int m = 0; m < entry.getValue().size(); m++) {
				if (entry.getValue().get(m).getSample() == null) {
					continue;
				}
				if (entry.getValue().get(m).getSample().annotation) {
					entry.getValue().get(m).inheritance = true;
					continue;
				}
				if (entry.getValue().get(m).getSample().affected
						&& entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
					continue;
				}
				if (VariantHandler.freeze.isSelected()) {
					if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
						continue;
					}
				}

				if (entry.getValue().get(m).getSample().children != null) {
					samples = 0;
					break;
				}
				if (entry.getValue().get(m).getSample().parents != 2) {
					continue;
				}
				samples++;

				entry.getValue().get(m).inheritance = true;
			}

			if (samples != 1) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	static boolean checkXlinked(final VarNode node, final Entry<String, ArrayList<SampleNode>> entry) {
		try {

			int samples = 0;

			for (int m = 0; m < entry.getValue().size(); m++) {
				if (entry.getValue().get(m).getSample() == null) {
					continue;
				}
				if (entry.getValue().get(m).getSample().annotation) {
					entry.getValue().get(m).inheritance = true;
					continue;
				}
				if (entry.getValue().get(m).getSample().affected
						&& entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
					continue;
				}
				if (VariantHandler.freeze.isSelected()) {
					if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
						continue;
					}
				}

				if (entry.getValue().get(m).getSample().children != null
						&& entry.getValue().get(m).getSample().children.size() > 0
						&& !entry.getValue().get(m).getSample().female) {
					samples = 0;
					break;
				}
				if (!entry.getValue().get(m).getSample().affected) {
					if (entry.getValue().get(m).isHomozygous()) {
						samples = 0;
						break;
					}
				}

				samples++;
				entry.getValue().get(m).inheritance = true;
			}

			if (samples != 2) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	static boolean checkCompound(final VarNode node, final Entry<String, ArrayList<SampleNode>> entry) {
		try {

			boolean passed = true;
			int samples = 0, parentcount = 0, parents = 0;

			for (int m = 0; m < entry.getValue().size(); m++) {
				if (entry.getValue().get(m).getSample() == null) {
					continue;
				}
				if (entry.getValue().get(m).getSample().annotation) {
					entry.getValue().get(m).inheritance = true;
					continue;
				}
				if (entry.getValue().get(m).getSample().affected
						&& entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
					continue;
				}
				if (VariantHandler.freeze.isSelected()) {
					if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
						continue;
					}
				}
				if (entry.getValue().get(m).isHomozygous()) {
					passed = false;
					break;
				}
				if (entry.getValue().get(m).getSample().children != null
						&& entry.getValue().get(m).getSample().children.size() > 0) {
					parentcount++;
				}
				entry.getValue().get(m).inheritance = true;
				if (entry.getValue().get(m).getSample().affected) {
					if (parents < entry.getValue().get(m).getSample().parents) {
						parents = entry.getValue().get(m).getSample().parents;
					}
					samples++;
				}

			}
			if (!passed) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}
			if (parentcount == 2) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}
			if (parents != 0) {
				if (parentcount == 0) {
					for (int m = 0; m < entry.getValue().size(); m++) {
						entry.getValue().get(m).inheritance = false;
					}
					return false;
				}
			}
			if (samples != affected) {
				for (int m = 0; m < entry.getValue().size(); m++) {
					entry.getValue().get(m).inheritance = false;
				}
				return false;
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}

		return true;

	}

	static boolean checkCompoundGene(final Gene gene, final VarNode vardraw) {

		if (gene.varnodes.size() < 1) {
			gene.compounds.clear();

			return false;
		}

		Boolean found2 = null;

		for (int i = 0; i < gene.varnodes.size(); i++) {

			for (Map.Entry<String, ArrayList<SampleNode>> entry : gene.varnodes.get(i).vars.entrySet()) {
			
				final ArrayList<Sample> healthArray = new ArrayList<Sample>();
				for (int s = 0; s < entry.getValue().size(); s++) {
					if (entry.getValue().get(s).getSample() == null) {
						continue;
					}
					if (entry.getValue().get(s).getSample().annotation) {
						entry.getValue().get(s).inheritance = true;
						continue;
					}
				
					if (!entry.getValue().get(s).getSample().affected) {
						healthArray.add(entry.getValue().get(s).getSample());
					} else {
						if (entry.getValue().get(s).isHomozygous()) {
							healthArray.clear();
							break;
						}
					}
				}
				if (healthArray.size() > 0) {

					for (Map.Entry<String, ArrayList<SampleNode>> entry2 : vardraw.vars.entrySet()) {
						found2 = null;
						for (int s = 0; s < entry2.getValue().size(); s++) {
							if (entry2.getValue().get(s).getSample() == null) {
								continue;
							}
							if (entry2.getValue().get(s).getSample().annotation) {
								entry2.getValue().get(s).inheritance = true;
								continue;
							}
							/*
							 * if(!entry2.getValue().get(s).inheritance) { continue; }
							 */
							if (!entry2.getValue().get(s).getSample().affected) {
								if (healthArray.contains(entry2.getValue().get(s).getSample())) {
									found2 = true;
									break;
								} else {
									found2 = false;
								}
							}
						}

						if (found2 != null && !found2) {
							if (!gene.compounds.contains(vardraw)) {
								gene.compounds.add(vardraw);
							}
							if (!gene.compounds.contains(gene.varnodes.get(i))) {
								gene.compounds.add(gene.varnodes.get(i));
							}
							// System.out.println(gene.compounds.size());
						}
					}
				}
			}
		}
		/*
		 * if(gene.samples.size() < FileRead.affected) { return false; }
		 */

		if (gene.compounds.size() == 0) {
			return false;
		}

		return true;
	}

	static void setUninherited(final Entry<String, ArrayList<SampleNode>> entry) {
		for (int m = 0; m < entry.getValue().size(); m++) {

			entry.getValue().get(m).inheritance = false;

		}
	}
	void annotateSV(String base, Map.Entry<String, ArrayList<SampleNode>> entry, VarNode varnode) {
	
		String[] split = base.split("\\s");
		String svtype = split[0];
		int svlength = Integer.parseInt(split[1].replaceAll(",", ""));

		if (svtype.equals("DEL") || svtype.equals("DUP")) {
			// TODO laita end positio samplenodeen? käytä sitten MethodLibrary.getOverlappingGenes(int start, int end, SplitClass split)
		}
	}
	VarNode annotateVariant(final VarNode vardraw) {

		if (vardraw.hideNode()) {

			returnnode = vardraw.getNext();
			if (VariantHandler.allChroms.isSelected()) {
				vardraw.removeNode();
			}
			return returnnode;
		}

		String base = null, amino = null;
		int pretrack = -1, preI = -1, v = -1;

		MainPane.drawCanvas.loadbarAll = (int) ((vardraw.getPosition() / (double) (Draw.getSplits().get(0).chromEnd))	* 100);
		MainPane.drawCanvas.loadBarSample = MainPane.drawCanvas.loadbarAll;
		boolean recessivefound = false;
		for (Map.Entry<String, ArrayList<SampleNode>> entry : vardraw.vars.entrySet()) {
			v++;
			if (vardraw.hideNodeVar(entry)) {
				if (VariantHandler.allChroms.isSelected()) {
					vardraw.vars.remove(entry.getKey());
				/* 	vardraw.vars.remove(v);
					v--; */
				}
				continue;
			}
			if (MainPane.drawCanvas.annotationOn) {
				if (vardraw.vars.size() == 1 && entry.getValue().size() == 1	&& entry.getValue().get(0).getSample().annotation) {
					if (VariantHandler.allChroms.isSelected()) {
						vardraw.vars.remove(entry.getKey());
						/* vardraw.vars.remove(v);
						v--; */
					}
					continue;
				}
			}
			recessivefound = false;

			base = entry.getKey();
			mutcount = 0;
			if (!VariantHandler.none.isSelected()) {
				if (VariantHandler.recessiveHomo.isSelected()) {
					if (!checkRecessiveHomo(vardraw, entry))
						continue;
				} else if (VariantHandler.denovo.isSelected()) {
					if (!checkDeNovo(vardraw, entry))
						continue;
				} else if (VariantHandler.dominant.isSelected()) {
					if (!checkDominant(vardraw, entry))
						continue;
				} else if (VariantHandler.xLinked.isSelected()) {
					if (!checkXlinked(vardraw, entry))
						continue;
				} else if (VariantHandler.compound.isSelected()) {
					if (!checkCompound(vardraw, entry))
						continue;
				} else if (VariantHandler.recessive.isSelected()) {
					recessivefound = checkRecessiveHomo(vardraw, entry);
					if (!recessivefound) {
						if (MenuBar.chromosomeDropdown.getSelectedItem().equals("X")) {
							recessivefound = checkXlinked(vardraw, entry);
						}
					}
					if (!recessivefound) {
						if (!checkCompound(vardraw, entry)) {
							continue;
						}
					}
				}
			}

			for (int m = 0; m < entry.getValue().size(); m++) {
				SampleNode sampleNode = entry.getValue().get(m);
				if (sampleNode.alleles != null) {
					break;
				}

				if (sampleNode.hideVar(entry.getKey().length() > 1)) {
					if (VariantHandler.allChroms.isSelected()) {
						entry.getValue().remove(m);
						m--;

					}
					continue;
				}
				if (sampleNode.getSample().annotation) {
					continue;
				}

				Sample sample = sampleNode.getSample();
				if (VariantHandler.onlyselected.isSelected()) {
					if (!sample.equals(Sample.selectedSample)) {
						continue;
					}
				}
				sample.varcount++;
				mutcount++;
				VariantHandler.stattable.variants++;
				if (vardraw.coding) {
					sample.coding++;
				}
				/* if (sample.SV) {
					annotateSV(base, entry, vardraw);
					continue;
				} */
				sample.callrates += sampleNode.getAlleleFraction();

				sample.coverages += sampleNode.getCoverage();

				if (sampleNode.isHomozygous()) {
					sample.homozygotes++;
				} else {
					sample.heterozygotes++;
				}
				if (entry.getKey().length() > 1) {
					if (entry.getKey().contains("del")) {
						sample.indels++;
					} else {
						sample.ins++;
					}
				} else {
					if (VariantHandler.onlyStats.isSelected()) {
						if (VariantHandler.outputContexts.isSelected()) {
							String context = MainPane.chromDraw.getSeq(Draw.getSplits().get(0).chrom,
									vardraw.getPosition() - 1, vardraw.getPosition() + 2, MainPane.referenceFile)
									.toString();
							if (!context.contains("N")) {
								String sigBase = base;

								if (contexts == null) {
									contexts = new HashMap<String, Integer[]>();
								}

								if (context.charAt(1) == 'A' || context.charAt(1) == 'G') {
									context = MethodLibrary.reverseComplement(context);
									sigBase = MethodLibrary.reverseComplement(base);
								}

								String basecontext = context.charAt(0) +"["+context.charAt(1)+">"+sigBase+"]"+context.charAt(2);
								//sigBase + context;

								if (contexts.containsKey(basecontext)) {
									if (contexts.get(basecontext)[sample.getIndex()] == null) {
										contexts.get(basecontext)[sample.getIndex()] = 1;
									} else {
										contexts.get(basecontext)[sample.getIndex()]++;
									}
								} else {
									contexts.put(basecontext, new Integer[Getter.getInstance().getSampleList().size()]);
									contexts.get(basecontext)[sample.getIndex()] = 1;
								
								}
							}
						}
					}

					sample.snvs++;

					try {
						if (!MainPane.getBase.get(vardraw.getRefBase()).equals("N") && !entry.getKey().equals("N") && !entry.getKey().equals("."))
							sample.mutationTypes[MainPane.mutTypes.get(MainPane.getBase.get(vardraw.getRefBase()) + entry.getKey())]++;						
					} catch (final Exception e) {

						System.out.println(sample.getName() + " " + vardraw.getPosition() + " " + (char) vardraw.getRefBase() + " "		+ MainPane.getBase.get(vardraw.getRefBase()) + " " + entry.getKey());
						e.printStackTrace();
					}
					if (((char) vardraw.getRefBase() == 'A' && entry.getKey().equals("G"))
							|| ((char) vardraw.getRefBase() == 'G' && entry.getKey().equals("A"))
							|| ((char) vardraw.getRefBase() == 'C' && entry.getKey().equals("T"))
							|| ((char) vardraw.getRefBase() == 'T' && entry.getKey().equals("C"))) {
						sample.sitioRate++;
					} else {
						sample.versioRate++;
					}
				}
			}
			if (mutcount == 0) continue;
			
			// INTRONIC

			if (VariantHandler.intronic.isSelected() && vardraw.isInGene() && vardraw.getTranscripts() != null	&& vardraw.getExons() == null) {

				for (int t = 0; t < vardraw.getTranscripts().size(); t++) {
					for (int i = 0; i < entry.getValue().size(); i++) {
						if (entry.getValue().get(i).alleles != null) break;
						
						if (entry.getValue().get(i).getSample().annotation) continue;
						
						if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) {
							if (VariantHandler.allChroms.isSelected()) {
								entry.getValue().remove(i);
								i--;
							}
							continue;
						}
						if (VariantHandler.onlyselected.isSelected()) 
							if (!entry.getValue().get(i).getSample().equals(Sample.selectedSample)) continue;
						
						if (!vardraw.getTranscripts().get(t).getGene().samples.contains(entry.getValue().get(i).getSample()))
							vardraw.getTranscripts().get(t).getGene().samples.add(entry.getValue().get(i).getSample());
						
					}

					if (!VariantHandler.onlyStats.isSelected()) {
						boolean add = true;
						if (VariantHandler.recessive.isSelected()) {

							if (!recessivefound) {
								add = checkCompoundGene(vardraw.getTranscripts().get(t).getGene(), vardraw);
							}
						} else if (VariantHandler.compound.isSelected()) {
							add = checkCompoundGene(vardraw.getTranscripts().get(t).getGene(), vardraw);

						}
						if (add && vardraw.getTranscripts().get(t).getGene().mutations == 0	&& vardraw.getTranscripts().get(t).getGene().samples.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
							VariantHandler.table.addEntry(vardraw.getTranscripts().get(t).getGene());
							VariantHandler.table.revalidate();
							VariantHandler.table.repaint();
						}
					}
					vardraw.getTranscripts().get(t).getGene().intronic += mutcount;
					vardraw.inVarList = true;

					if (vardraw.inVarList) {

						if (!vardraw.getTranscripts().get(t).getGene().varnodes.contains(vardraw)) {
							if (!VariantHandler.onlyStats.isSelected()) {
								vardraw.getTranscripts().get(t).getGene().varnodes.add(vardraw);
							}
							preI = -1;
						}
						if (v != preI) {
							VariantHandler.table.variants += mutcount;
							vardraw.getTranscripts().get(t).getGene().mutations += mutcount;
							preI = v;
						}

					}
				}
			}

			if (vardraw != null && vardraw.getExons() != null) {
				final ArrayList<Gene> calcgene = new ArrayList<Gene>();
				for (int exon = 0; exon < vardraw.getExons().size(); exon++) {

					amino = MainPane.chromDraw.getAminoChange(vardraw, base, vardraw.getExons().get(exon));
					
					if (amino.contains("UTR")) {
						if (VariantHandler.utr.isSelected()) {
							if (!VariantHandler.table.genearray.contains(vardraw.getExons().get(exon).getTranscript().getGene()) && vardraw.getExons().get(exon).getTranscript().getGene().samples.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
								if (!VariantHandler.onlyStats.isSelected()) {
									boolean add = true;
									if (VariantHandler.recessive.isSelected()) {
										if (!recessivefound) add = checkCompoundGene(vardraw.getExons().get(exon).getTranscript().getGene(), vardraw);
										
									} else if (VariantHandler.compound.isSelected()) add = checkCompoundGene(vardraw.getExons().get(exon).getTranscript().getGene(),	vardraw);
									
									if (add) VariantHandler.table.addEntry(vardraw.getExons().get(exon).getTranscript().getGene());
								}
							}

							vardraw.inVarList = true;
							vardraw.getExons().get(exon).getTranscript().getGene().utr += mutcount;

						} else continue;
						
					}
					// TODO talleta "pahin" aminochange-efekti samplenodeen tai sitte uus vars-table, jossa tää info
					if (VariantHandler.nonsense.isSelected()) 
						if (!MethodLibrary.aminoEffect(amino).contains("nonsense")) continue;						
					
					if (VariantHandler.synonymous.isSelected()) if (MethodLibrary.aminoEffect(amino).contains("synonymous")) continue;					

					for (int i = 0; i < entry.getValue().size(); i++) {
						if (entry.getValue().get(i).alleles != null) break;
						
						if (entry.getValue().get(i).getSample().annotation) continue;
						
						if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) continue;
						
						if (VariantHandler.onlyselected.isSelected()) {
							if (!entry.getValue().get(i).getSample().equals(Sample.selectedSample)) continue;							
						}
						if (!vardraw.getExons().get(exon).getTranscript().getGene().samples
								.contains(entry.getValue().get(i).getSample())) {
							if (!VariantHandler.onlyStats.isSelected()) {
								vardraw.getExons().get(exon).getTranscript().getGene().samples.add(entry.getValue().get(i).getSample());
							}
						}
					}
					boolean add = true;
					if (!VariantHandler.onlyStats.isSelected()) {
						if (VariantHandler.recessive.isSelected()) {

							if (!recessivefound) {
								add = checkCompoundGene(vardraw.getExons().get(exon).getTranscript().getGene(),
										vardraw);
							}
						} else if (VariantHandler.compound.isSelected()) {
							add = checkCompoundGene(vardraw.getExons().get(exon).getTranscript().getGene(), vardraw);

						}

						if (add && !VariantHandler.table.genearray.contains(vardraw.getExons().get(exon).getTranscript().getGene())	&& vardraw.getExons().get(exon).getTranscript().getGene().samples.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
							VariantHandler.table.addEntry(vardraw.getExons().get(exon).getTranscript().getGene());
						}
					}
					if (MethodLibrary.aminoEffect(amino).contains("nonsense")) {
						if (!calcgene.contains(vardraw.getExons().get(exon).getTranscript().getGene())) {
							for (int i = 0; i < entry.getValue().size(); i++) {
								if (entry.getValue().get(i).alleles != null) {
									break;
								}
								if (entry.getValue().get(i).getSample().annotation) {
									continue;
								}
								if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) {
									continue;
								}
								entry.getValue().get(i).getSample().nonsense++;
								if (entry.getKey().length() > 1) {
									entry.getValue().get(i).getSample().fshift++;
								}
								if (amino.contains("spl")) {
									entry.getValue().get(i).getSample().splice++;
								} else {
									entry.getValue().get(i).getSample().nonsyn++;
								}
							}

						}
						vardraw.getExons().get(exon).getTranscript().getGene().nonsense += mutcount;

					} else if (MethodLibrary.aminoEffect(amino).contains("missense")) {
						if (!calcgene.contains(vardraw.getExons().get(exon).getTranscript().getGene())) {
							for (int i = 0; i < entry.getValue().size(); i++) {
								if (entry.getValue().get(i).alleles != null) {
									break;
								}
								if (entry.getValue().get(i).getSample().annotation) {
									continue;
								}
								if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) {
									continue;
								}
								if (entry.getKey().length() > 1) {
									entry.getValue().get(i).getSample().inframe++;
								}
								entry.getValue().get(i).getSample().missense++;
								entry.getValue().get(i).getSample().nonsyn++;
							}
						}
						vardraw.getExons().get(exon).getTranscript().getGene().missense += mutcount;
					} else if (MethodLibrary.aminoEffect(amino).contains("synonymous")) {
						if (!calcgene.contains(vardraw.getExons().get(exon).getTranscript().getGene())) {
							for (int i = 0; i < entry.getValue().size(); i++) {
								if (entry.getValue().get(i).alleles != null) {
									break;
								}
								if (entry.getValue().get(i).getSample().annotation) {
									continue;
								}
								if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) {
									continue;
								}
								entry.getValue().get(i).getSample().syn++;
							}
						}
						vardraw.getExons().get(exon).getTranscript().getGene().synonymous += mutcount;
					}

					vardraw.inVarList = true;
					if (!vardraw.getExons().get(exon).getTranscript().getGene().varnodes.contains(vardraw)) {
						if (!VariantHandler.onlyStats.isSelected()) {
							vardraw.getExons().get(exon).getTranscript().getGene().varnodes.add(vardraw);
						}
						preI = -1;

					}
					if (v != preI) {
						vardraw.getExons().get(exon).getTranscript().getGene().mutations += mutcount;
						VariantHandler.table.variants += mutcount;
						preI = v;
					}

					VariantHandler.table.revalidate();
					VariantHandler.table.repaint();
					if (!calcgene.contains(vardraw.getExons().get(exon).getTranscript().getGene())) {
						calcgene.add(vardraw.getExons().get(exon).getTranscript().getGene());
					}
				}
			}
			preI = v;

			if (!vardraw.isInGene() && VariantHandler.intergenic.isSelected()) {

				/*
				 * for(int v = 0; v<vardraw.vars.size(); v++) { entry = vardraw.vars.get(v);
				 * if(MainPane.drawCanvas.hideNodeVar(vardraw, entry)) { continue; }
				 * 
				 * base = entry.getKey(); mutcount = 0;
				 * 
				 * for(int m = 0; m<entry.getValue().size(); m++) {
				 * if(sampleNode.alleles != null) { break; }
				 * 
				 * if(MainPane.drawCanvas.hideVar(sampleNode)) { continue; }
				 * mutcount++;
				 * 
				 * }
				 */

				if (mutcount > 0) {

					if (VariantHandler.table.genearray.size() == 0
							|| !VariantHandler.table.genearray.get(VariantHandler.table.genearray.size() - 1).intergenic
							|| !VariantHandler.table.genearray.get(VariantHandler.table.genearray.size() - 1).getName()
									.equals(vardraw.getTranscripts().get(0).getGene().getName())) {
						Gene addgene = null;
						try {
							addgene = new Gene(vardraw.getTranscripts().get(0).getGene());

						} catch (final Exception e) {
							e.printStackTrace();
							// Loader.ready("all");
							break;
						}
						addgene.intergenic = true;
						addgene.mutations = mutcount;
						VariantHandler.table.variants += mutcount;
						if (!VariantHandler.onlyStats.isSelected()) {
							addgene.varnodes.add(vardraw);
							boolean add = true;
							if (VariantHandler.recessive.isSelected()) {

								if (!recessivefound) {
									add = checkCompoundGene(addgene, vardraw);
								}
							} else if (VariantHandler.compound.isSelected()) {
								add = checkCompoundGene(addgene, vardraw);

							}
							if (add) {
								VariantHandler.table.addEntry(addgene);
							}
						}
						for (int m = 0; m < entry.getValue().size(); m++) {
							if (entry.getValue().get(m).alleles != null) {
								break;
							}
							if (entry.getValue().get(m).getSample().annotation) {
								continue;
							}
							if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
								continue;
							}
							if (!addgene.samples.contains(entry.getValue().get(m).getSample())) {
								if (!VariantHandler.onlyStats.isSelected()) {
									addgene.samples.add(entry.getValue().get(m).getSample());
								}
							}
						}
					} else {
						if (!VariantHandler.onlyStats.isSelected()) {

							VariantHandler.table.genearray
									.get(VariantHandler.table.genearray.size() - 1).mutations += mutcount;
							if (!VariantHandler.table.genearray.get(VariantHandler.table.genearray.size() - 1).varnodes
									.contains(vardraw)) {
								VariantHandler.table.genearray.get(VariantHandler.table.genearray.size() - 1).varnodes
										.add(vardraw);
							}

						}
						VariantHandler.table.variants += mutcount;
						for (int m = 0; m < entry.getValue().size(); m++) {
							if (entry.getValue().get(m).alleles != null) {
								break;
							}
							if (entry.getValue().get(m).getSample().annotation) {
								continue;
							}
							if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
								continue;
							}
							if (!VariantHandler.table.genearray.get(VariantHandler.table.genearray.size() - 1).samples
									.contains(entry.getValue().get(m).getSample())) {
								if (!VariantHandler.onlyStats.isSelected()) {
									VariantHandler.table.genearray
											.get(VariantHandler.table.genearray.size() - 1).samples
													.add(entry.getValue().get(m).getSample());
								}
							}
						}
						// }

					}
					vardraw.inVarList = true;
				}

			}
			// TODO TAHAN
			// System.out.println(vardraw.getTranscripts().get(0).getGenename() +"..."
			// +vardraw.getTranscripts().get(1).getGenename() );
		}

		if (!vardraw.inVarList) {
			returnnode = vardraw.getNext();
			if (VariantHandler.allChroms.isSelected()) {
				vardraw.removeNode();
			}
			return returnnode;

		} else {
			mutcount = 0;
			for (Map.Entry<String, ArrayList<SampleNode>> entry: vardraw.vars.entrySet()) {
				if (vardraw.hideNodeVar(entry)) {
					if (VariantHandler.allChroms.isSelected()) {
						vardraw.vars.remove(entry.getKey());
						/* vardraw.vars.remove(v);
						v--; */
					}
					continue;
				}

				base = entry.getKey();

				for (int m = 0; m < entry.getValue().size(); m++) {
					if (entry.getValue().get(m).alleles != null) {
						break;
					}
					if (entry.getValue().get(m).getSample().annotation) {
						continue;
					}
					if (entry.getValue().get(m).hideVar(entry.getKey().length() > 1)) {
						if (VariantHandler.allChroms.isSelected()) {
							entry.getValue().remove(m);
							m--;
						}
						continue;
					}

					mutcount++;
				}
			}

			if (MainPane.projectValues.variantHandlerValues.commonVariantsMin > 1 && VariantHandler.clusterSize > 0) {

				if (VarMaster.getInstance().clusterNodes.size() == 0) {

					final ClusterNode cluster = new ClusterNode();
					cluster.nodecount = mutcount;
					cluster.ID = vardraw.clusterId;
					vardraw.clusterNode = cluster;
					cluster.varnodes.add(vardraw);
					cluster.width = 1;
					VarMaster.getInstance().clusterNodes.add(cluster);

				} else if (VarMaster.getInstance().clusterNodes
						.get(VarMaster.getInstance().clusterNodes.size() - 1).ID != vardraw.clusterId) {

					VarMaster.getInstance().clusterNodes
							.get(VarMaster.getInstance().clusterNodes.size() - 1).width = VarMaster.getInstance().clusterNodes
									.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes
											.get(VarMaster.getInstance().clusterNodes
													.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes.size() - 1)
											.getPosition()
									- VarMaster.getInstance().clusterNodes.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes
											.get(0).getPosition()
									+ 1;
					final ClusterNode cluster = new ClusterNode();
					cluster.nodecount += mutcount;
					cluster.ID = vardraw.clusterId;
					vardraw.clusterNode = cluster;
					cluster.varnodes.add(vardraw);
					cluster.width = 1;
					VarMaster.getInstance().clusterNodes.add(cluster);
				} else {

					vardraw.clusterNode = VarMaster.getInstance().clusterNodes.get(VarMaster.getInstance().clusterNodes.size() - 1);
					VarMaster.getInstance().clusterNodes.get(VarMaster.getInstance().clusterNodes.size() - 1).nodecount += mutcount;
					VarMaster.getInstance().clusterNodes.get(VarMaster.getInstance().clusterNodes.size() - 1).varnodes.add(vardraw);
				}
			}
			if (!VariantHandler.writetofile.isSelected()) {
				if (vardraw != null && vardraw.inVarList && vardraw.bedhit && vardraw.getBedHits() != null) {

					pretrack = -1;
					for (int i = 0; i < vardraw.getBedHits().size(); i++) {

						vardraw.getBedHits().get(i).inVarlist = true;

						if (pretrack != vardraw.getBedHits().get(i).getTrack().trackIndex) {
							vardraw.getBedHits().get(i).getTrack().getTable().variants += mutcount;
							pretrack = vardraw.getBedHits().get(i).getTrack().trackIndex;
						}
						if (vardraw.getBedHits().get(i).getTrack().getTable().bedarray == null) {
							vardraw.getBedHits().get(i).getTrack().getTable().bedarray = new ArrayList<BedNode>();
						}
						if (!vardraw.getBedHits().get(i).getTrack().getTable().bedarray.contains(vardraw.getBedHits().get(i))) {

							vardraw.getBedHits().get(i).mutations += mutcount;

							vardraw.getBedHits().get(i).getTrack().getTable().bedarray.add(vardraw.getBedHits().get(i));
							vardraw.getBedHits().get(i).getTrack().getTable().setPreferredSize(new Dimension(
									vardraw.getBedHits().get(i).getTrack().getTable().tablescroll.getViewport()
											.getWidth(),
									(vardraw.getBedHits().get(i).getTrack().getTable().getTableSize() + 2)
											* vardraw.getBedHits().get(i).getTrack().getTable().rowHeight));
							vardraw.getBedHits().get(i).getTrack().getTable().revalidate();

							// vardraw.inVarList = true;
						} else {
							vardraw.getBedHits().get(i).mutations += mutcount;
							// vardraw.inVarList = true;

						}
					}
				}
			}
		}
		if (VariantHandler.writetofile.isSelected()) {

			FileWrite.writeToFile(vardraw, FileWrite.output, FileWrite.outputgz);
		}
		lastVar = vardraw;

		return vardraw.getNext();
		// }
	}
  int getNextIntergenic(final int pos) {
		int returnpos = pos;
		int pointer = 0, gene = -1;

		while (true) {
			gene = MethodLibrary.getRegion(returnpos, Draw.getSplits().get(0), pointer);

			if (gene == -1) {
				break;
			} else {
				if (returnpos < Draw.getSplits().get(0).getGenes().get(gene).getEnd() + 1) {
					returnpos = Draw.getSplits().get(0).getGenes().get(gene).getEnd() + 1;
				}
				pointer = gene;
			}
		}

		return returnpos;
	}
	static void clearVariantsFromBeds() {

		for (int b = 0; b < MainPane.bedCanvas.bedTrack.size(); b++) {
			BedNode node = MainPane.bedCanvas.bedTrack.get(b).getHead();
			while (node != null) {
				if (node.varnodes != null) {
					node.varnodes = null;
				}
				node = node.getNext();
			}
		}

	}

	public static void nullifyVarNodes() {
		lastVar = null;
		lastWriteVar = null;
		returnnode = null;
	}

	void clearVariantsFromGenes() {
		SplitClass split = Draw.getSplits().get(0);

		for (int g = 0; g < split.getGenes().size(); g++) {
			split.getGenes().get(g).mutations = 0;
			split.getGenes().get(g).missense = 0;
			split.getGenes().get(g).nonsense = 0;
			split.getGenes().get(g).synonymous = 0;
			split.getGenes().get(g).intronic = 0;
			split.getGenes().get(g).utr = 0;
			split.getGenes().get(g).samples.clear();
			split.getGenes().get(g).varnodes.clear();
			split.getGenes().get(g).intergenic = false;
			split.getGenes().get(g).transcriptString = new StringBuffer();

		}
		split = null;
	}
}
