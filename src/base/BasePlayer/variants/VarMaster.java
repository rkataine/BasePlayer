/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.variants;

import java.util.ArrayList;
import java.util.Map;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.sample.SampleNode;

public class VarMaster {
	private static final VarMaster INSTANCE = new VarMaster();
	private VarNode head = null, current = null;
	public static int clusterId = 1;
	public ArrayList<ClusterNode> clusterNodes;

	private VarMaster() {
		
	}
	public void reset() {
		this.head = new VarNode("", -1, (byte) 0, "N", null, null);
		this.current = head;
	}
	public static VarMaster getInstance() {
		return INSTANCE;
	} 
	public VarNode getHead() {
		return this.head;
	}
	public void setHead(VarNode head) {
		this.head = head;
	}
	public VarNode getCurrent() {
		return this.current;
	}
	public void setCurrent(VarNode current) {
		this.current = current;
	}

	public VarNode searchFirstVariantToDraw(String chrom, int start) {
		try {
			if (current.getPosition() + current.length >= start && current.getPrev() != null && current.getPrev().getPosition() +1 + current.length< start) return current; 
			if (current.getPosition() + 1 < start) {
				while (current.getPosition()+ current.length + 1 < start) {
					if (FileRead.novars) {
						if (current.getNext().getChrom().equals(chrom))	current = current.getNext();
						else break;
					} else current = current.getNext();
				}
			} else {
				while (current.getPosition() - 1 + current.length > start) {
					if (FileRead.novars) {
						if (current.getPrev().getChrom() != null && current.getPrev().getChrom().equals(chrom))
							current = current.getPrev();
						else break;
					} else current = current.getPrev();
					
					if (current == null) break;
				}
			}
		} catch(Exception e) {
			return this.head;
		}
		return current;
	}
	public static void calcClusters(final VarNode node, final int i) {
		clusterId = i;
		calcClusters(node);
	}
	public static void calcClusters(VarNode node) {
		try {
			if (MainPane.projectValues.variantHandlerValues.commonVariantsMin < 1 || VariantHandler.clusterSize < 1 || node == null) return;
			boolean first = true;
			VarNode tempnode;
			int clustersum = 0;
			if (node.getNext() != null) {
				tempnode = node.getNext();

				while (tempnode != null) {
					tempnode.inVarList = false;
					tempnode.incluster = false;
					tempnode.clusterId = -1;
					tempnode = tempnode.getNext();
				}
				tempnode = null;
			}
			node = node.getNext(node);

			while (node != null) {
				tempnode = node;
				if (!node.incluster) {
					clustersum = 0;
					first = true;

					if (first && tempnode.getNext(tempnode) == null) {
						for (Map.Entry<String, ArrayList<SampleNode>> sampleNodes : tempnode.vars.entrySet()) {
							if (tempnode.hideNodeVar(sampleNodes)) continue;
							
							for (int i = 0; i < sampleNodes.getValue().size(); i++) {
								if (!sampleNodes.getValue().get(i).hideVar(sampleNodes.getKey().length() > 1)) clustersum++;
							}
						}
					}
					if (tempnode.getNext(tempnode) != null && tempnode.getNext(tempnode).getPosition() - tempnode.getPosition() > VariantHandler.clusterSize) {
						for (Map.Entry<String, ArrayList<SampleNode>> sampleNodes : tempnode.vars.entrySet()) {
							if (tempnode.hideNodeVar(sampleNodes)) continue;

							for (int i = 0; i < sampleNodes.getValue().size(); i++) {
								if (!sampleNodes.getValue().get(i).hideVar(sampleNodes.getKey().length() > 1)) clustersum++;
							}
						}
						if (clustersum > 1 && clustersum >= MainPane.projectValues.variantHandlerValues.commonVariantsMin) {
							tempnode.incluster = true;
							tempnode.clusterId = clusterId;
							clusterId++;
						}
						node = tempnode.getNext(tempnode);
						continue;
					}

					while (tempnode.getNext(tempnode) != null && tempnode.getNext(tempnode).getPosition()	- tempnode.getPosition() <= VariantHandler.clusterSize) {
						if (first) {
							clustersum = 0;
							for (Map.Entry<String, ArrayList<SampleNode>> sampleNodes : tempnode.vars.entrySet()) {
								if (tempnode.hideNodeVar(sampleNodes)) continue;

								for (int i = 0; i < sampleNodes.getValue().size(); i++) {
									if (sampleNodes.getValue().get(i).hideVar(sampleNodes.getKey().length() > 1)) continue;
									tempnode.clusterId = clusterId;
									tempnode.incluster = true;
									clustersum++;									
								}
							}
							tempnode = tempnode.getNext(tempnode);

							for (Map.Entry<String, ArrayList<SampleNode>> sampleNodes : tempnode.vars.entrySet()) {
								if (tempnode.hideNodeVar(sampleNodes)) continue;
								for (int i = 0; i < sampleNodes.getValue().size(); i++) {
									if (sampleNodes.getValue().get(i).hideVar(sampleNodes.getKey().length() > 1)) continue; 
									tempnode.incluster = true;
									tempnode.clusterId = clusterId;
									clustersum++;									
								}
								first = false;
							}
						} else {
							tempnode = tempnode.getNext(tempnode);
							for (Map.Entry<String, ArrayList<SampleNode>> sampleNodes : tempnode.vars.entrySet()) {
								if (tempnode.hideNodeVar(sampleNodes)) continue;

								for (int i = 0; i < sampleNodes.getValue().size(); i++) {
									if (sampleNodes.getValue().get(i).hideVar(sampleNodes.getKey().length() > 1)) continue;
										tempnode.clusterId = clusterId;
										tempnode.incluster = true;
										clustersum++;								
								}
							}
						}
					}
				} else {
					node = node.getNext(node);
				}

				if (clustersum < MainPane.projectValues.variantHandlerValues.commonVariantsMin) {
					tempnode.incluster = false;
					while (tempnode != null && !tempnode.equals(node)) {
						tempnode.incluster = false;
						tempnode = tempnode.getPrev();
					}
					node.incluster = false;
					node = node.getNext(node);
				}	else {
					node.incluster = true;
					node.clusterId = clusterId;
					clusterId++;
					node = tempnode.getNext(node);
				}
			}
			tempnode = node = null;
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());	e.printStackTrace();
		}
	}
}
