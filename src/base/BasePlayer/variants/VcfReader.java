package base.BasePlayer.variants;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPInputStream;

import javax.swing.JOptionPane;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.Setter;
import base.BasePlayer.GUI.BedCanvas;
import base.BasePlayer.GUI.BedTrack;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.Control;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.io.FileWrite;
import base.BasePlayer.io.FileRead.SearchBamFiles;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.sample.SampleNode;
import htsjdk.tribble.index.Block;
import htsjdk.tribble.index.Index;
import htsjdk.tribble.index.IndexFactory;
import htsjdk.tribble.index.tabix.TabixIndex;

public class VcfReader {
  boolean cancelfileread = false, cancelvarcount = false;
	static HashMap<String, Sample> sampleHash = new HashMap<String, Sample>();
	int linecounter = 0;
	boolean stop = false;
	public static boolean asked = false;
	public static HashMap<String, Float> advQualities = null;
	public static HashMap<String, Float> advQualitiesIndel = null;
  public static void main(String args[]) {
	
	/* try {
		File file = new File("C:\\LocalData\\rkataine\\BasePlayer\\samples\\Fam_c491_1_7810_T_LP6005745-DNA_E03_GRCh38_Mutect2_PASS_MSS.vcf.gz");
		VCFReader reader = new VCFFileReader(file);
		//CloseableIterator<VariantContext> iterator = reader.query("chr20", 1, 64444167);
		CloseableIterator<VariantContext> iterator = reader.query("chr20", 58839686, 58911665);
		

		VariantContext line = null;
		while((line = iterator.next()) != null) {
			if (line.getStart() > 57839718 && line.getStart() < 58911192) {
				System.out.println(line.getStart());
			}
			
		}

		reader.close();
	} catch(Exception e) {
		e.printStackTrace();
	} */
		//VcfReader reader = new VcfReader();
		//Sample sample = new Sample("sample", (short) 1, null);
		//String testi = "chr1	12679694	.	CAAAAAAAAAAA	C,CAA,CAAAAA,CAAAAAA,CAAAAAAAAA	.	panel_of_normals	DP=101;ECNT=1;IN_PON;NLOD=11.89,7.36,6.49,8.3,2.37;N_ART_LOD=-0.9445,1.9,1.41,-0.8975,1.18;POP_AF=1e-05,1e-05,1e-05,1e-05,1e-05;P_CONTAM=0;P_GERMLINE=-12.67,-8.201,-6.176,-7.228,-2.29;RPA=22,11,13,16,17,20;RU=A;STR;TLOD=5.01,5.1,4.9,9.4,3.52	GT:AD:AF:F1R2:F2R1:MBQ:MFRL:MMQ:MPOS:SA_MAP_AF:SA_POST_PROB	0/0:23,0,1,1,0,2:0.027,0.058,0.071,0.065,0.189:14,0,1,1,0,0:9,0,0,0,0,2:0,37,35,0,24:500,0,466,579,0,491:0,60,60,0,60:0,55,2,0,39:.:.	0/1/2/3/4/5:14,2,2,4,6,4:0.072,0.087,0.124,0.165,0.174:8,1,0,2,4,3:6,1,2,2,2,1:35,36,36,36,36:536,703,542,568,615,607:60,60,60,60,60:49,42,44,29,39:0.131,0.182,0.188:0.023,0.015,0.962";
		//reader.readVCFLine(testi.split("\t"), null);
  }
	public static String[] parseALT(final String ref, final String alt, final String[] genotype) {
		
		//int noref = (genotype != null && !genotype[0].equals("0")) ? 1 : 0;
		
		//SNV call
		
		if (ref.length() == 1 && alt.length() == 1) return new String[] { alt };
		//multiallelic
		
		if (alt.contains(",")) {			
			String[] altarray = alt.split(",");
			ArrayList<String> alts = new ArrayList<String>();
			//for controlling read all ALTs
			if (genotype == null) {
				for (int i = 0; i < altarray.length; i++) {
					alts.add(parseALT(ref, altarray[i], null)[0]);
				}
				return alts.toArray(new String[0]);
			}
			for (int i = 0; i < genotype.length; i++) {
				if (i > 0 && (genotype[i].equals(genotype[i-1]))) break;
				if (genotype[i].equals("0")) continue;
				
				int gt = genotype == null ? i : Integer.parseInt(genotype[i]) - 1;
				if (altarray[gt].equals("*")) { genotype[0] = "0"; continue; } 
				alts.add(parseALT(ref, altarray[gt], genotype)[0]);
				
			}
			
			return alts.toArray(new String[0]);
		}

		//MNV call
		if (alt.length() == ref.length()) {
			//TODO phased
			return new String[] { alt.substring(0, 1) };
		} 
		//indel calls
		else if (alt.length() > ref.length()) return new String[] { "ins_" +alt.substring(ref.length()) };
		else return new String[] { "del_" +ref.substring(1, (ref.length() - alt.length()) + 1) };
	}
	
	public static ArrayList<VarNode> parseSV(final String[] split, final boolean translocations) {
		ArrayList<VarNode> breakpoints = new ArrayList<VarNode>();
		final String infoField = split[7];
		final int pos1 = Integer.parseInt(split[1]) - 1;
		final String chr1 = split[0].replace("chr", "");
		
		String[] endpoints = MethodLibrary.getTranslocationBreak(split);
		String chr2 = endpoints[0];
		
		int endpoint = Integer.parseInt(endpoints[1]);
		
		chr2 = !chr2.equals("") ? chr2.replace("chr", "") : chr1;
		final Boolean tra = chr2 != null && !chr1.equals(chr2);
		final String type = tra ? "TRA" : MethodLibrary.getValueFromInfo("SVTYPE", infoField, false);
		final String typeString = tra ? type +"_" +chr1 +"-" +chr2 : type +" " +MethodLibrary.formatNumber(Math.abs(endpoint - pos1));
		VarNode first = new VarNode(chr1, pos1, (byte)'N', typeString, null, null);	
		first.length = Math.abs((int)Double.parseDouble(MethodLibrary.getValueFromInfo("SVLEN", infoField, true)));
		
		VarNode second = endpoint > -1 ? new VarNode(chr2, endpoint, (byte)'N', typeString, null, null) : null;
		
		if (translocations) breakpoints.add(second);
		if (!translocations) breakpoints.add(first);
		if (second != null) {
			first.setAsPairs(second);
			second.primary = false;
			if (!tra || translocations) breakpoints.add(second);		
		}

		return breakpoints;	
	}	
	/* if (linecounter == 0 || pos - linecounter > (Draw.drawVariables.variantsEnd - Draw.drawVariables.variantsStart) / 100) {
				MainPane.drawCanvas.loadBarSample = (int) (((pos - Draw.drawVariables.variantsStart) / (double) (Draw.drawVariables.variantsEnd - Draw.drawVariables.variantsStart)) * 100);			
				linecounter = pos;
				if (search) {
					if (!Getter.getInstance().loading()) {
						
					}
				}

			} */


  public ArrayList<VarNode> readVCFLine(final int pos, final String[] split, Sample sample) {
		ConcurrentHashMap<String, ArrayList<SampleNode>> sampleNodes = new ConcurrentHashMap<String, ArrayList<SampleNode>>();
		ArrayList<VarNode> result = new ArrayList<VarNode>();
		
		try {
			if (split.length < 2) return null;
			if (split[0].startsWith("#") || (split.length > 4 && split[4].equals("*"))) return null;
			if (split.length == 2) {
				VarNode node = new VarNode(split[0], Integer.parseInt(split[1]), (byte)('C'), ".", null, null);
				SampleNode samplenode = new SampleNode(null, null, 0, 100F, 100F, null, null, Getter.getInstance().getSampleList().get(0));
				sampleNodes.put("G", new ArrayList<SampleNode>(Arrays.asList(samplenode)));
				node.vars = sampleNodes;
				result.add(node);
				return result;
			}
			
			String filterString = split.length > 6 ? split[6] : null;
			String advQualString = split.length > 7 ? split[7] : null;
						
			final HashMap<String, Integer> infofield = MethodLibrary.splitInfofield(split);
			String qualField = split[5];
			Float quality = !qualField.equals(".") ? Float.parseFloat(qualField) : 0F;
			Integer gtindex = infofield.get("GT");
			Integer coverageIndex = infofield.get("AD");
			Integer gqindex = infofield.get("GQ") == null ? infofield.get("MBQ") : infofield.get("GQ");
			byte ref = (byte) split[3].charAt(0);
			String rscode = split[2]; // TODO different rs-codes to indels
			String chr = split[0].replace("chr", "");
			VarNode node = new VarNode(chr, pos, ref, rscode, null, null);
			
			for (int s = 9; s < split.length; s++) {			
				Sample currentSample = sample.multiVCF ? Getter.getInstance().getSampleList().get(sample.getIndex() + 1 + s - 9) : sample;
				int infoColumn = sample.somaticColumn == null ? s : sample.somaticColumn;
				
				String[] info = split[infoColumn].split(":");
				
				if (gtindex == null || info[gtindex].contains(".")) continue;

				String[] genotype = info[gtindex].contains("/") ? info[gtindex].split("/") : info[gtindex].split("\\|");
							
				if (info[gtindex].contains("/") && genotype[1].equals("0")) continue;
				
				String[] altBases = parseALT(split[3], split[4], genotype);
				
				int[] coverages;
				// TODO nanopore VCF-hack
				if (coverageIndex == null)
					coverages = getNonstandardCoverages(split);
				else 
					//if (coverageIndex > info.length -1) continue;
					coverages = Arrays.stream(info[coverageIndex].split(",")).mapToInt(Integer::parseInt).toArray();

				if (coverages.length < 2) { continue;}
				Float[] gq = (gqindex != null && !info[gqindex].equals(".")) ? Arrays.stream(info[gqindex].split(",")).map(Float::valueOf).toArray(Float[]::new) : null;
				
				for (int i = 0; i < altBases.length; i++) {	
					
					if (!currentSample.annoTrack && (checkAdvFilters(filterString, altBases[i].length() > 1) || checkAdvQuals(advQualString, infofield, info, altBases[i].length() > 1, false) > 0)) continue;
					Float gquality = null;
					if (gq != null) gquality = i > gq.length -1 ? gq[0] : gq[i]; // sometimes genotype quality is for all ALTs. Sometimes not...
					SampleNode samplenode = null;
					try {						
						samplenode = new SampleNode(coverages, genotype, i, quality, gquality, null, filterString, currentSample);
					} catch(Exception e) { e.printStackTrace(); continue; }
					if (altBases[i].length() > 1) node.indel = true; 
					if (VariantHandler.freeze.isSelected() && checkFreeze(samplenode, altBases[i], split[2])) continue;	
								
					if (sampleNodes.containsKey(altBases[i])) sampleNodes.get(altBases[i]).add(samplenode);
					else sampleNodes.put(altBases[i], new ArrayList<SampleNode>(Arrays.asList(samplenode)));				
				}
				if (sample.somaticColumn != null) break;
			}
			
			node.vars = sampleNodes;
			result.add(node);
		} catch (final Exception ex) {			
			ex.printStackTrace();			
		}	
		return result;
	}
	private int[] getNonstandardCoverages(final String[] line) {
		if (line.length < 8) return null;
		String info = line[7];
		if (info.contains("AC"))
			return Arrays.stream(MethodLibrary.getValueFromInfo("AC", info, false).split(",")).mapToInt(Integer::parseInt).toArray();
		
		return null;
	}
	private int[] getSVCoverages(final HashMap<String, Integer> field, final String[] info) {
		int[] res = {0, 0};
		boolean found = false;
		if (field.containsKey("PR")) {
			res = Arrays.stream(info[field.get("PR")].split(",")).mapToInt(Integer::parseInt).toArray();
			found = true;
		}
		if (field.containsKey("SR")) {
			int[] splits = Arrays.stream(info[field.get("SR")].split(",")).mapToInt(Integer::parseInt).toArray();
			res[0] += splits[0];
			res[1] += splits[1];
			found = true;
		}
		return found ? res : new int[]{50, 100};
	}
	public ArrayList<VarNode> readSVLine(final String[] split, Sample sample, final boolean translocations) {
		ConcurrentHashMap<String, ArrayList<SampleNode>> sampleNodes = new ConcurrentHashMap<String, ArrayList<SampleNode>>();
		ArrayList<VarNode> result = new ArrayList<VarNode>();
		String filterString = split.length > 6 ? split[6] : null;
		String advQualString = split.length > 7 ? split[7] : null;
		
		try {
			if (split.length < 8) return null;
			
			final HashMap<String, Integer> infofield = MethodLibrary.splitInfofield(split);
		
			Float quality = !split[5].equals(".") ? (float) Double.parseDouble(split[5]) : 0F;
			
			
			result = parseSV(split, translocations);
			String svtype = result.get(0).rscode;
			
			if (infofield != null) {
				for (int s = 9; s < split.length; s++) {
				
					Sample currentSample = sample.multiVCF ? Getter.getInstance().getSampleList().get(sample.getIndex() + 1 + s - 9) : sample;
					int infoColumn = sample.somaticColumn == null ? s : sample.somaticColumn;
					
					String[] info = split[infoColumn].split(":");
					//int gtindex = infofield.get("GT");
					//String[] genotype = info[gtindex].contains("/") ? info[gtindex].split("/") : info[gtindex].split("\\|");
					
					int[] coverages = getSVCoverages(infofield, info);
					Integer SVid = infofield.get("ID");
					if (SVid != null && info[SVid].contains("NaN")) continue;
					if (checkAdvFilters(filterString, false) || checkAdvQuals(advQualString, infofield, info, false, true) > 0) continue;
					SampleNode samplenode = new SampleNode(coverages, null, 0, quality, 60F, null, null, currentSample);
					
					if (sampleNodes.containsKey(svtype)) sampleNodes.get(svtype).add(samplenode);
					else sampleNodes.put(svtype, new ArrayList<SampleNode>(Arrays.asList(samplenode)));
				
					if (sample.somaticColumn != null) break;
				}
			} else {
				
				if (checkAdvFilters(filterString, false) || checkAdvQuals(advQualString, null, null, false, true) > 0) return null;
				
				SampleNode samplenode = new SampleNode(null, null, 0, quality, 60F, null, null, sample);
				sampleNodes.put(svtype, new ArrayList<SampleNode>(Arrays.asList(samplenode)));
			}
			result.get(0).vars = sampleNodes;
			if (result.get(0).getPair() != null) result.get(0).getPair().vars = sampleNodes;
		} catch (final Exception ex) {
			ErrorLog.addError(ex.getStackTrace());
			ex.printStackTrace();
		}	
		return result;
	}
	public ArrayList<VarNode> readKatri(final int pos, final String[] split, Sample sample) {
		ConcurrentHashMap<String, ArrayList<SampleNode>> sampleNodes = new ConcurrentHashMap<String, ArrayList<SampleNode>>();
		ArrayList<VarNode> result = new ArrayList<VarNode>();

		try {
		
			String chr = split[0].replace("chr", "");
			VarNode node = new VarNode(chr, pos, (byte)'N', ".", null, null);
			node.length = Integer.parseInt(split[2]) - pos;

			for (int s = 3; s < split.length; s++) {			
				Sample currentSample = sample.multiVCF ? Getter.getInstance().getSampleList().get(sample.getIndex() + 1 + s - 3) : sample;
				int infoColumn = s;
				String type = split[infoColumn];
				SampleNode samplenode = new SampleNode(null, null, 0,100F,100F, null, null, currentSample);
				if (sampleNodes.containsKey(type)) sampleNodes.get(type).add(samplenode);
				else sampleNodes.put(type, new ArrayList<SampleNode>(Arrays.asList(samplenode)));			
			}
			node.vars = sampleNodes;
			result.add(node);
		} catch (final Exception ex) {
			ex.printStackTrace();
		}	
		return result;
	}
  public void getVariants(final String chrom, final int start, final int end, final Sample sample) {
	
	if (VariantHandler.hideIndels.isSelected() && VariantHandler.hideSNVs.isSelected()) return;

	  cancelfileread = cancelvarcount = false;
	  
	  	//Getter.getInstance().getVariantHead().putNext(null);
	  
		try {
			FileRead.readFiles = true;
			Draw.getSplits().get(0).transStart = 0;
			
			Draw.drawVariables.variantsStart = start;
			Draw.drawVariables.variantsEnd = end;
			MainPane.drawCanvas.loadbarAll = (int) ((sample.getIndex() / (double) (MainPane.samples)) * 100);
			
			linecounter = 0;
			if (cancelfileread) { cancelFileRead();	return;	}
			if (sample.getTabixFile() == null || sample.multipart) return;				
		
			final String searchChrom = setVCFFileStart(chrom, start, end, sample);
			
			final boolean vcf = sample.getVCFReader() != null;
			if (vcf) sample.getVCFReader().ready();

			sample.setMaxCoverage(0F);
		
			String line = "";

			stop = false;
			VarNode current = Getter.getInstance().getVariantHead();
			
			boolean foundchrom = false;
			
			if(sample.breakpoints != null) {
				
				for (String[] split: sample.breakpoints) {					

					if(!MethodLibrary.getTranslocationBreak(split)[0].equals(searchChrom)) continue;
					
					ArrayList<VarNode> varNodes = readSVLine(split, sample, true);
					if (varNodes == null) continue;				
					for (VarNode node : varNodes) current = addVarnode(node, current);		
				}
			}
			while (true) {
				if (stop || cancelfileread || cancelvarcount || !Getter.getInstance().loading()) {	cancelFileRead();	break;	}
				line = vcf ? sample.getVCFReader().readLine() : sample.getVCFInput().readLine();
				if (line == null) break;
				if (line.startsWith("#")) continue;
				if (line.startsWith("chrom")) { continue; } 
				String[] split = line.split("\\t+");
				//if (line.contains("BND")) continue;
				if (split.length < 2) continue;
				if (vcf) {
					if (!foundchrom && !split[0].equals(searchChrom)) continue;
					foundchrom = true;
					if (foundchrom && !split[0].equals(searchChrom)) break;
				}				
				
				if (sample.getVCFInput() != null && sample.getVCFInput().getFilePointer() > sample.vcfEndPos) break;
				
				int pos = Integer.parseInt(split[1]) - 1;
				
				if (sample.katri) {
					if (Integer.parseInt(split[2]) - 1 < Draw.drawVariables.variantsStart) continue;	
					else if (pos >= Draw.drawVariables.variantsEnd) {	stop = true; break;	}
				} else {
					if (pos < Draw.drawVariables.variantsStart) continue;		
					else if (pos >= Draw.drawVariables.variantsEnd) {	stop = true; break;	}
				}
				// Parsing vcf line
				
				ArrayList<VarNode> varNodes;
				
				if (sample.katri) varNodes = readKatri(pos, split, sample);
				else varNodes = sample.SV ? readSVLine(split, sample, false) : readVCFLine(pos, split, sample);
				
				if (varNodes == null) continue;
				
				for (VarNode node : varNodes) current = addVarnode(node, current);			
				
			}
			
			if (sample.getVCFReader() != null) sample.getVCFReader().close();	
			Loader.ready("Loading variants");		
			
		} catch (final Exception exc) {
			MainPane.showError(exc.getMessage(), "Error");
			exc.printStackTrace();
			ErrorLog.addError(exc.getStackTrace());
			FileRead.changing = false;
		}
		Setter.getInstance().setUpdateScreen();
	}
	VarNode addVarnode(VarNode node, VarNode current) {
		int pos = node.getPosition();	
		
		if (current.getPosition() < pos) {
			while (current.getNext() != null && current.getNext().getPosition() <= pos) current = current.getNext();
		} else if (current.getPosition() > pos) {
			while (current.getPosition() > pos) { current = current.getPrev(); }
		}
		
		if (current.getPosition() < pos) {			
			if (current.getNext() != null) {
				current.getNext().putPrev(node);
				node.putNext(current.getNext());
			}
			node.putPrev(current);
			current.putNext(node);
			current = node;
		} else if (current.getPosition() == pos) {
			for (Map.Entry<String, ArrayList<SampleNode>> entry : node.vars.entrySet()) 
				for (SampleNode sNode : entry.getValue()) current.addSample(entry.getKey(), sNode);
		}
		return current;
	}
	boolean checkFreeze(SampleNode node, String altbase, String rscode) {
		if (!node.getSample().annoTrack) {
			if (node.getCoverage() < MainPane.projectValues.variantHandlerValues.variantCoverage) return true;
			if (node.getQuality() < MainPane.projectValues.variantHandlerValues.variantQuality) return true;
			if (node.getGQ() != null && node.getGQ() < MainPane.projectValues.variantHandlerValues.variantGQ) return true;
			if (node.getAlleleFraction() < MainPane.projectValues.variantHandlerValues.variantCalls / 100.0) return true;				
		}
		if (!node.getSample().annotation) {
			if (VariantHandler.hideSNVs.isSelected() && altbase.length() == 1) return true;
			if (VariantHandler.hideIndels.isSelected() && altbase.length() > 1) return true;
			if (VariantHandler.rscode.isSelected() && !rscode.equals(".")) return true;
		}
		return false;
	}
  public void getVariantWindow(final String chrom, final int start, final int end) {
		try {
			FileRead.lastpos = 0;
			VarCalculations.removeNonListVariants();
			FileRead.removeBedLinks();

			for (int i = 0; i < Getter.getInstance().getControlList().size(); i++) {
				Getter.getInstance().getControlList().get(i).controlled = false;
			}
			FileRead.readFiles = true;
			VarMaster.getInstance().reset();
			cancelvarcount = false;
			cancelfileread = false;

			Loader.loadingtext = "Loading variants...";
			for (int i = 0; i < MainPane.samples; i++) {
				if (cancelvarcount || cancelfileread) {
					cancelFileRead();
					break;
				}
				if (Getter.getInstance().getSampleList().get(i).getTabixFile() == null || Getter.getInstance().getSampleList().get(i).multipart) {
					continue;
				}

				VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());
				getVariants(chrom, start, end, Getter.getInstance().getSampleList().get(i));

			}
			FileRead.readFiles = false;
			FileRead.annotate();

			if (Control.controlData.controlsOn) {
				Control.applyControl();
			}
			MainPane.bedCanvas.intersected = false;
			if (FileRead.bigcalc) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead());
			} else {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}

			if (MainPane.bedCanvas.bedOn) {
				// VarNode current = Getter.getInstance().getVariantHead().getNext();

				/*
				 * while(current != null) {
				 * 
				 * current.bedhit = true; current = current.getNext(); }
				 */
				final ArrayList<BedTrack> bigs = new ArrayList<BedTrack>();
				int smalls = 0;
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					if (!MainPane.bedCanvas.bedTrack.get(i).small
							|| MainPane.bedCanvas.bedTrack.get(i).getZoomlevel() != null) {
						if (MainPane.bedCanvas.bedTrack.get(i).intersect) {
							bigs.add(MainPane.bedCanvas.bedTrack.get(i));
							MainPane.bedCanvas.bedTrack.get(i).intersect = false;
						}
					} else {
						if (MainPane.bedCanvas.bedTrack.get(i).intersect) {
							smalls++;
						}
					}
				}
				if (smalls == 0) {
					VarNode current = Getter.getInstance().getVariantHead().getNext();
					while (current != null) {

						current.bedhit = true;
						current = current.getNext();
					}
				}
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					MainPane.bedCanvas.bedTrack.get(i).used = false;
					if (MainPane.bedCanvas.bedTrack.get(i).small && MainPane.bedCanvas.bedTrack.get(i).getZoomlevel() == null) {
						if (MainPane.bedCanvas.bedTrack.get(i).intersect && !MainPane.bedCanvas.bedTrack.get(i).loading) {
							MainPane.bedCanvas.annotate(MainPane.bedCanvas.bedTrack.get(i).getHead(), Getter.getInstance().getVariantHead().getNext());
						} else if (MainPane.bedCanvas.bedTrack.get(i).intersect && MainPane.bedCanvas.bedTrack.get(i).loading) {
							MainPane.bedCanvas.bedTrack.get(i).waiting = true;
						}
					}
					// else if(MainPane.bedCanvas.bedTrack.get(i).intersect) {

					/*
					 * BedCanvas.Annotator annotator = MainPane.bedCanvas.new
					 * Annotator(MainPane.bedCanvas.bedTrack.get(i)); annotator.annotateVars();
					 */
					// }
				}
				if (bigs.size() > 0) {
					for (int i = 0; i < bigs.size(); i++) {
						bigs.get(i).intersect = true;
						final BedCanvas.Annotator annotator = MainPane.bedCanvas.new Annotator(bigs.get(i));
						annotator.annotateVars();
					}
					bigs.clear();
				}
				MainPane.bedCanvas.intersected = true;
				if (FileRead.bigcalc) {
					VarMaster.calcClusters(Getter.getInstance().getVariantHead());
				} else {
					VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);

				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static String getVCFLine(VarNode node, Sample sample) {
		if (Draw.variantcalculator) return "";
		String line = "";

		FileRead.cancelfileread = false;
		VarCalculations.cancelvarcount = false;

		try {
			if (!(VariantHandler.hideIndels.isSelected() && VariantHandler.hideSNVs.isSelected())) {
				if (sample.multipart) {
					for (int i = sample.getIndex(); i >= 0; i--) {
						if (!Getter.getInstance().getSampleList().get(i).multipart) {
							sample = Getter.getInstance().getSampleList().get(i);
							break;
						}
					}
				}
				if (sample.getTabixFile() != null) {
					if (!node.primary) node = node.getPair();
					setVCFFileStart(node.getChrom(), node.getPosition(), node.getPosition() + 3, sample);
					final boolean vcf = sample.getVCFReader() != null;
				
					while (line != null) {
						try {
							if (vcf) line = sample.getVCFReader().readLine();
							else line = sample.getVCFInput().readLine();
							
							if (line == null || line.split("\t").length < 3 || line.startsWith("#")) continue;
							//System.out.println(line);
							if (line != null && Integer.parseInt(line.split("\t")[1]) == node.getPosition() + 1) {
								if (sample.getVCFReader() != null) sample.getVCFReader().close();
								return line;
							}
						} catch (final Exception ex) {
							MainPane.showError(ex.getMessage(), "Error");
							ErrorLog.addError(ex.getStackTrace());
							ex.printStackTrace();
							MainPane.cancel();
							FileRead.changing = false;
						}
					}

					if (sample.getVCFReader() != null) sample.getVCFReader().close();
					return line;
				}
			}
		} catch (final Exception exc) {
			MainPane.showError(exc.getMessage(), "Error");
			System.out.println(sample.getName());
			exc.printStackTrace();
			ErrorLog.addError(exc.getStackTrace());
			FileRead.changing = false;
		}
		return "";
	}

	/* public static String getVariant(final String ref, final String alt) {

		if (ref.length() == 1 && alt.length() == 1) {
			return alt;
		} else if (ref.length() == 1 || alt.length() == 1) {
			if (ref.length() == 2) {
				return "del" + ref.substring(1);
			} else if (alt.length() >= 2) {
				return "ins" + alt.substring(1);
			} else {
				return "del" + (ref.length() - 1);
			}

		} else if (alt.length() == ref.length()) {

			if (ref.contains("-")) {

				return getVariant(ref, "" + alt.charAt(0));
			} else if (alt.contains("-")) {
				return getVariant(ref, "" + alt.replaceAll("-", ""));
			} else {
				return "" + alt.charAt(0);
			}
		} else {

			if (ref.length() < alt.length()) {

				return "ins" + alt.substring(1, alt.length() - (ref.length() - 1));

			} else {

				if (ref.length() - alt.length() == 1) {
					return "del" + ref.substring(1, ref.length() - (alt.length() - 1));
				} else {
					return "del" + (ref.length() - alt.length());
				}

			}

		}

	} */
  static String setVCFFileStart(String chrom, final int start, final int end, final Sample sample) {
		try {
			Index index = null;
			if (sample.getVCFReader() != null) {
				try {
					index = IndexFactory.loadIndex(sample.getTabixFile() + ".idx");
				} catch (final Exception e) {
					File tempFile = new File(sample.getTabixFile() + ".tbi");
					if(tempFile.exists()) index = IndexFactory.loadIndex(sample.getTabixFile() + ".tbi");
				}
			} else {
				try {
					index = new TabixIndex(new File(sample.getTabixFile() + ".tbi"));
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			if (index == null) {
				sample.setInputStream();
				return sample.vcfchr + chrom;
			}
			java.util.List<Block> blocks = null;
			
			if (index.containsChromosome(sample.vcfchr + chrom)) {
				chrom = sample.vcfchr + chrom;
				
				try {
					blocks = index.getBlocks(chrom, start, end);
				} catch (final Exception e) {
					sample.vcfEndPos = 0;
					return "";
				}
				
				if (blocks.size() > 0) {

					if (sample.getVCFReader() != null) {
						sample.setInputStream();
						sample.getVCFReader().skip(blocks.get(0).getStartPosition());
					} else {
						try {
							sample.getVCFInput().seek(0);
							sample.getVCFInput().seek(blocks.get(0).getStartPosition());
						} catch (final Exception e) {
							ErrorLog.addError(e.getStackTrace());
							return "";
						}
					}

					sample.vcfEndPos = blocks.get(blocks.size() - 1).getEndPosition();

				} /* else {

					if (sample.getVCFReader() != null) {
						sample.setInputStream();
					} else {
						sample.getVCFInput().seek(0);
					}
				} */

			}/*  else {

				if (index.containsChromosome(sample.vcfchr + (MenuBar.chromosomeDropdown.getSelectedIndex() + 1))) {
					try {
						blocks = index.getBlocks(sample.vcfchr + (MenuBar.chromosomeDropdown.getSelectedIndex() + 1),
								start, end);
					} catch (final Exception e) {
						sample.vcfEndPos = 0;
						return "";
					}

					if (blocks.size() > 0) {
						if (sample.getVCFReader() != null) {
							sample.setInputStream();
							sample.getVCFReader().skip(blocks.get(0).getStartPosition());
						} else {
							sample.getVCFInput().seek(0);
							sample.getVCFInput().seek(blocks.get(0).getStartPosition());
						}
					} else {
						if (sample.getVCFReader() != null) {
							sample.setInputStream();
						} else {
							sample.getVCFInput().seek(0);
						}
					}
					chrom = sample.vcfchr + (MenuBar.chromosomeDropdown.getSelectedIndex() + 1);
				} else {
					sample.vcfEndPos = 0;
				}
			} */

		} catch (final Exception e) {
			e.printStackTrace();

		}
		return chrom;
	}
  boolean checkAdvFilters(final String split, final boolean indel) {
		if (split == null) return false;
		
			if (VariantHandler.advQDraw != null && VariantHandler.advQDraw.size() > 0) {
				
				for (int i = 0; i < VariantHandler.advQDraw.size(); i++) {
					QualEntry entry = VariantHandler.advQDraw.get(i);
					if (split.contains(entry.key) == !entry.reverse) {
						return true;
					}
				}
			}

		return false;
	}
	
  int checkAdvQuals(final String split, final HashMap<String, Integer> infofield, final String[] info, final boolean indel, final boolean SV) {
		
		if (split == null) return 0;
			
		
		if (VariantHandler.advQDraw == null || VariantHandler.advQDraw.size() == 0) return 0;
		for (int i = 0; i < VariantHandler.advQDraw.size(); i++) {
			String key = VariantHandler.advQDraw.get(i).key;
			
			if (key.contains("/")) key = key.split("/")[0]; // manual AF filter
			
			if (infofield != null && infofield.containsKey(key)) {
				
				float value = VariantHandler.advQDraw.get(i).value;
				String infoVal = info[infofield.get(key)];
				float infoValue;
				//TODO onko hyvä?
				if (infoVal.contains(",")) infoValue = Float.parseFloat(infoVal.split(",")[1]);
				else infoValue = Float.parseFloat(info[infofield.get(key)]);

				if (VariantHandler.advQDraw.get(i).format.equals("<")) {
					if (infoValue < value) {
						return 1;
					}
				} else if (VariantHandler.advQDraw.get(i).format.equals("<=")) {
					if (infoValue <= value) {
						return 1;
					}
				} else if (VariantHandler.advQDraw.get(i).format.equals(">")) {
					if (infoValue > value) {
						return 1;
					}
				} else {
					if (infoValue >= value) {
						return 1;
					}
				}
			} else if (split.contains(key)) {
				Float splitValue = 1F;
				
				if (split.indexOf(key + "=") < 0) {
					splitValue = -1F;
				} else {
					
					String valueString = MethodLibrary.getValueFromInfo(key, split, false);
					
					// TODO: tämäkin
					if (valueString.contains(",")) splitValue = Float.parseFloat(valueString.split(",")[1]);
					else splitValue = Float.parseFloat(valueString);
					
					/* try {						
						splitValue = Float.parseFloat(split.substring(split.indexOf(key + "=") + key.length() + 1).split(";")[0].replaceAll("[^\\d.]", ""));
					} catch(Exception e) {
						splitValue = Float.parseFloat(split.substring(split.indexOf(key + "=") + key.length() + 1).split(";")[0].replaceAll("[^\\d]", ""));
					} */
				}
				
				if (SV && key.equals("SVLEN")) {
					if (splitValue == 0) continue; // if SV is filtered by length. TRA is not filtered out as the len is zero
					splitValue = Math.abs(splitValue);
				}
				if (VariantHandler.advQDraw.get(i).key.contains("/")) {
					String key2 = VariantHandler.advQDraw.get(i).key.split("/")[1];
					try {
						Float splitValue2 = Float.parseFloat(split.substring(split.indexOf(key2 + "=") + key2.length() + 1).split(";")[0].replaceAll("[^\\d.]", ""));
						splitValue = splitValue / splitValue2;
					} catch(Exception e) {
						Float splitValue2 = Float.parseFloat(split.substring(split.indexOf(key2 + "=") + key2.length() + 1).split(";")[0].replaceAll("[^\\d]", ""));
						splitValue = splitValue / splitValue2;
					}
				}
				float value = VariantHandler.advQDraw.get(i).value;
				
				if (VariantHandler.advQDraw.get(i).format.equals("<")) {
					
					if (splitValue < value) {
						return 2;
					}
				} else if (VariantHandler.advQDraw.get(i).format.equals("<=")) {
					if (splitValue <= value) {
						return 2;
					}
				} else if (VariantHandler.advQDraw.get(i).format.equals(">")) {
					if (splitValue > value) {
						return 2;
					}
				} else {
					if (splitValue >= value) {
						return 2;
					}
				}
			}
		}			
		
			

		return 0;
	}
	public void readVCF(final File[] files, int start, int end) {
		try {
			if (files.length == 1 && files[0].getName().endsWith(".tbi")) {
				MainPane.showError("Please select vcf.gz file, not the index (.tbi)", "Error");
				return;
			}
			Loader.setLoading("Loading samples...");
			File[] addDir;
			final int sampletemp = MainPane.samples;
			Boolean added = false;
			Sample addSample = null;
			int fileindex = -1;
			FileRead.readFiles = true;
			cancelfileread = false;
			final ArrayList<File> bamdirs = new ArrayList<File>();

			if (Control.controlData.controlsOn) {
				Control.dismissControls(Getter.getInstance().getVariantHead());
			}
			int addnumber = 0;
			for (int fi = 0; fi < files.length; fi++) {

				if (MainPane.cancel || !Getter.getInstance().loading()) {
					VarMaster.getInstance().reset();
					return;
				}
				if (!files[fi].exists()) {
					continue;
				}

				addDir = null;

				if (files[fi].isDirectory()) {

					addDir = files[fi].listFiles(new FilenameFilter() {
						public boolean accept(final File dir, final String name) {
							return name.toLowerCase().endsWith(".vcf.gz") || name.toLowerCase().endsWith(".vcf");
						}
					});
					bamdirs.add(files[fi]);
					for (int f = 0; f < addDir.length; f++) {
						if (cancelfileread || !Getter.getInstance().loading()) {
							VarMaster.getInstance().reset();
							return;
						}
						if (!FileRead.checkIndex(addDir[f])) {
							MenuBar.putMessage("Check Tools->View log");
							ErrorLog.addError("No index file found for " + addDir[f].getName());
						}
						addSample = new Sample(addDir[f].getName(), (short) MainPane.samples, addDir[f].getCanonicalPath());
						Getter.getInstance().getSampleList().add(addSample);
						MainPane.varsamples++;
						MainPane.samples++;
						Draw.drawVariables.setVisibleSamples.accept(MainPane.samples);
						checkMulti(addSample);
						addnumber++;
						Loader.loadingtext = "Loading samples... " + addnumber;

						added = true;
						VariantHandler.commonSlider.setMaximum(MainPane.varsamples);
						VariantHandler.commonSlider.setUpperValue(MainPane.varsamples);
						VariantHandler.geneSlider.setMaximum(MainPane.varsamples);
						FileRead.sampleString.append(addSample.getName() + ";");
						fileindex = f;
					}

				} else {
					if (!files[fi].getName().endsWith(".vcf") && !files[fi].getName().endsWith(".vcf.gz") && !files[fi].getName().endsWith(".bedg.gz")) {
						continue;
					}

					if (!bamdirs.contains(files[fi].getParentFile())) {
						bamdirs.add(files[fi].getParentFile());
					}
					if (files[fi].getName().endsWith(".gz")) {
						File testfile = null;
						boolean testing = false;
						if (!FileRead.checkIndex(files[fi])) {
							MenuBar.putMessage("Check Tools->View log");
							ErrorLog.addError("No index file found for " + files[fi].getName());
							if (JOptionPane.showConfirmDialog(MainPane.drawScroll,
									"No index file found. Do you want to create one?", "Indexing?",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {

								Loader.loadingtext = "Creating index for " + files[fi].getName();
								if (files[fi].getName().endsWith(".vcf.gz")) {
									testing = true;
									testfile = MethodLibrary.createVCFIndex(files[fi]);
								} else {
									MethodLibrary.createVCFIndex2(files[fi]);
								}
							} else {
								continue;
							}
						}
						if (testing && testfile != null) {
							files[fi] = testfile;
						}
						if (fileindex > -1) {
							if (!files[fi].getParent().equals(files[fileindex].getParent())) {
								// diffPaths = true;
							}
						}
						fileindex = fi;
					}
					addSample = new Sample(files[fi].getName(), MainPane.samples, files[fi].getCanonicalPath());
					if (files[fi].getName().endsWith(".bedg.gz")) addSample.katri = true;
					Getter.getInstance().getSampleList().add(addSample);
					MainPane.varsamples++;
					MainPane.samples++;
					Draw.drawVariables.setVisibleSamples.accept(MainPane.samples);
					FileRead.sampleString.append(addSample.getName() + ";");
					checkMulti(addSample);
					addnumber++;
					Loader.loadingtext = "Loading samples... " + addnumber;
					added = true;
					VariantHandler.commonSlider.setMaximum(MainPane.varsamples);
					VariantHandler.commonSlider.setUpperValue(MainPane.varsamples);
					VariantHandler.geneSlider.setMaximum(MainPane.varsamples);

				}
			}
			if (!added) {
				Loader.ready("Loading samples...");
				return;
			}
			if (bamdirs.size() > 0) {

				final SearchBamFiles search = new SearchBamFiles(files, bamdirs, sampletemp);
				search.execute();
			}

			Draw.drawVariables.setVisibleSamples.accept(Getter.getInstance().getSampleList().size());			
			MainPane.drawCanvas.resizeCanvas(MainPane.drawScroll.getViewport().getWidth(),MainPane.drawScroll.getViewport().getHeight());

			final int loading = 0;
			if (!(VariantHandler.hideIndels.isSelected() && VariantHandler.hideSNVs.isSelected())) {
				Loader.loadingtext = "Loading variants...";
				for (int i = sampletemp; i < Getter.getInstance().getSampleList().size(); i++) {
					linecounter = 0;
					if (cancelfileread || !Getter.getInstance().loading()) {
						cancelFileRead();
						break;
					}
					
					MainPane.drawCanvas.loadbarAll = (int) ((loading	/ (double) (Getter.getInstance().getSampleList().size() - sampletemp)) * 100);
					
					try {

						if (Getter.getInstance().getSampleList().get(i).getTabixFile() == null) {
							continue;
						}
						if (Getter.getInstance().getSampleList().get(i).katri && end-start > Settings.settings.get("coverageDrawDistance")) break;
							
						if (start > 10000 && end < Draw.getSplits().get(0).chromEnd - 10000) {

							if (Draw.drawVariables.variantsEnd > 0) {
								FileRead.vcfReader.getVariants(MenuBar.chromosomeDropdown.getSelectedItem().toString(),	Draw.drawVariables.variantsStart, Draw.drawVariables.variantsEnd,	Getter.getInstance().getSampleList().get(i));
							} else {
								Draw.drawVariables.variantsStart = start;
								Draw.drawVariables.variantsEnd = end;			
								FileRead.vcfReader.getVariants(MenuBar.chromosomeDropdown.getSelectedItem().toString(), Draw.drawVariables.variantsStart, Draw.drawVariables.variantsEnd,	Getter.getInstance().getSampleList().get(i));
							}
						} else {
							Draw.drawVariables.variantsStart = 0;
							Draw.drawVariables.variantsEnd = Draw.getSplits().get(0).chromEnd;
							FileRead.vcfReader.getVariants(MenuBar.chromosomeDropdown.getSelectedItem().toString(),	Draw.drawVariables.variantsStart, Draw.drawVariables.variantsEnd,	Getter.getInstance().getSampleList().get(i));

						}
					} catch (final Exception e) {
						MainPane.showError(e.getMessage(), "Error");
						ErrorLog.addError(e.getStackTrace());
						e.printStackTrace();

					}
				}

			}
			// MainPane.opensamples.setText("Add samples");
			FileRead.checkSamples();
			FileRead.annotate();

			FileRead.readFiles = false;
			// MainPane.drawCanvas.clusterCalc = true;
			if (Control.controlData.controlsOn) {
				Control.applyControl();
			}

			MainPane.bedCanvas.intersected = false;
			if (FileRead.bigcalc) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead());
			} else {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}

			if (MainPane.bedCanvas.bedOn) {
				Loader.loadingtext = "Annotating variants";
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					if (MainPane.bedCanvas.bedTrack.get(i).intersect && !MainPane.bedCanvas.bedTrack.get(i).loading) {
						if (MainPane.bedCanvas.bedTrack.get(i).small) {
							MainPane.bedCanvas.annotate(MainPane.bedCanvas.bedTrack.get(i).getHead(), Getter.getInstance().getVariantHead().getNext());

						} else {
							final BedCanvas.Annotator annotator = MainPane.bedCanvas.new Annotator(
									MainPane.bedCanvas.bedTrack.get(i));
							annotator.annotateVars();

						}
					} else if (MainPane.bedCanvas.bedTrack.get(i).intersect && MainPane.bedCanvas.bedTrack.get(i).loading) {
						MainPane.bedCanvas.bedTrack.get(i).waiting = true;
					}
				}
				MainPane.bedCanvas.intersected = true;
			}

			if (FileRead.bigcalc) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead());
			} else {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
			if (Draw.getSplits().get(0).viewLength < 2000000) {

				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			}			
			
		VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());
			Loader.ready("Loading samples...");
			MainPane.drawCanvas.loadbarAll = 0;
			MainPane.drawCanvas.loadBarSample = 0;

		} catch (final Exception e) {
			e.printStackTrace();
			ErrorLog.addError(e.getStackTrace());
		}
	}
	public static void cancelFileRead() {
		FileRead.changing = false;
		FileRead.search = false;
		FileRead.bigcalc = false;
		// MainPane.opensamples.setText("Add samples");
		Getter.getInstance().getVariantHead().putNext(null);
		//current = null;
		VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());
		Draw.drawVariables.variantsStart = 0;
		Draw.drawVariables.variantsEnd = 1;
		
		MainPane.drawCanvas.repaint();

		try {
			if (FileWrite.output != null) {
			FileWrite.output.close();
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}
	public static void checkMulti(final Sample sample) {
		try {
			BufferedReader reader = null;
			GZIPInputStream gzip = null;
			FileReader freader = null;
			String line;
			Boolean somatic = Draw.drawVariables.somatic;
			String[] split, headersplit;
			
			if (somatic != null && somatic) asked = true;
			
			if (sample.getTabixFile().endsWith(".gz")) {
				try {
					gzip = new GZIPInputStream(new FileInputStream(sample.getTabixFile()));
					reader = new BufferedReader(new InputStreamReader(gzip));
				} catch (final Exception e) {
					MainPane.showError("Could not read the file: " + sample.getTabixFile()	+ "\nCheck that you have permission to read the file or try to bgzip and recreate the index file.",	"Error");
					Getter.getInstance().getSampleList().remove(sample);
					MainPane.varsamples--;
					MainPane.samples--;
				}
			} else {
				freader = new FileReader(sample.getTabixFile());
				reader = new BufferedReader(freader);
			}
			
			line = reader.readLine();
			
			if (!sample.multipart && line != null) {
				
				while (line != null) {
					try {

						if (line.startsWith("##INFO")) {
							if (line.contains("Type=Float") || line.contains("Type=Integer")	|| line.contains("Number=")) {
								VariantHandler.addMenuComponents(line);
							}
						}
						if (line.startsWith("##FILTER")) {
							if (line.contains("ID=") || line.contains("Description=") || line.contains("Number=")) {
								VariantHandler.addMenuComponents(line);
							}
						}
						if (line.startsWith("##FORMAT")) {
							if (line.contains("Type=Float") || line.contains("Type=Integer")	|| line.contains("Number=")) {
								VariantHandler.addMenuComponents(line);
							}
						}
						
						if (line.toLowerCase().startsWith("#chrom") || (sample.katri && line.toLowerCase().startsWith("chrom"))) {
							headersplit = line.split("\t+");
							if (!sample.katri && headersplit.length == 10) {
								sampleHash.put(headersplit[9], sample);
								break;
							}

							if (sample.katri || headersplit.length > 10) {
								if (!sample.katri && headersplit.length == 11 && !asked) {
									if (JOptionPane.showConfirmDialog(MainPane.drawScroll, "Is this somatic project?", "Somatic?", JOptionPane.YES_NO_OPTION,	JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
										somatic = true;
										Draw.drawVariables.somatic = true;
										asked = true;
									}
								}
								int startcol = sample.katri ? 3 : 9;
								if (!somatic) {
									sample.multiVCF = true;
									MainPane.varsamples--;

									for (int h = startcol; h < headersplit.length; h++) {
										String sampleName = headersplit[h];
										Sample addSample = new Sample(sampleName, MainPane.samples, null);
										sampleHash.put(sampleName, addSample);
										addSample.multipart = true;
										addSample.multiVCFparent = sample;
										addSample.katri = sample.katri;
										Getter.getInstance().getSampleList().add(addSample);
										MainPane.samples++;
										MainPane.varsamples++;
										if (FileRead.sampleString == null) FileRead.sampleString = new StringBuffer("");
										FileRead.sampleString.append(addSample.getName() + ";");
									}
									VariantHandler.commonSlider.setMaximum(MainPane.varsamples);
									VariantHandler.commonSlider.setUpperValue(MainPane.varsamples);
									VariantHandler.geneSlider.setMaximum(MainPane.varsamples);
									Draw.drawVariables.setVisibleSamples.accept(Getter.getInstance().getSampleList().size());									
									MainPane.drawCanvas.resizeCanvas(MainPane.drawScroll.getViewport().getWidth(),MainPane.drawScroll.getViewport().getHeight());
								}
							}
							line = reader.readLine();
							break;
						}
						split = line.split("\t");
						if (split.length > 1 && split[1].matches("\\d+")) break;

					} catch (final Exception ex) {
						ex.printStackTrace();
					}
					line = reader.readLine();
				}
				
				VariantHandler.menuScroll.setPreferredSize(new Dimension(250, 500));
			
				if (line == null) return;
				
				while (line != null && line.startsWith("#")) line = reader.readLine();
				
				if (line == null) return;
				split = line.split("\t");
				
				if (split != null && split.length == 8) sample.annoTrack = true;
				if (line.startsWith("chr")) sample.vcfchr = "chr";
				
				if (split.length > 7 && split[7].contains("SVTYPE")) {
					sample.SV = true; sample.setMaxCoverage(100F);					
				}
				
				if (somatic != null && somatic) {
					headersplit = line.split("\t");
					if (headersplit.length > 10) {
						if (headersplit[10].startsWith("0:") || (headersplit[10].charAt(0) == '0' && headersplit[10].charAt(2) == '0')) {
							sample.somaticColumn = 9;
						} else {
							sample.somaticColumn = 10;
						}
						sampleHash.put(headersplit[sample.somaticColumn], sample);
					}					
				}
				// Reading whole SV file for translocation breakpoints
				if (sample.SV && !split[2].contains("Manta")) {
					
					sample.breakpoints = new ArrayList<String[]>();		
					
					while(line != null) {							
						split = line.trim().split("\t");
						String SVtype = MethodLibrary.getValueFromInfo("SVTYPE", split[7], false);
						if (SVtype.equals("BND") || SVtype.equals("TRA")) sample.breakpoints.add(split);
						line = reader.readLine();				
					}
				}
				FileRead.checkSamples();
				line = null;
				if (freader != null) freader.close();
				reader.close();
				if (gzip != null) gzip.close();
			} else {
				reader.close();
				if (gzip != null) gzip.close();
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}

