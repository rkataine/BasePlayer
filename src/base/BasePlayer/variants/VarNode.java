/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.variants;

import java.util.ArrayList;
//import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.awt.geom.QuadCurve2D;
import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.genome.Transcript;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.sample.SampleNode;
import base.BasePlayer.tracks.BedNode;

import java.awt.Color;
import java.awt.Graphics2D;

public class VarNode {
	private final String chr;
	private final int position;
	Integer length = 0;	
	//static MethodLibrary.varSorter varsorter = new MethodLibrary.varSorter();
	private VarNode prev, next, pair;
	private final byte refBase;
	private ArrayList<Transcript.Exon> exons;
	private ArrayList<Transcript> transcripts;
	private boolean inGene;
	public boolean inVarList, bedhit, primary = true;
	public Integer clusterId;
	private ArrayList<BedNode> bedHits;
	public String rscode;
	public ClusterNode clusterNode;
	public Color varColor = BaseConstants.interColor;
	public ConcurrentHashMap<String, ArrayList<SampleNode>> vars = new ConcurrentHashMap<String, ArrayList<SampleNode>>();
	public boolean incluster, indel, found;
	public short phase = -1;
	public String codon;
	public boolean controlled, annotationOnly = false, coding = false;
	
	public VarNode(final String chr, final int position, final byte ref, final String rscode, final VarNode prev, final VarNode next) {
		this.chr = chr;
		this.position = position;
		this.rscode = rscode;
		this.refBase = ref;
		this.prev = prev;
		this.next = next;
	}
	/* public VarNode clone() throws CloneNotSupportedException { return (VarNode) super.clone(); } */
	public void setTranscripts() { this.transcripts = new ArrayList<Transcript>(); }
	public void setExons() { this.exons = new ArrayList<Transcript.Exon>();	}
	public void setBedhits() { this.bedHits = new ArrayList<BedNode>();	}
	public void remBedhits() { this.bedHits = null;	}
	public void putNext(final VarNode node) {	this.next = node;	}
	public void putPrev(final VarNode node) {	this.prev = node;	}
	public byte getRefBase() { return this.refBase;	}
	public String getChrom() { return this.chr;	}
	public int getPosition() { return this.position; }
	public VarNode getPrev() { return this.prev; }
	public VarNode getNext() { return this.next; }
	public boolean isInGene() {	return this.inGene; }
	public void setInGene() {	this.inGene = true; }
	public void setBedhit(final boolean value) { this.bedhit = value;	}
	public boolean isBedhit() {	return this.bedhit;	}
	public ArrayList<BedNode> getBedHits() { return this.bedHits;	}
	public void setCodon(final String codon) {	this.codon = BaseConstants.codons.get(codon); }
	public void setRscode(final String value) {	this.rscode = value; }
	public String isRscode() { return this.rscode; }
	public String getCodon() { return this.codon; }
	public java.util.List<Transcript.Exon> getExons() {	return this.exons; }
	public java.util.List<Transcript> getTranscripts() { return this.transcripts; }
	public List<SampleNode> getSamples(final String variant) { return vars.get(variant); }
	public VarNode getPair() { return this.pair; }
	public void setPair(VarNode node) { this.pair = node; }
	public void setAsPairs(VarNode other) { this.pair = other; other.setPair(this); }

	public VarNode getNextVisible() {
		VarNode node = this.getNext();
		if (node == null) return null; 
		while (node.hideNodeTotal(node)) {
			node = node.next;
			if (node == null) return null;
		}
		return node;
	}

	private VarNode getNextVisibleCluster(VarNode node) {
		if (node == null) return null;
		while (node.hideNodeCluster(node)) 
			if (node != null) node = node.next;		
		return node;
	}

	public VarNode getNext(final VarNode node) {
		if (this.next.hideNodeCluster(this.next)) {
			if (this.next != null) return getNextVisibleCluster(this.next.next);
			else return null;			
		} else return this.next;
	}

	public List<SampleNode> getAllSamples() {
		final List<SampleNode> samples = new ArrayList<SampleNode>();
		for (final ArrayList<SampleNode> entry : vars.values()) samples.addAll(entry);		
		return samples;
	}

	public void addSample(String variant, SampleNode sampleNode) {
		if (variant.length() > 1) indel = true;
		if (!vars.containsKey(variant)) vars.put(variant, new ArrayList<SampleNode>());
		if (!vars.get(variant).contains(sampleNode)) vars.get(variant).add(sampleNode);
	}

	public void removeNode() {
		if (this.getPrev() != null) this.getPrev().putNext(this.getNext());		
		if (this.getNext() != null) this.getNext().putPrev(this.getPrev());
		this.putNext(null);	this.putPrev(null);
	}
	public VarNode remove() {
		VarNode next = this.getNext();
		if (this.getPrev() != null) this.getPrev().putNext(this.getNext());		
		if (this.getNext() != null) this.getNext().putPrev(this.getPrev());
		this.putNext(null);	this.putPrev(null);
		return next;
	}
	public void removeSample(final Sample sample) {
		try {
			for (final Entry<String, ArrayList<SampleNode>> entry : this.vars.entrySet()) {
				for (int i = entry.getValue().size() - 1; i >= 0; i--) {
					if (entry.getValue().get(i).getSample() == null) continue;					
					if (entry.getValue().get(i).getSample().equals(sample)) {
						entry.getValue().remove(i);
						if (i < 1) break;
						if (entry.getValue().get(i - 1).getSample().equals(sample)) {
							int pointer = i - 1;
							while (entry.getValue().get(pointer).getSample().equals(sample)) {
								entry.getValue().remove(pointer);
								pointer--;
								i = pointer;
								if (pointer < 0) break;									
							}
						}						
						if (entry.getValue().size() == 0) vars.remove(entry.getKey());
					}
				}
			}
			if (vars.size() == 0) {
				if (this.getNext() != null) this.getNext().putPrev(this.getPrev());				
				this.getPrev().putNext(this.getNext());
			}
		} catch (final Exception e) {	e.printStackTrace(); }
	}

	public void draw(Graphics2D buf) {
		buf.setColor(this.varColor);		
		int varpos = (int)((this.getPosition() + 1 - Draw.drawVariables.start)*Draw.drawVariables.pixel) + MainPane.sidebarWidth;
		Integer length = null;
		
		if (this.length != null) length = (int)(this.length * Draw.drawVariables.pixel); 
	
		for (final Entry<String, ArrayList<SampleNode>> entry : this.vars.entrySet()) {
			
			if (this.hideNodeVar(entry)) continue;			
			
			for (int i = 0; i < entry.getValue().size(); i++) {
				
				try {					
					if (entry.getValue().get(i).allelenumber != null) break;
					if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) continue;			
					Sample sample = entry.getValue().get(i).getSample();
					
					boolean sv = sample.SV || (sample.multipart && sample.multiVCFparent.SV);
					final SampleNode samplenode = entry.getValue().get(i);
					variantCalculation(samplenode, entry);
					
					if (Draw.getSplits().get(0).viewLength > Settings.readDrawDistance) {
						if (varpos == sample.prepixel && !this.coding && !sv) continue;
					}
					
					if (sample.getIndex() > Draw.drawVariables.getVisibleStart.get() + Draw.drawVariables.getVisibleSamples.get() + 1) break;
					if (sample.getIndex() < Draw.drawVariables.getVisibleStart.get()) continue;
					if (sample.katri) {
						drawKatri(samplenode, entry.getKey(), varpos, length, buf);
						continue;
					}
					
					if (sample.multiVCFparent != null) 
						drawVarLine(sample.multiVCFparent, samplenode, entry.getKey(), varpos, sv, buf);
						
					drawVarLine(sample, samplenode, entry.getKey(), varpos, sv, buf);
				} catch (final Exception r) {
					ErrorLog.addError(r.getStackTrace());
					r.printStackTrace();
					break;
				}				
			}			
		}		
	}
	/* void drawBAFs(Sample sample, SampleNode sampleNode, int varpos){
		//int samplePos = (int) ((sample.getIndex()) * Draw.drawVariables.sampleHeight)	- Draw.scrollValue;
		double BAF = sampleNode.getAlleleFraction();
		buf.fillOval(varpos, (int)(Draw.drawVariables.sampleHeight * BAF), 4, 4);
	} */
	void drawVarLine(Sample sample, SampleNode sampleNode, String variant, int varpos, boolean sv, Graphics2D buf) {
		
		int samplePos = (int) ((sample.getIndex()) * Draw.drawVariables.sampleHeight)- Draw.scrollValue;
		
		if (sv && Draw.getSplits().size() == 1) {
			
			String svtype = this.rscode.substring(0,3);
			buf.setColor(BaseConstants.SV_COLORS_LINE.get(svtype));
					
			if (this.primary && !sample.multiVCF) {
				
				if (this.pair != null && !svtype.equals("TRA")) {
					
					if (this.length < 1) return;
					int varpos2 = (int)((this.pair.getPosition() + 1 - Draw.drawVariables.start)*Draw.drawVariables.pixel) + MainPane.sidebarWidth;
					if (Draw.drawVariables.pixel > 0.00001) {
						
						int middle = (varpos2-varpos)/2;

						int curveposRandom = (int)((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight- Draw.drawVariables.sampleHeight/2) - Draw.scrollValue;
						//(int)(((this.position+Math.abs(this.length)) / (double)Draw.getSplits().get(0).chromEnd) * MainPane.drawCanvas.getHeight());
						
						buf.setStroke(BaseConstants.doubledashed);
						QuadCurve2D curve = new QuadCurve2D.Double(varpos,curveposRandom,middle+varpos,curveposRandom+(this.length/Math.abs(this.length) * 50),varpos2,curveposRandom);
						buf.draw(curve);
						buf.setStroke(BaseConstants.basicStroke);
					}
					
					buf.drawLine(varpos2, (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight - Draw.drawVariables.varMinHeight) - sampleNode.heightValue * Draw.drawVariables.heightFrac / sample.getMaxCoverage()) - Draw.scrollValue,	varpos2, (int)(samplePos + Draw.drawVariables.sampleHeight));					
		
					//int length = (int)((this.pair.getPosition() - this.getPosition()) * Draw.getSplits().get(0).pixel);
					//buf.fillRect(varpos, samplePos, length, (int)Draw.drawVariables.sampleHeight);
				}
			}						
		}
		
		if(Settings.allelicFraction) {
			buf.drawString(".", varpos, samplePos+(int)((1-sampleNode.getAlleleFraction())*Draw.drawVariables.sampleHeight));
			return;
		}
		if (Draw.getSplits().get(0).viewLength > Settings.readDrawDistance) {
		
			if (varpos == sample.prepixel && !this.coding && !sv) return;
			if (sample.getMaxCoverage() < VariantHandler.maxSlideValue)				
				buf.drawLine(varpos, (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight - Draw.drawVariables.varMinHeight) - sampleNode.heightValue * Draw.drawVariables.heightFrac / sample.getMaxCoverage()) - Draw.scrollValue,	varpos, (int)(samplePos + Draw.drawVariables.sampleHeight));					
			else 
				buf.drawLine(varpos, (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight - Draw.drawVariables.varMinHeight) - sampleNode.heightValue * (Draw.drawVariables.heightFrac / VariantHandler.maxSlideValue)) - Draw.scrollValue, varpos, (int)(samplePos + Draw.drawVariables.sampleHeight));
		} else {
			if (variant.length() > 1 && !sv) varpos = (int) ((this.getPosition() + 2 - Draw.getSplits().get(0).start) * Draw.drawVariables.pixel) + MainPane.sidebarWidth;
			
			if (MainPane.drawCanvas.moveX <= varpos + (int)Draw.drawVariables.pixel + 1 && MainPane.drawCanvas.moveX >= varpos) {
				if (MainPane.drawCanvas.moveY >= (int) ((sample.getIndex()) * Draw.drawVariables.sampleHeight) && MainPane.drawCanvas.moveY <= (int) (sample.getIndex() * Draw.drawVariables.sampleHeight) + (int)Draw.drawVariables.sampleHeight) {
					MainPane.drawCanvas.sampleOverLap = sampleNode;
					MainPane.drawCanvas.baseHover = variant;
					MainPane.drawCanvas.varOverLap = this;
					buf.setColor(Color.white);
				}
			}
			if (varpos == sample.prepixel) sample.samePosCount++;
			else sample.samePosCount = 1;	
			
			sample.samePosCount = 1;	
			if (sample.samePosCount > 1) buf.fillRect(varpos, samplePos+(int)Draw.drawVariables.sampleHeight/sample.samePosCount, (int) Draw.drawVariables.pixel + 1, (int)Draw.drawVariables.sampleHeight/sample.samePosCount);
			else buf.fillRect(varpos, samplePos, (int) Draw.drawVariables.pixel + 1, (int)Draw.drawVariables.sampleHeight);

			if (Draw.getSplits().get(0).viewLength < 200) {
				buf.setColor(Color.white);
				buf.setFont(BaseVariables.seqFont);
				
				if (varpos == sample.prepixel) {
					
					buf.drawLine(varpos, (int) ((sample.getIndex() * Draw.drawVariables.sampleHeight) + Draw.drawVariables.sampleHeight / sample.samePosCount) - Draw.scrollValue, (int) (varpos + Draw.drawVariables.pixel + 1), (int) (((sample.getIndex()) * Draw.drawVariables.sampleHeight) + Draw.drawVariables.sampleHeight / sample.samePosCount) - Draw.scrollValue);
					buf.drawString(variant, varpos, (int) (((sample.getIndex()) * Draw.drawVariables.sampleHeight + BaseVariables.defaultFontSize + 2) + Draw.drawVariables.sampleHeight / sample.samePosCount) - Draw.scrollValue);
				} else {
					buf.drawString(variant, varpos, (int) (((sample.getIndex()) * Draw.drawVariables.sampleHeight + BaseVariables.defaultFontSize + 2))	- Draw.scrollValue);
				}
				buf.setFont(BaseVariables.defaultFont);
				buf.setColor(this.varColor);
			}

			//if (variant.length() > 1 && !sv) varpos = (int) ((this.getPosition() + 1 - Draw.getSplits().get(0).start) * Draw.drawVariables.pixel) + MainPane.sidebarWidth;
		}
		
		sample.prepixel = varpos;
	}

	void drawKatri(SampleNode sampleNode, String variant, int varpos, int length, Graphics2D buf) {
		if (varpos == sampleNode.getSample().prepixel) return;
		int samplePos = (int) ((sampleNode.getSample().getIndex()) * Draw.drawVariables.sampleHeight) - Draw.scrollValue;
		
		buf.setColor(BaseConstants.KATRI_COLORS.get(variant));	
		if (length < 1) buf.drawLine(varpos, samplePos, varpos, samplePos+(int)Draw.drawVariables.sampleHeight);
		else buf.fillRect(varpos, samplePos, length+1, (int)Draw.drawVariables.sampleHeight);
		
		sampleNode.getSample().prepixel = varpos;
	}

	public boolean hideNode() {
		if (VariantHandler.hidenoncoding.isSelected() && !this.coding) return true;		
		if (VariantHandler.rscode.isSelected() && this.isRscode() != null) return true;

		if (!FileRead.readFiles && MainPane.projectValues.variantHandlerValues.commonVariantsMin >= 1) {
			if (VariantHandler.clusterSize > 0) {
				if (MainPane.projectValues.variantHandlerValues.commonVariantsMax< MainPane.varsamples) {
					if (this.incluster) return true;
				} else if (!this.incluster) return true;
			}
		}

		if (MainPane.drawCanvas.annotationOn)
			if (this.annotationOnly) return false;

		if (MainPane.bedCanvas.bedOn) 
			if (!this.bedhit) if (!FileRead.readFiles) return true;
		return false;
	}
	boolean hideNodeTotal(final VarNode node) {
		if (node.hideNode()) return true;		
		boolean foundvisible = false;

		if (!MainPane.drawCanvas.annotationOn) {
			for (final Map.Entry<String, ArrayList<SampleNode>> sampleNodes : node.vars.entrySet()) {			
				if (!hideNodeVar(sampleNodes)) {	
					for (int i = 0; i < sampleNodes.getValue().size(); i++) {
						SampleNode sampleNode = sampleNodes.getValue().get(i);
						if (!sampleNode.hideVar(sampleNodes.getKey().length() > 1)) {	foundvisible = true;	break;	}
					}				
					/* for (final SampleNode sampleNode : sampleNodes.getValue()) //TODO concurrency
						if (!sampleNode.hideVar(sampleNodes.getKey().length() > 1)) {	foundvisible = true;	break;	}		 */			
				}
				if (foundvisible) break;				
			}
		} else {		
			for (final Map.Entry<String, ArrayList<SampleNode>> sampleNodes: node.vars.entrySet()) {				
				if (!hideNodeVar(sampleNodes)) {
					for (final SampleNode sampleNode : sampleNodes.getValue()) {
						if (sampleNode.getSample() != null && sampleNode.getSample().annotation) continue;
						if (!sampleNode.hideVar(sampleNodes.getKey().length() > 1)) {	foundvisible = true;	break;	}
					}
				}
				if (foundvisible) break;				
			}
		}
		if (foundvisible) return false;		
		return true;

	}
	boolean hideNodeCluster(final VarNode node) {
		if (node == null) return true;
		if (VariantHandler.hidenoncoding.isSelected()) {
			if (!node.coding) return true;			
		}

		if (MainPane.bedCanvas.bedOn && MainPane.bedCanvas.intersected && !node.bedhit) {
			if (!FileRead.readFiles && !MainPane.bedCanvas.annotator) return true;			
		}

		if (VariantHandler.rscode.isSelected()) {
			if (node.isRscode() != null) return true;			
		}
		boolean foundvisible = false;

		for (final Map.Entry<String, ArrayList<SampleNode>> sampleNodes: node.vars.entrySet()) {
			if (!hideNodeVar(sampleNodes)) {
				for (final SampleNode sampleNode : sampleNodes.getValue()) {
					if (!sampleNode.hideVar(sampleNodes.getKey().length() > 1)) {	foundvisible = true;	break; }
				}
			}
			if (foundvisible) break;
		}
		if (!foundvisible) return true;
		return false;
	}
	public boolean hideNodeVar(final Entry<String, ArrayList<SampleNode>> entry) {	
		if (VariantHandler.hideIndels.isSelected()) {
			if (entry.getKey().length() > 1) return true;
		}
		if (VariantHandler.hideSNVs.isSelected()) {
			if (entry.getKey().length() == 1) return true;			
		}
		
		if (MainPane.drawCanvas.annotationOn) {
			if (this.annotationOnly) return false;			

			if (MainPane.drawCanvas.intersect) {
				boolean found = false;

				for (int i = entry.getValue().size() - 1; i >= 0; i--) {
					if (entry.getValue().get(i).alleles != null) continue;					
					if (entry.getValue().get(i).getSample().annotation	&& entry.getValue().get(i).getSample().intersect) {	found = true;	break;	}
				}
				if (!found) return true;				
			}
		}

		if (!FileRead.readFiles && (MainPane.projectValues.variantHandlerValues.commonVariantsMin >= 1 || MainPane.projectValues.variantHandlerValues.commonVariantsMax< MainPane.varsamples)) {
			if (VariantHandler.clusterSize == 0) {
				int hidecount = 0;

				for (int i = 0; i < entry.getValue().size(); i++) {
					if (entry.getValue().get(i).getSample() == null) continue;
					if (entry.getValue().get(i).getSample().annotation) continue;					
					if (!entry.getValue().get(i).hideVarCommon(entry.getKey().length() > 1)) hidecount++;
				}
			
				if (MainPane.drawCanvas.annotationOn) {
					if (hidecount >= MainPane.projectValues.variantHandlerValues.commonVariantsMin) {
						for (int i = 0; i < entry.getValue().size(); i++) {
							if (entry.getValue().get(i).getSample() == null) continue;							
							if (entry.getValue().get(i).getSample().annotation) continue;
							
							if (!entry.getValue().get(i).hideVarCommon(entry.getKey().length() > 1)) entry.getValue().get(i).common = true;
							else entry.getValue().get(i).common = false;							
						}
					} else {
						for (int i = 0; i < entry.getValue().size(); i++) {
							if (entry.getValue().get(i).getSample() == null) continue;							
							if (entry.getValue().get(i).getSample().annotation) continue;
							
							entry.getValue().get(i).common = false;
						}
					}
				} else {
					if (hidecount < MainPane.projectValues.variantHandlerValues.commonVariantsMin) return true;
					if (hidecount > VariantHandler.commonSlider.getUpperValue()) return true;
				}
			}
		}
		
		if (this.controlled) {
			for (int i = entry.getValue().size() - 1; i > 0; i--) {
				if (entry.getValue().get(i).alleles == null) break;				
				if (!entry.getValue().get(i).getControlSample().controlOn) continue;				
				if (entry.getValue().get(i).alleles / (double) entry.getValue().get(i).allelenumber > entry.getValue().get(i).getControlSample().alleleFreq) return true;		
				
			}			
		}
		
		return false;
	}

	void variantCalculation(SampleNode samplenode, final Entry<String, ArrayList<SampleNode>> entry) {
		if (Draw.drawVariables.sampleHeight > 30) {
			samplenode.getSample().varcount++;
			if (entry.getKey().length() > 1) {
				samplenode.getSample().indels++;
				
			}
			if (Draw.drawVariables.sampleHeight > 45) {
				if (samplenode.isHomozygous()) samplenode.getSample().homozygotes++;
				else samplenode.getSample().heterozygotes++;
				
				if (Draw.drawVariables.sampleHeight > 75) {
					if (!this.indel || entry.getKey().length() < 2) {
						if (((char) this.getRefBase() == ('A') && entry.getKey().equals("G"))	|| ((char) this.getRefBase() == 'G'	&& entry.getKey().equals("A")) || ((char) this.getRefBase() == 'C'	&& entry.getKey().equals("T")) || ((char) this.getRefBase() == 'T'	&& entry.getKey().equals("C"))) 
							samplenode.getSample().sitioRate++;
						else samplenode.getSample().versioRate++;						
					}
				}
			}
		}
	}	
}
