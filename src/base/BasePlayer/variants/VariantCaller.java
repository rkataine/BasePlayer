package base.BasePlayer.variants;

/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.Main;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.BedCanvas;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.Control;
import base.BasePlayer.genome.ReferenceSeq;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.reads.Reads;
import base.BasePlayer.sample.Sample;
import htsjdk.samtools.CigarElement;
import htsjdk.samtools.CigarOperator;
import htsjdk.samtools.SAMRecord;

public class VariantCaller extends JPanel implements ActionListener {	
	private static final long serialVersionUID = 1L;
	static Boolean loading = false;
	public static JFrame frame = new JFrame("Variant Caller");
	JLabel testing = new JLabel("Test version");
	static JTextField minreads = new JTextField("4");
	JLabel minreadlabel = new JLabel("Min. alt-read count");
	static JTextField mincoverage = new JTextField("10");
	JLabel mincoveragelabel = new JLabel("Min. coverage");
	static JTextField minbaseq = new JTextField("5");
	JLabel minbaseqlabel = new JLabel("Min. base quality");
	static JTextField minmappingq = new JTextField("10");
	JLabel minmappingqlabel = new JLabel("Min. mapping quality");
	static JCheckBox bothstrand = new JCheckBox("Require both strands");
	static JCheckBox bothruns = new JCheckBox("Require multiple runs");
	static JCheckBox methyl = new JCheckBox("Call from bisulfite data");
	JCheckBox onlySel = new JCheckBox("Calc only for selected");
	public static JCheckBox inanno = new JCheckBox("Before annotation");
	JButton execute = new JButton("Execute");
	public VariantCaller(boolean caller) {}
	public VariantCaller() {
		super(new GridLayout(8,2));		
		try {
			testing.setForeground(Color.red);
			execute.addActionListener(this);
			inanno.addActionListener(this);
			add(testing);
			add(new JLabel());
			add(minreads);
			add(minreadlabel);
			add(mincoverage);
			add(mincoveragelabel);
			add(minmappingq);
			add(minmappingqlabel);
			add(minbaseq);
			add(minbaseqlabel);
			add(bothstrand);
			add(bothruns);
			add(methyl);
			add(onlySel);
			add(inanno);			
			add(execute);			
			bothstrand.setToolTipText("Variant call is discared if mismatches are present only in the other strand.");	
		}
		catch(Exception e) { e.printStackTrace(); }
	}
	
	private static void createAndShowGUI() {		
			if(Main.frame != null) {
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
				frame.setVisible(false);		
			}
			else {
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
				frame.setVisible(true);		
			}
			frame.setAlwaysOnTop(true);
		 	
			JComponent newContentPane = new VariantCaller();
			newContentPane.setOpaque(false);		   
			frame.setContentPane(newContentPane);
			setFonts(MainPane.menuFont);
			frame.pack();
			
			for(int i =0; i<frame.getContentPane().getComponentCount(); i++)
				frame.getContentPane().getComponent(i).setMinimumSize(frame.getContentPane().getComponent(i).getPreferredSize());
			
			minreads.setMinimumSize(new Dimension(minreads.getWidth(),minreads.getHeight()));	
			mincoverage.setMinimumSize(new Dimension(mincoverage.getWidth(),mincoverage.getHeight()));
			minbaseq.setMinimumSize(new Dimension(minbaseq.getWidth(),minbaseq.getHeight()));
			minmappingq.setMinimumSize(new Dimension(minmappingq.getWidth(),minmappingq.getHeight()));			
	}
	public static void main(String[] args) {		
		 javax.swing.SwingUtilities.invokeLater(new Runnable() { public void run() {	createAndShowGUI(); }  });	
	}
	
	public static void setFonts(Font menuFont) {
		for(int i = 0 ; i<frame.getContentPane().getComponentCount(); i++)
			frame.getContentPane().getComponent(i).setFont(menuFont);		
		frame.pack();
	}
	@Override
	public void actionPerformed(ActionEvent event) {		
		if(event.getSource() == execute) {
			try {				
				VarCaller caller = new VarCaller();
				caller.execute();												
			}
			catch(Exception e) {	e.printStackTrace(); }
		}	else if(event.getSource() == inanno) {
			if(inanno.isSelected()) {				
				VarCalculations.caller = true;
				FileRead.checkSamples();
			}	else {
				VarCalculations.caller = false;
				FileRead.checkSamples();
			}
		}
	}

	public class VarCaller extends SwingWorker<String, Object> {
		int readlevel,minquality, minreadquality, mincoverage; 
		boolean onlysel;
		public VarCaller(int readlevel, int minquality, int minreadquality, int mincoverage, boolean onlysel) {
			this.readlevel = readlevel;	this.minquality = minquality;	this.minreadquality = minreadquality;	this.mincoverage = mincoverage;	this.onlysel = onlysel;
		}
		public VarCaller() {
			int readlevel,minquality, minreadquality, mincoverage; 
			try {	readlevel = Integer.parseInt(minreads.getText());	}
			catch(Exception e) {	minreads.setForeground(Color.red);	return;	}
		
			try {	minquality = Integer.parseInt(minbaseq.getText()); }
			catch(Exception e) {	minbaseq.setForeground(Color.red);	return;	}

			try {	minreadquality = Integer.parseInt(minmappingq.getText());	}
			catch(Exception e) {	minmappingq.setForeground(Color.red);	return;	}

			try {	mincoverage = Integer.parseInt(VariantCaller.mincoverage.getText());	}
			catch(Exception e) {	VariantCaller.mincoverage.setForeground(Color.red);	return;	}

			this.readlevel = readlevel;	this.minquality = minquality;	this.minreadquality = minreadquality;	this.mincoverage = mincoverage;	this.onlysel = onlySel.isSelected();
		}
	
		void callVariants() {			
			if(onlysel && (Sample.selectedSample == null || Sample.selectedSample.getTabixFile() != null || Sample.selectedSample.samFile == null)) {
				loading = false; return;	}
		
			int startpos=(int)Draw.getSplits().get(0).start, endpos=(int)Draw.getSplits().get(0).end;
			Getter.getInstance().getVariantHead().putNext(null);
			loading = true;
			
			if(!Getter.getInstance().loading()) Loader.setLoading("Calling variants");			
			else Loader.loadingtext = "Calling variants";			
			MainPane.bedCanvas.bedOn = false;

			for(int s = 0; s<MainPane.samples;s++) {				
				try {	
					if(!Getter.getInstance().loading()) break;			
					
					MainPane.drawCanvas.loadBarSample = 0;  
					MainPane.drawCanvas.loadbarAll  = s/MainPane.samples*100;
					Sample sample = base.BasePlayer.Getter.getInstance().getSampleList().get(s);
					Reads readClass = sample.getreadHash().get(Draw.getSplits().get(0));
					sample.basequalsum = 0;	sample.basequals = 0;
					if(onlysel && !sample.equals(Sample.selectedSample)) continue;					
					if(sample.getTabixFile() != null || sample.samFile == null) continue;					
					
					VarNode node = Getter.getInstance().getVariantHead().getNext();						
					
					while(node != null) {	node.removeSample(sample);	node = node.getNext(); }					
					Reads reads = null;

					try {	reads = (Reads)readClass.clone();	}
					catch(Exception e) {	sample.resetreadHash();	reads = (Reads)readClass.clone();	e.printStackTrace();	}
					
					if(!sample.calledvariants) {
						MainPane.varsamples++;
						VariantHandler.commonSlider.setMaximum(MainPane.varsamples);	VariantHandler.commonSlider.setUpperValue(MainPane.varsamples);	VariantHandler.geneSlider.setMaximum(MainPane.varsamples);
					}
					sample.maxCoveragecaller = 0;
					sample.calledvariants = true;
					String[] coveragebases = { "", "A", "C", "G", "T", "N"}, line = new String[10];
					double homlevel = 0.95;						
					boolean foundscan = false;					
					int interval;					
					StringBuffer genotypes = new StringBuffer("");
					int calls = 0, refs = 0;								
					String refbase = "";
					VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());
					if(endpos-startpos < Settings.windowSize) interval = endpos-startpos;					
					else interval = Settings.windowSize;					
					
					MainPane.drawCanvas.repaint();					
					line[2] = "."; line[6] = "PASS"; line[7] = "INFO";	line[8] = "GT:AD:DP";			
					int start = startpos, end = startpos + interval;								
					Draw.drawVariables.variantsStart = (int)start;
					int scanpointer = 0;
					
					while(start < endpos) {						
						if(!Getter.getInstance().loading()) {	Loader.ready("all");	break; }						
						if(end > endpos) end = endpos;						
						Draw.drawVariables.variantsEnd = end;
						ReferenceSeq reference = new ReferenceSeq(Draw.getSplits().get(0).chrom, start-300, end+300, MainPane.referenceFile);
						VariantCall[][] coverages = variantCaller(Draw.getSplits().get(0).chrom, start,end, reads, minquality,minreadquality, reference);
						
						if(coverages != null) {				
							for(int i = 0 ; i<coverages.length; i++) {		
								if(!Getter.getInstance().loading()) break;							
								if(coverages[i][0] == null) continue;							
								if(coverages[i][0].calls > sample.maxCoveragecaller) sample.maxCoveragecaller = coverages[i][0].calls;							
							
								for(int j = 1; j< coverages[i].length-2; j++) {
									if(coverages[i][j] == null) continue;												
									if(coverages[i][j].calls >= readlevel && coverages[i][0].calls >= mincoverage) {									
										if(bothstrand.isSelected() && coverages[i][j].strands.size() < 2) continue;									
										if(bothruns.isSelected() && readClass.getRuns() > 1 && coverages[i][j].runs.size() < 2) continue;
										
										if (!methyl.isSelected()) {
											foundscan = false;
											scanpointer = i-1;
											while(scanpointer >= 0 && i-scanpointer < 100) {
												for(int c = 1; c < coverages[i].length-2; c++) {												
													if(coverages[scanpointer][c] != null) {	foundscan = true;	break;	}
												}
												if(foundscan) break;											
												scanpointer--;
											}
											if(foundscan) continue;
											
											scanpointer = i+1;
											while(scanpointer < coverages.length && scanpointer-i < 100) {
												for(int c = 1; c < coverages[i].length-2; c++) {
													if(coverages[scanpointer][c] != null) {	foundscan = true;	break;	}
												}
												if(foundscan) break;											
												scanpointer++;
											}
											if(foundscan) continue;										
										}
										genotypes = new StringBuffer("");
										calls = coverages[i][j].calls;
										refs = coverages[i][0].calls;
										if(calls/(double)refs >= homlevel) genotypes.append("1/1:");									
										else genotypes.append("0/1:");
										
										genotypes.append((refs-calls) +"," +calls +":" +refs);
										refbase = MainPane.getBase.get(reference.getSeq()[(reads.getCoverageStart() + i -1) -reference.getStartPos()]);								
										line[0] = Draw.getSplits().get(0).chrom;										
										line[1] = ""+(reads.getCoverageStart() + i);							
										line[3] = refbase;										
										line[4] = coveragebases[j];
										line[5] = ""+MethodLibrary.round((coverages[i][j].qualities/(double)coverages[i][j].calls),2);
										line[9] = genotypes.toString();								
									}					
								}
							}
						}
						start = end;	end += interval;				
						
						
						MainPane.drawCanvas.repaint();						
					}					
					reads = null;
					readClass = null;
					System.out.println(sample.getName() +": " +(sample.basequalsum/(double)sample.basequals));
				}
				catch(Exception e) {	e.printStackTrace();	break;	}				
			}			
			
			Draw.drawVariables.variantsEnd = endpos;
			VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead());
			FileRead.annotate();	
			if(Control.controlData.controlsOn) Control.applyControl();			
			VarMaster.calcClusters(Getter.getInstance().getVariantHead(),1);				
			MainPane.bedCanvas.bedOn = true;
			boolean ison = false;
			for(int i = 0 ; i<MainPane.bedCanvas.bedTrack.size();i++) {
				if(MainPane.bedCanvas.bedTrack.get(i).intersect) { ison = true;	break;	}
			}
			if(!ison) MainPane.bedCanvas.bedOn = false;			
		
			if(MainPane.bedCanvas.bedOn) {			
				Loader.loadingtext = "Annotating variants";
				for(int i = 0 ; i<MainPane.bedCanvas.bedTrack.size(); i++) {
					if(MainPane.bedCanvas.bedTrack.get(i).small && MainPane.bedCanvas.bedTrack.get(i).getBBfileReader() == null) {						
						if( MainPane.bedCanvas.bedTrack.get(i).intersect && !MainPane.bedCanvas.bedTrack.get(i).loading) {
							MainPane.bedCanvas.annotate(MainPane.bedCanvas.bedTrack.get(i).getHead(), Getter.getInstance().getVariantHead().getNext());	
							MainPane.bedCanvas.intersected = true;							
						}	else if(MainPane.bedCanvas.bedTrack.get(i).intersect && MainPane.bedCanvas.bedTrack.get(i).loading) MainPane.bedCanvas.bedTrack.get(i).waiting = true;
					}	else if(MainPane.bedCanvas.bedTrack.get(i).intersect) {						
						BedCanvas.Annotator annotator = MainPane.bedCanvas.new Annotator(MainPane.bedCanvas.bedTrack.get(i));
						annotator.annotateVars();						
						MainPane.bedCanvas.intersected = true;						
					}
				}				
			}			
			Loader.ready("Calling variants");
			
			MainPane.drawCanvas.repaint();
			loading = false;			
		}
		@Override
		protected String doInBackground() throws Exception { callVariants();	return null; }
	}
	
	public VariantCall[][] variantCaller(final String chrom, final int startpos, final int endpos, final Reads readClass, final int minBaseQuality, final int minReadQuality, final ReferenceSeq reference) {
		VariantCall[][] coverages = new VariantCall[endpos - startpos + 300][8];
		final Iterator<SAMRecord> bamIterator = FileRead.getBamIterator(readClass, chrom, startpos, endpos);
		String run;
		Boolean strand, scanFail = false;
		final HashMap<String, String> runs = new HashMap<String, String>();
		
		try {
			readClass.setCoverageStart(startpos);
			readClass.startpos = startpos;
			readClass.endpos = endpos;
			readClass.setReadStart(startpos);
			readClass.setReadEnd(endpos);
			readClass.setCoverageEnd(startpos + coverages.length);
			SAMRecord samRecord = null;
			while (bamIterator != null && bamIterator.hasNext()) {
				if (!Getter.getInstance().loading()) {	Loader.ready("all");	break;	}
				try {
					try {
						samRecord = bamIterator.next();
					} catch (final htsjdk.samtools.SAMFormatException ex) {
						ex.printStackTrace();
					}
					if (samRecord.getUnclippedStart() >= endpos) break;
					if (samRecord.getMappingQuality() < minReadQuality) continue;
					if (samRecord.getReadUnmappedFlag()) continue;
					if (samRecord.getReadLength() == 0) continue;					
					// TODO jos on pienempi ku vika unclipped start
					if (samRecord.getUnclippedEnd() < startpos) continue;					
					boolean Aread = false;

					if (methyl.isSelected()) {					
						String MD = samRecord.getStringAttribute("MD").replaceAll("[^A-Z]", "");
						if (MD.length() == 0) continue;						
						if (MD.replaceAll("C", "").length() == 0) continue;						
						if (MD.replaceAll("G", "").length() == 0) continue;						
						long count = MD.chars().filter(ch -> ch == 'G').count();
						if (count > 2) Aread = true;						
					}					
					if (MethodLibrary.isDiscordant(samRecord, false)) continue;					
					if (samRecord.getReadName().contains(":")) run = samRecord.getReadName().split(":")[1];
					else run = samRecord.getReadName();
					
					if (!runs.containsKey(run)) runs.put(run, "");
					
					if (samRecord.getAlignmentEnd() >= readClass.getCoverageEnd()) {
						coverages = FileRead.coverageArrayAdd(readClass.sample.longestRead * 2, coverages, readClass);
						reference.append(reference.getEndPos() + readClass.sample.longestRead * 2);
					}
					strand = samRecord.getReadNegativeStrandFlag();
					if (samRecord.getCigarLength() > 1) continue;					
					if (samRecord.getCigarLength() > 1) {					
						int readstart = samRecord.getUnclippedStart();
						int readpos = 0;
						int mispos = 0;

						for (int i = 0; i < samRecord.getCigarLength(); i++) {
							scanFail = false;
							CigarElement element = samRecord.getCigar().getCigarElement(i);
							if (element.getOperator().compareTo(CigarOperator.MATCH_OR_MISMATCH) == 0) {
								for (int r = readpos; r < readpos + element.getLength(); r++) {
									if (((readstart + r) - readClass.getCoverageStart()) < 0) continue;									
									try {
										if (samRecord.getReadBases()[mispos] != reference.getSeq()[((readstart + r) - reference.getStartPos() - 1)]) {
											if ((readstart + r) - readClass.getCoverageStart() < coverages.length - 1 && (readstart + r) - readClass.getCoverageStart() > -1) {
												if (methyl.isSelected()) {
													if (Aread) {														
														if (samRecord.getReadBases()[mispos] == 65) continue;														
													} else {
														if (samRecord.getReadBases()[mispos] == 84) continue;														
													}
												}
												readClass.sample.basequalsum += (int) samRecord.getBaseQualityString().charAt(mispos) - readClass.sample.phred;
												readClass.sample.basequals++;
												if ((int) samRecord.getBaseQualityString().charAt(mispos)	- readClass.sample.phred >= minBaseQuality) {
													if (mispos > 5 && mispos < samRecord.getReadLength() - 5) {
														/*
														* if(r < scanWindow) { scanner = 0; } else { scanner =
														* r-scanWindow; } if(r > samRecord.getReadLength()-scanWindow)
														* { scannerEnd = samRecord.getReadLength(); } else { scannerEnd
														* = r+scanWindow; } for(int s = scanner ; s<scannerEnd;s++) {
														* if(s == r) { continue; } if(samRecord.getReadBases()[s] !=
														* reference.getSeq()[((readstart+s)-reference.getStartPos()-1)]
														* ) { scanFail = true;
														* 
														* break; } }
														*/
														if (!scanFail) {
															if (coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])] == null) 
																  coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])] = new VariantCall();															

															coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])].calls++;
															coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])].qualities += (int) samRecord.getBaseQualityString().charAt(mispos)	- readClass.sample.phred;
															
															if (!coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])].runs.contains(run))
																	 coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])].runs.add(run);
															
															if (!coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])].strands.contains(strand)) 
																	 coverages[(readstart + r) - readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])].strands.add(strand);															
														}
													} else scanFail = true;													
												} else scanFail = true;												
											} else scanFail = true;											
										}
									} catch (final Exception e) {
										System.out.println(samRecord.getSAMString());
										System.out.println(samRecord.getReadLength() + " " + samRecord.getReadString().length());
										e.printStackTrace();
										return null;
									}
									if (!scanFail) {
										if (coverages[((readstart + r) - readClass.getCoverageStart())][0] == null)
											coverages[((readstart + r)	- readClass.getCoverageStart())][0] = new VariantCall();
										
										coverages[((readstart + r) - readClass.getCoverageStart())][0].calls++;

										if (coverages[((readstart + r)	- readClass.getCoverageStart())][0].calls > readClass.getMaxcoverage())
											readClass.setMaxcoverage(coverages[((readstart + r) - readClass.getCoverageStart())][0].calls);										
									}
									mispos++;
								}
								readpos += element.getLength();
							} else if (element.getOperator().compareTo(CigarOperator.DELETION) == 0) {
								scanFail = true;
								readpos += element.getLength();
							} else if (element.getOperator().compareTo(CigarOperator.INSERTION) == 0) {
								// coverages[(readstart+readpos)-
								// readClass.getCoverageStart()][MainPane.baseMap.get((byte)'I')]++;
								mispos += element.getLength();
								scanFail = true;
							} else if (element.getOperator().compareTo(CigarOperator.SOFT_CLIP) == 0) {
								scanFail = true;
								if (i == 0) {
									/*
									* if(element.getLength() > 1000) { System.out.println(samRecord.getReadLength()
									* +" " +samRecord.getReadString().length() +"\n" +samRecord.getSAMString());									
									* }
									*/
									readstart = samRecord.getAlignmentStart();
									mispos += element.getLength();
								}							
							} else if (element.getOperator().compareTo(CigarOperator.HARD_CLIP) == 0) {
								scanFail = true;
								if (i == 0) readstart = samRecord.getAlignmentStart();								
							} else if (element.getOperator().compareTo(CigarOperator.SKIPPED_REGION) == 0) {
								scanFail = true;
								readpos += element.getLength();
							}
						}
					} else {
						int readstart = samRecord.getUnclippedStart();

						for (int r = 0; r < samRecord.getReadLength(); r++) {
							try {
								scanFail = false;
								if (samRecord.getReadBases()[r] != reference.getSeq()[((readstart + r) - reference.getStartPos() - 1)]) {
									if ((readstart + r) - readClass.getCoverageStart() < coverages.length - 1	&& (readstart + r) - readClass.getCoverageStart() > -1) {
												if (methyl.isSelected()) {
													if (Aread) {														
														if (samRecord.getReadBases()[r] == 65) continue;														
													} else {
														if (samRecord.getReadBases()[r] == 84) continue;														
													}
												}
										readClass.sample.basequalsum += (int) samRecord.getBaseQualityString().charAt(r) - readClass.sample.phred;
										readClass.sample.basequals++;
										if ((int) samRecord.getBaseQualityString().charAt(r)	- readClass.sample.phred >= minBaseQuality) {
											if (r > 5 && r < samRecord.getReadLength() - 5) {
												/*
												* if(samRecord.getReadString().charAt(r) == 'C' &&
												* samRecord.getReadString().charAt(r-1) == 'C' &&
												* samRecord.getReadString().charAt(r+1) == 'C') {
												* if(samRecord.getReadString().charAt(r-2) != 'C' &&
												* samRecord.getReadString().charAt(r+2) != 'C') {
												* qualities +=
												* (int)samRecord.getBaseQualityString().charAt(r)-readClass.sample.
												* phred; amount++; } }
												*/
												/*
												* if(r < scanWindow) { scanner = 0; } else { scanner = r-scanWindow; }
												* if(r > samRecord.getReadLength()-scanWindow) { scannerEnd =
												* samRecord.getReadLength(); } else { scannerEnd = r+scanWindow; }
												* for(int i = scanner ; i<scannerEnd;i++) {
												* 
												* if(i == r) { continue; } if(samRecord.getReadBases()[i] !=
												* reference.getSeq()[((readstart+i)-reference.getStartPos()-1)]) {
												* scanFail = true; break; } }
												*/
												if (!scanFail) {
													if (coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])] == null) 
															coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])] = new VariantCall();
													
													coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])].calls++;
													coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])].qualities += (int) samRecord.getBaseQualityString().charAt(r)	- readClass.sample.phred;

													if (!coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])].runs.contains(run)) 
															 coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])].runs.add(run);
													
													if (!coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])].strands.contains(strand))
															 coverages[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[r])].strands.add(strand);													
												}
											} else scanFail = true;											
										} else scanFail = true;										
									} else scanFail = true;									
								}
								if (!scanFail) {
									if ((readstart + r) - readClass.getCoverageStart() < 0) continue;
									if (coverages[((readstart + r) - readClass.getCoverageStart())][0] == null) 
											coverages[((readstart + r)	- readClass.getCoverageStart())][0] = new VariantCall();
									
									coverages[(readstart + r) - readClass.getCoverageStart()][0].calls++;
								}
							} catch (final Exception e) {	ErrorLog.addError(e.getStackTrace());	e.printStackTrace();	break;	}
						}
					}
				} catch (final Exception e) {	e.printStackTrace();	break;	}
			}
		} catch (final Exception ex) { ex.printStackTrace();	return null;	}
		return coverages;
	}
}
