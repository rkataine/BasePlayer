package base.BasePlayer.variants;

import base.BasePlayer.MethodLibrary;

public class BreakPoint {
    public final String chr, svtype;
    public final int pos;

    public BreakPoint(String[] vcfLine) {
        String[] breakpoint = MethodLibrary.getTranslocationBreak(vcfLine);
        this.chr = breakpoint[0].replace("chr", "");
        this.pos = Integer.parseInt(breakpoint[1]);
        this.svtype = MethodLibrary.getValueFromInfo("SVTYPE", vcfLine[7], false);
    }
    public String getChr() { return this.chr; }
    public int getPos() { return this.pos; }
}
