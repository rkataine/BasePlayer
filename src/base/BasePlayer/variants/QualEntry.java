package base.BasePlayer.variants;

public class QualEntry {

	public String key;
	public float value;
	public String format;
	public boolean reverse = false;

	public QualEntry(String key, Float value, String format) {
		this.key = key;
		this.value = value;
		this.format = format;
	}
}