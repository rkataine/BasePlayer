/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.Setter;
import base.BasePlayer.genome.Gene;
import base.BasePlayer.genome.ReferenceSeq;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.reads.ReadNode;
public class SplitClass implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	public int chromEnd = 0, offset, chromOffset;
	public double pixel = 0.0, start=1, end=0, viewLength=0;
	public String chrom;
	public boolean loadingReads = false;
	public int middlePoint = -1;
	//private transient int minReadStart = Integer.MAX_VALUE, maxReadEnd = 0;
//	private transient ArrayList<Transcript> transcripts = new ArrayList<Transcript>();
	private transient ArrayList<Gene> genes = new ArrayList<Gene>();
	//StringBuffer sequence = null;
	//int seqStartPos = 0;	
	public boolean updateReads = false, removed = false, clearedReads = true, clearedCoverages = true;	
	//public StringBuffer readSequence = null;
	private transient ReferenceSeq drawReference, readReference;
	private transient Double divider = 4.0;
	public boolean splitRead = false;
	private transient SplitDraw splitDraw; 
	private transient ArrayList<String[]> chromBands = new ArrayList<String[]>();
	public int transStart = 0;
	
	public SplitClass() {
		
		splitDraw = new SplitDraw();		
	}		
	void resetSplits() {		
		splitDraw = new SplitDraw();
		splitDraw.resizeImages();
	}
	
	/*ArrayList<Transcript> getTranscripts() {
		return this.transcripts;
	}
	void setTranscripts(ArrayList<Transcript> trans) {
		this.transcripts = trans;
	}*/
	public void clearGenes() {
		this.genes.clear();
	}
	public void setChromBands(ArrayList<String[]> bands) {
		this.chromBands = bands;
	}
	public ArrayList<String[]> getChromBands() {
		return this.chromBands;
	}
	public ReferenceSeq getReference() {
		return this.drawReference;
	}
	public void setReference(ReferenceSeq ref) {
		this.drawReference = ref;
	}
	public ReferenceSeq getReadReference() {
		return this.readReference;
	}
	void setReadReference(ReferenceSeq ref) {
		this.readReference = ref;
	}
	public void nullRef() {
		this.readReference = null;
		this.drawReference = null;
	}
	
	/*int getMinReadStart() {
		return this.minReadStart;
	}
	void setMinReadStart(int value) {
		this.minReadStart = value;
	}
	int getMaxReadEnd() {
		return this.maxReadEnd;
	}
	void setMaxReadEnd(int value) {
		this.maxReadEnd = value;
	}*/
	public ArrayList<Gene> getGenes() {
		return this.genes;
	}
	public void setGenes(ArrayList<Gene> genes) {
		this.genes = genes;
	}
	public void setDivider(Double divider) {
		this.divider = divider;
	}
	public SplitDraw getSplitDraw() {
		return this.splitDraw;
	}
	public Double getDivider() {
		return this.divider;
	}
	public Graphics2D getExonImageBuffer() {
		return splitDraw.exonImageBuffer;
	}
	public Graphics2D getReadBuffer() {
		return splitDraw.readBuffer;
	}
	
	public BufferedImage getExonImage() {
		if(splitDraw == null) {
			return null;
		}
		else {
			return splitDraw.exonImage;
		}
	}
	public BufferedImage getReadImage() {
		if(splitDraw == null) {
			return null;
		}
		return splitDraw.readImage;
	}
	
	public Composite getBackupe() {
		return splitDraw.backupe;
	}
	public Composite getBackupr() {
		return splitDraw.backupr;
	}
	public Composite getBackups() {
		return splitDraw.backups;
	}
	public BufferedImage getCytoImage() {
		return splitDraw.cytoImage;
	}
	public void setCytoImage(BufferedImage image) {
		splitDraw.cytoImage = image;
	}
	public void removeSplitDraw() {
		this.splitDraw = null;
	}
	public class SplitDraw {
		Graphics2D exonImageBuffer;
		BufferedImage exonImage;
		Graphics2D readBuffer;
		BufferedImage readImage;
	/* 	Graphics2D selectbuf;
		BufferedImage selectbuffer; */
		Composite backupe, backupr, backups;
		BufferedImage cytoImage;

		public void resizeImages() {
			
			int width = Draw.getSplits().size() > 0 ? (int)(MainPane.screenSize.getWidth() - MainPane.sidebarWidth) / (Draw.getSplits().size()) - 15: (int)MainPane.screenSize.getWidth() - MainPane.sidebarWidth;
			exonImage = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int)MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));	
			exonImageBuffer = (Graphics2D)exonImage.getGraphics();
			exonImageBuffer.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			
			readImage = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int)MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));	
			readBuffer = (Graphics2D)readImage.getGraphics();
			readBuffer.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			backupr = readBuffer.getComposite();
			backupe = exonImageBuffer.getComposite();
			
		}
	}
	void removeSplit() {
		try {
			if (ReadNode.clicked != null && ReadNode.clicked.split.equals(this)) MainPane.drawCanvas.splitList.clear();
			final ArrayList<SplitClass> splits = Draw.getSplits();
			MainPane.drawCanvas.selectedSplit = splits.get(0);
			final String prechrom = splits.get(0).chrom;
			this.setCytoImage(null);
			this.removed = true;
			splits.remove(this);
			if (MainPane.samples > 0) {
				for (int s = 0; s < Getter.getInstance().getSampleList().size(); s++) {
					
					if (Getter.getInstance().getSampleList().get(s).getreadHash() == null) continue;
					Getter.getInstance().getSampleList().get(s).getreadHash().remove(this);
				}
			}
			
			if (!splits.get(0).chrom.equals(prechrom)) {
				FileRead.search = true;
				MainPane.drawCanvas.gotoPos(splits.get(0).chrom, splits.get(0).start, splits.get(0).end);
			}

			for (int i = 0; i < splits.size(); i++) {
				splits.get(i).setCytoImage(null);
				MainPane.chromDraw.drawCyto(splits.get(i));
			}

			MainPane.chromDraw.updateExons = true;
			MainPane.chromDraw.repaint();
			MainPane.drawCanvas.resizeCanvas(MainPane.drawCanvas.getWidth(), MainPane.drawCanvas.getHeight());

			for (int i = 0; i < splits.size(); i++) {
				MainPane.chromDraw.drawCyto(Draw.getSplits().get(i));
				MainPane.chromDraw.drawExons(Draw.getSplits().get(i));
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
				splits.get(i).updateReads = true;
			}
			Setter.getInstance().setUpdateScreen();
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
			
		}
		
	}
}
