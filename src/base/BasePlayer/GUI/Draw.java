/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;

import htsjdk.samtools.SAMRecord;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.datatransfer.*;
import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Supplier;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import base.BasePlayer.*;
import base.BasePlayer.GUI.BedCanvas.bedFeatureFetcher;
import base.BasePlayer.GUI.listeners.DrawListeners;
import base.BasePlayer.GUI.modals.SampleDialog;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.reads.ReadNode;
import base.BasePlayer.reads.Reads;
import base.BasePlayer.reads.SAread;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.sample.SampleNode;
import base.BasePlayer.variants.*;

public class Draw extends JPanel implements MouseMotionListener, MouseListener {

	private ArrayList<SplitClass> splits = new ArrayList<SplitClass>();
	MethodLibrary.mateListSorter mateSorter = new MethodLibrary.mateListSorter();
	private static boolean calculateVars = true;
	private String varloadString = "Click here to load variants", varloadhint = "Note: variant annotation is still possible through variant manager.";
	private Rectangle varStartRect = new Rectangle(), varEndRect = new Rectangle();
	private int varStringLen = 0, width=0, height=0, drawWidth=0;
	private Graphics2D buf, reactivebuf;
	private BufferedImage backbuffer, reactivebuffer;

	public SampleNode sampleOverLap = null;
	public VarNode varOverLap = null;	
	
	private ArrayList<Sample> sampleList = new ArrayList<Sample>();	
	public Rectangle mouseRect = new Rectangle(), readRect = new Rectangle();

	
	private Rectangle hoverRect = new Rectangle();	

	public static Color backColor = new Color(90,90,90);
	static Color curtainColor = new Color(70,70,60);
	static Color varColor= Color.red;
	public static Color sidecolor, backTransparent = new Color(255,255,230,100);
	//public static boolean updatevars = false;
	public Image leftTriangle = null, rightTriangle = null;
	public Iterator<Map.Entry<Integer, SampleNode>> varIterator;	
	public final static double defaultSampleHeight = 15;
	
	public Rectangle2D.Double rect;	
	public String tempreadname = "";
	
	public final static Composite composite = AlphaComposite.getInstance(AlphaComposite.CLEAR,0.5f); //TODO clearRect. readin piirto samaan buffiin
	
	public int mouseX=0, mouseY=0, pressX=0, pressY=0, dragX=0,tempDrag =0, moveX=0,moveY=0;
	public double starttemp, endtemp;	
	public boolean mouseDrag = false, moveDrag = false, scrollbar = false, lineZoomer = false;
	public int tempSample, bottom = 0;
	
	public int loadbarAll = 0, loadBarSample= 0, varpos;
	public static RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	static RenderingHints rhText = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
	
	boolean zoomDrag =false;
	private Sample sample;
	private int readY;
	public int selectedIndex = 0;
	public static boolean updateReads = false, fetchReads = false;
	public static boolean resize = false;
	boolean clusterCalc;	
	private ReadNode read;
	ArrayList<Entry<Integer, Byte>> mismatches;
	public int readwheel;
	public boolean mouseWheel = false;
	
	public SplitClass selectedSplit = null;
	private int prezoom;
	public static DrawVariables drawVariables = DrawVariables.getInstance();
	public Rectangle clickedMateRect;
	public ReadNode hoverMate = null;
	public final ArrayList<splitTuple> splitList = new ArrayList<splitTuple>();
	public Sample clickedReadSample = null;
	
	public String baseHover = null;
	private boolean splitremove, coverageregion, zoomNote = false, readScrollDrag = false, founddiscordant, getMoreVariants = false, resizeSidebar, sampleZoomer;
	public boolean sidebar, intersect = false, forcereload = false, bam = false, annotationOn = false, scrolldrag, overlap = false, first = true, drawed = false;
	private double sampleheight, barHeight, tempViewLength;
	private int sampleYpos, bottomYpos, totalheight, prestart = 0, previsiblesamples, yposition, moveYtemp, yValue, mateadd = 0, previsiblestart = 0, secondSample = -1;
	public int ctrlpressed = 5, coveragevariant, coveragevariantheight, readtextWidth;
	public static int clickedAdd = 0, scrollValue;
	public static boolean variantcalculator = false, updateCoverages = false, fetchVars = false;

	public boolean updateScreen;

	public Draw(final int width, final int height) {		
		super();
		
		this.width = width;
		this.height = height;
		this.drawWidth = (int) ((this.width - MainPane.sidebarWidth) / (double) splits.size());
		//addKeyListener(DrawListeners.drawKeyListener);
		

		backbuffer = MethodLibrary.toCompatibleImage(new BufferedImage((int) MainPane.screenSize.getWidth(), (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		buf = (Graphics2D) backbuffer.getGraphics();
		buf.setRenderingHints(rh);
		reactivebuffer = MethodLibrary.toCompatibleImage(new BufferedImage((int) MainPane.screenSize.getWidth(), (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		reactivebuf = (Graphics2D) reactivebuffer.getGraphics();
		reactivebuf.setRenderingHints(rh);
		reactivebuf.setBackground(BaseConstants.transparent);
		addMouseMotionListener(this);
		addMouseListener(this);
		addMouseWheelListener(DrawListeners::handleMouseWheel);
	}
	public void clearData() {
		
		if (VarMaster.getInstance().clusterNodes != null) VarMaster.getInstance().clusterNodes.clear();
		this.annotationOn = false;
		Getter.getInstance().getSampleList().clear();
		Sample.selectedSample = null;
		Sample.selectedSampleIndex = -1;
		if (this.mismatches != null) this.mismatches.clear();		

		if (Draw.getSplits().size() > 1) this.removeSplits();
		
	}
	public static int getDrawWidth() { return MainPane.drawCanvas.drawWidth; }
	public static Supplier<ArrayList<Sample>> getSampleList = () -> MainPane.drawCanvas.sampleList;

	public int getWidth()	{ return this.width; }
	public static ArrayList<SplitClass> getSplits() { return MainPane.drawCanvas.splits; }
	public void drawVars() {		
		
		if (sampleList.get(0).katri && splits.get(0).viewLength > Settings.settings.get("coverageDrawDistance")) return;
		
		if(this.sampleList.get(0).katri && fetchVars) {
			Loader.setLoading("Loading variants");
			Getter.getInstance().getVariantHead().putNext(null);
			FileRead.vcfReader.getVariants(this.splits.get(0).chrom, (int)this.splits.get(0).start, (int)this.splits.get(0).end, this.sampleList.get(0));
		}

		fetchVars = false;
		if (calculateVars) {
			forEachVisibleSample(sample -> {	
				sample.varcount = sample.homozygotes = sample.heterozygotes = sample.indels = sample.sitioRate = sample.versioRate = 0;
				sample.prepixel = -1;
			});
		}
		varOverLap = null;
		baseHover = null;
		VarNode currentDraw;
		if (sampleList.get(0).SV) {
			currentDraw = Getter.getInstance().getVariantHead().getNext();
		} else {
			currentDraw = VarMaster.getInstance().searchFirstVariantToDraw(this.splits.get(0).chrom, (int)this.splits.get(0).start);
		}
		
		if (currentDraw == null) return;
		
		Draw.drawVariables.heightFrac = 3 * Draw.drawVariables.sampleHeight / (double) 4;
		Draw.drawVariables.varMinHeight = Settings.varsBottom.isSelected() ? 0 : Draw.drawVariables.sampleHeight / (double) 4;
		Draw.drawVariables.pixel = Draw.getSplits().get(0).pixel;
		Draw.drawVariables.start = Draw.getSplits().get(0).start;
		
		while (true) {
			
			if (currentDraw.getPosition() > this.splits.get(0).end) break;
			
			if (FileRead.novars) {
				if(currentDraw.getPrev() != null && currentDraw.getPrev().getChrom().equals(splits.get(0).chrom)) currentDraw.draw(buf);
			} else currentDraw.draw(buf);
			
			currentDraw = currentDraw.getNextVisible();
			if (currentDraw == null) break;
		}
		currentDraw = null;
		
		if (varOverLap != null) setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		calculateVars = false;
		clusterCalc = false;
	}	

	void drawZoom(Graphics2D buf) {
		if (MainPane.chromDraw.lineZoomer || MainPane.bedCanvas.lineZoomer || MainPane.controlDraw.lineZoomer) return;
		buf.setColor(Color.black);
		
		if (lineZoomer) {
			buf.setStroke(BaseConstants.dashed);
			buf.drawLine(pressX, pressY - scrollValue, mouseX,	mouseY - scrollValue);
		} else if (!sampleZoomer && zoomDrag && !lineZoomer) { // || mouseX-pressX >= 0 && mouseY-pressY > -10)) {
			buf.setColor(Color.white);
			buf.drawRect(pressX, 0, mouseX - pressX - 1, MainPane.drawScroll.getViewport().getHeight());
			buf.setColor(BaseConstants.zoomColor);
			buf.fillRect(pressX, 0, mouseX - pressX, MainPane.drawScroll.getViewport().getHeight());
			if (this.mouseX - this.pressX > 0) {
				MenuBar.widthLabel.setText(MethodLibrary.formatNumber((int) ((this.mouseX - this.pressX) / selectedSplit.pixel))	+ "bp  (Right click to cancel zoom)");
			}
		}
		if (!sampleZoomer && mouseX - pressX < 0 && mouseY - pressY < 0) lineZoomer = true;	

		buf.setStroke(BaseConstants.basicStroke);
		buf.setColor(Color.black);
	}

	public void zoomSamples() {
		
		if (prezoom != (pressX - mouseX) / 20) {
			
			if (prezoom < (pressX - mouseX) / 20) 
				drawVariables.setVisibleSamples.accept(drawVariables.getVisibleSamples.get() + 1 + (drawVariables.getVisibleSamples.get() / 10));				
			else 
        		drawVariables.setVisibleSamples.accept(drawVariables.getVisibleSamples.get() - 1 - (drawVariables.getVisibleSamples.get() / 10));
			
			drawVariables.sampleHeight = MainPane.drawScroll.getViewport().getHeight() / (double) (drawVariables.getVisibleSamples.get());
			if (drawVariables.sampleHeight < 1) drawVariables.sampleHeight = 1;
			
			this.height = (int) (drawVariables.sampleHeight * MainPane.samples);
			this.setPreferredSize(new Dimension(this.getWidth(), this.height));
			this.revalidate();
			setScrollbar((int) (drawVariables.getVisibleStart.get() * drawVariables.sampleHeight));
			//pressY = (int) (this.height * pressYrelative);
			
		}
		prezoom = (pressX - mouseX) / 20;
	}

	

	

	void resizeAllCanvas(final int width) {
		buf.dispose();
		backbuffer = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		buf = (Graphics2D) backbuffer.getGraphics();
		buf.setRenderingHints(rh);
		reactivebuf.dispose();
		reactivebuffer = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		reactivebuf = (Graphics2D) reactivebuffer.getGraphics();
		reactivebuf.setRenderingHints(rh);
		reactivebuf.setBackground(BaseConstants.transparent);
		MainPane.chromDraw.chromImageBuffer.dispose();
		MainPane.chromDraw.chromImage = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		MainPane.chromDraw.chromImageBuffer = (Graphics2D) MainPane.chromDraw.chromImage.getGraphics();
		MainPane.chromDraw.selectImageBuffer.dispose();
		MainPane.chromDraw.selectImage = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		MainPane.chromDraw.selectImageBuffer = (Graphics2D) MainPane.chromDraw.selectImage.getGraphics();
	 
		MainPane.bedCanvas.buf.dispose();
		MainPane.bedCanvas.bufImage = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		MainPane.bedCanvas.buf = (Graphics2D) MainPane.bedCanvas.bufImage.getGraphics();
		MainPane.bedCanvas.nodebuf.dispose();
		MainPane.bedCanvas.nodeImage = MethodLibrary.toCompatibleImage(new BufferedImage(width, (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		MainPane.bedCanvas.nodebuf = (Graphics2D) MainPane.bedCanvas.nodeImage.getGraphics();

		MainPane.bedCanvas.backupComposite = MainPane.bedCanvas.nodebuf.getComposite();

		for (int i = 0; i < splits.size(); i++) {
			splits.get(i).getSplitDraw().resizeImages();
		}
		//Setter.getInstance().setUpdateScreen();
	}
	
	public void drawScreen(final Graphics g) {	
		reactivebuf.clearRect(0, 0, this.getWidth(), this.getHeight());
		
		if (updateScreen) {
			buf.setColor(backColor);
			if (Settings.wallpaper == null)	buf.fillRect(MainPane.sidebarWidth, 0, this.getWidth() - MainPane.sidebarWidth, MainPane.drawScroll.getViewport().getHeight());
			else buf.drawImage(Settings.wallpaper, 0, 0, this.getWidth(), MainPane.drawScroll.getViewport().getHeight(), this);
		}
		
		if (MainPane.samples > 0) drawSampleData(buf);
			
		if (zoomDrag || lineZoomer) drawZoom(reactivebuf);
		SideBars.drawSampleSidebar(buf, selectedIndex, scrollValue);
		
		mouseWheel = false;
		updateScreen = false;
		g.drawImage(backbuffer, 0, scrollValue, this);
		g.drawImage(reactivebuffer, 0, scrollValue, this);

	}

	private void drawSampleData(Graphics2D buf) {
			// draw variants behind reads when close zoom
			if ((MainPane.varsamples > 0 || annotationOn) && splits.get(0).viewLength <= 1000) drawVars();
			try {
				if (Sample.hoverSample != null && Sample.hoverSample.getreadHash() != null) {				
					if (selectedSplit != null && Sample.hoverSample.getreadHash().get(selectedSplit) != null) {				
						Sample.hoverSample.getreadHash().get(selectedSplit).mouseIntersectRead(selectedSplit); // TODO fix elsewhere
					}
				}
			} catch(Exception e) { e.printStackTrace(); }
			//drawReads(selectedSplit);
			for (int s = 0; s < this.splits.size(); s++) {
					
				if(drawReads(this.splits.get(s))) {
					//buf.setColor(backColor);
					//buf.fillRect(s*drawWidth + MainPane.sidebarWidth, 0, drawWidth, this.height);		
					buf.drawImage(Draw.getSplits().get(s).getReadImage(), this.splits.get(s).offset, 0, this);				
				}
			}
			if (ReadNode.selected != null) {
				
				reactivebuf.setColor(Color.white);
				reactivebuf.setStroke(BaseConstants.doubleStroke);
				ReadNode.selected.drawTriangles(true, reactivebuf);
				reactivebuf.drawRect(ReadNode.selected.getRect().x + selectedSplit.offset, ReadNode.selected.getRect().y, ReadNode.selected.getRect().width, ReadNode.selected.getRect().height);
			}
			if (Sample.hoverSample != null && Sample.hoverSample.getreadHash() != null) {
				if (selectedSplit != null && Sample.hoverSample.getreadHash().get(selectedSplit) != null) {
					Sample.hoverSample.getreadHash().get(selectedSplit).drawCoverageMouseHover(selectedSplit, reactivebuf);
				}
			}			
			else {
				zoomNote = true;
				/* if (lineZoomer || sampleZoomer) {
					splitList.clear();
					ReadNode.selected = null;
				}
				splitList.clear(); */
				ReadNode.selected = null;
				ReadNode.clicked = null;
			}
			
			if (Draw.drawVariables.variantsEnd > 0 && MainPane.varsamples > 0) drawVarCurtain();
			// draw variants in front of reads when close zoom
			
			if (updateScreen && (MainPane.varsamples > 0 || annotationOn) && splits.get(0).viewLength > 1000) { drawVars(); }
		
			buf.setColor(Color.black);
			buf.setStroke(BaseConstants.basicStroke);

			drawSampleSeparators(reactivebuf);
			if (selectedSplit.viewLength < 300 && MainPane.genomehash.size() > 0) {
				buf.setColor(Color.black);
				buf.setStroke(BaseConstants.dashed);
				buf.drawLine((Draw.getDrawWidth()) / 2 + selectedSplit.offset, 0, (Draw.getDrawWidth()) / 2 + selectedSplit.offset, MainPane.drawScroll.getViewport().getHeight());
				buf.drawLine((int) (Draw.getDrawWidth() / 2 + selectedSplit.pixel) + selectedSplit.offset, 0, (int) (Draw.getDrawWidth() / 2 + selectedSplit.pixel) + selectedSplit.offset, MainPane.drawScroll.getViewport().getHeight());
				buf.setStroke(BaseConstants.basicStroke);
			}

			if (splits.size() > 1 && (selectedIndex < sampleList.size() || sampleList.size() == 0)) {
				buf.setStroke(BaseConstants.strongStroke);
				if (sampleList.size() > 0 && Sample.hoverSample.getreadHash() == null) Sample.hoverSample.resetreadHash();
				splitremove = false;
				
				if (mouseRect.x > this.drawWidth - 35) {					
					if (mouseRect.x < this.drawWidth - 5) {										
						if (mouseRect.y - scrollValue > 5 && mouseRect.y - scrollValue < 35) {								
							buf.setColor(Color.white);
							splitremove = true;
						}					
					}
				}
			
				Rectangle remRect = new Rectangle(selectedSplit.offset + this.drawWidth - 35, 5, 30, 30);
				
				if (mouseRect.intersects(remRect)) {buf.setColor(Color.white); 	splitremove = true;}
				if (buf.getColor().equals(Color.black)) {
					buf.setColor(Color.white);
					buf.fillRect(remRect.x, remRect.y, remRect.width, remRect.height);
					buf.setColor(Color.black);
				} else if (buf.getColor().equals(Color.white)) {
					buf.setColor(Color.black);
					buf.fillRect(remRect.x, remRect.y, remRect.width, remRect.height);
					buf.setColor(Color.white);
				}
				buf.drawLine(selectedSplit.offset + this.drawWidth - 33, 33, selectedSplit.offset + this.drawWidth - 7, 7);
				buf.drawLine(selectedSplit.offset + this.drawWidth - 7, 33, selectedSplit.offset + this.drawWidth - 33, 7);
				buf.setColor(Color.black);
				buf.drawRect(selectedSplit.offset + this.drawWidth - 35, 5, 30, 30);

				for (int i = 1; i < splits.size(); i++) {
					buf.setColor(Color.gray);
					buf.fillRect(splits.get(i).offset - 3, 0, 5, this.getHeight());
					buf.setColor(Color.lightGray);
					buf.fillRect(splits.get(i).offset - 1, 0, 2, this.getHeight());
					if (splits.get(i).middlePoint > -1) {
						buf.setColor(Color.red);
						buf.setStroke(BaseConstants.doubledashed);						
						int middle = (int)((splits.get(i).middlePoint - splits.get(i).start) * splits.get(i).pixel);					
						if (middle > 0 && middle < splits.get(i).viewLength*splits.get(i).pixel) buf.drawLine( splits.get(i).offset + middle, 0, splits.get(i).offset +  middle, this.height);
											
					}
				}
				
				buf.setStroke(BaseConstants.basicStroke);
			}
			if (ReadNode.clicked != null) ReadNode.clicked.drawClickedRead(buf);
			if (sampleList.get(0).katri && splits.get(0).viewLength > Settings.settings.get("coverageDrawDistance")) {
				buf.setColor(Color.lightGray);
				buf.drawString("Zoom in closer than " + MethodLibrary.formatNumber(Settings.settings.get("coverageDrawDistance"))	+ " bp to see events", MainPane.sidebarWidth + 20, BaseVariables.defaultFontSize * 2 + 6);

			}
			else if (bam && zoomNote && splits.get(0).viewLength > Settings.settings.get("coverageDrawDistance") && drawVariables.sampleHeight >= Settings.coverageWindowHeight) {
				buf.setColor(Color.lightGray);
				buf.drawString("Zoom in closer than " + MethodLibrary.formatNumber(Settings.settings.get("coverageDrawDistance"))	+ " bp to see reads and coverages", MainPane.sidebarWidth + 20, BaseVariables.defaultFontSize * 2 + 6);

			} else if (bam && zoomNote && drawVariables.sampleHeight < Settings.coverageWindowHeight) {
				buf.setColor(Color.lightGray);				
				buf.drawString("Expand sample tracks to see reads and coverages", MainPane.sidebarWidth + 20, 20);

			} else {
				if (FileRead.cancelreadread) {					
					buf.setColor(Color.lightGray);					
					buf.drawString("Double click sample tracks to load reads again.", MainPane.sidebarWidth + 20, 20);
				} else {
					if (Settings.fixedCoverage > 0) buf.drawString("Fixed scale: 0-" +Settings.fixedCoverage , MainPane.sidebarWidth + 10, BaseVariables.defaultFontSize );				
				}
			}

			/* if (sampleDrag) {
				buf.setColor(softLight);
				buf.fillRect(moveX, moveY, drawWidth + MainPane.sidebarWidth, (int) drawVariables.sampleHeight);
			} */
	}

	void drawSampleSeparators(Graphics2D buf) {
		
		forEachVisibleSample(sample -> {
			if (drawVariables.sampleHeight > buf.getFont().getSize() && sample.getIndex() != 0) {
				buf.setColor(Color.gray);
				buf.drawLine(MainPane.sidebarWidth,	(int) (sample.getIndex() * drawVariables.sampleHeight) - scrollValue, this.getWidth(), (int) (sample.getIndex() * drawVariables.sampleHeight) - scrollValue);
			}
			
			if (sample.getIndex() == Sample.selectedSampleIndex || sample.getIndex()==selectedIndex) {
				if (drawVariables.sampleHeight > buf.getFont().getSize()) buf.setColor(Color.white);
				else buf.setColor(Color.lightGray);
				
				buf.drawLine(MainPane.sidebarWidth, (int) (sample.getIndex() * drawVariables.sampleHeight) - scrollValue, this.getWidth(), (int) (sample.getIndex() * drawVariables.sampleHeight) - scrollValue);
				buf.drawLine(MainPane.sidebarWidth, (int) ((sample.getIndex() + 1) * drawVariables.sampleHeight) - scrollValue - 1, this.getWidth(), (int) ((sample.getIndex() + 1) * drawVariables.sampleHeight) - scrollValue - 1);

			}
		});
	}
	public void setFont() {
		buf.setFont(BaseVariables.defaultFont);
		varStringLen = buf.getFontMetrics().stringWidth(varloadString);
	}
	void drawVarCurtain() {
		buf.setColor(curtainColor);

		if (drawVariables.variantsEnd > 1 && drawVariables.variantsEnd < splits.get(0).chromEnd) {
			double tempwidth = (drawVariables.variantsStart - splits.get(0).start) * splits.get(0).pixel;

			if (tempwidth > 0) {
				//buf.fillRect(splits.get(0).offset, 0, (int) tempwidth, this.getHeight());
				varStartRect.setBounds((int) (tempwidth - this.varStringLen - 10), 0, this.varStringLen+10, 20);
			}
			tempwidth = (splits.get(0).end - drawVariables.variantsEnd) * splits.get(0).pixel;

			if (tempwidth > 0) {
				//buf.fillRect((int) ((drawVariables.variantsEnd + 1 - splits.get(0).start) * splits.get(0).pixel)	+ splits.get(0).offset, 0, (int) tempwidth, this.getHeight());
				varEndRect.setBounds((int) ((drawVariables.variantsEnd + 1 - splits.get(0).start) * splits.get(0).pixel) + 10, 0, this.varStringLen+10, 20);
			}
		} else {
			if (varEndRect.x != -1) varEndRect.setBounds(-1, -1, 0, 0);
		}

		reactivebuf.setColor(Color.white);
		
		if (drawVariables.variantsStart > splits.get(0).start) {
			
			if (varStartRect.x + varStartRect.width + 10 < this.drawWidth) {
				reactivebuf.drawString(this.varloadString, varStartRect.x + splits.get(0).offset, BaseVariables.defaultFontSize + 2);
			} else {
				reactivebuf.drawString(this.varloadString, this.drawWidth + splits.get(0).offset - (varStartRect.width + 10),	BaseVariables.defaultFontSize + 2);
				varStartRect.setBounds(this.drawWidth - varStartRect.width + 10, 0, this.varStringLen,	BaseVariables.defaultFontSize * 2);
			}
		} else {
			if (varStartRect.x != -1) varStartRect.setBounds(-1, -1, 0, 0);
		}

		if (drawVariables.variantsEnd == 1) {
			varEndRect.setBounds((int) splits.get(0).offset + 10 - MainPane.sidebarWidth, 0, this.varStringLen, 20);
			reactivebuf.drawString(this.varloadString, splits.get(0).offset + 10, BaseVariables.defaultFontSize + 2);
			reactivebuf.drawString(this.varloadhint, (int) (splits.get(0).offset + this.varloadString.length() * MainPane.chromDraw.bounds), BaseVariables.defaultFontSize + 2);

			if (!MenuBar.manage.isEnabled()) MenuBar.manage.setEnabled(true);
		}

		if (drawVariables.variantsEnd < splits.get(0).end) {
			if (varEndRect.x > 10) {
				reactivebuf.drawString(this.varloadString, varEndRect.x + splits.get(0).offset, BaseVariables.defaultFontSize + 2);
			} else {
				reactivebuf.drawString(this.varloadString, splits.get(0).offset + 10, BaseVariables.defaultFontSize + 2);
				varEndRect.setBounds(10, 0, this.varStringLen, BaseVariables.defaultFontSize * 2);
			}
		}
		if (!buf.getFont().equals(BaseVariables.defaultFont)) buf.setFont(BaseVariables.defaultFont);
	}

	void searchMate(final ReadNode readnode) {
		if (readnode.getMatePos() == -1 || clickedReadSample.getreadHash() == null || clickedReadSample.getreadHash().get(readnode.split) == null) return;
		if (readnode.getMates() == null) {
			for (int j = 0; j < clickedReadSample.getreadHash().get(readnode.split).getReads().size(); j++) {
				read = clickedReadSample.getreadHash().get(readnode.split).getReads().get(j);

				while (read != null) {
					if (read.getName().equals(readnode.getName()) && !read.equals(readnode)) {
						if (readnode.getMates() == null) readnode.mates = new ArrayList<ReadNode>();						
						if (!readnode.getMates().contains(read)) {
							readnode.getMates().add(read);
							readnode.getMates().add(readnode);
							read.mates = readnode.getMates();
						}
						read = null;
						return;
					}
					read = read.getNext();
				}
			}
		}
	}

	void eraseReads() {
		for (int i = 0; i < splits.size(); i++) {
			splits.get(i).getReadBuffer().setComposite(composite);
			splits.get(i).getReadBuffer().fillRect(0, 0, (int) MainPane.screenSize.getWidth(), (int) MainPane.screenSize.getHeight());
			splits.get(i).getReadBuffer().setComposite(splits.get(i).getBackupr());
		}
	}

	public void clearReads(final SplitClass split) {
		try {
			if (split.clearedReads) return;

			split.getReadBuffer().setComposite(composite);
			split.getReadBuffer().fillRect(0, 0, (int) MainPane.screenSize.getWidth(), MainPane.drawScroll.getViewport().getHeight());
	
			Reads reads;

			for (int i = 0; i < this.sampleList.size(); i++) {				
				sample = this.sampleList.get(i);
				if (sample.getMates() != null) sample.getMates().clear();			
				if (sample.getreadHash() == null) break;
				
				reads = sample.getreadHash().get(split);

				if (reads == null) continue;

				if (reads.getReads() != null) {
					reads.getReads().clear();
					reads.getHeadAndTail().clear();
				}
				reads.setReadEnd(0);
				reads.setReadStart(Integer.MAX_VALUE);
				reads.setFirstRead(null);
				reads.setLastRead(null);
				reads.setCoverageStart(Integer.MAX_VALUE);
				reads.setCoverageEnd(0);
				reads.searchstart = Integer.MAX_VALUE;
				reads.searchend = 0;
			}
			reads = null;
			read = null;
			ReadNode.selected = null;
			ReadNode.clicked = null;
			ReadNode.saReads = null;
			splitList.clear();
			split.clearedReads = true;

		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}

	public void clearReads() {
		try {
			Iterator<Map.Entry<SplitClass, Reads>> it;
			Map.Entry<SplitClass, Reads> pair;
			Reads reads;

			for (int i = 0; i < this.sampleList.size(); i++) {						
				sample = this.sampleList.get(i);
				if (sample.getreadHash() == null) continue;				
				if (sample.getMates() != null) sample.getMates().clear();

				it = sample.getreadHash().entrySet().iterator();
				while (it.hasNext()) {
					pair = (Map.Entry<SplitClass, Reads>) it.next();
					reads = pair.getValue();
					if (reads.getReads() != null) {
						reads.getReads().clear();
						reads.getHeadAndTail().clear();
					}
					reads.searchend = 0;
					reads.searchstart = Integer.MAX_VALUE;
					reads.setReadEnd(0);
					reads.setReadStart(Integer.MAX_VALUE);
					reads.setFirstRead(null);
					reads.setLastRead(null);
					reads.setCoverageStart(Integer.MAX_VALUE);
					reads.setCoverageEnd(0);
				}
			}

			pair = null;
			reads = null;
			it = null;
			read = null;
			ReadNode.selected = null;
			ReadNode.clicked = null;
			ReadNode.saReads = null;
			splitList.clear();
			selectedSplit.clearedReads = true;

		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}

	void clearReads(final Sample sample) {
		if (sample.getreadHash() == null) return;
		Iterator<Map.Entry<SplitClass, Reads>> it;
		Map.Entry<SplitClass, Reads> pair;
		Reads reads;
		it = sample.getreadHash().entrySet().iterator();
		while (it.hasNext()) {
			pair = (Map.Entry<SplitClass, Reads>) it.next();
			reads = pair.getValue();
			if (reads.getReads() != null) {
				reads.getReads().clear();
				reads.getHeadAndTail().clear();
			}
			reads.setReadEnd(0);
			reads.setReadStart(Integer.MAX_VALUE);
			reads.setFirstRead(null);
			reads.setLastRead(null);
			it.remove();
		}
		ReadNode.selected = null;		
		splitList.clear();
	}

	public static Polygon makeTriangle(final int x, final int y, final int width, final int height, final boolean right) {
		final int[] xs = new int[3];
		final int[] ys = new int[3];
		if (right) {
			final Point p1 = new Point(x, y);
			final Point p2 = new Point(x, y + height);
			final Point p3 = new Point(x + width, (int) (y + (height / 2.0)));
			xs[0] = p1.x;	xs[1] = p2.x;	xs[2] = p3.x;
			ys[0] = p1.y;	ys[1] = p2.y;	ys[2] = p3.y;
		} else {
			final Point p1 = new Point(x, y);
			final Point p2 = new Point(x, y + height);
			final Point p3 = new Point(x - width, (int) (y + (height / 2.0)));
			xs[0] = p1.x;	xs[1] = p2.x;	xs[2] = p3.x;
			ys[0] = p1.y;	ys[1] = p2.y;	ys[2] = p3.y;
		}
		return new Polygon(xs, ys, xs.length);
	}
	
	boolean checkReadFetching(SplitClass split) {
		
		if(fetchReads) { clearReads(split); fetchReads = false; return true; };
		
		if (lineZoomer || mouseDrag) return false;
		
		if (sampleZoomer || Loader.isLoading || scrollbar) return false;
	
		if (split.splitRead) { split.splitRead = false; return false; }
		
		if (prestart != (int) split.start || previsiblesamples != drawVariables.getVisibleSamples.get()) return true;
	
		return true;
	}
	boolean drawReads(final SplitClass split) {
	
		try {
			if (!lineZoomer && (split.viewLength > Settings.readDrawDistance || drawVariables.sampleHeight <= Settings.coverageWindowHeight)) {
				if (!split.clearedReads) {
					updateReads = false;
					clearReads(split);
				}
			}
			if (split.viewLength > Settings.settings.get("coverageDrawDistance") || drawVariables.sampleHeight < 100) return false;
		
			if (!updateReads && !split.updateReads && !updateCoverages) return true;
						
			if (variantcalculator) return false;
			if (MainPane.readsamples < 1 || FileRead.cancelreadread) return false;
			
			zoomNote = bam && split.viewLength > Settings.settings.get("coverageDrawDistance");
			
			split.getReadBuffer().setComposite(composite);
			if (mouseWheel) split.getReadBuffer().fillRect(0, (int) (selectedIndex * drawVariables.sampleHeight) - scrollValue, drawWidth, (int) drawVariables.sampleHeight);
			else split.getReadBuffer().fillRect(0, 0, split.getReadImage().getWidth(), MainPane.drawScroll.getViewport().getHeight());

			split.getReadBuffer().setComposite(split.getBackupr());
			split.getReadBuffer().setStroke(BaseConstants.basicStroke);
			
			if (!split.splitRead) {
				if (previsiblestart != drawVariables.getVisibleStart.get() || previsiblesamples != drawVariables.getVisibleSamples.get()) {
					for (int i = 0; i < drawVariables.getVisibleStart.get(); i++)
						clearReads(sampleList.get(i));
					
					for (int i = drawVariables.getVisibleStart.get() + drawVariables.getVisibleSamples.get(); i < sampleList.size(); i++) 
						clearReads(sampleList.get(i));
					
				}
			}
		
			if (split.viewLength <= Settings.readDrawDistance && split.viewLength > 10) {
				try {
					MainPane.chromDraw.getReadSeq(split);
				} catch (final Exception ex) {
					ex.printStackTrace();
				}
			}
			
			if(checkReadFetching(split)) getReads(split);
			
			previsiblestart = drawVariables.getVisibleStart.get();
			previsiblesamples = drawVariables.getVisibleSamples.get();
			prestart = (int) split.start;

			forEachVisibleSample(readsample -> {
				try {
					
					if (readsample.getreadHash() == null) return;					
					if (readsample.getreadHash().size() < splits.size()) readsample.resetreadHash();
				
					final Reads readClass = readsample.getreadHash().get(split);
					if (!scrolldrag && !FileRead.cancelreadread) {
					
						if (split.viewLength <= Settings.settings.get("coverageDrawDistance") && split.viewLength > Settings.readDrawDistance) {
						
							if (!lineZoomer && !mouseDrag	&& ((readClass.getCoverageStart() > (int) split.start	|| readClass.getCoverageEnd() < (int) split.end)	|| (readClass.getCoverageEnd() > (int) split.end	&& readClass.getCoverageStart() < (int) split.start))) {
								final FileRead reader = new FileRead();
								readClass.setReadStart(Integer.MAX_VALUE);
								readClass.setReadEnd(0);
								reader.readClass = readClass;
								reader.chrom = split.chrom;
								reader.start = (int) (split.start);
								reader.end = (int) (split.end);
								reader.viewLength = (int) (split.viewLength);
								reader.pixel = split.pixel;
								reader.splitIndex = split;
								reader.getreads = true;
								reader.execute();
								updateCoverages = false;
								split.clearedCoverages = false;
							}
						} else if (split.viewLength <= Settings.readDrawDistance) {
							//TODO joku tilanne?
						}						
						if (readClass == null) readsample.resetreadHash();
						readClass.setReadSize(readClass.getReads().size());
						
						for (int j = 0; j < readClass.getReads().size(); j++) {
							if ((int) (((readsample.getIndex() + 1) * Draw.drawVariables.sampleHeight - (BaseVariables.readHeight + 2) * j)) + readsample.getreadHash().get(split).readwheel < (readsample.getIndex()	* Draw.drawVariables.sampleHeight	+ BaseVariables.readHeight)	+ Draw.drawVariables.sampleHeight	/ split.getDivider())	break;
							else if ((int) (((readsample.getIndex() + 1) * Draw.drawVariables.sampleHeight	- (BaseVariables.readHeight + 2) * j)) + readClass.readwheel > (readsample.getIndex()	+ 1) * Draw.drawVariables.sampleHeight) continue;

							try {
								read = readClass.getReads().get(j);

								while (read.getPrev() != null	&& read.getPosition() > split.start - split.viewLength) read = read.getPrev();
								while (read.getNext() != null	&& read.getNext().getPosition() < split.start - split.viewLength) read = read.getNext();

								if (read.getPosition() > split.end) {	}// TODO check								
								
								try {
									readClass.getReads().set(j, read);
								} catch (final Exception e) {
									e.printStackTrace();
									break;
								}
								while (read != null) {

									if (read.getPosition() > split.end) break;
									
									if (read.getPosition() + read.getWidth() < split.start && !read.equals(ReadNode.clicked)) {
										read = read.getNext();
										continue;
									}

									readY = (int) (((readsample.getIndex() + 1) * drawVariables.sampleHeight	- this.bottom	- (j + 1)	* (BaseVariables.readHeight + 2))	+ readClass.readwheel	- scrollValue);

									read.setRectBounds((int) ((read.getPosition() - split.start) * split.pixel), readY,	(int) (read.getWidth() * split.pixel), BaseVariables.readHeight);									
									read.draw(buf);
									read = read.getNext();
								}

							} catch (final Exception e) {
								ErrorLog.addError(e.getStackTrace());
								e.printStackTrace();
							}
						}
						if (ReadNode.clicked != null && clickedReadSample.equals(readsample)) {
							if (ReadNode.clicked.getMates() != null) {
								mateadd = 0;
								for (int m = 0; m < ReadNode.clicked.getMates().size(); m++) {
									read = ReadNode.clicked.getMates().get(m);

									if (read.getRect().y > ((int) (readsample.getIndex() * Draw.drawVariables.sampleHeight	- drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight)	+ Draw.drawVariables.sampleHeight / read.split.getDivider()) && read.getRect().y < ((int) (readsample.getIndex()	* Draw.drawVariables.sampleHeight	- drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight)	+ Draw.drawVariables.sampleHeight)) {
										if (read.getPosition() + read.getWidth() > read.split.start	&& read.getPosition() < read.split.end) {
											read.yposition = read.getRect().y;
											//read.setRectBounds((int) ((read.getPosition() - split.start) * split.pixel), read.getRect().y, (int) (read.getWidth() * split.pixel),	BaseVariables.readHeight);
											read.draw(buf);
										}
										continue;
									}

									if (read.split == null || clickedReadSample.getreadHash().get(read.split) == null) {
										ReadNode.clicked.getMates().remove(m);
										m--;
										continue;
									}
									if (!ReadNode.clicked.equals(ReadNode.clicked.getMates().get(m))) {
										if (!ReadNode.clicked.split.equals(read.split)) read.split.updateReads = true;
										
										if (m > 0) {
											if (ReadNode.clicked.getMates().get(m - 1).split.equals(read.split)) mateadd++;
											else mateadd = 0;
										} else {
											if (read.getRect().intersects(ReadNode.clicked.getRect())) mateadd++;
											else mateadd = 0;
										}
									} else	mateadd = 0;

									yposition = ReadNode.clicked.getRect().y - (BaseVariables.readHeight + 20) * mateadd;
									read.yposition = yposition;

									if (read.getPosition() + read.getWidth() > read.split.start	&& read.getPosition() < read.split.end) read.draw(buf);
								}

							} else {
								if (ReadNode.clicked.getRect().y > ((int) (readsample.getIndex() * Draw.drawVariables.sampleHeight	- drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight)
										+ Draw.drawVariables.sampleHeight / ReadNode.clicked.split.getDivider())	&& ReadNode.clicked.getRect().y < ((int) (readsample.getIndex()	* Draw.drawVariables.sampleHeight
												- drawVariables.getVisibleStart.get()	* Draw.drawVariables.sampleHeight)	+ Draw.drawVariables.sampleHeight)) {
									ReadNode.clicked.yposition = ReadNode.clicked.getRect().y;
									ReadNode.clicked.draw(buf);									
								}
							}
						}
						
						if (readClass.getReadSize()	* (BaseVariables.readHeight	+ 2) > Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight / split.getDivider())) {							
							sampleheight = Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight / split.getDivider());
							totalheight = readClass.getReadSize()	* (BaseVariables.readHeight + 2);
							split.getReadBuffer().setColor(Color.lightGray);

							yValue = (int) ((readsample.getIndex() * Draw.drawVariables.sampleHeight)	- scrollValue	+ Draw.drawVariables.sampleHeight / split.getDivider());

							final int bottom = (int) ((readsample.getIndex()	* Draw.drawVariables.sampleHeight)	- scrollValue	+ Draw.drawVariables.sampleHeight);
							barHeight = sampleheight * (sampleheight / totalheight);
							if (barHeight < 4) barHeight = 4;

							readClass.getScrollBar().setBounds(drawWidth - (BaseVariables.defaultFontSize + 8), yValue,	BaseVariables.defaultFontSize + 8, (int) (Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight / split.getDivider())));
							readClass.getScroller().setBounds(drawWidth - (BaseVariables.defaultFontSize + 6), (int) (bottom - (readClass.readwheel	* (sampleheight / totalheight)) - barHeight),	BaseVariables.defaultFontSize + 6, (int) barHeight);
							split.getReadBuffer().fillRect(readClass.getScrollBar().x, yValue, 20, (int) Draw.drawVariables.sampleHeight);
							split.getReadBuffer().setColor(Color.gray);
							split.getReadBuffer().fillRect(readClass.getScroller().x,	readClass.getScroller().y, 16, (int) barHeight);
							readClass.setReadScroll(true);
						} else readClass.setReadScroll(false);
						
					}
				
					readClass.drawCoverage(split);							
					
				} catch (final Exception e) {
					ErrorLog.addError(e.getStackTrace());
					e.printStackTrace();
				}
				
			});

			split.updateReads = false;
			updateReads = false;
			updateCoverages = false;
		} catch (final Exception ex) {
			ErrorLog.addError(ex.getStackTrace());
			ex.printStackTrace();
		}
		
		return true;
	}

	

	public void paintComponent(final Graphics g) {
		
		try {
			drawScreen(g);
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}

	public void resizeSidebar(final int move) {
		MainPane.sidebarWidth = move;
		if (MainPane.sidebarWidth < 3) MainPane.sidebarWidth = 3;
		repaint();
		for (int i = 0; i < splits.size(); i++) {
			splits.get(i).offset = MainPane.sidebarWidth + (int) (getDrawWidth() * i);
			splits.get(i).chromOffset = (int) (getDrawWidth() * i);
			splits.get(i).pixel = (Draw.getDrawWidth()) / (double) (this.splits.get(i).end - this.splits.get(i).start);
		}
	}

	@Override
	public void mouseDragged(final MouseEvent event) {
		moveY = event.getY();
		moveX = event.getX();
		
		switch (event.getModifiersEx()) {
			case MouseEvent.BUTTON1_DOWN_MASK: {
				mouseX = event.getX();
				mouseY = event.getY();
				if (readScrollDrag) {
					if (moveY - scrollValue <= Sample.hoverSample.getreadHash().get(selectedSplit).getScrollBar().y	+ Sample.hoverSample.getreadHash().get(selectedSplit).getScrollBar().height
							&& moveY - scrollValue >= this.sampleList.get(selectedIndex).getreadHash().get(selectedSplit).getScrollBar().y) {
						final double sampleheight = Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight / selectedSplit.getDivider());
						final double totalheight = Sample.hoverSample.getreadHash().get(selectedSplit).getReadSize()	* (BaseVariables.readHeight + 2);
						Sample.hoverSample.getreadHash().get(selectedSplit).readwheel -= (moveY - moveYtemp) / (sampleheight / totalheight);
						if (Sample.hoverSample.getreadHash().get(selectedSplit).readwheel < 0) Sample.hoverSample.getreadHash().get(selectedSplit).readwheel = 0;

						if (((Draw.drawVariables.sampleHeight - (Draw.drawVariables.sampleHeight	/ selectedSplit.getDivider()))) > (Sample.hoverSample.getreadHash().get(selectedSplit).getReadSize()
										* (BaseVariables.readHeight + 2))	- (Sample.hoverSample.getreadHash().get(selectedSplit).readwheel)) {
							Sample.hoverSample.getreadHash().get(selectedSplit).readwheel = (int) ((Getter.getInstance().getSampleList().get(selectedIndex).getreadHash().get(selectedSplit).getReadSize()
											* (BaseVariables.readHeight + 2))	- ((Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight	/ selectedSplit.getDivider()))));
						}
						moveYtemp = moveY;
						
						selectedSplit.updateReads = true;
						Setter.getInstance().setUpdateScreen();
					}
					break;
				}
				
				if (resizeSidebar) { resizeSidebar(mouseX);	break; }
				
				if (!sidebar) {
					
					if (!sampleZoomer && (mouseX - pressX < -5 && mouseY - pressY < -5 || mouseX - pressX > 5 && mouseY - pressY > -5) || lineZoomer) {
											
						zoomDrag = true;
						if (lineZoomer) {
							MainPane.chromDraw.updateExons = true;
							if (selectedSplit.start > 0 || selectedSplit.end < selectedSplit.chromEnd) {
								zoom();
								MainPane.chromDraw.repaint();
							}
						}
					} else if (!zoomDrag && !lineZoomer && ((mouseX - pressX > 5 && mouseY - pressY < -10) || (mouseX - pressX < -5 && mouseY - pressY > 5))) {				
						sampleZoomer = true;						
						zoomSamples();
					}
					repaint();
				} /* else {
					if (!bamHover && selectedIndex > -1) {
						setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						//sampleDrag = true;
						hoverIndex = (int) ((moveY) / drawVariables.sampleHeight);
						repaint();
					}
				} */
				break;
			}
			case MouseEvent.BUTTON3_DOWN_MASK: {
				if ((int) selectedSplit.start == 0 && (int) selectedSplit.end == selectedSplit.chromEnd) break;
				mouseDrag = true;
				drag(event.getX());
				break;
			}
			case 17: {
				if ((int) selectedSplit.start == 0 && (int) selectedSplit.end == selectedSplit.chromEnd) break;
				mouseDrag = true;
				drag(event.getX());
				break;
			}
		}
	}

	void drag(final int dragX) {

		starttemp = selectedSplit.start;
		endtemp = selectedSplit.end;

		if (starttemp - (dragX - tempDrag) / selectedSplit.pixel < 0) {
			starttemp = 0;
			endtemp = starttemp + selectedSplit.viewLength;
		} else if (endtemp - (dragX - tempDrag) / selectedSplit.pixel > selectedSplit.chromEnd) {
			endtemp = selectedSplit.chromEnd;
			starttemp = endtemp - selectedSplit.viewLength;
		} else {
			starttemp -= (dragX - tempDrag) / selectedSplit.pixel;
			endtemp -= (dragX - tempDrag) / selectedSplit.pixel;
		}

		setStartEnd(starttemp, endtemp);
		MainPane.bedCanvas.repaint();

		MainPane.chromDraw.updateExons = true;
		MainPane.chromDraw.repaint();
		tempDrag = dragX;
		repaint();
	}

	public void addSplit(final String chrom, final int start, final int end) {
		final SplitClass split = new SplitClass();
		split.middlePoint = start + (end-start)/2;
		split.chrom = chrom.replace("chr", "");
		if (selectedSplit.chrom.equals(split.chrom)) split.setGenes(selectedSplit.getGenes());
		else split.setGenes(MainPane.fileReader.getExons(split.chrom));
		
		for (int i = 0; i < this.sampleList.size(); i++) {
			
			if (sampleList.get(i).getreadHash() == null) continue;
			
			final Reads newReads = new Reads();
			newReads.sample = this.sampleList.get(i);
			this.sampleList.get(i).getreadHash().put(split, newReads);
		}

		try {
			if (MenuBar.chromIndex.containsKey(MainPane.refchrom + split.chrom)) {
				split.chromEnd = MenuBar.chromIndex.get(MainPane.refchrom + split.chrom)[1].intValue();
			} else {
				MainPane.showError("Target chromosome (" + (MainPane.refchrom + split.chrom) + ") is not available.\nTry to download more comprehensive reference FASTA-file.",	"Note");
				return;
			}
		} catch (final Exception e) {
			split.chromEnd = end + (end - start);
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
		split.start = start;
		split.end = end;
		split.viewLength = split.end - split.start;

		this.splits.add(split);
		drawVariables.setVisibleStart.accept(selectedIndex);

		drawVariables.setVisibleSamples.accept(1);
		MainPane.drawCanvas.resizeCanvas(MainPane.drawCanvas.getWidth(), MainPane.drawCanvas.getHeight());
		setScrollbar((int) (selectedIndex * drawVariables.sampleHeight));

		for (int i = 0; i < splits.size(); i++) {
			MainPane.chromDraw.drawCyto(Draw.getSplits().get(i));
			MainPane.chromDraw.drawExons(Draw.getSplits().get(i));
			MainPane.chromDraw.updateExons = true;
			MainPane.chromDraw.repaint();
			splits.get(i).updateReads = true;
		}
		Setter.getInstance().setUpdateScreen();
	}

	void removeSplits() {
		try {
			selectedSplit = splits.get(0);
			for (int i = splits.size() - 1; i > 0; i--) {
				splits.get(i).setCytoImage(null);
				splits.get(i).setGenes(null);
				splits.get(i).removeSplitDraw();
				try {
					if (MainPane.samples > 0) {
						for (int s = 0; s < sampleList.size(); s++) {							
							sampleList.get(s).getreadHash().remove(splits.get(i));
						}
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
				splits.remove(i);
			}

			splits.get(0).setCytoImage(null);
			MainPane.chromDraw.drawCyto(splits.get(0));
			MainPane.chromDraw.updateExons = true;
			MainPane.chromDraw.repaint();

			MainPane.drawCanvas.resizeCanvas(MainPane.drawCanvas.getWidth(), MainPane.drawCanvas.getHeight());
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}

	

	void resizeCanvas(final int height) {
		this.height = height;
		this.setPreferredSize(new Dimension(this.getWidth(), height));
		drawVariables.sampleHeight = this.height / (double) drawVariables.getVisibleSamples.get();
		updateReads = updateCoverages = true;
		this.revalidate();
		Setter.getInstance().setUpdateScreen();
	}

	public void resizeCanvas(final int width, final int height) {
		try {
			
			if (splits.get(0) == null || splits.get(0).getReadImage() == null) return;
			if (splits.get(0).getReadImage().getWidth() < this.getWidth()) resizeAllCanvas((int) (MainPane.screenSize.getWidth() * 2));
			if (MainPane.samples == 0) {
				this.setPreferredSize(new Dimension(this.getWidth(), height));
				this.width = width;
				this.height = height;
				this.drawWidth = (int) ((this.width - MainPane.sidebarWidth) / (double) splits.size());
			} else {
				drawVariables.sampleHeight = MainPane.drawScroll.getViewport().getHeight() / (double) drawVariables.getVisibleSamples.get();
				if (drawVariables.sampleHeight < 1) drawVariables.sampleHeight = 1;

				this.setPreferredSize(new Dimension(this.getWidth(), (int) (drawVariables.sampleHeight * MainPane.samples)));

				this.width = width;
				this.height = (int) (drawVariables.sampleHeight * MainPane.samples);
				this.drawWidth = (int) ((this.width - MainPane.sidebarWidth) / (double) splits.size());

				updateReads = updateCoverages = true;
				this.revalidate();
			}

			for (int i = 0; i < splits.size(); i++) {
				splits.get(i).offset = MainPane.sidebarWidth + (int) (getDrawWidth() * i);
				splits.get(i).chromOffset = (int) (getDrawWidth() * i);
				splits.get(i).pixel = (Draw.getDrawWidth())	/ (double) (this.splits.get(i).end - this.splits.get(i).start);
				
				splits.get(i).getSplitDraw().resizeImages();
				//if (i > 0) MainPane.chromDraw.drawExons(splits.get(i));
						
			}
			MainPane.chromDraw.updateExons = true;
			MainPane.chromDraw.repaint();
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}

	@Override
	public void mouseMoved(final MouseEvent event) {
		try {
			moveX = event.getX();
			moveY = event.getY();

			if (!lineZoomer && moveX >= MainPane.sidebarWidth - 6 && moveX <= MainPane.sidebarWidth + 3) {			
				if (getCursor().getType() != Cursor.W_RESIZE_CURSOR) {				
					
					setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
				}
			} else {
				if (getCursor().getType() == Cursor.W_RESIZE_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}	
			
			
			if (MainPane.samples == 0) {
				if (splits.size() > 1) {
					mouseRect.setBounds(moveX - selectedSplit.offset, moveY, 1, 1);
					if (splits.size() > (moveX - MainPane.sidebarWidth) / (Draw.getDrawWidth())) {
						selectedSplit = splits.get((moveX - MainPane.sidebarWidth) / (Draw.getDrawWidth()));
					}
				}
				//repaint();
				return;			
			}
			
			selectedIndex = (int) ((moveY) / drawVariables.sampleHeight);
			if (selectedIndex > -1 && selectedIndex < sampleList.size()) {
				Sample.hoverSample = sampleList.get(selectedIndex);
				//hoverIndex = selectedIndex;
			} else {
				Sample.hoverSample = null;
			}				
			if ((moveX - MainPane.sidebarWidth) / (Draw.getDrawWidth()) > -1	&& (moveX - MainPane.sidebarWidth) / (Draw.getDrawWidth()) < splits.size()) {
				if (selectedSplit != splits.get((moveX - MainPane.sidebarWidth) / (Draw.getDrawWidth()))) {
					selectedSplit = splits.get((moveX - MainPane.sidebarWidth) / (Draw.getDrawWidth()));
					if (selectedSplit == null) {
						selectedSplit = splits.get(0);
					}
					MainPane.chromDraw.repaint();
				}
			}
			if (moveX < MainPane.sidebarWidth) {
				sidebar = true;
				mouseRect.setBounds(moveX, moveY - scrollValue, 1, 1);
			} else {
				sidebar = false;
				SideBars.removeSample = null;
			}

			if (!sidebar) {				
				mouseRect.setBounds(moveX - selectedSplit.offset, moveY, 1, 1);
				readRect.setBounds(moveX - selectedSplit.offset, moveY - scrollValue, 1, 1);
			}
			if (selectedIndex < 0) selectedIndex = 0;
			if (selectedIndex >= sampleList.size()) selectedIndex = sampleList.size() -1; 
			//if (sidebar || splits.get(0).viewLength < 1000000) repaint();
			if (this.varOverLap == null && !sidebar) {
			
				if ((readRect.intersects(varStartRect) || readRect.intersects(varEndRect))) {
					reactivebuf.setFont(MainPane.menuFontBold);
					if (getCursor().getType() != Cursor.HAND_CURSOR) {
						setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						getMoreVariants = true;
						repaint();
					}
					
				} else {
					if (getCursor().getType() == Cursor.HAND_CURSOR)  {
						setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						getMoreVariants = false;
						reactivebuf.setFont(MainPane.menuFont);
						repaint();
					}
				}
			}
			
			
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
		repaint();
	}

	void showsampleMenu(final Sample sample) {
		final SampleDialog dialog = new SampleDialog(sample);
		dialog.createAndShowGUI();
	}

	void addBam(final int sampleindex) {
		try {
			if (VariantHandler.frame != null) VariantHandler.frame.setState(Frame.ICONIFIED);
			
			boolean cram = false;
			final FileDialog fc = new FileDialog(Main.frame, "Choose BAM file", FileDialog.LOAD);
			fc.setDirectory(MainPane.path);
			fc.setFile("*.bam;*.cram;");
			fc.setFilenameFilter(new FilenameFilter() {
				public boolean accept(final File dir, final String name) {
					return name.toLowerCase().endsWith(".bam") || name.toLowerCase().endsWith(".cram");
				}
			});
			fc.setMultipleMode(false);
			fc.setVisible(true);
			final String openfile = fc.getFile();

			if (openfile != null) {
				if (openfile.endsWith(".bai") || openfile.endsWith(".crai")) {
					MainPane.showError("Please, select BAM or CRAM, not the index file.", "Error");
					return;
				}
				final File file = new File(fc.getDirectory() + openfile);
				if (!file.exists()) {
					MainPane.showError("File does not exist.", "Error");
					return;
				}
				MainPane.path = fc.getDirectory();
				MainPane.writeToConfig("DefaultDir=" + MainPane.path);
				MainPane.drawCanvas.bam = true;
				MainPane.readsamples++;
				Getter.getInstance().getSampleList().get(sampleindex).samFile = file;
				Getter.getInstance().getSampleList().get(sampleindex).resetreadHash();

				if (MainPane.readsamples == 1) FileRead.checkSamples();

				if (file.getName().endsWith(".cram")) {
					cram = true;
				} else if (!file.getName().endsWith(".bam")) {
					MainPane.showError("Open BAM or CRAM file.", "Error");
					return;
				}

				Getter.getInstance().getSampleList().get(sampleindex).CRAM = cram;
				if (cram) Getter.getInstance().getSampleList().get(sampleindex).readString = "CRAM";
				else Getter.getInstance().getSampleList().get(sampleindex).readString = "BAM";
			}
		} catch (final Exception ex) {
			MainPane.showError(ex.getMessage(), "Error");
		}
	}
  private void setSampleSelected() {
   
    Sample.selectedSampleIndex = (int) (pressY / drawVariables.sampleHeight);
    if (Sample.selectedSample != null && Sample.selectedSample.equals(sampleList.get(Sample.selectedSampleIndex))) {
      Sample.selectedSampleIndex = -1;
      Sample.selectedSample = null;
    } else {
     
		Sample.selectedSample = sampleList.get(Sample.selectedSampleIndex);
    }
  }
  private void clickSidebar() {
    splitList.clear();
    ReadNode.selected = null;

    if (SideBars.sampleInfo) showsampleMenu(Sample.hoverSample);
    else if (SampleDialog.frame.isVisible()) showsampleMenu(Sample.hoverSample);
    else if (SideBars.bamHover) addBam(selectedIndex);
    else setSampleSelected();			
  }
	@Override
	public void mouseClicked(final MouseEvent event) {
		switch (event.getButton()) {
			case MouseEvent.BUTTON1: {
        if (!sidebar && coverageregion && !splitremove) return;				
				
				if (event.isShiftDown()) {
					selectSampleInterval();
					break;
				}

				if (FileRead.cancelreadread && event.getClickCount() == 2) {				
          FileRead.cancelreadread = false;
          MenuBar.putMessage(null);
          updateReads = true;
          repaint();
        }
       
				if (varOverLap != null) {					
					MethodLibrary.showVariantMenu(this, varOverLap, sampleOverLap, moveX + (int) selectedSplit.pixel,	moveY, "");
					varOverLap = null;
					break;
				}
				if (splitremove) {
					selectedSplit.removeSplit();
				} else if (SideBars.removeSample != null) {
					SideBars.removeSample.removeSample();
				} else if (MainPane.samples > 0) {
					if (sidebar) clickSidebar();													
					clickRead(event);
				}
				
				if (event.getClickCount() == 2) {
					if (drawVariables.getVisibleSamples.get() == 1) {
						setScrollbar(0);
						drawVariables.setVisibleStart.accept(0);
						drawVariables.setVisibleSamples.accept(MainPane.samples);						
						this.resizeCanvas(this.getWidth(), (int) (MainPane.samples * drawVariables.sampleHeight));
					
					} else {
						drawVariables.setVisibleStart.accept(selectedIndex);						
						drawVariables.setVisibleSamples.accept(1);
						this.resizeCanvas(this.getWidth(), (int) (MainPane.samples * drawVariables.sampleHeight));
						setScrollbar((int) (selectedIndex * drawVariables.sampleHeight));
					}					
					
				} 
				break;
			}	case MouseEvent.BUTTON3: {
				if (coverageregion) return;
				if (ReadNode.clicked != null && readRect.intersects(ReadNode.clicked.getRect())) return;
				if (hoverMate != null	&& (readRect.intersects(hoverMate.getRect()) || (readRect.intersects(hoverRect)))) {

					final SAMRecord read = MainPane.fileReader.getRead(hoverMate.split.chrom,	hoverMate.getPosition() - hoverMate.getStartOffset(),	hoverMate.getPosition() + hoverMate.getWidth(), hoverMate.getName(),	clickedReadSample.getreadHash().get(hoverMate.split));
					final String myString = read.getReadString();
					final StringSelection stringSelection = new StringSelection(myString);
					final Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
					clpbrd.setContents(stringSelection, null);
					MenuBar.putMessage("Read sequence copied to clipboard.");					
				} else if (ReadNode.clicked != null && readRect.intersects(ReadNode.clicked.getRect())) {
					final SAMRecord read = MainPane.fileReader.getRead(ReadNode.clicked.split.chrom,	ReadNode.clicked.getPosition() - ReadNode.clicked.getStartOffset(),	ReadNode.clicked.getPosition() + ReadNode.clicked.getWidth(), ReadNode.clicked.getName(),	clickedReadSample.getreadHash().get(ReadNode.clicked.split));
					final String myString = read.getReadString();
					final StringSelection stringSelection = new StringSelection(myString);
					final Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
					clpbrd.setContents(stringSelection, null);
					MenuBar.putMessage("Read sequence copied to clipboard.");
				} else {
					ReadNode.clicked = null;
					ReadNode.saReads = null;
					splitList.clear();
					for (int i = 0; i < splits.size(); i++) {
						splits.get(i).updateReads = true;
					}
				}
        setSampleSelected();
				repaint();
			}
		}
  }
		//Setter.getInstance().setUpdateScreen();
	private void selectSampleInterval() {
    if (secondSample == -1) {
      secondSample = Math.min(sampleList.size() - 1, (int) (pressY / drawVariables.sampleHeight));
      int newScrollValue;						
      if (secondSample - Sample.selectedSampleIndex >= 0) {
        drawVariables.setVisibleSamples.accept(secondSample - Sample.selectedSampleIndex + 1);
        newScrollValue = Sample.selectedSampleIndex;						
      } else {
        drawVariables.setVisibleSamples.accept(Sample.selectedSampleIndex - secondSample + 1);
        newScrollValue = secondSample;							
      }
      drawVariables.setVisibleStart.accept(newScrollValue);							
      updateReads = true;
      updateCoverages = true;
      this.resizeCanvas(this.getWidth(), (int) (MainPane.samples * drawVariables.sampleHeight));
      setScrollbar((int) (newScrollValue * drawVariables.sampleHeight));            
    }
    secondSample = -1;
  }


	private void clickRead(MouseEvent event) {
    if (event.getClickCount() == 2 && ReadNode.clicked != null && readRect.intersects(ReadNode.clicked.getRect())) {
      if (ReadNode.clicked.SA != null) {
        final String[] SAs = ReadNode.clicked.SA.split(";");
        ArrayList<ReadNode[]> headAndTail = Sample.hoverSample.getreadHash().get(selectedSplit).getHeadAndTail();
        ReadNode addread;

        if (Sample.hoverSample.getMates() == null) Sample.hoverSample.setMates();
      
        for (int j = 0; j < headAndTail.size(); j++) {
          addread = headAndTail.get(j)[FileRead.headnode];

          while (addread != null) {
            if (addread.SA == null) {
              addread = addread.getNext();
              continue;
            }
            if (addread.getName().contains("/")) {
              if (!Sample.hoverSample.getMates().containsKey(addread.getName().substring(0, addread.getName().lastIndexOf("/")))) {
                final ArrayList<ReadNode> addList = new ArrayList<ReadNode>();
                addList.add(addread);
                Sample.hoverSample.getMates().put(addread.getName().substring(0, addread.getName().lastIndexOf("/")), addList);
                addread.mates = addList;
                addread.split = selectedSplit;
              } else {
                if (!Sample.hoverSample.getMates().get(addread.getName().substring(0, addread.getName().lastIndexOf("/"))).contains(addread)) {
                  addread.mates = Sample.hoverSample.getMates().get(addread.getName().substring(0, addread.getName().lastIndexOf("/")));
                  addread.mates.add(addread);
                  addread.split = selectedSplit;
                } else break;
              }
            } else {
              if (!Sample.hoverSample.getMates().containsKey(addread.getName())) {
                final ArrayList<ReadNode> addList = new ArrayList<ReadNode>();
                addList.add(addread);
                Sample.hoverSample.getMates().put(addread.getName(), addList);
                addread.mates = addList;
                addread.split = selectedSplit;
              } else {
                if (!Sample.hoverSample.getMates().get(addread.getName()).contains(addread)) {
                  addread.mates = Sample.hoverSample.getMates().get(addread.getName());
                  addread.mates.add(addread);
                  addread.split = selectedSplit;
                } else break;
              }
            }
            addread = addread.getNext();
          }
        }
        addread = null;
        headAndTail = null;
        boolean found = false;
        int pos;
        final ArrayList<splitTuple> splitList = new ArrayList<splitTuple>();

        for (int i = 0; i < SAs.length; i++) {
          final String[] sa = SAs[i].split(",");
          final String chr = sa[0];
          found = false;
          pos = Integer.parseInt(sa[1]);
          for (int s = 0; s < splits.size(); s++) {
            if (chr.equals(splits.get(s).chrom)	&& pos > clickedReadSample.getreadHash().get(splits.get(s)).getCoverageStart()	&& pos < clickedReadSample.getreadHash().get(splits.get(s)).getCoverageEnd()) {
              found = true;
            }
          }
          if (!found) splitList.add(new splitTuple(chr, pos));
        }
        if (splitList.size() > 1) {
          for (int i = 0; i < splitList.size() - 1; i++) {
            for (int j = 1; j < splitList.size(); j++) {
              if (splitList.get(i).chr.equals(splitList.get(j).chr)) {
                if (Math.abs(splitList.get(i).pos	- splitList.get(j).pos) < selectedSplit.viewLength + 10000) splitList.remove(j);
              }
            }
          }
        }

        if (splitList.size() > 0) {
          for (int i = 0; i < splitList.size(); i++) {
            addSplit(splitList.get(i).chr, splitList.get(i).pos - (int) selectedSplit.viewLength / 2,	splitList.get(i).pos + (int) selectedSplit.viewLength / 2);
          }
        }							
      }
      else if (ReadNode.clicked.isDiscordant()) {							
        selectedSplit.splitRead = true;						
        if (!ReadNode.clicked.getMateChrom().equals(selectedSplit.chrom)|| ReadNode.clicked.getMatePos() < clickedReadSample.getreadHash().get(selectedSplit).getReadStart()	|| ReadNode.clicked.getMatePos() > clickedReadSample.getreadHash().get(selectedSplit).getReadEnd()) {
          ArrayList<ReadNode[]> headAndTail = Sample.hoverSample.getreadHash().get(selectedSplit).getHeadAndTail();
          ReadNode addread;

          for (int j = 0; j < headAndTail.size(); j++) {
            if (headAndTail.get(j).length == 0) continue;

            addread = headAndTail.get(j)[FileRead.headnode];
            
            while (addread != null) {
              if (addread.getMateChrom() == null) { // TODO: where this is null? Yeast data
                addread = addread.getNext();
                continue;
              }
              if (!addread.isDiscordant()) {
                addread = addread.getNext();
                continue;
              } 
              if (!addread.getMateChrom().equals(ReadNode.clicked.getMateChrom())) {
                addread = addread.getNext();
                continue;
              }

              if (Sample.hoverSample.getMates() == null) Sample.hoverSample.setMates();										

              if (Sample.hoverSample.getMates().containsKey(addread.getName())) {
                if (!Sample.hoverSample.getMates().get(addread.getName()).contains(addread)) {
                  Sample.hoverSample.getMates().get(addread.getName()).add(addread);
                  addread.mates = Sample.hoverSample.getMates().get(addread.getName());
                  addread.split = selectedSplit;
                }
              } else {
                final ArrayList<ReadNode> addlist = new ArrayList<ReadNode>();
                addlist.add(addread);
                addread.mates = addlist;
                Sample.hoverSample.getMates().put(addread.getName(),	addlist);
                addread.split = selectedSplit;
              }
              addread = addread.getNext();										
            }
          }

          headAndTail = null;
          addSplit(ReadNode.clicked.getMateChrom(),	ReadNode.clicked.getMatePos() - (int) selectedSplit.viewLength / 2,	ReadNode.clicked.getMatePos() + (int) selectedSplit.viewLength / 2);
        } else {
          ArrayList<ReadNode[]> headAndTail = Sample.hoverSample.getreadHash().get(selectedSplit).getHeadAndTail();
          ReadNode addread;
          if (Sample.hoverSample.getMates() == null) Sample.hoverSample.setMates();
          Sample.hoverSample.getMates().clear();

          for (int j = 0; j < headAndTail.size(); j++) {
            addread = headAndTail.get(j)[FileRead.headnode];

            founddiscordant = false;
            while (addread != null) {
              if (!addread.isDiscordant()) {
                addread = addread.getNext();
                continue;
              }	else if (!addread.getMateChrom().equals(ReadNode.clicked.getMateChrom())) {
                founddiscordant = true;
                addread = addread.getNext();
                continue;
              } else {
                founddiscordant = true;
                if (Sample.hoverSample.getMates().containsKey(addread.getName())) {
                  if (!Sample.hoverSample.getMates().get(addread.getName()).contains(addread)) {
                    addread.mates = Sample.hoverSample.getMates().get(addread.getName());
                    addread.mates.add(addread);
                    addread.split = selectedSplit;
                  }
                } else {
                  final ArrayList<ReadNode> addList = new ArrayList<ReadNode>();
                  addList.add(addread);

                  Sample.hoverSample.getMates().put(addread.getName(),	addList);
                  addread.mates = addList;
                  addread.split = selectedSplit;
                }
                addread = addread.getNext();
              }
            }
            if (!founddiscordant) break;
          }
          headAndTail = null;
        }
       
      } 
      if (ReadNode.clicked.getMates() != null) Collections.sort(ReadNode.clicked.getMates(), mateSorter);
      repaint();
    }	else if (ReadNode.selected != null) {
	  ReadNode.saReads = null;
      splitList.clear();
      ReadNode.clicked = ReadNode.selected;
      clickedReadSample = Sample.hoverSample;
      ReadNode.clickedReadInfo = ReadNode.clicked.createReadInfo();
      //ReadNode.clicked.drawClickedRead();
      MethodLibrary.showReadMenu(MainPane.drawCanvas, ReadNode.clicked);
      searchMate(ReadNode.clicked);

      if (ReadNode.clicked.getMates() != null) {
        Collections.sort(ReadNode.clicked.getMates(), mateSorter);
        for (int i = 0; i < ReadNode.clicked.getMates().size() - 1; i++) {
          if (ReadNode.clicked.getMates().get(i).getPosition() == ReadNode.clicked.getMates().get(i + 1).getPosition()) {
            ReadNode.clicked.getMates().remove(i + 1);
            i--;
          }
        }
      }
      for (int i = 0; i < splits.size(); i++) {
        splits.get(i).updateReads = true;
      }
      if (ReadNode.clicked.SA != null) ReadNode.saReads = new SAread(ReadNode.clicked);
      
      repaint();
    }
  }

  private class splitTuple {
		String chr;
		int pos;

		public splitTuple(final String chr, final int pos) {
			this.chr = chr;
			this.pos = pos;
		}
	}

	@Override
	public void mouseEntered(final MouseEvent event) {}

	@Override
	public void mouseExited(final MouseEvent event) { selectedIndex = -1; repaint();}

	@Override
	public void mousePressed(final MouseEvent event) {
		if (!this.hasFocus()) this.requestFocus();
		pressX = event.getX();
		pressY = event.getY();
		moveYtemp = pressY;
		tempDrag = pressX;
		tempViewLength = selectedSplit.viewLength;
		
		switch (event.getButton()) {
			case MouseEvent.BUTTON1: {
				if (getMoreVariants) {
					FileRead.search = true;
					gotoPos(splits.get(0).chrom, splits.get(0).start, splits.get(0).end);
					getMoreVariants = false;
					break;
				}

				if (Sample.hoverSample != null && Sample.hoverSample.getreadHash() != null && Sample.hoverSample.getreadHash().get(selectedSplit) != null && Sample.hoverSample.getreadHash().get(selectedSplit).isReadScroll()) {
					if (mouseRect.intersects(Sample.hoverSample.getreadHash().get(selectedSplit).getScrollBar())) {
						if (!mouseRect.intersects(Sample.hoverSample.getreadHash().get(selectedSplit).getScroller())) {
							if (mouseRect.y < Sample.hoverSample.getreadHash().get(selectedSplit).getScroller().y) {
								final double sampleheight = Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight / selectedSplit.getDivider());
								final double totalheight = Sample.hoverSample.getreadHash().get(selectedSplit).getReadSize() * (BaseVariables.readHeight + 2);
								Sample.hoverSample.getreadHash().get(selectedSplit).readwheel += Sample.hoverSample.getreadHash().get(selectedSplit).getScroller().height	/ (sampleheight / totalheight);

								if (((Draw.drawVariables.sampleHeight	- (Draw.drawVariables.sampleHeight / selectedSplit.getDivider()))) > (Getter.getInstance().getSampleList().get(selectedIndex).getreadHash().get(selectedSplit)
														.getReadSize()	* (BaseVariables.readHeight + 2))	- (Sample.hoverSample.getreadHash().get(selectedSplit).readwheel)) {
									Sample.hoverSample.getreadHash().get(selectedSplit).readwheel = (int) ((Getter.getInstance().getSampleList().get(selectedIndex).getreadHash().get(selectedSplit).getReadSize() * (BaseVariables.readHeight + 2))
													- ((Draw.drawVariables.sampleHeight - (Draw.drawVariables.sampleHeight / selectedSplit.getDivider()))));
								}
							} else {
								final double sampleheight = Draw.drawVariables.sampleHeight - (Draw.drawVariables.sampleHeight / selectedSplit.getDivider());
								final double totalheight = Sample.hoverSample.getreadHash().get(selectedSplit).getReadSize()	* (BaseVariables.readHeight + 2);
								Sample.hoverSample.getreadHash().get(selectedSplit).readwheel -= Sample.hoverSample.getreadHash().get(selectedSplit).getScroller().height	/ (sampleheight / totalheight);

								if (Sample.hoverSample.getreadHash().get(selectedSplit).readwheel < 0) Sample.hoverSample.getreadHash().get(selectedSplit).readwheel = 0;
							}
							updateReads = true;
							repaint();
						} else readScrollDrag = true;
					} else {
								
						resizeSidebar = !lineZoomer && moveX >= MainPane.sidebarWidth - 6 && moveX < MainPane.sidebarWidth + 3;					
					}
				} else {
						
					resizeSidebar = !lineZoomer && moveX >= MainPane.sidebarWidth - 6 && moveX < MainPane.sidebarWidth + 3;				
				}
				break;
			}
			case MouseEvent.BUTTON3: {
				if (sidebar) {
					if (selectedIndex < 0) {
						return;
					}
					showsampleMenu(Sample.hoverSample);
				}
			}
		}
	}

	public static void forEachVisibleSample(Consumer<Sample> action) {
    for (int i = drawVariables.getVisibleStart.get(); i < drawVariables.getVisibleStart.get() + drawVariables.getVisibleSamples.get(); i++) {
        action.accept(Getter.getInstance().getSampleList().get(i));
    }
	}

	public void gotoPos(final double start, final double end) {	setStartEnd(start, end); }

	public void gotoPos(final String chrom, final double start, final double end) {
		if (Draw.variantcalculator) return;
		Draw.updateReads = false;
		splits.get(0).transStart = 0;
		clearReads();
		if (FileRead.search) {
			splits.get(0).nullRef();

			removeSplits();
			if (MainPane.undoPointer < MainPane.undoList.size() - 1) {
				MainPane.undoList.add(MainPane.undoPointer + 1, chrom + ":" + (int) start + "-" + (int) end);
				if (MainPane.undoPointer < MainPane.undoList.size() - 1) {
					for (int i = MainPane.undoPointer + 2; i < MainPane.undoList.size(); i++) {
						MainPane.undoList.remove(i);
						i--;
					}
				}
			} else MainPane.undoList.add(chrom + ":" + (int) start + "-" + (int) end);
			
			MainPane.undoPointer = MainPane.undoList.size() - 1;

			if (forcereload || (MenuBar.chromosomeDropdown.getSelectedItem() != null && !MenuBar.chromosomeDropdown.getSelectedItem().toString().equals(chrom))	|| drawVariables.variantsEnd < splits.get(0).end || drawVariables.variantsStart > splits.get(0).start) {
				if (MenuBar.chromosomeDropdown.getSelectedItem() != null) {
					FileRead.searchStart = (int) start;
					FileRead.searchEnd = (int) end;
					if (FileRead.searchStart < 0) FileRead.searchStart = 0;

					if (MenuBar.chromIndex.get(MainPane.refchrom + chrom) == null) {
						MainPane.showError("Select correct genome.", "Error.");
						return;
					}
					if (FileRead.searchEnd > MenuBar.chromIndex.get(MainPane.refchrom + chrom)[1].intValue()) FileRead.searchEnd = MenuBar.chromIndex.get(MainPane.refchrom + chrom)[1].intValue();
				
					if (!MenuBar.chromosomeDropdown.getSelectedItem().toString().equals(chrom)) {
						for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
							MainPane.bedCanvas.bedTrack.get(i).getHead().putNext(null);
						}
					}
					MenuBar.chromosomeDropdown.setSelectedItem(chrom);
				}
			} else {
				Setter.getInstance().setLoading(true, "Loading Tracks...");
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					if (!MainPane.bedCanvas.bedTrack.get(i).small) {
						MainPane.bedCanvas.bedTrack.get(i).getHead().putNext(null);
						MainPane.bedCanvas.getBEDfeatures(MainPane.bedCanvas.bedTrack.get(i), (int) start, (int) end);
					}
				}
				Loader.ready("Loading Tracks...");
			}

			if (FileRead.novars) {
				drawVariables.variantsStart = 0;
				drawVariables.variantsEnd = splits.get(0).chromEnd;
			}
		}
		setStartEnd(start, end);
		MainPane.chromDraw.updateExons = true;
		Draw.updateReads = true;
		updateCoverages = true;
		MainPane.chromDraw.repaint();
	}

	public void setStartEnd(final double start, final double end) {
		starttemp = start;
		this.endtemp = end;

		if (end - start < BaseConstants.minzoom) {
			this.endtemp = start + ((end - start) / 2) + BaseConstants.minzoom / 2;
			starttemp = start + ((end - start) / 2) - BaseConstants.minzoom / 2;
		}
		if (end > selectedSplit.chromEnd) this.endtemp = selectedSplit.chromEnd;
		if (start < 0) starttemp = 0;
		selectedSplit.updateReads = true;
		updateCoverages = true;
		selectedSplit.start = starttemp;
		selectedSplit.end = endtemp;
		selectedSplit.viewLength = selectedSplit.end - selectedSplit.start;
		selectedSplit.pixel = (Draw.getDrawWidth()) / (double) (selectedSplit.viewLength);
		if (MainPane.samples > 0) {
			forEachVisibleSample(sample -> sample.prepixel = -1);
		}
		if ((int) splits.get(0).start == 0 && (int) splits.get(0).end == splits.get(0).chromEnd) MenuBar.zoomout.setEnabled(false);
		else MenuBar.zoomout.setEnabled(true);
		MenuBar.updatePositions(selectedSplit.start, selectedSplit.end);
		Setter.getInstance().setUpdateScreen();
	}

	void zoom() {
		if (lineZoomer) {
			gotoPos(selectedSplit.start - (tempDrag - mouseX) / selectedSplit.pixel * 2,	selectedSplit.end + (tempDrag - mouseX) / selectedSplit.pixel * 2);
			tempDrag = mouseX;
		} else if (tempSample <= mouseY / drawVariables.sampleHeight && pressX < mouseX) {
			if (selectedSplit.viewLength < BaseConstants.minzoom + 2) return;
			if (MainPane.samples > 0) {
				gotoPos((int) (selectedSplit.start + ((pressX - selectedSplit.offset) / selectedSplit.pixel)), (int) (selectedSplit.start + ((mouseX - selectedSplit.offset) / selectedSplit.pixel)));
			} else {
				gotoPos((int) (selectedSplit.start + ((pressX - selectedSplit.offset) / selectedSplit.pixel)), (int) (selectedSplit.start + ((mouseX - selectedSplit.offset) / selectedSplit.pixel)));
			}
		}
		if (selectedSplit.viewLength > Settings.readDrawDistance)	splitList.clear();
	}

	void release() {
		MainPane.bedCanvas.repaint();
		prezoom = 0;
		zoomDrag = mouseDrag = lineZoomer = mouseDrag = lineZoomer = sampleZoomer = scrollbar = false;
		updateReads = updateCoverages = true;
	}

	/* void moveSample(final int move, final int where) {

		Sample movesample = this.sampleList.get(move);
		this.sampleList.remove(move);
		this.sampleList.add(where, movesample);

		checkSampleIndices();
		if (Sample.selectedSampleIndex != -1) {
			Sample.selectedSampleIndex = where;
			//selectedIndex = -1;
		}

		VarNode node = Getter.getInstance().getVariantHead().getNext();

		while (node != null) {
			node.moveSample(movesample);
			node = node.getNext();
		}

		node = null;
		movesample = null;
	} */

	public void getReads(final SplitClass split) {
		if (split.viewLength > Settings.readDrawDistance || drawVariables.sampleHeight < Settings.coverageWindowHeight) return;
		split.clearedReads = false;
		
		final FileRead reader = new FileRead();
		reader.splitIndex = split;
		reader.getreads = true;
		reader.execute();	
	}

	@Override
	public void mouseReleased(final MouseEvent event) {
		readScrollDrag = false;
    sampleZoomer = false;
		if (getCursor().getType() == Cursor.HAND_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		fetchVars = true;
		if (zoomDrag) {
			zoom();
			MainPane.chromDraw.updateExons = true;
			MainPane.chromDraw.repaint();
		}
		if (resizeSidebar) {
			resizeCanvas(this.getWidth(), this.getHeight());
			MainPane.bedCanvas.repaint();
			MainPane.controlDraw.repaint();
			MainPane.upPanel.setDividerLocation(MainPane.sidebarWidth - 4);
			resizeSidebar = false;
		}
		if (tempViewLength > Settings.readDrawDistance && selectedSplit.viewLength < Settings.readDrawDistance	&& drawVariables.sampleHeight > Settings.coverageWindowHeight) {
			forEachVisibleSample(sample -> {				
				if (sample.samFile == null) return;

				if (sample.getreadHash().get(selectedSplit) != null) {
					sample.getreadHash().get(selectedSplit).setCoverageStart(Integer.MAX_VALUE);
					sample.getreadHash().get(selectedSplit).setCoverages(null);
					sample.getreadHash().get(selectedSplit).getReads().clear();
					sample.getreadHash().get(selectedSplit).getHeadAndTail().clear();
				}
			});

		} else if (tempViewLength < Settings.readDrawDistance && selectedSplit.viewLength > Settings.readDrawDistance	&& drawVariables.sampleHeight > Settings.coverageWindowHeight) {
			forEachVisibleSample(sample -> {
				if (sample.samFile == null) return;
				
				if (sample.getreadHash().get(selectedSplit) != null)
					sample.getreadHash().get(selectedSplit).setCoverageStart(Integer.MAX_VALUE);
			});
		}
		if (sampleZoomer) {
			if (splits.size() > 1) {
				for (int i = 0; i < splits.size(); i++) {
					splits.get(i).updateReads = true;
				}
			}
		}

		release();
		MenuBar.widthLabel.setText("" + MethodLibrary.formatNumber((int) (splits.get(0).end - splits.get(0).start)) + "bp");
		if (MainPane.bedCanvas.bedTrack.size() > 0) {
			final bedFeatureFetcher fetch = MainPane.bedCanvas.new bedFeatureFetcher();
			fetch.execute();
		}
	
		if(!Loader.isLoading && drawVariables.sampleHeight > Settings.coverageWindowHeight) calculateVars = true;
	
		repaint();
	}	
	
	public static void setScrollbar(final int value) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {	
				for(int i = 0; i<Draw.getSplits().size(); i++) {
					Draw.getSplits().get(i).updateReads = true;
				}
					
				updateCoverages = true;
				MainPane.drawScroll.getVerticalScrollBar().setValue(value);
				Draw.scrollValue = value;
				MainPane.drawScroll.getVerticalScrollBar().revalidate();
				MainPane.drawCanvas.scrollbar = false;

				forEachVisibleSample(sample -> sample.prepixel = -1);
				
				if(Draw.drawVariables.getVisibleStart.get() != (0.5+scrollValue/Draw.drawVariables.sampleHeight)) {
					drawVariables.setVisibleStart.accept((int)(0.5+scrollValue/Draw.drawVariables.sampleHeight));
				}
				Setter.getInstance().setUpdateScreen();
			}
		});	
	}
	public static void setGlasspane(final boolean value) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainPane.glassPane.setVisible(value);   	 
				MainPane.glassPane.repaint();
			}
		});
	}
}
