
/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.util.ArrayList;

import htsjdk.tribble.readers.TabixReader;
import htsjdk.tribble.bed.BEDCodec;

import javax.swing.JPanel;

import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.Control;
import base.BasePlayer.variants.VarMaster;
public class ControlCanvas extends JPanel implements MouseMotionListener, MouseListener, KeyListener {

private static final long serialVersionUID = 1L;
	
	BufferedImage bufImage, nodeImage;
	Graphics2D buf, nodebuf;
	int width, height, bedwidth;
	Color zoomColor = new Color(115,115,100,100);	
	Composite backupComposite;
	Rectangle remoBox = new Rectangle();
	TabixReader tabixReader = null;
	TabixReader.Iterator iterator = null;
	TabixReader.Iterator bedIterator = null;	
	Rectangle mouseRect = new Rectangle();
	BEDCodec bedcodec = null;
	private int trackstart;
	private int selectedBox = -1;
	private boolean overlapping = false;
	private boolean typing = false;
	private int cursorPosition = 0;
	private int typeTextWidth = 0;	
	FontMetrics fm;
	private int typeBox = 0;
	private int selectedPlay = 0;
	private boolean sidebar = false;	
	private int hoverIndex = -1;	
	private int removeControl = -1;
	private Rectangle sideMouseRect = new Rectangle();

	private int pressY;
	public ArrayList<Double> trackDivider = new ArrayList<Double>();
	private boolean mouseDrag;
	private int resizeDivider = 0;
	private boolean resizer;
	private int preresize = 0;
	private double preresizer = 0.0;
	private int mouseY = 0, mouseX = 0;
	boolean zoomDrag = false;	
	private ArrayList<Double> tempDivider;
	boolean lineZoomer;

	private boolean positivelock;

	private boolean negative;

	private boolean negativelock;

	private String zoomtext;

	private Rectangle2D textWidth;

	private int zoompostemp;

	private int trackheight;	
	
	public ControlCanvas(int width, int height) {	
	
		this.width = width;
		this.height = height;
		bufImage = new BufferedImage((int)MainPane.screenSize.getWidth(), (int)MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB);	
		buf = (Graphics2D)bufImage.getGraphics();
//		buf.setRenderingHints(Draw.rh);
		nodeImage = new BufferedImage((int)MainPane.screenSize.getWidth(), (int)MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB);
		nodebuf = (Graphics2D)nodeImage.getGraphics();
//		nodebuf.setRenderingHints(Draw.rh);
		backupComposite = nodebuf.getComposite();
		buf.setFont(BaseVariables.defaultFont);
		nodebuf.setFont(BaseVariables.defaultFont);
		fm = buf.getFontMetrics();
		addMouseMotionListener(this);
		addMouseListener(this);
		addKeyListener(this);
	}
	

void drawScreen(Graphics g) {	
	
	//buf.drawImage(Draw.image, MainPane.sidebarWidth, 0, this.getWidth(),(int)MainPane.screenSize.getHeight(), this);
	
	if(Settings.wallpaper == null) {
		buf.setColor(Draw.backColor);
		buf.fillRect(MainPane.sidebarWidth, 0, this.getWidth(), nodeImage.getHeight());	
	}
	else {
		
		buf.drawImage(Settings.wallpaper, 0,0, null);
		buf.setColor(Draw.backColor);	
		buf.fillRect(MainPane.sidebarWidth, 0,this.getWidth(), nodeImage.getHeight());
	}
//	buf.setColor(Color.gray);
	if(MainPane.readingControls) {
		return;
	}
	if(this.trackDivider.size() > 0 && this.trackDivider.get(this.trackDivider.size()-1) != 1.0) {	
		 for(int i = 0 ; i<this.trackDivider.size(); i++) {
			 this.trackDivider.set(i, ((i+1)*(this.getHeight()/(double)trackDivider.size())/this.getHeight()));
		 }				
	}

	drawSidebar();
	
	if(resizer && !mouseDrag) {
		resizer = false;
	}
	//drawNodes();
	if(Getter.getInstance().getControlList().size() > 1) {
		buf.setStroke(BaseConstants.doubleStroke);
	/*	for(int i = 1; i< Getter.getInstance().getControlList().size(); i++) {
			buf.drawLine(0, (int)(i*(MainPane.controlScroll.getViewport().getHeight()/(double)Getter.getInstance().getControlList().size()))-2, MainPane.controlScroll.getViewport().getWidth(), (int)(i*(MainPane.controlScroll.getViewport().getHeight()/(double)Getter.getInstance().getControlList().size()))-2);
		}*/
		for(int i = 0 ; i<Getter.getInstance().getControlList().size(); i++) {
			
			if(i <Getter.getInstance().getControlList().size()-1) {
				buf.setColor(Color.lightGray);
				buf.drawLine(0, (int)(trackDivider.get(i)*this.getHeight()), this.getWidth(), (int)(trackDivider.get(i)*this.getHeight()));
				buf.setColor(Color.gray);
				buf.drawLine(0, (int)(trackDivider.get(i)*this.getHeight())+1, this.getWidth(), (int)(trackDivider.get(i)*this.getHeight())+1);
				if(!lineZoomer && mouseY < (int)(trackDivider.get(i)*this.getHeight())+4 && mouseY > (int)(trackDivider.get(i)*this.getHeight())-4) {
					resizer = true;
					
					if(getCursor().getType() != Cursor.N_RESIZE_CURSOR) {
						resizeDivider = i;
						setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));					
					}	
				}			
			}		
		}
	}
	
	if(!resizer && !overlapping) {		
		if(getCursor().getType() != Cursor.DEFAULT_CURSOR) {			
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	drawZoom();
	g.drawImage(bufImage, 0, 0, null);	
}	

void drawSidebar() {
	
	buf.setColor(Draw.sidecolor);
	buf.fillRect(0, 0, MainPane.sidebarWidth, this.getHeight());
	buf.setColor(BaseConstants.softColor);
	buf.fillRect(0, 0, MainPane.sidebarWidth, this.getHeight());
	buf.setColor(Color.black);
	
	if(Getter.getInstance().getControlList().size() > 0 && this.trackDivider.size() > 0) {
		overlapping = false;
		for(int i = 0; i<Getter.getInstance().getControlList().size(); i++) {
			if(i == 0) {
				trackstart = BaseVariables.defaultFontSize;
				trackheight = (int)(trackDivider.get(i)*this.getHeight());
			}
			else {
				trackstart = BaseVariables.defaultFontSize+(int)(trackDivider.get(i-1)*this.getHeight());
				trackheight = (int)(trackDivider.get(i)*this.getHeight())-(int)(trackDivider.get(i-1)*this.getHeight());
			}
			
			buf.drawString((i+1) +": " +Getter.getInstance().getControlList().get(i).getName(), 10, trackstart);
			/* if(trackheight > BaseVariables.defaultFontSize*2+6) {
				buf.drawString("Allele count: "+Getter.getInstance().getControlList().get(i).varcount, 10, trackstart+BaseVariables.defaultFontSize+5);
			} */
			if(trackheight > BaseVariables.defaultFontSize*4+6) {
				if((int)Getter.getInstance().getControlList().get(i).alleleBox.getBounds().getY() != trackstart+BaseVariables.defaultFontSize*2) {
					Getter.getInstance().getControlList().get(i).alleleBox.setBounds(10, trackstart+BaseVariables.defaultFontSize*2, 100, BaseVariables.defaultFontSize+6);
					Getter.getInstance().getControlList().get(i).playbox.setBounds((int)Getter.getInstance().getControlList().get(i).alleleBox.getMaxX()+10, trackstart+BaseVariables.defaultFontSize*2,  BaseVariables.defaultFontSize+6,  BaseVariables.defaultFontSize+6);
					Getter.getInstance().getControlList().get(i).playTriangle.reset();
					
					Getter.getInstance().getControlList().get(i).playTriangle.addPoint(Getter.getInstance().getControlList().get(i).playbox.x+BaseVariables.defaultFontSize/4, Getter.getInstance().getControlList().get(i).playbox.y+BaseVariables.defaultFontSize/4);
					Getter.getInstance().getControlList().get(i).playTriangle.addPoint(Getter.getInstance().getControlList().get(i).playbox.x+BaseVariables.defaultFontSize/4, Getter.getInstance().getControlList().get(i).playbox.y+(int)Getter.getInstance().getControlList().get(i).playbox.getHeight()-BaseVariables.defaultFontSize/4);
					Getter.getInstance().getControlList().get(i).playTriangle.addPoint(Getter.getInstance().getControlList().get(i).playbox.x+(int)Getter.getInstance().getControlList().get(i).playbox.getWidth() -BaseVariables.defaultFontSize/4, Getter.getInstance().getControlList().get(i).playbox.y+(int)Getter.getInstance().getControlList().get(i).playbox.getHeight()/2);
				}
			//	if( Getter.getInstance().getControlList().get(i).varcount > 2) {
				buf.setColor(Color.white);
			
				
				buf.fillRect(Getter.getInstance().getControlList().get(i).alleleBox.getBounds().x,Getter.getInstance().getControlList().get(i).alleleBox.getBounds().y,Getter.getInstance().getControlList().get(i).alleleBox.getBounds().width, Getter.getInstance().getControlList().get(i).alleleBox.getBounds().height );
				buf.setColor(Color.black);
				buf.drawString("AF:", Getter.getInstance().getControlList().get(i).alleleBox.getBounds().x+4, Getter.getInstance().getControlList().get(i).alleleBox.getBounds().y+BaseVariables.defaultFontSize);
				
				buf.drawString(Getter.getInstance().getControlList().get(i).alleletext.toString(), Getter.getInstance().getControlList().get(i).alleleBox.getBounds().x+BaseVariables.defaultFontSize*2, Getter.getInstance().getControlList().get(i).alleleBox.getBounds().y+BaseVariables.defaultFontSize);
				buf.setColor(Color.gray);
				buf.drawRect(Getter.getInstance().getControlList().get(i).alleleBox.getBounds().x,Getter.getInstance().getControlList().get(i).alleleBox.getBounds().y,Getter.getInstance().getControlList().get(i).alleleBox.getBounds().width, Getter.getInstance().getControlList().get(i).alleleBox.getBounds().height );
					
					
					//	}
				buf.setColor(Color.white);
				buf.fillRoundRect(Getter.getInstance().getControlList().get(i).playbox.getBounds().x-1,Getter.getInstance().getControlList().get(i).playbox.getBounds().y-1,Getter.getInstance().getControlList().get(i).playbox.getBounds().width, Getter.getInstance().getControlList().get(i).playbox.getBounds().height,2,2 );
				buf.setColor(Color.gray);
				buf.fillRoundRect(Getter.getInstance().getControlList().get(i).playbox.getBounds().x+1,Getter.getInstance().getControlList().get(i).playbox.getBounds().y+1,Getter.getInstance().getControlList().get(i).playbox.getBounds().width, Getter.getInstance().getControlList().get(i).playbox.getBounds().height,2,2 );
				
				if(sideMouseRect.intersects(Getter.getInstance().getControlList().get(i).playbox)) {
					overlapping = true;
					if(getCursor().getType() != Cursor.HAND_CURSOR) {
						selectedPlay = i;
						setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					}
					buf.setColor(Color.white);
				//	buf.drawRect(Getter.getInstance().getControlList().get(i).playbox.getBounds().x-1,Getter.getInstance().getControlList().get(i).playbox.getBounds().y-1,Getter.getInstance().getControlList().get(i).playbox.getBounds().width+1, Getter.getInstance().getControlList().get(i).playbox.getBounds().height+1);
					buf.fillRoundRect(Getter.getInstance().getControlList().get(i).playbox.getBounds().x,Getter.getInstance().getControlList().get(i).playbox.getBounds().y,Getter.getInstance().getControlList().get(i).playbox.getBounds().width, Getter.getInstance().getControlList().get(i).playbox.getBounds().height,2,2 );
					
				}
				else {
					buf.setColor(Draw.sidecolor);
					buf.fillRoundRect(Getter.getInstance().getControlList().get(i).playbox.getBounds().x,Getter.getInstance().getControlList().get(i).playbox.getBounds().y,Getter.getInstance().getControlList().get(i).playbox.getBounds().width, Getter.getInstance().getControlList().get(i).playbox.getBounds().height,2,2 );
				}
				
				if(Getter.getInstance().getControlList().get(i).controlOn) {
					buf.setColor(BaseConstants.greenColor);
					buf.fillRoundRect(Getter.getInstance().getControlList().get(i).playTriangle.getBounds().x, Getter.getInstance().getControlList().get(i).playTriangle.getBounds().y,(int)Getter.getInstance().getControlList().get(i).playbox.getWidth()-BaseVariables.defaultFontSize/4*2,(int)Getter.getInstance().getControlList().get(i).playbox.getHeight()-BaseVariables.defaultFontSize/4*2,2,2);
				}
				else {
					buf.setColor(BaseConstants.redColor);
					buf.fillPolygon(Getter.getInstance().getControlList().get(i).playTriangle);
				}				
				
				buf.setColor(Color.black);
				
				if(sideMouseRect.intersects(Getter.getInstance().getControlList().get(i).alleleBox)/* &&  Getter.getInstance().getControlList().get(i).varcount > 2*/) {
					overlapping = true;
					
					if(getCursor().getType() != Cursor.TEXT_CURSOR) {
						selectedBox = i;
						setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
					}		
					
				}
			}
		}
		if(typing && trackheight > BaseVariables.defaultFontSize*4) {
			if(typeBox > -1) {
				buf.drawLine(Getter.getInstance().getControlList().get(typeBox).alleleBox.getBounds().x+this.typeTextWidth+BaseVariables.defaultFontSize*2, Getter.getInstance().getControlList().get(typeBox).alleleBox.getBounds().y+2, Getter.getInstance().getControlList().get(typeBox).alleleBox.getBounds().x+this.typeTextWidth+BaseVariables.defaultFontSize*2, Getter.getInstance().getControlList().get(typeBox).alleleBox.y +(int)Getter.getInstance().getControlList().get(typeBox).alleleBox.getHeight() -2);
			}
		}
		if(!overlapping) {			
				
			if(getCursor().getType() != Cursor.DEFAULT_CURSOR) {
				selectedBox = -1;
				selectedPlay = -1;
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}			
		}
	}
	if(sidebar) {
		buf.setColor(Color.black);
		buf.setStroke(BaseConstants.basicStroke);
		if(hoverIndex > -1 && hoverIndex < trackDivider.size() && (this.remoBox.getBounds().x != MainPane.sidebarWidth-(BaseVariables.defaultFontSize+10) || this.remoBox.getBounds().y != (int)(trackDivider.get(hoverIndex)*this.getHeight()) -(BaseVariables.defaultFontSize+6))) {
			this.remoBox.setBounds(MainPane.sidebarWidth-(BaseVariables.defaultFontSize+10), (int)(trackDivider.get(hoverIndex)*this.getHeight()) -(BaseVariables.defaultFontSize+6), BaseVariables.defaultFontSize+3, BaseVariables.defaultFontSize+3);
		}		
	
		if(sideMouseRect.intersects(this.remoBox)) {				
			removeControl = hoverIndex;
			buf.setColor(Color.white);
			buf.fillRect(this.remoBox.x-2, this.remoBox.y-1, this.remoBox.width+2, this.remoBox.height+2);
		}
		else {				
			removeControl = -1;
		}
		
		buf.setColor(Color.black);
		buf.drawRect(this.remoBox.x, this.remoBox.y, this.remoBox.width, this.remoBox.height);
		buf.drawLine(this.remoBox.x, this.remoBox.y, this.remoBox.x+this.remoBox.width, this.remoBox.y+(int)this.remoBox.getHeight());
		buf.drawLine(this.remoBox.x, this.remoBox.y+(int)this.remoBox.getHeight(), this.remoBox.x+this.remoBox.width,this.remoBox.y);	
	}
	
	buf.setStroke(BaseConstants.doubleStroke);
	buf.setColor(Color.gray);
	buf.drawLine(MainPane.sidebarWidth-1, 0, MainPane.sidebarWidth-1, this.getHeight());
	buf.drawLine(1, 0, 1, this.getHeight());
	buf.setColor(Color.lightGray);
	buf.drawLine(3, 0,3, this.getHeight());
	buf.drawLine(MainPane.sidebarWidth-3, 0, MainPane.sidebarWidth-3, this.getHeight());
	buf.setStroke(BaseConstants.basicStroke);
}

void drawNodes() {
	
	nodebuf.setColor(Draw.backColor);
	nodebuf.fillRect(0,0, bufImage.getWidth(),this.getHeight());	
/*	nodebuf.setComposite( MainPane.drawCanvas.composite);		
	nodebuf.fillRect(0,0, bufImage.getWidth(),this.getHeight());	
	nodebuf.setComposite(this.backupComposite);	
	*/
	buf.drawImage(nodeImage, MainPane.sidebarWidth, 0, this);
	
}

void drawZoom() {	
	
	if(lineZoomer ) {
		
		buf.setColor(Color.black);
		buf.setStroke(BaseConstants.dashed);
		
		buf.drawLine(MainPane.drawCanvas.pressX, pressY, mouseX, mouseY);
		buf.setStroke(BaseConstants.doubleStroke);
		
	}
	else if(zoomDrag) {
		
		
	//	buf.setStroke(Draw.dashed);
		buf.setColor(Color.white);
	//	buf.setFont(ChromBaseVariables.defaultFont);
		if(this.mouseX-MainPane.drawCanvas.pressX >= 0) {
			buf.drawRect(MainPane.drawCanvas.pressX, 0, this.mouseX-MainPane.drawCanvas.pressX-1, this.getHeight());	
			if(Draw.getDrawWidth()-this.mouseX > 200) {
				buf.drawString("" +MethodLibrary.formatNumber((int)((this.mouseX-MainPane.drawCanvas.pressX)/MainPane.drawCanvas.selectedSplit.pixel)) +"bp", MainPane.drawCanvas.pressX+(this.mouseX-MainPane.drawCanvas.pressX)+4, this.mouseY-35);
				buf.drawString("Right click to cancel zoom" , MainPane.drawCanvas.pressX+(this.mouseX-MainPane.drawCanvas.pressX)+4, this.mouseY-6);
			}
			else {
				fm = buf.getFontMetrics();
				zoomtext = ""+MethodLibrary.formatNumber((int)((this.mouseX-MainPane.drawCanvas.pressX)/MainPane.drawCanvas.selectedSplit.pixel)) +"bp";
				textWidth = fm.getStringBounds(zoomtext, buf);
				this.zoompostemp = (int)((MainPane.drawCanvas.pressX+(this.mouseX-MainPane.drawCanvas.pressX))-textWidth.getWidth());
				buf.drawString(zoomtext, this.zoompostemp, this.mouseY-35);
				
				textWidth = fm.getStringBounds("Right click to cancel zoom", buf);
				this.zoompostemp = (int)((MainPane.drawCanvas.pressX+(this.mouseX-MainPane.drawCanvas.pressX))-textWidth.getWidth())-6;
				buf.drawString("Right click to cancel zoom" , this.zoompostemp, this.mouseY-6);
	
			}
			buf.setColor(BaseConstants.zoomColor);
			buf.fillRect(MainPane.drawCanvas.pressX, 0, this.mouseX-MainPane.drawCanvas.pressX, this.getHeight());
		
		}
		else {
			
			lineZoomer = true;
			MainPane.drawCanvas.lineZoomer = true;
			zoomDrag = false;
		
		
		}
		buf.setFont(BaseVariables.defaultFont);
	}
//	buf.setStroke(BaseConstants.doubleStroke);
}

public void paint(Graphics g) {
	try {	
		
		drawScreen(g);
	}
	catch(Exception e) {
		e.printStackTrace();
	}
}

public void mouseClicked(MouseEvent event) {
	switch(event.getModifiersEx()) {	
		
	case InputEvent.BUTTON1_DOWN_MASK: {
		
		
	}
	case InputEvent.BUTTON3_DOWN_MASK: {			
		
		//this.bedTrack.get(hoverIndex).getPopup().show(this, mouseX, mouseY);
	}	
	}	
}	

public void removeControl(int removeControl) {
	
	MethodLibrary.removeHeaderColumns(Getter.getInstance().getControlList().get(removeControl));
	Getter.getInstance().getControlList().remove(removeControl);
	hoverIndex = -1;
	trackDivider.remove(removeControl);
	
	if(Getter.getInstance().getControlList().size() == 0) {
		//MainPane.fixControlFile.setEnabled(false);
		MainPane.controlScroll.setVisible(false);
		 MainPane.trackPane.setDividerLocation(0);		
		 MainPane.trackPane.setDividerSize(0);
		if(!MainPane.bedScroll.isVisible()) {
			MainPane.varpane.setDividerSize(0);
			MainPane.trackPane.setVisible(false);
			MainPane.trackPane.setDividerSize(0);
			MainPane.varpane.setResizeWeight(0.0);   
		}
	}
	boolean allfalse = true;
	for(int i = 0; i<Getter.getInstance().getControlList().size(); i++) {
		Getter.getInstance().getControlList().get(i).setIndex((short)i);
		if(Getter.getInstance().getControlList().get(i).controlOn) {
			allfalse = false;
			
		}
	}
	if(allfalse) {
		Control.controlData.controlsOn = false;
	}
}
public void mouseEntered(MouseEvent arg0) {}	
public void mouseExited(MouseEvent arg0) {}	
@SuppressWarnings("unchecked")
public void mousePressed(MouseEvent event) {
	
	pressY = event.getY();
	mouseDrag = true;
	
	this.requestFocus();
	MainPane.drawCanvas.pressX = event.getX();	
	MainPane.drawCanvas.tempDrag = MainPane.drawCanvas.pressX;
	
	switch(event.getModifiersEx()) {	
		case InputEvent.BUTTON1_DOWN_MASK: {	
			if(resizer) {
				preresize = mouseY;
				preresizer = this.trackDivider.get(this.resizeDivider)*this.getHeight();
				tempDivider = (ArrayList<Double>)trackDivider.clone();			
			} else if(this.selectedBox > -1) {
				this.requestFocus();
				typing  = true;
				typeBox = this.selectedBox;
				if(event.getClickCount() == 2) {
					Getter.getInstance().getControlList().get(selectedBox).alleletext = new StringBuffer("");
					typeTextWidth = 0;
					cursorPosition = 0;
				}
				else {
					typeTextWidth = (int)fm.getStringBounds(Getter.getInstance().getControlList().get(typeBox).alleletext.toString(), buf).getWidth();
					if(event.getX() > 12+typeTextWidth) {
						cursorPosition = Getter.getInstance().getControlList().get(selectedBox).alleletext.length();
						
					}
					else {
						int pos = 0;
						while((int)fm.getStringBounds(Getter.getInstance().getControlList().get(typeBox).alleletext.toString().substring(0,pos), buf).getWidth()+12 <= event.getX()) {
							pos++;
						}				
						cursorPosition = pos;
						typeTextWidth = (int)fm.getStringBounds(Getter.getInstance().getControlList().get(typeBox).alleletext.substring(0, cursorPosition), buf).getWidth();
					}			
				}	
			}
			else {
				typing = false;
			}
			if(this.selectedPlay > -1 && Getter.getInstance().getControlList().get(selectedPlay).playbox.intersects(sideMouseRect)) {
				
				if(!Getter.getInstance().getControlList().get(selectedPlay).controlOn) {
					try {
						Getter.getInstance().getControlList().get(selectedPlay).alleleFreq = Double.parseDouble(Getter.getInstance().getControlList().get(selectedPlay).alleletext.toString());
					}
					catch(Exception e) {
						Getter.getInstance().getControlList().get(selectedPlay).alleletext = new StringBuffer("0");
						Getter.getInstance().getControlList().get(selectedPlay).alleleFreq = 0;
					}
					Getter.getInstance().getControlList().get(selectedPlay).controlOn = true;
					Control.controlData.controlsOn = true;
				
					Control.runner runner = new Control.runner();
					runner.execute();
				}
				else {
					Getter.getInstance().getControlList().get(selectedPlay).controlOn = false;
					boolean allfalse = true;
					for(int i = 0; i<Getter.getInstance().getControlList().size(); i++) {
						if(Getter.getInstance().getControlList().get(i).controlOn) {
							allfalse = false;
							break;
						}
					}
					if(allfalse) {
						Control.controlData.controlsOn = false;
					}
					if(MainPane.projectValues.variantHandlerValues.commonVariantsMin > 1 && VariantHandler.clusterSize > 0) {
						VarMaster.calcClusters(Getter.getInstance().getVariantHead());
					}
				//	Control.useCheck.setSelected(false);
					
					MainPane.drawCanvas.repaint();
				}
				
			}
			if(sidebar && removeControl > -1) {			
				removeControl(removeControl);
			}
			repaint();
			break;
		}
		case InputEvent.BUTTON3_DOWN_MASK: {		
			if(!this.zoomDrag) {
				Getter.getInstance().getControlList().get(hoverIndex).getPopupMenu().show(this, mouseX, mouseY);
			}			
		}
}
	
	
	
}	
public void mouseReleased(MouseEvent event) {
	if(!sidebar) {
		if(zoomDrag) {			
			if(mouseX-MainPane.drawCanvas.pressX > 0) {			
				MainPane.drawCanvas.gotoPos(MainPane.drawCanvas.selectedSplit.start+(MainPane.drawCanvas.pressX-MainPane.drawCanvas.selectedSplit.offset)/MainPane.drawCanvas.selectedSplit.pixel, MainPane.drawCanvas.selectedSplit.start+(mouseX-MainPane.drawCanvas.selectedSplit.offset)/MainPane.drawCanvas.selectedSplit.pixel);
			}
		}
	
		MainPane.chromDraw.updateExons = true;
		repaint();
		MainPane.drawCanvas.mouseDrag = false;
		MainPane.chromDraw.repaint();
		
		MainPane.drawCanvas.repaint();
	}
	for(int i = 0 ; i<MainPane.bedCanvas.bedTrack.size(); i++) {
		if(MainPane.bedCanvas.bedTrack.get(i).graph) {
			MainPane.bedCanvas.calcScale(MainPane.bedCanvas.bedTrack.get(i));			
		}		
	}
	
	mouseDrag = false; 
	zoomDrag = false;	
	MainPane.drawCanvas.lineZoomer = false;
	lineZoomer = false;
}	
@SuppressWarnings("unchecked")
public void mouseDragged(MouseEvent event) {
	
switch(event.getModifiersEx()) {	
	
	case InputEvent.BUTTON1_DOWN_MASK: {	
		/*if(sidebar) {
			return;
		}*/
		this.mouseX = event.getX();
		this.mouseY = event.getY();		
		
		if(resizer && this.getHeight() > this.trackDivider.size()*20) {				
				
				this.trackDivider.set(this.resizeDivider, mouseY/(double)this.getHeight());
			
				if((positivelock || negative) && this.trackDivider.get(this.resizeDivider)*this.getHeight()-preresizer >= 0) {	
					positivelock = false;
					negative = false;
					preresize = mouseY;					
					tempDivider = (ArrayList<Double>)trackDivider.clone();
					
				}
				else if ((!negative || negativelock ) && this.trackDivider.get(this.resizeDivider)*this.getHeight()-preresizer < 0) {					
					negativelock = false;
					preresize = mouseY;					
					tempDivider = (ArrayList<Double>)trackDivider.clone();
					negative = true;
				}
				
				if(negativelock) { // || (this.trackDivider.get(this.resizeDivider+1)*this.getHeight()) -(this.trackDivider.get(this.resizeDivider)*this.getHeight()) < 20) {
					this.trackDivider.set(this.resizeDivider, (this.trackDivider.get(this.resizeDivider+1)*this.getHeight()-19)/(double)this.getHeight());
				}
				if(positivelock) { // || (this.trackDivider.get(this.resizeDivider+1)*this.getHeight()) -(this.trackDivider.get(this.resizeDivider)*this.getHeight()) < 20) {
					this.trackDivider.set(this.resizeDivider, 19/(double)this.getHeight());
				}
				if(this.trackDivider.get(this.resizeDivider)*this.getHeight()-preresizer < 0) {
					
					negative = true;
					positivelock = true;
					if(this.resizeDivider > 0) {
						
						for(int i =1 ; i<this.resizeDivider+1; i++) {								
							if((this.trackDivider.get(i)*this.getHeight())-(this.trackDivider.get(i-1)*this.getHeight()) < 20) {
								
								this.trackDivider.set(i,((this.trackDivider.get(i-1)*this.getHeight())+19)/(double)this.getHeight());	
							}
							else {
								positivelock = false;
								if(i != this.resizeDivider) {
									this.trackDivider.set(i,(this.tempDivider.get(i)/preresize)*mouseY);			
								}
							}
						}						
					}
					if((this.trackDivider.get(0)*this.getHeight()) >= 20) {
						this.trackDivider.set(0,(this.tempDivider.get(0)/preresize)*mouseY);			
						positivelock = false;
					}
					else {
						this.trackDivider.set(0, 19/(double)this.getHeight());
					}
					
				}
				else {
					
					negative = false;
					negativelock = true;
					if(this.resizeDivider < this.trackDivider.size()-1) {						
						
						for(int i =this.resizeDivider ; i<this.trackDivider.size()-1; i++) {
							
							if((this.trackDivider.get(i+1)*this.getHeight())-(this.trackDivider.get(i)*this.getHeight()) < 20) {
								
								this.trackDivider.set(i,((this.trackDivider.get(i+1)*this.getHeight())-19)/(double)this.getHeight());
							}
							else {
								
								negativelock = false;
								if(i !=this.resizeDivider) {
									try {
										this.trackDivider.set(i,1-((1-this.tempDivider.get(i))/(this.getHeight()-preresize))*(this.getHeight()-mouseY));
									}
									catch(Exception e) {
									//	System.out.println(this.tempDivider.get(i));
										e.printStackTrace();
									}
								}
							}							
						}		
						if(this.getHeight()-(this.trackDivider.get(this.trackDivider.size()-2)*this.getHeight()) >= 20) {
							negativelock = false;
						}
						else {
							this.trackDivider.set(this.trackDivider.size()-2, (this.getHeight()-19)/(double)this.getHeight());
						}
						
					}
				}				
				
				preresizer = this.trackDivider.get(this.resizeDivider)*this.getHeight();
				repaint();
				
			
		}
		else if(lineZoomer) {	
			
			 if (MainPane.drawCanvas.selectedSplit.start > 1 || MainPane.drawCanvas.selectedSplit.end < MainPane.drawCanvas.selectedSplit.chromEnd ) {					
				 MainPane.drawCanvas.gotoPos(MainPane.drawCanvas.selectedSplit.start-(MainPane.drawCanvas.tempDrag-mouseX)/MainPane.drawCanvas.selectedSplit.pixel*2, MainPane.drawCanvas.selectedSplit.end+(MainPane.drawCanvas.tempDrag-mouseX)/MainPane.drawCanvas.selectedSplit.pixel*2);
			 }
			
			MainPane.drawCanvas.tempDrag = mouseX;
			MainPane.chromDraw.updateExons = true;
			repaint();
			MainPane.chromDraw.repaint();
			
			MainPane.drawCanvas.repaint();
			
		}
		else {							
			zoomDrag = true;				
			
			repaint();
			return;
		}
		break;
	}
	case InputEvent.BUTTON3_DOWN_MASK: {	
		if((int)MainPane.drawCanvas.selectedSplit.start == 1 && (int)MainPane.drawCanvas.selectedSplit.end == MainPane.drawCanvas.selectedSplit.chromEnd) {
			break;
		}			
	
		MainPane.drawCanvas.mouseDrag = true;
		MainPane.drawCanvas.moveX = event.getX();
		MainPane.drawCanvas.drag(MainPane.drawCanvas.moveX);
		break;
	}
	case 17: {
		if((int)MainPane.drawCanvas.selectedSplit.start == 1 && (int)MainPane.drawCanvas.selectedSplit.end == MainPane.drawCanvas.selectedSplit.chromEnd) {
			break;
		}			
	
		MainPane.drawCanvas.mouseDrag = true;
		MainPane.drawCanvas.moveX = event.getX();
		MainPane.drawCanvas.drag(MainPane.drawCanvas.moveX);
		break;
	}
		
	}
	
}	
public void mouseMoved(MouseEvent event) {
	mouseY = event.getY();
	mouseX = event.getX();
	this.mouseRect.setBounds(event.getX()-MainPane.sidebarWidth, event.getY(), 1, 1);	
	this.sideMouseRect.setBounds(event.getX(), event.getY(), 1, 1);
	
	if(!sidebar && event.getX() < MainPane.sidebarWidth) {
		sidebar = true;
	}
	else {
		if(sidebar && event.getX() >= MainPane.sidebarWidth) {
			sidebar = false;
		}
	}
	for(int i = 0; i<this.trackDivider.size(); i++) {
		if(this.trackDivider.get(i)*this.getHeight() < mouseY) {
			continue;
		}
		if(this.trackDivider.get(i)*this.getHeight() >= mouseY) {			
			if(hoverIndex != i) {
				hoverIndex = i;				
			}
			break;
		}	
	}
	repaint();
}

@Override
public void keyTyped(KeyEvent e) {
	
}

@Override
public void keyPressed(KeyEvent e) {
	if(typing) {
		
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if(cursorPosition < Getter.getInstance().getControlList().get(typeBox).alleletext.length()) {
				cursorPosition++;
			}		
		}
		else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			if(cursorPosition > 0) {
				cursorPosition--;
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			if(cursorPosition > 0) {
				Getter.getInstance().getControlList().get(typeBox).alleletext.deleteCharAt(cursorPosition-1);
				cursorPosition--;
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_DELETE) {
			if(cursorPosition < Getter.getInstance().getControlList().get(typeBox).alleletext.length()) {
				Getter.getInstance().getControlList().get(typeBox).alleletext.deleteCharAt(cursorPosition);
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			try {
				Getter.getInstance().getControlList().get(typeBox).alleleFreq = Double.parseDouble(Getter.getInstance().getControlList().get(typeBox).alleletext.toString());
			
					Getter.getInstance().getControlList().get(typeBox).controlOn = true;
					Control.controlData.controlsOn = true;
					Control.runner runner = new Control.runner();
					runner.execute();
		
			}
			catch(Exception ex) {
				
			}
		}
		else {
			Getter.getInstance().getControlList().get(typeBox).alleletext.insert(cursorPosition, e.getKeyChar());
			cursorPosition++;
		}			
		
		typeTextWidth = (int)fm.getStringBounds(Getter.getInstance().getControlList().get(typeBox).alleletext.substring(0, cursorPosition), buf).getWidth();
		
		repaint();
	
}
	
}


@Override
public void keyReleased(KeyEvent e) {
	
}
	
	
}
