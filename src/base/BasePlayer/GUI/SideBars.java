package base.BasePlayer.GUI;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.sample.Sample;

public class SideBars {
    private static Rectangle remoBox = new Rectangle();
    static boolean sampleInfo = false, bamHover = false ;
    public static Sample removeSample;
    private static void drawSidebarBackground(Graphics2D buf) {
		buf.setColor(Draw.sidecolor);
		buf.fillRect(0, 0, MainPane.sidebarWidth, MainPane.drawScroll.getViewport().getHeight());
		buf.setStroke(BaseConstants.doubleStroke);
		buf.setColor(Color.gray);
		buf.drawLine(MainPane.sidebarWidth - 1, 0, MainPane.sidebarWidth - 1, MainPane.drawScroll.getViewport().getHeight());
		buf.drawLine(1, 0, 1, MainPane.drawScroll.getViewport().getHeight());
		buf.setColor(Color.lightGray);
		buf.drawLine(3, 0, 3, MainPane.drawScroll.getViewport().getHeight());
		buf.drawLine(MainPane.sidebarWidth - 3, 0, MainPane.sidebarWidth - 3, MainPane.drawScroll.getViewport().getHeight());
		buf.setStroke(BaseConstants.basicStroke);
	}

    public static void drawSampleSidebar(Graphics2D buf, int selectedIndex, int scrollValue) {
		
		drawSidebarBackground(buf);
		if(MainPane.samples == 0) return;
		if (Sample.selectedSampleIndex != -1) {
			buf.setColor(Color.white);
			buf.fillRect(0,	(int) (Sample.selectedSampleIndex * Draw.drawVariables.sampleHeight) - scrollValue + 2,	BaseVariables.defaultFontSize * 4, (int) Draw.drawVariables.sampleHeight);
			buf.setColor(Color.black);
			buf.setColor(Color.gray);
			buf.drawLine(0,	(int) ((Sample.selectedSampleIndex + 1) * Draw.drawVariables.sampleHeight)	- scrollValue, MainPane.sidebarWidth - 1, (int) ((Sample.selectedSampleIndex + 1) * Draw.drawVariables.sampleHeight)	- scrollValue);
			buf.setColor(Color.lightGray);
			buf.drawLine(0,	(int) ((Sample.selectedSampleIndex + 1) * Draw.drawVariables.sampleHeight)	- scrollValue + 1,	MainPane.sidebarWidth - 1, (int) ((Sample.selectedSampleIndex + 1) * Draw.drawVariables.sampleHeight) - scrollValue + 1);
			buf.setColor(Color.black);
		}
		//if (sidebar) {
			buf.setColor(Color.white);
			buf.fillRect(0,	(int) (selectedIndex * Draw.drawVariables.sampleHeight)	- scrollValue,	BaseVariables.defaultFontSize * 4, (int) Draw.drawVariables.sampleHeight);
			if (selectedIndex > -1) buf.fillRect(0,	(int) (selectedIndex * Draw.drawVariables.sampleHeight)	- scrollValue,	BaseVariables.defaultFontSize * 4, (int) Draw.drawVariables.sampleHeight);
		//}
		int lettersAmount = (MainPane.sidebarWidth - 53)/buf.getFontMetrics().stringWidth("A");
		
		buf.setColor(Color.black);
		if (Draw.drawVariables.sampleHeight < BaseVariables.defaultFontSize) {
			buf.drawString("Tracks: " + MainPane.samples, 10, BaseVariables.defaultFontSize + 5);
			if (Sample.selectedSample != null) {
				buf.drawString("Selected: " + (Sample.selectedSample.getIndex() + 1) + ". " + Sample.selectedSample.getName().substring(0, Math.min(lettersAmount, Sample.selectedSample.getName().length()-1)), 10, BaseVariables.defaultFontSize * 3);
			}
			if (Sample.hoverSample != null) buf.drawString((selectedIndex + 1) + ". " + Sample.hoverSample.getName().substring(0, Math.min(lettersAmount, Sample.hoverSample.getName().length()-1)), 10,	BaseVariables.defaultFontSize * 5);
		}
		
		sampleInfo = false;
		bamHover = false;
		removeSample = null;
		
		Draw.forEachVisibleSample(sidebarSample -> {
			int sampleYpos = (int) (Draw.drawVariables.sampleHeight * sidebarSample.getIndex())	- scrollValue;
			int bottomYpos = (int) (Draw.drawVariables.sampleHeight * (sidebarSample.getIndex() + 1))	- scrollValue;
			if (sidebarSample.familyColor != null) {
				buf.setColor(sidebarSample.familyColor);
				buf.fillRect(0, sampleYpos + 2, BaseVariables.defaultFontSize * 2, (int) Draw.drawVariables.sampleHeight);
				buf.setColor(Color.black);
			}
			if (sidebarSample.affected != null && sidebarSample.affected) {
				buf.setColor(Color.red);
				buf.fillOval(MainPane.sidebarWidth - BaseVariables.defaultFontSize - 8, sampleYpos + 4, BaseVariables.defaultFontSize,	BaseVariables.defaultFontSize);
				buf.setColor(Color.black);
			}
			if (sidebarSample.annotation) {
				buf.setColor(Color.orange);
				buf.fillOval(MainPane.sidebarWidth - BaseVariables.defaultFontSize - 8, sampleYpos + 4, BaseVariables.defaultFontSize,	BaseVariables.defaultFontSize);
				buf.setColor(Color.black);
			}
			if (MainPane.drawCanvas.sidebar	&& (Sample.selectedSampleIndex == sidebarSample.getIndex() || selectedIndex == sidebarSample.getIndex())) {
				if ((remoBox.getBounds().x != MainPane.sidebarWidth - (BaseVariables.defaultFontSize + 10)	|| remoBox.getBounds().y != sampleYpos + (int) Draw.drawVariables.sampleHeight	- (BaseVariables.defaultFontSize + 6))) {
					remoBox.setBounds(MainPane.sidebarWidth - (BaseVariables.defaultFontSize + 10),	sampleYpos + (int) Draw.drawVariables.sampleHeight - (BaseVariables.defaultFontSize + 6),	BaseVariables.defaultFontSize + 3, BaseVariables.defaultFontSize + 3);
				}

				if (MainPane.drawCanvas.sidebar && MainPane.drawCanvas.mouseRect.intersects(remoBox)) {
					buf.setStroke(BaseConstants.doubleStroke);
					removeSample = sidebarSample;
				}

				buf.setColor(Color.white);
				buf.fillRect(remoBox.x - 2, remoBox.y - 1, remoBox.width + 2,	remoBox.height + 2);
				buf.setColor(Color.black);
				buf.drawRect(remoBox.x, remoBox.y, remoBox.width, remoBox.height);
				buf.drawLine(remoBox.x, remoBox.y, remoBox.x + remoBox.width,	remoBox.y + (int) remoBox.getHeight());
				buf.drawLine(remoBox.x, remoBox.y + (int) remoBox.getHeight(),		remoBox.x + remoBox.width, remoBox.y);
				if (buf.getStroke().equals(BaseConstants.doubleStroke)) buf.setStroke(BaseConstants.basicStroke);
			}
			
			if (Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize) {
				if (MainPane.drawCanvas.sidebar) {
					
					if (MainPane.drawCanvas.moveX > 50 && MainPane.drawCanvas.moveY - scrollValue >= sampleYpos && MainPane.drawCanvas.moveY - scrollValue <= sampleYpos + BaseVariables.defaultFontSize + 2) {
						buf.setFont(MainPane.menuFontBold);
						MainPane.drawCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						sampleInfo = true;
					}
				}
				
				buf.drawString(sidebarSample.getName().substring(0, Math.min(lettersAmount, sidebarSample.getName().length()-1)), 50, sampleYpos + BaseVariables.defaultFontSize);
				buf.drawString("" + (sidebarSample.getIndex() + 1), 10, sampleYpos + BaseVariables.defaultFontSize);
				buf.setColor(Color.gray);
				buf.drawLine(0, bottomYpos, MainPane.sidebarWidth - 1, bottomYpos);
				buf.setColor(Color.lightGray);
				buf.drawLine(0, bottomYpos + 1, MainPane.sidebarWidth - 1, bottomYpos + 1);
				buf.setColor(Color.black);

				if (MainPane.drawCanvas.sidebar) {
					if (sampleInfo) buf.setFont(BaseVariables.defaultFont);
				}
			}
			
			if (sidebarSample.getTabixFile() == null && !sidebarSample.calledvariants && !sidebarSample.multipart	&& Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 2) {
				if (sidebarSample.samFile != null) {
					buf.setColor(ChromDraw.exonBarColor);
					if (sidebarSample.readString != null) {
						buf.drawString(sidebarSample.readString, BaseVariables.defaultFontSize * 5,	(int) (Draw.drawVariables.sampleHeight * sidebarSample.getIndex())	- scrollValue + BaseVariables.defaultFontSize * 2);
					}
				}
				buf.setColor(Color.red);
				buf.drawString("No VCF", 10, sampleYpos + BaseVariables.defaultFontSize * 2);
				buf.setColor(Color.black);
				return;
			}
			if ((sidebarSample.getTabixFile() != null || sidebarSample.calledvariants || sidebarSample.multipart)	&& Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 3) {
				if (sidebarSample.indels == 1) {
					buf.drawString("variants: " + sidebarSample.varcount + " (" + sidebarSample.indels + " indel)",	10, (int) (Draw.drawVariables.sampleHeight * sidebarSample.getIndex())	- scrollValue + BaseVariables.defaultFontSize * 3);
				} else {
					buf.drawString("variants: " + sidebarSample.varcount + " (" + sidebarSample.indels + " indels)",	10, (int) (Draw.drawVariables.sampleHeight * sidebarSample.getIndex())	- scrollValue + BaseVariables.defaultFontSize * 3);
				}
				if (Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 4) {
					buf.drawString("het: " + sidebarSample.heterozygotes, 10,	sampleYpos + BaseVariables.defaultFontSize * 4);
					if (Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 5) {
						buf.drawString("hom: " + sidebarSample.homozygotes, 10,	sampleYpos + BaseVariables.defaultFontSize * 5);
						if (Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 6) {
							buf.drawString("Ts/Tv: " + MethodLibrary.round(sidebarSample.sitioRate / (double) sidebarSample.versioRate, 2),	10, sampleYpos + BaseVariables.defaultFontSize * 6);

							if (Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 7) {
								if (sidebarSample.getTabixFile() != null || sidebarSample.multipart) {
									buf.setColor(ChromDraw.exonBarColor);
									buf.drawString("VCF", 10, sampleYpos + BaseVariables.defaultFontSize * 7);
								} else {
									buf.setColor(Color.red);
									buf.drawString("No VCF", 10, sampleYpos + BaseVariables.defaultFontSize * 7);
								}
								if (sidebarSample.samFile != null) {
									buf.setColor(ChromDraw.exonBarColor);
									if (sidebarSample.readString != null) {
										buf.drawString(sidebarSample.readString, BaseVariables.defaultFontSize * 4,	sampleYpos + BaseVariables.defaultFontSize * 7);
									}
								} else {
									buf.setColor(Color.red);
									if (!FileRead.searchingBams && sidebarSample.readString != null) {
										if (MainPane.drawCanvas.sidebar) {										
											if (MainPane.drawCanvas.moveY - scrollValue >= sampleYpos	+ BaseVariables.defaultFontSize * 6	&& MainPane.drawCanvas.moveY - MainPane.drawScroll.getVerticalScrollBar().getValue() <= sampleYpos + BaseVariables.defaultFontSize * 6	+ BaseVariables.defaultFontSize + 2) {
												if (MainPane.drawCanvas.moveX >= BaseVariables.defaultFontSize * 4) {
													buf.setFont(MainPane.menuFontBold);
													MainPane.drawCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
													bamHover = true;
												}
											}
										}
										buf.drawString(sidebarSample.readString, BaseVariables.defaultFontSize * 4,	sampleYpos + BaseVariables.defaultFontSize * 7);
										if (MainPane.drawCanvas.sidebar) {
											if (bamHover) buf.setFont(BaseVariables.defaultFont);
										}
									} else {
										buf.drawString("Searching reads...", BaseVariables.defaultFontSize * 4,	sampleYpos + BaseVariables.defaultFontSize * 7);
									}
								}
								buf.setColor(Color.black);
								if (Draw.drawVariables.sampleHeight > BaseVariables.defaultFontSize * 8) {
									buf.drawString("Var height by " + Settings.varDrawList.getSelectedItem().toString()	+ " (max " + (int) sidebarSample.getMaxCoverage() + ")",	10, sampleYpos + BaseVariables.defaultFontSize * 8);
								}
							}
						}
					}
				}
			}
		});
		
		if (MainPane.drawCanvas.sidebar && !sampleInfo && !bamHover && MainPane.drawCanvas.moveX < MainPane.sidebarWidth - 6) MainPane.drawCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));	
		
	}
}
