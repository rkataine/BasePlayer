/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI.modals;
import htsjdk.samtools.util.BlockCompressedOutputStream;
import htsjdk.tribble.Feature;
import htsjdk.tribble.index.Index;
import htsjdk.tribble.index.tabix.TabixFormat;
import htsjdk.tribble.index.tabix.TabixIndexCreator;
import htsjdk.variant.vcf.VCFHeader;
import htsjdk.variant.vcf.VCFHeaderLine;
import htsjdk.variant.vcf.VCFHeaderVersion;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.RangeSlider;
import base.BasePlayer.GUI.listeners.VariantHandlerListeners;
import base.BasePlayer.control.Control;
import base.BasePlayer.control.ControlFile;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.io.FileWrite;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.variants.OwnVCFCodec;
import base.BasePlayer.variants.QualEntry;
import base.BasePlayer.variants.VarMaster;
import base.BasePlayer.variants.VarNode;
import base.BasePlayer.variants.VariantCaller;
import base.BasePlayer.variants.VcfReader;

public class VariantHandler extends JPanel implements ActionListener, MouseListener, KeyListener, ComponentListener {
	public static RangeSlider commonSlider = new RangeSlider(1,1);
	public static RangeSlider callSlider = new RangeSlider(0,100);
	
	public static JSlider readSlider = new JSlider(1,10);
	public static JSlider qualitySlider = new JSlider(0,60);
	public static JSlider gqSlider = new JSlider(0,60);
	public static JSlider coverageSlider = new JSlider(1,40);
	public static JSlider maxCoverageSlider = new JSlider(1,2000);
//	public static JSlider callSlider = new JSlider(0,100);
	public static JSlider geneSlider = new JSlider(1,1);
	public static JLabel geneLabel = new JLabel("At least 1/1 samples share a mutated gene");	
	public static JLabel aminoCount = new JLabel("");
	public static JLabel callsLabel = new JLabel();
	public static JLabel readsLabel = new JLabel();
	public static JLabel coverageLabel = new JLabel();
	public static JLabel maxCoverageLabel = new JLabel();
	public static JLabel qualityLabel = new JLabel(), gqLabel = new JLabel(), comparison = new JLabel("Sample comparison");
	public static JLabel slideLabel = new JLabel("Shared variants in 1/1 samples");	
	public static JLabel clusterLabel = new JLabel("Window size for variant clusters");	
	
	public static JMenu filters;	
	public static JMenuBar aminobar;
	public static JMenu aminomenu;
	public static JMenu outputmenu;
	public static JMenuItem write;
	//public static JButton hideVars;
	public static JTextField clusterBox;
	public static ButtonGroup outputgroup;
	public static JRadioButton tsv;
	public static JRadioButton geneTsv;
	public static JRadioButton compactTsv;
	public static JRadioButton vcf;
	public static JRadioButton oncodrive;
	public static JCheckBox hidenoncoding;
	public static JCheckBox freeze;
	public static JCheckBox rscode;
	public static JCheckBox allChroms;
	public static JCheckBox allChromsfrom;
	public static JCheckBox onlyAutosomes;
	public static JCheckBox hideSNVs;
	public static JCheckBox hideHomos;
	public static JCheckBox onlyStats;
	public static JCheckBox outputContexts;
	public static JCheckBox hideIndels;
	public static JCheckBox synonymous;
	public static JCheckBox nonsense;
	public static JCheckBox windowcalc;
	public static JCheckBox intronic;
	public static JCheckBox intergenic;
	public static JCheckBox utr;
	public static JCheckBox onlyselected;
	public static JCheckBox writetofile;

	public static JLabel SNVFilters;
	public static JFrame frame;    
	public static JLabel empty;	
	public static JButton varcalc;
	public static JButton statcalc;	
	public static ArrayList<ControlFile> controlarray = new ArrayList<ControlFile>();
	String userDir;
	public static HashMap<String, Integer> variantSettings;
	public static int lastWrittenPos = 0;
	public static  ArrayList<String> outputStrings = new ArrayList<String>();
	public static MouseWheelListener sliderWheelListener;
	public static JTabbedPane tabs;
	public static JScrollPane tableScroll = new JScrollPane();
	public static JSeparator separator;
	public static AminoTable table;
	public static StatsTable stattable;
	public static JScrollPane statsScroll = new JScrollPane();
	public static ArrayList<JScrollPane> tablescrolls = new ArrayList<JScrollPane>();
	public static ArrayList<BedTable> tables = new ArrayList<BedTable>();
	public static NodeSorter nodesorter = new NodeSorter();
	public static JPopupMenu menu;
	public static JScrollPane menuScroll;
	public static JPanel menuPanel;
	public static JButton applyQualities;
	public static JButton advQualities;
	public static OwnVCFCodec vcfCodec= new OwnVCFCodec();
	public static String format = "GT:DP:AD:GQ";
	public static JRadioButton none = new JRadioButton("None");
	public static JRadioButton recessiveHomo = new JRadioButton("Homozygous Recessive");
	public static JRadioButton dominant = new JRadioButton("Autosomal Dominant");
	public static JRadioButton denovo = new JRadioButton("De Novo");
	public static JRadioButton xLinked = new JRadioButton("X-Linked Recessive");
	public static JRadioButton compound = new JRadioButton("Compound Heterozygous");
	public static JRadioButton recessive = new JRadioButton("Recessive");
	ButtonGroup inheritgroup = new ButtonGroup();
    
	int moveX=0, moveY=0, pressX=0,pressY=0;
	public static JLabel adder, adder2, adder3, adder4, adder5;
	//final int buttonHeight = 15, buttonWidth = 40;
	int buttonHeight = (int)(11);
	int buttonWidth = 12*6;
	public static float maxSlideValue = 0;
	Dimension buttondimension = new Dimension(buttonWidth, buttonHeight);		
	//final Dimension buttondimension = new Dimension(MainPane.buttonWidth, MainPane.buttonHeight);
	public static Color backColor = new Color(228,228,218,255);
	public static Color frameColor = new Color(188,188,178,255);
	public static int clusterSize = 0;
	public static JTabbedPane filterPanes = new JTabbedPane();
	public static JPanel filterpanel;
	public static JPanel hidepanel;
	public static JPanel inheritpanel;
	public static JPanel comparepanel;	
	public static JPanel aminopanel;

	public static ArrayList<QualEntry> advQDraw = null;
	
	public VariantHandler() {	
		super(new GridBagLayout());		 
		
		sliderWheelListener = new MouseWheelListener() {
		    @Override
		    public void mouseWheelMoved(final MouseWheelEvent e) {
				final int notches = e.getWheelRotation();
				final JSlider slider = (JSlider) e.getSource();
				if (notches < 0) {

					slider.setValue(slider.getValue() + 1);
				} else if (notches > 0) {
					slider.setValue(slider.getValue() - 1);
				}
			}
		};
		createButtons();
		final GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTHWEST;

		c.insets = new Insets(0, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		userDir = new File(MainPane.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent()
				.replace("%20", " ");

		this.setOpaque(false);
		if (MainPane.screenSize == null) {
			MainPane.screenSize = new Dimension(1000, 1000);
		}

		stattable = new StatsTable((int) MainPane.screenSize.width, (int) MainPane.screenSize.height, statsScroll);
		stattable.setEnabled(true);
		table = new AminoTable((int) MainPane.screenSize.width, (int) MainPane.screenSize.height, tableScroll);
		table.setEnabled(true);		
		tabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		filterPanes.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		aminopanel.setLayout(new GridLayout(1, 3));
		tableScroll.getViewport().add(table);
		tableScroll.getVerticalScrollBar().addAdjustmentListener(

				new AdjustmentListener() {

					public void adjustmentValueChanged(final AdjustmentEvent event) {

						table.repaint();

					}
				});

		tableScroll.getVerticalScrollBar().setUnitIncrement(16);
		statsScroll.setPreferredSize(new Dimension(500, 400));
		statsScroll.getViewport().add(stattable);
		statsScroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {

			public void adjustmentValueChanged(final AdjustmentEvent event) {

				stattable.repaint();

			}
		});
		
		filterPanes.addKeyListener(this);
		// add(menubar,c);
		c.gridy = 1;
		filterPanes.add("Variants", filterpanel);
	
		filterPanes.add("Hide", hidepanel);
		filterPanes.add("Compare", comparepanel);
		filterPanes.add("Inheritance", inheritpanel);
		add(filterPanes, c);
		c.gridy = 2;
		// add(comparepanel,c);
		// c.gridy = 3;
		c.fill = GridBagConstraints.HORIZONTAL;
		add(aminopanel, c);
		c.gridy = 3;
		c.weightx = 1;
		c.weighty = 0.8;
		tabs.addMouseListener(this);
		tabs.setBackground(backColor);
		tabs.add("Genes", tableScroll);
		tabs.add("Stats", statsScroll);

		// tabs.add("Clusters", clusterScroll);
		c.fill = GridBagConstraints.BOTH;

		add(tabs, c);
		if (MainPane.menuFont == null) {
			MainPane.menuFont = new Font("SansSerif", Font.PLAIN, 12);
		}
		initMenu();
		setFonts(MainPane.menuFont);

	}
	private static void initMenu() {
		final GridBagConstraints constr = new GridBagConstraints();
		
		menuPanel.add(new JLabel("Hard filters"), constr);
		menuPanel.add(new JSeparator());
		JTextField value1 = new JTextField("AC");
		value1.setName("value1");
		constr.gridy = 1; menuPanel.add(value1, constr);
		constr.gridx = 1; menuPanel.add(new JLabel("/"), constr);
		JTextField value2 = new JTextField("AN");
		value2.setName("value2");
		constr.gridx = 2; menuPanel.add(value2, constr);
		JTextField value3 = new JTextField(">=");
		value3.setName("value3");
		constr.gridx = 3; menuPanel.add(value3, constr);

	}
	protected void paintComponent(final Graphics g) {
		super.paintComponent(g);

		// g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);
		g.setColor(frameColor);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	void createButtons() {
		freeze = new JCheckBox("Freeze filters");
		freeze.addActionListener(e -> freezeFilters(freeze.isSelected()));
		commonSlider = new RangeSlider(1, 1);
		callSlider = new RangeSlider(0, 100);
		readSlider = new JSlider(1, 10);
		qualitySlider = new JSlider(0, 60);
		gqSlider = new JSlider(0, 60);
		coverageSlider = new JSlider(1, 40);
		maxCoverageSlider = new JSlider(1, 2000);
		geneSlider = new JSlider(1, 1);
		geneLabel = new JLabel("At least 1/1 samples share a mutated gene");
		aminoCount = new JLabel("");
		callsLabel = new JLabel();
		readsLabel = new JLabel();
		coverageLabel = new JLabel();
		maxCoverageLabel = new JLabel();
		qualityLabel = new JLabel();
		gqLabel = new JLabel();
		comparison = new JLabel("Sample comparison");
		slideLabel = new JLabel("Shared variants in 1/1 samples");
		clusterLabel = new JLabel("Window size for variant clusters");
		filters = new JMenu("Variant Filters");
		aminobar = new JMenuBar();
		aminomenu = new JMenu("Options");
		outputmenu = new JMenu("Variant output");
		write = new JMenuItem("Save");
		// hideVars = new JButton("Hide var types");
		// hideVars.addActionListener(this);
		clusterBox = new JTextField("0");
		outputgroup = new ButtonGroup();
		tsv = new JRadioButton("TSV");
		compactTsv = new JRadioButton("Compact TSV");
		geneTsv = new JRadioButton("Gene TSV");
		vcf = new JRadioButton("VCF");
		oncodrive = new JRadioButton("Oncodrive");
		hidenoncoding = new JCheckBox("Hide non-coding variants");

		rscode = new JCheckBox("Hide rs-coded variants");
		allChroms = new JCheckBox("All chromosomes");
		allChromsfrom = new JCheckBox("From this chr?");
		onlyAutosomes = new JCheckBox("Only autosomes?");
		hideSNVs = new JCheckBox("Hide SNVs");
		hideHomos = new JCheckBox("Hide homozygotes");
		onlyStats = new JCheckBox("Only stats");
		outputContexts = new JCheckBox("Output contexts");
		hideIndels = new JCheckBox("Hide indels");
		synonymous = new JCheckBox("Only non-synonymous");
		nonsense = new JCheckBox("Only truncs");
		windowcalc = new JCheckBox("Window calculation");
		intronic = new JCheckBox("Show intronic");
		intergenic = new JCheckBox("Show intergenic");
		utr = new JCheckBox("Show UTR");
		onlyselected = new JCheckBox("Only selected sample");
		writetofile = new JCheckBox("Write directly to a file");
		
		SNVFilters = new JLabel("SNV & indel filters");
		SNVFilters.setName("header");
		comparison.setName("header");
		empty = new JLabel("");
		varcalc = new JButton("Annotate");
		statcalc = new JButton("Stats");
		tabs = new JTabbedPane();

		separator = new JSeparator();
		menu = new JPopupMenu("Advanced quality control");
		menuPanel = new JPanel(new GridBagLayout());
		applyQualities = new JButton("Apply");
		advQualities = new JButton("Hard filters");

		filterPanes = new JTabbedPane();
		filterpanel = new JPanel(new GridBagLayout()) {
		
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);

				g.setColor(backColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());

			}
		};

		hidepanel = new JPanel(new GridBagLayout()) {
		
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);

				g.setColor(backColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());

			}
		};
		inheritpanel = new JPanel(new GridBagLayout()) {
		
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);

				g.setColor(backColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());

			}
		};
		comparepanel = new JPanel(new GridBagLayout()) {

		
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);

				g.setColor(backColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
			}
		};

		aminopanel = new JPanel() {

		
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);

				g.setColor(backColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());

			}
		};
		setSliders();
		
		hideSNVs.setOpaque(false);
		hideSNVs.addActionListener(this);
		hideIndels.setOpaque(false);
		hideIndels.addActionListener(this);
		hideHomos.setOpaque(false);
		hideHomos.addActionListener(this);
		rscode.addActionListener(this);
		rscode.setOpaque(false);
		hidenoncoding.addActionListener(this);
		hidenoncoding.setOpaque(false);
		frame.getContentPane().setBackground(Color.white);
		frame.setBackground(Color.white);
		frame.addComponentListener(this);

		menuScroll = new JScrollPane(menuPanel);

		// menuScroll.setMinimumSize(new Dimension(200, 100));
		menuPanel.setBackground(Color.white);

		menu.addKeyListener(this);
		menuScroll.addKeyListener(this);
		// menuScroll.setMaximumSize(new Dimension(250,500));
		menu.add(menuScroll);
		menu.add(applyQualities);

		applyQualities.addActionListener(this);
		advQualities.addActionListener(this);
		qualityLabel.setToolTipText("Variants below quality threshold will be hidden");
		gqLabel.setToolTipText("Variants below quality threshold will be hidden");
		final GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(2, 4, 0, 4);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0;
		c.weighty = 0;
		c.gridwidth = 1;
		filterpanel.add(SNVFilters, c);
		qualityLabel.setToolTipText("Click for hard filters (advanced)");
		qualityLabel.addMouseListener(this);
		c.gridx = 1;
		adder2 = new JLabel("__________________________________");
		adder2.setForeground(Draw.sidecolor);
		filterpanel.add(adder2, c);
		
		c.gridy++;
		c.gridx = 0;
		filterpanel.add(qualityLabel, c);
		c.gridx = 1;
		filterpanel.add(qualitySlider, c);
		c.gridy++;
		c.gridx = 0;
		filterpanel.add(gqLabel, c);
		c.gridx = 1;
		filterpanel.add(gqSlider, c);
		// filterpanel.add(advQualities);
		// filterpanel.add(new JSeparator());
		c.gridy++;
		c.gridx = 0;
		filterpanel.add(coverageLabel, c);
		c.gridx = 1;
		filterpanel.add(coverageSlider, c);
		c.gridy++;
		c.gridx = 0;
		filterpanel.add(maxCoverageLabel, c);
		c.gridx = 1;
		filterpanel.add(maxCoverageSlider, c);
		c.gridy++;
		c.gridx = 0;

		filterpanel.add(readsLabel, c);
		c.gridx = 1;
		filterpanel.add(readSlider, c);
		c.gridy++;
		c.gridx = 0;
		filterpanel.add(callsLabel, c);
		c.gridx = 1;
		filterpanel.add(callSlider, c);
		c.gridy++;
		c.gridx = 0;
		adder = new JLabel("__________________________________");
		adder.setForeground(Draw.sidecolor);
		filterpanel.add(adder, c);
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		filterpanel.add(new JLabel(), c);

		adder3 = new JLabel("__________________________________");
		adder3.setForeground(Draw.sidecolor);

		adder4 = new JLabel("__________________________________");
		adder4.setForeground(Draw.sidecolor);
		
		c.insets = new Insets(0, 4, 0, 4);

		// gb.anchor = GridBagConstraints.WEST;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		hidepanel.add(hidenoncoding, c);
		c.gridy++;
		hidepanel.add(rscode, c);
		c.gridy++;
		hidepanel.add(hideSNVs, c);
		c.gridy++;
		hidepanel.add(hideIndels, c);
		c.gridy++;
		hidepanel.add(hideHomos, c);
		c.gridy++;
		// hidepanel.add(hideVars,c);
		// c.gridx = 1;
		freeze.setBackground(new Color(170, 220, 255));
		hidepanel.add(freeze, c);

		c.gridx = 0;
		c.gridy++;
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		hidepanel.add(new JLabel(), c);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		none.setSelected(true);
		inheritgroup.add(none);
		inheritgroup.add(recessiveHomo);
		inheritgroup.add(dominant);
		inheritgroup.add(denovo);
		inheritgroup.add(xLinked);
		inheritgroup.add(recessive);
		inheritgroup.add(compound);
		none.setOpaque(false);
		recessiveHomo.setOpaque(false);
		dominant.setOpaque(false);
		denovo.setOpaque(false);
		xLinked.setOpaque(false);
		recessive.setOpaque(false);
		compound.setOpaque(false);
		
		inheritpanel.add(none, c);
		c.gridy++;
		inheritpanel.add(dominant, c);
		c.gridy++;

		inheritpanel.add(recessive, c);
		c.gridy++;
		inheritpanel.add(recessiveHomo, c);
		c.gridy++;
		inheritpanel.add(compound, c);

		c.gridy = 0;
		c.gridx = 1;

		inheritpanel.add(xLinked, c);
		c.gridy++;
		inheritpanel.add(denovo, c);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.insets = new Insets(2, 4, 0, 4);
		c.weightx = 0;
		c.weighty = 0;
		comparepanel.add(comparison, c);
		c.gridx = 1;
		
		c.gridy++;
		c.gridx = 0;
		// comparepanel.add(new JLabel(""));

		comparepanel.add(geneLabel, c);
		// adder5 = new JLabel("__________________________________________");
		// adder5.setForeground(Draw.sidecolor);

		c.gridx = 1;
		comparepanel.add(geneSlider, c);
		c.gridx = 0;
		c.gridy++;
		comparepanel.add(slideLabel, c);
		c.gridx = 1;
		comparepanel.add(commonSlider, c);
		c.gridx = 0;
		c.gridy++;
		comparepanel.add(clusterLabel, c);
		c.gridx = 1;
		clusterBox.addKeyListener(this);
		comparepanel.add(clusterBox, c);
		c.gridy++;
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		comparepanel.add(new JLabel(), c);
		varcalc.addActionListener(this);
		// varcalc.setPreferredSize(buttondimension);
		allChroms.addActionListener(this);
		write.addActionListener(this);
		aminomenu.add(synonymous);
		aminomenu.add(nonsense);
		aminomenu.add(intronic);
		aminomenu.add(intergenic);
		aminomenu.add(utr);

		aminomenu.add(allChroms);
		aminomenu.add(onlyselected);
		onlyStats.addActionListener(this);
		aminomenu.add(onlyStats);
		aminomenu.add(windowcalc);
		aminomenu.add(writetofile);
		aminomenu.setIcon(MenuBar.settingsIcon);
		writetofile.addActionListener(this);
		writetofile.setIcon(MenuBar.saveIcon);
		outputmenu.setIcon(MenuBar.saveIcon);
		aminopanel.add(aminobar);
		aminopanel.add(aminoCount);
		aminobar.add(varcalc);
		aminobar.add(aminomenu);
		tsv.setSelected(true);
		outputgroup.add(tsv);
		outputgroup.add(compactTsv);
		outputgroup.add(geneTsv);
		outputgroup.add(vcf);
		outputgroup.add(oncodrive);
		outputmenu.add(tsv);
		outputmenu.add(compactTsv);
		outputmenu.add(vcf);
		outputmenu.add(geneTsv);
		outputmenu.add(oncodrive);
		// outputmenu.add(forVEP);
		write.setIcon(MenuBar.saveIcon);
		outputmenu.add(write);
		aminobar.add(outputmenu);
		setValues();
		
		// setFonts();
		// add(table);
	}
	private void setSliders() {		
		qualitySlider.addMouseWheelListener(sliderWheelListener);
		qualitySlider.addChangeListener(VariantHandlerListeners::performQualitySliderChange);
		qualitySlider.addMouseListener(this);
		
		qualitySlider.setValue(0);
		qualitySlider.setOpaque(false);
		gqSlider.addMouseWheelListener(sliderWheelListener);
		gqSlider.addChangeListener(VariantHandlerListeners::performGQSliderChange);
		gqSlider.addMouseListener(this);
		
		gqSlider.setValue(0);
		gqSlider.setOpaque(false);
		coverageSlider.addMouseWheelListener(sliderWheelListener);
		coverageSlider.addChangeListener(VariantHandlerListeners::performCoverageSliderChange);
		coverageSlider.addMouseListener(this);
		coverageSlider.setOpaque(false);
		coverageSlider.setValue(4);
		maxCoverageSlider.addMouseWheelListener(sliderWheelListener);
		maxCoverageSlider.addChangeListener(VariantHandlerListeners::performMaxCoverageSliderChange);
		maxCoverageSlider.addMouseListener(this);
		maxCoverageSlider.setOpaque(false);
		maxCoverageSlider.setValue(1500);
		callSlider.addMouseWheelListener(sliderWheelListener);
		callSlider.addChangeListener(VariantHandlerListeners::performCallSliderChange);
		callSlider.addMouseListener(this);
		callSlider.setValue(10);
		callSlider.setUpperValue(100);
		callSlider.setOpaque(false);

		readSlider.addChangeListener(VariantHandlerListeners::performReadSliderChange);
		readSlider.addMouseListener(this);
		readSlider.setValue(1);
		readSlider.setOpaque(false);

		geneSlider.setValue(1);
		geneSlider.setSnapToTicks(true);
		geneSlider.setMajorTickSpacing(1);
		geneSlider.setMinorTickSpacing(1);
		geneSlider.setOpaque(false);
		geneSlider.addChangeListener(VariantHandlerListeners::performGeneSliderChange);

		geneSlider.addMouseWheelListener(sliderWheelListener);
		commonSlider.addMouseWheelListener(sliderWheelListener);
		
		commonSlider.setValue(1);
		commonSlider.setUpperValue(1);
		commonSlider.addChangeListener(VariantHandlerListeners::performCommonSliderChange);
		commonSlider.addMouseListener(this);
		commonSlider.setOpaque(false);
	}
	public static void loadValues() {
		//variant tab
		VariantHandler.qualitySlider.setValue(MainPane.projectValues.variantHandlerValues.variantQuality);
		VariantHandler.gqSlider.setValue(MainPane.projectValues.variantHandlerValues.variantGQ);

		VariantHandler.coverageSlider.setValue(variantSettings.get("mincoverage"));
		VariantHandler.maxCoverageSlider.setValue(variantSettings.get("maxcoverage"));
		VariantHandler.callSlider.setValue(variantSettings.get("fraction"));

		VariantHandler.callSlider.setUpperValue(variantSettings.get("upperFraction"));

		VariantHandler.maxSlideValue = MainPane.projectValues.variantHandlerValues.variantMaxCoverage;
		//compare tab
		VariantHandler.geneSlider.setMinimum(MainPane.projectValues.variantHandlerValues.compareGene);
		VariantHandler.geneSlider.setMaximum(Math.max(MainPane.projectValues.samples.size(), 1));
		VariantHandler.commonSlider.setMinimum(MainPane.projectValues.variantHandlerValues.commonVariantsMin);
		VariantHandler.commonSlider.setMaximum(Math.max(MainPane.projectValues.variantHandlerValues.commonVariantsMax, 1));
			
		VariantHandler.clusterSize = MainPane.projectValues.variantHandlerValues.clusterSize;
		VariantHandler.clusterBox.setText("" + VariantHandler.clusterSize);
		/* 
		VariantHandler.hidenoncoding.setSelected(variantSettings.get("hideNoncoding") == 1);
		VariantHandler.rscode.setSelected(variantSettings.get("hideRscoded") == 1);
		VariantHandler.hideSNVs.setSelected(variantSettings.get("hideSNVs") == 1);
		VariantHandler.hideIndels.setSelected(variantSettings.get("hideIndels") == 1);
		VariantHandler.hideHomos.setSelected(variantSettings.get("hideHomozygotes") == 1);
		VariantHandler.freeze.setSelected(variantSettings.get("freeze") == 1);

		VariantHandler.synonymous.setSelected(variantSettings.get("nonSyn") == 1);
		VariantHandler.nonsense.setSelected(variantSettings.get("truncs") == 1);
		if (variantSettings.get("windowcalc") != null) {
			VariantHandler.windowcalc.setSelected(variantSettings.get("windowcalc") == 1);
		}
		VariantHandler.intronic.setSelected(variantSettings.get("intronic") == 1);
		VariantHandler.intergenic.setSelected(variantSettings.get("intergenic") == 1);
		VariantHandler.utr.setSelected(variantSettings.get("utr") == 1);
		VariantHandler.allChroms.setSelected(variantSettings.get("allChroms") == 1);
		VariantHandler.onlyselected.setSelected(variantSettings.get("onlySel") == 1);
		VariantHandler.onlyStats.setSelected(variantSettings.get("onlyStats") == 1);
		if (variantSettings.get("contexts") != null) {
			VariantHandler.outputContexts.setSelected(variantSettings.get("contexts") == 1);
		}
		VariantHandler.writetofile.setSelected(variantSettings.get("writeFile") == 1);
		checkWriteFiles();

		VariantHandler.tsv.setSelected(variantSettings.get("tsv") == 1);
		VariantHandler.compactTsv.setSelected(variantSettings.get("compact") == 1);
		VariantHandler.vcf.setSelected(variantSettings.get("vcf") == 1);
		if (variantSettings.get("genetsv") != null) {
			VariantHandler.geneTsv.setSelected(variantSettings.get("genetsv") == 1);
		}
		VariantHandler.oncodrive.setSelected(variantSettings.get("oncodrive") == 1); */

	}


	public static void createAndShowGUI() {
		try {

			frame = new JFrame("Variant Manager");
			frame.setResizable(true);
			final JComponent newContentPane = new VariantHandler();
			newContentPane.setOpaque(true);
			frame.setContentPane(newContentPane);
			frame.pack();
			VariantHandler.filterPanes.setMinimumSize(filterPanes.getSize());
			table.setPreferredSize(
					new Dimension(tableScroll.getViewport().getWidth(), tableScroll.getViewport().getHeight()));
			table.setMinimumSize(
					new Dimension(tableScroll.getViewport().getWidth(), tableScroll.getViewport().getHeight()));
			table.resizeTable(tableScroll.getViewport().getWidth());
			aminobar.setMinimumSize(
					new Dimension((int) aminobar.getSize().getWidth(), (int) aminobar.getSize().getHeight()));
			// frame.setAlwaysOnTop(true);
			filters.setMinimumSize(
					new Dimension((int) filters.getSize().getWidth(), (int) filters.getSize().getHeight()));
						
			if (MainPane.chromDraw == null) {
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				String add = "##INFO=<ID=ABHom,Number=1,Type=Float,Description=\"Allele Balance for homs (A/(A+O))\">";
				addMenuComponents(add);
				add = "##INFO=<ID=AC,Number=A,Type=Integer,Description=\"Allele count in genotypes, for each ALT allele, in the same order as listed\">";
				addMenuComponents(add);

			} else {
				frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				frame.setVisible(false);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(final String[] args) {
		try {
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			System.setProperty("sun.java2d.d3d", "false");
		} catch (final Exception e) {
			e.printStackTrace();
		}
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				createAndShowGUI();

			}
		});
	}

	public static class NodeSorter implements Comparator<VarNode> {

		public int compare(final VarNode o1, final VarNode o2) {

			if (o1.getPosition() <= o2.getPosition()) {
				return -1;
			} else {
				return 1;
			}
		}
	}

	public static void checkWriteFiles() {
		if (writetofile.isSelected()) {
			writetofile.setBackground(Color.white);
			// tabs.setEnabled(false);
			aminomenu.add(tsv, aminomenu.getItemCount() - 1);
			aminomenu.add(compactTsv, aminomenu.getItemCount() - 1);
			aminomenu.add(vcf, aminomenu.getItemCount() - 1);
			aminomenu.add(geneTsv, aminomenu.getItemCount() - 1);
			aminomenu.add(oncodrive, aminomenu.getItemCount() - 1);
			aminomenu.getPopupMenu().pack();
			// varcalc.setText("Annotate and write");
			aminomenu.revalidate();
			aminomenu.repaint();
			tabs.revalidate();
		} else {
			writetofile.setBackground(Color.lightGray);
			// tabs.setEnabled(true);
			outputmenu.add(oncodrive, 0);
			outputmenu.add(geneTsv, 0);
			outputmenu.add(vcf, 0);
			outputmenu.add(compactTsv, 0);
			outputmenu.add(tsv, 0);
			outputmenu.revalidate();
			aminomenu.getPopupMenu().pack();
			// varcalc.setText("Annotate");
			tabs.revalidate();
		}
	}

	@Override
	public void actionPerformed(final ActionEvent event) {

		if (event.getSource() == writetofile) {
			checkWriteFiles();
		} else if (event.getSource() == hidenoncoding) {
			if (commonSlider.getValue() >= MainPane.varsamples) {

				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
			
			MainPane.drawCanvas.repaint();

			if (Draw.getSplits().get(0).viewLength <= 1000000) {
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			}
		}	else if (event.getSource() == varcalc) {
			if (VariantHandler.onlyStats.isSelected()) {
				if (VariantHandler.outputContexts.isSelected()) {
					try {

						final FileDialog fs = new FileDialog(frame, "Save output as...", FileDialog.SAVE);
						fs.setDirectory(MainPane.savedir);
						fs.setFile("*.tsv");
						fs.setVisible(true);

						final String filename = fs.getFile();

						if (filename != null) {
							final File outfname = new File(fs.getDirectory() + "/" + filename);
							BufferedWriter output = null, sigOutput = null;
							MainPane.savedir = fs.getDirectory();
							MainPane.writeToConfig("DefaultSaveDir=" + MainPane.savedir);
							lastWrittenPos = 0;
							if (tsv.isSelected() || compactTsv.isSelected() || oncodrive.isSelected() || geneTsv.isSelected()) {
								if (!outfname.getName().contains(".tsv")) {
									final File outfile = new File(outfname.getCanonicalPath() + ".tsv");
									FileRead.outputName = outfname.getCanonicalPath() + ".tsv";
									output = new BufferedWriter(new FileWriter(outfile));
								} else {
									final File outfile = outfname;
									FileRead.outputName = outfname.getCanonicalPath();
									output = new BufferedWriter(new FileWriter(outfile));
								}

								if (output != null) {
									sigOutput = new BufferedWriter(new FileWriter(outfname.getCanonicalPath() + "_contexts.tsv"));
									final String header = createTSVHeader();
									output.write(header);
									FileWrite.output = output;
									FileWrite.sigOutput = sigOutput;

								}
							}
							VariantHandler.table.clear();
							VariantHandler.table.headerHover = 2;
							VariantHandler.table.sorter.ascending = true;
							VariantHandler.table.createPolygon();
							VariantHandler.table.repaint();

							final FileRead calculator = new FileRead();
							calculator.varcalc = true;
							calculator.execute();

						}

					
					} catch (final Exception e) {
						JOptionPane.showMessageDialog(MainPane.chromDraw, e.getMessage(), "Error",
								JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
				} else {
					VariantHandler.table.clear();
					VariantHandler.table.headerHover = 2;
					VariantHandler.table.sorter.ascending = true;
					VariantHandler.table.createPolygon();
					VariantHandler.table.repaint();

					final FileRead calculator = new FileRead();
					calculator.varcalc = true;
					calculator.execute();
				}
			} else if (writetofile.isSelected()) {
				try {
				
					final FileDialog fs = new FileDialog(frame, "Save output file to...", FileDialog.SAVE);
					fs.setDirectory(MainPane.savedir);
					fs.setFile("*.tsv");
					fs.setVisible(true);
					String outfname = "";
					final String filename = fs.getFile();

					if (filename != null) {

						outfname = fs.getDirectory() + "/" + filename;
						BufferedWriter output = null;
						MainPane.savedir = fs.getDirectory();
						MainPane.writeToConfig("DefaultSaveDir=" + MainPane.savedir);
						lastWrittenPos = 0;
						if (tsv.isSelected() || compactTsv.isSelected() || oncodrive.isSelected()
								|| geneTsv.isSelected()) {
							if (!outfname.contains(".tsv")) {
								final File outfile = new File(outfname + ".tsv");
								FileRead.outputName = outfname + ".tsv";
								output = new BufferedWriter(new FileWriter(outfile));
							} else {
								final File outfile = new File(outfname);
								FileRead.outputName = outfname;
								output = new BufferedWriter(new FileWriter(outfile));
							}

							if (output != null) {
								final String header = createTSVHeader();
								output.write(header);
								VariantHandler.table.clear();
								VariantHandler.table.headerHover = 2;
								VariantHandler.table.sorter.ascending = true;
								VariantHandler.table.createPolygon();
								VariantHandler.table.repaint();
								FileWrite.output = output;
								final FileRead calculator = new FileRead();
								calculator.varcalc = true;
								calculator.execute();
							}
						} else {
							if (vcf.isSelected()) {
								//final SAMSequenceDictionary dict = AddGenome.ReadDict(MainPane.ref);
								FileRead.indexCreator = new TabixIndexCreator(TabixFormat.VCF);
								//FileRead.indexCreator = new TabixIndexCreator(dict, TabixFormat.VCF);
								FileRead.filepointer = 0;

								if (!outfname.contains(".vcf")) {
									outfname = outfname + ".vcf";
								} else {
									FileRead.outputName = outfname;
								}

								if (!outfname.endsWith(".gz")) {
									outfname = outfname + ".gz";
								}
								FileRead.lastpos = 0;
								FileWrite.outputgz = new BlockCompressedOutputStream(outfname);
								FileWrite.outFile = new File(outfname);
								final String header = createVCFHeader();
								final VCFHeader vcfheader = new VCFHeader();
								final VCFHeaderLine headerline = new VCFHeaderLine("format", "##fileformat=VCFv4.1");
								vcfheader.addMetaDataLine(headerline);
								vcfCodec.setVCFHeader(vcfheader, VCFHeaderVersion.VCF4_1);

								FileWrite.outputgz.write(header.getBytes());
								VariantHandler.table.clear();
								VariantHandler.table.headerHover = 2;
								VariantHandler.table.sorter.ascending = true;
								VariantHandler.table.createPolygon();
								VariantHandler.table.repaint();

								// FileRead.output = output;
								final FileRead calculator = new FileRead();
								calculator.varcalc = true;
								calculator.execute();

							}
						}
					}
				} catch (final Exception e) {
					JOptionPane.showMessageDialog(MainPane.chromDraw, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}

			} else {

				FileWrite.outputgz = null;
				VariantHandler.table.clear();
				VariantHandler.table.headerHover = 2;
				VariantHandler.table.sorter.ascending = true;
				VariantHandler.table.createPolygon();
				VariantHandler.table.repaint();

				final FileRead calculator = new FileRead();
				calculator.varcalc = true;

				calculator.execute();
			}
		} else if (event.getSource() == hideSNVs) {
			
			if (commonSlider.getValue() > 1) {

				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
			
			MainPane.drawCanvas.repaint();
			if (Draw.getSplits().get(0).viewLength <= 1000000) {
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			}
		} else if (event.getSource() == hideIndels) {
			
			if (commonSlider.getValue() > 1) {

				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
			MainPane.drawCanvas.repaint();
			if (Draw.getSplits().get(0).viewLength <= 1000000) {
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			}
		} else if (event.getSource() == hideHomos) {
			
			if (commonSlider.getValue() > 1) {

				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
			
			MainPane.drawCanvas.repaint();
			if (Draw.getSplits().get(0).viewLength <= 1000000) {
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			}
		} else if (event.getSource() == rscode) {		
			
			if (commonSlider.getValue() > 1) {

				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
			MainPane.drawCanvas.repaint();
			if (Draw.getSplits().get(0).viewLength <= 1000000) {
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			}
		} else if (event.getSource() == allChroms) {
			if (allChroms.isSelected()) {
				varcalc.setEnabled(true);
				aminomenu.add(allChromsfrom, 6);
				aminomenu.add(onlyAutosomes, 7);
				aminomenu.getPopupMenu().pack();
			} else {
				aminomenu.remove(allChromsfrom);
				aminomenu.remove(onlyAutosomes);
				aminomenu.getPopupMenu().pack();
			}
		} else if (event.getSource() == onlyStats) {
			if (onlyStats.isSelected()) {
				aminomenu.add(outputContexts, aminomenu.getPopupMenu().getComponentIndex(onlyStats) + 1);
				aminomenu.getPopupMenu().pack();
			} else {
				outputContexts.setSelected(false);
				aminomenu.remove(outputContexts);
				aminomenu.getPopupMenu().pack();
			}
		} else if (event.getSource() == write) {

			try {

				/*
				 * String outfname = ""; String path =MainPane.savedir; //path = new
				 * java.io.File(".").getCanonicalPath(); JFileChooser chooser = new
				 * JFileChooser(path); if(tabs.getSelectedComponent().equals(statsScroll)) {
				 * chooser.setDialogTitle("Save variant statistics"); } else {
				 * chooser.setDialogTitle("Save variants"); }
				 */
				final FileDialog fs = new FileDialog(frame, "Save output file to...", FileDialog.SAVE);
				fs.setDirectory(MainPane.savedir);
				fs.setFile("*.tsv");
				fs.setVisible(true);
				String outfname = "";
				final String filename = fs.getFile();

				if (filename != null) {

					outfname = fs.getDirectory() + "/" + filename;
					BufferedWriter output = null;
					MainPane.savedir = fs.getDirectory();
					MainPane.writeToConfig("DefaultSaveDir=" + MainPane.savedir);
					try {
						File outfile = null;
						if (tsv.isSelected() || compactTsv.isSelected() || oncodrive.isSelected()
								|| geneTsv.isSelected()) {

							if (!outfname.contains(".tsv")) {
								outfile = new File(outfname + ".tsv");

								output = new BufferedWriter(new FileWriter(outfile));
							} else {
								outfile = new File(outfname);

								output = new BufferedWriter(new FileWriter(outfile));
							}

							if (output != null) {
								final OutputRunner runner = new OutputRunner(output, null, null);
								runner.execute();
							}
						} else if (vcf.isSelected()) {
							BlockCompressedOutputStream outputgz;

							if (!outfname.contains(".vcf")) {
								outfile = new File(outfname + ".vcf.gz");
								outputgz = new BlockCompressedOutputStream(outfile);
							} else {
								if (!outfname.endsWith(".gz")) {
									outfile = new File(outfname + ".gz");
									outputgz = new BlockCompressedOutputStream(outfile);
								} else {
									outfile = new File(outfname);
									outputgz = new BlockCompressedOutputStream(outfile);
								}
							}

							if (outputgz != null) {

								final OutputRunner runner = new OutputRunner(output, outputgz, outfile);
								runner.execute();
							}
						}
						/*
						 * else if(forVEP.isSelected()) {
						 * 
						 * File outfile = new File(outfname); output = new BufferedWriter(new
						 * FileWriter(outfile));
						 * 
						 * if(output != null) {
						 * 
						 * Loader.setLoading("Writing output..."); String[] row, position; VarNode
						 * node; // String rscode, uniprot;
						 * 
						 * for(int trans = 0; trans < table.transarray.size(); trans++) {
						 * table.getAminos(table.transarray.get(trans));
						 * 
						 * for(int s = 0; s<table.aminoarray.size(); s++) { row =
						 * table.aminoarray.get(s).getRow(); node = table.aminoarray.get(s).getNode();
						 * // rscode = table.getRscode(node, row[5]); position = row[2].split(":");
						 * //getaminosiin jo uniprottikoodi // uniprot =
						 * node.getExons().get(0).getTranscript().getUniprot(); output.write(position[0]
						 * +" " +position[1] +" " +position[1] +" " +node.getRefBase() +"/" +row[5]
						 * +" 1");
						 * 
						 * }
						 * 
						 * } output.close(); Loader.ready("Writing output..."); }
						 * 
						 * 
						 * }
						 */

					} catch (final Exception ex) {
						JOptionPane.showMessageDialog(frame, ex.getMessage());
					}
				}

			} catch (final Exception e) {
				ErrorLog.addError(e.getStackTrace());
				e.printStackTrace();
			}
		} else if (event.getSource() == advQualities) {
			menu.show(this, 100, 100);
		} else if (event.getSource() == applyQualities) {
			if (event.getSource() == applyQualities) 
				 applyAdvQual(menuPanel, false);

			
			if (MainPane.drawCanvas != null && Getter.getInstance().getVariantHead().getNext() != null) {
				FileRead.search = true;
				MainPane.drawCanvas.forcereload = true;
				MainPane.drawCanvas.gotoPos(Draw.getSplits().get(0).chrom, Draw.getSplits().get(0).start, Draw.getSplits().get(0).end);
				MainPane.drawCanvas.forcereload = false;
			}
		}
	}
	private QualEntry getFormat(String label, String text) {
		if (text.length() == 0) return null;
		try {	
			String format = "<";
			Float number = null;
			if (text.trim().startsWith("<")) {
				number = Float.parseFloat(text.substring(1).trim());
			} else if (text.trim().startsWith("<=")) {
				format += "=";
				number = Float.parseFloat(text.substring(2).trim());
			} else if (text.trim().startsWith(">=")) {
				format = ">=";
				number = Float.parseFloat(text.substring(2).trim());
			} else if (text.trim().startsWith(">")) {
				format = ">";
				number = Float.parseFloat(text.substring(1).trim());
			} else {
				number = Float.parseFloat(text.trim());
			}			
			return new QualEntry(label, number, format);
		} catch (Exception e) {
			return null;
		}		
	}

	private void applyAdvQual(JPanel panel, boolean indel) {
		
		ArrayList<QualEntry> advQDraw = new ArrayList<QualEntry>();
		for (int i = 0; i < panel.getComponentCount(); i++) {
			if(panel.getComponent(i).getName() != null && panel.getComponent(i).getName().equals("value1")) {
				final JTextField field = (JTextField)panel.getComponent(i);
				final JTextField field2 = (JTextField)panel.getComponent(i+2);
				final JTextField field3 = (JTextField)panel.getComponent(i+3); 
				QualEntry entry = getFormat(field.getText() +"/" +field2.getText(), field3.getText());
				
				if (entry != null) advQDraw.add(entry);
				i+=3; continue;						
			}
			if (panel.getComponent(i) instanceof JLabel) {
				final JLabel label = (JLabel) panel.getComponent(i);
				if (panel.getComponent(i + 1) instanceof JTextField) {
					final JTextField field = (JTextField) panel.getComponent(i + 1);
					QualEntry entry = getFormat(label.getText(), field.getText());
					
					try {						
						field.setForeground(Color.black);
						if (entry != null) advQDraw.add(entry);
						/* final JTextField indelfield = (JTextField) menuPanelIndel.getComponent(i + 1);
						indelfield.setText(format + number); */
						
					} catch (final Exception e) {
						field.setForeground(Color.red);
					}
				} else {
					if (panel.getComponent(i + 1) instanceof JCheckBox) {
						final JCheckBox field = (JCheckBox) panel.getComponent(i + 1);

						if (field.isSelected()) {
							QualEntry entry = new QualEntry(field.getText(), 1F, "");
							JCheckBox reverse = (JCheckBox) panel.getComponent(i-1);
							entry.reverse = reverse.isSelected();							advQDraw.add(entry);							
							
							/* final JCheckBox fieldIndel = (JCheckBox) menuPanelIndel.getComponent(i + 1);
							fieldIndel.setSelected(true); */
						}
					}
				}
			}
		}
		VariantHandler.advQDraw = advQDraw;
	}
	public static void removeMenuComponents() {
		menuPanel.removeAll();
		
		initMenu();
	}

	public static void addMenuComponents(final String line) {
		if (line.startsWith("##FILTER")) {
			final String key = line.substring(line.indexOf("<ID=") + 4, line.indexOf(","));
			if (MainPane.drawCanvas != null && VcfReader.advQualities == null) {
				VcfReader.advQualities = new HashMap<String, Float>();
				VcfReader.advQualitiesIndel = new HashMap<String, Float>();
			}
		
			if (!VcfReader.advQualities.containsKey(key)) {
				VcfReader.advQualities.put(key, 0F);
				String description = line.substring(line.indexOf("ion=\"") + 5);
				description = description.substring(0, description.indexOf("\""));
				final JCheckBox addCheck = new JCheckBox(key);
				final JCheckBox addCheckIndel = new JCheckBox(key);
				addCheck.setToolTipText(description);

				final GridBagConstraints constr = new GridBagConstraints();
				//constr.fill = GridBagConstraints.HORIZONTAL;
				constr.anchor = GridBagConstraints.NORTHWEST;
				constr.gridx = 0;
				constr.gridwidth = 4;
				addCheck.setOpaque(false);
				constr.gridy = menuPanel.getComponentCount() / 2;
				
				VariantHandler.menuPanel.add(new JCheckBox(), constr);
			
				constr.gridx = 1;
				VariantHandler.menuPanel.add(new JLabel(), constr);

				constr.gridx = 2;
				VariantHandler.menuPanel.add(addCheck, constr);
				
				if (VariantHandler.advQDraw != null && VariantHandler.advQDraw.size() > 0) {

					for (int i = 0; i < VariantHandler.advQDraw.size(); i++) {
						if (VariantHandler.advQDraw.get(i).key.equals(key)) {
							addCheck.setSelected(VariantHandler.advQDraw.get(i).value == 1);
							addCheckIndel.setSelected(VariantHandler.advQDraw.get(i).value == 1);
							break;
						}
					}
				}
				
			}
			
		} else {

			if (line.indexOf("Number=") < 0) return;
			
			final String key = line.substring(line.indexOf("<ID=") + 4, line.indexOf(","));
		
			if (VcfReader.advQualities == null) {
				VcfReader.advQualities = new HashMap<String, Float>();
				VcfReader.advQualitiesIndel = new HashMap<String, Float>();
			}
			if (!VcfReader.advQualities.containsKey(key)) {
				final GridBagConstraints constr = new GridBagConstraints();

				constr.fill = GridBagConstraints.HORIZONTAL;

				constr.anchor = GridBagConstraints.NORTHWEST;
				constr.gridx = 0;
				constr.gridy = menuPanel.getComponentCount() / 2;
				constr.gridwidth = 4;
				final String[] split = line.substring(line.indexOf("<") + 1).replace(">", "").split(",");
				final StringBuffer tooltip = new StringBuffer("<html>");
				for (int i = 0; i < split.length; i++) {

					tooltip.append(split[i] + "<br>");
				}
				tooltip.append("</html>");
				
				VcfReader.advQualities.put(key, 0F);

				final JLabel addLabel = new JLabel(key);
				
				addLabel.setToolTipText(tooltip.toString());

				VariantHandler.menuPanel.add(addLabel, constr);

				final JTextField field = new JTextField("<");
				final JTextField fieldIndel = new JTextField("<");
				
				if (VariantHandler.advQDraw != null && VariantHandler.advQDraw.size() > 0) {

					for (int i = 0; i < VariantHandler.advQDraw.size(); i++) {
						if (VariantHandler.advQDraw.get(i).key.equals(key)) {
							field.setText(VariantHandler.advQDraw.get(i).format + " " + VariantHandler.advQDraw.get(i).value);
							fieldIndel.setText(VariantHandler.advQDraw.get(i).format + " " + VariantHandler.advQDraw.get(i).value);
							break;
						}
					}
				}
				
				field.setPreferredSize(new Dimension(100, (int) (BaseVariables.defaultFontSize * 2)));
				field.setToolTipText(tooltip.toString());
				fieldIndel.setPreferredSize(new Dimension(100, (int) (BaseVariables.defaultFontSize * 2)));
				fieldIndel.setToolTipText(tooltip.toString());
				constr.gridx = 1;
				VariantHandler.menuPanel.add(field, constr);
			}
		}
	

		for (int i = 0; i < VariantHandler.menuPanel.getComponentCount(); i++) {
			VariantHandler.menuPanel.getComponent(i).setFont(MainPane.menuFont);

			if (VariantHandler.menuPanel.getComponent(i) instanceof JTextField) {
				VariantHandler.menuPanel.getComponent(i).setPreferredSize(new Dimension(BaseVariables.defaultFontSize * 6, (int) (BaseVariables.defaultFontSize * 2)));
			}
		}
		

		VariantHandler.menu.pack();

	}

	String createVCFHeader() {
		final StringBuffer headerstring = new StringBuffer("##fileformat=VCFv4.1"
				+ MainPane.lineseparator + "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">"
				+ MainPane.lineseparator + "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Approximate read depth\">"
				+ MainPane.lineseparator
				+ "##FORMAT=<ID=AD,Number=.,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">"
				+ MainPane.lineseparator + "##FORMAT=<ID=GQ,Number=1,Type=Float,Description=\"Genotype Quality\">"
				+ MainPane.lineseparator + "##INFO=<ID=AN,Number=1,Type=Integer,Description=\"Allele number\">"
				+ MainPane.lineseparator + "##INFO=<ID=AC,Number=1,Type=Integer,Description=\"Allele count\">"
				+ MainPane.lineseparator + "##INFO=<ID=AF,Number=1,Type=Float,Description=\"Allele frequency\">"
				+ MainPane.lineseparator + "##reference=" + MainPane.ref.getName() + MainPane.lineseparator);

		if (VariantHandler.onlyselected.isSelected()) {
			headerstring.append("##sample=" + Sample.selectedSample.getName() + MainPane.lineseparator);
			headerstring.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t"
					+ Sample.selectedSample.getName() + MainPane.lineseparator);
		} else {
			StringBuffer headersamples = new StringBuffer("");
			headerstring.append("##files=");
			for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
				if (Getter.getInstance().getSampleList().get(i).getTabixFile() != null
						|| Getter.getInstance().getSampleList().get(i).calledvariants
						|| (VariantCaller.inanno.isSelected() && Getter.getInstance().getSampleList().get(i).samFile != null)) {
					headerstring.append(Getter.getInstance().getSampleList().get(i).getName() + ",");

				}
				if ((VariantCaller.inanno.isSelected() && Getter.getInstance().getSampleList().get(i).samFile != null)
						|| Getter.getInstance().getSampleList().get(i).calledvariants
						|| !Getter.getInstance().getSampleList().get(i).multiVCF
								&& (Getter.getInstance().getSampleList().get(i).getTabixFile() != null
										|| Getter.getInstance().getSampleList().get(i).multipart)) {

					headersamples.append("\t" + Getter.getInstance().getSampleList().get(i).getName());
				}
			}
			headerstring.deleteCharAt(headerstring.length() - 1);
			headerstring.append(MainPane.lineseparator);
			headerstring.append("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT");
			headerstring.append(headersamples + MainPane.lineseparator);
			headersamples = new StringBuffer();
		}

		return headerstring.toString();
	}

	String createTSVHeader() {
		final StringBuffer headerstring = new StringBuffer("");
		headerstring.append("##BasePlayer version: " + MenuBar.version + " output "
				+ new SimpleDateFormat("dd.MM.yyyy HH:mm").format(Calendar.getInstance().getTime())
				+ MainPane.lineseparator);

		headerstring.append("##Files: ");
		if (VariantHandler.onlyselected.isSelected()) {
			headerstring.append(Sample.selectedSample.getName());
		} else {
			for (int i = 0; i < MainPane.samples; i++) {
				if (Getter.getInstance().getSampleList().get(i).calledvariants
						|| (Getter.getInstance().getSampleList().get(i).getTabixFile() != null
								|| Getter.getInstance().getSampleList().get(i).multipart)) {

					headerstring.append(Getter.getInstance().getSampleList().get(i).getName() + ",");
				}
			}
			headerstring.deleteCharAt(headerstring.length() - 1);
		}

		headerstring.append(MainPane.lineseparator);
		headerstring
				.append("##Genome:" + MainPane.ref.getName() + ",Annotation:" + MainPane.annotationfile + MainPane.lineseparator);

		headerstring.append("##" + VariantHandler.synonymous.getText() + ":" + VariantHandler.synonymous.isSelected()
				+ "," + VariantHandler.utr.getText() + ":" + VariantHandler.utr.isSelected() + ","
				+ VariantHandler.intronic.getText() + ":" + VariantHandler.intronic.isSelected() + ","
				+ VariantHandler.nonsense.getText() + ":" + VariantHandler.nonsense.isSelected() + ","
				+ VariantHandler.intergenic.getText() + ":" + VariantHandler.intergenic.isSelected()
				+ MainPane.lineseparator);

		if (Getter.getInstance().getControlList().size() > 0 && Control.controlData.controlsOn) {

			headerstring.append("##Controls:");
			for (int i = 0; i < Getter.getInstance().getControlList().size(); i++) {
				if (Getter.getInstance().getControlList().get(i).controlOn) {
					headerstring.append(Getter.getInstance().getControlList().get(i).getName() + ":"
							+ Getter.getInstance().getControlList().get(i).alleleFreq);
					if (Getter.getInstance().getControlList().get(i).remOverlaps.isSelected()) {
						headerstring.append(",Overlap_indels");
					}
					headerstring.append(",");
				}
			}
			headerstring.deleteCharAt(headerstring.length() - 1);
			headerstring.append(MainPane.lineseparator);

		} else {
			headerstring.append("##No controls applied" + MainPane.lineseparator);
		}
		if (MainPane.bedCanvas.bedOn) {
			headerstring.append("##Tracks:");
			for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
				if (MainPane.bedCanvas.bedTrack.get(i).intersect) {
					if (MainPane.bedCanvas.bedTrack.get(i).limitValue > Double.MIN_VALUE) {
						headerstring.append(MainPane.bedCanvas.bedTrack.get(i).file.getName() + ">="
								+ MainPane.bedCanvas.bedTrack.get(i).limitValue + ",");
					} else {
						headerstring.append(MainPane.bedCanvas.bedTrack.get(i).file.getName() + ",");
					}
				}
			}
			headerstring.deleteCharAt(headerstring.length() - 1);
			headerstring.append(MainPane.lineseparator);
		}
		if (MainPane.projectValues.variantHandlerValues.commonVariantsMin > 1) {

			if (VariantHandler.clusterSize > 0) {
				headerstring.append(
						"##Variant clusters in " + MainPane.projectValues.variantHandlerValues.commonVariantsMin + "/" + MainPane.varsamples
								+ " samples within " + VariantHandler.clusterSize + "bp" + MainPane.lineseparator);
			} else {
				headerstring.append("##Shared variants in " + MainPane.projectValues.variantHandlerValues.commonVariantsMin + "/"
						+ MainPane.varsamples + " samples" + MainPane.lineseparator);
			}
		}
		if (MainPane.projectValues.variantHandlerValues.compareGene > 1) {
			headerstring.append("##At least " + MainPane.projectValues.variantHandlerValues.compareGene + "/" + MainPane.varsamples
					+ " samples share a mutated gene" + MainPane.lineseparator);
		}
		headerstring.append("##Variant filters:" + MainPane.lineseparator);
		headerstring.append("##Hide rs-coded variants: " + VariantHandler.rscode.isSelected() + MainPane.lineseparator);
		headerstring.append("##Hide SNVs: " + VariantHandler.hideSNVs.isSelected() + MainPane.lineseparator);
		headerstring.append("##Hide indels: " + VariantHandler.hideIndels.isSelected() + MainPane.lineseparator);
		
		headerstring.append("##Min. coverage: " + MainPane.projectValues.variantHandlerValues.variantCoverage + MainPane.lineseparator);
		headerstring.append(
				"##Min. allelic/fraction: " + MainPane.projectValues.variantHandlerValues.variantCalls + "%" + MainPane.lineseparator);
		headerstring
				.append("##Min. quality score: " + MainPane.projectValues.variantHandlerValues.variantQuality + MainPane.lineseparator);
		headerstring.append(
				"##Min. genotype quality score: " + MainPane.projectValues.variantHandlerValues.variantGQ + MainPane.lineseparator);
		headerstring.append("##Max. coverage: " + MainPane.projectValues.variantHandlerValues.variantMaxCoverage + MainPane.lineseparator);
	
		if (VariantHandler.advQDraw != null) {
			for (int i = 0; i < VariantHandler.advQDraw.size(); i++) {
				headerstring.append("##" + VariantHandler.advQDraw.get(i).key + ": "
						+ VariantHandler.advQDraw.get(i).format
						+ VariantHandler.advQDraw.get(i).value + MainPane.lineseparator);
			}
		}
		
		final StringBuffer controls = new StringBuffer("");

		if (Control.controlData.controlsOn) {
			controlarray.clear();
			for (int i = 0; i < Getter.getInstance().getControlList().size(); i++) {
				if (!Getter.getInstance().getControlList().get(i).controlOn) {
					continue;
				}
				controls.append("AF: " + Getter.getInstance().getControlList().get(i).getName() + "\tOR\t");
				controlarray.add(Getter.getInstance().getControlList().get(i));
			}
		}
		final StringBuffer tracks = new StringBuffer("");
		if (MainPane.bedCanvas.bedOn) {
			for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
				if (MainPane.bedCanvas.bedTrack.get(i).intersect) {
					tracks.append(MainPane.bedCanvas.bedTrack.get(i).file.getName() + "\t");
				}
			}
		}
		String clusters = "";
		if (commonSlider.getValue() > 1 && clusterSize > 0) {
			clusters = "ClusterID\tClusterMutCount\tClusterWidth\tClusterMutFreq\t";
		}

		if (!tabs.getSelectedComponent().equals(statsScroll) && !onlyStats.isSelected()) {
			if (oncodrive.isSelected()) {
				headerstring.append("#CHROM\tPOS\tREF\tALT\tSAMPLE" + MainPane.lineseparator);
			} else if (geneTsv.isSelected()) {
				headerstring.append("Gene\tMutationCount\tSampleCount\t");
				if (VariantHandler.onlyselected.isSelected()) {
					headerstring.append(Sample.selectedSample.getName());
				} else {
					for (int i = 0; i < MainPane.samples; i++) {
						if (Getter.getInstance().getSampleList().get(i).calledvariants
								|| (Getter.getInstance().getSampleList().get(i).getTabixFile() != null
										|| Getter.getInstance().getSampleList().get(i).multipart)) {

							headerstring.append(Getter.getInstance().getSampleList().get(i).getName() + "\t");
						}
					}
					headerstring.deleteCharAt(headerstring.length() - 1);
				}
				headerstring.append(MainPane.lineseparator);
			} else {
				headerstring.append(
						"#Sample\tGene\tMutationCount\tSampleCount\tENSG\tENST\tBioType\tPosition\tStrand\tRegion\tEffect\tBaseChange\tGenotype(calls/coverage)\tAllelicFraction\tQuality\tGQ\tFILTER\trs-code\t"
								+ clusters + controls + tracks + "Description" + MainPane.lineseparator);
			}
		} else {
			// if(onlyStats.isSelected()) {
			headerstring.append(
					"#Sample\tVariants\tSNVs\tDELs\tINSs\tCoding\tHetero/homo-rate\tTS/TV-rate\tT>A\tT>C\tT>G\tC>A\tC>G\tC>T\tAvg.call/cov\tAvg.cov\tSynonymous\tNonsynonymous\tMissense\tSplice-site\tNonsense\tFrameShift\tInframe"
							+ MainPane.lineseparator);
			// }
			// else {
			// headerstring.append("#Sample\tVariants\tSNVs\tDELs\tINSs\tCoding\tHetero/homo-rate\tTS/TV-rate\tT>A\tT>C\tT>G\tC>A\tC>G\tC>T\tAvg.call/cov"+MainPane.lineseparator);
			// }
		}
		return headerstring.toString();
	}

	void writeOutput(final BufferedWriter output, final BlockCompressedOutputStream outputgz, final File outFile) {
		frame.getGlassPane().setVisible(true);
		table.setEnabled(false);
		frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {

			if (tabs.getSelectedComponent().equals(statsScroll)) {
				final String header = createTSVHeader();
				output.write(header);
				Sample sample;
				// output.write("#Sample\tVariants\tSNVs\tDELs\tINSs\tCoding\tHetero/homo-rate\tTS/TV-rate\tT>A\tT>C\tT>G\tC>A\tC>G\tC>T\tAvg.call/cov");
				for (int i = 0; i < VariantHandler.stattable.sampleArray.size(); i++) {
					sample = (Sample) VariantHandler.stattable.sampleArray.get(i)[0];
					output.write(sample.getName());

					for (int j = 1; j < VariantHandler.stattable.headerlengths.length; j++) {
						output.write("\t" + VariantHandler.stattable.sampleArray.get(i)[j]);
					}
					// if(onlyStats.isSelected()) {
					output.write("\t" + sample.syn + "\t" + sample.nonsyn + "\t" + sample.missense + "\t"
							+ sample.splice + "\t" + sample.nonsense + "\t" + sample.fshift + "\t" + sample.inframe);
					// }
					output.write(MainPane.lineseparator);
				}
				output.close();
			} else {

				if (vcf.isSelected()) {
					final String header = createVCFHeader();
					if (outputgz != null) {
						//final SAMSequenceDictionary dict = AddGenome.ReadDict(MainPane.ref);
						FileRead.indexCreator = new TabixIndexCreator(TabixFormat.VCF);
						//FileRead.indexCreator = new TabixIndexCreator(dict, TabixFormat.VCF);
						FileRead.filepointer = 0;
						final VCFHeader vcfheader = new VCFHeader();
						final VCFHeaderLine headerline = new VCFHeaderLine("format", "##fileformat=VCFv4.1");
						vcfheader.addMetaDataLine(headerline);
						vcfCodec.setVCFHeader(vcfheader, VCFHeaderVersion.VCF4_1);
						outputgz.write(header.getBytes());
					} else {
						output.write(createVCFHeader());
					}

					FileWrite.writeGeneListToVCF(output, outputgz);
					if (outputgz != null) {

						for (int i = 0; i < VariantHandler.outputStrings.size(); i++) {
							outputgz.write(VariantHandler.outputStrings.get(i).getBytes());

							final Feature vcf = VariantHandler.vcfCodec.decode(VariantHandler.outputStrings.get(i));

							FileRead.indexCreator.addFeature(vcf, FileRead.filepointer);
							FileRead.filepointer = outputgz.getFilePointer();
						}
						VariantHandler.outputStrings.clear();
						outputgz.flush();

						final Index index = FileRead.indexCreator.finalizeIndex(outputgz.getFilePointer());

						index.writeBasedOnFeatureFile(outFile);
						outputgz.close();

					}
				} else {

					output.write(createTSVHeader());
					for (int gene = 0; gene < table.genearray.size(); gene++) {
						MainPane.drawCanvas.loadbarAll = (int) ((gene / (double) table.genearray.size()) * 100);
						MainPane.drawCanvas.loadBarSample = (int) ((gene / (double) table.genearray.size()) * 100);
						FileWrite.writeTranscriptToFile(table.genearray.get(gene), output);
					}
					output.close();
				}

			}
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			table.setEnabled(true);
			e.printStackTrace();

			JOptionPane.showMessageDialog(MainPane.chromDraw, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		frame.getGlassPane().setVisible(false);
		frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		controlarray.clear();
		table.setEnabled(true);
	}
		public class OutputRunner extends SwingWorker<String, Object> {
		BufferedWriter output;
		BlockCompressedOutputStream outputgz;
		File outFile;

		public OutputRunner(final BufferedWriter output, final BlockCompressedOutputStream outputgz,
				final File outFile) {
			this.output = output;
			this.outputgz = outputgz;
			this.outFile = outFile;
		}

		protected String doInBackground() {
			Loader.setLoading("Writing output...");
			writeOutput(output, outputgz, outFile);
			Loader.ready("Writing output...");
			return "";
		}

	}

	@Override
	public void mouseClicked(final MouseEvent event) {

		if (event.getSource() == qualityLabel) {
			menu.show(this, 100, 100);
		} 
	}

	@Override
	public void mouseEntered(final MouseEvent event) {
		if (event.getSource() == qualityLabel) {
			qualityLabel.setForeground(Color.white);

		}
	}

	@Override
	public void mouseExited(final MouseEvent event) {
		if (event.getSource() == qualityLabel) {
			qualityLabel.setForeground(Color.black);

		}
	}

	@Override
	public void mousePressed(final MouseEvent event) {

		if (event.getSource() == tabs) {
			if (tabs.getSelectedIndex() == 0) {
				VariantHandler.aminoCount.setText(table.variants + " variants");
				outputmenu.setText("Variant output");
				vcf.setVisible(true);
				VariantHandler.compactTsv.setVisible(true);
				VariantHandler.oncodrive.setVisible(true);
				outputmenu.revalidate();
			} else if (tabs.getSelectedIndex() == tabs.indexOfComponent(statsScroll)) {
				if (stattable.bufImage.getWidth() == 1) {
					stattable.bufImage = MethodLibrary.toCompatibleImage(new BufferedImage(MainPane.screenSize.width,
							MainPane.screenSize.height, BufferedImage.TYPE_INT_ARGB));
					stattable.buf = (Graphics2D) stattable.bufImage.getGraphics();
				}
				VariantHandler.aminoCount.setText(stattable.variants + " variants");
				outputmenu.setText("Stats output");
				outputmenu.revalidate();
				vcf.setVisible(false);
				VariantHandler.compactTsv.setVisible(false);
				VariantHandler.oncodrive.setVisible(false);
			} else {
				VariantHandler.aminoCount.setText(tables.get(tabs.getSelectedIndex() - (tabs.indexOfComponent(statsScroll) + 1)).variants + " variants");
				outputmenu.setText("Variant output");
				vcf.setVisible(true);
				VariantHandler.compactTsv.setVisible(true);
				VariantHandler.oncodrive.setVisible(true);
				outputmenu.revalidate();
			}
		}
	}

	@Override
	public void mouseReleased(final MouseEvent event) {
		if (event.getSource() == tableScroll.getVerticalScrollBar()) {
			table.repaint();
			return;
		} else if (event.getSource() == coverageSlider) {
			if (coverageSlider.getValue() == coverageSlider.getMaximum()) {
				coverageSlider.setMaximum(coverageSlider.getMaximum() * 2);
			}
			if (commonSlider.getValue() > 1) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
		} else if (event.getSource() == qualitySlider) {
			if (qualitySlider.getValue() == qualitySlider.getMaximum()) {
				qualitySlider.setMaximum(qualitySlider.getMaximum() * 2);
			}
			if (commonSlider.getValue() > 1) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
		} else if (event.getSource() == gqSlider) {
			if (gqSlider.getValue() == gqSlider.getMaximum()) {
				gqSlider.setMaximum(gqSlider.getMaximum() * 2);
			}
			if (commonSlider.getValue() > 1) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
		} else if (event.getSource() == maxCoverageSlider) {
			if (maxCoverageSlider.getValue() == maxCoverageSlider.getMaximum()) {
				maxCoverageSlider.setMaximum(maxCoverageSlider.getMaximum() * 2);
			}
			if (commonSlider.getValue() > 1) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
		} else if (event.getSource() == callSlider) {
			if (commonSlider.getValue() > 1) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
		} else if (event.getSource() == readSlider) {
			if (commonSlider.getValue() > 1) {
				VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);
			}
		} 
		
		if (MainPane.drawCanvas != null) MainPane.drawCanvas.repaint();
	}

	void freezeFilters(final boolean value) {

		if (value) {
			for (int i = 2; i < filterpanel.getComponentCount(); i++) {
				if (filterpanel.getComponent(i).equals(freeze)) {
					
					continue;
				}
				filterpanel.getComponent(i).setEnabled(false);
				filterpanel.getComponent(i).revalidate();

			}
			
		} else {
			for (int i = 0; i < filterpanel.getComponentCount(); i++) {
				filterpanel.getComponent(i).setEnabled(true);
				filterpanel.getComponent(i).revalidate();

			}
			
		}

	}

	public static void setFonts(final Font menuFont) {
		try {

			for (int i = 0; i < VariantHandler.filterpanel.getComponentCount(); i++) {
				if (VariantHandler.filterpanel.getComponent(i).getName() != null) {
					VariantHandler.filterpanel.getComponent(i).setFont(MainPane.menuFontBold);
				} else {
					VariantHandler.filterpanel.getComponent(i).setFont(menuFont);
				}
			}
			for (int i = 0; i < VariantHandler.comparepanel.getComponentCount(); i++) {
				if (VariantHandler.comparepanel.getComponent(i).getName() != null) {
					VariantHandler.comparepanel.getComponent(i).setFont(MainPane.menuFontBold);
				} else {
					VariantHandler.comparepanel.getComponent(i).setFont(menuFont);
				}
			}
			for (int i = 0; i < VariantHandler.aminopanel.getComponentCount(); i++) {
				VariantHandler.aminopanel.getComponent(i).setFont(menuFont);
			}
			for (int i = 0; i < VariantHandler.hidepanel.getComponentCount(); i++) {
				VariantHandler.hidepanel.getComponent(i).setFont(menuFont);
			}
			for (int i = 0; i < VariantHandler.inheritpanel.getComponentCount(); i++) {
				VariantHandler.inheritpanel.getComponent(i).setFont(menuFont);
			}
			VariantHandler.filterPanes.setFont(menuFont);
			VariantHandler.filters.setFont(menuFont);

			for (int i = 0; i < VariantHandler.filters.getPopupMenu().getComponentCount(); i++) {
				VariantHandler.filters.getPopupMenu().getComponent(i).setFont(menuFont);
			}
			VariantHandler.aminomenu.setFont(menuFont);
			VariantHandler.outputmenu.setFont(menuFont);
			for (int i = 0; i < VariantHandler.aminomenu.getPopupMenu().getComponentCount(); i++) {
				VariantHandler.aminomenu.getPopupMenu().getComponent(i).setFont(menuFont);
			}
			for (int i = 0; i < VariantHandler.outputmenu.getPopupMenu().getComponentCount(); i++) {
				VariantHandler.outputmenu.getPopupMenu().getComponent(i).setFont(menuFont);
			}
			varcalc.setFont(menuFont);
			VariantHandler.allChromsfrom.setFont(menuFont.deriveFont(Font.ITALIC));
			VariantHandler.onlyAutosomes.setFont(menuFont.deriveFont(Font.ITALIC));
			VariantHandler.tabs.setFont(menuFont);
			VariantHandler.tabs.revalidate();

			if (VariantHandler.table != null) {

				VariantHandler.table.buf.setFont(menuFont);

				VariantHandler.table.rowHeight = menuFont.getSize() + 5;
				VariantHandler.table.fm = VariantHandler.table.buf.getFontMetrics();

				VariantHandler.stattable.buf.setFont(menuFont);
				VariantHandler.stattable.rowHeight = menuFont.getSize() + 5;
				
				for (int i = 0; i < VariantHandler.tables.size(); i++) {
					VariantHandler.tables.get(i).buf.setFont(menuFont);
					VariantHandler.tables.get(i).rowHeight = menuFont.getSize() + 5;
				}
			}

			for (int i = 0; i < VariantHandler.menuPanel.getComponentCount(); i++) {
				VariantHandler.menuPanel.getComponent(i).setFont(MainPane.menuFont);

				if (VariantHandler.menuPanel.getComponent(i) instanceof JTextField) {

					VariantHandler.menuPanel.getComponent(i).setPreferredSize(
							new Dimension(BaseVariables.defaultFontSize * 6, (int) (BaseVariables.defaultFontSize * 1.5)));
				}
			}
			
			VariantHandler.menu.setFont(menuFont);
			for (int i = 0; i < VariantHandler.menu.getComponentCount(); i++) {
				VariantHandler.menu.getComponent(i).setFont(MainPane.menuFont);

			}
			
			VariantHandler.menu.pack();
			VariantHandler.frame.pack();
			aminobar.setMinimumSize(
					new Dimension((int) aminobar.getSize().getWidth(), (int) aminobar.getSize().getHeight()));

		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void keyTyped(final KeyEvent e) {

	}

	@Override
	public void keyPressed(final KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_ENTER) {

			if (e.getSource() == clusterBox) {
				if (commonSlider.getValue() >= 1) {
					clusterSize = Integer.parseInt(clusterBox.getText());

					VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);					
				}		

			}
		}
		if (e.getKeyCode() == KeyEvent.VK_0) {

			// setFonts();
		}

	}

	@Override
	public void keyReleased(final KeyEvent e) {

	}

	@Override
	public void componentResized(final ComponentEvent e) {

		table.resizeTable();
		for (int i = 0; i < tables.size(); i++) {
			tables.get(i).resizeTable();
		}
		stattable.resizeTable();
		
	}

	@Override
	public void componentMoved(final ComponentEvent e) {

	}

	@Override
	public void componentShown(final ComponentEvent e) {

	}

	@Override
	public void componentHidden(final ComponentEvent e) {
		
	}

	public static void setValues() {
		if (variantSettings == null) {
			variantSettings = new HashMap<String, Integer>();
			variantSettings.put("SNVquality", 0);
			variantSettings.put("SNVgq", 0);
			variantSettings.put("mincoverage", 4);
			variantSettings.put("maxcoverage", 1500);
			variantSettings.put("fraction", 10);
			variantSettings.put("upperFraction", 100);
			variantSettings.put("Indelquality", 0);
			variantSettings.put("Indelgq", 0);
			variantSettings.put("Indelmincoverage", 4);
			variantSettings.put("Indelmaxcoverage", 1500);
			variantSettings.put("Indelfraction", 10);
			variantSettings.put("IndelUpperFraction", 100);
			variantSettings.put("IndelFilters", 0);
			variantSettings.put("geneSlider", 1);
			variantSettings.put("commonSliderMin", 1);
			variantSettings.put("commonSliderMax", 1);
			variantSettings.put("clusterSize", 0);
			variantSettings.put("hideNoncoding", 0);
			variantSettings.put("hideRscoded", 0);
			variantSettings.put("hideSNVs", 0);
			variantSettings.put("hideIndels", 0);
			variantSettings.put("hideHomozygotes", 0);
			variantSettings.put("freeze", 0);
			variantSettings.put("nonSyn", 0);
			variantSettings.put("truncs", 0);
			variantSettings.put("intronic", 0);
			variantSettings.put("intergenic", 0);
			variantSettings.put("utr", 0);
			variantSettings.put("allChroms", 0);
			variantSettings.put("onlySel", 0);
			variantSettings.put("onlyStats", 0);
			variantSettings.put("contexts", 0);
			variantSettings.put("writeFile", 0);
			variantSettings.put("tsv", 1);
			variantSettings.put("compact", 0);
			variantSettings.put("vcf", 0);
			variantSettings.put("windowcalc", 0);
			variantSettings.put("oncodrive", 0);
		}

		VariantHandler.qualitySlider.setValue(variantSettings.get("SNVquality"));
		VariantHandler.gqSlider.setValue(variantSettings.get("SNVgq"));
		VariantHandler.coverageSlider.setValue(variantSettings.get("mincoverage"));
		VariantHandler.maxCoverageSlider.setValue(variantSettings.get("maxcoverage"));
		VariantHandler.callSlider.setValue(variantSettings.get("fraction"));

		VariantHandler.callSlider.setUpperValue(variantSettings.get("upperFraction"));

		VariantHandler.maxSlideValue = MainPane.projectValues.variantHandlerValues.variantMaxCoverage;

		if (MainPane.varsamples > 1) {
			VariantHandler.commonSlider.setMinimum(1);
			VariantHandler.commonSlider.setMaximum(MainPane.varsamples);
			VariantHandler.geneSlider.setMinimum(1);
			VariantHandler.geneSlider.setMaximum(MainPane.varsamples);
		} else {
			VariantHandler.commonSlider.setMinimum(1);
			VariantHandler.commonSlider.setMaximum(1);
			VariantHandler.geneSlider.setMinimum(1);
			VariantHandler.geneSlider.setMaximum(1);
		}
		VariantHandler.geneSlider.setValue(variantSettings.get("geneSlider"));

		VariantHandler.commonSlider.setValue(variantSettings.get("commonSliderMin"));
		VariantHandler.commonSlider.setUpperValue(variantSettings.get("commonSliderMax"));
		VariantHandler.clusterSize = variantSettings.get("clusterSize");
		VariantHandler.clusterBox.setText("" + VariantHandler.clusterSize);
		VariantHandler.hidenoncoding.setSelected(variantSettings.get("hideNoncoding") == 1);
		VariantHandler.rscode.setSelected(variantSettings.get("hideRscoded") == 1);
		VariantHandler.hideSNVs.setSelected(variantSettings.get("hideSNVs") == 1);
		VariantHandler.hideIndels.setSelected(variantSettings.get("hideIndels") == 1);
		VariantHandler.hideHomos.setSelected(variantSettings.get("hideHomozygotes") == 1);
		VariantHandler.freeze.setSelected(variantSettings.get("freeze") == 1);

		VariantHandler.synonymous.setSelected(variantSettings.get("nonSyn") == 1);
		VariantHandler.nonsense.setSelected(variantSettings.get("truncs") == 1);
		if (variantSettings.get("windowcalc") != null) {
			VariantHandler.windowcalc.setSelected(variantSettings.get("windowcalc") == 1);
		}
		VariantHandler.intronic.setSelected(variantSettings.get("intronic") == 1);
		VariantHandler.intergenic.setSelected(variantSettings.get("intergenic") == 1);
		VariantHandler.utr.setSelected(variantSettings.get("utr") == 1);
		VariantHandler.allChroms.setSelected(variantSettings.get("allChroms") == 1);
		VariantHandler.onlyselected.setSelected(variantSettings.get("onlySel") == 1);
		VariantHandler.onlyStats.setSelected(variantSettings.get("onlyStats") == 1);
		if (variantSettings.get("contexts") != null) {
			VariantHandler.outputContexts.setSelected(variantSettings.get("contexts") == 1);
		}
		VariantHandler.writetofile.setSelected(variantSettings.get("writeFile") == 1);
		checkWriteFiles();

		VariantHandler.tsv.setSelected(variantSettings.get("tsv") == 1);
		VariantHandler.compactTsv.setSelected(variantSettings.get("compact") == 1);
		VariantHandler.vcf.setSelected(variantSettings.get("vcf") == 1);
		if (variantSettings.get("genetsv") != null) {
			VariantHandler.geneTsv.setSelected(variantSettings.get("genetsv") == 1);
		}
		VariantHandler.oncodrive.setSelected(variantSettings.get("oncodrive") == 1);

	}

	/* public static void saveValues() {
		variantSettings.put("SNVquality", MainPane.projectValues.variantQuality);
		variantSettings.put("SNVgq", MainPane.projectValues.variantHandlerValues.variantGQ);
		variantSettings.put("mincoverage", MainPane.projectValues.variantHandlerValues.variantCoverage);
		variantSettings.put("maxcoverage", MainPane.projectValues.variantHandlerValues.variantMaxCoverage);
		variantSettings.put("fraction", MainPane.projectValues.variantHandlerValues.variantCalls);

		variantSettings.put("upperFraction", MainPane.projectValues.variantHandlerValues.variantMaxCalls);


		variantSettings.put("geneSlider", MainPane.projectValues.variantHandlerValues.compareGene);
		variantSettings.put("commonSliderMin", MainPane.projectValues.variantHandlerValues.commonVariantsMin);
		variantSettings.put("commonSliderMax", VariantHandler.commonSlider.getUpperValue());
		variantSettings.put("clusterSize", clusterSize);
		variantSettings.put("hideNoncoding", VariantHandler.hidenoncoding.isSelected() ? 1 : 0);
		variantSettings.put("hideRscoded", VariantHandler.rscode.isSelected() ? 1 : 0);
		variantSettings.put("hideSNVs", VariantHandler.hideSNVs.isSelected() ? 1 : 0);
		variantSettings.put("hideIndels", VariantHandler.hideIndels.isSelected() ? 1 : 0);
		variantSettings.put("hideHomozygotes", VariantHandler.hideHomos.isSelected() ? 1 : 0);
		variantSettings.put("freeze", VariantHandler.freeze.isSelected() ? 1 : 0);

		variantSettings.put("nonSyn", VariantHandler.synonymous.isSelected() ? 1 : 0);
		variantSettings.put("truncs", VariantHandler.nonsense.isSelected() ? 1 : 0);
		variantSettings.put("windowcalc", VariantHandler.windowcalc.isSelected() ? 1 : 0);
		variantSettings.put("intronic", VariantHandler.intronic.isSelected() ? 1 : 0);
		variantSettings.put("intergenic", VariantHandler.intergenic.isSelected() ? 1 : 0);
		variantSettings.put("utr", VariantHandler.utr.isSelected() ? 1 : 0);
		variantSettings.put("allChroms", VariantHandler.allChroms.isSelected() ? 1 : 0);
		variantSettings.put("onlySel", VariantHandler.onlyselected.isSelected() ? 1 : 0);
		variantSettings.put("onlyStats", VariantHandler.onlyStats.isSelected() ? 1 : 0);
		variantSettings.put("contexts", VariantHandler.outputContexts.isSelected() ? 1 : 0);
		variantSettings.put("writeFile", VariantHandler.writetofile.isSelected() ? 1 : 0);

		variantSettings.put("tsv", VariantHandler.tsv.isSelected() ? 1 : 0);
		variantSettings.put("compact", VariantHandler.compactTsv.isSelected() ? 1 : 0);
		variantSettings.put("genetsv", VariantHandler.geneTsv.isSelected() ? 1 : 0);
		variantSettings.put("vcf", VariantHandler.vcf.isSelected() ? 1 : 0);
		variantSettings.put("oncodrive", VariantHandler.oncodrive.isSelected() ? 1 : 0);

	} */
	
	
	
	
}
