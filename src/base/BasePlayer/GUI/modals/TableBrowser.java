/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI.modals;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import javax.swing.*;
import javax.swing.table.*;

import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;

public class TableBrowser extends JPanel implements ActionListener, MouseListener {
	private static final long serialVersionUID = 1L;
	
	public JFrame frame = new JFrame("Table Browser");    
	JPanel panel = new JPanel(new GridBagLayout());
	static JTable table;	
	JLabel info = new JLabel("Open TSV files.");
	Object[][] data = {};
	static HashMap<Integer, Boolean> editables = new HashMap<Integer, Boolean>();
	static HashMap<String, Integer> columnValues = new HashMap<String, Integer>();
	static JScrollPane tableScroll = new JScrollPane();
	static TableColumnModel columnModel;
	 //String[] columns = null, selectcols = null;
	static DefaultTableModel tablemodel;
    String[][] selectdata = {};
    //static ArrayList<String[]> datarows;
    static String[] items = { "Select","Position","Chromosome","Start","End","Sample","Gene","Editable" };
   // String[] header;
    Object[] headers = new Object[]{};     
    //String[] selheaders = new String[]{"select", "select", "select", "select"};    
    ArrayList<String> header = new ArrayList<String>();
    boolean changing = false;
    ArrayList<JComboBox> comboboxes = new ArrayList<JComboBox>();
    JTableHeader tableHeader;
   	//DefaultTableModel selectmodel = new DefaultTableModel(data, selheaders); 
   	JButton open = new JButton("Open");
   	//JButton add = new JButton("Add column");
	JButton write = new JButton("Write");
   	boolean found = false, tableSet = false;
   // DefaultTableModel model = new DefaultTableModel(data, headers); 
    MouseAdapter tableMouseListener = new MouseAdapter() {
    	
			@Override
			 public void mouseEntered(final java.awt.event.MouseEvent evt) {
					final EditableHeader header = (EditableHeader) table.getTableHeader();
					header.removeEditor();
				}

				@Override

				public void mouseClicked(final java.awt.event.MouseEvent evt) {

					if (evt.getClickCount() == 2) {

						final int row = table.rowAtPoint(evt.getPoint());
						final int col = table.columnAtPoint(evt.getPoint());

						final EditableHeaderTableColumn column = (EditableHeaderTableColumn) table.getColumnModel()
								.getColumn(col);

						if (column.getHeaderValue() != null && !column.getHeaderValue().toString().equals("Select")
								&& !column.getHeaderValue().toString().equals("Editable")) {

							if (row >= 0 && col >= 0) {

								if (column.getHeaderValue().toString().equals("Position")) {
									final String pos = (String) table.getValueAt(row, col);
									if (!pos.contains(":") && columnValues.get("Chromosome") < 0) {
										MainPane.showError("Please, select chromosome column.", "Note",
												TableBrowser.tableScroll);
										return;
									}
									MenuBar.searchField.setText(pos);
									MenuBar.search(pos);
								} else if (column.getHeaderValue().toString().equals("Chromosome")) {
									final String pos = (String) table.getValueAt(row, col);
									MenuBar.chromosomeDropdown.setSelectedItem(pos.replace("chr", ""));
								} else if (column.getHeaderValue().toString().equals("Start")) {

									if (columnValues.get("Chromosome") < 0) {
										MainPane.showError("Please, select chromosome column.", "Note",
												TableBrowser.tableScroll);
										return;
									}
									if (columnValues.get("End") < 0) {
										final int chrom = columnValues.get("Chromosome");
										final String start = (String) table.getValueAt(row, col);
										final String search = (String) table.getValueAt(row, chrom) + ":" + start;

										MenuBar.searchField.setText(search);
										MenuBar.search(search);
									} else if (columnValues.get("End") > -1) {
										final int chrom = columnValues.get("Chromosome");
										final String start = (String) table.getValueAt(row, col);
										final int end = columnValues.get("End");

										final String search = (String) table.getValueAt(row, chrom) + ":" + start + "-"
												+ (String) table.getValueAt(row, end);

										MenuBar.searchField.setText(search);
										MenuBar.search(search);
									}
								} else if (column.getHeaderValue().toString().equals("Gene")) {
									final String gene = (String) table.getValueAt(row, col);
									MenuBar.searchField.setText(gene);
									MenuBar.search(gene);
								}
								if (columnValues.get("Sample") > -1) {

									final int sample = columnValues.get("Sample");

									MenuBar.search("s " + (String) table.getValueAt(row, sample));
								}

							}
						} else {
							if (column.getHeaderValue() != null
									&& column.getHeaderValue().toString().equals("Select")) {
								MainPane.showError("Please, select column type.", "Note", TableBrowser.tableScroll);
								return;
							}
						}

					}
				}

			};
			ActionListener comboActionListener = new ActionListener() {

				public void actionPerformed(final ActionEvent actionEvent) {

					try {

						if (actionEvent.getActionCommand() == "comboBoxChanged") {

							if (columnValues.size() == 0) {
								return;
							}

							if (changing) {
								return;
							}
							@SuppressWarnings("unchecked")
							final JComboBox<String> comboBox = (JComboBox<String>) actionEvent.getSource();
							/*
							 * if(columnValues.get(comboBox.getSelectedItem()) != -1) {
							 * System.out.println(columnValues.get(comboBox.getSelectedItem())); JComboBox
							 * box = (JComboBox)selectortable.getCellEditor(0,
							 * columnValues.get(comboBox.getSelectedItem())).getTableCellEditorComponent(
							 * selectortable, 0, false, 0, columnValues.get(comboBox.getSelectedItem()));
							 * columnValues.put(comboBox.getSelectedItem().toString(), -1);
							 * box.setSelectedIndex(0); box.revalidate(); }
							 */
							for (int i = 0; i < comboboxes.size(); i++) {

								if (comboboxes.get(i).equals(comboBox)) {
									continue;
								}
								if (comboboxes.get(i).getSelectedItem().toString().equals("Editable")) {
									continue;
								}
								if (comboboxes.get(i).getSelectedItem().toString()
										.equals(comboBox.getSelectedItem().toString())) {
									changing = true;

									comboboxes.get(i).setSelectedIndex(0);
									final EditableHeaderTableColumn col = (EditableHeaderTableColumn) table
											.getColumnModel().getColumn(i);
									col.setHeaderValue("Select");
									// comboboxes.get(i).setSelectedIndex(0);
									// selectortable.setValueAt("select", 0, i);
									comboboxes.get(i).revalidate();
									comboboxes.get(i).repaint();
									table.revalidate();
									table.repaint();
									changing = false;
									break;
								}
							}

							checkHeaders();
							/*
							 * for(int i = 0; i<comboboxes.size(); i++) {
							 * 
							 * columnValues.put(comboboxes.get(i).getSelectedItem().toString(), i);
							 * if(comboboxes.get(i).getSelectedItem().toString().equals("Editable")) {
							 * editables.put(i, true); }
							 * 
							 * } for(int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
							 * 
							 * EditableHeaderTableColumn col =(EditableHeaderTableColumn)
							 * table.getColumnModel().getColumn(i); if(col.getHeaderRenderer() == null) {
							 * editables.put(i, true); } }
							 */

						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			};

			static int addedItems = 0;

			void checkHeaders() {
				columnValues.put("Select", -1);
				columnValues.put("Position", -1);
				columnValues.put("Chromosome", -1);
				columnValues.put("Start", -1);
				columnValues.put("End", -1);
				columnValues.put("Sample", -1);
				columnValues.put("Gene", -1);
				columnValues.put("Editable", -1);
				editables.clear();

				for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {

					final EditableHeaderTableColumn col = (EditableHeaderTableColumn) table.getColumnModel()
							.getColumn(i);
					if (col.getHeaderRenderer() == null) {

						editables.put(i, true);
					} else {

						if (col.getHeaderValue().toString().equals("Editable")) {

							editables.put(i, true);
						} else {
							columnValues.put(col.getHeaderValue().toString(), i);
						}
					}
				}
				table.repaint();
			}
			/*
			 * DefaultTableModel model = new DefaultTableModel(data, headers) {
			 * 
			 * 
			 * private static final long serialVersionUID = 1L;
			 * 
			 * @Override public boolean isCellEditable(int row, int column) { //all cells
			 * false return false; } };
			 */

			void setWindow() {
				try {
					JFrame.setDefaultLookAndFeelDecorated(false);
					// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					if (VariantHandler.aminoCount == null) {
						frame.setVisible(true);
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					} else {
						frame.setVisible(false);
					}

					// JComponent newContentPane = this;
					frame.setContentPane(this);
					frame.setResizable(true);
					frame.setMinimumSize(new Dimension(600, 300));
					createTable();

				} catch (final Exception e) {
					e.printStackTrace();
				}
			}

			public TableBrowser() {
				super(new GridBagLayout());

				setWindow();
			}

			void createTable(final File file) {
				try {
					addedItems = 0;
					int headercount = 4, rows = 0;
					BufferedReader reader = null;
					GZIPInputStream gzip = null;
					String line;

					data = new Object[][] {};
					tablemodel = new DefaultTableModel(data, new Object[] { "", "", "", "" });
					table = new JTable(tablemodel) {
						private static final long serialVersionUID = 1L;

						@Override

						public boolean isCellEditable(final int rowIndex, final int columnIndex) {
							if (editables.containsKey(columnIndex)) {
								return true;
							}
							return false;
						}
					};

					table.addMouseListener(tableMouseListener);
					columnModel = table.getColumnModel();
					for (int i = columnModel.getColumnCount() - 1; i >= 0; i--) {
						columnModel.removeColumn(columnModel.getColumn(i));
					}
					table.setTableHeader(new EditableHeader(columnModel));

					if (file.getName().endsWith(".gz")) {
						try {
							gzip = new GZIPInputStream(new FileInputStream(file));
							reader = new BufferedReader(new InputStreamReader(gzip));
						} catch (final Exception e) {
							e.printStackTrace();
						}
					} else {
						reader = new BufferedReader(new FileReader(file));
					}

					comboboxes.clear();
					tablemodel.setRowCount(0);
					header.clear();
					final int rowlimit = 100000;
					boolean first = true;
					while ((line = reader.readLine()) != null) {

						if (line.startsWith("#")) {
							header.add(line);
							continue;
						}
						if (first) {
							if (header.size() > 0) {

								final String[] row = header.get(header.size() - 1).split("\t");
								if (row.length > headercount) {
									for (int i = 0; i < row.length - headercount; i++) {
										tablemodel.addColumn("new");
									}
									headercount = row.length;
								}
								tablemodel.addRow(row);
							}
							first = false;
						}
						if (rows > rowlimit) {
							MainPane.showError("Showing " + rowlimit + " rows.", "Note", TableBrowser.tableScroll);
							break;
						}
						final String[] row = line.split("\t");
						rows++;

						// datarows.add(row);
						if (row.length > headercount) {
							for (int i = 0; i < row.length - headercount; i++) {
								tablemodel.addColumn("new");
							}
							headercount = row.length;
						}
						tablemodel.addRow(row);
					}
					editables.clear();
					reader.close();
					tablemodel.addColumn("Editable");
					headercount++;
					/*
					 * table = new JTable(rows,headercount) {
					 * 
					 * private static final long serialVersionUID = 1L;
					 * 
					 * 
					 * @Override public boolean isCellEditable(int rowIndex, int columnIndex) {
					 * 
					 * if(editables.containsKey(columnIndex)) { return true; } return false; } };
					 */
					// table.addMouseListener(tableMouseListener);
					// columns = new String[tablelength];
					columnModel = table.getColumnModel();
					table.setTableHeader(new EditableHeader(columnModel));
					final String[] items = { "Select", "Position", "Chromosome", "Start", "End", "Sample", "Gene",
							"Editable" };
					final ComboRenderer renderer = new ComboRenderer(items);
					final BrowserColorColumnRenderer columnRenderer = new BrowserColorColumnRenderer(
							new Color(150, 255, 150), Color.black);

					for (int i = 0; i < columnModel.getColumnCount() - 1; i++) {

						final JComboBox<String> combo = new JComboBox<String>();
						for (int c = 0; c < items.length; c++) {
							combo.addItem(items[c]);
						}

						combo.addActionListener(comboActionListener);

						final EditableHeaderTableColumn col = (EditableHeaderTableColumn) columnModel.getColumn(i);
						col.setCellRenderer(columnRenderer);
						col.setHeaderValue(combo.getItemAt(0));
						col.setHeaderRenderer(renderer);
						col.setHeaderEditor(new DefaultCellEditor(combo));

						comboboxes.add(combo);

					}
					final int columnIndex = columnModel.getColumn(columnModel.getColumnCount() - 1).getModelIndex();
					final EditableHeaderTableColumn col = (EditableHeaderTableColumn) columnModel
							.getColumn(columnIndex);
					col.setHeaderValue("Editable");

					col.setCellRenderer(columnRenderer);
					checkTable(table);
					checkHeaders();
					/*
					 * for(int i = 0; i<rows;i++) { for(int j=0;j<datarows.get(i).length;j++) {
					 * table.setValueAt(datarows.get(i)[j], i, j); } }
					 */
					tableScroll.getViewport().removeAll();
					tableScroll.getViewport().add(table);
					/*
					 * String[] row = new String[headercount]; for(int i = 0 ; i<headercount; i++ )
					 * { row[i] = "select"; }
					 * 
					 * selectmodel.setRowCount(0); selectmodel.addRow(row); //selectortable = new
					 * JTable(selectdata,columns); //selectcols = new String[headercount]; /*
					 * for(int i = 0 ; i<selectcols.length; i++) { TableColumn column =
					 * selectortable.getColumnModel().getColumn(i); JComboBox<String> comboBox = new
					 * JComboBox<String>(); comboBox.addItem("select");
					 * comboBox.addItem("Position"); comboBox.addItem("Chromosome");
					 * comboBox.addItem("Start"); comboBox.addItem("End");
					 * comboBox.addItem("Sample"); column.setCellEditor(new
					 * DefaultCellEditor(comboBox)); }
					 */
					// table = new JTable(model);
					table.revalidate();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}

			void checkTable(final JTable table) {
				final TableColumnModel columnModel = table.getColumnModel();
				if (table.getRowCount() < 2) {
					return;
				}

				changing = true;
				if (table.getValueAt(0, 0).toString().equals("#Sample")) {

					for (int i = 0; i < columnModel.getColumnCount() - 1; i++) {
						final EditableHeaderTableColumn col = (EditableHeaderTableColumn) columnModel.getColumn(i);
						if (table.getValueAt(0, i).toString().contains("#Sample")) {
							comboboxes.get(i).setSelectedItem("Sample");
							col.setHeaderValue(comboboxes.get(i).getSelectedItem().toString());
						} else if (table.getValueAt(0, i).toString().contains("Gene")) {
							comboboxes.get(i).setSelectedItem("Gene");
							col.setHeaderValue(comboboxes.get(i).getSelectedItem().toString());
						} else if (table.getValueAt(0, i).toString().contains("Position")) {
							comboboxes.get(i).setSelectedItem("Position");
							col.setHeaderValue(comboboxes.get(i).getSelectedItem().toString());
						}

					}
				}
				changing = false;
				// checkHeaders();
			}

			void createTable() {
				try {

					tablemodel = new DefaultTableModel(data, new Object[] { "", "", "", "", "Editable" });
					table = new JTable(tablemodel) {

						private static final long serialVersionUID = 1L;

						@Override
						public boolean isCellEditable(final int rowIndex, final int columnIndex) {

							if (editables.containsKey(columnIndex)) {
								return true;
							}
							return false;
						}
					};
					for (int i = 0; i < 40; i++) {
						tablemodel.addRow(new String[] { "", "", "", "" });
					}
					table.addMouseListener(this);
					// columns = new String[tablelength];
					columnModel = table.getColumnModel();
					table.setTableHeader(new EditableHeader(columnModel));

					frame.addMouseListener(this);
					final BrowserColorColumnRenderer columnRenderer = new BrowserColorColumnRenderer(
							new Color(150, 255, 150), Color.black);
					final ComboRenderer renderer = new ComboRenderer(items);
					for (int i = 0; i < columnModel.getColumnCount() - 1; i++) {

						final JComboBox<String> combo = new JComboBox<String>();
						for (int c = 0; c < items.length; c++) {
							combo.addItem(items[c]);
						}
						// combo.setName(""+i);
						combo.addActionListener(comboActionListener);

						final EditableHeaderTableColumn col = (EditableHeaderTableColumn) columnModel.getColumn(i);
						col.setCellRenderer(columnRenderer);
						col.setHeaderValue(combo.getItemAt(0));
						col.setHeaderRenderer(renderer);
						col.setHeaderEditor(new DefaultCellEditor(combo));
						comboboxes.add(combo);
					}
					checkHeaders();
					panel.setBackground(Draw.sidecolor);
					table.addMouseListener(tableMouseListener);

					columnValues.put("Select", -1);
					columnValues.put("Position", -1);
					columnValues.put("Chromosome", -1);
					columnValues.put("Start", -1);
					columnValues.put("End", -1);
					columnValues.put("Sample", -1);
					columnValues.put("Gene", -1);
					columnValues.put("Editable", -1);
					final GridBagConstraints c = new GridBagConstraints();
					
					c.gridx = 0;
					c.gridy = 0;
					c.anchor = GridBagConstraints.NORTHWEST;
					// table.setRowHeight(0, 30);
					// add.setToolTipText("Add column");
					// add.addActionListener(this);

					panel.add(open, c);
					c.gridx++;
					panel.add(write, c);
					c.gridx++;
					panel.add(info, c);
					// c.gridx++;;
					// c.anchor = GridBagConstraints.EAST;
					// panel.add(add,c);

					c.gridx = 0;
					open.addActionListener(this);
					write.addActionListener(this);
					write.setPreferredSize(MainPane.buttonDimension);
					write.setMinimumSize(MainPane.buttonDimension);
					open.setPreferredSize(MainPane.buttonDimension);
					open.setMinimumSize(MainPane.buttonDimension);

					c.gridy++;
					c.gridwidth = 4;
					c.weightx = 1.0;

					c.fill = GridBagConstraints.HORIZONTAL;
					table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					table.setVisible(true);
					// selectortable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					// selectortable.setVisible(true);
					// panel.add(selectortable, c);
					// c.gridy++;
					c.weighty = 1.0;
					c.fill = GridBagConstraints.BOTH;
					tableScroll.getViewport().add(table);
					panel.add(tableScroll, c);
					add(panel, c);

					frame.pack();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}

			void setFonts(final Font menuFont) {
				for (int i = 0; i < this.panel.getComponentCount(); i++) {
					this.panel.getComponent(i).setFont(menuFont);
				}
				if (table != null) {
					table.getTableHeader().setFont(menuFont);
					table.setFont(menuFont);
					// selectortable.getTableHeader().setFont(menuFont);
					// selectortable.setFont(menuFont);
				}
				frame.pack();

			}

			public static void main(final String[] args) {
				final TableBrowser bro = new TableBrowser();

				bro.frame.setVisible(true);
				bro.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			}

			@Override
			public void actionPerformed(final ActionEvent event) {
				if (event.getSource() == write) {
					try {
						File savefile = null;
						final FileDialog fs = new FileDialog(frame, "Save table as...", FileDialog.SAVE);

						fs.setFile("*.tsv");
						fs.setVisible(true);

						while (true) {
							final String filename = fs.getFile();

							if (filename != null) {
								savefile = new File(fs.getDirectory() + "/" + filename);

								if (!Files.isWritable(Paths.get(savefile.getParent()))) {
									MainPane.showError("No permission to write.", "Error");
									continue;
								}

								if (!savefile.getName().contains(".")) {
									savefile = new File(savefile.getAbsolutePath() + ".tsv");
								}
								MainPane.savedir = fs.getDirectory();
								MainPane.writeToConfig("DefaultSaveDir=" + MainPane.savedir);
								final BufferedWriter writer = new BufferedWriter(new FileWriter(savefile));

								for (int i = 0; i < header.size() - 1; i++) {
									writer.write(header + "\n");
								}
								final int columns = table.getColumnCount();

								for (int i = 0; i < table.getRowCount(); i++) {
									for (int j = 0; j < columns; j++) {
										if (j > 0) {
											writer.write("\t");
										}

										if (table.getValueAt(i, j) == null) {
											writer.write("\t");
										} else {
											writer.write("" + table.getValueAt(i, j));
										}
									}
									writer.write("\n");
								}

								writer.close();
								break;
							} else {
								break;
							}
						}
						/*
						 * if(1==1) { return; } JFileChooser chooser = new JFileChooser();
						 * chooser.setDialogTitle("Save table as..."); int returnVal =
						 * chooser.showSaveDialog((Component)this.getParent());
						 * 
						 * if(returnVal == JFileChooser.APPROVE_OPTION) { File outfile =
						 * chooser.getSelectedFile(); if(!outfile.getName().contains(".")) { outfile =
						 * new File(outfile.getCanonicalPath() +".tsv"); } BufferedWriter writer = new
						 * BufferedWriter(new FileWriter(outfile));
						 * 
						 * for(int i = 0; i<header.size()-1; i++) { writer.write(header +"\n"); } int
						 * columns = table.getColumnCount();
						 * 
						 * for(int i=0;i<table.getRowCount(); i++) { for(int j = 0; j<columns; j++) {
						 * if(j > 0) { writer.write("\t"); }
						 * 
						 * if(table.getValueAt(i, j) == null) { writer.write("\t"); } else {
						 * writer.write(""+table.getValueAt(i, j)); } } writer.write("\n"); }
						 * 
						 * writer.close(); }
						 */
					} catch (final Exception ex) {
						ex.printStackTrace();
					}
				} else if (event.getSource() == open) {
					final FileDialog fs = new FileDialog(frame, "Choose a tab separated file", FileDialog.LOAD);
					fs.setDirectory(MainPane.savedir);
					fs.setVisible(true);
					final String filename = fs.getFile();

					if (filename != null) {
						final File addfile = new File(fs.getDirectory() + "/" + filename);
						MainPane.savedir = fs.getDirectory();
						MainPane.writeToConfig("DefaultSaveDir=" + MainPane.savedir);
						if (addfile.exists()) {

							createTable(addfile);
							info.setText("Select correct header values for BED columns. File: " + addfile.getName());
						} else {
							MainPane.showError("File does not exists.", "Error", frame);
						}

					}
				/* 	if (1 == 1) {
						return;
					}
					final JFileChooser chooser = new JFileChooser("");

					chooser.setMultiSelectionEnabled(false);
					// chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					chooser.setAcceptAllFileFilterUsed(false);

					chooser.setDialogTitle("Open file");
					// chooser.setPreferredSize(new Dimension((int)MainPane.screenSize.getWidth()/3,
					// (int)MainPane.screenSize.getHeight()/3));
					final int returnVal = chooser.showOpenDialog((Component) this.getParent());

					if (returnVal == JFileChooser.APPROVE_OPTION) {
						final File file = chooser.getSelectedFile();

						createTable(file);
						info.setText("Select correct header values for columns.");
					} */

				}
				// else if(event.getSource() == add) {
				// TableColumnModel columnModel = table.getColumnModel();
				// EditableHeaderTableColumn col = new EditableHeaderTableColumn(true);
				/*
				 * tablemodel.addColumn("NewColumn");
				 * 
				 * columnModel = table.getColumnModel(); table.setTableHeader(new
				 * EditableHeader(columnModel)); ComboRenderer renderer = new
				 * ComboRenderer(items); BrowserColorColumnRenderer columnRenderer = new
				 * BrowserColorColumnRenderer(new Color(150,255,150), Color.black); for(int i =
				 * 0; i<comboboxes.size(); i++) { EditableHeaderTableColumn col
				 * =(EditableHeaderTableColumn) columnModel.getColumn(i);
				 * col.setHeaderValue(comboboxes.get(i).getSelectedItem());
				 * col.setHeaderRenderer(renderer); col.setHeaderEditor(new
				 * DefaultCellEditor(comboboxes.get(i))); col.setCellRenderer(columnRenderer); }
				 * 
				 * checkHeaders(); table.revalidate();
				 */
				// }
			}

			@Override
			public void mouseClicked(final MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(final MouseEvent arg0) {

				final EditableHeader header = (EditableHeader) table.getTableHeader();
				header.removeEditor();

			}

			@Override
			public void mouseExited(final MouseEvent arg0) {

				final EditableHeader header = (EditableHeader) table.getTableHeader();
				header.removeEditor();
			}

			@Override
			public void mousePressed(final MouseEvent arg0) {

			}

			@Override
			public void mouseReleased(final MouseEvent arg0) {

			}

		}

		class BrowserColorColumnRenderer extends DefaultTableCellRenderer {
			private static final long serialVersionUID = 1L;
			Color bkgndColor, fgndColor;

			public BrowserColorColumnRenderer(final Color bkgnd, final Color foregnd) {
				super();
				bkgndColor = bkgnd;
				fgndColor = foregnd;
			}

			public Component getTableCellRendererComponent(final JTable table, final Object value,
					final boolean isSelected, final boolean hasFocus, final int row, final int column) {
				final Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
						column);
				final EditableHeaderTableColumn col = (EditableHeaderTableColumn) table.getColumnModel()
						.getColumn(column);
				if (!isSelected) {
					if (!col.getHeaderValue().equals("Select")) {
						cell.setBackground(bkgndColor);

					} else {
						cell.setBackground(Color.white);

					}
				}
				return cell;
			}
		}

		class ComboRenderer extends JComboBox implements TableCellRenderer {

			ComboRenderer(final String[] items) {
				for (int i = 0; i < items.length; i++) {
					addItem(items[i]);
				}
			}

			public Component getTableCellRendererComponent(final JTable table, final Object value,
					final boolean isSelected, final boolean hasFocus, final int row, final int column) {
      setSelectedItem(value);
      return this;
    }
  }