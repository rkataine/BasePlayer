/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import htsjdk.tribble.readers.TabixReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;


import base.BasePlayer.*;
import base.BasePlayer.GUI.BedCanvas.bedFeatureFetcher;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.genome.Gene;
import base.BasePlayer.genome.ReferenceSeq;
import base.BasePlayer.genome.Transcript;
import base.BasePlayer.sample.SampleNode;
import base.BasePlayer.variants.VarMaster;
import base.BasePlayer.variants.VarNode;
public class ChromDraw extends JPanel implements MouseMotionListener, MouseListener {

private static final long serialVersionUID = 1L;
		
	public VarNode vardraw = null;
	private String startPhaseCodon, zoomtext, base;
	Runtime instance = Runtime.getRuntime();
	SplitClass splitClass = null;
	static ArrayList<String[]> bandVector = new ArrayList<String[]>();	
	Transcript.Exon selectedExon = null;
	static HashMap<String, Integer> chromPos;
	ArrayList<Double> gerpList = new ArrayList<Double>();	
	BufferedImage chromImage, splitCytoImage, selectImage;
	Graphics2D chromImageBuffer, selectImageBuffer;	
	boolean zoomDrag = false, foundlevel = false, nameDraw = false, lineZoomer;
	final static Color exonBarColor = new Color(20,100,20), forwardExon = new Color(50, 200, 100, 30), reverseExon = new Color(255, 50, 50, 30), seqpaint = new Color(100,200,200,150), highlight = new Color(110, 110, 90, 35);
	ArrayList<Integer> geneLevelMatrix= new ArrayList<Integer>();
	StringBuffer seqBuffer;
	RandomAccessFile chromo;
	private BufferedImage tempImage;
	public BufferedImage cytoImage;
	Graphics2D cytoImageBuffer;
	Rectangle mouseRect = new Rectangle();
	private String[] color;
	private final String[] exonString = new String[8];
	static Color lightGray = new Color(230, 230, 230, 100);
	public static float[] dash = { 5f };
	public static TabixReader exonReader;
	ArrayList<String> exonRemove = new ArrayList<String>();
	private Rectangle2D textWidth;
	private Transcript transcript;
	private Transcript.Exon exon;
	public boolean updateExons = false;
	public static Color backTransparent = new Color(255, 255, 250, 255);
	private Transcript transcriptSelect;
	private Color varColor = new Color(100, 100, 100, 30);
	private final Color gray = new Color(100, 100, 100, 30);
	private boolean seqDrag;
	long timer;
	private char[] array;
	private Transcript.Exon clickedExon;
	private short foundcursor;
	public double bounds = 0;
	private SplitClass clickedSplit = null;
	private int seqstart, seqend, prevExonEnd, zoompostemp, mutcount, baselength, mutScreenPos, phase, exonwidth, genewidth, level, mouseX, bandwidth, Xpos, nextExonStart, screenPos, geneStartPos, geneEndPos, levelEndPos,
	chromScrollvalue = 0, exonInfoWidth, aminoNro, codonStartPos, mouseY=0, pressY=0, memoryUsage = 0, baselevel, toMegabytes = 1048576, startPixel = 0, areaWidth = 0;
	public int cytoHeight = 15, exonDrawY = 20;

	public ChromDraw(final int width, final int height) {
		chromImage = MethodLibrary.toCompatibleImage(new BufferedImage((int) MainPane.screenSize.getWidth(), (int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		chromImageBuffer = (Graphics2D) chromImage.getGraphics();
		selectImage = MethodLibrary.toCompatibleImage(new BufferedImage((int) MainPane.screenSize.getWidth(),	(int) MainPane.screenSize.getHeight(), BufferedImage.TYPE_INT_ARGB));
		selectImageBuffer = (Graphics2D) selectImage.getGraphics();
		selectImageBuffer.setRenderingHints(Draw.rh);
		selectImageBuffer.setFont(BaseVariables.defaultFont);
		chromImageBuffer.setFont(BaseVariables.defaultFont);
		bounds = chromImageBuffer.getFontMetrics().getStringBounds("K", chromImageBuffer).getWidth();
		addMouseMotionListener(this);
		addMouseListener(this);
	}

	void getDrawSeq(final SplitClass split) {
		if (!ReferenceSeq.wait) {
			if (split.getReference() == null) {
				if ((int) split.start - (int) split.viewLength < 0)	seqstart = 0;
				else seqstart = (int) split.start - (int) split.viewLength;

				if ((int) split.end + (int) split.viewLength > split.chromEnd)	seqend = split.chromEnd;
				else seqend = (int) split.end + (int) split.viewLength;

				split.setReference(new ReferenceSeq(split.chrom, seqstart, seqend, MainPane.referenceFile));
			}	else if (split.getReference().getStartPos() > 1 && split.getReference().getStartPos() > split.start - 200) {
				if (split.getReference().getStartPos() - (int) split.viewLength < 1)	seqstart = 1;
				else	seqstart = split.getReference().getStartPos() - (int) split.viewLength;
				split.getReference().appendToStart(seqstart);

			} else if (split.getReference().getEndPos() < split.end + 200) {
				if (split.getReference().getEndPos() + (int) split.viewLength > split.chromEnd)	seqend = split.chromEnd;
				else	seqend = split.getReference().getEndPos() + (int) split.viewLength + 200;
				split.getReference().append(seqend);
			}
		}
	}

	void getReadSeq(final SplitClass split) {
		if (!ReferenceSeq.wait) {
			
			if (split.getReadReference() == null) {
				if ((int) split.start - (int) split.viewLength < 0)	seqstart = 0;
				else	seqstart = (int) split.start - (int) split.viewLength;
				if ((int) split.end + (int) split.viewLength > split.chromEnd)	seqend = split.chromEnd;
				else	seqend = (int) split.end + (int) split.viewLength;

				split.setReadReference(new ReferenceSeq(split.chrom, seqstart, seqend, MainPane.referenceFile));
			}

			else if (split.getReadReference().getStartPos() > 1	&& split.getReadReference().getStartPos() > split.start - 200) {
				if (split.getReadReference().getStartPos() - (int) split.viewLength < 1) seqstart = 1;
				else	seqstart = split.getReadReference().getStartPos() - (int) split.viewLength;
				split.getReadReference().appendToStart(seqstart);
			} else if (split.getReadReference().getEndPos() < split.end + 200) {
				if (split.getReadReference().getEndPos() + (int) split.viewLength > split.chromEnd)	seqend = split.chromEnd;
				else	seqend = split.getReadReference().getEndPos() + (int) split.viewLength + 200;
				split.getReadReference().append(seqend);
			}
		}
	}

	void drawSeq(final SplitClass split) {
		if (split.viewLength <= Settings.readDrawDistance && split.viewLength > 10) {
			Graphics2D exonImage = split.getExonImageBuffer();
			exonImage.setColor(Color.black);

			if (split.getReference() == null || split.getReference().getSeq() == null) return;
			if (MainPane.noreadthread) return;
			chromScrollvalue = MainPane.chromScroll.getVerticalScrollBar().getValue();

			exonImage.fillRect(0,	MainPane.chromScroll.getViewport().getHeight() - (BaseVariables.defaultFontSize * 2 + 11) + chromScrollvalue,	this.getWidth(), BaseVariables.defaultFontSize + 12);

			for (int i = Math.max(0, (int) (split.start - split.getReference().getStartPos() - 1)); i < split.getReference().getSeq().length; i++) {
				try {
					if (split.getReference().getStartPos() + i < split.start - 1) continue;
					else if (split.getReference().getStartPos() + i > split.end - 1) break;
					else {
						if (split.getReference().getSeq()[i] == (byte) 'A') exonImage.setColor(Color.green);
						else if (split.getReference().getSeq()[i] == (byte) 'C') exonImage.setColor(Color.cyan);
						else if (split.getReference().getSeq()[i] == (byte) 'G') exonImage.setColor(Color.orange);
						else if (split.getReference().getSeq()[i] == (byte) 'T') exonImage.setColor(Color.red);
						else exonImage.setColor(Color.gray);

						exonImage.setFont(BaseVariables.seqFont);
						if (exonImage.getFontMetrics().stringWidth("T") < split.pixel) {
							exonImage.drawString(MainPane.getBase.get(split.getReference().getSeq()[i]),	(int) ((split.getReference().getStartPos() + i + 1 - split.start) * split.pixel)	+ (int) (split.pixel / 3),	MainPane.chromScroll.getViewport().getHeight() - (BaseVariables.defaultFontSize + 11)	+ chromScrollvalue);
						} else {
							int width = split.pixel < 1 ? 1 : (int) split.pixel;						
							exonImage.fillRect((int) ((split.getReference().getStartPos() + i - split.start) * split.pixel),	MainPane.chromScroll.getViewport().getHeight() - (BaseVariables.defaultFontSize * 2 + 9)	+ chromScrollvalue,	width, BaseVariables.defaultFontSize * 2 + 10);							
						}
					}
				} catch (final Exception e) {
					System.out.println(split.chrom);
					ErrorLog.addError(e.getStackTrace());
					e.printStackTrace();
				}
			}
			// Middle line
			exonImage.setColor(Color.black);
			exonImage.setStroke(BaseConstants.dashed);
			exonImage.drawLine((Draw.getDrawWidth()) / 2, 0, ((Draw.getDrawWidth())) / 2, MainPane.chromScroll.getViewport().getHeight());
			exonImage.drawLine((int) ((Draw.getDrawWidth()) / 2 + split.pixel), 0, (int) ((Draw.getDrawWidth()) / 2 + split.pixel),	MainPane.chromScroll.getViewport().getHeight());
			exonImage.setStroke(BaseConstants.basicStroke);
			exonImage.drawString(MethodLibrary.formatNumber(getPosition((int) (Draw.getDrawWidth() / 2.0 + split.pixel / 2), split)),	(int) ((Draw.getDrawWidth()) / 2 + split.pixel) + 4,	MainPane.chromScroll.getViewport().getHeight() - (BaseVariables.defaultFontSize * 3));
		} else split.nullRef();
	}

	public int getPosition(final int pixel, final SplitClass split) {
		return (int) (split.start + (pixel / split.pixel));
	}

	void drawSideBar() {
		if (memoryUsage > (int) (((instance.totalMemory() - instance.freeMemory()) / toMegabytes))) {
			if (instance.maxMemory() / toMegabytes < 600) {
				if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 100) {
					System.gc();
					if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 100) {
						MainPane.drawCanvas.clearReads();
						if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory()))	/ toMegabytes < 100) Loader.memory = true;
					}
				}
			} else if (instance.maxMemory() / toMegabytes < 1300) {
				if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 200) {
					System.gc();
					if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 200) {
						MainPane.drawCanvas.clearReads();					
						if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory()))	/ toMegabytes < 200) Loader.memory = true;
					}
				}
			} else if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 300) {
				System.gc();
				if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 300) {
					MainPane.drawCanvas.clearReads();
					if ((instance.maxMemory() - (instance.totalMemory() - instance.freeMemory())) / toMegabytes < 300) Loader.memory = true;
				}
			}
		}
		memoryUsage = (int) (((instance.totalMemory() - instance.freeMemory()) / toMegabytes));
		if ((instance.totalMemory() - instance.freeMemory()) / (double) instance.maxMemory() > 0.8) MainPane.memLabel.setForeground(Color.red);
		else MainPane.memLabel.setForeground(Color.black);

		MainPane.memLabel.setText("" + memoryUsage + " / " + (instance.maxMemory() / toMegabytes) + "MB (" + (int) (MethodLibrary.round((instance.totalMemory() - instance.freeMemory()) / (double) instance.maxMemory(), 2) * 100)	+ "%) ");
	}

	public void drawCyto(final SplitClass split) {
		if (split.getCytoImage() == null) split.setCytoImage(createBands(split));
	
		chromImageBuffer.setColor(backTransparent);
		chromImageBuffer.fillRect(split.chromOffset, 0, Draw.getDrawWidth() + 1, cytoHeight);
		chromImageBuffer.drawImage(split.getCytoImage(), split.chromOffset, 2, Draw.getDrawWidth() + 1,	cytoHeight - 4, null);
		chromImageBuffer.setColor(Color.red);
		chromImageBuffer.setStroke(BaseConstants.doubleStroke);

		startPixel = (int) ((Draw.getDrawWidth()) * (split.start / (double) split.chromEnd))	+ split.chromOffset;
		areaWidth = (int) ((Draw.getDrawWidth()) * split.viewLength / (double) split.chromEnd) - 3;
		if (areaWidth < 1) areaWidth = 1;

		chromImageBuffer.drawRect(startPixel + 1, 1, areaWidth, cytoHeight - 2);
		chromImageBuffer.setColor(Color.black);
		if (split.getChromBands() == null) return;
		for (int i = split.getChromBands().size() - 1; i >= 0; i--) {
			try {
				if ((Integer.parseInt(split.getChromBands().get(i)[2])	- Integer.parseInt(split.getChromBands().get(i)[1]))	* (Draw.getDrawWidth() / (double) split.chromEnd) > bounds	* split.getChromBands().get(i)[3].length()) {
					if (split.getChromBands().get(i)[4].contains(",0,")	|| split.getChromBands().get(i)[4].equals("gpos100")) {
						if (chromImageBuffer.getColor() != Color.white) chromImageBuffer.setColor(Color.white);
						chromImageBuffer.drawString(split.getChromBands().get(i)[3],(int) (Integer.parseInt(split.getChromBands().get(i)[1])	* ((Draw.getDrawWidth()) / (double) split.chromEnd))	+ split.chromOffset + 1,	BaseVariables.defaultFontSize + 2);
						chromImageBuffer.setColor(Color.black);
					} else {
						if (!split.getChromBands().get(i)[3].matches("\\w11(.1)?")) {
							chromImageBuffer.drawString(split.getChromBands().get(i)[3], (int) (Integer.parseInt(split.getChromBands().get(i)[1])	* ((Draw.getDrawWidth()) / (double) split.chromEnd))	+ split.chromOffset + 1,	BaseVariables.defaultFontSize + 2);
						}
					}
				}

				if (!Getter.getInstance().loading() && mouseY < cytoHeight && mouseY > 0) {
					if (mouseX	- split.chromOffset > (int) (Integer.parseInt(split.getChromBands().get(i)[1])	* ((Draw.getDrawWidth()) / (double) split.chromEnd))	&& mouseX - split.chromOffset <= (int) (Integer.parseInt(split.getChromBands().get(i)[2])
									* ((Draw.getDrawWidth()) / (double) split.chromEnd))) {
						chromImageBuffer.setColor(Color.black);
						chromImageBuffer.fillRect(mouseX + 18, 0,	(int) (bounds * split.getChromBands().get(i)[3].length() + 4), cytoHeight);
						chromImageBuffer.setColor(Color.white);
						chromImageBuffer.drawString(split.getChromBands().get(i)[3], mouseX + 20,	BaseVariables.defaultFontSize + 2);
						chromImageBuffer.setColor(Color.black);
					}
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		if (mouseY <= cytoHeight) {
			if (getCursor().getType() != Cursor.TEXT_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		}
	}

	void drawZoom() {
		if (lineZoomer) {
			chromImageBuffer.setColor(Color.black);
			chromImageBuffer.setStroke(BaseConstants.dashed);
			chromImageBuffer.drawLine(MainPane.drawCanvas.pressX, pressY, mouseX, mouseY);
			chromImageBuffer.setStroke(BaseConstants.basicStroke);
		} else if (zoomDrag) {
			if (mouseY <= cytoHeight) {
				chromImageBuffer.setColor(BaseConstants.zoomColor);
				chromImageBuffer.fillRect(MainPane.drawCanvas.pressX, 0, this.mouseX - MainPane.drawCanvas.pressX, cytoHeight);
				return;
			}

			if (this.mouseX - MainPane.drawCanvas.pressX >= 0) {
				chromImageBuffer.setColor(Color.white);
				chromImageBuffer.drawRect(MainPane.drawCanvas.pressX, 0, this.mouseX - MainPane.drawCanvas.pressX,	this.getHeight());
				chromImageBuffer.setColor(Color.black);
				if (Draw.getDrawWidth() - this.mouseX > 200) {
					chromImageBuffer.drawString("" + MethodLibrary.formatNumber((int) ((this.mouseX - MainPane.drawCanvas.pressX) / MainPane.drawCanvas.selectedSplit.pixel))	+ "bp", MainPane.drawCanvas.pressX + (this.mouseX - MainPane.drawCanvas.pressX) + 4, this.mouseY - 35);
					chromImageBuffer.drawString("Right click to cancel zoom",	MainPane.drawCanvas.pressX + (this.mouseX - MainPane.drawCanvas.pressX) + 4, this.mouseY - 6);
				} else {
					zoomtext = "" + MethodLibrary.formatNumber((int) ((this.mouseX - MainPane.drawCanvas.pressX) / MainPane.drawCanvas.selectedSplit.pixel))	+ "bp";
					textWidth = chromImageBuffer.getFontMetrics().getStringBounds(zoomtext, chromImageBuffer);
					this.zoompostemp = (int) ((MainPane.drawCanvas.pressX + (this.mouseX - MainPane.drawCanvas.pressX)) - textWidth.getWidth());
					chromImageBuffer.drawString(zoomtext, this.zoompostemp, this.mouseY - 35);
					textWidth = chromImageBuffer.getFontMetrics().getStringBounds("Right click to cancel zoom",	chromImageBuffer);
					this.zoompostemp = (int) ((MainPane.drawCanvas.pressX + (this.mouseX - MainPane.drawCanvas.pressX))	- textWidth.getWidth()) - 6;
					chromImageBuffer.drawString("Right click to cancel zoom", this.zoompostemp, this.mouseY - 6);
				}
				chromImageBuffer.setColor(BaseConstants.zoomColor);
				chromImageBuffer.fillRect(MainPane.drawCanvas.pressX, 0, this.mouseX - MainPane.drawCanvas.pressX,	this.getHeight());
			} else {
				lineZoomer = true;
				MainPane.drawCanvas.lineZoomer = true;
				zoomDrag = false;
			}
			chromImageBuffer.setStroke(BaseConstants.basicStroke);
		}
	}

	void drawScreen(final Graphics g) {
		
		drawSideBar();
		drawCyto(MainPane.drawCanvas.selectedSplit);
		if (clickedExon != null) drawClickedExon(clickedSplit);

		drawExons(MainPane.drawCanvas.selectedSplit);
		
		for (int s = 0; s < Draw.getSplits().size(); s++) {
			SplitClass split = Draw.getSplits().get(s);
			
			if (split.getExonImage() == null) continue;
			
			chromImageBuffer.drawImage(split.getExonImage(),split.chromOffset, this.cytoHeight, null);
			if (MainPane.ref != null) {
				if (s > 0) chromImageBuffer.drawString("chr" +split.chrom,	split.chromOffset + 4,	this.cytoHeight + BaseVariables.defaultFontSize + 2 + BaseVariables.defaultFontSize / 2);
			} else {
				if (chromImageBuffer.getFont().getSize() != MainPane.chromScroll.getViewport().getHeight() / 2) {
					chromImageBuffer.setFont(new Font("SansSerif", Font.BOLD, MainPane.chromScroll.getViewport().getHeight() / 2));
					chromImageBuffer.setColor(new Color(245, 245, 245));
					this.textWidth = chromImageBuffer.getFontMetrics().getStringBounds("Welcome to BasePlayer",	chromImageBuffer);
				}

				chromImageBuffer.drawString("Welcome to BasePlayer",	(int) (MainPane.chromScroll.getViewport().getWidth() / 2 - this.textWidth.getWidth() / 2),	(int) (MainPane.chromScroll.getViewport().getHeight()	- chromImageBuffer.getFont().getSize() * 0.5));
				chromImageBuffer.setFont(MainPane.menuFont);
			}
			
			if (s > 0) {
				chromImageBuffer.setColor(Color.gray);
				chromImageBuffer.fillRect(Draw.getSplits().get(s).chromOffset - 3, 0, 5, this.getHeight());
				chromImageBuffer.setColor(Color.lightGray);
				chromImageBuffer.fillRect(Draw.getSplits().get(s).chromOffset - 1, 0, 2, this.getHeight());
			}
		
		}

		chromImageBuffer.drawImage(selectImage, MainPane.drawCanvas.selectedSplit.chromOffset, this.cytoHeight, null);

		if (VariantHandler.table != null && VariantHandler.table.hoverVar != null	&& VariantHandler.table.hoverVar.getExons() != null && VariantHandler.table.hoverVar.getExons().get(0).getTranscript().getChrom().equals(MenuBar.chromosomeDropdown.getSelectedItem().toString())) {
			chromImageBuffer.setColor(Color.white);
			chromImageBuffer.fillRect((int) ((VariantHandler.table.hoverVar.getPosition() + 1 - Draw.getSplits().get(0).start) * Draw.getSplits().get(0).pixel) - 2,	17, 4, this.getHeight());
			chromImageBuffer.setColor(Color.black);
			chromImageBuffer.drawLine((int) ((VariantHandler.table.hoverVar.getPosition() + 1 - Draw.getSplits().get(0).start) * Draw.getSplits().get(0).pixel), 17, (int) ((VariantHandler.table.hoverVar.getPosition() + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
		}
		if (VariantHandler.table != null && VariantHandler.table.hoverNode != null && VariantHandler.table.hoverNode.getChrom().equals(MenuBar.chromosomeDropdown.getSelectedItem().toString())) {
			if (VariantHandler.table.hoverNode.getStart() > Draw.getSplits().get(0).start - 1	&& VariantHandler.table.hoverNode.getStart() < Draw.getSplits().get(0).end) {
				chromImageBuffer.setColor(Color.white);
				chromImageBuffer.fillRect((int) ((VariantHandler.table.hoverNode.getStart() + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel) - 2,	17, 4, this.getHeight());
				chromImageBuffer.setColor(Color.black);
				chromImageBuffer.drawLine((int) ((VariantHandler.table.hoverNode.getStart() + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	17, (int) ((VariantHandler.table.hoverNode.getStart() + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
			}
		}

		if (MenuBar.searchChrom.equals(Draw.getSplits().get(0).chrom)) drawSearchInterval();

		if (this.seqDrag && mouseX - MainPane.drawCanvas.pressX > 0) {
			chromImageBuffer.setColor(seqpaint);
			chromImageBuffer.fillRect(MainPane.drawCanvas.pressX,	MainPane.chromScroll.getViewport().getHeight() - (BaseVariables.defaultFontSize),	mouseX - MainPane.drawCanvas.pressX, BaseVariables.defaultFontSize * 2 + 10);
			chromImageBuffer.setColor(Color.black);
		}
		for (int s = 1; s < Draw.getSplits().size(); s++) {
			chromImageBuffer.setColor(Color.white);
			chromImageBuffer.fillRect(Draw.getSplits().get(s).chromOffset + 2,	this.cytoHeight + 2 + BaseVariables.defaultFontSize / 2,	chromImageBuffer.getFontMetrics().stringWidth("chr" + Draw.getSplits().get(s).chrom) + 4,	BaseVariables.defaultFontSize + 4);
			chromImageBuffer.setColor(Color.black);
			chromImageBuffer.drawString("chr" + Draw.getSplits().get(s).chrom,	Draw.getSplits().get(s).chromOffset + 4, this.cytoHeight + BaseVariables.defaultFontSize + 2 + BaseVariables.defaultFontSize / 2);
		}
		drawZoom();
		g.drawImage(chromImage, 0, 0, null);
	}

	void drawSearchInterval() {
		if (MenuBar.searchStart > 0 && MenuBar.searchEnd < 1) {
			if (MenuBar.searchStart > Draw.getSplits().get(0).start	&& MenuBar.searchStart < Draw.getSplits().get(0).end) {
				chromImageBuffer.setColor(Color.black);
				chromImageBuffer.drawLine((int) ((MenuBar.searchStart - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((MenuBar.searchStart - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
				chromImageBuffer.drawLine((int) ((MenuBar.searchStart + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((MenuBar.searchStart + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
				chromImageBuffer.setColor(highlight);
				chromImageBuffer.fillRect((int) ((MenuBar.searchStart - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) Draw.getSplits().get(0).pixel, this.getHeight());
			}
		} else if (MenuBar.searchStart > 0 && MenuBar.searchEnd > 0) {
			chromImageBuffer.setColor(Color.black);
			if (MenuBar.searchStart > Draw.getSplits().get(0).start	&& MenuBar.searchEnd < Draw.getSplits().get(0).end) {
				chromImageBuffer.drawLine((int) ((MenuBar.searchStart - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((MenuBar.searchStart - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
				chromImageBuffer.drawLine((int) ((MenuBar.searchEnd + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((MenuBar.searchEnd + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
				chromImageBuffer.setColor(highlight);
				chromImageBuffer.fillRect((int) ((MenuBar.searchStart - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight,	(int) ((MenuBar.searchEnd + 1 - MenuBar.searchStart) * Draw.getSplits().get(0).pixel), this.getHeight());

			} else if (MenuBar.searchStart > Draw.getSplits().get(0).start	&& MenuBar.searchStart < Draw.getSplits().get(0).end	&& MenuBar.searchEnd > Draw.getSplits().get(0).end) {
				chromImageBuffer.drawLine((int) ((MenuBar.searchStart - Draw.getSplits().get(0).start + 1)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((MenuBar.searchStart - Draw.getSplits().get(0).start + 1)	* Draw.getSplits().get(0).pixel),	this.getHeight());
				chromImageBuffer.setColor(highlight);
				chromImageBuffer.fillRect((int) ((MenuBar.searchStart - Draw.getSplits().get(0).start + 1)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((Draw.getSplits().get(0).end - MenuBar.searchStart)	* Draw.getSplits().get(0).pixel),	this.getHeight());
			} else if (MenuBar.searchStart < Draw.getSplits().get(0).start	&& MenuBar.searchEnd < Draw.getSplits().get(0).end	&& MenuBar.searchEnd > Draw.getSplits().get(0).start) {
				chromImageBuffer.drawLine((int) ((MenuBar.searchEnd + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	cytoHeight, (int) ((MenuBar.searchEnd + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
				chromImageBuffer.setColor(highlight);
				chromImageBuffer.fillRect(0, cytoHeight, (int) ((MenuBar.searchEnd + 1 - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel),	this.getHeight());
			} else if (MenuBar.searchStart < Draw.getSplits().get(0).start	&& MenuBar.searchEnd > Draw.getSplits().get(0).end) {
				chromImageBuffer.setColor(highlight);
				chromImageBuffer.fillRect(0, cytoHeight, (int) (Draw.getSplits().get(0).viewLength * Draw.getSplits().get(0).pixel),	this.getHeight());
			}
		}
	}

	public void paintComponent(final Graphics g) {
		try {
			drawScreen(g);
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}

	static Boolean fetching = false;
	public StringBuffer getSeq(final String chrom, int start, int end, final RandomAccessFile seqchrom) {
		byte[] seqresult;
		try {
			if (chrom == null) return null;
			if (start < 0) {
				start = 0;
				if (end > MenuBar.chromIndex.get(MainPane.refchrom + chrom)[1]) end = MenuBar.chromIndex.get(MainPane.refchrom + chrom)[1].intValue();
			}
			seqresult = new byte[(end - start + 1) + ((end - start) / (MenuBar.chromIndex.get(MainPane.refchrom + chrom)[2].intValue() - 1))];

			if (seqresult.length == 0 || seqresult.length > 200000) return new StringBuffer();

		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
			return new StringBuffer();
		}

		StringBuffer seqBuffer = new StringBuffer();

		try {
			try {
				seqchrom.seek((MenuBar.chromIndex.get(MainPane.refchrom + chrom)[0] + (start)	+ ((start) / MenuBar.chromIndex.get(MainPane.refchrom + chrom)[2].intValue())));
			} catch (final Exception e) {
				e.printStackTrace();
			}
			seqchrom.readFully(seqresult);
			
			if (seqresult[0] == 10) {
				seqchrom.seek((MenuBar.chromIndex.get(MainPane.refchrom + chrom)[0] + (start - 1) + ((start) / MenuBar.chromIndex.get(MainPane.refchrom + chrom)[2].intValue())));
				seqchrom.readFully(seqresult);
			}
	
			for (int i = 0; i < seqresult.length; i++) {
				if (seqresult[i] != 10) {
					seqBuffer.append((char) seqresult[i]);
				}
				if (seqBuffer.length() >= end - start) break;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return seqBuffer;
	}

	static int[] makeLine(final String[] split) {
		if (split.length > 16) {
			return new int[] { Integer.parseInt(split[8]), Integer.parseInt(split[9]), Integer.parseInt(split[12]),	Integer.parseInt(split[13]), Integer.parseInt(split[14]), Integer.parseInt(split[15]),	Integer.parseInt(split[16]), Integer.parseInt(split[17].trim()), Integer.parseInt(split[6]),	Integer.parseInt(split[7]) };
		} else {
			return new int[] { Integer.parseInt(split[8]), Integer.parseInt(split[9]), Integer.parseInt(split[12]),	Integer.parseInt(split[13]), Integer.parseInt(split[14]), Integer.parseInt(split[15].trim()),	Integer.parseInt(split[6]), Integer.parseInt(split[7]) };
		}
	}

	void drawExons(final SplitClass split) {
		try {
		
			if (updateExons) {
				if (bounds * 3 > split.pixel * 3) exonDrawY = BaseVariables.defaultFontSize + 10;
				else exonDrawY = BaseVariables.defaultFontSize * 2 + 10;
				Graphics2D exonImage = split.getExonImageBuffer();
				try {
					exonImage.setColor(backTransparent);
					exonImage.fillRect(0, 0, Draw.getDrawWidth() + 1, this.getHeight());
					level = 1;
					exonImage.setFont(BaseVariables.defaultFont);

					if (split.viewLength <= Settings.readDrawDistance && split.viewLength > 10) {
						try {
							getDrawSeq(split);
						} catch (final Exception ex) {
							ex.printStackTrace();
						}
					}
				
					if (split.getGenes() != null && split.getGenes().size() > 0) {
						if (split.transStart > split.getGenes().size() - 1) split.transStart = 0;
						
						if (split.getGenes().get(split.transStart) != null	&& split.getGenes().get(split.transStart).getEnd() + 100000 < (int) split.start	&& split.transStart < split.getGenes().size() - 2	&& split.getGenes().get(split.transStart + 1).getEnd() + 100000 < (int) split.start) {
							while (split.getGenes().get(split.transStart) != null	&& split.getGenes().get(split.transStart).getEnd() + 100000 < (int) split.start) {
								split.transStart++;
								if (split.transStart > split.getGenes().size() - 1) break;
							}
						} else if (split.getGenes().get(split.transStart) != null	&& split.getGenes().get(split.transStart).getEnd() + 100000 > (int) split.start) {
							while (split.transStart > 0	&& split.getGenes().get(split.transStart).getEnd() + 100000 > (int) split.start) {
								split.transStart--;
							}
						}

						for (int g = split.transStart; g < split.getGenes().size(); g++) {
							if (g < 0) continue;
							Gene gene = split.getGenes().get(g);
							for (int i = 0; i < gene.getTranscripts().size(); i++) {
								transcript = gene.getTranscripts().get(i);

								if (!transcript.getGene().showIsoforms() && !transcript.equals(gene.getLongest())) continue;
								if (transcript.getStart() > (int) split.end) break;
						
								geneStartPos = (int) ((transcript.getStart() + 1 - split.start) * split.pixel);
								geneEndPos = (int) ((transcript.getEnd() - split.start) * split.pixel);
								genewidth = (int) ((transcript.getEnd() - (transcript.getStart())) * split.pixel);

								if (genewidth < 2 && split.viewLength > 10000000) {
									if (transcript.getStrand()) exonImage.setColor(exonBarColor);
									else exonImage.setColor(Color.RED);									
									exonImage.drawLine(geneStartPos, BaseVariables.defaultFontSize / 2,	geneStartPos, 2);
									continue;
								}

								String geneName = transcript.getGenename();
								textWidth = exonImage.getFontMetrics().getStringBounds(geneName, exonImage);

								if (geneEndPos < geneStartPos + textWidth.getWidth()) levelEndPos = (int) (geneStartPos + textWidth.getWidth()) + 5;
								else levelEndPos = geneEndPos + 5;

								if (geneLevelMatrix.isEmpty()) {
									geneLevelMatrix.add(levelEndPos);
									level = 1;
								} else {
									foundlevel = false;
									for (int c = 0; c < geneLevelMatrix.size(); c++) {
										if (geneLevelMatrix.get(c) >= geneStartPos) continue;
										level = c + 1;
										foundlevel = true;
										geneLevelMatrix.set(c, levelEndPos);
										break;										
									}
									if (!foundlevel) {
										geneLevelMatrix.add(levelEndPos);
										level = geneLevelMatrix.size();
									}
								}
								transcript.ypos = (exonDrawY * level) + 2;
								if (genewidth > 2) {
									exonImage.setColor(Color.black);
									if (geneEndPos - geneStartPos > 0) exonImage.drawLine(geneStartPos,	transcript.ypos + exonDrawY / 4, geneEndPos,	transcript.ypos + exonDrawY / 4);
								}
								nameDraw = false;

								for (int e = 0; e < transcript.getExons().length; e++) {
									try {
										if (e > transcript.getExons().length - 1) break;
										exon = transcript.getExons()[e];
									} catch (final Exception ex) {
										ex.printStackTrace();
									}
									if (exon.getEnd() < split.start) continue;
									else if (exon.getStart() > split.end) break;

									screenPos = (int) ((exon.getStart() + 1 - split.start) * split.pixel);
									exonwidth = (int) ((exon.getEnd() - (exon.getStart())) * split.pixel) + 1;
									exon.getRectangle().setBounds(screenPos, transcript.ypos, exonwidth, exonDrawY / 2);

									if (exonwidth < 1) exonwidth = 1;
									exonImage.setColor(Color.black);

									if (!nameDraw && screenPos >= 0) {
										exonImage.drawString(geneName, screenPos, transcript.ypos - 1);
										nameDraw = true;
									}
									if (transcript.getStrand()) exonImage.setColor(exonBarColor);
									else exonImage.setColor(Color.RED);
								
									if (exon.getStartPhase() == -1) exonImage.setColor(Color.lightGray);

									exonImage.fillRect(screenPos, transcript.ypos, exonwidth,	exonDrawY / 2);
									// UTR draw
									exonwidth = (int) ((transcript.getCodingStart() - exon.getStart()) * split.pixel)	+ 1;
									// Start & End exon
									if (exon.getEnd() > transcript.getCodingEnd()	&& exon.getStart() < transcript.getCodingEnd()	&& exon.getEnd() > transcript.getCodingStart()	&& exon.getStart() < transcript.getCodingStart()) {
										exonImage.setColor(Color.lightGray);
										exonwidth = (int) ((transcript.getCodingStart() - exon.getStart())	* split.pixel) + 1;
										exonImage.fillRect(screenPos, transcript.ypos, exonwidth,	exonDrawY / 2);
										screenPos = (int) ((transcript.getCodingEnd() + 1 - split.start) * split.pixel)	+ 1;
										exonwidth = (int) ((exon.getEnd() - transcript.getCodingEnd()) * split.pixel)	+ 1;
										exonImage.fillRect(screenPos, transcript.ypos, exonwidth,	exonDrawY / 2);
									}
									// Start exon
									else if (exon.getStart() < transcript.getCodingStart() && exon.getEnd() > transcript.getCodingStart()) {
										exonImage.setColor(Color.lightGray);
										exonwidth = (int) ((transcript.getCodingStart() - exon.getStart()) * split.pixel) + 1;
										exonImage.fillRect(screenPos, transcript.ypos, exonwidth, exonDrawY / 2);
									}
									// End exon
									else if (exon.getStart() < transcript.getCodingEnd()	&& exon.getEnd() > transcript.getCodingEnd()) {
										exonImage.setColor(Color.lightGray);
										exonwidth = (int) ((exon.getEnd() - transcript.getCodingEnd()) * split.pixel)	+ 1;
										screenPos = (int) ((transcript.getCodingEnd() + 1 - split.start) * split.pixel)	+ 1;
										exonImage.fillRect(screenPos, transcript.ypos, exonwidth,	exonDrawY / 2);
									}

									if (split.viewLength < 200) {
										if (split.getReference() == null) break;
										// FORWARD STRAND aminoacid sequence draw TODO split
										if (transcript.getStrand()) {
											if (split.end > transcript.getCodingStart()	&& split.start < transcript.getCodingEnd()) {
												if (exon.getStartPhase() == -1) continue;
												// Start exon
												else if (exon.getEnd() > transcript.getCodingStart()	&& exon.getStart() < transcript.getCodingStart()) {
													aminoNro = exon.getFirstAmino();

													for (int codon = transcript.getCodingStart(); codon < exon.getEnd()	- exon.getEndPhase(); codon += 3) {
														if (codon >= transcript.getCodingEnd() || (codon - split.getReference().getStartPos()) + 3 > split.getReference().getSeq().length	- 1) break;														
														if (codon - split.getReference().getStartPos() < 0) continue;

														codonStartPos = (int) ((codon + 1 - split.start) * split.pixel);
														exonImage.setColor(Color.white);
														exonImage.drawLine(codonStartPos, transcript.ypos, codonStartPos,	transcript.ypos + this.exonDrawY / 2 - 2);
														
														if (bounds * 3 < split.pixel * 3) {
															exonImage.drawString(MethodLibrary.getAminoAcid(new String(Arrays.copyOfRange(split.getReference().getSeq(),	codon - split.getReference().getStartPos(),	(codon - split.getReference().getStartPos())+ 3))),	codonStartPos + 1,transcript.ypos + this.exonDrawY / 2 - 2);
															if (bounds * 3 < split.pixel * 1.5) exonImage.drawString("" + aminoNro,	codonStartPos + (int) (3 * split.pixel) / 2,	transcript.ypos + this.exonDrawY / 2 - 2);
														}													
														aminoNro++;
													}
													exonImage.drawLine((int) ((exon.getEnd() - exon.getEndPhase() + 1	- split.start) * split.pixel), transcript.ypos,	(int) ((exon.getEnd() - exon.getEndPhase() + 1 - split.start) * split.pixel),	transcript.ypos + this.exonDrawY / 2 - 2);
												} else {
													// Other exons
													aminoNro = exon.getFirstAmino();
													for (int codon = exon.getStart()	+ exon.getStartPhase(); codon < exon.getEnd()	- exon.getEndPhase(); codon += 3) {
														if (codon >= transcript.getCodingEnd() || codon > split.end) break;

														codonStartPos = (int) ((codon + 1 - split.start) * split.pixel);
														exonImage.setColor(Color.white);
														exonImage.drawLine(codonStartPos, transcript.ypos, codonStartPos,	transcript.ypos + (this.exonDrawY / 2 - 2));
														try {
															if (bounds * 3 < split.pixel * 3) {
																if (codon - split.getReference().getStartPos() > 0) {
																	String codonString = new String(Arrays.copyOfRange(split.getReference().getSeq(), codon - split.getReference().getStartPos(),	(codon - split.getReference().getStartPos()) + 3));
																	
																	exonImage.drawString(MethodLibrary.getAminoAcid(codonString.toUpperCase()),	codonStartPos + 1, transcript.ypos + this.exonDrawY / 2 - 2);
																}
																if (bounds * 3 < split.pixel * 1.5) exonImage.drawString("" + aminoNro,	codonStartPos + (int) (3 * split.pixel) / 2, transcript.ypos + this.exonDrawY / 2 - 2);														
															}
														} catch (final Exception ex) {
															ex.printStackTrace();
														}
														aminoNro++;
													}
													exonImage.drawLine((int) ((exon.getEnd() - exon.getEndPhase() + 1 - split.start) * split.pixel),	transcript.ypos, (int) ((exon.getEnd() - exon.getEndPhase() + 1	- split.start) * split.pixel), transcript.ypos + this.exonDrawY / 2 - 2);
												}
											}
										} else {
											// REVERSE STRAND
											if (split.end > transcript.getCodingStart() && split.start < transcript.getCodingEnd()) {
												if (exon.getStartPhase() == -1) continue;
												// Start exon
												else if (exon.getEnd() > transcript.getCodingEnd()	&& exon.getStart() < transcript.getCodingEnd()) {
													aminoNro = 1;
													for (int codon = transcript.getCodingEnd(); codon - 3 >= exon.getStart(); codon -= 3) {													
														if (codon < transcript.getCodingStart() + 1) break;

														if (codon - 3 - split.getReference().getStartPos() < 0 || codon - split.getReference().getStartPos() > split.getReference().getSeq().length - 1) continue;
														
														codonStartPos = (int) ((codon - 2 - split.start) * split.pixel);
														exonImage.setColor(Color.white);
														exonImage.drawLine(codonStartPos,	transcript.ypos, codonStartPos,	transcript.ypos + (this.exonDrawY / 2 - 2));
														if (bounds * 3 < split.pixel * 3) {
															String codonString = new String(Arrays.copyOfRange(split.getReference().getSeq(), codon - 3 - split.getReference().getStartPos(), codon - split.getReference().getStartPos()));
																	
															exonImage.drawString(MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(codonString.toUpperCase())),	codonStartPos + 1, transcript.ypos + this.exonDrawY / 2 - 2);
															if (bounds * 3 < split.pixel * 1.5) exonImage.drawString("" + aminoNro,	codonStartPos + (int) (3 * split.pixel) / 2,	transcript.ypos + this.exonDrawY / 2 - 2);
														}
														aminoNro++;
													}
													exonImage.drawLine((int) ((exon.getEnd() + 1 - exon.getStartPhase()	- split.start) * split.pixel),	transcript.ypos,	(int) ((exon.getEnd() + 1 - exon.getStartPhase()	- split.start) * split.pixel),	transcript.ypos + this.exonDrawY);
												}
												// Other exons
												else {
													aminoNro = exon.getFirstAmino();

													if (split.getReference() == null) continue;
									
													for (int codon = exon.getEnd() - exon.getStartPhase(); codon - 3 >= exon.getStart(); codon -= 3) {
														if (codon - 3 < transcript.getCodingStart()) break;

														codonStartPos = (int) ((codon - 2 - split.start) * split.pixel);
														exonImage.setColor(Color.white);
														exonImage.drawLine(codonStartPos, transcript.ypos, codonStartPos,	transcript.ypos + (this.exonDrawY / 2));
														try {
															if (codon > split.start && codon < split.end) {
																if (bounds * 3 < split.pixel * 3) {
																	String codonString = new String(Arrays.copyOfRange(split.getReference().getSeq(),	codon - 3 - split.getReference().getStartPos(),	codon - split.getReference().getStartPos()));
																	exonImage.drawString(MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(codonString.toUpperCase())),	codonStartPos + 1, transcript.ypos + this.exonDrawY / 2 - 2);
																	if (bounds * 3 < split.pixel * 1.5) exonImage.drawString("" + aminoNro, codonStartPos	+ (int) (3 * split.pixel) / 2, transcript.ypos + this.exonDrawY / 2 - 2);
																}
															}
														} catch (final Exception ex) {
															ex.printStackTrace();
														}
														aminoNro++;
													}
													exonImage.drawLine((int) ((exon.getEnd() + 1 - exon.getStartPhase()	- split.start) * split.pixel),	transcript.ypos,	(int) ((exon.getEnd() + 1 - exon.getStartPhase()	- split.start) * split.pixel), transcript.ypos + this.exonDrawY / 2);
												}
											}
										}
									}
								}
							}
						}
						
						if (split.equals(Draw.getSplits().get(0))) drawMutations((exonDrawY * level) + 2);
						// exon for end

						this.setPreferredSize(new Dimension(MainPane.drawScroll.getViewport().getWidth(),	((geneLevelMatrix.size() + 1) * exonDrawY)));
						this.revalidate();
						geneLevelMatrix.clear();
						updateExons = false;
					}

					if (MainPane.referenceFile != null) {
						if (!Draw.variantcalculator) drawSeq(split);
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
			} else {
				selectImageBuffer.setComposite(Draw.composite);
				selectImageBuffer.fillRect(0, 0, this.getWidth(), this.getHeight());
				selectImageBuffer.setComposite(split.getBackupe());

				if (Getter.getInstance().loading()) return;
				if (split.transStart > split.getGenes().size() - 1) split.transStart = 0;

				for (int g = split.transStart; g < split.getGenes().size(); g++) {
					for (int i = 0; i < split.getGenes().get(g).getTranscripts().size(); i++) {
						transcriptSelect = split.getGenes().get(g).getTranscripts().get(i);
						if (!transcriptSelect.getGene().showIsoforms()	&& !transcriptSelect.equals(split.getGenes().get(g).getLongest())) continue;
						if (transcriptSelect.getStart() > (int) split.end) break;		
						if (transcriptSelect != null && transcriptSelect.getEnd() + 100000 < (int) split.start) continue;
						if (transcriptSelect.getStart() > (int) split.end) break;
						if ((transcriptSelect.getEnd() - transcriptSelect.getStart()) * split.pixel < 2	&& split.viewLength > 10000000) continue;

						for (int e = 0; e < transcriptSelect.getExons().length; e++) {
							exon = transcriptSelect.getExons()[e];
							if (exon.getEnd() < split.start) continue;
							else if (exon.getStart() > split.end) break;
							if (selectedExon == null && exon.getRectangle().intersects(mouseRect)) {
								selectedExon = exon;
								// TODO Splice Amino
								/*
								 * if(viewLength < 200 && (exon.getStartPhase() > 0 || exon.getEndPhase() > 0))
								 * { if(exon.getStartPhase() > 0) { this.startPhaseCodon =
								 * MethodLibrary.getSpliceAmino(MenuBar.chromosomeDropdown.getItemAt(MainPane.
								 * selectedChrom), exon, true); } if(exon.getEndPhase() > 0) {
								 * this.endPhaseCodon =
								 * MethodLibrary.getSpliceAmino(MenuBar.chromosomeDropdown.getItemAt(MainPane.
								 * selectedChrom),exon, false); } }
								 */
							}
							if (selectedExon != null && exon == selectedExon) {
								screenPos = (int) ((exon.getStart() + 1 - split.start) * split.pixel);
								exonwidth = (int) ((exon.getEnd() - (exon.getStart())) * split.pixel) + 1;
								transcript.ypos = (int) selectedExon.getRectangle().getY();
								selectImageBuffer.setColor(Color.red);
								selectImageBuffer.drawRect(screenPos - 1, transcript.ypos - 1, exonwidth + 1,	exonDrawY / 2 + 1);
								// Splice amino draw

								if (split.viewLength < 200 && (exon.getStartPhase() > 0 || exon.getEndPhase() > 0)) {
									selectImageBuffer.setColor(Color.black);
									// Forward strand
									if (transcriptSelect.getStrand()) {
										if (exon.getStartPhase() > 0) selectImageBuffer.drawString(startPhaseCodon + " " + (exon.getFirstAmino() - 1), (int) (screenPos - ((3 - exon.getStartPhase()) * split.pixel)), transcript.ypos + exonDrawY);
										if (exon.getEndPhase() > 0) {
										/* 	if (exon.getFirstAmino() == 1) {
												lastAmino = ((exon.getEnd() - transcriptSelect.getCodingStart()) / 3)	+ 1;
											} else {
												lastAmino = ((exon.getFirstAmino()	+ (exon.getEnd() - (exon.getStart() + exon.getStartPhase()))	/ 3));
											} */
											// selectImageBuffer.drawString(endPhaseCodon +" " +lastAmino,
											// (int)((exon.getEnd()+1-split.start)*split.pixel)+4,
											// transcript.ypos+exonDrawY/2+4);
										}
									}
									// Reverse strand
									else {
										if (exon.getStartPhase() > 0) {
											selectImageBuffer.drawString(startPhaseCodon + " " + (exon.getFirstAmino() - 1),	(int) ((exon.getEnd() + 1 - split.start) * split.pixel) + 4,	transcript.ypos + exonDrawY / 2 + 4);
										}
										if (exon.getEndPhase() > 0) {
										/* 	if (exon.getFirstAmino() == 1) {
												lastAmino = ((transcriptSelect.getCodingEnd() - exon.getStart()) / 3)	+ 1;
											} else {
												lastAmino = ((exon.getFirstAmino()+ (exon.getEnd() - (exon.getStart() + exon.getStartPhase()))	/ 3));
											} */
											// selectImageBuffer.drawString(endPhaseCodon +" " +lastAmino,
											// (int)(screenPos-((3-exon.getEndPhase())*split.pixel)),
											// transcript.ypos+exonDrawY);
										}
									}
								}
							}
						}
					}
				}
				// select exon end

				if (selectedExon != null && !selectedExon.equals(clickedExon)) {
					if (getCursor().getType() != Cursor.HAND_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					screenPos = this.mouseRect.x;
					transcript.ypos = (int) selectedExon.getRectangle().getY();
					exonString[0] = "Exon " + selectedExon.getNro();
					if (selectedExon.getTranscript().getGene().getStrand()) exonString[1] = selectedExon.transcript.getGenename() + " (forward strand)";
					else exonString[1] = selectedExon.transcript.getGenename() + " (reverse strand)";
					exonString[2] = selectedExon.transcript.getENSG();
					exonString[3] = selectedExon.transcript.getENST();
					exonString[4] = selectedExon.transcript.getBiotype();
					exonString[5] = selectedExon.transcript.getUniprot();
					if (selectedExon.transcript.getDescription().length() < 50) exonString[6] = selectedExon.transcript.getDescription();
					else exonString[6] = selectedExon.transcript.getDescription().substring(0, 50) + "...";

					if (selectedExon.transcript.getGene().getTranscripts().size() > 1) {
						if (selectedExon != null && selectedExon.transcript.getGene().showIsoforms()) exonString[7] = "Right click to collapse isoforms.";
						else exonString[7] = "Right click to expand all isoforms.";
					} else exonString[7] = "No other isoforms.";
				
					exonInfoWidth = (int) Draw.getSplits().get(0).getExonImageBuffer().getFontMetrics().stringWidth(exonString[maxWidth(exonString)]) + 2;
					selectImageBuffer.setColor(Color.white);
					selectImageBuffer.fillRect(screenPos, transcript.ypos + exonDrawY, exonInfoWidth,	(int) (textWidth.getHeight() * (exonString.length)) + 2);
					selectImageBuffer.setColor(Color.black);
					selectImageBuffer.drawRect(screenPos - 1, transcript.ypos + exonDrawY - 1, exonInfoWidth + 2, (int) (textWidth.getHeight() * (exonString.length)) + 4);

					for (int i = 0; i < exonString.length; i++) {
						selectImageBuffer.drawString(exonString[i], screenPos + 2,	(int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1)));
					}
				}
				if (selectedExon != null && !selectedExon.getRectangle().intersects(mouseRect) && mouseY > cytoHeight) {
					selectedExon = null;
					if (getCursor().getType() != Cursor.DEFAULT_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		} catch (final Exception ex) {
			ex.printStackTrace();
			return;
		}
	}

	void drawClickedExon(final SplitClass split) {
		if (clickedExon == null) return;
		screenPos = (int) clickedExon.getRectangle().getX();
		if (screenPos < 12) screenPos = 12;
		transcript.ypos = (int) clickedExon.getRectangle().getY();
		exonString[0] = "Exon " + clickedExon.getNro();
		exonString[1] = clickedExon.transcript.getGenename();
		if (clickedExon.transcript.getENSG().contains("ENS")) exonString[2] = clickedExon.transcript.getENSG() + " (view in Ensembl)";
		else if (clickedExon.transcript.getENSG().startsWith("GeneID")) exonString[2] = clickedExon.transcript.getENSG() + " (view in RefSeq)";
		else exonString[2] = clickedExon.transcript.getENSG();

		if (clickedExon.transcript.getENST().contains("ENS")) exonString[3] = clickedExon.transcript.getENST() + " (view in Ensembl)";

		exonString[4] = clickedExon.transcript.getBiotype();
		exonString[5] = "View in GeneCards";

		exonString[6] = clickedExon.transcript.getDescription();
		if (clickedExon.transcript.getGene().getTranscripts().size() > 1) {
			if (clickedExon != null && clickedExon.transcript.getGene().showIsoforms()) exonString[7] = "Right click to collapse isoforms.";
			else exonString[7] = "Right click to expand all isoforms.";
		} else exonString[7] = "";
		Graphics2D exonImage = split.getExonImageBuffer();
		exonInfoWidth = (int) exonImage.getFontMetrics().stringWidth(exonString[maxWidth(exonString)]) + 2;
		exonImage.setColor(Color.white);
		exonImage.fillRect(screenPos, transcript.ypos + exonDrawY, exonInfoWidth,	(int) (textWidth.getHeight() * (exonString.length)) + 2);
		exonImage.setColor(Color.black);
		exonImage.drawRect(screenPos - 1, transcript.ypos + exonDrawY - 1, exonInfoWidth + 2, (int) (textWidth.getHeight() * (exonString.length)) + 4);
		foundcursor = -1;
		for (int i = 0; i < exonString.length - 1; i++) {
			if (mouseRect.getX() > screenPos && mouseRect.getX() < screenPos + exonInfoWidth) {
				if (i < 6 && mouseRect.getY() > (int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i))	&& mouseRect.getY() < (int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1))) {
					exonImage.setFont(MainPane.menuFontBold);
					foundcursor = (short) i;
					if (getCursor().getType() != Cursor.HAND_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				} else exonImage.setFont(BaseVariables.defaultFont);
			} else exonImage.setFont(BaseVariables.defaultFont);

			if (exonImage.getFont().equals(BaseVariables.seqFont)) {
				if (i == 0) exonImage.drawString(exonString[i] + "  (zoom to exon)", screenPos + 2, (int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1)));
				else if (i == 1) exonImage.drawString(exonString[i] + "  (zoom to gene)", screenPos + 2,	(int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1)));
				else if (i == 2 || i == 3) exonImage.drawString(exonString[i], screenPos + 2,	(int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1)));
				else if (i >= 4) exonImage.drawString(exonString[i], screenPos + 2, (int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1)));					
			} else exonImage.drawString(exonString[i], screenPos + 2, (int) (transcript.ypos + exonDrawY + (textWidth.getHeight()) * (i + 1)));
		}	
		if (foundcursor < 0 && selectedExon == null && mouseY > cytoHeight	&& mouseY < MainPane.chromScroll.getViewport().getHeight() - 15) setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));		
	}

	void drawMutations(final int ylevel) {
		try {
			if (MainPane.drawCanvas.selectedSplit.viewLength > 1000000) return;			
				vardraw = VarMaster.getInstance().searchFirstVariantToDraw(Draw.getSplits().get(0).chrom, (int)Draw.getSplits().get(0).start);
				
				while (vardraw != null && vardraw.getPosition() < Draw.getSplits().get(0).end + 1) {
					baselevel = 0;					
					if (vardraw.vars == null) continue;

					for (Map.Entry<String, ArrayList<SampleNode>> entry : vardraw.vars.entrySet()) {	
						if (vardraw.hideNodeVar(entry)) continue;
						mutcount = 0;
						for (int i = 0; i < entry.getValue().size(); i++) {
							if (!entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) mutcount++;
						}
						if (mutcount == 0) continue;
						baselevel++;
						base = entry.getKey();
						int posPlus = vardraw.indel && entry.getKey().length() > 1 ? 2 : 1;						
						mutScreenPos = (int) ((vardraw.getPosition() + posPlus - Draw.getSplits().get(0).start)	* Draw.getSplits().get(0).pixel);						
						if (vardraw.coding) {
							if (base.length() > 1) varColor = forwardExon;								
							else varColor = reverseExon;
							Draw.getSplits().get(0).getExonImageBuffer().setColor(varColor);							
							Draw.getSplits().get(0).getExonImageBuffer().fillRect(mutScreenPos, 0, (int) Draw.getSplits().get(0).pixel + 1, this.getHeight());
						} else {						
							varColor = gray;							
							Draw.getSplits().get(0).getExonImageBuffer().setColor(varColor);							
							Draw.getSplits().get(0).getExonImageBuffer().fillRect(mutScreenPos, 0,	(int) Draw.getSplits().get(0).pixel + 1, this.getHeight());

							if (Draw.getSplits().get(0).viewLength < 200) {
								Draw.getSplits().get(0).getExonImageBuffer().setFont(BaseVariables.seqFont);
								Draw.getSplits().get(0).getExonImageBuffer().setColor(Color.black);
								if (base.length() > 1) Draw.getSplits().get(0).getExonImageBuffer().drawString(mutcount + " " + base,	mutScreenPos, 27 * baselevel);
								else Draw.getSplits().get(0).getExonImageBuffer().drawString(mutcount + " " + MainPane.getBase.get(vardraw.getRefBase()) + "->" + base, mutScreenPos, 27 * baselevel);
								
								Draw.getSplits().get(0).getExonImageBuffer().setFont(BaseVariables.defaultFont);
							}
						}

						if (Draw.getSplits().get(0).viewLength < 200 && vardraw.getExons() != null) {
							// TODO draw aminochanges
							Draw.getSplits().get(0).getExonImageBuffer().setFont(BaseVariables.seqFont);
							Draw.getSplits().get(0).getExonImageBuffer().setColor(Color.black);

							for (int i = 0; i < vardraw.getExons().size(); i++) {
								if (vardraw.getExons().get(i).getTranscript().getGene().showIsoforms()) {
									Draw.getSplits().get(0).getExonImageBuffer().drawString(mutcount + " " + getChange(vardraw, base, vardraw.getExons().get(i)),	mutScreenPos,	vardraw.getExons().get(i).getTranscript().ypos + (exonDrawY) * baselevel);
								} else {
									if (vardraw.getExons().get(i).getTranscript().equals(vardraw.getExons().get(i).getTranscript().getGene().getLongest())) {
										Draw.getSplits().get(0).getExonImageBuffer().drawString(mutcount + " " + getChange(vardraw, base, vardraw.getExons().get(i)),	mutScreenPos, vardraw.getExons().get(i).getTranscript().ypos	+ (exonDrawY) * baselevel);
									}
								}
							}
						}
						Draw.getSplits().get(0).getExonImageBuffer().setFont(BaseVariables.defaultFont);
					}
					vardraw = vardraw.getNextVisible();
				}
				vardraw = null;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		Draw.getSplits().get(0).getExonImageBuffer().setFont(BaseVariables.defaultFont);
	}

	String getChange(final VarNode node, final String base, final Transcript.Exon exon) {
		try {
			
			if (exon == null) return "";
			//int aminopos = 0;
			if (base.length() > 1) {
				/* if (exon.getTranscript().getStrand()) {
					if (exon.getFirstAmino() == 1) aminopos = (node.getPosition() + 1 - exon.getTranscript().getCodingStart()) / 3 + 1;
					//else aminopos = (node.getPosition() + 1 - (exon.getStart() + exon.getStartPhase())) / 3	+ exon.getFirstAmino();
				} else {
					if (exon.getFirstAmino() == 1) aminopos = (exon.getTranscript().getCodingEnd() - (node.getPosition() + 1)) / 3 + 1;
					//else aminopos = ((exon.getEnd() - exon.getStartPhase() - 1) - (node.getPosition() + 1)) / 3	+ exon.getFirstAmino();
				} */
				int baselength = base.substring(3).matches("\\d+") ? Integer.parseInt(base.substring(3)) : base.substring(3).length();			

				if (node.getPosition() + 1 < exon.getTranscript().getCodingStart()	|| node.getPosition() + 1 > exon.getTranscript().getCodingEnd()) return base + " (UTR)";

				if (node.getPosition() + 1 > exon.getEnd()) {
					if (node.getPosition() + 2 - exon.getEnd() > 2) {
						return "";
					}
					return "spl" + (node.getPosition() + 2 - exon.getEnd()) + "-" + base;
				} else if (node.getPosition() + 1 < exon.getStart()) {

					if (base.contains("ins")) {
						return "spl" + (exon.getStart() - (node.getPosition() + 1)) + "-" + base;
					} else {
						if (((node.getPosition() + 1 + baselength) - exon.getStart()) % 3 == 0) {
							return "spl-" + base + "-if";
						} else {
							return "spl-" + base + "-fs";
						}
					}
				} else if (base.contains("del") && node.getPosition() + 1 + baselength >= exon.getEnd()) {

					if ((baselength - (node.getPosition() + 1 + baselength - exon.getEnd())) % 3 == 0) {
						return "if-spl-" + base;
					} else {
						return "fs-spl-" + base;
					}

				} else if (baselength % 3 == 0) {

					return "if-" + base;
				} else {
					return "fs-" + base;
				}
			}

			if (node.getPosition() < exon.getTranscript().getCodingStart()	|| node.getPosition() >= exon.getTranscript().getCodingEnd()) {
				if (node.getPosition() >= exon.getEnd()) {
					return "spl" + (node.getPosition() + 1 - exon.getEnd()) + " (UTR)";
				} else if (node.getPosition() < exon.getStart()) {
					return "spl" + (exon.getStart() - (node.getPosition())) + " (UTR)";
				} else {
					return base + " (UTR)";
				}
			}
			if (node.getPosition() >= exon.getEnd()) {
				return "spl" + (node.getPosition() + 1 - exon.getEnd());
			} else if (node.getPosition() < exon.getStart()) {
				return "spl" + (exon.getStart() - (node.getPosition()));
			}
			if (exon.getTranscript().getStrand()) {

				if (exon.getFirstAmino() == 1) {
					phase = (node.getPosition() - exon.getTranscript().getCodingStart()) % 3;
					//aminopos = (node.getPosition() - exon.getTranscript().getCodingStart()) / 3 + 1;
				} else {
					phase = (node.getPosition() - (exon.getStart() + exon.getStartPhase())) % 3;
					//aminopos = (node.getPosition() - (exon.getStart() + exon.getStartPhase())) / 3 + exon.getFirstAmino();
				}
				// SPLICE CODON
				if (phase < 0 || (exon.getEnd() - node.getPosition() <= exon.getEndPhase())) {

					if (exon.getEnd() - node.getPosition() <= exon.getEndPhase()) {
						if (phase == 1) {
							if (node.getCodon() == null) {
								nextExonStart = exon.getTranscript().getExons()[exon.getNro()].getStart();
								node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(), MainPane.referenceFile).append(this.getSeq(node.getChrom(), nextExonStart, nextExonStart + 1,	MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(new String(array));
						} else {
							if (exon.getEndPhase() == 2) {
								if (node.getCodon() == null) {
									nextExonStart = exon.getTranscript().getExons()[exon.getNro()].getStart();
									node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(),	MainPane.referenceFile).append(this.getSeq(node.getChrom(), nextExonStart, nextExonStart + 1,	MainPane.referenceFile))));
								}
								array = node.getCodon().toCharArray();
								array[phase] = base.charAt(0);
								return MethodLibrary.getAminoAcid(new String(array));
							} else {
								if (node.getCodon() == null) {
									nextExonStart = exon.getTranscript().getExons()[exon.getNro()].getStart();
									node.setCodon(new String(this
											.getSeq(node.getChrom(), exon.getEnd() - 1, exon.getEnd(),
													MainPane.referenceFile)
											.append(this.getSeq(node.getChrom(), nextExonStart, nextExonStart + 2,
													MainPane.referenceFile))));
								}
								array = node.getCodon().toCharArray();
								array[phase] = base.charAt(0);
								return MethodLibrary.getAminoAcid(new String(array));
							}

						}
						// System.out.println(node.getChrom() +":" +node.getPosition() +" " +phase);

					} else if (phase == -2) {
						phase = 1;
						if (node.getCodon() == null) {
							prevExonEnd = exon.getTranscript().getExons()[exon.getNro() - 2].getEnd();
							node.setCodon(new String(
									this.getSeq(node.getChrom(), prevExonEnd - 1, prevExonEnd, MainPane.referenceFile)
											.append(this.getSeq(node.getChrom(), exon.getStart(), exon.getStart() + 2,
													MainPane.referenceFile))));
						}
						array = node.getCodon().toCharArray();
						array[phase] = base.charAt(0);
						return MethodLibrary.getAminoAcid(new String(array));

					} else {
						phase = 2;
						if (node.getPosition() == exon.getStart()) {
							if (node.getCodon() == null) {
								prevExonEnd = exon.getTranscript().getExons()[exon.getNro() - 2].getEnd();
								node.setCodon(new String(
										this.getSeq(node.getChrom(), prevExonEnd - 2, prevExonEnd, MainPane.referenceFile)
												.append(this.getSeq(node.getChrom(), exon.getStart(),
														exon.getStart() + 1, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(new String(array));
						} else {
							if (node.getCodon() == null) {
								prevExonEnd = exon.getTranscript().getExons()[exon.getNro() - 2].getEnd();
								node.setCodon(new String(
										this.getSeq(node.getChrom(), prevExonEnd - 1, prevExonEnd, MainPane.referenceFile)
												.append(this.getSeq(node.getChrom(), exon.getStart(),
														exon.getStart() + 2, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(new String(array));
						}
					}
				} else {

					if (node.getCodon() == null) node.setCodon(new String(this.getSeq(exon.getTranscript().getChrom(),	node.getPosition() - phase, node.getPosition() - phase + 3, MainPane.referenceFile)));
				
					array = node.getCodon().toCharArray();
					array[phase] = base.charAt(0);
					return MethodLibrary.getAminoAcid(new String(array));
				}
			} else {

				if (exon.getFirstAmino() == 1) {
					phase = (exon.getTranscript().getCodingEnd() - node.getPosition()) % 3;
					//aminopos = (exon.getTranscript().getCodingEnd() - (node.getPosition() + 1)) / 3 + 1;

				} else {
					phase = ((exon.getEnd() - exon.getStartPhase()) - node.getPosition()) % 3;
					//aminopos = ((exon.getEnd() - exon.getStartPhase() - 1) - node.getPosition()) / 3 + exon.getFirstAmino();
				}
				if (phase > 0) phase = 3 - phase;

				if (phase < 0 || (phase == 0 && (exon.getEnd() - node.getPosition() < 3))	|| (node.getPosition() - exon.getStart() < exon.getEndPhase())) {

					if (node.getPosition() - exon.getStart() < exon.getEndPhase()) {
						if (exon.getEndPhase() == 1) {
							if (node.getCodon() == null) {
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length	- exon.getNro() - 1].getEnd();
								node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 2, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(),	exon.getStart() + 1, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
						} else {
							if (node.getCodon() == null) {
								if (exon.getTranscript().getExons().length - exon.getNro() - 1 < 0) return "";
						
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length	- exon.getNro() - 1].getEnd();
								node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 1, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(),	exon.getStart() + 2, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
						}
					} else if (phase == -1) {
						phase = 1;
						if (node.getCodon() == null) {
							prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length - exon.getNro() + 1].getStart();
							node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(), MainPane.referenceFile).append(this.getSeq(node.getChrom(), prevExonEnd, prevExonEnd + 1,	MainPane.referenceFile))));
						}
						array = node.getCodon().toCharArray();
						array[phase] = base.charAt(0);
						return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
					} else {
						if (exon.getEnd() - node.getPosition() == 2) {

							if (node.getCodon() == null) {
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length
										- exon.getNro() + 1].getStart();
								node.setCodon(new String(this
										.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(), MainPane.referenceFile)
										.append(this.getSeq(node.getChrom(), prevExonEnd, prevExonEnd + 1,
												MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));

						} else {
							if (node.getCodon() == null) {
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length
										- exon.getNro() + 1].getStart();
								node.setCodon(new String(this
										.getSeq(node.getChrom(), exon.getEnd() - 1, exon.getEnd(), MainPane.referenceFile)
										.append(this.getSeq(node.getChrom(), prevExonEnd, prevExonEnd + 2,
												MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));

						}
					}

				} else {
					if (node.getCodon() == null) {
						node.setCodon(new String(this.getSeq(exon.getTranscript().getChrom(),
								node.getPosition() - phase, node.getPosition() - phase + 3, MainPane.referenceFile)));
					}

					array = node.getCodon().toCharArray();
					array[phase] = base.charAt(0);
					return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
				}
			}
		} catch (final Exception e) {
			// System.out.println(node.getExons().get(0).getTranscript().getGenename() +" "
			// +node.getPosition());
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
			return "";
		}

	}

	public String getAminoChange(final VarNode node, final String base, final Transcript.Exon exon) {
		try {			
			if (exon == null) return "";
			int aminopos = 0;
			String stream = "upstream";
			if (node.getPosition() < exon.getEnd() && !exon.getTranscript().getStrand()) stream = "downstream";
			if (node.getPosition() > exon.getStart() && exon.getTranscript().getStrand()) stream = "downstream";
			if (base.length() > 1) {
				if (exon.getTranscript().getStrand()) {
					if (exon.getFirstAmino() == 1) aminopos = (node.getPosition() + 1 - exon.getTranscript().getCodingStart()) / 3 + 1;
					else aminopos = (node.getPosition() + 1 - (exon.getStart() + exon.getStartPhase())) / 3	+ exon.getFirstAmino();					
				} else {
					if (exon.getFirstAmino() == 1) aminopos = (exon.getTranscript().getCodingEnd() - (node.getPosition() + 1)) / 3 + 1;
					else aminopos = ((exon.getEnd() - exon.getStartPhase() - 1) - (node.getPosition() + 1)) / 3	+ exon.getFirstAmino();					
				}
			
				baselength = base.substring(3).matches("\\d+") ? Integer.parseInt(base.substring(3)) : base.substring(3).length();
			
				if (node.getPosition() + 1 < exon.getTranscript().getCodingStart()	|| node.getPosition() + 1 > exon.getTranscript().getCodingEnd()) return base + " (UTR)";
				
				if (node.getPosition() + 1 > exon.getEnd()) {
					//if (node.getPosition() + 2 - exon.getEnd() > 2) return "";
					return "spl" + (node.getPosition() + 2 - exon.getEnd()) + "-" + base +"-" +stream;
				} else if (node.getPosition() + 1 < exon.getStart()) {
					if (base.contains("ins")) return "spl" + (exon.getStart() - (node.getPosition() + 1)) + "-" + base +"-" +stream;
					else {
						if (((node.getPosition() + 1 + baselength) - exon.getStart()) % 3 == 0) return "spl-" + base + "-if" +"-" +stream;
						else return "spl-" + base + "-fs-" +stream;
					}
				} else if (base.contains("del") && node.getPosition() + 1 + baselength >= exon.getEnd()) {
					if ((baselength - (node.getPosition() + 1 + baselength - exon.getEnd())) % 3 == 0) {
						return aminopos + "-if-spl-" + base +"-" +stream;
					} else {
						return aminopos + "-fs-spl-" + base +"-" +stream;
					}
				} else if (baselength % 3 == 0) {
					return aminopos + "-if-" + base;
				} else {
					return aminopos + "-fs-" + base;
				}
			}
			if (node.getPosition() < exon.getTranscript().getCodingStart()	|| node.getPosition() >= exon.getTranscript().getCodingEnd()) {
				if (node.getPosition() >= exon.getEnd()) {
					return "spl" + (node.getPosition() + 1 - exon.getEnd()) + " (UTR)" +"-" +stream;
				} else if (node.getPosition() < exon.getStart()) {
					return "spl" + (exon.getStart() - (node.getPosition())) + " (UTR)" +"-" +stream;
				} else {
					return base + " (UTR)";
				}
			}
			if (node.getPosition() >= exon.getEnd()) {
				return "spl" + (node.getPosition() + 1 - exon.getEnd()) +"-" +stream;
			} else if (node.getPosition() < exon.getStart()) {
				return "spl" + (exon.getStart() - (node.getPosition())) +"-" +stream;
			}

			if (exon.getTranscript().getStrand()) {
				if (exon.getFirstAmino() == 1) {
					phase = (node.getPosition() - exon.getTranscript().getCodingStart()) % 3;
					aminopos = (node.getPosition() - exon.getTranscript().getCodingStart()) / 3 + 1;
				} else {
					phase = (node.getPosition() - (exon.getStart() + exon.getStartPhase())) % 3;
					aminopos = (node.getPosition() - (exon.getStart() + exon.getStartPhase())) / 3	+ exon.getFirstAmino();
				}
				// SPLICE CODON
				if (phase < 0 || (exon.getEnd() - node.getPosition() <= exon.getEndPhase())) {
					if (exon.getNro() >= exon.getTranscript().getExons().length) return "";
					
					if (exon.getEnd() - node.getPosition() <= exon.getEndPhase()) {
						if (phase == 1) {
							if (node.getCodon() == null || node.phase != 1) {
								node.phase = 1;
								nextExonStart = exon.getTranscript().getExons()[exon.getNro()].getStart();
								node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(), MainPane.referenceFile).append(this.getSeq(node.getChrom(), nextExonStart, nextExonStart + 1,	MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(node.getCodon()) + (aminopos)	+ MethodLibrary.getAminoAcid(new String(array));
						} else {
							if (exon.getEndPhase() == 2) {
								if (node.getCodon() == null || node.phase != 2) {
									node.phase = 2;
									nextExonStart = exon.getTranscript().getExons()[exon.getNro()].getStart();
									node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(),	MainPane.referenceFile).append(this.getSeq(node.getChrom(), nextExonStart, nextExonStart + 1,	MainPane.referenceFile))));
								}
								array = node.getCodon().toCharArray();
								array[phase] = base.charAt(0);
								return MethodLibrary.getAminoAcid(node.getCodon()) + (aminopos)	+ MethodLibrary.getAminoAcid(new String(array));
							} else {
								if (node.getCodon() == null || node.phase != 0) {
									node.phase = 0;
									nextExonStart = exon.getTranscript().getExons()[exon.getNro()].getStart();
									node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 1, exon.getEnd(),	MainPane.referenceFile).append(this.getSeq(node.getChrom(), nextExonStart, nextExonStart + 2,	MainPane.referenceFile))));
								}
								array = node.getCodon().toCharArray();
								array[phase] = base.charAt(0);
								return MethodLibrary.getAminoAcid(node.getCodon()) + (aminopos)	+ MethodLibrary.getAminoAcid(new String(array));
							}
						}
					} else if (phase == -2) {
						phase = 1;
						if (node.getCodon() == null || node.phase != -2) {
							node.phase = -2;
							prevExonEnd = exon.getTranscript().getExons()[exon.getNro() - 2].getEnd();
							node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 1, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(), exon.getStart() + 2,	MainPane.referenceFile))));
						}
						array = node.getCodon().toCharArray();
						array[phase] = base.charAt(0);
						return MethodLibrary.getAminoAcid(node.getCodon()) + (aminopos - 1)	+ MethodLibrary.getAminoAcid(new String(array));
					} else {
						phase = 2;
						if (node.getPosition() == exon.getStart()) {
							if (node.getCodon() == null || node.phase != 2) {
								node.phase = 2;
								prevExonEnd = exon.getTranscript().getExons()[exon.getNro() - 2].getEnd();
								node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 2, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(),	exon.getStart() + 1, MainPane.referenceFile))));
							}

							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(node.getCodon()) + (aminopos - 1)	+ MethodLibrary.getAminoAcid(new String(array));
						} else {
							if (node.getCodon() == null || node.phase != 0) {
								node.phase = 0;
								prevExonEnd = exon.getTranscript().getExons()[exon.getNro() - 2].getEnd();
								node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 1, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(), exon.getStart() + 2, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(node.getCodon()) + (aminopos - 1)	+ MethodLibrary.getAminoAcid(new String(array));
						}
					}
				} else {
					if (node.getCodon() == null || node.phase != phase) {
						node.phase = (short) phase;
						node.setCodon(new String(this.getSeq(exon.getTranscript().getChrom(),	node.getPosition() - phase, node.getPosition() - phase + 3, MainPane.referenceFile)));
					}
					array = node.getCodon().toCharArray();
					array[phase] = base.charAt(0);
					return MethodLibrary.getAminoAcid(node.getCodon()) + aminopos	+ MethodLibrary.getAminoAcid(new String(array));
				}
			} else {
				if (exon.getFirstAmino() == 1) {
					phase = (exon.getTranscript().getCodingEnd() - node.getPosition()) % 3;
					aminopos = (exon.getTranscript().getCodingEnd() - (node.getPosition() + 1)) / 3 + 1;
				} else {
					phase = ((exon.getEnd() - exon.getStartPhase()) - node.getPosition()) % 3;
					aminopos = ((exon.getEnd() - exon.getStartPhase() - 1) - node.getPosition()) / 3	+ exon.getFirstAmino();
				}
				if (phase > 0) phase = 3 - phase;
				
				if (phase < 0 || (phase == 0 && (exon.getEnd() - node.getPosition() < 3))	|| (node.getPosition() - exon.getStart() < exon.getEndPhase())) {
					if (node.getPosition() - exon.getStart() < exon.getEndPhase()) {
						if (exon.getEndPhase() == 1) {
							if (node.getCodon() == null || node.phase != 1) {
								node.phase = 1;
								if (exon.getTranscript().getExons().length - exon.getNro() - 1 < 0) return "";
								
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length	- exon.getNro() - 1].getEnd();
								node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 2, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(), exon.getStart() + 1, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(node.getCodon()))	+ (aminopos)	+ MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
						} else {
							if (node.getCodon() == null || node.phase != 2) {
								if (exon.getTranscript().getExons().length - exon.getNro() - 1 < 0) return "";								
								node.phase = 2;
								try {
									prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length - exon.getNro() - 1].getEnd();
								} catch (final Exception e) {
									System.out.println(exon.getTranscript().getGenename());
									e.printStackTrace();
								}
								node.setCodon(new String(this.getSeq(node.getChrom(), prevExonEnd - 1, prevExonEnd, MainPane.referenceFile).append(this.getSeq(node.getChrom(), exon.getStart(), exon.getStart() + 2, MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(node.getCodon()))	+ (aminopos)	+ MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
						}
					} else if (phase == -1) {
						phase = 1;
						if (node.getCodon() == null || node.phase != -1) {
							node.phase = -1;
							prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length - exon.getNro() + 1].getStart();
							node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(), MainPane.referenceFile).append(this.getSeq(node.getChrom(), prevExonEnd, prevExonEnd + 1,	MainPane.referenceFile))));
						}
						array = node.getCodon().toCharArray();
						array[phase] = base.charAt(0);
						return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(node.getCodon()))	+ (aminopos + 1)	+ MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
					} else {
						if (exon.getEnd() - node.getPosition() == 2) {
							if (node.getCodon() == null || node.phase != -2) {
								node.phase = -2;
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length - exon.getNro() + 1].getStart();
								node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 2, exon.getEnd(), MainPane.referenceFile).append(this.getSeq(node.getChrom(), prevExonEnd, prevExonEnd + 1,	MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(node.getCodon()))	+ (aminopos + 1)	+ MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
						} else {
							if (node.getCodon() == null || node.phase != -1) {
								node.phase = -1;
								prevExonEnd = exon.getTranscript().getExons()[exon.getTranscript().getExons().length	- exon.getNro() + 1].getStart();
								node.setCodon(new String(this.getSeq(node.getChrom(), exon.getEnd() - 1, exon.getEnd(), MainPane.referenceFile).append(this.getSeq(node.getChrom(), prevExonEnd, prevExonEnd + 2,	MainPane.referenceFile))));
							}
							array = node.getCodon().toCharArray();
							array[phase] = base.charAt(0);
							return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(node.getCodon()))	+ (aminopos + 1)	+ MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
						}
					}
				} else {
					if (node.getCodon() == null || node.phase != phase) {
						node.phase = (short) phase;
						node.setCodon(new String(this.getSeq(exon.getTranscript().getChrom(), node.getPosition() - phase, node.getPosition() - phase + 3, MainPane.referenceFile)));
					}			
					array = node.getCodon().toCharArray();
					array[phase] = base.charAt(0);
					return MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(node.getCodon())) + aminopos	+ MethodLibrary.getAminoAcid(MethodLibrary.reverseComplement(new String(array)));
				}
			}
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
			return "";
		}
	}

	int maxWidth(final String[] list) {
		int max = 0, index = 0;
		for (int i = 0; i < list.length; i++) {
			if (list[i].length() <= max) continue;
			max = list[i].length();
			index = i;			
		}
		return index;
	}

	int getMousePos(final int mousex) {
		return (int) (MainPane.drawCanvas.selectedSplit.start	+ ((mousex - MainPane.drawCanvas.selectedSplit.chromOffset) / MainPane.drawCanvas.selectedSplit.pixel));
	}
	@Override
	public void mouseDragged(final MouseEvent event) {
		switch (event.getModifiersEx()) {
			case InputEvent.BUTTON1_DOWN_MASK: {
				this.mouseX = event.getX();
				this.mouseY = event.getY();

				if (!seqDrag && (mouseY < MainPane.chromScroll.getViewport().getHeight() - this.cytoHeight || MainPane.drawCanvas.selectedSplit.viewLength > Settings.readDrawDistance)) {
					if (MainPane.drawCanvas.lineZoomer) {
						if (MainPane.drawCanvas.selectedSplit.start > 1	|| MainPane.drawCanvas.selectedSplit.end < MainPane.drawCanvas.selectedSplit.chromEnd) {
							MainPane.drawCanvas.gotoPos(MainPane.drawCanvas.selectedSplit.start - (MainPane.drawCanvas.tempDrag - mouseX)	/ MainPane.drawCanvas.selectedSplit.pixel * 2, MainPane.drawCanvas.selectedSplit.end + (MainPane.drawCanvas.tempDrag - mouseX)	/ MainPane.drawCanvas.selectedSplit.pixel * 2);
						}

						MainPane.drawCanvas.tempDrag = mouseX;
						updateExons = true;
						repaint();
						
						MainPane.drawCanvas.repaint();
					} else {
						zoomDrag = true;
						repaint();
						return;
					}
				} else {
					seqDrag = true;
					repaint();
					return;
				}
				break;
			}
			case InputEvent.BUTTON3_DOWN_MASK: {
				if ((int) MainPane.drawCanvas.selectedSplit.start == 1	&& (int) MainPane.drawCanvas.selectedSplit.end == MainPane.drawCanvas.selectedSplit.chromEnd) break;
				MainPane.drawCanvas.mouseDrag = true;
				MainPane.drawCanvas.moveX = event.getX();
				MainPane.drawCanvas.drag(MainPane.drawCanvas.moveX);
				break;
			}
			case 17: {
				if ((int) MainPane.drawCanvas.selectedSplit.start == 1&& (int) MainPane.drawCanvas.selectedSplit.end == MainPane.drawCanvas.selectedSplit.chromEnd) break;				
				MainPane.drawCanvas.mouseDrag = true;
				MainPane.drawCanvas.moveX = event.getX();
				MainPane.drawCanvas.drag(MainPane.drawCanvas.moveX);
				break;
			}
		}
	}

	@Override
	public void mouseMoved(final MouseEvent event) {
		this.mouseX = event.getX();
		this.mouseY = event.getY();

		if ((this.mouseX) / (Draw.getDrawWidth()) > -1	&& (this.mouseX) / (Draw.getDrawWidth()) < Draw.getSplits().size()) {
			if (MainPane.drawCanvas.selectedSplit != Draw.getSplits().get((this.mouseX) / (Draw.getDrawWidth()))) {
				MainPane.drawCanvas.selectedSplit = Draw.getSplits().get((this.mouseX) / (Draw.getDrawWidth()));
				if (MainPane.drawCanvas.selectedSplit == null) MainPane.drawCanvas.selectedSplit = Draw.getSplits().get(0);
			}
		}
		if (event.getY() > MainPane.chromScroll.getViewport().getHeight() - 15	&& MainPane.drawCanvas.selectedSplit.viewLength < Settings.readDrawDistance) {
			if (getCursor().getType() != Cursor.TEXT_CURSOR) setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		} else {
			if (getCursor().getType() == Cursor.TEXT_CURSOR && getCursor().getType() != Cursor.DEFAULT_CURSOR	&& mouseY > cytoHeight) setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
		mouseRect.setBounds(event.getX() - MainPane.drawCanvas.selectedSplit.chromOffset, event.getY() - this.cytoHeight, 2, 2);
		
		repaint();
	}

	@Override
	public void mouseClicked(final MouseEvent event) {
		switch (event.getButton()) {
			case MouseEvent.BUTTON1: {
				if (clickedExon != null && foundcursor > -1) {
					if (foundcursor == 0) {
						MainPane.drawCanvas.clearReads();
						MainPane.drawCanvas.gotoPos(clickedExon.getStart() + 1, clickedExon.getEnd() + 1);
					} else if (foundcursor == 1) {
						MainPane.drawCanvas.clearReads();
						MainPane.drawCanvas.gotoPos(clickedExon.transcript.getStart() + 1,	clickedExon.transcript.getEnd() + 1);
					} else if (foundcursor == 2) {
						if (exonString[2].contains("ENS")) MainPane.gotoURL("http://ensembl.org/Multi/Search/Results?q="	+ exonString[2].substring(exonString[2].indexOf("ENS")).split(" ")[0]);
						else MainPane.gotoURL("http://www.ncbi.nlm.nih.gov/gene/?term="	+ exonString[2].split(":")[1].split(",")[0].split(" ")[0]);
					} else if (foundcursor == 3) {
						if (exonString[3].contains("ENS")) MainPane.gotoURL("http://ensembl.org/Multi/Search/Results?q="	+ exonString[3].substring(exonString[3].indexOf("ENS")).split(" ")[0]);
					} else if (foundcursor == 5) MainPane.gotoURL("http://www.genecards.org/cgi-bin/carddisp.pl?gene=" + exonString[1]);

					updateExons = true;
					repaint();
					Draw.updateReads = true;
					
					MainPane.drawCanvas.repaint();
					break;
				}
				break;
			}
			case MouseEvent.BUTTON3: {
				if (selectedExon != null && selectedExon.transcript.getGene().getTranscripts().size() > 1	&& !selectedExon.transcript.getGene().showIsoforms()) {
					selectedExon.transcript.getGene().setShowIsoforms(true);
					updateExons = true;
					repaint();
				} else if (selectedExon != null && selectedExon.transcript.getGene().getTranscripts().size() > 1	&& selectedExon.transcript.getGene().showIsoforms()) {
					selectedExon.transcript.getGene().setShowIsoforms(false);
					updateExons = true;
					repaint();
				} else {
					final String copy = MainPane.drawCanvas.selectedSplit.chrom + ":" + getPosition(mouseX - MainPane.drawCanvas.selectedSplit.chromOffset, MainPane.drawCanvas.selectedSplit);
					final StringSelection stringSelection = new StringSelection(copy);
					MenuBar.putMessage("Position " + copy + " copied to clipboard.");
					final Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
					clpbrd.setContents(stringSelection, null);
				}
				break;
			}
		}
	}

	@Override
	public void mouseEntered(final MouseEvent arg0) {}

	@Override
	public void mouseExited(final MouseEvent arg0) { repaint(); }

	@Override
	public void mousePressed(final MouseEvent event) {
		this.pressY = event.getY();
		switch (event.getButton()) {
			case MouseEvent.BUTTON1: {
				MainPane.drawCanvas.pressX = event.getX();
				if (selectedExon != null) {
					clickedSplit = MainPane.drawCanvas.selectedSplit;
					clickedExon = selectedExon;
					repaint();
				}
				break;
			}
			case MouseEvent.BUTTON3: {			
				MainPane.drawCanvas.pressX = event.getX();
				clickedExon = null;
				this.zoomDrag = false;
				updateExons = true;
				repaint();
			}
		}
		MainPane.drawCanvas.tempDrag = MainPane.drawCanvas.pressX;
	}

	@Override
	public void mouseReleased(final MouseEvent arg0) {
		if (seqDrag) {
			seqDrag = false;
			if (mouseX - MainPane.drawCanvas.pressX > 0) {
				final String myString = new String(Arrays.copyOfRange(MainPane.drawCanvas.selectedSplit.getReference().getSeq(),(getMousePos(MainPane.drawCanvas.pressX) - 1 - MainPane.drawCanvas.selectedSplit.getReference().getStartPos()),	getMousePos(mouseX) - 1 - MainPane.drawCanvas.selectedSplit.getReference().getStartPos() + 1));
				final StringSelection stringSelection = new StringSelection(myString);
				final Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
				clpbrd.setContents(stringSelection, null);
				MenuBar.putMessage("Sequence copied to clipboard.");
				timer = System.currentTimeMillis();
			}
			repaint();
		}
	
		MainPane.drawCanvas.lineZoomer = false;
		lineZoomer = false;
		if (MainPane.drawCanvas.mouseDrag) MainPane.drawCanvas.mouseDrag = false;
		if (zoomDrag) {
			if (mouseY <= cytoHeight) {
				if (mouseX - MainPane.drawCanvas.pressX > 0) {
					MainPane.drawCanvas.gotoPos((MainPane.drawCanvas.pressX - MainPane.drawCanvas.selectedSplit.chromOffset)	/ (Draw.getDrawWidth()	/ (double) chromPos.get(MainPane.refchrom + MainPane.drawCanvas.selectedSplit.chrom)),	(mouseX - MainPane.drawCanvas.selectedSplit.chromOffset) / (Draw.getDrawWidth()	/ (double) chromPos.get(MainPane.refchrom + MainPane.drawCanvas.selectedSplit.chrom)));
				}
			} else if (mouseX - MainPane.drawCanvas.pressX > 0) {			
				MainPane.drawCanvas.gotoPos(MainPane.drawCanvas.selectedSplit.start	+ (MainPane.drawCanvas.pressX - MainPane.drawCanvas.selectedSplit.chromOffset)	/ MainPane.drawCanvas.selectedSplit.pixel,	MainPane.drawCanvas.selectedSplit.start + (mouseX - MainPane.drawCanvas.selectedSplit.chromOffset)	/ MainPane.drawCanvas.selectedSplit.pixel);
			}
			zoomDrag = false;
			updateExons = true;
			
		}
		if (MainPane.bedCanvas.bedTrack.size() > 0) {
			final bedFeatureFetcher fetch = MainPane.bedCanvas.new bedFeatureFetcher();
			fetch.execute();
		}
		MainPane.bedCanvas.repaint();
		MainPane.drawCanvas.repaint();
		repaint();
	}
	
	Polygon makeTriangle(final int x, final int y, final int width, final int height, final boolean right) {
		final int[] xs = new int[3];
		final int[] ys = new int[3];
		if (right) {
			final Point p1 = new Point(x, y);
			final Point p2 = new Point(x, y + height);
			final Point p3 = new Point(x + width, (int) (y + (height / 2.0)));

			xs[0] = p1.x;	xs[1] = p2.x;	xs[2] = p3.x;
			ys[0] = p1.y;	ys[1] = p2.y;	ys[2] = p3.y;
		} else {
			final Point p1 = new Point(x, y);
			final Point p2 = new Point(x, y + height);
			final Point p3 = new Point(x - width, (int) (y + (height / 2.0)));

			xs[0] = p1.x;	xs[1] = p2.x;	xs[2] = p3.x;
			ys[0] = p1.y; ys[1] = p2.y; ys[2] = p3.y;
		}
		return new Polygon(xs, ys, xs.length);
	}

	public BufferedImage createBands(final SplitClass split) {
		try {
			final int height = cytoHeight - 4;

			tempImage = MethodLibrary.toCompatibleImage(new BufferedImage((Draw.getDrawWidth()), height, BufferedImage.TYPE_INT_RGB));
			cytoImageBuffer = (Graphics2D) tempImage.getGraphics();
			cytoImageBuffer.setRenderingHints(Draw.rh);

			if (bandVector.size() == 0) {
				cytoImageBuffer.setColor(backTransparent);
				cytoImageBuffer.fillRect(0, 0, Draw.getDrawWidth(), height);
				cytoImageBuffer.setStroke(BaseConstants.strongStroke);
				cytoImageBuffer.setColor(Color.white);
				cytoImageBuffer.drawRoundRect(6, 8, Draw.getDrawWidth() - 14, 1, 20, 20);
				cytoImageBuffer.setColor(Color.lightGray);
				cytoImageBuffer.drawRoundRect(6, 2, Draw.getDrawWidth() - 14, height - 5, 20, 20);
				cytoImageBuffer.setColor(Color.gray);
				cytoImageBuffer.drawRoundRect(4, 0, Draw.getDrawWidth() - 10, height - 1, 20, 20);
			} else {
				final ArrayList<String[]> chromBands = new ArrayList<String[]>();
				int first = 0, second = 0;
				boolean firstdraw = true;
				for (int i = 0; i < bandVector.size(); i++) {
					if (!bandVector.get(i)[0].equals(split.chrom)	&& !bandVector.get(i)[0].equals("chr" + split.chrom)) {
						if (!firstdraw) {
							cytoImageBuffer.setColor(backTransparent);
							cytoImageBuffer.fillRect(Draw.getDrawWidth() - 8, 0, 10, height);
							break;
						}
						continue;
					}
					chromBands.add(bandVector.get(i));
					bandwidth = (int) ((Integer.parseInt(bandVector.get(i)[2]) - Integer.parseInt(bandVector.get(i)[1]))	* ((Draw.getDrawWidth()) / (double) chromPos.get(MainPane.refchrom + split.chrom)));
					if (firstdraw) {
						cytoImageBuffer.setColor(backTransparent);
						cytoImageBuffer.fillRect(0, 0, 6, height);
						Xpos = (int) (Integer.parseInt(bandVector.get(i)[1]) * ((Draw.getDrawWidth())	/ (double) chromPos.get(MainPane.refchrom + split.chrom))) + 6;
						firstdraw = false;
					} else {
						Xpos = (int) (Integer.parseInt(bandVector.get(i)[1]) * ((Draw.getDrawWidth()) / (double) chromPos.get(MainPane.refchrom + split.chrom))) + 1;
					}
					if (bandVector.get(i)[4].contains(",")) color = bandVector.get(i)[4].split(",");
					else color = BaseConstants.getGposColor(bandVector.get(i)[4]);
					
					if (color == null || color.length < 3) break;
					
					if (bandVector.get(i)[3].contains("p11") && Integer.parseInt(color[0]) > 200	&& Integer.parseInt(color[1]) < 100) {
						first = i;
						continue;
					} else if (bandVector.get(i)[3].contains("q11") && Integer.parseInt(color[0]) > 200	&& Integer.parseInt(color[1]) < 100) {
						second = i;
						continue;
					} else {
						cytoImageBuffer.setColor(new Color(Integer.parseInt(color[0]), Integer.parseInt(color[1]),	Integer.parseInt(color[2])));
						cytoImageBuffer.fillRect(Xpos, 0, bandwidth, height);
					}
					if (color[0].equals("0")) cytoImageBuffer.setColor(Color.white);
					else cytoImageBuffer.setColor(Color.black);
				}
				if (chromBands.size() > 0) {
					cytoImageBuffer.setColor(new Color(255, 255, 255, 120));
					cytoImageBuffer.fillRect(0, 4, Draw.getDrawWidth(), height - 10);
					cytoImageBuffer.setColor(new Color(0, 0, 0, 100));
					cytoImageBuffer.setStroke(BaseConstants.strongStroke);
					cytoImageBuffer.drawRoundRect(4, 0, Draw.getDrawWidth() - 10, height - 1, 20, 20);
					if (bandVector.size() > 0) {
						bandwidth = (int) ((Integer.parseInt(bandVector.get(first)[2]) - Integer.parseInt(bandVector.get(first)[1]))	* ((Draw.getDrawWidth())	/ (double) chromPos.get(MainPane.refchrom + split.chrom)));
						Xpos = (int) (Integer.parseInt(bandVector.get(first)[1]) * ((Draw.getDrawWidth()) / (double) chromPos.get(MainPane.refchrom + split.chrom))) + 1;
						cytoImageBuffer.setColor(backTransparent);
						cytoImageBuffer.fillRect(Xpos, 0, bandwidth + 2, height);
						if (bandVector.get(first)[4].contains(",")) color = bandVector.get(first)[4].split(",");
						else color = BaseConstants.getGposColor(bandVector.get(first)[4]);
						
						cytoImageBuffer.setColor(new Color(Integer.parseInt(color[0]), Integer.parseInt(color[1]),	Integer.parseInt(color[2])));
						cytoImageBuffer.fillPolygon(makeTriangle(Xpos, 0, bandwidth, height, true));

						bandwidth = (int) ((Integer.parseInt(bandVector.get(second)[2])	- Integer.parseInt(bandVector.get(second)[1]))	* ((Draw.getDrawWidth())	/ (double) chromPos.get(MainPane.refchrom + split.chrom)));
						Xpos = (int) (Integer.parseInt(bandVector.get(second)[1]) * ((Draw.getDrawWidth())	/ (double) chromPos.get(MainPane.refchrom + split.chrom))) + 1;
						cytoImageBuffer.setColor(backTransparent);
						cytoImageBuffer.fillRect(Xpos, 0, bandwidth, height);
						cytoImageBuffer.setColor(new Color(Integer.parseInt(color[0]), Integer.parseInt(color[1]),	Integer.parseInt(color[2])));
						cytoImageBuffer.fillPolygon(makeTriangle(Xpos + bandwidth, 0, bandwidth, height, false));
					}
					split.setChromBands(chromBands);
				} else {
					cytoImageBuffer.setColor(backTransparent);
					cytoImageBuffer.fillRect(0, 0, Draw.getDrawWidth(), height);
					cytoImageBuffer.setStroke(BaseConstants.strongStroke);
					cytoImageBuffer.setColor(Color.white);
					cytoImageBuffer.drawRoundRect(6, 8, Draw.getDrawWidth() - 14, 1, 20, 20);
					cytoImageBuffer.setColor(Color.lightGray);
					cytoImageBuffer.drawRoundRect(6, 2, Draw.getDrawWidth() - 14, height - 5, 20, 20);
					cytoImageBuffer.setColor(Color.gray);
					cytoImageBuffer.drawRoundRect(4, 0, Draw.getDrawWidth() - 10, height - 1, 20, 20);
				}
			}
		} catch (final Exception e) {
		e.printStackTrace();
	}
		return tempImage;
}
}
