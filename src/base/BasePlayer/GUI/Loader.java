/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;
import java.awt.Cursor;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.Main;
import base.BasePlayer.Setter;


public class Loader extends SwingWorker<String, Object>  {
	String text = "Loading";
	int loadBar = 0;
	Graphics g = Main.frame.getGlassPane().getGraphics();
	public static boolean memory = false;
	boolean updater = false;
	public static String loadingtext = "";
	public static ArrayList<String> readyQueue = new ArrayList<String>();
	public static boolean isLoading = false;
	public Loader(String text) {
		
		if(!text.equals("note")) {
			Main.frame.setCursor( Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		}
		memory = false;
		//Loader.loadingtext = text;
		Setter.getInstance().setLoading(true, Loader.loadingtext);
		
		Draw.setGlasspane(true);
		
	}

	@Override
	protected String doInBackground() throws Exception {
		try {
				
				while(Getter.getInstance().loading()) {	
				
					if(memory) {
						String loadtext = Loader.loadingtext;
						MainPane.cancel(1);
						Draw.drawVariables.variantsStart = 0;
						Draw.drawVariables.variantsEnd = 1;
						memory = false;
						
						if(loadtext.contains("Annotating")) {
							JOptionPane.showMessageDialog(MainPane.chromDraw, "Memory allocation exceeded in " +loadtext +"\nTip1: Goto \"Tools > Settings > General\" and change 'Processing window size (bp)' to 100,000.\nTip2: try to increase memory allocation for BasePlayer\n", "Information", JOptionPane.INFORMATION_MESSAGE);
							
						}
						else {
							JOptionPane.showMessageDialog(MainPane.chromDraw, "Try to search more specific region to visualize all data\nTip: try to increase memory allocation for BasePlayer (config.txt in your BasePlayer folder)\nNote: Variant annotation is still possible through Variant handler.", "Information", JOptionPane.INFORMATION_MESSAGE);
						}
						
						System.gc();
						MainPane.chromDraw.repaint();
					}
				      
					MainPane.glassPane.repaint();				
					Thread.sleep(100);
				
			}
			
			}
			catch(Exception e) {
				ErrorLog.addError(e.getStackTrace());
				e.printStackTrace();
			}
		MainPane.glassPane.repaint();	
		Draw.setGlasspane(false);
		return null;
	}
	public static void setLoading(final String text) {
		if (!Loader.isLoading) {
			MainPane.loading = new Loader(text);
			MainPane.loading.execute();
		}

		readyQueue.add(text);
		loadingtext = text;
		Draw.setGlasspane(true);
	}

	public static void ready(final String text) {
		if (text.equals("all")) {
			readyQueue.clear();
			Setter.getInstance().setLoading(false, "");
		}
		if (readyQueue.size() > 0) {
			readyQueue.remove(text);
			if (readyQueue.size() > 0) Setter.getInstance().setLoading(true, readyQueue.get(readyQueue.size() - 1));
		}

		if (readyQueue.size() == 0) {
			Main.frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			Setter.getInstance().setLoading(false, "");
			Draw.setGlasspane(false);
		}
	}

}
