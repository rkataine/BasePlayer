package base.BasePlayer.GUI.listeners;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import base.BasePlayer.BaseVariables;
import base.BasePlayer.Main;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.modals.AddGenome;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.io.FileRead;
import htsjdk.samtools.seekablestream.SeekableStream;
import htsjdk.samtools.seekablestream.SeekableStreamFactory;
import htsjdk.tribble.readers.TabixReader;

public class MainPaneActionListeners {

		public static void annoDropActionListener(final ActionEvent actionEvent) {

			if (actionEvent.getActionCommand() == "comboBoxChanged") {
				if (MainPane.clickedAnno) {
					if (MainPane.geneDropdown.getSelectedIndex() < MainPane.geneDropdown.getItemCount() - 1) {
						MainPane.changeAnnotation(MainPane.geneDropdown.getSelectedIndex());
						MainPane.geneDropdown.setToolTipText(MainPane.geneDropdown.getSelectedItem().toString());
					} else {
						MainPane.clickedAnno = false;

						MainPane.geneDropdown.setSelectedItem(MainPane.defaultAnnotation);
						MainPane.geneDropdown.revalidate();
						MainPane.clickedAnno = true;
						for (int i = 0; i < AddGenome.root.getChildCount(); i++) {
							if (AddGenome.root.getChildAt(i).toString()
									.equals(MainPane.refDropdown.getSelectedItem().toString())) {
								AddGenome.tree.setSelectionRow(i);
								AddGenome.tree.expandRow(i);
								break;
							}
						}
						if (AddGenome.frame == null) {
							AddGenome.createAndShowGUI();
						}
						AddGenome.annotation = true;
						AddGenome.remove.setEnabled(false);
						AddGenome.download.setEnabled(false);
						AddGenome.frame.setVisible(true);
						AddGenome.frame.setLocation(Main.frame.getLocationOnScreen().x + Main.frame.getWidth() / 2 - AddGenome.frame.getWidth() / 2,	Main.frame.getLocationOnScreen().y + Main.frame.getHeight() / 6);
						AddGenome.frame.setState(JFrame.NORMAL);
					}
				}
			}		
	};

	public static void refDropActionListener(ActionEvent actionEvent) {
			if (actionEvent.getActionCommand() == "comboBoxChanged") {
				if (MainPane.clicked) {
					if (!MainPane.rightclick) {
						if (MainPane.refDropdown.getSelectedIndex() < MainPane.refDropdown.getItemCount() - 1) {
							MainPane.changeRef(MainPane.refDropdown.getSelectedItem().toString());
						} else {
							MainPane.clicked = false;
							MainPane.refDropdown.setSelectedItem(MainPane.defaultGenome);
							MainPane.refDropdown.revalidate();
							MainPane.clicked = true;
							if (AddGenome.frame == null) {
								AddGenome.createAndShowGUI();
							}
							AddGenome.remove.setEnabled(false);
							AddGenome.download.setEnabled(false);
							AddGenome.annotation = false;
							AddGenome.frame.setVisible(true);
							AddGenome.frame.setLocation(
									Main.frame.getLocationOnScreen().x + Main.frame.getWidth() / 2
											- AddGenome.frame.getWidth() / 2,
                      Main.frame.getLocationOnScreen().y + Main.frame.getHeight() / 6);
							AddGenome.frame.setState(JFrame.NORMAL);

						}
					}
				}
			}
	};
public static void drawScrollAdjustmentListener(AdjustmentEvent actionEvent) {
	Draw drawCanvas = MainPane.drawCanvas;
	JScrollPane drawScroll = MainPane.drawScroll;
	if (Draw.drawVariables.getVisibleStart.get() != (short) (drawScroll.getVerticalScrollBar().getValue() / Draw.drawVariables.sampleHeight)) {
	if (!drawCanvas.sidebar) 
		Draw.drawVariables.setVisibleStart.accept((int) (drawScroll.getVerticalScrollBar().getValue() / Draw.drawVariables.sampleHeight));
	
	if (Draw.getSplits().size() > 1) {
		for (int i = 0; i < Draw.getSplits().size(); i++) 
			Draw.getSplits().get(i).updateReads = true;		
	} else Draw.updateReads = true;
	
		
	}	
  };
  public static void drawScrollMouseWheelListener(MouseWheelEvent e) {		
    Draw.setGlasspane(true);
    if (e.getWheelRotation() < 0) 
      Draw.drawVariables.setVisibleStart.accept(Draw.drawVariables.getVisibleStart.get() - 1);
    else
	  Draw.drawVariables.setVisibleStart.accept(Draw.drawVariables.getVisibleStart.get() + 1);    
    
	Draw.setScrollbar((int) (Draw.drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight));
  }
  public static MouseListener settingsButtonMouseListener = new MouseListener() {
	@Override
	public void mousePressed(final MouseEvent e) {
		Settings.frame.setLocation(Main.frame.getLocationOnScreen().x + Main.frame.getWidth() / 2 - Settings.frame.getWidth() / 2,	Main.frame.getLocationOnScreen().y + Main.frame.getHeight() / 6);
		Settings.frame.setState(JFrame.NORMAL);
		Settings.frame.setVisible(true);
	}			
	@Override
	public void mouseExited(final MouseEvent e) {
		MenuBar.setbut.setOpaque(false);
		MenuBar.setbut.revalidate();
	}
	@Override
	public void mouseReleased(final MouseEvent e) {}
	@Override
	public void mouseEntered(final MouseEvent e) {}
	@Override
	public void mouseClicked(final MouseEvent e) {	}

	};
    public static DocumentListener searchFieldDocumentListener = new DocumentListener() {
				private String searchstring;
        private String[] searchList;
        JTextField searchField = MenuBar.searchField;
        Hashtable<String, String[]> searchTable = MenuBar.searchTable;
        public void changedUpdate(final DocumentEvent e) {

					if (searchField.getText().contains(";")) {
						searchList = searchField.getText().split(";");
						for (int i = 0; i < searchList.length; i++) {
							warn(searchList[i].replace(" ", ""));
						}
					} else {
						warn(searchField.getText().replace(" ", ""));
					}

				}

				public void removeUpdate(final DocumentEvent e) {
					if (searchField.getText().contains(";")) {
						searchList = searchField.getText().split(";");
						for (int i = 0; i < searchList.length; i++) {
							warn(searchList[i].replace(" ", ""));
						}
					} else {
						warn(searchField.getText().replace(" ", ""));
					}
				}

				public void insertUpdate(final DocumentEvent e) {
					if (searchField.getText().contains(";")) {
						searchList = searchField.getText().split(";");
						for (int i = 0; i < searchList.length; i++) {
							warn(searchList[i].replace(" ", ""));
						}
					} else {
						warn(searchField.getText().replace(" ", ""));
					}
				}

				public void warn(final String searchtext) {

					if (searchTable.containsKey(searchtext.toUpperCase())) {
						if (searchTable.get(searchtext.toUpperCase())[0].equals(MenuBar.chromosomeDropdown.getSelectedItem())) {
							MenuBar.searchChrom = searchTable.get(searchtext.toUpperCase())[0];
							MenuBar.searchStart = Integer.parseInt(searchTable.get(searchtext.toUpperCase())[1]);
							MenuBar.searchEnd = Integer.parseInt(searchTable.get(searchtext.toUpperCase())[2]);
						} else {
							MainPane.chromDraw.repaint();
							MenuBar.searchStart = -1;
							MenuBar.searchEnd = -1;
						}
						MainPane.chromDraw.repaint();
						searchField.setForeground(Color.black);
					} else if (searchField.getText().toUpperCase().matches("CHR.{1,2}(?!:)")) {

						if (MenuBar.chromnamevector.contains(searchtext.toUpperCase().substring(3))) {
							searchField.setForeground(Color.black);

						} else {
							MainPane.chromDraw.repaint();
							searchField.setForeground(Color.red);
						}
					} else if (searchtext.toUpperCase().replace(",", "").matches("(CHR)?(.+:)?\\d+(-\\d+)?")) {

						searchField.setForeground(Color.black);
						if (searchtext.contains(":")) {
							searchstring = searchtext.substring(searchtext.indexOf(":") + 1).replace(",", "");
						} else {
							MainPane.chromDraw.repaint();
							searchstring = searchtext.replace(",", "");
						}

						if (!searchstring.contains("-")) {
							try {
								MenuBar.searchStart = Integer.parseInt(searchstring);
							} catch (final Exception ex) {

							}
							MenuBar.searchEnd = -1;
						} else {
							try {
								MenuBar.searchStart = Integer.parseInt(searchstring.substring(0, searchstring.indexOf("-")));
								MenuBar.searchEnd = Integer.parseInt(searchstring.substring(searchstring.indexOf("-") + 1));
							} catch (final Exception ex) {

							}
						}
						MainPane.chromDraw.repaint();

					} else {
						MainPane.chromDraw.repaint();
						searchField.setForeground(Color.red);
						MenuBar.searchStart = -1;
						MenuBar.searchEnd = -1;
					}
				}
			};
  public static void fromURLlistener(ActionEvent actionEvent) {
    final JPopupMenu menu = new JPopupMenu();
    final JTextField area = new JTextField();
    final JButton add = new JButton("Fetch");
    final JLabel label = new JLabel("Paste track URL below");
    final JScrollPane menuscroll = new JScrollPane();
    add.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        try {
          final String urltext = area.getText().trim();
          Boolean size = true;
          if (urltext.contains("pleiades")) {	MenuBar.openPleiades(urltext);	return;	}
          if (!FileRead.isTrackFile(urltext)) {
            MainPane.showError("The file format is not supported.\n"	+ "Supported formats: bed, bigwig, bigbed, gff, bedgraph", "Error");
            return;
          }
          if (!urltext.toLowerCase().endsWith(".bw") && !urltext.toLowerCase().endsWith(".bigwig")	&& !urltext.toLowerCase().endsWith(".bb")	&& !urltext.toLowerCase().endsWith(".bigbed")) {
            URL url = null;
            try {
              url = new URL(urltext);
            } catch (final Exception ex) {
              menu.setVisible(false);
              MainPane.showError("Please paste whole url (protocol included)", "Error");
              return;
            }
            URL testurl = url;
            HttpURLConnection huc = (HttpURLConnection) testurl.openConnection();
            huc.setRequestMethod("HEAD");
            int responseCode = huc.getResponseCode();

            if (responseCode != 404) {
              final SeekableStream stream = SeekableStreamFactory.getInstance().getStreamFor(url);
              TabixReader tabixReader = null;
              String index = null;

              try {
                if (stream.length() / (double) 1048576 >= Settings.settings.get("bigFile")) size = false;
                
                tabixReader = new TabixReader(urltext, urltext + ".tbi", stream);
                index = urltext + ".tbi";
                testurl = new URL(index);
                huc = (HttpURLConnection) testurl.openConnection();
                huc.setRequestMethod("HEAD");
                responseCode = huc.getResponseCode();

                if (responseCode == 404) {
                  menu.setVisible(false);
                  MainPane.showError("Index file (.tbi) not found in the URL.", "Error");
                  tabixReader.close();
                  return;
                }

              } catch (final Exception ex) {
                try {
                  tabixReader = new TabixReader(urltext, urltext.substring(0, urltext.indexOf(".gz")) + ".tbi", stream);
                  index = urltext.substring(0, urltext.indexOf(".gz")) + ".tbi";
                } catch (final Exception exc) {
                  menu.setVisible(false);
                  MainPane.showError("Could not read tabix file.", "Error");
                }
              }
              if (tabixReader != null && index != null) {
                stream.close();
                tabixReader.close();
                menu.setVisible(false);
                final FileRead filereader = new FileRead();
                filereader.readBED(urltext, index, size);
              }
            } else {
              menu.setVisible(false);
              MainPane.showError("Not a valid URL", "Error");
            }
          } else {
            URL url = null;
            try {
              url = new URL(urltext);
            } catch (final Exception ex) {
              MainPane.showError("Please paste whole url (protocol included)", "Error");
              return;
            }
            final URL testurl = url;
            final HttpURLConnection huc = (HttpURLConnection) testurl.openConnection();
            huc.setRequestMethod("HEAD");
            final int responseCode = huc.getResponseCode();

            if (responseCode != 404) {
              menu.setVisible(false);
              final FileRead filereader = new FileRead();
              filereader.readBED(urltext, "nan", true);
            } else {
              menu.setVisible(false);
              MainPane.showError("Not a valid URL", "Error");
            }
          }
        } catch (final Exception ex) {	ex.printStackTrace();	}
      }
    });
    

    area.setFont(MainPane.menuFont);
    menu.add(label);
    menu.add(menuscroll);
    menu.add(add);
    area.setPreferredSize(new Dimension(300, BaseVariables.defaultFontSize + 8));
    area.setCaretPosition(0);
    area.revalidate();
    menuscroll.getViewport().add(area);
    area.requestFocus();
    menu.pack();
    menu.show(Main.frame, MainPane.mouseX + 20, MenuBar.fromURL.getY());
  }
}
