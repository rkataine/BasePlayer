package base.BasePlayer.GUI.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelEvent;

import base.BasePlayer.BaseVariables;
import base.BasePlayer.Setter;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.reads.ReadNode;
import base.BasePlayer.reads.Reads;
import base.BasePlayer.sample.Sample;

public class DrawListeners {
  static Draw draw = MainPane.drawCanvas;
  public static void handleMouseWheel(MouseWheelEvent mwe) {
    draw.mouseWheel = true;
    if ((draw.sidebar || draw.selectedSplit.viewLength > Settings.readDrawDistance) && MainPane.samples > 0) {
      draw.splitList.clear();
      ReadNode.selected = null;

      if (mwe.getWheelRotation() < 0)
        Draw.drawVariables.setVisibleStart.accept(Draw.drawVariables.getVisibleStart.get() - 1);
      else 
        Draw.drawVariables.setVisibleStart.accept(Draw.drawVariables.getVisibleStart.get() + 1);

      Draw.setScrollbar((int) (Draw.drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight));
      if (Draw.getSplits().size() > 1) {
        for (int i = 0; i < Draw.getSplits().size(); i++) {
          Draw.getSplits().get(i).updateReads = true;
        }
        Draw.updateCoverages = true;
      } else {
        Draw.updateReads = true;
        Draw.updateCoverages = true;
        
      }
      draw.repaint();
    } else {
      if (MainPane.samples > 0 && MainPane.readsamples > 0 && draw.selectedSplit.viewLength < Settings.readDrawDistance) {
        try {
          final Reads readClass = Sample.hoverSample.getreadHash().get(draw.selectedSplit);
          readClass.readwheel -= (BaseVariables.readHeight + 2) * draw.ctrlpressed	* mwe.getWheelRotation();
          if (readClass.readwheel < 0) readClass.readwheel = 0;
          if (readClass.readwheel > readClass.getReads().size() * BaseVariables.readHeight) readClass.readwheel = readClass.getReads().size() * BaseVariables.readHeight;
          draw.selectedSplit.updateReads = true;
          Setter.getInstance().setUpdateScreen();
        } catch (final Exception e) {
        }
      }
    }
  }
  public static KeyListener drawKeyListener = new KeyListener() {
      public void keyPressed(final KeyEvent e) {
        /* final int keyCode = e.getKeyCode();
        SplitClass selectedSplit = MainPane.drawCanvas.selectedSplit;
        if ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) return;
        
        if (keyCode == KeyEvent.VK_PLUS || keyCode == 107) {
          if (selectedSplit.viewLength <= Settings.readDrawDistance	&& Draw.drawVariables.sampleHeight > Settings.coverageWindowHeight	&& hoverSample.getreadHash() != null) {
            final double temp = (BaseVariables.readHeight + 2) / (double) hoverSample.getreadHash().get(selectedSplit).readwheel;
            if (BaseVariables.readHeight < 20) {
              BaseVariables.readfont = new Font("SansSerif", Font.BOLD,	BaseVariables.readHeight);
              BaseVariables.readHeight++;
              hoverSample.getreadHash().get(selectedSplit).readwheel = (int) ((BaseVariables.readHeight + 2) / (double) temp);
              updateReads = true;
              repaint();
            }
          } else {
            if (Draw.backColor.getAlpha() <= 250) {
              backColor = new Color(backColor.getRed(), backColor.getGreen(), backColor.getBlue(),	Draw.backColor.getAlpha() + 5);
              repaint();
            }
          }
        } else if (keyCode == KeyEvent.VK_MINUS || keyCode == 109) {
          if (selectedSplit.viewLength <= Settings.readDrawDistance	&& Draw.drawVariables.sampleHeight > Settings.coverageWindowHeight	&& hoverSample.getreadHash() != null) {
            if (BaseVariables.readHeight > 1) {
              final double temp = (BaseVariables.readHeight + 2) / (double) hoverSample.getreadHash().get(selectedSplit).readwheel;
              BaseVariables.readfont = new Font("SansSerif", Font.BOLD,	BaseVariables.readHeight);
              BaseVariables.readHeight--;
              hoverSample.getreadHash().get(selectedSplit).readwheel = (int) ((BaseVariables.readHeight + 2)	/ (double) temp);
              updateReads = true;
              repaint();
            }
          } else {
            if (Draw.backColor.getAlpha() >= 5) {
              backColor = new Color(backColor.getRed(), backColor.getGreen(), backColor.getBlue(), Draw.backColor.getAlpha() - 5);
              repaint();
            }
          }
        }
        if (keyCode == KeyEvent.VK_PLUS) {
          if (selectedSplit.viewLength > Settings.readDrawDistance) {
            if (Draw.backColor.getAlpha() <= 250) {
              backColor = new Color(60, 60, 60, Draw.backColor.getAlpha() + 5);
              repaint();
            }
          }
        } */
      }
      public void keyReleased(final KeyEvent e) {}
      public void keyTyped(final KeyEvent e) {}
    };
}
