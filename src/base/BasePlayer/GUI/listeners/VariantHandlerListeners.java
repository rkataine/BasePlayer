package base.BasePlayer.GUI.listeners;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import base.BasePlayer.Getter;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.variants.VarMaster;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.RangeSlider;

public class VariantHandlerListeners {
  
  public static void performQualitySliderChange(ChangeEvent e) {
    VariantHandler.qualityLabel.setText("Min. quality score: " + value(e));
    MainPane.projectValues.variantHandlerValues.variantQuality = value(e);
    repaint();
  } 

  public static void performGQSliderChange(ChangeEvent e) {
    VariantHandler.gqLabel.setText("Min. genotype quality: " + value(e));
    MainPane.projectValues.variantHandlerValues.variantGQ = value(e);
    repaint();
  }
  public static void performCoverageSliderChange(ChangeEvent e) {
    VariantHandler.coverageLabel.setText("Min. coverage: " + value(e));
    MainPane.projectValues.variantHandlerValues.variantCoverage = value(e);
    repaint();
  }
  public static void performMaxCoverageSliderChange(ChangeEvent e) {
    VariantHandler.maxCoverageLabel.setText("Max. coverage: " + value(e));
    MainPane.projectValues.variantHandlerValues.variantMaxCoverage = value(e);
    repaint();
  }
 
  public static void performCallSliderChange(ChangeEvent e) {
    VariantHandler.callsLabel.setText("Min/max allelic fraction: " + value(e) + "% - " + ((RangeSlider)e.getSource()).getUpperValue() + "%");
    MainPane.projectValues.variantHandlerValues.variantCalls = value(e);
    MainPane.projectValues.variantHandlerValues.variantMaxCalls = ((RangeSlider)e.getSource()).getUpperValue();
  
    if (Settings.selectedVarDraw == 1) {
      VariantHandler.maxSlideValue = MainPane.projectValues.variantHandlerValues.variantMaxCalls / (float) 100;
    }
    
    repaint();
  }
 
  public static void performReadSliderChange(ChangeEvent e) {
    VariantHandler.readsLabel.setText("Min. alt reads: " + value(e));
    MainPane.projectValues.variantHandlerValues.variantReads = value(e);
    repaint();
  }
	public static void performCommonSliderChange(ChangeEvent e) {  
    int value = MainPane.projectValues.variantHandlerValues.commonVariantsMin, upperValue = VariantHandler.commonSlider.getUpperValue();
    VariantHandler.slideLabel.setText("Shared variants in " + value + "/" + upperValue + " samples");
    MainPane.projectValues.variantHandlerValues.commonVariantsMin = value;
    MainPane.projectValues.variantHandlerValues.commonVariantsMax = upperValue;
    if ((value > 1 || upperValue < MainPane.varsamples)	&& (!VariantHandler.clusterBox.getText().equals("0") && !VariantHandler.clusterBox.getText().equals(""))) {
      VariantHandler.clusterSize = Integer.parseInt(VariantHandler.clusterBox.getText());
      VarMaster.calcClusters(Getter.getInstance().getVariantHead(), 1);      
    }
    MainPane.projectValues.variantHandlerValues.clusterSize = VariantHandler.clusterSize;
    repaint();
  }
  public static void performGeneSliderChange(ChangeEvent e) {
    VariantHandler.geneLabel.setText("At least " + value(e) + "/" + ((JSlider)e.getSource()).getMaximum() + " samples share a mutated gene");
    MainPane.projectValues.variantHandlerValues.compareGene = value(e);
    repaint();
  }

  private static void repaint() {
    if (MainPane.drawCanvas != null) {
      MainPane.drawCanvas.repaint();
      if (Draw.getSplits().get(0).viewLength <= 1000000) {
        MainPane.chromDraw.updateExons = true;
        MainPane.chromDraw.repaint();
      }
    }
  }
  private static int value(ChangeEvent e) {
    return ((JSlider)e.getSource()).getValue();
  }
}
