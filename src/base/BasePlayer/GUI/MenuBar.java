package base.BasePlayer.GUI;

import java.awt.Color;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileSystemView;
import base.BasePlayer.*;
import base.BasePlayer.GUI.listeners.MainPaneActionListeners;
import base.BasePlayer.GUI.modals.*;
import base.BasePlayer.control.Control;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.io.SessionHandler;
import base.BasePlayer.utils.Average;
import base.BasePlayer.utils.PeakCaller;
import base.BasePlayer.utils.ScreenPhotos;
import base.BasePlayer.utils.Updater;
import base.BasePlayer.variants.SteppedComboBox;
import base.BasePlayer.variants.VarCalculations;
import base.BasePlayer.variants.VariantCaller;

public class MenuBar extends JMenuBar implements ActionListener, MouseListener, KeyListener, ComponentListener {
  static JEditorPane area = new JEditorPane();
	public static ImageIcon openIcon, saveIcon, settingsIcon;
  public static JButton manage;
  JFrame frame = Main.frame;
  public static String version = "1.0.2";
  public static JMenu filemenu;
	static JMenu toolmenu, help, about;
  static JMenuItem openvcfs, openbams, exit, addtracks, addcontrols, pleiadesButton, saveProjectNew, saveProjectAsNew, openProjectNew, clear, clearMemory, errorlog;
	public static JMenuItem fromURL, update, average, tbrowser, bconvert, variantCaller, peakCaller, settings;
	static JMenuItem addGenome = new JMenuItem("Add new genome...");	   
  static JButton zoomout;
  static JButton manual;
  static JButton dosomething = new JButton("Do stuff!");
  static JButton back, forward;
	public static JButton setbut;
  private JMenuItem photo;
  static boolean pleiades = false;
  static JMenu addURL;
	static JTextField urlField;
  static String[] empty = {""};	
  private static String[] searchList;
  public static int searchStart=-1, searchEnd=-1;
  public static String searchChrom = "";
  public static JTextField chromlabel = new JTextField(" Chrom "); //TODO: toinen nimi
  public static int selectedChrom = 0;
  public static java.util.List<String> chromnamevector = Collections.synchronizedList(new ArrayList<String>());
  public static Hashtable<String, String[]> searchTable = new Hashtable<String, String[]>();
	public static DefaultComboBoxModel<String> chromModel = new DefaultComboBoxModel<String>(empty);  
	public static SteppedComboBox chromosomeDropdown = new SteppedComboBox(chromModel);		
  public static Hashtable<String, Long[]> chromIndex = new Hashtable<String, Long[]>();
	static TableBrowser tablebrowser;
	static BEDconvert bedconverter;
  static String[] chromnames;
  static JTextField positionField = new JTextField();
	static JTextField widthLabel = new JTextField();
  static JLabel chromLabel = new JLabel("");
  static Draw drawCanvas;
  BedCanvas bedCanvas = MainPane.bedCanvas;
  public static JTextField searchField = new JTextField("Search by position or gene") {
	  Image glass = Toolkit.getDefaultToolkit().getImage(Main.class.getResource("icons/glass.jpg"));

		protected void paintComponent(final Graphics g) {
			super.paintComponent(g);
			if (glass != null) {
				g.drawImage(glass, 4, (searchField.getHeight() - (BaseVariables.defaultFontSize + 4)) / 2,
				BaseVariables.defaultFontSize + 4, BaseVariables.defaultFontSize + 4, this);
			}
		}
	};
  public MenuBar() {
    super();
    setMargin(new Insets(0, 2, 0, 2));
    setOpaque(true);
    setMenuBar();
  }
  private void setMenuBar() {
		URL imgUrl = Main.class.getResource("icons/settings.png");
		settingsIcon = new ImageIcon(imgUrl);
		imgUrl = Main.class.getResource("icons/save.gif");
		saveIcon = new ImageIcon(imgUrl);
		imgUrl = Main.class.getResource("icons/open.gif");
		openIcon = new ImageIcon(imgUrl);
   filemenu = new JMenu("File");
		toolmenu = new JMenu("Tools");
		help = new JMenu("Help");
		about = new JMenu("About");
		exit = new JMenuItem("Exit");
		manual = new JButton("Online manual");
		manual.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {	MainPane.gotoURL("https://baseplayer.fi/BPmanual");	}
		});
		zoomout = new JButton("Zoom out");
		back = new JButton("<<");
		forward = new JButton(">>");
		manage = new JButton("Variant Manager");
		openvcfs = new JMenuItem("Add VCFs", openIcon);
		openbams = new JMenuItem("Add BAMs", openIcon);
		average = new JMenuItem("Coverage calculator");
		update = new JMenuItem("Update");
		update.setVisible(false);
		errorlog = new JMenuItem("View log");
		addURL = new JMenu("Add from URL");
		urlField = new JTextField("Enter URL");
		addtracks = new JMenuItem("Add tracks");
		fromURL = new JMenuItem("Add track from URL");
		addcontrols = new JMenuItem("Add controls");
		pleiadesButton = new JMenuItem("PLEIADES");
		
		
		clear = new JMenuItem("Clear data");
		clearMemory = new JMenuItem("Clean memory");
		filemenu.add(openvcfs);
		filemenu.add(openbams);
		variantCaller = new JMenuItem("Variant Caller");
		tablebrowser = new TableBrowser();
		bedconverter = new BEDconvert();
		tbrowser = new JMenuItem("Table Browser");
		bconvert = new JMenuItem("BED converter");
		photo = new JMenuItem("Take a photo");
		peakCaller = new JMenuItem("Peak Caller");
		addtracks = new JMenuItem("Add tracks", openIcon);
		filemenu.add(addtracks);
		addcontrols = new JMenuItem("Add controls", openIcon);
		filemenu.add(addcontrols);
		filemenu.add(fromURL);
		if (pleiades) {
			pleiadesButton.setPreferredSize(MainPane.buttonDimension);
			pleiadesButton.addActionListener(this);
			filemenu.add(pleiadesButton);
		}

		filemenu.add(new JSeparator());
		openProjectNew = new JMenuItem("Open project (new)", openIcon);
		filemenu.add(openProjectNew);
		saveProjectNew = new JMenuItem("Save project (new)", saveIcon);
		filemenu.add(saveProjectNew);
		saveProjectAsNew = new JMenuItem("Save project as... (new)", saveIcon);
		filemenu.add(saveProjectAsNew);
		
		filemenu.add(new JSeparator());
	
		filemenu.add(update);
		filemenu.add(clear);
		filemenu.add(new JSeparator());
		filemenu.add(exit);
		exit.addActionListener(this);
		add(filemenu);
		manage.addActionListener(this);
		manage.addMouseListener(this);
		update.addActionListener(this);
		average.addActionListener(this);
		average.setEnabled(false);
		average.setToolTipText("No bam/cram files opened");
		tbrowser.addActionListener(this);
		bconvert.addActionListener(this);		
		toolmenu.add(tbrowser);
		toolmenu.add(average);
		toolmenu.add(variantCaller);
		toolmenu.add(bconvert);
		fromURL.addMouseListener(this);
    searchField.getDocument().addDocumentListener(MainPaneActionListeners.searchFieldDocumentListener);
		fromURL.addActionListener(MainPaneActionListeners::fromURLlistener);
    addGenome.addMouseListener(this);
		
		
		settings = new JMenuItem("Settings", settingsIcon);
		setbut = new JButton("", settingsIcon);
		setbut.setToolTipText("Settings");
		setbut.setOpaque(false);
		setbut.setContentAreaFilled(false);
		setbut.setBackground(getBackground());
		setbut.addMouseListener(MainPaneActionListeners.settingsButtonMouseListener); 
		variantCaller.setToolTipText("No bam/cram files opened");
		variantCaller.addActionListener(this);
		variantCaller.setEnabled(false);
		peakCaller.setEnabled(true);
		peakCaller.addActionListener(this);
		settings.addActionListener(this);
		clearMemory.addActionListener(this);
		errorlog.addActionListener(this);
		toolmenu.add(clearMemory);
		toolmenu.add(errorlog);
		toolmenu.add(new JSeparator());
		toolmenu.add(settings);
		add(toolmenu);
		add(manage);
		area = new JEditorPane();

		final String infotext = "<html><h2>BasePlayer</h2>This is a version " + version
				+ " of BasePlayer (<a href=https://baseplayer.fi>https://baseplayer.fi</a>)<br/> Author: Riku Katainen <br/> University of Helsinki<br/>"
				+ "Tumor Genomics Group (<a href=http://research.med.helsinki.fi/gsb/aaltonen/>http://research.med.helsinki.fi/gsb/aaltonen/</a>) <br/> "
				+ "Contact: help@baseplayer.fi <br/> <br/>"

				+ "Supported filetype for variants is VCF and VCF.gz (index file will be created if missing)<br/> "
				+ "Supported filetypes for reads are BAM and CRAM. Index files required (.bai or .crai). <br/> "
				+ "Supported filetypes for additional tracks are BED(.gz), GFF.gz, BedGraph, BigWig, BigBed.<br/> (tabix index required for bgzipped files). <br/><br/> "

				+ "For optimal usage, you should have vcf.gz and bam -files for each sample. <br/> "
				+ "e.g. in case you have a sample named as sample1, name all files similarly and <br/>"
				+ "place in the same folder:<br/>" + "sample1.vcf.gz<br/>" + "sample1.vcf.gz.tbi<br/>"
				+ "sample1.bam<br/>" + "sample1.bam.bai<br/><br/>"
				+ "When you open sample1.vcf.gz, sample1.bam is recognized and opened<br/>"
				+ "on the same track.<br/><br/>"
				+ "Instructional videos can be viewed at our <a href=https://www.youtube.com/channel/UCywq-T7W0YPzACyB4LT7Q3g> Youtube channel</a>";
		area = new JEditorPane();
		area.setEditable(false);
		area.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
		area.setText(infotext);
		area.setFont(MainPane.menuFont);
		area.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(final HyperlinkEvent hyperlinkEvent) {
				final HyperlinkEvent.EventType type = hyperlinkEvent.getEventType();
				final URL url = hyperlinkEvent.getURL();
				if (type == HyperlinkEvent.EventType.ACTIVATED) {
					MainPane.gotoURL(url.toString());
				}
			}
		});
    chromosomeDropdown.setMaximumRowCount(25);
    chromosomeDropdown.setEnabled(true);
    chromosomeDropdown.addActionListener(ChromoDropActionListener);
    chromosomeDropdown.addMouseListener(this);
		about.add(area);
		about.addMouseListener(this);
		help.add(about);
		help.add(manual);
		add(help);
		final JLabel emptylab = new JLabel("  ");
		emptylab.setEnabled(false);
		emptylab.setOpaque(false);
		add(emptylab);
		chromosomeDropdown.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.lightGray));
		chromosomeDropdown.setBorder(BorderFactory.createCompoundBorder(chromosomeDropdown.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		chromlabel.setToolTipText("Current chromosome");
		chromlabel.setFocusable(false);
		chromlabel.addMouseListener(this);
		chromlabel.setBackground(Color.white);
		chromlabel.setEditable(false);
		chromlabel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 0, Color.lightGray));
		chromlabel.setBorder(BorderFactory.createCompoundBorder(chromlabel.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		add(chromlabel);
		chromosomeDropdown.setBackground(Color.white);
		chromosomeDropdown.setToolTipText("Current chromosome");
		add(chromosomeDropdown);
		final JLabel empty3 = new JLabel("  ");
		empty3.setEnabled(false);
		empty3.setOpaque(false);
		add(empty3);
		add(back);
		add(searchField);
		searchField.setForeground(Color.gray);
		searchField.setBorder(BorderFactory.createCompoundBorder(searchField.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		searchField.addMouseListener(this);
		add(back);
    searchField.addKeyListener(this);
		add(searchField);
		searchField.setForeground(Color.gray);
		back.addMouseListener(this);
		back.setToolTipText("Back");
		forward.addMouseListener(this);
		forward.setToolTipText("Forward");
		back.setEnabled(false);
		forward.setEnabled(false);
		searchField.addMouseListener(this);
		add(back);
		add(searchField);
		searchField.setForeground(Color.gray);
		back.addMouseListener(this);
		forward.addMouseListener(this);
		back.setEnabled(false);
		forward.setEnabled(false);
		forward.setMargin(new Insets(0, 2, 0, 2));
		back.setMargin(new Insets(0, 2, 0, 2));
		add(forward);
		final JLabel empty4 = new JLabel("  ");
		empty4.setOpaque(false);
		empty4.setEnabled(false);
		add(empty4);
		add(zoomout);
		final JLabel empty5 = new JLabel("  ");
		empty5.setEnabled(false);
		empty5.setOpaque(false);
		add(empty5);
		positionField.setEditable(false);
		positionField.setBackground(new Color(250, 250, 250));
		positionField.setMargin(new Insets(0, 2, 0, 0));
		positionField.setBorder(BorderFactory.createCompoundBorder(widthLabel.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		add(positionField);
		widthLabel.setEditable(false);
		widthLabel.setBackground(new Color(250, 250, 250));
		widthLabel.setMargin(new Insets(0, 2, 0, 0));
		widthLabel.setBorder(BorderFactory.createCompoundBorder(widthLabel.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		final JLabel empty6 = new JLabel("  ");
		empty6.setEnabled(false);
		empty6.setOpaque(false);
		add(empty6);
		add(widthLabel);
		final JLabel empty7 = new JLabel("  ");
		empty7.setOpaque(false);
		empty7.setEnabled(false);
		add(empty7);
		add(setbut);	
    openvcfs.addActionListener(this);
    openbams.addActionListener(this);
    addtracks.addActionListener(this);
    addcontrols.addActionListener(this);
    openProjectNew.addActionListener(this);
    saveProjectNew.addActionListener(this);
    saveProjectAsNew.addActionListener(this);
    dosomething.addActionListener(this);
    clear.addActionListener(this);
    zoomout.addActionListener(this);	
    
    
	}
  public void actionPerformed(final ActionEvent e) {
    if (e.getSource() == manage) {
			if (VariantHandler.frame == null) VariantHandler.main(new String[] {});			
			VariantHandler.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - VariantHandler.frame.getWidth() / 2,frame.getLocationOnScreen().y + frame.getHeight() / 6);

			VariantHandler.frame.setState(JFrame.NORMAL);
			VariantHandler.frame.setVisible(true);
			
			
			drawCanvas.repaint();
		} else if (e.getSource() == average) {
			if (Average.frame == null) Average.createAndShowGUI();
			Average.setSamples();
			Average.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - Average.frame.getWidth() / 2, frame.getLocationOnScreen().y + frame.getHeight() / 6);
			Average.frame.setState(JFrame.NORMAL);
			Average.frame.setVisible(true);
		} else if (e.getSource() == settings) {

			Settings.frame.setLocation(
					frame.getLocationOnScreen().x + frame.getWidth() / 2 - Settings.frame.getWidth() / 2,
					frame.getLocationOnScreen().y + frame.getHeight() / 6);

			Settings.frame.setState(JFrame.NORMAL);
			Settings.frame.setVisible(true);
		} else if (e.getSource() == update) {
			try {
				final Updater update = new Updater();
				update.execute();

			} catch (final Exception ex) {
				ex.printStackTrace();
			}

		} else if (e.getSource() == clearMemory) {
			VarCalculations.nullifyVarNodes();

			// FileRead.removeNonListVariants();f
			System.gc();
			MainPane.chromDraw.repaint();
		} else if (e.getSource() == zoomout) {

			MainPane.zoomout();
		} else if (e.getSource() == dosomething) {

	

		} 
		else if (e.getSource() == clear) {MainPane.clearData(); } 
		else if (e.getSource() == exit) { System.exit(0); } 
		else if (e.getSource() == openbams) {
			try {
				if (!checkGenome())	return;
				if (VariantHandler.frame != null) VariantHandler.frame.setState(Frame.ICONIFIED);				

				final FileDialog fc = new FileDialog(frame, "Choose BAM file(s)", FileDialog.LOAD);
				fc.setDirectory(MainPane.path);
				fc.setFile("*.bam;*.cram;*.link");
				fc.setFilenameFilter(new FilenameFilter() {
					public boolean accept(final File dir, final String name) {
						return name.toLowerCase().endsWith(".bam") || name.toLowerCase().endsWith(".cram")
								|| name.toLowerCase().endsWith(".link");
					}
				});
				fc.setMultipleMode(true);
				fc.setVisible(true);
				final File[] openfiles = fc.getFiles();

				if (openfiles != null && openfiles.length > 0) {

					MainPane.path = openfiles[0].getParent();
					MainPane.writeToConfig("DefaultDir=" + MainPane.path);
					final FileRead filereader = new FileRead(openfiles);

					filereader.start = (int) drawCanvas.selectedSplit.start;
					filereader.end = (int) drawCanvas.selectedSplit.end;
					filereader.readBAM = true;
					filereader.execute();

				}
			} catch (final Exception ex) {
				MainPane.showError(ex.getMessage(), "Error");
			}
		} else if (e.getSource() == addcontrols) {
			if (!checkGenome())	return;
			if (VariantHandler.frame != null) VariantHandler.frame.setState(Frame.ICONIFIED);
			
			final FileDialog fc = new FileDialog(frame, "Choose control file(s)", FileDialog.LOAD);
			fc.setDirectory(MainPane.controlDir);
			fc.setFile("*.vcf.gz");
			fc.setFilenameFilter(new FilenameFilter() {
				public boolean accept(final File dir, final String name) {
					return name.toLowerCase().endsWith(".vcf.gz");
				}
			});
			fc.setMultipleMode(true);
			fc.setVisible(true);
			final File[] openfiles = fc.getFiles();

			if (openfiles != null && openfiles.length > 0) {
				MainPane.controlDir = openfiles[0].getParent();
				MainPane.writeToConfig("DefaultControlDir=" + MainPane.controlDir);
				Control.addFiles(openfiles);
			}			
		} else if (e.getSource() == photo) {
			try {
				final ScreenPhotos photos = new ScreenPhotos();
				photos.execute();				
			} catch (final Exception ex) { ex.printStackTrace(); }
		} else if (e.getSource() == openvcfs) {
			try {
				if (!checkGenome())	return;
				if (VariantHandler.frame != null) VariantHandler.frame.setState(Frame.ICONIFIED);				

				final FileDialog fc = new FileDialog(frame, "Choose VCF file(s)", FileDialog.LOAD);
				fc.setDirectory(MainPane.path);
				
				fc.setFilenameFilter(new FilenameFilter() {
					public boolean accept(final File dir, final String name) {
						return name.toLowerCase().endsWith(".vcf") || name.toLowerCase().endsWith(".vcf.gz") || name.toLowerCase().endsWith(".bedg.gz");
					}
				});

				fc.setMultipleMode(true);
				fc.setVisible(true);
				final File[] openfiles = fc.getFiles();

				if (openfiles != null && openfiles.length > 0) {
					MainPane.path = openfiles[0].getParent();
					MainPane.writeToConfig("DefaultDir=" + MainPane.path);
					final FileRead filereader = new FileRead(openfiles);

					filereader.start = (int) drawCanvas.selectedSplit.start;
					filereader.end = (int) drawCanvas.selectedSplit.end;
					filereader.readVCF = true;
					filereader.execute();

				}	
			} catch (final Exception ex) {	ex.printStackTrace(); }

		} else if (e.getSource() == addtracks) {
			if (!checkGenome())	return;
			try {				
				if (VariantHandler.frame != null) VariantHandler.frame.setState(Frame.ICONIFIED);

				final FileDialog fc = new FileDialog(frame, "Choose track file(s)", FileDialog.LOAD);
				fc.setDirectory(MainPane.trackDir);
				fc.setFile("*.bed;*.bedgraph.gz;*.gff.gz;*.gff3.gz;*.bigwig;*.bw;*.bigbed;*.bb;*.tsv.gz;*.tsv.bgz;*.txt");
				fc.setFilenameFilter(new FilenameFilter() {
					public boolean accept(final File dir, final String name) {
						return name.toLowerCase().endsWith(".bed") || name.toLowerCase().endsWith(".bed.gz")
								|| name.toLowerCase().endsWith(".bedgraph.gz")
								|| name.toLowerCase().endsWith(".bedgraph.gz")
								|| name.toLowerCase().endsWith(".bedgraph.gz") || name.toLowerCase().endsWith(".gff.gz")
								|| name.toLowerCase().endsWith(".gff3.gz") || name.toLowerCase().endsWith(".bigwig")
								|| name.toLowerCase().endsWith(".bw") || name.toLowerCase().endsWith(".bigbed")
								|| name.toLowerCase().endsWith(".bb") || name.toLowerCase().endsWith(".tsv.gz")
								|| name.toLowerCase().endsWith(".tsv.bgz") || name.toLowerCase().endsWith(".txt");
					}
				});
				fc.setMultipleMode(true);
				fc.setVisible(true);
				final File[] openfiles = fc.getFiles();

				if (openfiles != null && openfiles.length > 0) {

					MainPane.trackDir = openfiles[0].getParent();
					MainPane.writeToConfig("DefaultMainPane.trackDir=" + MainPane.trackDir);
					final FileRead filereader = new FileRead(openfiles);
					filereader.readBED = true;
					filereader.execute();

				} else {
					// MainPane.showError("File(s) does not exist.", "Error");
				}

			} catch (final Exception ex) {
				MainPane.showError(ex.getMessage(), "Error");
			}
		} else if (e.getSource() == saveProjectAsNew) {
			
			if (VariantHandler.frame != null) {
				VariantHandler.frame.setState(Frame.ICONIFIED);
			}
			try {
				
				File savefile = null;
				final FileDialog fs = new FileDialog(frame, "Save project as...", FileDialog.SAVE);
				fs.setDirectory(MainPane.projectDir);
				fs.setFile("*.json");
				fs.setFilenameFilter(new FilenameFilter() {
					public boolean accept(final File dir, final String name) {
						return name.toLowerCase().endsWith(".json");
					}
				});
				fs.setVisible(true);
				
				while (true) {
					final String filename = fs.getFile();

					if (filename != null) {
						savefile = new File(fs.getDirectory() + "/" + filename);
						MainPane.projectDir = fs.getDirectory();
						MainPane.writeToConfig("DefaultMainPane.projectDir=" + MainPane.projectDir);

						if (!savefile.getAbsolutePath().endsWith(".json")) {
							savefile = new File(savefile.getAbsolutePath() + ".json");
						}					
						MainPane.projectValues.projectFile = savefile.getCanonicalPath();
						SessionHandler.saveSession();
						break;
					} else {
						break;
					}
				}
				
			} catch (final Exception ex) {
				ex.printStackTrace();
			}

		} else if (e.getSource() == saveProjectNew) {

			if (Draw.drawVariables.projectName.equals("Untitled")) {
				saveProjectAsNew.doClick();
			} else {
				SessionHandler.saveSession();
				
			}
		} else if (e.getSource() == openProjectNew) {
			if (!checkGenome())
				return;
			if (VariantHandler.frame != null) {
				VariantHandler.frame.setState(Frame.ICONIFIED);
			}
			final FileDialog fc = new FileDialog(frame, "Choose project file", FileDialog.LOAD);
			fc.setDirectory(MainPane.projectDir);
			fc.setFile("*.json");
			fc.setFilenameFilter(new FilenameFilter() {
				public boolean accept(final File dir, final String name) {
					return name.toLowerCase().endsWith(".json");
				}
			});
			fc.setMultipleMode(false);
			fc.setVisible(true);
			final String openfile = fc.getFile();

			if (openfile != null) {
				final File addfile = new File(fc.getDirectory() + "/" + openfile);
				MainPane.projectDir = fc.getDirectory();
				MainPane.writeToConfig("DefaultMainPane.projectDir=" + MainPane.projectDir);
				MainPane.clearData();
				SessionHandler.openSession(addfile);
			}
			
		} else if (e.getSource() == pleiadesButton) {
			MainPane.gotoURL("http://kaptah.local.lab.helsinki.fi/pleiades/");

		} else if (e.getSource() == tbrowser) {
			tablebrowser.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - VariantCaller.frame.getWidth() / 2,frame.getLocationOnScreen().y + frame.getHeight() / 6);
			tablebrowser.frame.setState(JFrame.NORMAL);
			tablebrowser.frame.setVisible(true);
		} else if (e.getSource() == bconvert) {
			bedconverter.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - VariantCaller.frame.getWidth() / 2,frame.getLocationOnScreen().y + frame.getHeight() / 6);
			bedconverter.frame.setState(JFrame.NORMAL);
			bedconverter.frame.setVisible(true);
		} else if (e.getSource() == peakCaller) {
			if (PeakCaller.frame == null) PeakCaller.main(new String[]{});

			PeakCaller.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - VariantCaller.frame.getWidth() / 2,	frame.getLocationOnScreen().y + frame.getHeight() / 6);
			PeakCaller.frame.setState(JFrame.NORMAL);
			PeakCaller.frame.setVisible(true);
		}/*  else if (e.getSource() == variantCaller) {
			if (VariantCaller.frame == null) VariantCaller.main(argsit);
			VariantCaller.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - VariantCaller.frame.getWidth() / 2,	frame.getLocationOnScreen().y + frame.getHeight() / 6);
			VariantCaller.frame.setState(JFrame.NORMAL);
			VariantCaller.frame.setVisible(true);
		}  */else if (e.getSource() == errorlog) {
			ErrorLog.frame.setLocation(frame.getLocationOnScreen().x + frame.getWidth() / 2 - ErrorLog.frame.getWidth() / 2, frame.getLocationOnScreen().y + frame.getHeight() / 6);	
			ErrorLog.frame.setState(JFrame.NORMAL);
			ErrorLog.frame.setVisible(true);

		} 
		/*
		 * else if(e.getSource() == welcome) { WelcomeScreen.main(args);
		 * WelcomeScreen.frame.setVisible(true);
		 * WelcomeScreen.frame.setLocation(frame.getLocationOnScreen().x+frame.getWidth(
		 * )/2 - WelcomeScreen.frame.getWidth()/2,
		 * frame.getLocationOnScreen().y+frame.getHeight()/6);
		 * 
		 * }
		 */

  }
  static boolean checkGenome() {
		if (chromosomeDropdown.getItemAt(0) == null) {
			MainPane.showError("Add reference genome first.", "Note");
			if (AddGenome.frame == null) AddGenome.createAndShowGUI();
			
			AddGenome.frame.setVisible(true);
			AddGenome.frame.setLocation(Main.frame.getLocationOnScreen().x + Main.frame.getWidth() / 2 - AddGenome.frame.getWidth() / 2,	Main.frame.getLocationOnScreen().y + Main.frame.getHeight() / 6);
			AddGenome.frame.setState(JFrame.NORMAL);
			return false;
		}
		return true;
	}
  public static void openPleiades(final String pleiadesurl) {
		boolean bamonly = false;
		String pleiades = pleiadesurl.trim();
		if (pleiades.toLowerCase().startsWith("bam")) {
			bamonly = true;
			pleiades = pleiades.replace("bam", "").trim();
		}

		if (pleiades.contains("`")) {
			pleiades.replace("`", "?");
		}
		if (pleiades.contains(" ")) {
			pleiades.replace(" ", "%20");
		}
		if (pleiades.contains("pleiades")) {
			try {
				final URL url = new URL(pleiadesurl.trim());
				// System.out.println(chooserText);
				final HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
				httpConn.connect();

				final int responseCode = httpConn.getResponseCode();

				if (responseCode == HttpsURLConnection.HTTP_OK) {

					final String loading = Loader.loadingtext;
					final InputStream inputStream = httpConn.getInputStream();
					final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
					Loader.loadingtext = loading + " 0MB";
					String line;
					final StringBuffer buffer = new StringBuffer("");
					while ((line = reader.readLine()) != null) {

						buffer.append(line);

					}
					inputStream.close();
					reader.close();
					String split2;
					final String[] split = buffer.toString().split("dataUnit");
					String location;
					final ArrayList<File> array = new ArrayList<File>();
					File[] paths;
					final FileSystemView fsv = FileSystemView.getFileSystemView();
					paths = File.listRoots();
					String loc = "/mnt";
					boolean missingfiles = false;
					for (final File path : paths) {
						if (fsv.getSystemDisplayName(path).contains("merit")) {
							loc = path.getCanonicalPath();
						}
					}

					for (int i = 0; i < split.length; i++) {

						if (!split[i].contains("lastLocation")) {

							continue;
						}

						split2 = split[i].split("\"lastLocation\":\"")[1];
						location = split2.substring(0, split2.indexOf("\"}"));
						String filename = "";
						final String testloc = location.replace("/mnt", loc) + "/wgspipeline/";
						final File testDir = new File(testloc);
						if (testDir.exists() && testDir.isDirectory()) {
							if (bamonly) {
								final File[] addDir = testDir.listFiles(new FilenameFilter() {
									public boolean accept(final File dir, final String name) {
										return name.toLowerCase().endsWith(".bam")
												|| name.toLowerCase().endsWith(".cram");
									}
								});
								if (addDir.length > 0) {
									filename = addDir[0].getName();
								}
							} else {
								final File[] addDir = testDir.listFiles(new FilenameFilter() {
									public boolean accept(final File dir, final String name) {
										return name.toLowerCase().endsWith(".vcf.gz");
									}
								});
								if (addDir.length > 0) {
									filename = addDir[0].getName();
								}
							}
						}

						location = testloc + "/" + filename;

						if (!new File(location).exists()) {

							if (!new File(location).exists()) {
								missingfiles = true;
								ErrorLog.addError("No sample files found in " + testloc);

							} else {
								array.add(new File(location));
							}
						} else {

							array.add(new File(location));
						}
					}

					final File[] files = new File[array.size()];
					for (int i = 0; i < files.length; i++) {

						files[i] = array.get(i);
					}
					final FileRead filereader = new FileRead(files);
					filereader.start = (int) drawCanvas.selectedSplit.start;
					filereader.end = (int) drawCanvas.selectedSplit.end;
					if (bamonly) {
						filereader.readBAM = true;
					} else {
						filereader.readVCF = true;
					}
					filereader.execute();
					if (missingfiles) {
						JOptionPane.showMessageDialog(MainPane.drawScroll, "Missing files. Check Tools->View log", "Note",	JOptionPane.INFORMATION_MESSAGE);
					}
				}
			} catch (final Exception ex) {
				ex.printStackTrace();
			}
		}
		return;
	}
  ActionListener ChromoDropActionListener = new ActionListener() {

		public void actionPerformed(final ActionEvent actionEvent) {

			try {

				if (actionEvent.getActionCommand() == "comboBoxChanged") {
					if (chromosomeDropdown.getItemCount() == 1) {
						return;
					}

					Draw.getSplits().get(0).getReadBuffer().setComposite(Draw.composite);
					Draw.getSplits().get(0).getReadBuffer().fillRect(0, 0,	Draw.getSplits().get(0).getReadImage().getWidth(),	MainPane.drawScroll.getViewport().getHeight());
					Draw.getSplits().get(0).getReadBuffer().setComposite(Draw.getSplits().get(0).getBackupr());				
					drawCanvas.clearReads();
					selectedChrom = chromosomeDropdown.getSelectedIndex();

					if (chromosomeDropdown.getSelectedItem() == null) {					
						Draw.getSplits().get(0).chromEnd = 100;
						Draw.getSplits().get(0).viewLength = 100;
						Draw.getSplits().get(0).start = 0;
					} else {

						// drawCanvas.chrom = chromosomeDropdown.getSelectedItem().toString();
						Draw.getSplits().get(0).chromEnd = chromIndex.get(MainPane.refchrom + chromosomeDropdown.getSelectedItem().toString())[1].intValue();
						chromLabel.setText("Chromosome " + chromosomeDropdown.getSelectedItem().toString());
						MainPane.chromDraw.cytoImage = null;
						Draw.getSplits().get(0).setCytoImage(null);
						Draw.getSplits().get(0).chrom = chromosomeDropdown.getSelectedItem().toString();
						Draw.getSplits().get(0).transStart = 0;
						Draw.getSplits().get(0).nullRef();

						final FileRead filereader = new FileRead();
						filereader.chrom = chromosomeDropdown.getSelectedItem().toString();

						if (!FileRead.search) {

							FileRead.searchStart = 0;
							FileRead.searchEnd = Draw.getSplits().get(0).chromEnd;
							drawCanvas.setStartEnd(0.0, (double) Draw.getSplits().get(0).chromEnd);
						}
						try {
							for (int i = 0; i < Getter.getInstance().getControlList().size(); i++) {
								Getter.getInstance().getControlList().get(i).controlled = false;
							}
						} catch (final Exception ex) {
							//Collections.synchronizedList(new ArrayList<ControlFile>()));
							ex.printStackTrace();
						}
						if (MainPane.nothread) {
							filereader.changeChrom(filereader.chrom);
							MainPane.nothread = false;
						} else {
							filereader.changeChrom = true;
							filereader.execute();
						}
					}
				}

				chromosomeDropdown.revalidate();
				chromosomeDropdown.repaint();
			} catch (final Exception e) {

				e.printStackTrace();
			}
		}
	};
  @Override
  public void keyTyped(KeyEvent e) {
    
  }
  @Override
  public void keyPressed(KeyEvent e) {
    int keyCode = e.getKeyCode();
    if (keyCode == KeyEvent.VK_ENTER) if (e.getSource() == searchField) search(searchField.getText());
  }
  @Override
  public void keyReleased(KeyEvent e) {
    
  }
  @Override
  public void mouseClicked(MouseEvent event) {

    if (event.getSource() == searchField) {
			searchField.setFocusable(true);
			searchField.requestFocus();
			if (event.getClickCount() == 2) {

				searchField.setText("");
			}
		} else {
			searchField.setFocusable(false);
		}
		if (event.getSource() == back && back.isEnabled()) {
			MainPane.undoPointer--;
			if (MainPane.undoPointer < 1) {
				MainPane.undoPointer = 0;
				back.setEnabled(false);
			}
			forward.setEnabled(true);
			String[] returnlist = parseSearch(MainPane.undoList.get(MainPane.undoPointer));
			searchField.setText(MainPane.undoList.get(MainPane.undoPointer));
			FileRead.search = true;
			drawCanvas.gotoPos(returnlist[0], Integer.parseInt(returnlist[1]), Integer.parseInt(returnlist[2]));

		}
		if (event.getSource() == forward && forward.isEnabled()) {
			MainPane.undoPointer++;
			if (MainPane.undoPointer >= MainPane.undoList.size() - 1) {
				MainPane.undoPointer = MainPane.undoList.size() - 1;
				forward.setEnabled(false);
			}
			back.setEnabled(true);
			String[] returnlist = parseSearch(MainPane.undoList.get(MainPane.undoPointer));
			FileRead.search = true;
			searchField.setText(MainPane.undoList.get(MainPane.undoPointer));
			drawCanvas.gotoPos(returnlist[0], Integer.parseInt(returnlist[1]), Integer.parseInt(returnlist[2]));

		}
  }
  public static void putMessage(final String message) {
		if (message != null) {
			widthLabel.setBackground(new Color(240, 210, 160));
			widthLabel.setText(message);

			MainPane.chromDraw.timer = System.currentTimeMillis();
			MainPane.chromDraw.repaint();
		} else {
			widthLabel.setBackground(new Color(250, 250, 250));
			widthLabel.setText(MethodLibrary.formatNumber(Draw.getSplits().get(0).chromEnd) + "bp");
		}
	}
  @Override
  public void mousePressed(MouseEvent event) {
    if (event.getSource() == chromlabel) {
			chromosomeDropdown.showPopup();

		} else if (event.getSource() == searchField) {
			searchField.requestFocus();
			searchField.setForeground(Color.black);
			if (searchField.getText().contains("Search by")) {
				searchField.setText("");
			}
		} 
  }
  @Override
  public void mouseReleased(MouseEvent e) {
   
  }
  @Override
  public void mouseEntered(MouseEvent e) {
   
  }
  @Override
  public void mouseExited(MouseEvent e) {
    
  }
  static void updatePositions(final double start, final double end) {
		widthLabel.setBackground(new Color(250, 250, 250));
		positionField.setText("chr" + drawCanvas.selectedSplit.chrom + ":" + MethodLibrary.formatNumber((int) start + 1)
				+ "-" + MethodLibrary.formatNumber((int) end));
		widthLabel.setText("" + MethodLibrary.formatNumber((int) (end - start)) + "bp");

	}

  public static void setChromDrop(final String dir) {
		try {

			if (!new File(MainPane.genomeDir.getCanonicalPath() + "/" + dir).exists() || dir.length() == 0) {
				if (chromModel != null) {
					chromModel.removeAllElements();
					chromosomeDropdown.removeAllItems();
					chromosomeDropdown.revalidate();
					chromosomeDropdown.repaint();
					searchTable.clear();
					if (Draw.getSplits().size() > 0) {
						Draw.getSplits().get(0).clearGenes();
					}
					MainPane.chromDraw.updateExons = true;
					MainPane.chromDraw.repaint();
				} else {
					final String[] empty = { "" };
					chromModel = new DefaultComboBoxModel<String>(empty);
					chromosomeDropdown = new SteppedComboBox(chromModel);
					chromosomeDropdown.revalidate();
					chromosomeDropdown.repaint();
				}
			} else {

				File chromindex = null;
				MainPane.selectedGenome = dir;
				final File defdir = new File(MainPane.genomeDir.getCanonicalPath() + "/" + dir);

				final File[] files = defdir.listFiles();
				if (files == null) {
					return;
				}
				String chromtemp = "";
				chromnamevector = Collections.synchronizedList(new ArrayList<String>());
				// boolean faifound = false;
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						continue;
					}
					if (files[i].getName().contains(".gz")) {
						continue;
					} else if (files[i].getName().contains(".fai")) {
						chromindex = new File(defdir.getCanonicalPath() + "/" + files[i].getName());
						// faifound = true;
					} else if (files[i].getName().contains(".fa")) {
						chromtemp = defdir.getCanonicalPath() + "/" + files[i].getName();
					}
				}
				if (chromtemp.equals("")) {
					final String[] empty = { "" };

					chromModel = new DefaultComboBoxModel<String>(empty);
					chromosomeDropdown = new SteppedComboBox(chromModel);
					chromosomeDropdown.revalidate();
					chromosomeDropdown.repaint();
					return;
				}

				if (MainPane.referenceFile != null) {
					MainPane.referenceFile.close();
				}
				MainPane.referenceFile = new RandomAccessFile(chromtemp, "r"); 

				MainPane.ref = new File(chromtemp);

				final BufferedReader reader = new BufferedReader(new FileReader(chromindex));
				String line;
				String[] split;
				ChromDraw.chromPos = new HashMap<String, Integer>();
				chromIndex = new Hashtable<String, Long[]>();
				MainPane.textlength = 0;
				while ((line = reader.readLine()) != null) {

					split = line.split("\t");
					chromnamevector.add(split[0]);
					final Long[] add = { Long.parseLong(split[2]), Long.parseLong(split[1]), Long.parseLong(split[3]) };
					if (split[0].equals("MT")) {
						ChromDraw.chromPos.put("M", Integer.parseInt(split[1]));
						chromIndex.put("M", add);
					}
					chromIndex.put(split[0], add);
					if (split[0].length() > MainPane.textlength) {
						MainPane.textlength = split[0].length();
					}
					ChromDraw.chromPos.put(split[0], Integer.parseInt(split[1]));

				}
				reader.close();
				final MethodLibrary.ChromSorter sorter = new MethodLibrary.ChromSorter();
				Collections.sort(chromnamevector, sorter);
				chromnames = new String[chromnamevector.size()];

				if (chromnamevector.get(0).contains("chr")) {

					MainPane.refchrom = "chr";
				} else {
					MainPane.refchrom = "";
				}
				for (int i = 0; i < chromnames.length; i++) {
					chromnames[i] = chromnamevector.get(i).replace(MainPane.refchrom, "");

				}

				chromModel = new DefaultComboBoxModel<String>(chromnames);
				if (chromosomeDropdown == null) {
					chromosomeDropdown = new SteppedComboBox(chromModel);

				} else {

					chromosomeDropdown.setModel(chromModel);
				}
				MainPane.clicked = false;

				MainPane.refDropdown.setSelectedItem(dir);
				MainPane.clicked = true;
				MainPane.clickedAnno = false;
				MainPane.setAnnotationDrop(dir);

				if (MainPane.defaultAnnotation.length() == 0) {
					MainPane.geneDropdown.setSelectedIndex(0);
				} else {
					MainPane.geneDropdown.setSelectedItem(MainPane.defaultAnnotation);
				}
				MainPane.clickedAnno = true;

				final int letterlength = chromosomeDropdown.getFontMetrics(chromosomeDropdown.getFont()).stringWidth("E");
				chromosomeDropdown.setPopupWidth(MainPane.textlength * letterlength + 25);
				chromosomeDropdown.revalidate();
				chromosomeDropdown.repaint();
				MainPane.refDropdown.setToolTipText(MainPane.refDropdown.getSelectedItem().toString());
				MainPane.geneDropdown.setToolTipText(MainPane.geneDropdown.getSelectedItem().toString());
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		
	}
  @Override
  public void componentResized(ComponentEvent e) {
   
  }
  @Override
  public void componentMoved(ComponentEvent e) {
  
  }
  @Override
  public void componentShown(ComponentEvent e) {
   
  }
  @Override
  public void componentHidden(ComponentEvent e) {
   
  }
  static String[] parseSearch(String searchstring) {
		if (searchstring.replace(" ", "").toUpperCase().matches("CHR.{1,2}(?!:)")) {

			if (chromnamevector.contains(searchstring.replace(" ", "").toUpperCase().substring(3))) {
				chromosomeDropdown.setSelectedItem(searchstring.toUpperCase().substring(3));
			}
			return null;
		}
		if (searchstring.contains(",")) {
			searchstring = searchstring.replace(" ", "").replace(",", "");
		}
		if (searchstring.contains("chr")) {
			searchstring = searchstring.replace(" ", "").replace("chr", "");
		}

		if (MenuBar.searchTable.containsKey(searchstring.replace(" ", "").toUpperCase()) || MainPane.geneIDMap.containsKey(searchstring.replace(" ", "").toUpperCase())) {

			FileRead.search = true;
			String[] result = {};
			if (MenuBar.searchTable.containsKey(searchstring.replace(" ", "").toUpperCase())) {
				result = MenuBar.searchTable.get(searchstring.replace(" ", "").toUpperCase());
			} else {
				result = MenuBar.searchTable.get(MainPane.geneIDMap.get(searchstring.replace(" ", "").toUpperCase()));
			}
			// drawCanvas.clearReads();
			FileRead.searchStart = Integer.parseInt(result[1]);

			FileRead.searchEnd = Integer.parseInt(result[2]);
			searchChrom = result[0];
			searchStart = Integer.parseInt(result[1]);
			searchEnd = Integer.parseInt(result[2]);
			// drawCanvas.gotoPos(result[0], Integer.parseInt(result[1]),
			// Integer.parseInt(result[2]));
			final String[] returnstring = { result[0], result[1], result[2] };
			return returnstring;
		} else if (searchstring.replace(" ", "").matches("\\d+-?\\d+?")) {

			if (searchstring.contains("-")) {
				// drawCanvas.clearReads();
				// drawCanvas.gotoPos(Draw.getSplits().get(0).chrom,Integer.parseInt(searchString.replace("
				// ", "").split("-")[0]), Integer.parseInt(searchString.replace(" ",
				// "").split("-")[1]));
				final String[] returnstring = { Draw.getSplits().get(0).chrom,
						searchstring.replace(" ", "").split("-")[0], searchstring.replace(" ", "").split("-")[1] };
				return returnstring;
			} else {
				// drawCanvas.clearReads();

				// drawCanvas.gotoPos(Draw.getSplits().get(0).chrom,Integer.parseInt(searchString.replace("
				// ", ""))-200, Integer.parseInt(searchString.replace(" ", ""))+200);
				final String[] returnstring = { Draw.getSplits().get(0).chrom,
						"" + (Integer.parseInt(searchstring.replace(" ", "")) - 200),
						"" + (Integer.parseInt(searchstring.replace(" ", "")) + 200) };
				return returnstring;
			}

			// chromDraw.updateExons = true;
			// chromDraw.repaint();
		}

		else if (searchstring.replace(" ", "").matches(".+:\\d+-?\\d+?")) {
			final String[] result = searchstring.replace(" ", "").split(":");

			if (result[1].contains("-")) {
				// drawCanvas.clearReads();
				// drawCanvas.gotoPos(result[0].replace(" ", ""),
				// Integer.parseInt(result[1].split("-")[0]),
				// Integer.parseInt(result[1].split("-")[1]));

				searchChrom = result[0].replace(" ", "");
				final String[] returnstring = { result[0].replace(" ", ""), result[1].split("-")[0],
						result[1].split("-")[1] };
				return returnstring;
			} else {
				// drawCanvas.clearReads();
				// drawCanvas.gotoPos(result[0].replace(" ", ""),
				// Integer.parseInt(result[1])-200, Integer.parseInt(result[1])+200);
				searchChrom = result[0].replace(" ", "");
				final String[] returnstring = { result[0].replace(" ", ""), "" + (Integer.parseInt(result[1]) - 200),
						"" + (Integer.parseInt(result[1]) + 200) };
			return returnstring;
		}
	}
	return null;
	
}
public static void search(final String pos) {

  if (!MainPane.isPosition(pos)) return;

  MainPane.drawCanvas.scrollbar = false;
  // searchString = searchField.getText();
  if (pos.toUpperCase().startsWith("S ") && pos.length() > 2) {

    for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {

      if (Getter.getInstance().getSampleList().get(i).getName().toUpperCase().contains(pos.toUpperCase().substring(2))) {

        Draw.drawVariables.setVisibleStart.accept(i);
        Draw.drawVariables.setVisibleSamples.accept(1);

        drawCanvas.resizeCanvas(Main.frame.getWidth(),(int) (MainPane.samples * Draw.drawVariables.sampleHeight));
        Draw.setScrollbar((int) (i * Draw.drawVariables.sampleHeight));
        break;
      }
    }
    return;
  }
  String[] returnlist;
  if (pos.contains(";")) {
    searchList = pos.split(";");

    for (int i = 0; i < searchList.length; i++) {

      returnlist = parseSearch(searchList[i]);

      if (returnlist != null) {

        if (i == 0) {
          FileRead.search = true;
          drawCanvas.gotoPos(returnlist[0], Integer.parseInt(returnlist[1]),
              Integer.parseInt(returnlist[2]));
          FileRead.search = false;
        } else {

          drawCanvas.addSplit(returnlist[0], Integer.parseInt(returnlist[1]),
              Integer.parseInt(returnlist[2]));
        }
      }
    }
  } else {
    try {
      returnlist = parseSearch(pos);
      if (returnlist != null) {
        if (MainPane.undoPointer < MainPane.undoList.size() - 1) {
          MainPane.undoList.add(MainPane.undoPointer + 1, searchField.getText());
          if (MainPane.undoPointer < MainPane.undoList.size() - 1) {
            for (int i = MainPane.undoPointer + 2; i < MainPane.undoList.size(); i++) {
              MainPane.undoList.remove(i);
              i--;
            }
          }
        } else {
          MainPane.undoList.add(searchField.getText());
        }
        MainPane.undoPointer = MainPane.undoList.size() - 1;

        forward.setEnabled(false);

        if (MainPane.undoPointer > 0) {
          back.setEnabled(true);
        }
        FileRead.search = true;
        drawCanvas.gotoPos(returnlist[0], Integer.parseInt(returnlist[1]), Integer.parseInt(returnlist[2]));
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }
}
}
