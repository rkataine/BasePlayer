/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;
import java.io.File;
import java.io.Serializable;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class DrawVariables implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final DrawVariables INSTANCE = new DrawVariables();	
	public double sampleHeight = Draw.defaultSampleHeight, heightFrac, varMinHeight, pixel, start;
	public Boolean somatic = false; 
	private int visiblesamples=1,visiblestart=0;
	public int variantsStart = 0, variantsEnd = 0;
	public int scrollbarpos = 0;
	public String projectName = "Untitled";
	
	public File projectFile;
	
	public static DrawVariables getInstance() {
		return INSTANCE;
	}
	public Supplier<Integer> getVisibleSamples = () -> this.visiblesamples;
	public Supplier<Integer> getVisibleStart = () -> this.visiblestart;
	public Consumer<Integer> setVisibleStart = value -> {
		if (value < 0) value = 0;
		else if (value + visiblesamples > MainPane.samples) value = MainPane.samples - visiblesamples;
		this.visiblestart = value; 
	};
	public Consumer<Integer> setVisibleSamples = value -> {
		
		if (value > MainPane.samples) value = MainPane.samples;
		else if (value < 1) value = 1;
		else if (visiblestart + value > MainPane.samples) setVisibleStart.accept(visiblestart - 1);
		this.visiblesamples = value; 
	};
	
	

}