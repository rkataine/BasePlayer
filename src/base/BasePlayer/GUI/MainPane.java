/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.GUI;

import java.awt.*;
import java.awt.event.*;
import htsjdk.tribble.readers.TabixReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.io.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import base.BasePlayer.GUI.BedCanvas.bedFeatureFetcher;
import base.BasePlayer.GUI.modals.*;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.reads.Reads;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.utils.Average;
import base.BasePlayer.utils.PeakCaller;
import base.BasePlayer.utils.Updater;
import base.BasePlayer.variants.*;
import base.BasePlayer.*;
import base.BasePlayer.GUI.listeners.MainPaneActionListeners;

	public class MainPane extends JPanel implements ChangeListener, ComponentListener, MouseListener, KeyListener, MouseMotionListener {
	private static final long serialVersionUID = 1L;		
    public static JFrame frame = Main.frame;  
	
	//UI    
    
    public static String version = "1.0.2";
    public static int sidebarWidth = 200;
    static String[] argsit = {}, args;    
    public static GraphicsDevice gd;   
    public static boolean loaddraw = false;
    public static HashMap<Byte, String> getBase = new HashMap<Byte,String>();
    public static int width = Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int height= Toolkit.getDefaultToolkit().getScreenSize().height;
    public static ArrayList<JLabel> labels = new ArrayList<JLabel>();
    public static int loadTextWidth = 200;
    public static Dimension screenSize;
    public static int drawWidth, drawHeight, chromHeight = 150, bedHeight = 200;
    public static GradientPaint gradient = new GradientPaint(drawWidth/2-loadTextWidth/2,0,Color.red,drawWidth/2+loadTextWidth,height,Color.green,true);
   
    public static int samples = 0, varsamples = 0, readsamples = 0;
    public static boolean readingControls = false, readingbeds = false;
    public static String gerp;
   
    public static int textlength = 0, annolength = 0, reflength = 0;
    public static int coverages = 0, alleles = 1, qualities = 2, gqs = 3;
    public static int letterlength = 0 ;
	//Labels	
    
  
   
    public static HashMap<String, String> factorNames = new HashMap<String, String>();
    public static boolean configChanged = false;
    static int trackdivider = 0;
  
    static Image A, C, G, T;
    public static HashMap<Byte, Integer> baseMap = new HashMap<Byte, Integer>();
    static Dimension chromDimensions, bedDimensions, drawDimensions;
    public static Dimension buttonDimension;
    public static int buttonHeight = 20;
    public static int buttonWidth = 60;
    public static FileRead fileReader = new FileRead();
    public static String refchrom = "";
    public static String selectedGenome;
    
    static Loader loading; 
   
    public static Hashtable<String, ArrayList<File>> genomehash = new Hashtable<String, ArrayList<File>>();
    public static Hashtable<String, File> fastahash = new Hashtable<String, File>();
    
    static String path;
    public static boolean cancelhover = false, cancel = false;
    static JLabel backImage;
    public static BedCanvas bedCanvas;
    //static VarMaster varmaster;
    public static ChromDraw chromDraw;
    public static Draw drawCanvas;
    public static ControlCanvas controlDraw;
    static java.util.List<String> undoList = Collections.synchronizedList(new ArrayList<String>());
    static int undoPointer = 0;
    public static Hashtable<String, Integer> mutTypes = new Hashtable<String, Integer>();
    static Hashtable<String, String> bases;
    public static HashMap<Byte, Double> background = new HashMap<Byte, Double>();
	//Buttons etc.
    //public static JMenuItem variantCaller;
   
    public static String defaultGenome = "";
    public static String downloadDir = "";
    static String proxyHost = "";
    static String proxyPort = "";
    static String proxyType = "";
    static boolean isProxy = false;
    static JSplitPane splitPane;
    public static JSplitPane trackPane;
    public static JSplitPane varpane;
    static JSplitPane drawpane;
    public static boolean nothread = false, noreadthread = false;	
   
    public static Hashtable<String, String> geneIDMap = new Hashtable<String, String>();
    
  //  static double[][] snow = new double[200][4];
    static String[] snowtable = {"A", "C", "G", "T" };
    public static String userDir;
    public static File genomeDir;
    public static boolean clicked = false;
		public static boolean clickedAnno = false;
    VarNode prevTemp;
    public static boolean rightclick = false;
    
    public static JPanel chrompan;
	static JLabel memLabel = new JLabel("");		
    static JSplitPane upPanel;
	

	static MenuBar menubar;

	public static boolean updatelauncher = false;


	public static RandomAccessFile referenceFile;    
	public static Font menuFont, menuFontBold;
	
	public static SteppedComboBox refDropdown;	
	public static SteppedComboBox geneDropdown;	
	static File[] genomes;
	//UI	
    static MouseListener thisMainListener;    
    public static DefaultComboBoxModel<String> refModel;
    static DefaultComboBoxModel<String> geneModel;
    public static Hashtable<String, int[][]> SELEXhash = new Hashtable<String, int[][]>();
		public static JScrollPane drawScroll;
		public static JScrollPane chromScroll;
		public static JScrollPane bedScroll;
		public static JScrollPane controlScroll;
		public static String hoverGenome = "", hoverAnnotation = "";
		
    
	MyFilterLINK linkFilter = new MyFilterLINK();
	MyFilterBAM bamFilter = new MyFilterBAM();
	MyFilterVCF vcfFilter = new MyFilterVCF();
	String defaultSelectType = "vcf";
	public static ProjectValues projectValues = new ProjectValues();
	// private String searchString;
	static Dimension fieldDimension;
	private File[] annofiles;
	private JTextField chooserTextField;
	public static int mouseX = 0;
	public static String lineseparator;
	public static String savedir = "";
	public static String controlDir = "";
	public static String trackDir = "";
	public static String projectDir = "";
	static BasicSplitPaneDivider splitPaneDivider, trackPaneDivider, chromPaneDivider, varPaneDivider;
	public static String annotationfile = "";	
	public static File ref;
	public static int canceltextwidth;	
	private static String chooserText;
	static Double vardivider = 0.0;
	public static int annotation = 0;
	public static String defaultAnnotation = "";
	public static ProxySettings proxysettings;
	static boolean shift = false;
	private static boolean resizing;
	public static GlassPane glassPane;
	
	public MainPane() {
		
		super(new GridBagLayout());
		try {
			
			Launcher.fromMain = true;
			Launcher.main(args);
			Settings.main(args);
			VariantHandler.main(argsit);
			
			ToolTipManager.sharedInstance().setInitialDelay(100);
			UIManager.put("ToolTip.background", new Color(255, 255, 214));
			UIManager.put("ToolTip.border", BorderFactory.createCompoundBorder(UIManager.getBorder("ToolTip.border"),
			BorderFactory.createEmptyBorder(4, 4, 4, 4)));
			lineseparator = System.getProperty("line.separator");
			proxysettings = new ProxySettings();
			screenSize = new Dimension(width-20, height);
			drawHeight = (int) (screenSize.getHeight() * 0.8);
			sidebarWidth = (int) (screenSize.getWidth() * 0.1);	
			drawWidth = (int) (screenSize.getWidth() - sidebarWidth);
			chromHeight = height - drawHeight; 
			drawDimensions = new Dimension(drawWidth, drawHeight - chromHeight);
			bedDimensions = new Dimension(drawWidth, bedHeight);
			chromDimensions = new Dimension(drawWidth - sidebarWidth - 1, drawHeight);
			drawCanvas = new Draw((int) drawDimensions.getWidth(), (int) drawDimensions.getHeight());
			glassPane = new GlassPane();
			menubar = new MenuBar();
			MenuBar.drawCanvas = drawCanvas;
			bedScroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			bedScroll.getViewport().setPreferredSize(bedDimensions);

			drawScroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			drawScroll.getViewport().setPreferredSize(drawDimensions);

			chromScroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,	JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			chromScroll.getViewport().setPreferredSize(chromDimensions);
			drawScroll.getVerticalScrollBar().setAutoscrolls(false);

			controlScroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,	JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			
			bedCanvas = new BedCanvas(drawWidth, 200);
			chromDraw = new ChromDraw(drawWidth, chromHeight);
			addSplit(MenuBar.chromosomeDropdown.getItemAt(0));
			controlDraw = new ControlCanvas((int) bedDimensions.getWidth(), (int) bedDimensions.getHeight());
		
			gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
			width = gd.getDisplayMode().getWidth();
			height = gd.getDisplayMode().getHeight();
			if (Launcher.fontSize.equals("")) {
				if (width < 1500) {
					BaseVariables.defaultFontSize = 11;

					buttonHeight = BaseVariables.defaultFontSize * 2;
					buttonWidth = BaseVariables.defaultFontSize * 6;

				} else if (width < 2000) {
					BaseVariables.defaultFontSize = 12;

					buttonHeight = BaseVariables.defaultFontSize * 2 + 4;
					buttonWidth = BaseVariables.defaultFontSize * 6 + 4;

				} else if (width < 3000) {
					BaseVariables.defaultFontSize = 15;
					buttonHeight = BaseVariables.defaultFontSize * 2 + 4;
					buttonWidth = BaseVariables.defaultFontSize * 6 + 4;
				} else {
					BaseVariables.defaultFontSize = 19;
					buttonHeight = BaseVariables.defaultFontSize * 2 + 4;
					buttonWidth = BaseVariables.defaultFontSize * 6 + 4;
				}
			} else {
				try {
					BaseVariables.defaultFontSize = Integer.parseInt(Launcher.fontSize);
				} catch (final Exception e) {
					BaseVariables.defaultFontSize = 12;
				}
			}
			BaseVariables.readHeight = BaseVariables.defaultFontSize;
			menuFont = new Font("SansSerif", Font.PLAIN, BaseVariables.defaultFontSize);
			menuFontBold = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
			BaseVariables.defaultFont = new Font("SansSerif", Font.PLAIN, BaseVariables.defaultFontSize);
			// menuFont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		final FileSystemView fsv = FileSystemView.getFileSystemView();
		final File[] paths = File.listRoots();

		for (final File path : paths) {
			if (fsv.getSystemDisplayName(path).contains("merit")) {
				MenuBar.pleiades = true;
			}
		}

		
		thisMainListener = this;
		try {
			
			
			
			userDir = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent().replace("%20", " ");
			

			// Average.frame.setVisible(false);

			try {

				savedir = Launcher.defaultSaveDir;
				path = Launcher.defaultDir;
				gerp = Launcher.gerpfile;
				defaultGenome = Launcher.defaultGenome;
				defaultAnnotation = Launcher.defaultAnnotation;
				isProxy = Launcher.isProxy;
				proxyHost = Launcher.proxyHost;
				proxyPort = Launcher.proxyPort;
				proxyType = Launcher.proxyType;
				if (isProxy) {
					ProxySettings.useProxy.setSelected(true);
				}
				if (!proxyHost.equals("")) {
					ProxySettings.hostField.setText(proxyHost);
				}
				if (!proxyPort.equals("")) {
					ProxySettings.portField.setText(proxyPort);
				}
				if (!Launcher.proxyType.equals("")) {
					ProxySettings.proxytypes.setSelectedItem(proxyType);
				}
				if (Launcher.backColor.equals("")) {
					Draw.backColor = new Color(90, 90, 90);
				}

				else {

					Draw.backColor = new Color(Integer.parseInt(Launcher.backColor),
							Integer.parseInt(Launcher.backColor), Integer.parseInt(Launcher.backColor));
					Settings.graySlider.setValue(Integer.parseInt(Launcher.backColor));
				}

				if (Launcher.genomeDir.equals("")) {

					genomeDir = new File(userDir + "/genomes/");
				} else {
					if (new File(Launcher.genomeDir).exists()) {

						genomeDir = new File(Launcher.genomeDir);
					} else {
						genomeDir = new File(userDir + "/genomes/");
					}
				}

				annotationfile = defaultAnnotation;
				controlDir = Launcher.ctrldir;
				trackDir = Launcher.trackDir;
				projectDir = Launcher.projectDir;
				downloadDir = Launcher.downloadDir;
			} catch (final Exception e) {
				e.printStackTrace();
			}
			
			new BaseConstants();
			setGenomes();

			MenuBar.chromosomeDropdown.setSelectedIndex(0);
			
			MenuBar.positionField.setText("chr1:1-" + MethodLibrary.formatNumber(Draw.getSplits().get(0).chromEnd));
			MenuBar.positionField.setToolTipText("Current chromosomal region");
			MenuBar.widthLabel.setText(MethodLibrary.formatNumber(Draw.getSplits().get(0).chromEnd) + "bp");
    	MenuBar.widthLabel.setToolTipText("Current region width in base pairs");

			try {

				A = Toolkit.getDefaultToolkit().getImage(Main.class.getResource("SELEX/A.png"));
				C = Toolkit.getDefaultToolkit().getImage(Main.class.getResource("SELEX/C.png"));
				G = Toolkit.getDefaultToolkit().getImage(Main.class.getResource("SELEX/G.png"));
				T = Toolkit.getDefaultToolkit().getImage(Main.class.getResource("SELEX/T.png"));

			} catch (final Exception e) {
				e.printStackTrace();
			}
			ErrorLog.main(args);

			//this.setBackground(Color.black);
			UIManager.put("FileChooser.readOnly", Boolean.TRUE);

			setBackground(Draw.sidecolor);
			setBorder(BorderFactory.createLineBorder(Color.white));
			

			frame.addKeyListener(this);
			//frame.getContentPane().setBackground(Color.black);
			frame.addMouseListener(this);
			

			glassPane.addMouseMotionListener(glassPaneMotionListener);

			background.put((byte) 'A', 0.3);
			background.put((byte) 'C', 0.2);
			background.put((byte) 'G', 0.2);
			background.put((byte) 'T', 0.3);

			VariantCaller.main(argsit);
			PeakCaller.main(argsit);

			

			if (Launcher.firstStart) {
				WelcomeScreen.createAndShowGUI();
				WelcomeScreen.frame.setLocation((int) (screenSize.getWidth() / 2 - WelcomeScreen.frame.getWidth() / 2),	(int) (screenSize.getHeight() / 6));
				WelcomeScreen.frame.setVisible(true);
			}
			createBaseMaps();
			setButtons();
			
			Updater.CheckUpdates check = new Updater().new CheckUpdates();
			check.execute();
			frame.requestFocus();
			drawCanvas.addKeyListener(this);
			bedCanvas.addKeyListener(this);
			setFonts();
			MenuBar.chromLabel.setText("Chromosome " + MenuBar.chromosomeDropdown.getSelectedItem().toString());
			check.execute();
			Draw.getSplits().get(0).setCytoImage(chromDraw.createBands(Draw.getSplits().get(0)));
		} catch (final Exception ex) {
			ex.printStackTrace();
			showError(ex.getMessage(), "Error");
		}
		Setter.getInstance().setUpdateScreen();
	}
	private void setGenomes() {
		try {

			File annodir;

			File[] annotations;
			
			File[] fastadir;
			final String[] empty = {};

			refModel = new DefaultComboBoxModel<String>(empty);

			refDropdown = new SteppedComboBox(refModel);
			refDropdown.addMouseListener(this);
			final String[] emptygenes = {};
			refDropdown.addActionListener(MainPaneActionListeners::refDropActionListener);

			geneModel = new DefaultComboBoxModel<String>(emptygenes);
			geneDropdown = new SteppedComboBox(geneModel);
			geneDropdown.addMouseListener(this);
			final File[] genomes = genomeDir.listFiles(new FilenameFilter() {
				public boolean accept(final File dir, final String name) {
					return !name.contains(".txt") && !name.startsWith(".");
				}
			});
			if (genomes != null) {
				for (int i = 0; i < genomes.length; i++) {
					if (!genomes[i].isDirectory()) {
						continue;
					}
					annodir = new File(genomes[i].getAbsolutePath() + "/annotation/");
					if (genomes[i].isDirectory()) {
						fastadir = genomes[i].listFiles();
						for (int f = 0; f < fastadir.length; f++) {
							if (fastadir[f].isDirectory()) {
								continue;
							}
							if (fastadir[f].getName().contains(".fai")) {
								continue;
							} else if (fastadir[f].getName().contains(".fa")) {
								fastahash.put(genomes[i].getName(), fastadir[f]);
							}
						}
					}

					annotations = annodir.listFiles();
					genomehash.put(genomes[i].getName(), new ArrayList<File>());

					refModel.addElement(genomes[i].getName());
					if (genomes[i].getName().length() > reflength) {
						reflength = genomes[i].getName().length();
					}
				
					if (annotations != null) {
						for (int j = 0; j < annotations.length; j++) {
							annofiles = annotations[j].listFiles();
							for (int f = 0; f < annofiles.length; f++) {
								if (annofiles[f].getName().endsWith(".bed.gz")) {
									if (annofiles[f].getName()
											.substring(0, annofiles[f].getName().indexOf(".bed.gz"))
											.length() > annolength) {
										annolength = annofiles[f].getName().length();
									}

									genomehash.get(genomes[i].getName()).add(annofiles[f].getAbsoluteFile());
								
									break;
								}
							}
						}
					}
				}
				refModel.addElement("Add new reference...");

			}

			if (genomes.length == 0) {					
				AddGenome.createAndShowGUI();
				AddGenome.frame.setTitle("Add new genome");

				AddGenome.remove.setEnabled(false);
				AddGenome.download.setEnabled(false);

				AddGenome.frame.setLocation((int) (screenSize.getWidth() / 2 - AddGenome.frame.getWidth() / 2), (int) (screenSize.getHeight() / 6));

				AddGenome.frame.setState(JFrame.NORMAL);
				AddGenome.frame.setVisible(true);
				AddGenome.frame.setAlwaysOnTop(true);
				
				if (genomes.length != 0) {
					if (!genomehash.containsKey(defaultGenome)) {
						MenuBar.setChromDrop(genomes[0].getName());
						defaultGenome = genomes[0].getName();
					} else MenuBar.setChromDrop(defaultGenome);						
					getBands();
					getExons();
				} else MenuBar.setChromDrop(null);					
			} else {
				if (!genomehash.containsKey(defaultGenome)) {
					MenuBar.setChromDrop(genomes[0].getName());
					defaultGenome = genomes[0].getName();
				} else MenuBar.setChromDrop(defaultGenome);
				
				getBands();
				getExons();
			}
		} catch (final Exception e) { e.printStackTrace(); }
	}
	public static void getExons() {
		try {
			String s;
			String[] exonSplit;
			Boolean found = false;
			if (ChromDraw.exonReader != null) {
				ChromDraw.exonReader.close();
			}
			for (int i = 0; i < genomehash.get(defaultGenome).size(); i++) {
				if (genomehash.get(defaultGenome).get(i).getName().equals(defaultAnnotation)) {
					ChromDraw.exonReader = new TabixReader(genomehash.get(defaultGenome).get(i).getCanonicalPath());
					annotationfile = genomehash.get(defaultGenome).get(i).getName();
					annotation = i;
					found = true;
					break;
				}

			}
			if (!found) {

				if (genomehash.get(defaultGenome).size() > 0) {
					ChromDraw.exonReader = new TabixReader(genomehash.get(defaultGenome).get(0).getCanonicalPath());
					annotationfile = genomehash.get(defaultGenome).get(0).getName();
					annotation = 0;
				} else {
					annotationfile = "";
				}
			}
			if (ChromDraw.exonReader == null) {
				return;
			}
			MenuBar.searchTable.clear();
			while ((s = ChromDraw.exonReader.readLine()) != null) {
				if (s.startsWith("#")) continue;				
				exonSplit = s.split("\t");

				if (!MenuBar.searchTable.containsKey(exonSplit[3].toUpperCase())) {
					final String[] adder = { exonSplit[0], exonSplit[1], exonSplit[2] };
					MenuBar.searchTable.put(exonSplit[3].toUpperCase(), adder);
					if (exonSplit[6].contains(":")) geneIDMap.put(exonSplit[6].split(":")[1].toUpperCase(), exonSplit[3].toUpperCase());
					else geneIDMap.put(exonSplit[6].toUpperCase(), exonSplit[3].toUpperCase());				

				} else {
					try {
						if (Integer.parseInt(MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1]) > Integer.parseInt(exonSplit[1]))
							MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1] = exonSplit[1];						
						if (Integer.parseInt(MenuBar.searchTable.get(exonSplit[3].toUpperCase())[2]) < Integer.parseInt(exonSplit[2]))
							MenuBar.searchTable.get(exonSplit[3].toUpperCase())[2] = exonSplit[2];						
					} catch (final Exception e) {	System.out.println("error: " + MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1]);	}
				}
			}
			ChromDraw.exonReader.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	void setButtons() {
		try {		
			final GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.NORTHWEST;
			c.insets = new Insets(1, 4, 4, 2);
			c.gridx = 0;
			c.gridy = 0;
			c.gridwidth = 1;
			
			add(menubar, c);
			c.gridx = 1;
			
			c.gridwidth = 10;
			c.gridx = 0;
			c.gridy = 1;
			
			controlScroll.getViewport().setPreferredSize(bedDimensions);
			controlScroll.getViewport().add(controlDraw);
			controlDraw.setVisible(false);
			controlScroll.setVisible(false);

			chromScroll.setBorder(BorderFactory.createEmptyBorder());
			drawScroll.setBorder(BorderFactory.createEmptyBorder());
			bedScroll.setBorder(BorderFactory.createLoweredBevelBorder());
			controlScroll.setBorder(BorderFactory.createLoweredBevelBorder());

		

			chromScroll.getViewport().add(chromDraw);
			drawScroll.getViewport().add(drawCanvas);
			drawScroll.addMouseListener(this);
			bedScroll.getViewport().add(bedCanvas);

			frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

			c.weightx = 1.0;
			c.weighty = 1.0;
			c.fill = GridBagConstraints.BOTH;

			trackPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, bedScroll, controlScroll);
			trackPane.setUI(new BasicSplitPaneUI() {
				public BasicSplitPaneDivider createDefaultDivider() {
					return new BasicSplitPaneDivider(this) {
						private static final long serialVersionUID = 1L;
						public void setBorder(final Border b) {	}
						@Override
						public void paint(final Graphics g) {
							g.setColor(Color.lightGray);
							g.fillRect(0, 0, getSize().width, getSize().height);
							super.paint(g);
						}
					};
				}
			});
			trackPane.setBorder(null);
			trackPane.setDividerSize(0);
			trackPane.setPreferredSize(drawDimensions);
			trackPane.setResizeWeight(0.0);
			trackPane.setContinuousLayout(true);
			trackPane.setVisible(false);
			varpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, trackPane, drawScroll);
			varpane.setUI(new BasicSplitPaneUI() {
				public BasicSplitPaneDivider createDefaultDivider() {
					return new BasicSplitPaneDivider(this) {
						private static final long serialVersionUID = 1L;
						public void setBorder(final Border b) {	}
						@Override
						public void paint(final Graphics g) {
							g.setColor(Color.lightGray);
							g.fillRect(0, 0, getSize().width, getSize().height);
							super.paint(g);
						}
					};
				}
			});
			varpane.setBorder(null);
			varpane.setDividerSize(0);
			varpane.setPreferredSize(drawDimensions);
			varpane.setResizeWeight(0.0);
			varpane.setContinuousLayout(true);
			bedScroll.setVisible(false);
			controlScroll.setVisible(false);
			chrompan = new JPanel() {
				protected void paintComponent(final Graphics g) {
					super.paintComponent(g);
					g.setColor(Draw.sidecolor);
					g.fillRect(0, 0, this.getWidth(), this.getHeight());
					g.setColor(Color.gray);
					g.fillRect(0, 0, 3, this.getHeight());
					g.setColor(Color.lightGray);
					g.fillRect(2, 0, 2, this.getHeight());
				}
			};
			chrompan.setLayout(new GridBagLayout());
			final GridBagConstraints gb = new GridBagConstraints();
			gb.anchor = GridBagConstraints.NORTHWEST;
			gb.insets = new Insets(2, 10, 2, 2);
			gb.gridx = 0;
			gb.gridy = 0;
			gb.gridwidth = 1;
			gb.fill = GridBagConstraints.HORIZONTAL;
			refDropdown.setBackground(Color.white);
			refDropdown.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
			refDropdown.setBorder(BorderFactory.createCompoundBorder(refDropdown.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
			geneDropdown.setBackground(Color.white);
			geneDropdown.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
			geneDropdown.setBorder(BorderFactory.createCompoundBorder(geneDropdown.getBorder(),	BorderFactory.createEmptyBorder(0, 0, 0, 0)));
			geneDropdown.addActionListener(MainPaneActionListeners::annoDropActionListener);
			final JLabel refLabel = new JLabel("Reference genome:");
			final JLabel geneLabel = new JLabel("Gene annotation:");
			MenuBar.chromLabel.setName("header");
			chrompan.add(MenuBar.chromLabel, gb);
			gb.gridy++;
			chrompan.add(new JSeparator(), gb);
			gb.gridy++;
			gb.insets = new Insets(0, 10, 0, 2);
			chrompan.add(refLabel, gb);
			gb.gridy++;
			chrompan.add(refDropdown, gb);
			gb.gridy++;
			chrompan.add(geneLabel, gb);
			gb.gridy++;
			chrompan.add(geneDropdown, gb);
			gb.gridy++;
			gb.insets = new Insets(20, 10, 0, 2);
			final JLabel memory = new JLabel("Memory usage:");
			memory.setName("header");
			chrompan.add(memory, gb);
			gb.insets = new Insets(0, 10, 0, 2);
			gb.gridy++;
			chrompan.add(memLabel, gb);
			gb.weightx = 1;
			gb.weighty = 1;
			gb.gridwidth = GridBagConstraints.REMAINDER;
			chrompan.add(new JLabel(), gb);
			chrompan.setMinimumSize(new Dimension(1, 1));
			chrompan.addComponentListener(this);
			upPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, chrompan, chromScroll);
			drawScroll.addComponentListener(this);
			upPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.white));
			upPanel.setDividerLocation(sidebarWidth - 2);
			chrompan.setBackground(Draw.sidecolor);
			final BasicSplitPaneUI chromPaneUI = (BasicSplitPaneUI) upPanel.getUI();
			chromPaneDivider = chromPaneUI.getDivider();
			chromPaneDivider.addMouseListener(this);
			upPanel.setDividerSize(3);
			upPanel.setUI(new BasicSplitPaneUI() {
				public BasicSplitPaneDivider createDefaultDivider() {
					return new BasicSplitPaneDivider(this) {
						private static final long serialVersionUID = 1L;
						public void setBorder(final Border b) {	}
						@Override
						public void paint(final Graphics g) {
							g.setColor(Color.lightGray);
							g.fillRect(0, 0, getSize().width, getSize().height);
							super.paint(g);
						}
					};
				}
			});

			splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, upPanel, varpane);

			splitPane.setUI(new BasicSplitPaneUI() {
				public BasicSplitPaneDivider createDefaultDivider() {
					return new BasicSplitPaneDivider(this) {
						private static final long serialVersionUID = 1L;
						public void setBorder(final Border b) {	}
						@Override
						public void paint(final Graphics g) {
							g.setColor(Color.lightGray);
							g.fillRect(0, 0, getSize().width, getSize().height);
							super.paint(g);
						}
					};
				}
			});

			BasicSplitPaneUI basicSplitPaneUI = (BasicSplitPaneUI) splitPane.getUI();
			splitPaneDivider = basicSplitPaneUI.getDivider();
			basicSplitPaneUI = (BasicSplitPaneUI) trackPane.getUI();
			trackPaneDivider = basicSplitPaneUI.getDivider();
			final BasicSplitPaneUI splitPaneUI = (BasicSplitPaneUI) varpane.getUI();
			varPaneDivider = splitPaneUI.getDivider();
			splitPane.setDividerSize(3);
			splitPane.setPreferredSize(drawDimensions);
			splitPane.setContinuousLayout(true);
			add(splitPane, c);
			
			
			drawScroll.getVerticalScrollBar().addMouseListener(this);
			drawScroll.getVerticalScrollBar().addMouseMotionListener(this);
			drawScroll.getVerticalScrollBar().addMouseWheelListener(MainPaneActionListeners::drawScrollMouseWheelListener);
			drawScroll.getVerticalScrollBar().addAdjustmentListener(MainPaneActionListeners::drawScrollAdjustmentListener);
		
			VarMaster.getInstance().reset();
			frame.addComponentListener(this);
			frame.addMouseListener(this);
			frame.setGlassPane(glassPane);
			glassPane.addMouseListener(this);
			glassPane.setOpaque(false);
			glassPane.setVisible(false);
			
		} catch (final Exception e) {	e.printStackTrace();	}
	}

	static void addSplit(final String chrom) {
		final SplitClass split = new SplitClass();
		split.getSplitDraw().resizeImages();
		drawCanvas.selectedSplit = split;
		split.chrom = chrom;
		if (Draw.getSplits().size() > 0 && drawCanvas.selectedSplit.chrom.equals(chrom)) split.setGenes(drawCanvas.selectedSplit.getGenes());
		else split.setGenes(fileReader.getExons(chrom));		

		if (samples > 0) {
			for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
				final Reads newReads = new Reads();
				newReads.sample = Getter.getInstance().getSampleList().get(i);
				Getter.getInstance().getSampleList().get(i).getreadHash().put(split, newReads);
			}
		}
		try {
			if (MenuBar.chromIndex.size() != 0) {
				if (chrom == null) split.chromEnd = 100;
				else split.chromEnd = MenuBar.chromIndex.get(refchrom + chrom)[1].intValue();				
			} else split.chromEnd = 100;			
		} catch (final Exception e) {
			System.out.println(chrom);
			e.printStackTrace();
		}
		split.start = 0;
		split.end = split.chromEnd;
		split.viewLength = split.end - split.start;
		Draw.getSplits().add(split);
		
		drawCanvas.resizeCanvas(drawCanvas.getWidth(), drawCanvas.getHeight());
		split.getExonImageBuffer().setFont(BaseVariables.defaultFont);
		split.getReadBuffer().setFont(BaseVariables.defaultFont);
		
		for (int i = 0; i < Draw.getSplits().size(); i++) {
			Draw.getSplits().get(i).setCytoImage(null);
			chromDraw.drawCyto(Draw.getSplits().get(i));
			chromDraw.updateExons = true;
			chromDraw.repaint();
		}
	}

	static class MyFilterVCF extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().endsWith(".vcf.gz")) return true;
			if (file.getName().endsWith(".vcf")) return true;			
			return false;			
		}
		public String getDescription() { return "*.vcf, *.vcf.gz"; }
	}

	static class MyFilterSES extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().contains(".ses")) return true;			
			return false;			
		}

		public String getDescription() { return "*.ses"; }
	}

	static class MyFilterBAM extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().endsWith(".bam")) return true;
			if (file.getName().endsWith(".cram")) return true;			
			return false;			
		}

		public String getDescription() {
			return "*.bam, *.cram";
		}
	}

	static class MyFilterLINK extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().endsWith(".link")) return true;
			else return false;			
		}

		public String getDescription() {
			return "*.link";
		}
	}

	static class MyFilterBED extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().toLowerCase().endsWith(".bed.gz") || file.getName().toLowerCase().endsWith(".bed")) return true;			
			if (file.getName().toLowerCase().endsWith(".bedgraph.gz")) return true;			
			if (file.getName().toLowerCase().endsWith(".gff.gz")) return true;			
			if (file.getName().toLowerCase().endsWith(".gff3.gz")) return true;			
			if (file.getName().toLowerCase().endsWith(".bigwig")) return true;			
			if (file.getName().toLowerCase().endsWith(".bw")) return true;			
			if (file.getName().toLowerCase().endsWith(".bigbed") || file.getName().toLowerCase().endsWith(".bb")) return true;
			if (file.getName().toLowerCase().endsWith(".tsv.gz")) return true;
			if (file.getName().toLowerCase().endsWith(".tsv.bgz")) return true;
			return false;			
		}

		public String getDescription() {
			return "*.bed, *.gff.gz, *.gff3.gz *.bedgraph.gz, *.bigWig, *.bigBed, *.tsv.gz";
		}
	}

	static class MyFilterTXT extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().endsWith(".txt")) return true;	
			return false;
		}

		public String getDescription() { return "Gene list *.txt"; }
	}

	static class MyFilterCRAM extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) {
			if (file.isDirectory()) return true;			
			if (file.getName().endsWith(".cram")) return true;			
			return false;			
		}
		public String getDescription() { return "*.cram"; }
	}

	static class MyFilterALL extends javax.swing.filechooser.FileFilter {
		public boolean accept(final File file) { return true; }
		public String getDescription() { return "All files"; }
	}

	public static void zoomout() {
		drawCanvas.setStartEnd(0.0, (double) Draw.getSplits().get(0).chromEnd);

		if (samples > 0) {
			if (Draw.getSplits().get(0).chromEnd > Settings.settings.get("readDrawDistance")) drawCanvas.clearReads();
			
			drawCanvas.removeSplits();
			chromDraw.vardraw = null;
			VariantHandler.table.hoverNode = null;
			VariantHandler.table.selectedNode = null;
			
			Draw.getSplits().get(0).getReadBuffer().setComposite(Draw.composite);
			Draw.getSplits().get(0).getReadBuffer().fillRect(0, 0, Draw.getSplits().get(0).getReadImage().getWidth(),	drawScroll.getViewport().getHeight());
			Draw.getSplits().get(0).getReadBuffer().setComposite(Draw.getSplits().get(0).getBackupr());			
		}

		if (bedCanvas.bedTrack.size() > 0) {			
			final bedFeatureFetcher fetch = bedCanvas.new bedFeatureFetcher();
			fetch.execute();
		}
		bedCanvas.repaint();
		chromDraw.updateExons = true;
		Setter.getInstance().setUpdateScreen();
		chromDraw.repaint();
	}

	public void getText(final Component[] comp) {

		for (int x = 0; x < comp.length; x++) {
			if (comp[x] instanceof JPanel) {
				getText(((JPanel) comp[x]).getComponents());

			} else if (comp[x] instanceof JTextField) {

				chooserTextField = ((JTextField) comp[x]);
				chooserTextField.addKeyListener(new KeyListener() {

					@Override
					public void keyTyped(final KeyEvent e) {

						chooserText = chooserTextField.getText().trim();
						if (chooserText.contains("?")) {
							chooserTextField.setText(chooserText.replace("?", "`"));
						}
					}

					@Override
					public void keyPressed(final KeyEvent e) {

					}

					@Override
					public void keyReleased(final KeyEvent e) {

					}
				});

			}
		}
	}

	static void clearData() {
		try {
			FileRead.checkSamples();
			VcfReader.asked = false;
			Draw.drawVariables.somatic = false;
			undoList.clear();
			undoPointer = 0;
			bedCanvas.bedOn = false;
			MenuBar.back.setEnabled(false);
			MenuBar.forward.setEnabled(false);
			MenuBar.average.setEnabled(false);
			Average.outVector.clear();
			zoomout();
			VarCalculations.nullifyVarNodes();
			samples = 0;
			varsamples = 0;
			readsamples = 0;
			VarMaster.getInstance().reset();
			chromDraw.vardraw = null;
		
			for (int i = 0; i < bedCanvas.bedTrack.size(); i++) {
				bedCanvas.bedTrack.get(i).getHead().putNext(null);
				bedCanvas.bedTrack.get(i).setCurrent(null);
				bedCanvas.bedTrack.get(i).setDrawNode(null);
				MethodLibrary.removeHeaderColumns(bedCanvas.bedTrack.get(i));
				FileRead.removeTable(bedCanvas.bedTrack.get(i));

				if (bedCanvas.bedTrack.get(i).getTable() != null && bedCanvas.bedTrack.get(i).getTable().bedarray != null) {
					bedCanvas.bedTrack.get(i).getTable().bedarray.clear();
					bedCanvas.bedTrack.get(i).getTable().hoverNode = null;
					bedCanvas.bedTrack.get(i).getTable().selectedNode = null;
				}
			}

			SplitClass split = Draw.getSplits().get(0);

			if (split.getGenes() != null) {
				for (int g = 0; g < split.getGenes().size(); g++) {
					split.getGenes().get(g).mutations = 0;
					split.getGenes().get(g).missense = 0;
					split.getGenes().get(g).nonsense = 0;
					split.getGenes().get(g).synonymous = 0;
					split.getGenes().get(g).intronic = 0;
					split.getGenes().get(g).utr = 0;
					split.getGenes().get(g).samples.clear();
					split.getGenes().get(g).varnodes.clear();
					split.getGenes().get(g).transcriptString = new StringBuffer();					
				}
			}
			split = null;
			Draw.getSplits().get(0).resetSplits();
			bedCanvas.bedTrack.clear();
			bedCanvas.trackDivider.clear();
			controlDraw.trackDivider.clear();
			if (VcfReader.advQualities != null) {
				VcfReader.advQualities.clear();
				if (VariantHandler.advQDraw != null) VariantHandler.advQDraw.clear();				
			}
			VariantHandler.removeMenuComponents();
			bedCanvas.track = null;
			bedCanvas.infoNode = null;
			bedCanvas.preInfoNode = null;
			bedScroll.setVisible(false);
			controlScroll.setVisible(false);
			varpane.setDividerSize(0);
			trackPane.setVisible(false);
			varpane.setResizeWeight(0.0);
			trackPane.setDividerSize(0);
			if (VariantHandler.table.vararray != null) VariantHandler.table.vararray.clear();			

			if (VariantHandler.tables.size() > 0) {
				for (int i = 0; i < VariantHandler.tables.size(); i++) {
					if (VariantHandler.tables.get(i).vararray != null) VariantHandler.tables.get(i).vararray.clear();					
					VariantHandler.tables.get(i).clear();
				}
			}
			VariantHandler.stattable.clear();
			VariantHandler.commonSlider.setMaximum(1);
			VariantHandler.commonSlider.setValue(1);
			VariantHandler.commonSlider.setUpperValue(1);
			VariantHandler.geneSlider.setMaximum(1);
			VariantHandler.geneSlider.setValue(1);

			VariantHandler.table.clear();
			VariantHandler.table.repaint();
			Draw.setScrollbar(0);
			Draw.drawVariables.setVisibleStart.accept(0);
			Draw.drawVariables.setVisibleSamples.accept(1);
			drawCanvas.sidebar = false;
			drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawScroll.getViewport().getHeight());
			Setter.getInstance().setUpdateScreen();

			if (VariantHandler.table.geneheader.size() > VariantHandler.table.geneheaderlength) {
				while (VariantHandler.table.geneheader.size() > VariantHandler.table.geneheaderlength) {
					VariantHandler.table.geneheader.remove(VariantHandler.table.geneheader.size() - 1);
				}
				VariantHandler.table.repaint();
			}
			for (int i = 0; i < Getter.getInstance().getControlList().size(); i++) {
				MethodLibrary.removeHeaderColumns(Getter.getInstance().getControlList().get(i));
			}
			Getter.getInstance().getControlList().clear();			
			Draw.drawVariables.projectName = "Untitled";
			Draw.drawVariables.projectFile = null;
			frame.setTitle("BasePlayer - Untitled Project");		
		} catch (final Exception e) { e.printStackTrace(); }
	}

	public static class runner extends SwingWorker<String, Object> {
		protected String doInBackground() {	return ""; }
	}	

	@Override
	public void stateChanged(final ChangeEvent event) {

	}

	public void componentResized(final ComponentEvent e) {
		
		if (e.getSource() == drawScroll) {
			
			if (sidebarWidth != upPanel.getDividerLocation() + 2) {
				if (samples * Draw.drawVariables.sampleHeight < drawScroll.getViewport().getHeight()) {
					drawDimensions.setSize(drawScroll.getViewport().getWidth(),	drawScroll.getViewport().getSize().height);
					drawCanvas.setPreferredSize(drawDimensions);
					drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawScroll.getViewport().getHeight());

					if (Draw.getSplits().size() > 0) {
						for (int i = 0; i < Draw.getSplits().size(); i++) {
							Draw.getSplits().get(i).updateReads = true;
						}
					}
				}
				
				Setter.getInstance().setUpdateScreen();
			}
			sidebarWidth = upPanel.getDividerLocation() + 4;
			chromDimensions.setSize(drawScroll.getViewport().getWidth() - upPanel.getDividerLocation(),	splitPane.getDividerLocation());
			chromDraw.setPreferredSize(chromDimensions);
			chromDraw.updateExons = true;
			chromDraw.repaint();
			if (samples == 0) {
				drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawScroll.getViewport().getHeight());
			} else if (Draw.drawVariables.getVisibleSamples.get() == samples) {

				drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawCanvas.getHeight());

				if (Draw.drawVariables.sampleHeight < drawScroll.getViewport().getHeight()) {
					Draw.drawVariables.setVisibleSamples.accept((int)((drawScroll.getViewport().getHeight() / (double) Draw.drawVariables.sampleHeight) + 0.5));
				}
			}

			return;
		} else if (e.getSource() == chrompan) {
			if (sidebarWidth != upPanel.getDividerLocation() + 2) {
				if (samples * Draw.drawVariables.sampleHeight < drawScroll.getViewport().getHeight()) {

					drawDimensions.setSize(drawScroll.getViewport().getWidth(),
							drawScroll.getViewport().getSize().height);
					drawCanvas.setPreferredSize(drawDimensions);

					drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawScroll.getViewport().getHeight());
					if (Draw.getSplits().size() > 0) {
						for (int i = 0; i < Draw.getSplits().size(); i++) {
							Draw.getSplits().get(i).updateReads = true;

						}
					}

				}
				bedCanvas.repaint();
				controlDraw.repaint();
				
				Setter.getInstance().setUpdateScreen();

			}

			sidebarWidth = upPanel.getDividerLocation() + 4;
			// drawCanvas.drawWidth = drawScroll.getViewport().getWidth()-sidebarWidth;
			chromDimensions.setSize(drawScroll.getViewport().getWidth() - upPanel.getDividerLocation(),
					splitPane.getDividerLocation());
			chromDraw.setPreferredSize(chromDimensions);
			chromDraw.updateExons = true;
			chromDraw.repaint();
			if (Draw.drawVariables.getVisibleSamples.get() == samples) {
				drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawCanvas.getHeight());
				if (Draw.drawVariables.sampleHeight < drawScroll.getViewport().getHeight()) {
					Draw.drawVariables.setVisibleSamples.accept((int) ((drawScroll.getViewport().getHeight() / (double) Draw.drawVariables.sampleHeight) + 0.5));
				}
			} else {
				drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawCanvas.getHeight());
			}

			return;

		}

		if (e.getComponent().getName() != null && e.getComponent().getName().contains("frame0")) {
			
			if (drawScroll.getViewport().getWidth() > 0) {
				drawDimensions.setSize(drawScroll.getViewport().getWidth(), drawScroll.getViewport().getSize().height);
				drawCanvas.setPreferredSize(drawDimensions);

				chromDimensions.setSize(drawScroll.getViewport().getWidth() - sidebarWidth - 1,
						splitPane.getDividerLocation());

				chromDraw.setPreferredSize(chromDimensions);

				drawCanvas.resizeCanvas(drawScroll.getViewport().getWidth(), drawScroll.getViewport().getHeight());

				if (Draw.getSplits().size() > 0) {
					for (int i = 0; i < Draw.getSplits().size(); i++) {
						Draw.getSplits().get(i).updateReads = true;
					}
				}
				Setter.getInstance().setUpdateScreen();
				return;
			}
		}
	}

	public static void cancel() {
		cancel = true;
		
		if (Draw.variantcalculator) {
			VarCalculations.cancelvarcount = true;
			Loader.ready("all");
		} else if (Loader.loadingtext.contains("Loading variants")) {
			Loader.ready("all");
			chromDraw.vardraw = null;
			Draw.drawVariables.variantsStart = 0;
			Draw.drawVariables.variantsEnd = 1;
			VarMaster.getInstance().reset();
			
			VarCalculations.cancelvarcount = true;
			FileRead.cancelfileread = true;
		} else if (Loader.loadingtext.contains("Processing variants")) {
			Loader.ready("Processing variants...");
			VarCalculations.cancelvarcount = true;
			VarMaster.getInstance().reset();
			chromDraw.vardraw = null;
			Draw.drawVariables.variantsStart = 0;
			Draw.drawVariables.variantsEnd = 1;
			
			VarCalculations.cancelvarcount = true;
			FileRead.cancelfileread = true;
			Loader.ready("all");
		} else if (Loader.loadingtext.contains("reads")) {
			FileRead.cancelreadread = true;
			Iterator<Map.Entry<SplitClass, Reads>> it;
			Map.Entry<SplitClass, Reads> pair;
			Reads reads;
			for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
				if (Getter.getInstance().getSampleList().get(i).getreadHash() != null) {
					it = Getter.getInstance().getSampleList().get(i).getreadHash().entrySet().iterator();
					while (it.hasNext()) {
						pair = (Map.Entry<SplitClass, Reads>) it.next();
						reads = pair.getValue();
						// reads.loading = false;
						reads.setLastRead(null);
					}
				}
			}
			FileRead.cancelfileread = true;
			drawCanvas.clearReads();
			Loader.ready("all");

			Draw.updateReads = true;
			Draw.updateCoverages = true;
			for (int i = 0; i < Draw.getSplits().size(); i++) {
				Draw.getSplits().get(i).updateReads = true;
			}

		} else if (Loader.loadingtext.contains("Processing variants")) {
			FileRead.cancelfileread = true;
			Loader.ready("all");
		} else if (Loader.loadingtext.contains("BED")) {
			FileRead.cancelfileread = true;
			Loader.ready("all");
		} else if (Loader.loadingtext.contains("samples")) {
			FileRead.cancelfileread = true;
			Loader.ready("all");
		} else if (Loader.loadingtext.contains("Updating")) {
			FileRead.cancelfileread = true;
			Loader.ready("Updating BasePlayer... (downloading BasePlayer.jar from http://baseplayer.fi/update/");
		} else if (Loader.loadingtext.contains("Annotating")) {
			/*
			 * FileRead.cancelfileread = true; BedCanvas.annoTrack.intersect = false;
			 * BedCanvas.annoTrack.used = false;
			 * 
			 * bedCanvas.removeBedhits(BedCanvas.annoTrack); BedCanvas.annoTrack =
			 * null;
			 * 
			 * Loader.ready(Loader.loadingtext);
			 * Loader.ready("Annotating variants");
			 * Loader.ready("Loading variants...");
			 */
			/*
			 * FileRead.cancelvarcount = true; drawCanvas.current = null;
			 * drawCanvas.currentDraw = null; chromDraw.vardraw = null; chromDraw.varnode =
			 * null; drawCanvas.variantsStart = 0; drawCanvas.variantsEnd = 1;
			 * FileRead.head.putNext(null);  FileRead.cancelvarcount
			 * = true; FileRead.cancelfileread = true;
			 */
			Loader.ready("all");
		} else {
			Loader.ready("all");
		}

		// Loader.readyQueue.clear();
		bedCanvas.annotator = false;
		FileRead.readFiles = false;
		cancel = false;
		Setter.getInstance().setUpdateScreen();
		chromDraw.repaint();
		FileRead.cancelfileread = false;
	}

	public static void cancel(final int nro) {
		cancel = true;
		Loader.ready("all");
		Draw.updateReads = false;
		for (int i = 0; i < Draw.getSplits().size(); i++) {
			Draw.getSplits().get(i).updateReads = false;

		}

		// Iterator<Map.Entry<SplitClass, Reads>> it;
		// Map.Entry<SplitClass, Reads> pair;
		// Reads reads;
		/*
		 * for(int i = 0; i<Getter.getInstance().getSampleList().size(); i++) {
		 * if(Getter.getInstance().getSampleList().get(i).getreadHash() != null) { it =
		 * Getter.getInstance().getSampleList().get(i).getreadHash().entrySet().iterator(); while
		 * (it.hasNext()) { pair = (Map.Entry<SplitClass, Reads>)it.next(); // reads =
		 * pair.getValue(); //reads.loading = false; }
		 * 
		 * } }
		 */
		for (int i = 0; i < bedCanvas.bedTrack.size(); i++) {
			bedCanvas.removeBeds(bedCanvas.bedTrack.get(i));
		}
		VarMaster.getInstance().reset();
		chromDraw.vardraw = null;
		
		VarCalculations.cancelvarcount = true;
		FileRead.cancelfileread = true;
		FileRead.cancelreadread = true;
		Draw.drawVariables.variantsStart = 0;
		Draw.drawVariables.variantsEnd = 1;
		cancel = false;

	}

	public static void writeToConfig(final String attribute) {
		configChanged = true;
		Boolean found = false;

		for (int i = 0; i < Launcher.config.size(); i++) {

			if (Launcher.config.get(i).contains(attribute.subSequence(0, attribute.indexOf("=")))) {
				found = true;

				Launcher.config.set(i, attribute);
				break;
			}
		}
		if (!found) {
			Launcher.config.add(attribute);
		}

	}

	@Override
	public void mouseClicked(final MouseEvent event) {
		
		if (cancelhover && Getter.getInstance().loading()) {
			cancel();
		}
		if (event.getComponent().getName() != null && genomehash.containsKey(event.getComponent().getName())) {

			changeRef(event.getComponent().getName());

		}
	}

	public static void addGenomeFile(final String genomeName) {
		genomehash.put(genomeName, new ArrayList<File>());
		final JMenu addMenu = new JMenu(genomeName);
		addMenu.addMouseListener(thisMainListener);
		addMenu.setName(genomeName);
		if (drawCanvas != null) {
			clicked = false;
			refModel.removeElementAt(refModel.getSize() - 1);
			refModel.addElement(genomeName);
			refModel.addElement("Add new reference...");
			clicked = true;
			/* genome.add(addMenu);
			genome.revalidate(); */
		}
	}

	public static void removeAnnotationFile(final String genomeName, final String annotationFile) {

		try {
			if (genomehash.get(genomeName) == null) {
				return;
			}
			/* for (int i = 1; i < genome.getItemCount(); i++) {
				if (genome.getItem(i).getName().equals(genomeName)) {
					final JMenu addMenu = (JMenu) genome.getItem(i);
					for (int j = 0; j < addMenu.getItemCount(); j++) {
						if (addMenu.getItem(j) == null || addMenu.getItem(j).getText() == null) {
							continue;
						}

						if (annotationFile.contains(addMenu.getItem(j).getText())) {
							addMenu.remove(j);
							addMenu.revalidate();
							break;
						}
					}
					break;
				}
			} */
			for (int i = 0; i < genomehash.get(genomeName).size(); i++) {

				if (genomehash.get(genomeName).get(i).getName().contains(annotationFile.replace(".gff3.gz", ""))) {
					genomehash.get(genomeName).remove(i);
					break;
				}
			}

			defaultAnnotation = "";
			MenuBar.setChromDrop(genomeName);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static void addAnnotationFile(final String genomeName, final File annotationFile) {
		
		if (genomehash.get(genomeName) == null) {
			genomehash.put(genomeName, new ArrayList<File>());
		}

		genomehash.get(genomeName).add(annotationFile);
		final JMenuItem additem = new JMenuItem(
				annotationFile.getName().substring(0, annotationFile.getName().indexOf(".bed.gz")));
		additem.setName(annotationFile.getName().substring(0, annotationFile.getName().indexOf(".bed.gz")));
		additem.addMouseListener(thisMainListener);

		/* for (int i = 1; i < genome.getItemCount(); i++) {

			if (genome.getItem(i).getName().equals(genomeName)) {
				final JMenu addMenu = (JMenu) genome.getItem(i);
				addMenu.setFont(menuFont);
				if (first) {
					final JMenuItem addAnnotation = new JMenuItem("Add new annotation file...");
					addAnnotation.setFont(menuFont);
					addAnnotation.addMouseListener(thisMainListener);
					addAnnotation.setName("add_annotation");
					addMenu.add(addAnnotation);
					final JLabel addLabel = new JLabel("  Select annotation: ");
					addLabel.setFont(menuFont);
					labels.add(addLabel);
					addMenu.add(addLabel);
					addMenu.add(new JSeparator());
				}
				additem.setFont(menuFont);
				addMenu.add(additem);
				addMenu.revalidate();
				//genome.revalidate();
				break;
			}
		} */
		defaultAnnotation = annotationFile.getName();
		MenuBar.setChromDrop(genomeName);
	}

	

	
	public static void showError(final String error, final String dialogtype) {
		if (VariantHandler.frame != null) {
			VariantHandler.frame.setAlwaysOnTop(false);
		}
		if (dialogtype.equals("Error")) {

			JOptionPane.showMessageDialog(drawScroll, error, dialogtype, JOptionPane.ERROR_MESSAGE);
		}

		else {
			JOptionPane.showMessageDialog(drawScroll, error, dialogtype, JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public static void showError(final String error, final String dialogtype, final Component component) {
		if (VariantHandler.frame != null) {
			VariantHandler.frame.setAlwaysOnTop(false);
		}
		if (dialogtype.equals("Error")) {

			JOptionPane.showMessageDialog(component, error, dialogtype, JOptionPane.ERROR_MESSAGE);
		}

		else {
			JOptionPane.showMessageDialog(component, error, dialogtype, JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public static void gotoURL(final String url) {

		if (!java.awt.Desktop.isDesktopSupported()) {

			System.err.println("Desktop is not supported");
			System.exit(1);
		}

		final java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

		if (!desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {

			System.err.println("Desktop doesn't support the browse action");
			System.exit(1);
		}

		try {

			final java.net.URI uri = new java.net.URI(url);
			desktop.browse(uri);
		} catch (final Exception ex) {
			showError("Can not open URL.", "Error");
			// System.err.println( ex.getMessage() );
			ex.printStackTrace();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent event) {

		if (event.getComponent().getName() != null) {

			if (event.getComponent() instanceof JMenu) {
				hoverGenome = event.getComponent().getName();
				hoverAnnotation = "";

			} else if (event.getComponent() instanceof JMenuItem) {

				hoverAnnotation = event.getComponent().getName();
			}
		}
	}

	@Override
	public void mouseExited(final MouseEvent event) {
		if (event.getSource() == drawCanvas) {
			drawCanvas.sidebar = false;
			Setter.getInstance().setUpdateScreen();
		}
	}

	@Override
	public void mousePressed(final MouseEvent event) {
		mouseX = event.getX();

		if (event.getSource() == refDropdown) {
			switch (event.getModifiersEx()) {
				case InputEvent.BUTTON1_DOWN_MASK: {
					if (genomehash.size() == 0) {
						if (AddGenome.frame == null) {
							AddGenome.createAndShowGUI();
						}
						AddGenome.frame.setTitle("Add new genome");
						AddGenome.annotation = false;
						AddGenome.remove.setEnabled(false);
						AddGenome.download.setEnabled(false);
						AddGenome.frame.setVisible(true);
						AddGenome.frame.setLocation(
								frame.getLocationOnScreen().x + frame.getWidth() / 2 - AddGenome.frame.getWidth() / 2,
								frame.getLocationOnScreen().y + frame.getHeight() / 6);

						AddGenome.frame.setState(JFrame.NORMAL);
					}
					rightclick = false;
					break;
				}
				case InputEvent.BUTTON3_DOWN_MASK: {
					rightclick = true;
					break;
				}
			}
		} else if (event.getSource() == geneDropdown) {
			switch (event.getModifiersEx()) {
				case InputEvent.BUTTON1_DOWN_MASK: {
					if (genomehash.size() == 0) {
						if (AddGenome.frame == null) {
							AddGenome.createAndShowGUI();
						}
						AddGenome.frame.setTitle("Add new genome");
						AddGenome.annotation = false;
						AddGenome.remove.setEnabled(false);
						AddGenome.download.setEnabled(false);
						AddGenome.frame.setVisible(true);
						AddGenome.frame.setLocation(
								frame.getLocationOnScreen().x + frame.getWidth() / 2 - AddGenome.frame.getWidth() / 2,
								frame.getLocationOnScreen().y + frame.getHeight() / 6);

						AddGenome.frame.setState(JFrame.NORMAL);
					}
					rightclick = false;
					break;
				}
				case InputEvent.BUTTON3_DOWN_MASK: {
					rightclick = true;
					break;
				}
			}
		} 

		else if (event.getSource() == splitPaneDivider) {
			vardivider = bedCanvas.nodeImage.getHeight() / (double) varPaneDivider.getY();
		} else if (event.getSource() == varPaneDivider) {
			vardivider = bedCanvas.nodeImage.getHeight() / (double) varPaneDivider.getY();
		} else if (Loader.loadingtext.equals("note")) {
			Loader.loadingtext = "";
			Loader.ready("note");
		}	else if (event.getSource() == drawScroll.getVerticalScrollBar()) {
			if (glassPane.getCursor().getType() != Cursor.WAIT_CURSOR) {
				glassPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			}
			Setter.getInstance().setUpdateScreen();
			if (drawScroll.getVerticalScrollBar().getValue() > (Draw.drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight	+ Draw.drawVariables.sampleHeight / 2.0))
				Draw.drawVariables.setVisibleStart.accept(Draw.drawVariables.getVisibleStart.get() + 1);				
			
			Draw.setScrollbar((int) (Draw.drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight));
			
			//Setter.getInstance().setUpdateScreen();
			//Draw.setGlasspane(true);
			
		}
	}

	public static void changeRef(final String dir) {
		writeToConfig("DefaultGenome=" + dir);
		defaultGenome = dir;
		MenuBar.setChromDrop(refDropdown.getSelectedItem().toString());
		getBands();
		try {
			if (genomehash.get(dir).size() > 0) {

				ChromDraw.exonReader = new TabixReader(genomehash.get(dir).get(0).getCanonicalPath());
				String s;
				String[] exonSplit;
				MenuBar.searchTable.clear();
				while ((s = ChromDraw.exonReader.readLine()) != null) {
					exonSplit = s.split("\t");
					if (!MenuBar.searchTable.containsKey(exonSplit[3].toUpperCase())) {

						final String[] adder = { exonSplit[0], exonSplit[1], exonSplit[2] };
						MenuBar.searchTable.put(exonSplit[3].toUpperCase(), adder);
						if (exonSplit[6].contains(":")) {
							geneIDMap.put(exonSplit[6].split(":")[1].toUpperCase(), exonSplit[3].toUpperCase());
						} else {
							geneIDMap.put(exonSplit[6].toUpperCase(), exonSplit[3].toUpperCase());
						}
					} else {

						if (Integer.parseInt(MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1]) > Integer
								.parseInt(exonSplit[1])) {
							MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1] = exonSplit[1];
						}
						if (Integer.parseInt(MenuBar.searchTable.get(exonSplit[3].toUpperCase())[2]) < Integer
								.parseInt(exonSplit[2])) {
							MenuBar.searchTable.get(exonSplit[3].toUpperCase())[2] = exonSplit[2];
						}
					}
				}

			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		changeAnnotation(0);
	}

	public static void changeAnnotation(final int annotation) {
		try {
			if (genomehash.get(defaultGenome).size() < 1) {
				return;
			}
			MainPane.annotation = annotation;
			defaultAnnotation = genomehash.get(defaultGenome).get(annotation).getName();
			annotationfile = defaultAnnotation;
			writeToConfig("DefaultGenome=" + defaultGenome);
			writeToConfig("DefaultGenes=" + annotationfile);
			ChromDraw.exonReader = new TabixReader(genomehash.get(defaultGenome).get(annotation).getCanonicalPath());
			String s;
			String[] exonSplit;
			MenuBar.searchTable.clear();
			geneIDMap.clear();
			while ((s = ChromDraw.exonReader.readLine()) != null) {

				exonSplit = s.split("\t");
				if (!MenuBar.searchTable.containsKey(exonSplit[3].toUpperCase())) {

					final String[] adder = { exonSplit[0], exonSplit[1], exonSplit[2] };
					MenuBar.searchTable.put(exonSplit[3].toUpperCase(), adder);
					if (exonSplit[6].contains(":")) {
						geneIDMap.put(exonSplit[6].split(":")[1].toUpperCase(), exonSplit[3].toUpperCase());
					} else {
						geneIDMap.put(exonSplit[6].toUpperCase(), exonSplit[3].toUpperCase());
					}
				} else {

					if (Integer.parseInt(MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1]) > Integer
							.parseInt(exonSplit[1])) {
						MenuBar.searchTable.get(exonSplit[3].toUpperCase())[1] = exonSplit[1];
					}
					if (Integer.parseInt(MenuBar.searchTable.get(exonSplit[3].toUpperCase())[2]) < Integer
							.parseInt(exonSplit[2])) {
						MenuBar.searchTable.get(exonSplit[3].toUpperCase())[2] = exonSplit[2];
					}
				}
			}
			ChromDraw.exonReader.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
		MenuBar.chromosomeDropdown.setSelectedIndex(0);
	}

	@Override
	public void mouseReleased(final MouseEvent event) {
		
		if (resizing) {
			
			drawCanvas.resizeCanvas(width, height);
			resizing = false;
		}
		if (event.getSource() == splitPaneDivider) {
			bedCanvas.resize = false;
			bedCanvas.repaint();
		}
		if (event.getSource() == varPaneDivider) {

			bedCanvas.resize = false;
			bedCanvas.repaint();
			
			Draw.updateReads = true;
			Draw.updateCoverages = true;
			Setter.getInstance().setUpdateScreen();
		}

		if (event.getSource() == drawScroll.getVerticalScrollBar()) {
				
			if (drawScroll.getVerticalScrollBar().getValue() > (Draw.drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight	+ Draw.drawVariables.sampleHeight / 2.0))
				Draw.drawVariables.setVisibleStart.accept(Draw.drawVariables.getVisibleStart.get() + 1);

			Draw.setScrollbar((int) (Draw.drawVariables.getVisibleStart.get() * Draw.drawVariables.sampleHeight));
		}

		if (!Getter.getInstance().loading()) {
		
			Draw.setGlasspane(false);
		}
		drawCanvas.scrolldrag = false;

		if (bedCanvas.bedTrack.size() > 0) {
			for (int i = 0; i < bedCanvas.bedTrack.size(); i++) {

				bedCanvas.getMoreBeds(bedCanvas.bedTrack.get(i));
			}
		}
	}

	@Override
	public void componentHidden(final ComponentEvent arg0) {

	}

	@Override
	public void componentMoved(final ComponentEvent arg0) {

		Logo.frame.setLocation(frame.getLocation().x + (int) width / 2 - 300,
				frame.getLocation().y + (int) height / 2 - 300);
	}

	@Override
	public void componentShown(final ComponentEvent arg0) {

	}


	public static void setFonts() {
		if (Settings.bold.isSelected()) {
			menuFont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
			menuFontBold = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize + 1);

		} else {
			menuFont = new Font("SansSerif", Font.PLAIN, BaseVariables.defaultFontSize);
			menuFontBold = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
		}

		BaseVariables.defaultFont = menuFont.deriveFont((float) BaseVariables.defaultFontSize - 1);

		for (int i = 0; i < menubar.getComponentCount(); i++) {
			menubar.getComponent(i).setFont(menuFont);
		}

		BaseVariables.loadingFont = menuFont.deriveFont((float) (BaseVariables.defaultFontSize * 1.5));//
		buttonHeight = (int) (BaseVariables.defaultFontSize * 1.5);
		buttonWidth = BaseVariables.defaultFontSize * 6;
		// searchField.setMargin(new Insets(0,BaseVariables.defaultFontSize+8, 0, 0));
		MenuBar.searchField.setBorder(null);
		MenuBar.searchField.setBorder(BorderFactory.createCompoundBorder(MenuBar.searchField.getBorder(),
				BorderFactory.createEmptyBorder(0, BaseVariables.defaultFontSize + 12, 0, 0)));
		buttonDimension = new Dimension(buttonWidth, buttonHeight);

		BaseVariables.seqFont = BaseVariables.seqFont.deriveFont((float) (BaseVariables.defaultFontSize + 2));
		bedCanvas.buf.setFont(BaseVariables.defaultFont);
		bedCanvas.nodebuf.setFont(BaseVariables.defaultFont);
		bedCanvas.fm = bedCanvas.nodebuf.getFontMetrics();
		for (int i = 0; i < bedCanvas.bedTrack.size(); i++) {
			for (int c = 0; c < bedCanvas.bedTrack.get(i).getPopup().getComponentCount(); c++) {
				bedCanvas.bedTrack.get(i).getPopup().getComponent(c).setFont(menuFont);

			}
			if (bedCanvas.bedTrack.get(i).getSelector() != null) {
				bedCanvas.bedTrack.get(i).getSelector().setFonts(menuFont);
			}
			bedCanvas.bedTrack.get(i).getLimitField().setPreferredSize(new Dimension(
					bedCanvas.buf.getFontMetrics().stringWidth("__Value limit__"), BaseVariables.defaultFontSize + 6));
			bedCanvas.bedTrack.get(i).getLimitField().setMinimumSize(new Dimension(
					bedCanvas.buf.getFontMetrics().stringWidth("__Value limit__"), BaseVariables.defaultFontSize + 6));

		}
		for (int i = 0; i < Getter.getInstance().getControlList().size(); i++) {
			for (int c = 0; c < Getter.getInstance().getControlList().get(i).getPopupMenu().getComponentCount(); c++) {
				Getter.getInstance().getControlList().get(i).getPopupMenu().getComponent(c).setFont(menuFont);

			}
		}
		Average.setFonts(menuFont);
		SampleDialog.setFonts(menuFont);
		
		MenuBar.filemenu.setMinimumSize(MenuBar.filemenu.getPreferredSize());
		MenuBar.toolmenu.setMinimumSize(MenuBar.toolmenu.getPreferredSize());
		MenuBar.help.setMinimumSize(MenuBar.help.getPreferredSize());
		MenuBar.manage.setPreferredSize(
				new Dimension(bedCanvas.buf.getFontMetrics().stringWidth("Variant Managerrrrrrrr") + 4, buttonHeight));

				MenuBar.manage.setMinimumSize(
				new Dimension(bedCanvas.buf.getFontMetrics().stringWidth("Variant Managerrrrrrrrr") + 4, buttonHeight));
				MenuBar.chromlabel.setPreferredSize(
				new Dimension(bedCanvas.buf.getFontMetrics().stringWidth("..Chrom..") + 4, buttonHeight));
				MenuBar.chromlabel.setMinimumSize(
				new Dimension(bedCanvas.buf.getFontMetrics().stringWidth("..Chrom..") + 4, buttonHeight));
		for (int i = 0; i < Main.frame.getContentPane().getComponentCount(); i++) {
			Main.frame.getContentPane().getComponent(i).setFont(menuFont);
		}
		for (int i = 0; i < MenuBar.filemenu.getItemCount(); i++) {
			if (MenuBar.filemenu.getItem(i) != null) {
				MenuBar.filemenu.getItem(i).setFont(menuFont);
			}
		}
		for (int i = 0; i < MenuBar.toolmenu.getItemCount(); i++) {
			if (MenuBar.toolmenu.getItem(i) != null) {
				MenuBar.toolmenu.getItem(i).setFont(menuFont);

			}
		}
		MenuBar.area.setFont(menuFont);
		for (int i = 0; i < MenuBar.help.getItemCount(); i++) {
			if (MenuBar.help.getItem(i) != null) {
				MenuBar.help.getItem(i).setFont(menuFont);
			}
		}
		/* for (int i = 0; i < MenuBar.genome.getItemCount(); i++) {
			MenuBar.genome.getItem(i).setFont(menuFont);
			if (MenuBar.genome.getItem(i) instanceof JMenu) {
				final JMenu menu = (JMenu) MenuBar.genome.getItem(i);
				for (int j = 0; j < menu.getItemCount(); j++) {
					if (menu.getItem(j) != null) {
						menu.getItem(j).setFont(menuFont);
					}
				}
			}
		} */
		for (int i = 0; i < labels.size(); i++) {
			labels.get(i).setFont(menuFont);
		}

		VariantCaller.setFonts(menuFont);
		PeakCaller.setFonts(menuFont);
		for (int i = 0; i < Draw.getSplits().size(); i++) {
			Draw.getSplits().get(i).getExonImageBuffer().setFont(BaseVariables.defaultFont);
			Draw.getSplits().get(i).getReadBuffer().setFont(BaseVariables.defaultFont);
			//Draw.getSplits().get(i).getSelectbuf().setFont(BaseVariables.defaultFont);
		}

		for (int i = 0; i < chrompan.getComponentCount(); i++) {
			if (chrompan.getComponent(i).getName() != null) {
				chrompan.getComponent(i).setFont(menuFontBold);
			} else {
				chrompan.getComponent(i).setFont(menuFont);
			}
		}
		if (AddGenome.tree != null) {
			AddGenome.setFonts(menuFont);
		}
		Settings.setFonts(menuFont);
		chromDraw.selectImageBuffer.setFont(BaseVariables.defaultFont);
		chromDraw.chromImageBuffer.setFont(BaseVariables.defaultFont);
		MenuBar.manage.setToolTipText("No variants on screen");
		MenuBar.manage.setMargin(new Insets(0, 4, 0, 4));
		MenuBar.zoomout.setPreferredSize(
				new Dimension(bedCanvas.buf.getFontMetrics().stringWidth("Zoom outtttttt") + 4, buttonHeight));
				MenuBar.zoomout.setMinimumSize(
				new Dimension(bedCanvas.buf.getFontMetrics().stringWidth("Zoom outtttttt") + 4, buttonHeight));
				MenuBar.zoomout.setMargin(new Insets(0, 4, 0, 4));
		fieldDimension = new Dimension(
			MenuBar.widthLabel.getFontMetrics(MenuBar.widthLabel.getFont()).stringWidth("chrX:000,000,000-000,000,000bp") + 4,
				buttonHeight);
				MenuBar.positionField.setPreferredSize(fieldDimension);
				MenuBar.positionField.setMinimumSize(fieldDimension);
		controlDraw.buf.setFont(BaseVariables.defaultFont);
		controlDraw.nodebuf.setFont(BaseVariables.defaultFont);
		controlDraw.fm = controlDraw.buf.getFontMetrics();
		controlDraw.repaint();
		letterlength = MenuBar.chromosomeDropdown.getFontMetrics(MenuBar.chromosomeDropdown.getFont()).stringWidth("E");
		MenuBar.chromosomeDropdown.setPopupWidth(textlength * letterlength + 25);
		MenuBar.chromosomeDropdown.revalidate();
		MenuBar.chromosomeDropdown.repaint();
		MenuBar.chromosomeDropdown.setPreferredSize(new Dimension(BaseVariables.defaultFontSize * 5, buttonHeight));
		geneDropdown.setPopupWidth(annolength * letterlength);
		refDropdown.setPopupWidth(reflength * letterlength);
		// searchField.setMargin(new Insets(0,buttonHeight+4, 0, 0));
		MenuBar.searchField.setPreferredSize(fieldDimension);
		MenuBar.searchField.setMinimumSize(fieldDimension);
		MenuBar.widthLabel.setPreferredSize(new Dimension(MenuBar.widthLabel.getFontMetrics(MenuBar.widthLabel.getFont()).stringWidth("000,000,000bp (Right click to cancel zoom)  NNNNNNNNNNNNNNNNNNNNNNNN") + 10,	buttonHeight));
		MenuBar.widthLabel.setMinimumSize(new Dimension(MenuBar.widthLabel.getFontMetrics(MenuBar.widthLabel.getFont()).stringWidth("000,000,000bp") + 10, buttonHeight));
		MenuBar.back.setFont(menuFont);
		MenuBar.back.setPreferredSize(new Dimension(MenuBar.back.getFontMetrics(MenuBar.back.getFont()).stringWidth(".<<.") + 10, buttonDimension.height));
		MenuBar.forward.setFont(menuFont);
		MenuBar.forward.setPreferredSize(new Dimension(MenuBar.forward.getFontMetrics(MenuBar.forward.getFont()).stringWidth(".>>.") + 10, buttonDimension.height));
		chromDraw.bounds = chromDraw.chromImageBuffer.getFontMetrics().getStringBounds("K", chromDraw.chromImageBuffer).getWidth();
		chromDraw.cytoHeight = BaseVariables.defaultFontSize + 10;
		chromDraw.exonDrawY = BaseVariables.defaultFontSize * 2 + 10;
		MainPane.drawCanvas.setFont();
		if (VariantHandler.filters != null) {
			VariantHandler.setFonts(menuFont);
		}
		chromDraw.updateExons = true;
		chromDraw.repaint();

		for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
			if (Getter.getInstance().getSampleList().get(i).getreadHash() != null) {

				for (int j = 0; j < Draw.getSplits().size(); j++) {
					if (Getter.getInstance().getSampleList().get(i).getreadHash().get(Draw.getSplits().get(j)) == null) continue;

					final double temp = (BaseVariables.readHeight + 2) / (double) Getter.getInstance().getSampleList().get(i).getreadHash().get(Draw.getSplits().get(j)).readwheel;
					BaseVariables.readfont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
					BaseVariables.readHeight = BaseVariables.defaultFontSize;
					Getter.getInstance().getSampleList().get(i).getreadHash().get(Draw.getSplits().get(j)).readwheel = (int) ((BaseVariables.readHeight + 2) / (double) temp);
					Draw.updateReads = true;
					Setter.getInstance().setUpdateScreen();
				}
			}
		}

		splitPane.setDividerLocation(chrompan.getComponentCount() * (BaseVariables.defaultFontSize + 6));
		splitPane.revalidate();
	}

	public static void setAnnotationDrop(final String ref) {
		if (drawCanvas != null) {
			geneModel.removeAllElements();
			int maxlength = 0;
			final int letterLength = MenuBar.chromosomeDropdown.getFontMetrics(MenuBar.chromosomeDropdown.getFont()).stringWidth("E");
			if (genomehash.get(ref) != null) {
				for (int i = 0; i < genomehash.get(ref).size(); i++) {
					if (genomehash.get(ref).get(i).getName().length() > maxlength) {
						maxlength = genomehash.get(ref).get(i).getName().length();
					}
					geneModel.addElement(genomehash.get(ref).get(i).getName());
				}
			}
			final String addAnno = "Add new annotation...";
			if (addAnno.length() > maxlength) {
				maxlength = addAnno.length();
			}
			geneModel.addElement("Add new annotation...");
			geneDropdown.setPopupWidth(maxlength * letterLength);
		}
	}

	public static void getBands() {

		try {

			final File bandfile = new File(genomeDir.getCanonicalPath() + "/" + selectedGenome + "/bands.txt");

			if (bandfile.exists()) {
				ChromDraw.bandVector.clear();
				final BufferedReader in = new BufferedReader(new FileReader(bandfile));
				String line;
				while ((line = in.readLine()) != null) {
					ChromDraw.bandVector.add(line.split("\\s"));
				}
				in.close();
			} else {
				ChromDraw.bandVector.clear();
			}

		} catch (final Exception e) {

			e.printStackTrace();
		}
	}

	public void mouseDragged(final MouseEvent event) {

		if (event.getSource() == drawScroll.getVerticalScrollBar()) {
			glassPane.setVisible(true);
			drawCanvas.updateScreen = true;
			Draw.scrollValue = drawScroll.getVerticalScrollBar().getValue();
			/*  * Draw.drawVariables.getVisibleStart.get() =
			 * (short)(drawScroll.getVerticalScrollBar().getValue()/drawCanvas.
			 * drawVariables.sampleHeight); // if(Draw.drawVariables.getVisibleStart.get() +
			 * Draw.drawVariables.visiblesamples < samples-1) {
			 * 
			 * drawCanvas.scrolldrag = true;
			 * 
			 * if(Draw.getSplits().size() > 1) { for(int i = 0; i<Draw.getSplits().size();
			 * i++) { Draw.getSplits().get(i).updateReads = true; } } else {
			 * Draw.updateReads = true;  } 
			 * 
			 * // } */
			
			//
			//Setter.getInstance().setUpdateScreen();
		}

	}

	@Override
	public void mouseMoved(final MouseEvent event) {
	}

	@Override
	public void keyTyped(final KeyEvent e) {
	}

	public static boolean zoomtopos(final String chrom, final String pos, String sample) {

		if (sample.length() > 10) {
			sample = sample.toUpperCase().substring(0, 10);
		} else {
			sample = sample.toUpperCase();
		}
		boolean found = false;
		for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {

			if (Getter.getInstance().getSampleList().get(i).getName().toUpperCase().contains(sample)) {
				Draw.drawVariables.setVisibleStart.accept(i);
				Draw.drawVariables.setVisibleSamples.accept(1);

				drawCanvas.resizeCanvas(drawCanvas.getWidth(),
						(int) (samples * Draw.drawVariables.sampleHeight));
				Draw.setScrollbar((int) (i * Draw.drawVariables.sampleHeight));
				found = true;
				break;
			}
		}
		if (!found) {
			return false;
		}
		nothread = true;
		noreadthread = true;
		FileRead.search = true;
		drawCanvas.gotoPos(chrom, Integer.parseInt(pos) - 100, Integer.parseInt(pos) + 100);
		return true;
	}

	@Override

	public void keyPressed(final KeyEvent e) {
		int keyCode = e.getKeyCode();
		
		if (!shift && (e.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) != 0) {

			shift = true;
		} else if ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
			drawCanvas.ctrlpressed = 100;

			if (keyCode == KeyEvent.VK_S) {
				//TODO: save project
			}
			if (keyCode == KeyEvent.VK_W) {
			
				Draw.updateReads = true;
				Draw.fetchReads = true;
				Setter.getInstance().setUpdateScreen();
				
			}
			if (keyCode == KeyEvent.VK_PLUS || keyCode == 107) {

			}
			if (keyCode == KeyEvent.VK_M || keyCode == KeyEvent.VK_MINUS || keyCode == 109) {

			}
			
		} else if (keyCode == KeyEvent.VK_DELETE) {
			if (Sample.selectedSample != null) Sample.selectedSample.removeSample();			
		} else if (keyCode == KeyEvent.VK_F9) {
			
		} else if (keyCode == KeyEvent.VK_F12) {

		}
	}
	
	static boolean isPosition(final String text) {
		if (text.equals("tati") || text.equals("third")) { MethodLibrary.getSplitTargets(); return false; }
		if (text.equals("genes")) { MethodLibrary.listVisibleGenes(); return false; }
		return true;
	}
	

	@Override
	public void keyReleased(final KeyEvent e) {
		drawCanvas.ctrlpressed = 5;
		shift = false;

	}
	private MouseMotionListener glassPaneMotionListener = new MouseMotionListener() {
		@Override
		public void mouseMoved(final MouseEvent event) {
			if (Getter.getInstance().loading()	&& event.getX() > drawScroll.getWidth() / 2 - canceltextwidth / 2	- BaseVariables.defaultFontSize / 2
					&& event.getX() < drawScroll.getWidth() / 2 + canceltextwidth / 2
							+ BaseVariables.defaultFontSize / 2
					&& event.getY() > frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3
							- BaseVariables.defaultFontSize / 4
					&& event.getY() < frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 4
							+ BaseVariables.defaultFontSize / 2) {
				if (!cancelhover) {
					cancelhover = true;
					glassPane.requestFocus();
				}
			} else {
				if (cancelhover) {
					cancelhover = false;
					glassPane.requestFocus(false);
				}
			}
		}
		@Override
		public void mouseDragged(final MouseEvent arg0) {	}
	};

	private void createBaseMaps() {
		bases = new Hashtable<String, String>();
		bases.put("A", "A");
		bases.put("C", "C");
		bases.put("G", "G");
		bases.put("T", "T");
		bases.put("N", "N");
		bases.put("delA", "delA");
		bases.put("delC", "delC");
		bases.put("delG", "delG");
		bases.put("delT", "delT");
		bases.put("insA", "insA");
		bases.put("insC", "insC");
		bases.put("insG", "insG");
		bases.put("insT", "insT");
		baseMap.put((byte) 'A', 1);
		baseMap.put((byte) 'C', 2);
		baseMap.put((byte) 'G', 3);
		baseMap.put((byte) 'T', 4);
		baseMap.put((byte) 'N', 5);
		baseMap.put((byte) 'I', 6);
		baseMap.put((byte) 'D', 7);
		mutTypes.put("TA", 0);
		mutTypes.put("AT", 0);
		mutTypes.put("TC", 1);
		mutTypes.put("AG", 1);
		mutTypes.put("TG", 2);
		mutTypes.put("AC", 2);
		mutTypes.put("CA", 3);
		mutTypes.put("GT", 3);
		mutTypes.put("CG", 4);
		mutTypes.put("GC", 4);
		mutTypes.put("CT", 5);
		mutTypes.put("GA", 5);

		getBase.put((byte) 'A', "A");
		getBase.put((byte) 'C', "C");
		getBase.put((byte) 'G', "G");
		getBase.put((byte) 'T', "T");
		getBase.put((byte) 'N', "N");
		getBase.put((byte) 'a', "A");
		getBase.put((byte) 'c', "C");
		getBase.put((byte) 'g', "G");
		getBase.put((byte) 't', "T");
		getBase.put((byte) 'n', "N");
	}


}





