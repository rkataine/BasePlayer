package base.BasePlayer.GUI;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JFrame;
import javax.swing.JPanel;
import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.Getter;
import base.BasePlayer.Main;

public class GlassPane extends JPanel {
    JFrame frame = Main.frame;
    int loadTextWidth = MainPane.loadTextWidth;
    int canceltextwidth = MainPane.canceltextwidth;
    boolean loaddraw = MainPane.loaddraw;
    GradientPaint gradient = MainPane.gradient;
    public void paintComponent(final Graphics g) {
        paintComponent((Graphics2D) g);

    }

    public void paintComponent(final Graphics2D g) {
        g.setRenderingHints(Draw.rh);

        if (Getter.getInstance().loading()) {

            if (Loader.loadingtext != null && !loaddraw && Loader.loadingtext.length() > 0 && !Loader.loadingtext.equals("note")) {
                loaddraw = true;
                g.setFont(BaseVariables.loadingFont);

                if (loadTextWidth != (int) (g.getFontMetrics().getStringBounds(Loader.loadingtext, g)
                        .getWidth())) {
                    loadTextWidth = (int) (g.getFontMetrics().getStringBounds(Loader.loadingtext, g)
                            .getWidth());
                    gradient = new GradientPaint(
                            MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2 - MainPane.drawScroll.getWidth() / 6, 0, Color.red,
                            MainPane.drawScroll.getWidth() / 2 + loadTextWidth, 0, Color.green, true);
                    canceltextwidth = (int) (g.getFontMetrics().getStringBounds("Cancel", g).getWidth());
                }

                g.setColor(BaseConstants.intronColor);
                g.fillRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2 - 9,
                        frame.getHeight() * 1 / 3 - BaseVariables.loadingFont.getSize() - 4, loadTextWidth + 18,
                        BaseVariables.loadingFont.getSize() * 3 + 12, 20, 20);
                g.setColor(Draw.sidecolor);

                g.fillRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2 - 5,
                        frame.getHeight() * 1 / 3 - BaseVariables.loadingFont.getSize(), loadTextWidth + 10,
                        BaseVariables.loadingFont.getSize() * 3 + 4, 20, 20);
                g.drawRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2 - 8,
                        frame.getHeight() * 1 / 3 - BaseVariables.loadingFont.getSize() - 3, loadTextWidth + 15,
                        BaseVariables.loadingFont.getSize() * 3 + 9, 20, 20);

                g.setPaint(gradient);
                g.fillRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.defaultFontSize / 2,
                        (int) (loadTextWidth * (MainPane.drawCanvas.loadBarSample / 100.0)), BaseVariables.defaultFontSize,
                        BaseVariables.defaultFontSize / 2, BaseVariables.defaultFontSize / 2);
                g.fillRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.defaultFontSize * 2,
                        (int) (loadTextWidth * (MainPane.drawCanvas.loadbarAll / 100.0)), BaseVariables.defaultFontSize,
                        BaseVariables.defaultFontSize / 2, BaseVariables.defaultFontSize / 2);
                g.setColor(Color.black);
                g.drawRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.defaultFontSize / 2,
                        (int) (loadTextWidth * (MainPane.drawCanvas.loadBarSample / 100.0)), BaseVariables.defaultFontSize, 4, 4);
                g.drawRoundRect(MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.defaultFontSize * 2,
                        (int) (loadTextWidth * (MainPane.drawCanvas.loadbarAll / 100.0)), BaseVariables.defaultFontSize, 4, 4);

                g.drawString(Loader.loadingtext, MainPane.drawScroll.getWidth() / 2 - loadTextWidth / 2,
                        frame.getHeight() * 1 / 3);
                g.drawRect(MainPane.drawScroll.getWidth() / 2 - canceltextwidth / 2 - BaseVariables.defaultFontSize / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3 - BaseVariables.defaultFontSize / 4,
                        canceltextwidth + BaseVariables.defaultFontSize,
                        BaseVariables.loadingFont.getSize() + BaseVariables.defaultFontSize / 2);
                g.setColor(Color.lightGray);
                g.fillRect(MainPane.drawScroll.getWidth() / 2 - canceltextwidth / 2 - BaseVariables.defaultFontSize / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3 - BaseVariables.defaultFontSize / 4,
                        canceltextwidth + BaseVariables.defaultFontSize,
                        BaseVariables.loadingFont.getSize() + BaseVariables.defaultFontSize / 2);
                g.setColor(Color.white);
                g.drawRect(MainPane.drawScroll.getWidth() / 2 - canceltextwidth / 2 - BaseVariables.defaultFontSize / 2 + 1,
                        frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3 - BaseVariables.defaultFontSize / 4 + 1,
                        canceltextwidth + BaseVariables.defaultFontSize - 2,
                        BaseVariables.loadingFont.getSize() + BaseVariables.defaultFontSize / 2 - 2);

                if (MainPane.cancelhover) {
                    
                    if (getCursor().getType() != Cursor.HAND_CURSOR) {
                        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    }

                    g.setColor(Color.gray);
                    g.fillRect(MainPane.drawScroll.getWidth() / 2 - canceltextwidth / 2 - BaseVariables.defaultFontSize / 2,
                            frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3 - BaseVariables.defaultFontSize / 4,
                            canceltextwidth + BaseVariables.defaultFontSize,
                            BaseVariables.loadingFont.getSize() + BaseVariables.defaultFontSize / 2);

                    g.setColor(Color.white);
                    g.drawRect(MainPane.drawScroll.getWidth() / 2 - canceltextwidth / 2 - BaseVariables.defaultFontSize / 2,
                            frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3 - BaseVariables.defaultFontSize / 4,
                            canceltextwidth + BaseVariables.defaultFontSize,
                            BaseVariables.loadingFont.getSize() + BaseVariables.defaultFontSize / 2);

                } else {
                    if (getCursor().getType() != Cursor.WAIT_CURSOR) {
                        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    }
                    g.setColor(Color.black);
                }
                g.drawString("Cancel", MainPane.drawScroll.getWidth() / 2 - canceltextwidth / 2,
                        frame.getHeight() * 1 / 3 + BaseVariables.loadingFont.getSize() * 3 + BaseVariables.loadingFont.getSize()
                                - 4);
            
                loaddraw = false;
            } else {
            
                g.setFont(BaseVariables.loadingFont);
                g.setColor(ChromDraw.backTransparent);
                
            }
        } else {

            if (getCursor().getType() != Cursor.DEFAULT_CURSOR) {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }
};