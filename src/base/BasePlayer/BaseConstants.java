package base.BasePlayer;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import base.BasePlayer.GUI.ChromDraw;

import java.awt.*;
// change to enum
public class BaseConstants {
	public final static int minzoom = 40;	
	final public static BasicStroke strongStroke = new BasicStroke(4), doubleStroke = new BasicStroke(2), basicStroke = new BasicStroke(1), doubledashed = new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, ChromDraw.dash, 0.0f);
	
	public BaseConstants() { setArrays(); }
	public static final Map<String, Color> CHROM_COLORS = Stream.of(new Object[][] {
			{ "1",  new Color(153,102,0) },   { "2",  new Color(102,102,0) },   { "3",  new Color(153,153,30) },  { "4",  new Color(204,0,0) }, 
			{ "5",  new Color(255,0,0) },     { "6",  new Color(255,0,204) },   { "7",  new Color(255,204,204) }, { "8",  new Color(255,153,0) }, 
			{ "9",  new Color(255,204,0) },   { "10", new Color(255,255,0) },   { "11", new Color(204,255,0) },   { "12", new Color(0,255,0) }, 
			{ "13", new Color(53,128,0) },    { "14", new Color(0,0,204) },     { "15", new Color(102,153,255) }, { "16", new Color(153,204,255) }, 
			{ "17", new Color(0,255,255) },   { "18", new Color(204,255,255) }, { "19", new Color(153,0,204) },   { "20", new Color(204,51,255) },
			{ "21", new Color(204,153,255) }, { "22", new Color(102,102,102) }, { "X",  new Color(153,153,153) }, { "Y",  new Color(204,204,204) },
			{ "M",  new Color(204,204,153) }, { "MT", new Color(204,204,153) }
			}).collect(Collectors.toMap(data -> (String) data[0], data -> (Color) data[1]));
	public static final Map<String, Color> SV_COLORS = Stream.of(new Object[][] {
	{ "DEL",  new Color(10,200,50,100) },  { "INV",  new Color(10,50,200, 100) },  { "DUP",  new Color(200,200,200,100) }, { "TRA",  new Color(200,200,0, 100) }, { "BND",  new Color(200,200,200,200) } }).collect(Collectors.toMap(data -> (String) data[0], data -> (Color) data[1]));
	public static final Map<String, Color> SV_COLORS_LINE = Stream.of(new Object[][] {
		{ "DEL",  new Color(10,200,50) },  { "INV",  new Color(10,50,200) },  { "DUP",  new Color(200,200,200) }, { "TRA",  new Color(200,200,0) }, { "BND",  new Color(200,200,200) } }).collect(Collectors.toMap(data -> (String) data[0], data -> (Color) data[1]));
	public static final Color infoBoxColor = new Color(225, 225, 210, 240);
	public static final Stroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, ChromDraw.dash, 0.0f);
	public static final Color intronColor = new Color(170, 170, 160), interColor = new Color(140, 140, 140);
	public static final Color redColor = new Color(226, 37, 24), greenColor = new Color(37,226,24);
	public static final Color softLight = new Color(255,255,255,50);
	public static final Color softColor = new Color(0,0,0,50);
	public static final Color loadColor = new Color(255,255,200,200);
	public static final Color forwardColor = new Color(171,194,171);
	public static final Color reverseColor = new Color(194,171,171);
	public static final Color reverseText= new Color(255,50,70);
	public static final Color forwardText = new Color(255,255,255);
	public static final Color reverseTextLow= new Color(150,100,100,200);
	public static final Color forwardTextLow = new Color(220,255,240,200);
	public static final Color zoomColor = new Color(255,150,50,140);
	public static final Color dark = new Color(25,25,25);
	public static final Color transparent = new Color(0,0,0,0);
	public static Color[] colorPalette = new Color[11];

	public HashMap<Character,Color> baseColors = new HashMap<Character,Color>();
	static Hashtable<String, String> aminoacids = new Hashtable<String, String>();
	public static Hashtable<String, String> codons = new Hashtable<String, String>();
	public static String[] baseContexts = new String[96];
	public static  final String contexts = "A[C>A]A A[C>A]C A[C>A]G A[C>A]T A[C>G]A A[C>G]C A[C>G]G A[C>G]T A[C>T]A A[C>T]C A[C>T]G A[C>T]T A[T>A]A A[T>A]C A[T>A]G A[T>A]T A[T>C]A A[T>C]C A[T>C]G A[T>C]T A[T>G]A A[T>G]C A[T>G]G A[T>G]T C[C>A]A C[C>A]C C[C>A]G C[C>A]T C[C>G]A C[C>G]C C[C>G]G C[C>G]T C[C>T]A C[C>T]C C[C>T]G C[C>T]T C[T>A]A C[T>A]C C[T>A]G C[T>A]T C[T>C]A C[T>C]C C[T>C]G C[T>C]T C[T>G]A C[T>G]C C[T>G]G C[T>G]T G[C>A]A G[C>A]C G[C>A]G G[C>A]T G[C>G]A G[C>G]C G[C>G]G G[C>G]T G[C>T]A G[C>T]C G[C>T]G G[C>T]T G[T>A]A G[T>A]C G[T>A]G G[T>A]T G[T>C]A G[T>C]C G[T>C]G G[T>C]T G[T>G]A G[T>G]C G[T>G]G G[T>G]T T[C>A]A T[C>A]C T[C>A]G T[C>A]T T[C>G]A T[C>G]C T[C>G]G T[C>G]T T[C>T]A T[C>T]C T[C>T]G T[C>T]T T[T>A]A T[T>A]C T[T>A]G T[T>A]T T[T>C]A T[T>C]C T[T>C]G T[T>C]T T[T>G]A T[T>G]C T[T>G]G T[T>G]T";
	public static final Map<String, Color> KATRI_COLORS = Stream.of(new Object[][] {
		{ "TssA",  new Color(255,0,0) }, 
		{ "TssFlnk",  new Color(255,69,0) },  
		{ "TssFlnkU",  new Color(255,69,0) }, 
		{ "TssFlnkD",  new Color(255,69,0) },
		{ "Tx", new Color(0,128,0) }, 
		{ "TxWk",  new Color(0,100,0) }, 
		{ "EnhG1",  new Color(194,225,5) }, 
		{ "EnhG2",  new Color(194,225,5) }, 
		{ "EnhA1",  new Color(255,195,77) }, 
		{ "EnhA2",  new Color(255,195,77) }, 
		{ "EnhWk",  new Color(255,255,0) }, 
		{ "ZNF/Rpts",  new Color(102,205,170) }, 
		{ "Het",  new Color(138,145,208) }, 
		{ "TssBiv",  new Color(205,92,92) }, 
		{ "EnhBiv",  new Color(189,183,107) }, 
		{ "ReprPC",  new Color(128,128,128) }, 
		{ "ReprPCWk",  new Color(192,192,192) }, 
		{ "Quies",  new Color(255,255,255) } }).collect(Collectors.toMap(data -> (String) data[0], data -> (Color) data[1]));

	void setArrays() {
		baseColors.put('A', Color.GREEN);
		baseColors.put('C', Color.BLUE);
		baseColors.put('G', Color.YELLOW);
		baseColors.put('T', Color.RED);
		colorPalette[10] = new Color(255, 60, 60); colorPalette[9] = new Color(255, 100, 100); colorPalette[8] = new Color(255, 140, 140); colorPalette[7] = new Color(255, 180, 180); colorPalette[6] = new Color(255, 220, 220);
		colorPalette[5] = new Color(255, 255, 255);	colorPalette[4] = new Color(220, 220, 255);	colorPalette[3] = new Color(180, 180, 255);	colorPalette[2] = new Color(140, 140, 255);	colorPalette[1] = new Color(100, 100, 255);
		colorPalette[0] = new Color(80, 80, 255);
		aminoacids.put("ATT", "Ile");	aminoacids.put("ATC", "Ile");	aminoacids.put("ATA", "Ile");	
		aminoacids.put("CTT", "Leu");	aminoacids.put("CTC", "Leu");	aminoacids.put("CTA", "Leu");	aminoacids.put("CTG", "Leu"); aminoacids.put("TTA", "Leu");	aminoacids.put("TTG", "Leu");	
		aminoacids.put("GTT", "Val");	aminoacids.put("GTC", "Val");	aminoacids.put("GTA", "Val");	aminoacids.put("GTG", "Val");	
		aminoacids.put("TTT", "Phe");	aminoacids.put("TTC", "Phe");
		aminoacids.put("ATG", "Met");
		aminoacids.put("TGT", "Cys");	aminoacids.put("TGC", "Cys");
		aminoacids.put("GCT", "Ala");	aminoacids.put("GCC", "Ala");	aminoacids.put("GCA", "Ala");	aminoacids.put("GCG", "Ala");
		aminoacids.put("GGT", "Gly");	aminoacids.put("GGC", "Gly");	aminoacids.put("GGA", "Gly");	aminoacids.put("GGG", "Gly");
		aminoacids.put("CCT", "Pro");	aminoacids.put("CCC", "Pro");	aminoacids.put("CCA", "Pro");	aminoacids.put("CCG", "Pro");
		aminoacids.put("ACT", "Thr");	aminoacids.put("ACC", "Thr");	aminoacids.put("ACA", "Thr");	aminoacids.put("ACG", "Thr");
		aminoacids.put("TCT", "Ser");	aminoacids.put("TCC", "Ser");	aminoacids.put("TCA", "Ser");	aminoacids.put("TCG", "Ser");	aminoacids.put("AGT", "Ser");	aminoacids.put("AGC", "Ser");
		aminoacids.put("TAT", "Tyr");	aminoacids.put("TAC", "Tyr");
		aminoacids.put("TGG", "Trp");
		aminoacids.put("CAA", "Gln");	aminoacids.put("CAG", "Gln");
		aminoacids.put("AAT", "Asn");	aminoacids.put("AAC", "Asn");
		aminoacids.put("CAT", "His");	aminoacids.put("CAC", "His");
		aminoacids.put("GAA", "Glu");	aminoacids.put("GAG", "Glu");
		aminoacids.put("GAT", "Asp");	aminoacids.put("GAC", "Asp");
		aminoacids.put("AAA", "Lys");	aminoacids.put("AAG", "Lys");
		aminoacids.put("CGT", "Arg");	aminoacids.put("CGC", "Arg");	aminoacids.put("CGA", "Arg");	aminoacids.put("CGG", "Arg");	aminoacids.put("AGA", "Arg");	aminoacids.put("AGG", "Arg");
		aminoacids.put("TAA", "Stop"); aminoacids.put("TAG", "Stop");	aminoacids.put("TGA", "Stop");
		codons.put("ATT", "ATT");	codons.put("ATC", "ATC");	codons.put("ATA", "ATA");	codons.put("CTT", "CTT"); codons.put("CTC", "CTC");	codons.put("CTA", "CTA"); codons.put("CTG", "CTG");	codons.put("TTA", "TTA");
		codons.put("TTG", "TTG");	codons.put("GTT", "GTT"); codons.put("GTC", "GTC");	codons.put("GTA", "GTA");	codons.put("GTG", "GTG");	codons.put("TTT", "TTT");	codons.put("TTC", "TTC");	codons.put("ATG", "ATG");
		codons.put("TGT", "TGT");	codons.put("TGC", "TGC");	codons.put("GCT", "GCT");	codons.put("GCC", "GCC");	codons.put("GCA", "GCA");	codons.put("GCG", "GCG");	codons.put("GGT", "GGT");	codons.put("GGC", "GGC");
		codons.put("GGA", "GGA");	codons.put("GGG", "GGG");	codons.put("CCT", "CCT");	codons.put("CCC", "CCC");	codons.put("CCA", "CCA");	codons.put("CCG", "CCG");	codons.put("ACT", "ACT");	codons.put("ACC", "ACC");
		codons.put("ACA", "ACA");	codons.put("ACG", "ACG");	codons.put("TCT", "TCT");	codons.put("TCC", "TCC");	codons.put("TCA", "TCA");	codons.put("TCG", "TCG");	codons.put("AGT", "AGT");	codons.put("AGC", "AGC");
		codons.put("TAT", "TAT");	codons.put("TAC", "TAC");	codons.put("TGG", "TGG");	codons.put("CAA", "CAA");	codons.put("CAG", "CAG");	codons.put("AAT", "AAT");	codons.put("AAC", "AAC");	codons.put("CAT", "CAT");
		codons.put("CAC", "CAC");	codons.put("GAA", "GAA");	codons.put("GAG", "GAG");	codons.put("GAT", "GAT");	codons.put("GAC", "GAC");	codons.put("AAA", "AAA");	codons.put("AAG", "AAG");	codons.put("CGT", "CGT");
		codons.put("CGC", "CGC");	codons.put("CGA", "CGA");	codons.put("CGG", "CGG");	codons.put("AGA", "AGA");	codons.put("AGG", "AGG");	codons.put("TAA", "TAA");	codons.put("TAG", "TAG");	codons.put("TGA", "TGA");
		}

	public static String[] getGposColor(final String gpos) {
		final String[] color = new String[3];
		if (gpos.equals("gneg")) {
			color[0] = "255";
			color[1] = "255";
			color[2] = "255";

			return color;
		} else if (gpos.equals("gpos50")) {
			color[0] = "200";
			color[1] = "200";
			color[2] = "200";
			return color;
		} else if (gpos.equals("gpos100")) {
			color[0] = "0";
			color[1] = "0";
			color[2] = "0";
			return color;
		} else if (gpos.equals("gpos75")) {
			color[0] = "130";
			color[1] = "130";
			color[2] = "130";
			return color;
		} else if (gpos.equals("gpos66")) {
			color[0] = "160";
			color[1] = "160";
			color[2] = "160";
			return color;
		} else if (gpos.equals("gpos33")) {
			color[0] = "210";
			color[1] = "210";
			color[2] = "210";
			return color;
		} else if (gpos.equals("gpos25")) {
			color[0] = "200";
			color[1] = "200";
			color[2] = "200";
			return color;
		} else if (gpos.equals("gvar")) {
			color[0] = "220";
			color[1] = "220";
			color[2] = "220";
			return color;
		} else if (gpos.equals("acen")) {
			color[0] = "217";
			color[1] = "47";
			color[2] = "39";
			return color;
		} else if (gpos.equals("stalk")) {
			color[0] = "100";
			color[1] = "127";
			color[2] = "164";
			return color;
		} else if (gpos.equals("gpos")) {
			color[0] = "0";
			color[1] = "0";
			color[2] = "0";
			return color;
		}
		return color;
	}
}
