package base.BasePlayer.io;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import base.BasePlayer.Getter;
import base.BasePlayer.ProjectValues;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.ControlFile;
import base.BasePlayer.sample.Sample;

public class SessionHandler {
    public static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static void saveSession() {
        try {       
            ProjectValues values = MainPane.projectValues;
            values.samples = Getter.getInstance().getSampleList().stream().map(Sample::toJSON).collect(Collectors.toList());
            values.controls = Getter.getInstance().getControlList().stream().map(ControlFile::toJSON).collect(Collectors.toList());
            String json = gson.toJson(values);
            Files.write(Paths.get(values.projectFile), json.getBytes());
            
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void openSession(File openfile) {
        try {
           
            try {
                String content = new String(Files.readAllBytes(Paths.get(openfile.getCanonicalPath())));
                     
                MainPane.projectValues = gson.fromJson(content, ProjectValues.class);
            } catch (IOException e) {
                MainPane.projectValues = new ProjectValues();
            }            
            VariantHandler.loadValues();
            
            final FileRead filereader = new FileRead();

            /* filereader.start = (int) MainPane.drawCanvas.selectedSplit.start;
            filereader.end = (int) MainPane.drawCanvas.selectedSplit.end; */
            filereader.openSession = true;
            filereader.execute();
            Draw.drawVariables.projectName = openfile.getName().replace(".json", "");
           
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    //test for openSession
    public static void main(String[] args) {
        File openfile = new File("project_values.json");
        openSession(openfile);
        System.out.println("openSession test passed");
    }
}
