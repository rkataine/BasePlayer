/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.io;
import htsjdk.samtools.CRAMFileReader;
import htsjdk.samtools.CigarElement;
import htsjdk.samtools.CigarOperator;
import htsjdk.samtools.QueryInterval;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import htsjdk.tribble.readers.TabixReader;

import htsjdk.tribble.index.tabix.TabixIndexCreator;
import htsjdk.samtools.cram.ref.ReferenceSource;

import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import java.util.zip.GZIPInputStream;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;

import org.apache.commons.io.FilenameUtils;

import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.Main;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.Setter;
import base.BasePlayer.GUI.BedCanvas;
import base.BasePlayer.GUI.BedTrack;
import base.BasePlayer.GUI.ChromDraw;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.BedTable;
import base.BasePlayer.GUI.modals.SampleDialog;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.Control;
import base.BasePlayer.genome.Gene;
import base.BasePlayer.genome.Transcript;
import base.BasePlayer.genome.Transcript.Exon;
import base.BasePlayer.reads.ReadBuffer;
import base.BasePlayer.reads.ReadNode;
import base.BasePlayer.reads.Reads;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.tracks.BedNode;
import base.BasePlayer.variants.VarCalculations;
import base.BasePlayer.variants.VarMaster;
import base.BasePlayer.variants.VarNode;
import base.BasePlayer.variants.VariantCall;
import base.BasePlayer.variants.VcfReader;


// @SuppressWarnings("deprecation")
public class FileRead extends SwingWorker< String, Object >  {
	public static VcfReader vcfReader = new VcfReader();
	
	public Boolean readVCF = false, changeChrom = false, readBAM = false,searchInsSites = false, varcalc = false, getreads = false, readBED = false;
	public File[] files;
	public String chrom = "0";
	public static boolean searchingBams = false, nobeds = false;
	public int pos, calls1, calls2;	
	private boolean found;
	private int xpos;	
	private ReadNode mundane = null;
	
	static int searchwindow = 1000;
	//private boolean left = false;
	public boolean stop = false;
	
	private ReadNode addNode;
	
	public static Gene currentGene = new Gene();
	public static int currentGeneEnd = 0;
	public static StringBuffer sampleString = new StringBuffer("");
	public static SamReader samFileReader;	
	public static CRAMFileReader CRAMReader = null;
	public Iterator<SAMRecord> bamIterator = null;	
	public SAMRecord samRecord, samRecordBuffer;
	private Sample sample;
	public static boolean novars = false;
	public String basecontext = "";
	public int start, end, viewLength;
	public double pixel;
	public Reads readClass;	
	public static final int headnode = 0;

  static final int tailnode = 1;	
	public 	VarNode current;
	public static int firstReadPos, lastReadPos;
	private int startY;
	
	public SplitClass splitIndex;
	public boolean statcalc;
	
	private int middle;
	private ReadBuffer currentread;
	private int searchPos;	
	private int oldstart, readstart, readpos, mispos;	
	private CigarElement element;	
	public boolean firstCov;

  	public boolean openSession = false;
	
	public static boolean bigcalc=false, changing = false, readFiles, search;
	public static int searchStart, searchEnd;
	public static boolean cancelfileread = false, cancelreadread= false, readsLoaded = false;
	public static TabixIndexCreator indexCreator;	
	public static int lastpos = 0;
	public static String outputName = "";
	public static long filepointer = 0;

	public FileRead(final File[] files) {
		this.files = files;
	}

	public FileRead() {

	}

	protected String doInBackground() throws Exception {
		if (openSession) {			
			File[] controlFiles = MainPane.projectValues.controls.stream().map(sample -> new File(sample.vcf)).toArray(File[]::new);
			Control.addFiles(controlFiles);
			Control.controlData.loadData();
			File[] sampleFiles = MainPane.projectValues.samples.stream().map(sample -> new File(sample.get("vcf"))).toArray(File[]::new);
			vcfReader.readVCF(sampleFiles, start, end);			
			openSession = false;
			
			MainPane.drawCanvas.repaint();
		} else if (readVCF) {
			if (Draw.drawVariables.projectName.equals("Untitled")) {
				Main.frame.setTitle("BasePlayer - Untitled Project");
			}

			vcfReader.readVCF(files, start, end);
			readVCF = false;

			
			MainPane.drawCanvas.repaint();
		} else if (readBAM) {
			if (Draw.drawVariables.projectName.equals("Untitled")) {
				Main.frame.setTitle("BasePlayer - Untitled Project");
			}
			Loader.setLoading("Loading samples");
			readBAM(files);
			readBAM = false;
			Loader.ready("Loading samples");
		} else if (readBED) {
			readBED(files);
			readBED = false;
			Loader.ready("Loading tracks");
		} else if (changeChrom) {
			changeChrom(chrom);
			changeChrom = false;
			
			Draw.updateReads = true;
			MainPane.drawCanvas.repaint();
		} else if (varcalc) {
			Loader.setLoading("Processing variants...");

			try {
				// if(Getter.getInstance().getVariantHead().getNext() == null) {
				if (VariantHandler.windowcalc.isSelected()) {
					VarCalculations.getInstance().varCalcBig();
				} else {
					VarCalculations.getInstance().varCalc();
				}

				varcalc = false;
			} catch (final Exception e) {
				e.printStackTrace();
				Loader.ready("Processing variants...");
			}
			Loader.ready("Processing variants...");
		} else if (getreads) {

			if (splitIndex.viewLength <= Settings.readDrawDistance) {

				Draw.forEachVisibleSample(sample -> {
					try {										
						if (sample.samFile == null) return;
						
						if (sample.getreadHash().get(splitIndex) == null) 
							sample.resetreadHash();
						
						final Reads readClass = sample.getreadHash().get(splitIndex);

						if (readClass.searchstart != Integer.MAX_VALUE && readClass.searchstart <= (int) splitIndex.start && readClass.searchend >= (int) splitIndex.end)
							return;						

						if (!Loader.readyQueue.contains("Loading reads")) 
							Loader.setLoading("Loading reads");
						
						getReads(splitIndex.chrom, (int) splitIndex.start, (int) splitIndex.end, readClass, splitIndex);
						
						splitIndex.updateReads = true;
						splitIndex.loadingReads = false;
					} catch (final Exception e) {
						e.printStackTrace();
					}
				});
			} else {
				// this.readClass.loading = true;
				Loader.setLoading("Loading reads");
				splitIndex.loadingReads = true;

				getReads(chrom, start, end, this.readClass, splitIndex);
				splitIndex.updateReads = true;
			}

			getreads = false;
			Loader.ready("Loading reads");
			MainPane.drawCanvas.repaint();
			FileRead.readsLoaded = true;

		}

		return null;
	}

	public void changeChrom(final String chrom) {

		try {
			nobeds = false;
			cancelfileread = false;
			if (!search) FileRead.novars = false;
			
			try {
				Loader.setLoading("Loading annotation...");
				Draw.getSplits().get(0).setGenes(getExons(chrom));
				MainPane.chromDraw.updateExons = true;
				MainPane.chromDraw.repaint();
			} catch (final Exception e) {
				ErrorLog.addError(e.getStackTrace());
				e.printStackTrace();
			}

			Loader.ready("Loading annotation...");
			final ArrayList<BedTrack> bigs = new ArrayList<BedTrack>();
			if (MainPane.bedCanvas.bedTrack.size() > 0) {
				Loader.setLoading("Loading BED-files...");
				MainPane.bedCanvas.bedOn = true;
				boolean ison = false;
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
					if (MainPane.bedCanvas.bedTrack.get(i).intersect) {
						ison = true; break;
					}
				}
				if (!ison) MainPane.bedCanvas.bedOn = false;				
			
				for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {

					MainPane.bedCanvas.bedTrack.get(i).used = false;

					if (MainPane.bedCanvas.bedTrack.get(i).small	&& MainPane.bedCanvas.bedTrack.get(i).getBBfileReader() == null) {
						MainPane.bedCanvas.getBEDfeatures(MainPane.bedCanvas.bedTrack.get(i), 1,	Draw.getSplits().get(0).chromEnd);
					} else {
						if (search && searchEnd - searchStart < Settings.windowSize) {
							MainPane.bedCanvas.getBEDfeatures(MainPane.bedCanvas.bedTrack.get(i), searchStart, searchEnd);
						} else {
							if (MainPane.bedCanvas.bedTrack.get(i).small) {
								MainPane.bedCanvas.getBEDfeatures(MainPane.bedCanvas.bedTrack.get(i), 1,	Draw.getSplits().get(0).chromEnd);
							}
						}
					}
					if (MainPane.bedCanvas.bedTrack.get(i).intersect && (!MainPane.bedCanvas.bedTrack.get(i).small || MainPane.bedCanvas.bedTrack.get(i).getBBfileReader() != null)) {
						MainPane.bedCanvas.bedTrack.get(i).intersect = false;
						bigs.add(MainPane.bedCanvas.bedTrack.get(i));
					}
					if (nobeds) {	Loader.ready("Loading BED-files..."); return;	}
				}
			}

			Loader.ready("Loading BED-files...");

			if (novars) {
				Draw.drawVariables.variantsStart = 0;
				Draw.drawVariables.variantsEnd = 0;
			} else changing = true;			

			if (MainPane.varsamples > 0 && !novars && !bigcalc) {
				VarCalculations.removeNonListVariants();
				removeBedLinks();
				Loader.setLoading("Loading variants...");
				VarMaster.getInstance().reset();
				MainPane.chromDraw.vardraw = null;

				for (int i = 0; i < MainPane.samples; i++) {
					if (nobeds) return;					
					if (cancelfileread || !Getter.getInstance().loading()) {
						VcfReader.cancelFileRead();	break;
					}
					if (Getter.getInstance().getSampleList().get(i).getTabixFile() == null || Getter.getInstance().getSampleList().get(i).multipart) continue;
					if (search) vcfReader.getVariants(chrom, FileRead.searchStart, FileRead.searchEnd, Getter.getInstance().getSampleList().get(i));
					else vcfReader.getVariants(chrom, 0, Draw.getSplits().get(0).chromEnd,	Getter.getInstance().getSampleList().get(i));
				}
				annotate();

				if (MainPane.drawCanvas.annotationOn) SampleDialog.checkAnnotation();				

				Loader.setLoading("Applying controls...");
				if (Control.controlData.controlsOn) Control.applyControl();				
				Loader.ready("Applying controls...");
				readFiles = false;
				MainPane.bedCanvas.intersected = false;
				
				VarMaster.calcClusters(Getter.getInstance().getVariantHead());			

				if (MainPane.bedCanvas.bedOn) {
					Loader.loadingtext = "Annotating variants";
					int smalls = 0;
					for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
						if (MainPane.bedCanvas.bedTrack.get(i).small	&& MainPane.bedCanvas.bedTrack.get(i).getBBfileReader() == null) {
							if (MainPane.bedCanvas.bedTrack.get(i).intersect && !MainPane.bedCanvas.bedTrack.get(i).loading) {
								smalls++;
								MainPane.bedCanvas.annotate(MainPane.bedCanvas.bedTrack.get(i).getHead(),	Getter.getInstance().getVariantHead().getNext());
								MainPane.bedCanvas.intersected = true;
							} else if (MainPane.bedCanvas.bedTrack.get(i).intersect	&& MainPane.bedCanvas.bedTrack.get(i).loading) 
								MainPane.bedCanvas.bedTrack.get(i).waiting = true;							
						}
					}
					if (smalls == 0) {
						VarNode current = Getter.getInstance().getVariantHead().getNext();
						while (current != null) {
							current.bedhit = true;
							current = current.getNext();
						}
					}
					for (int i = 0; i < bigs.size(); i++) {
						bigs.get(i).intersect = true;
						final BedCanvas.Annotator annotator = MainPane.bedCanvas.new Annotator(bigs.get(i));
						annotator.annotateVars();
						MainPane.bedCanvas.intersected = true;
					}
					bigs.clear();
				}			
				VarMaster.calcClusters(Getter.getInstance().getVariantHead());
			}

			
			Loader.ready("Loading variants...");
			if (!Getter.getInstance().loading()) 
			
			if (novars) {
				Draw.drawVariables.variantsStart = 0;
				Draw.drawVariables.variantsEnd = Draw.getSplits().get(0).chromEnd;
			}

			search = changing = false;
			current = null;
			MainPane.chromDraw.updateExons = true;
			MainPane.chromDraw.repaint();

		} catch (final Exception e) {
			e.printStackTrace();
			ErrorLog.addError(e.getStackTrace());
			changing = false;
		}
		MainPane.drawCanvas.loadbarAll = 0;
		MainPane.drawCanvas.loadBarSample = 0;
		MainPane.drawCanvas.repaint();
	}

	public ArrayList<Gene> getExons(final String chrom) {

		final ArrayList<Gene> transcriptsTemp = new ArrayList<Gene>();
		try {
			if (MainPane.genomehash.size() == 0 || MainPane.genomehash.get(MainPane.defaultGenome).size() == 0) 
				return new ArrayList<Gene>();
			
			if (ChromDraw.exonReader != null) ChromDraw.exonReader.close();			
			ChromDraw.exonReader = new TabixReader(MainPane.genomehash.get(MainPane.defaultGenome).get(MainPane.annotation).getCanonicalPath());

			if (chrom == null) return null;		

			TabixReader.Iterator exonIterator = null;
			try {
				if (!ChromDraw.exonReader.getChromosomes().contains(chrom)) {
					final String[] gene = { MenuBar.chromosomeDropdown.getSelectedItem().toString(), "1","" + Draw.getSplits().get(0).chromEnd,MenuBar.chromosomeDropdown.getSelectedItem().toString(), "1", "+", "-", "-", "-", "-", "-",
							"1", "1", "1", "" + Draw.getSplits().get(0).chromEnd, "-1,", "-" };
					final Gene addGene = new Gene(gene);
					Transcript addtrans = null;
					try {
						addtrans = new Transcript(gene);
					} catch (final Exception e) {
						e.printStackTrace();
					}
					addGene.addTranscript(addtrans);
					addGene.setLongest(addtrans);
					addtrans.setGene(addGene);
					transcriptsTemp.add(addGene);
					return transcriptsTemp;				
				} else exonIterator = ChromDraw.exonReader.query(chrom);
				
			} catch (final Exception e) {
				try {
					if (chrom.matches("\\w+")) exonIterator = ChromDraw.exonReader.query("M");					
				} catch (final Exception ex) {				
					e.printStackTrace();
				}
			}
			String s;
			String[] exonSplit;
			Transcript addtrans = null;
			final Hashtable<String, Gene> genes = new Hashtable<String, Gene>();
			Gene setGene;
			while (exonIterator != null && (s = exonIterator.next()) != null) {
				exonSplit = s.split("\t");
				if (exonSplit[0].equals("23")) exonSplit[0] = "X";
				else if (exonSplit[0].equals("24")) exonSplit[0] = "Y";
				else if (exonSplit[0].equals("25")) exonSplit[0] = "MT";				
				addtrans = new Transcript(exonSplit);

				if (!genes.containsKey(exonSplit[6])) {
					final Gene addgene = new Gene(exonSplit);
					genes.put(exonSplit[6], addgene);
					addgene.addTranscript(addtrans);
					addgene.setLongest(addtrans);
					addtrans.setGene(addgene);
					transcriptsTemp.add(addgene);
				} else {
					setGene = genes.get(exonSplit[6]);
					setGene.addTranscript(addtrans);
					addtrans.setGene(setGene);
					if (addtrans.getLength() > setGene.getLongest().getLength()) setGene.setLongest(addtrans);					
				}
			}
			genes.clear();
			ChromDraw.exonReader.close();
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
		return transcriptsTemp;
	}

	static File[] readLinkFile(final File linkfile) {
		File[] bamfiles = null;
		try {
			final FileReader freader = new FileReader(linkfile);
			final BufferedReader reader = new BufferedReader(freader);
			final ArrayList<File> filetemp = new ArrayList<File>();
			String line;
			while ((line = reader.readLine()) != null) {
				final File addfile = new File(line.trim());
				if (addfile.exists()) {
					filetemp.add(addfile);
				}
			}
			if (freader != null) {
				freader.close();
			}
			reader.close();
			bamfiles = new File[filetemp.size()];
			for (int i = 0; i < filetemp.size(); i++) {
				bamfiles[i] = filetemp.get(i);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return bamfiles;
	}

	private void readBAM(File[] files) {
		try {

			File addFile = null;
			File[] addDir;
			Sample currentSample = null;
			Boolean added = false;
		
			if (files.length == 1 && files[0].getName().endsWith(".link")) {
				files = readLinkFile(files[0]);
			}

			for (int i = 0; i < files.length; i++) {

				if (files[i].isDirectory()) {
					addDir = files[i].listFiles();
					for (int f = 0; f < addDir.length; f++) {
						if (addDir[f].getName().endsWith(".bam") || addDir[f].getName().endsWith(".cram")) {
							addFile = addDir[f];
							break;
						}
					}
				} else {
					if (files[i].getName().endsWith(".bam") || files[i].getName().endsWith(".cram")) {
						addFile = files[i];
					} else {
						continue;
					}
				}

				if (addFile != null) {
					MainPane.drawCanvas.bam = true;
					currentSample = new Sample(addFile.getName(), (short) MainPane.samples, null);
					Getter.getInstance().getSampleList().add(currentSample);
					currentSample.samFile = addFile;
					currentSample.resetreadHash();
					if (addFile.getName().endsWith(".cram")) {
						currentSample.CRAM = true;
					}

					if (currentSample.samFile.getName().endsWith(".cram")) {
						currentSample.readString = "CRAM";
					} else {
						currentSample.readString = "BAM";
					}
					try {

					} catch (final Exception e) {
						ErrorLog.addError(e.getStackTrace());
						e.printStackTrace();
					}
					added = true;
					MainPane.readsamples++;
					MainPane.samples++;
				}

			}
			if (!added) {
				return;
			}
			checkSamples();
			Draw.drawVariables.setVisibleSamples.accept(MainPane.samples);
		
			if (MainPane.drawScroll.getViewport().getHeight()
					/ (Getter.getInstance().getSampleList().size()) > Draw.defaultSampleHeight) {
				Draw.drawVariables.sampleHeight = MainPane.drawScroll.getViewport().getHeight()
						/ Getter.getInstance().getSampleList().size();
			} else {
				Draw.drawVariables.sampleHeight = Draw.defaultSampleHeight;
			}

			if (MainPane.drawCanvas.getHeight() < (Getter.getInstance().getSampleList().size())
					* Draw.drawVariables.sampleHeight) {

				MainPane.drawCanvas.resizeCanvas(MainPane.drawCanvas.getWidth(),
						(int) ((Getter.getInstance().getSampleList().size()) * Draw.drawVariables.sampleHeight));
				MainPane.drawCanvas.revalidate();
			}
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
		Draw.updateReads = true;
		
		MainPane.drawCanvas.repaint();
	}

	public static boolean checkIndex(final File file) {
		try {
			if (file.getName().endsWith(".vcf")) {
				return new File(file.getCanonicalPath() + ".idx").exists()
						|| new File(file.getCanonicalPath() + ".tbi").exists();
			} else if (file.getName().toLowerCase().endsWith(".vcf.gz")) {
				return new File(file.getCanonicalPath() + ".tbi").exists()
						|| new File(file.getCanonicalPath() + ".csi").exists();
			} else if (file.getName().toLowerCase().endsWith(".bam")) {
				if (!new File(file.getCanonicalPath() + ".bai").exists()) {
					return new File(file.getCanonicalPath().replace(".bam", "") + ".bai").exists();
				} else {
					return true;
				}
			} else if (file.getName().toLowerCase().endsWith(".bed.gz")
					|| file.getName().toLowerCase().endsWith(".gff.gz") || file.getName().toLowerCase().endsWith(".bed")
					|| file.getName().toLowerCase().endsWith(".gff")) {

				return new File(file.getCanonicalPath() + ".tbi").exists();
			} else if (file.getName().toLowerCase().endsWith(".tsv.gz")
					|| file.getName().toLowerCase().endsWith(".tsv.bgz")) {

				return new File(file.getCanonicalPath() + ".tbi").exists();
			} else {
				return true;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public static class SearchBamFiles extends SwingWorker<String, Object> {
		boolean cram = false;
		// SamReader samFileReader;
		int sampletemp;

		CRAMFileReader CRAMReader = null;
		File[] files;
		ArrayList<File> bamdirs;

		public SearchBamFiles(final File[] files, final ArrayList<File> bamdirs, final int sampletemp) {
			this.files = files;
			this.bamdirs = bamdirs;
			/* this.fileindex = fileindex; */
			this.sampletemp = sampletemp;

		}

		protected String doInBackground() {

			try {
				File[] bamfilestemp = null;
				final ArrayList<File> bamfiles = new ArrayList<File>();
				searchingBams = true;

				for (int i = 0; i < bamdirs.size(); i++) {

					bamfilestemp = bamdirs.get(i).listFiles(new FilenameFilter() {
						public boolean accept(final File dir, final String name) {

							return name.toLowerCase().endsWith(".bam") || name.toLowerCase().endsWith(".cram");
						}
					});

					if (bamfilestemp != null && bamfilestemp.length > 0) {
						for (int j = 0; j < bamfilestemp.length; j++) {

							bamfiles.add(bamfilestemp[j]);
						}
					} else {

						bamfilestemp = bamdirs.get(i).listFiles(new FilenameFilter() {
							public boolean accept(final File dir, final String name) {
								return name.toLowerCase().endsWith(".link");
							}
						});

						if (bamfilestemp != null && bamfilestemp.length > 0) {
							for (int j = 0; j < bamfilestemp.length; j++) {

								final File[] fileTemp = readLinkFile(bamfilestemp[j]);
								for (int f = 0; f < fileTemp.length; f++) {
									bamfiles.add(fileTemp[f]);
								}

							}
						}

					}
				}
				
				if (bamfiles.size() > 0) {

					int index = -1, sampleindex;
					for (int i = 0; i < bamfiles.size(); i++) {

						cram = false;
						sampleindex = 0;
						index = sampleString.indexOf(bamfiles.get(i).getName().substring(0, bamfiles.get(i).getName().indexOf(".")));

						if (index < 0)
							continue;
						if (!checkIndex(bamfiles.get(i))) {
							MenuBar.putMessage("Check Tools->View log");
							ErrorLog.addError("No index file found for " + bamfiles.get(i).getName());
							continue;
						}
						for (final char letter : sampleString.substring(0, index).toString().toCharArray()) {
							if (letter == ';')
								sampleindex++;
						}
						MainPane.drawCanvas.bam = true;
						MainPane.readsamples++;
						Getter.getInstance().getSampleList().get(sampleindex + sampletemp).samFile = new File(
								bamfiles.get(i).getCanonicalPath());
						Getter.getInstance().getSampleList().get(sampleindex + sampletemp).resetreadHash();

						if (MainPane.readsamples == 1) {
							checkSamples();
						}

						if (bamfiles.get(i).getName().endsWith(".cram")) {
							cram = true;
						} else {
							cram = false;

						}
						Getter.getInstance().getSampleList().get(sampleindex + sampletemp).CRAM = cram;
						if (cram) {
							Getter.getInstance().getSampleList().get(sampleindex + sampletemp).readString = "CRAM";
						} else {
							Getter.getInstance().getSampleList().get(sampleindex + sampletemp).readString = "BAM";
						}
						/*
						 * if(samFileReader != null) { samFileReader.close(); }
						 */
					}
				}

				sampleString = new StringBuffer("");
				files = null;
			} catch (final Exception e) {
				searchingBams = false;
				e.printStackTrace();
			}
			searchingBams = false;
			MainPane.drawCanvas.repaint();
			return "";
		}

	}

	

	public static void checkSamples() {
		/*
		 * if(MainPane.varsamples == 0) { MainPane.drawCanvas.varbuffer =
		 * MethodLibrary.toCompatibleImage(new BufferedImage(1, 1,
		 * BufferedImage.TYPE_INT_ARGB)); MainPane.drawCanvas.g2v =
		 * (Graphics2D)MainPane.drawCanvas.varbuffer.getGraphics(); } else {
		 */
		// MainPane.drawCanvas.varbuffer = MethodLibrary.toCompatibleImage(new
		// BufferedImage(MainPane.screenSize.width, MainPane.screenSize.height,
		// BufferedImage.TYPE_INT_ARGB));
		// MainPane.drawCanvas.g2v = (Graphics2D)MainPane.drawCanvas.varbuffer.getGraphics();
		// }
		if (MainPane.varsamples < 2) {
			VariantHandler.commonSlider.setMaximum(1);
			VariantHandler.commonSlider.setValue(1);
			VariantHandler.commonSlider.setUpperValue(1);
			VariantHandler.geneSlider.setMaximum(1);
			VariantHandler.geneSlider.setValue(1);
			// VariantHandler.filterPanes.setEnabledAt(VariantHandler.filterPanes.getTabCount()-1,
			// false);
			VariantHandler.filterPanes.setToolTipTextAt(VariantHandler.filterPanes.getTabCount() - 1,
					"Open more samples to compare variants.");

		} else {
			VariantHandler.commonSlider.setMaximum(MainPane.varsamples);
			VariantHandler.commonSlider.setValue(1);
			VariantHandler.commonSlider.setUpperValue(MainPane.varsamples);
			VariantHandler.geneSlider.setMaximum(MainPane.varsamples);
			VariantHandler.geneSlider.setValue(1);
			VariantHandler.filterPanes.setEnabledAt(VariantHandler.filterPanes.getTabCount() - 1, true);
			VariantHandler.filterPanes.setToolTipTextAt(VariantHandler.filterPanes.getTabCount() - 1,
					"Compare variants.");
		}

		if (MainPane.readsamples > 0) {
			/*
			 * MainPane.drawCanvas.readbuffer = MethodLibrary.toCompatibleImage(new
			 * BufferedImage(MainPane.screenSize.width, MainPane.screenSize.height,
			 * BufferedImage.TYPE_INT_ARGB)); MainPane.drawCanvas.rbuf =
			 * (Graphics2D)MainPane.drawCanvas.readbuffer.getGraphics(); MainPane.drawCanvas.backupr
			 * = MainPane.drawCanvas.rbuf.getComposite();
			 * MainPane.drawCanvas.rbuf.setRenderingHints(Draw.rh);
			 * MainPane.drawCanvas.coveragebuffer = MethodLibrary.toCompatibleImage(new
			 * BufferedImage(MainPane.screenSize.width, MainPane.screenSize.height,
			 * BufferedImage.TYPE_INT_ARGB)); MainPane.drawCanvas.cbuf =
			 * (Graphics2D)MainPane.drawCanvas.coveragebuffer.getGraphics();
			 * MainPane.drawCanvas.backupc = MainPane.drawCanvas.cbuf.getComposite();
			 */
			MenuBar.average.setEnabled(true);
			//MainPane.variantCaller.setEnabled(true);
			MenuBar.peakCaller.setEnabled(true);

			if (VarCalculations.caller) {
				if (MainPane.readsamples > 1) {
					VariantHandler.filterPanes.setEnabledAt(VariantHandler.filterPanes.getTabCount() - 1, true);
					VariantHandler.commonSlider.setMaximum(MainPane.readsamples);
					VariantHandler.commonSlider.setUpperValue(MainPane.readsamples);
					VariantHandler.geneSlider.setMaximum(MainPane.readsamples);
					VariantHandler.geneSlider.setValue(1);
				}
				MenuBar.manage.setEnabled(true);
			}
		} else {
			/*
			 * MainPane.drawCanvas.readbuffer = MethodLibrary.toCompatibleImage(new
			 * BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)); MainPane.drawCanvas.rbuf =
			 * (Graphics2D)MainPane.drawCanvas.readbuffer.getGraphics(); MainPane.drawCanvas.backupr
			 * = MainPane.drawCanvas.rbuf.getComposite();
			 * MainPane.drawCanvas.rbuf.setRenderingHints(Draw.rh);
			 * MainPane.drawCanvas.coveragebuffer = MethodLibrary.toCompatibleImage(new
			 * BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)); MainPane.drawCanvas.cbuf =
			 * (Graphics2D)MainPane.drawCanvas.coveragebuffer.getGraphics();
			 * MainPane.drawCanvas.backupc = MainPane.drawCanvas.cbuf.getComposite();
			 */
			MenuBar.average.setEnabled(false);
			MenuBar.average.setToolTipText("No bam/cram files opened");
			/* MainPane.variantCaller.setEnabled(false);
			MainPane.variantCaller.setToolTipText("No bam/cram files opened"); */
		}
	}

	public static void annotate() {
		if (Draw.getSplits().get(0).getGenes().size() == 0) return;
		Transcript transcript;
		Gene gene, prevGene = Draw.getSplits().get(0).getGenes().get(0);
		VarNode current = Getter.getInstance().getVariantHead().getNext();
		if (current == null) return;
		Transcript.Exon exon;
		int position = 0, baselength;
		boolean intronic = true;
		int spliceLength = Integer.parseInt(Settings.spliceField.getText().trim());
		try {
			
			for (int g = 0; g < Draw.getSplits().get(0).getGenes().size(); g++) {
				gene = Draw.getSplits().get(0).getGenes().get(g);
				
				if (current == null) break;
				
				for (int t = 0; t < gene.getTranscripts().size(); t++) {
					transcript = gene.getTranscripts().get(t);

					if (current != null && current.getPrev() != null) {
						while (current.getPrev().getPosition() >= transcript.getStart()) 
							if (current.getPrev() != null) current = current.getPrev();							
					}

					position = current.getPosition();
					
					if (current.indel) {
						position++;
						baselength = MethodLibrary.getBaseLength(current.vars);
					}
					
					while (position < transcript.getEnd()) {
						try {
							
							if (position >= transcript.getStart() && position <= transcript.getEnd()) {
								current.setInGene();
								baselength = 0;
								intronic = true;
								
								for (int e = 0; e < transcript.getExons().length; e++) {
									exon = transcript.getExons()[e];
									
									if (position + baselength >= exon.getStart() - spliceLength	&& position < exon.getEnd() + spliceLength) {
										
										if (current.getExons() == null) current.setExons();											
										
										intronic = false;
										if (!current.getExons().contains(exon)) {
											current.getExons().add(exon);
											if (exon.getStartPhase() > -1 && position + baselength >= exon.getTranscript().getCodingStart()	&& position < exon.getTranscript().getCodingEnd())
												current.coding = true;
											
											break;
										}
									}
								}
								if (intronic) {
									if (current.getTranscripts() == null) current.setTranscripts();
									current.getTranscripts().add(transcript);
								}
							}
							if (!current.isInGene()) {
								if (current.getTranscripts() == null) {
									current.setTranscripts();
									current.getTranscripts().add(prevGene.getTranscripts().get(0));
									current.getTranscripts().add(gene.getTranscripts().get(0));
								}
							}
						
							if (!current.isInGene()) current.varColor = BaseConstants.interColor;
							else if (!current.coding) current.varColor = BaseConstants.intronColor;
							else current.varColor = BaseConstants.redColor;
							
							if (current.getNext() == null) break;

							current = current.getNext();
							position = current.getPosition();
							if (current.indel) {
								position++;
								baselength = MethodLibrary.getBaseLength(current.vars);
							}
						} catch (final Exception e) {							
							e.printStackTrace();
							break;
						}
					}
				}
				if (gene.getEnd() > prevGene.getEnd()) prevGene = gene;
			}
			while (current != null) {
				if (!current.isInGene()) {
					if (current.getTranscripts() == null) {
						current.setTranscripts();
						current.getTranscripts().add(prevGene.getTranscripts().get(0));
						current.getTranscripts().add(prevGene.getTranscripts().get(0));
					}
				}
				current = current.getNext();
			}
			
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
		current = null;
		transcript = null;
		exon = null;
		Setter.getInstance().setUpdateScreen();
	}

	/* public void readLineMulti(final String[] split, final Sample sample) {

		samplecount = 0;
		try {
			pos = Integer.parseInt(split[1]) - 1;
		} catch (final Exception e) {
			return;
		}
		if (pos < Draw.drawVariables.variantsStart) {
			return;
		} else if (pos >= Draw.drawVariables.variantsEnd) {
			stop = true;
			return;
		}
		
		if (linecounter == 0
				|| pos - linecounter > (Draw.drawVariables.variantsEnd - Draw.drawVariables.variantsStart) / 100) {
			MainPane.drawCanvas.loadBarSample = (int) (((pos - Draw.drawVariables.variantsStart)
					/ (double) (Draw.drawVariables.variantsEnd - Draw.drawVariables.variantsStart)) * 100);
			
			linecounter = pos;
		}

		first = true;

		while (current != null && current.getNext() != null && current.getPosition() < pos) {
			current = current.getNext();
		}
		if (current.getPosition() == pos) {
			first = false;

		}

		for (int s = 9; s < split.length; s++) {
			try {
				currentSample = Getter.getInstance().getSampleList().get(sample.getIndex() + 1 + s - 9);
				if (currentSample.removed) {

					continue;
				}

				info = split[s].split(":");
				noref = false;
				final HashMap<String, Integer> infofield = new HashMap<String, Integer>();

				final String[] infos = split[8].split(":");
				for (int i = 0; i < infos.length; i++) {
					infofield.put(infos[i], i);
				}

				if (infofield.containsKey("GT")) {
					gtindex = infofield.get("GT");
					if (info[gtindex].contains(".") || info[gtindex].length() < 3) {
						continue;
					}

					if (info[gtindex].contains("|")) {

						firstallele = Short.parseShort("" + info[gtindex].split("|")[0]);
						secondallele = Short.parseShort("" + info[gtindex].split("|")[2]);
					} else {
						firstallele = Short.parseShort("" + info[gtindex].split("/")[0]);
						secondallele = Short.parseShort("" + info[gtindex].split("/")[1]);
					}

					genotype = firstallele == secondallele;

					if (genotype && firstallele == 0) {
						continue;
					}

					if (infofield.containsKey("AD")) {
						if (infofield.containsKey("RD")) {
							refcalls = Integer.parseInt(info[infofield.get("RD")]);
							altcalls = Integer.parseInt(info[infofield.get("AD")]);
						} else {
							try {
								coverages = info[infofield.get("AD")].split(",");
								calls1 = Integer.parseInt(coverages[firstallele]);
								calls2 = Integer.parseInt(coverages[secondallele]);
							} catch (final Exception e) {
								return;
							}
						}
					} else {
						calls1 = 20;
						calls2 = 20;
					}

					if (!genotype) {
						if (firstallele == 0) {
							refallele = firstallele;
							refcalls = calls1;
							altallele = secondallele;
							altcalls = calls2;
						} else if (secondallele == 0) {
							refallele = secondallele;
							refcalls = calls2;
							altallele = firstallele;
							altcalls = calls1;
						} else {
							noref = true;
							refallele = firstallele;
							refcalls = calls1;
							altallele = secondallele;
							altcalls = calls2;
						}
					} else {

						refcalls = calls1; // (short)(Short.parseShort(coverages[0]));
						altallele = secondallele;
						altcalls = calls2;

					}
				}

				if (!split[4].contains(",")) {
					altbase = getVariant(split[3], split[4]);

				} else if (!noref) {

					altbase = getVariant(split[3], split[4].split(",")[altallele - 1]);

				} else {
					altbase = getVariant(split[3], split[4].split(",")[altallele - 1]);
					altbase2 = getVariant(split[3], split[4].split(",")[refallele - 1]);

				}
				if (altbase.contains("*") || (altbase2 != null && altbase2.contains("*"))) {
					continue;
				}
				quality = null;
				if (split[8].contains("Q")) {
					if (infofield.containsKey("GQ") && !info[infofield.get("GQ")].equals(".")) {
						gq = (float) Double.parseDouble(info[split[8].indexOf("GQ") / 3]);
					} else if (infofield.containsKey("BQ") && !info[infofield.get("BQ")].equals(".")) {
						quality = (float) Double.parseDouble(info[infofield.get("BQ")]);
					} else if (split[7].contains("SSC")) {
						quality = Float.parseFloat(split[7].substring(split[7].indexOf("SSC") + 4).split(";")[0]);
					}

				}
				if (quality == null) {
					if (split[5].matches("\\d+.?.*")) {
						quality = (float) Double.parseDouble(split[5]);

					}
				}

				final HashMap<String, Float> advancedQualities = null;

				if (VariantHandler.freeze.isSelected()) {

					if (refcalls + altcalls < MainPane.projectValues.variantHandlerValues.variantCoverage) {

						continue;
					}

					if (quality != null && quality < MainPane.projectValues.variantQuality) {
						continue;
					}
					if (gq != null && gq < MainPane.projectValues.variantHandlerValues.variantGQ) {
						continue;
					}
					if (altcalls / (double) (refcalls + altcalls) < MainPane.projectValues.variantHandlerValues.variantCalls / 100.0) {

						continue;
					}

					if (VariantHandler.hideSNVs.isSelected() && altbase.length() == 1) {
						continue;
					}

					if (VariantHandler.hideIndels.isSelected() && altbase.length() > 1) {
						continue;
					}

					if (VariantHandler.rscode.isSelected() && !split[2].equals(".")) {
						continue;
					}

				}
				if (split.length > 6) {
					if (checkAdvFilters(split[6], altbase.length() > 1)) {
						return;
					}
				}
				if (split.length > 7) {
					advqual = checkAdvQuals(split[7], infofield, info, altbase.length() > 1);
					if (advqual == 2) {
						return;
					}
					if (advqual == 1) {
						continue;
					}
				}
				if (refcalls + altcalls > VariantHandler.maxCoverageSlider.getMaximum()) {
					VariantHandler.maxCoverageSlider.setMaximum(refcalls + altcalls);
					VariantHandler.maxCoverageSlider.setValue(refcalls + altcalls);
				}
				
				if (first && current.getNext() == null) {

					if (!split[2].equals(".")) {
						current.putNext(new VarNode(pos, (byte) split[3].charAt(0), altbase, refcalls + altcalls,
								altcalls, genotype, quality, gq, advancedQualities, split[2], currentSample, current,
								null));
					} else {
						current.putNext(
								new VarNode(pos, (byte) split[3].charAt(0), altbase, refcalls + altcalls, altcalls,
										genotype, quality, gq, advancedQualities, null, currentSample, current, null));
					}
					if (noref) {
						if (split.length > 6) {
							if (checkAdvFilters(split[6], altbase2.length() > 1)) {
								return;
							}
						}
						if (split.length > 7) {
							advqual = checkAdvQuals(split[7], infofield, info, altbase2.length() > 1);
							if (advqual == 1) {
								current = current.getNext();
								first = false;
								continue;
							}
							if (advqual == 2) {
								return;
							}
						}

						current.getNext().addSample(altbase2, refcalls + altcalls, refcalls, genotype, quality, gq,
								advancedQualities, currentSample);
					}

					current = current.getNext();
					first = false;

				} else if (pos == current.getPosition()) {

					current.addSample(altbase, refcalls + altcalls, altcalls, genotype, quality, gq, advancedQualities,
							currentSample);
					if (noref) {
						if (split.length > 6) {
							if (checkAdvFilters(split[6], altbase2.length() > 1)) {
								return;
							}
						}
						if (split.length > 7) {
							advqual = checkAdvQuals(split[7], infofield, info, altbase2.length() > 1);
							if (advqual == 1) {

								continue;
							}
							if (advqual == 2) {
								return;
							}
						}

						current.addSample(altbase2, refcalls + altcalls, refcalls, genotype, quality, gq,
								advancedQualities, currentSample);
					}
					if (current.isRscode() == null && !split[2].equals(".")) {
						current.setRscode(split[2]);
					}

				}

				else if (current.getPosition() > pos) {

					if (!split[2].equals(".")) {
						current.getPrev()
								.putNext(new VarNode(pos, (byte) split[3].charAt(0), altbase, refcalls + altcalls,
										altcalls, genotype, quality, gq, advancedQualities, split[2], currentSample,
										current.getPrev(), current));
					} else {
						current.getPrev()
								.putNext(new VarNode(pos, (byte) split[3].charAt(0), altbase, refcalls + altcalls,
										altcalls, genotype, quality, gq, advancedQualities, null, currentSample,
										current.getPrev(), current));

					}
					if (noref) {
						if (split.length > 6) {
							if (checkAdvFilters(split[6], altbase2.length() > 1)) {
								return;
							}
							if (split.length > 7) {

								advqual = checkAdvQuals(split[7], infofield, info, altbase2.length() > 1);
								if (advqual == 1) {

									continue;
								}
								if (advqual == 2) {
									return;
								}
							}
						}

						current.getPrev().addSample(altbase2, refcalls + altcalls, refcalls, genotype, quality, gq,
								advancedQualities, currentSample);
					}
					current.putPrev(current.getPrev().getNext());
					current = current.getPrev();

				}

			} catch (final Exception ex) {
				ErrorLog.addError(ex.getStackTrace());
				ex.printStackTrace();
				for (int i = 0; i < split.length; i++) {
					System.out.print(split[i] + " ");
				}
				System.out.println();
				break;
			}

			if (first) {
				first = false;
			}
		}

	} */

	
	


	/*
	 * File getListFile(File listfile, String chrom) {
	 * 
	 * try { if(!listfile.exists()) { return new
	 * File(listfile.getCanonicalPath().replace(".list", ".cram")); } BufferedReader
	 * reader = new BufferedReader(new FileReader(listfile)); String mnt = "/mnt";
	 * if(!listfile.getAbsolutePath().startsWith("/mnt")) {
	 * if(listfile.getAbsolutePath().indexOf("\\cg") < 0) {
	 * if(listfile.getAbsolutePath().indexOf("/cg") > -1) { mnt =
	 * listfile.getAbsolutePath().substring(0,listfile.getAbsolutePath().indexOf(
	 * "/cg")); } else { mnt = "X:"; } } else { mnt =
	 * listfile.getAbsolutePath().substring(0,listfile.getAbsolutePath().indexOf(
	 * "\\cg")); } } String listline;
	 * 
	 * while((listline = reader.readLine()) != null) { if(MenuBar.selectedChrom > 24) {
	 * if(listline.contains("_" +"GL" +".")) {
	 * 
	 * reader.close(); return new File(listline.replace("/mnt", mnt)); } } else
	 * if(listline.contains("_" +chrom +".")) { reader.close(); return new
	 * File(listline.replace("/mnt", mnt)); } } reader.close(); } catch(Exception e)
	 * { ErrorLog.addError(e.getStackTrace()); e.printStackTrace(); } return null; }
	 */

	

	void addToSequence(final SplitClass split, final StringBuffer buffer, final int length) {

		/*
		 * if(length < 0) {
		 * 
		 * buffer.insert(0, MainPane.chromDraw.getSeq(split.chrom,
		 * (int)(split.readSeqStart-length)-1, split.readSeqStart, MainPane.referenceFile));
		 * split.readSeqStart = (int)(split.readSeqStart-length)-1;
		 * 
		 * if(split.readSeqStart < 0) { split.readSeqStart = 0; } readSeqStart =
		 * split.readSeqStart; } else {
		 * split.readSequence.append(MainPane.chromDraw.getSeq(split.chrom,
		 * split.readSeqStart+split.readSequence.length(),
		 * (int)(split.readSeqStart+split.readSequence.length()+length+200),
		 * MainPane.referenceFile));
		 * 
		 * }
		 */
	}

	public static Iterator<SAMRecord> getBamIterator(final Reads readClass, final String chrom, final int startpos, final int endpos) {

		try {
			if(samFileReader != null) samFileReader.close();			
			if(CRAMReader != null) CRAMReader.close();			
			if(readClass.sample.samFile == null) return null; 
			
			if (readClass.sample.CRAM) {
				CRAMReader = new CRAMFileReader(readClass.sample.samFile,	new File(readClass.sample.samFile.getCanonicalFile() + ".crai"), new ReferenceSource(MainPane.ref),ValidationStringency.SILENT);
				if (CRAMReader != null && !CRAMReader.hasIndex()) {
					MainPane.showError("Index file is missing (.crai) for " + readClass.sample.samFile.getName(), "Note");
					return null;
				}
			} else {
				try {
					samFileReader = SamReaderFactory.make().validationStringency(ValidationStringency.SILENT).open(readClass.sample.samFile);					
					// samFileReader = new SAMFileReader(readClass.sample.samFile);
					if (samFileReader != null && !samFileReader.hasIndex()) {
						MainPane.showError("Index file is missing (.bai) for " + readClass.sample.samFile.getName(),"Note");
						return null;
					}
				} catch (final Exception e) {
					//MainPane.showError(e.getMessage(), "Note");
					e.printStackTrace();
				}
			}

		} catch (final Exception e) {
			if (readClass.sample.CRAM) {
				if (CRAMReader != null && !CRAMReader.hasIndex()) {
					MainPane.showError("Index file is missing (.crai) for " + readClass.sample.samFile.getName(), "Note");
					return null;
				}
			} else {
				if (samFileReader != null && !samFileReader.hasIndex()) {
					MainPane.showError("Index file is missing (.bai) for " + readClass.sample.samFile.getName(), "Note");
					return null;
				}
			}
			e.printStackTrace();
		}

		try {
			if (readClass.sample.CRAM) {
				if (!readClass.sample.chrSet) {

					if (CRAMReader.getFileHeader().getSequence(0).getSAMString().contains("chr")) {
						readClass.sample.chr = "chr";
					}
					readClass.sample.chrSet = true;
				}
				
				final QueryInterval[] interval = { new QueryInterval(CRAMReader.getFileHeader().getSequence(readClass.sample.chr + chrom).getSequenceIndex(), startpos, endpos) };
				
				final Iterator<SAMRecord> value = CRAMReader.query(interval, false);

				return value;
			} else {
				Iterator<SAMRecord> value = null;
				// SAMReadGroupRecord record = new SAMReadGroupRecord("fixed_My6090N_sorted");
				// samFileReader.getFileHeader().addReadGroup(record);
				try {
					// SAMFileReader.setDefaultValidationStringency(ValidationStringency.SILENT);
					if (!readClass.sample.chrSet) {
						if (samFileReader.getFileHeader().getSequence(0).getSAMString().contains("chr")) {
							readClass.sample.chr = "chr";
						}
						readClass.sample.chrSet = true;
					}

					value = samFileReader.queryOverlapping(readClass.sample.chr + chrom, startpos, endpos);

				} catch (final htsjdk.samtools.SAMFormatException e) {
					e.printStackTrace();
				}

				return value;
			}
		} catch (final Exception e) {

			try {

				if (readClass.sample.CRAM) {
					e.printStackTrace();
					/*
					 * if(CRAMReader.getFileHeader().getSequence(readClass.sample.chr + "M") !=
					 * null) { QueryInterval[] interval = { new
					 * QueryInterval(CRAMReader.getFileHeader().getSequence(readClass.sample.chr +
					 * "M").getSequenceIndex(), startpos, endpos) };
					 * 
					 * }
					 * 
					 * Iterator<SAMRecord> value = CRAMReader.query(interval, false);
					 * 
					 * if(!readClass.sample.chrSet) { if(!value.hasNext()) { QueryInterval[]
					 * interval2 = { new
					 * QueryInterval(CRAMReader.getFileHeader().getSequence("chrM").getSequenceIndex
					 * (), startpos, endpos) };
					 * 
					 * value = CRAMReader.query(interval2, false); if(value.hasNext()) {
					 * readClass.sample.chr = "chr"; readClass.sample.chrSet = true; } } else {
					 * readClass.sample.chrSet = true; } }
					 */
					return null;
					// return value;
				} else {
					return null;
					/*
					 * Iterator<SAMRecord> value =
					 * samFileReader.queryOverlapping(readClass.sample.chr+"M", startpos, endpos);
					 * if(!readClass.sample.chrSet) { if(!value.hasNext()) { value =
					 * samFileReader.queryOverlapping("chrM", startpos, endpos); if(value.hasNext())
					 * { readClass.sample.chr = "chr"; readClass.sample.chrSet = true; } } else {
					 * readClass.sample.chrSet = true; } }
					 */
					// return value;
				}

			} catch (final Exception ex) {

				ex.printStackTrace();
				ErrorLog.addError(e.getStackTrace());
				return null;
			}
		}
	}

	public SAMRecord getRead(final String chrom, final int startpos, final int endpos, final String name,
			final Reads readClass) {
		if (cancelreadread) {
			return null;
		}
		final Iterator<SAMRecord> bamIterator = getBamIterator(readClass, chrom, startpos - 1, endpos);
		SAMRecord samRecord = null;

		while (bamIterator != null && bamIterator.hasNext()) {

			try {
				samRecord = bamIterator.next();
				if (samRecord.getUnclippedStart() < startpos) {
					continue;
				}
				if (samRecord.getUnclippedStart() == startpos && samRecord.getReadName().equals(name)) {

					return samRecord;
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	void setReadArrays(final Reads readClass, final int startpos, final int endpos) {
		readClass.setCoverageStart(startpos);
		final double[][] coverages = new double[endpos - startpos][8];
		readClass.setCoverageEnd(startpos + coverages.length);
		readClass.setCoverages(coverages);
		readClass.setMaxcoverage(1);
	}

	public ArrayList<ReadNode> getReads(final String chrom, final int startpos, final int endpos, final Reads readClass, final SplitClass split) {
		
		try {
			
			if (endpos - startpos > Settings.settings.get("coverageDrawDistance") || endpos - startpos < 1) return null;

			double[][] coverageArray = null;
			if (Draw.drawVariables.sampleHeight > Settings.coverageWindowHeight) {
				bamIterator = getBamIterator(readClass, chrom, startpos, endpos);
				if (viewLength > Settings.readDrawDistance) {
					final double[][] coverages = new double[(int) Main.frame.getWidth()][8];
					readClass.setCoverages(coverages);
					readClass.setMaxcoverage(1);
					readClass.setCoverageStart(startpos);
					oldstart = endpos;
					readClass.setCoverageEnd(endpos);
					firstCov = false;
					coverageArray = readClass.getCoverages();
				}

				boolean firstRead = true;
				int firststart = 0, laststart = 0;
				
				while (bamIterator != null && bamIterator.hasNext()) {

					try {

						if (cancelreadread || !Getter.getInstance().loading() || readClass.sample.getIndex() < Draw.drawVariables.getVisibleStart.get()	|| readClass.sample.getIndex() > Draw.drawVariables.getVisibleStart.get()	+ Draw.drawVariables.getVisibleSamples.get()) {
							return null;
						}
						try {

							samRecord = bamIterator.next();
						} catch (final htsjdk.samtools.SAMFormatException ex) {
							ex.printStackTrace();
							continue;
						}
						
						if (samRecord.getReadUnmappedFlag() && !Settings.showUnmapped.isSelected()) continue;						
						
						if (Draw.variantcalculator) {
							if (readClass.getHeadAndTail().size() > Settings.readDepthLimit) {
								break;
							}
						}

						if (samRecord.getUnclippedStart() > endpos) {
							MainPane.drawCanvas.loadBarSample = 0;
							MainPane.drawCanvas.loadbarAll = 0;
							break;
						}
						
						if (readClass.getReadStart() != Integer.MAX_VALUE	&& readClass.searchstart != Integer.MAX_VALUE) {

							if (readClass.searchstart > startpos && samRecord.getUnclippedEnd() >= readClass.searchstart	&& samRecord.getUnclippedStart() < readClass.searchend) continue;
							if (readClass.searchstart < startpos	&& samRecord.getUnclippedStart() < readClass.searchend) continue;
							
							/*
							 * if( readClass.getReadEnd() < split.end) { if(samRecord.getUnclippedStart() >=
							 * readClass.getReadStart() && samRecord.getUnclippedStart() <=
							 * readClass.getReadEnd()) { continue; } }
							 */
						}
						int readEnd = samRecord.getReadUnmappedFlag() ? samRecord.getUnclippedStart() + samRecord.getReadLength() : samRecord.getUnclippedEnd();
						if (readEnd < startpos) continue;						
						
						if (readClass.sample.getComplete() == null) {
							if (samRecord.getReadName().startsWith("GS")) readClass.sample.setcomplete(true);
							else readClass.sample.setcomplete(false);							
						}
						final boolean isDiscordant = MethodLibrary.isDiscordant(samRecord, readClass.sample.getComplete());
						
						if (Settings.hideDiscordant.isSelected() && isDiscordant) continue;						
						if (Settings.showDiscordant.isSelected() && !isDiscordant) continue;						

						if (viewLength < Settings.readDrawDistance) {
							if (firstRead || readClass.sample.longestRead < samRecord.getCigar().getReferenceLength()) 
								readClass.sample.longestRead = samRecord.getCigar().getReferenceLength();
							
							/*
							 * if(samRecord.getUnclippedEnd() > splitIndex.end +
							 * readClass.sample.longestRead) { endpos = samRecord.getUnclippedEnd(); } else
							 * { endpos = (int)splitIndex.end + readClass.sample.longestRead; }
							 */

							if (firstRead) {
								if (readClass.getReadStart() > samRecord.getUnclippedStart()) 
									firststart = samRecord.getUnclippedStart();								

								if (readClass.getCoverageStart() == Integer.MAX_VALUE) {
									
									setReadArrays(readClass, firststart, endpos + readClass.sample.longestRead);
								}

								coverageArray = readClass.getCoverages();
								firstRead = false;

								// break;
							}
							if (readClass.getReadEnd() < samRecord.getUnclippedStart()) {
								laststart = samRecord.getUnclippedStart();
							}
						
							/*
							 * if(samRecord.getUnclippedEnd() > readClass.getCoverageEnd()-1) {
							 * 
							 * double[][] coverages = new double[(int)(readClass.getCoverages().length
							 * +(samRecord.getUnclippedEnd()-samRecord.getUnclippedStart() +
							 * (int)splitIndex.viewLength))][8];
							 * 
							 * //pointer = coverages.length-1; for(int i =0;
							 * i<readClass.getCoverages().length; i++) {
							 * 
							 * coverages[i][0] = readClass.getCoverages()[i][0]; coverages[i][1] =
							 * readClass.getCoverages()[i][1]; coverages[i][2] =
							 * readClass.getCoverages()[i][2]; coverages[i][3] =
							 * readClass.getCoverages()[i][3]; coverages[i][4] =
							 * readClass.getCoverages()[i][4]; coverages[i][5] =
							 * readClass.getCoverages()[i][5]; coverages[i][6] =
							 * readClass.getCoverages()[i][6]; coverages[i][7] =
							 * readClass.getCoverages()[i][7]; // pointer--;
							 * 
							 * } readClass.setCoverageEnd(readClass.getCoverageStart()+coverages.length);
							 * readClass.setCoverages(coverages); coverageArray = readClass.getCoverages();
							 * 
							 * }
							 */
						} else {

							if (MainPane.drawCanvas.loadBarSample != (int) (((samRecord.getUnclippedStart() - startpos)	/ (double) (oldstart - startpos)) * 100)) {
								MainPane.drawCanvas.loadBarSample = (int) (((samRecord.getUnclippedStart() - startpos) / (double) (oldstart - startpos)) * 100);
								MainPane.drawCanvas.loadbarAll = (int) (((samRecord.getUnclippedStart() - startpos)	/ (double) (oldstart - startpos)) * 100);
							}
							int readstart = samRecord.getAlignmentStart();
							for (int i = 0; i < (int) (samRecord.getReadLength() * splitIndex.pixel + 1); i++) {
								if ((readstart - start) * splitIndex.pixel + i < 0) continue;								
								if ((readstart - start) * splitIndex.pixel	+ i > coverageArray.length - 1) break;
								if (samRecord.getReadLength() * splitIndex.pixel >= 1) {
									coverageArray[(int) ((readstart - start) * splitIndex.pixel	+ i)][0] += (1 / splitIndex.pixel);
								} else {
									coverageArray[(int) ((readstart - start) * splitIndex.pixel	+ i)][0] += samRecord.getReadLength();
								}
								if (coverageArray[(int) ((readstart - start) * splitIndex.pixel	+ i)][0]/* pixel */ > readClass.getMaxcoverage()) {
									readClass.setMaxcoverage((coverageArray[(int) ((readstart - start)	* splitIndex.pixel + i)][0]/* pixel */));
								}
							}
							continue;
						}
					
						java.util.ArrayList<java.util.Map.Entry<Integer, Byte>> mismatches = null;

						if (MainPane.drawCanvas.loadBarSample != (int) (((samRecord.getUnclippedStart() - startpos)	/ (double) (endpos - startpos)) * 100)) {
							MainPane.drawCanvas.loadBarSample = (int) (((samRecord.getUnclippedStart() - startpos)	/ (double) (endpos - startpos)) * 100);
							MainPane.drawCanvas.loadbarAll = (int) (((samRecord.getUnclippedStart() - startpos)	/ (double) (endpos - startpos)) * 100);
						}
						/*
						 * if(right && readClass.getLastRead() != null && (samRecord.getUnclippedStart()
						 * <= startpos)) { continue; }
						 */

						/*
						 * if(right && samRecord.getUnclippedStart() > endpos) { right = false; }
						 */
						try {
							//if(samRecord.getReadUnmappedFlag()) System.out.println(samRecord.getReadLength()); 
							if (coverageArray == null) {
								return null;
							}
							if (readClass.getCoverageEnd() < endpos) {
								coverageArray = coverageArrayAdd((int) ((endpos - readClass.getCoverageEnd()	+ (int) readClass.sample.longestRead + 100)), coverageArray, readClass);
							}
							if (samRecord.getAlignmentEnd() >= readClass.getCoverageEnd()) {

								coverageArray = coverageArrayAdd((int) (((samRecord.getAlignmentEnd() - samRecord.getUnclippedStart())	+ (int) readClass.sample.longestRead + 100)),	coverageArray, readClass);
							}
							if (samRecord.getUnclippedStart() < readClass.getCoverageStart()) {

								coverageArray = coverageArrayAddStart(samRecord.getUnclippedStart(),	readClass.getCoverageEnd(), coverageArray, readClass);
							}
							if (coverageArray == null) {

								setReadArrays(readClass, firststart, endpos + readClass.sample.longestRead);
								coverageArray = readClass.getCoverages();
							}
							
							mismatches = getMismatches(samRecord, readClass, coverageArray, split);
						} catch (final Exception e) {
							e.printStackTrace();
						}
						/*
						 * if(left) {
						 * 
						 * if(readClass.getFirstRead().getPosition() > samRecord.getUnclippedStart()) {
						 * 
						 * ReadNode addNode = new ReadNode(samRecord, readClass.sample.getComplete(),
						 * chrom, readClass.sample, splitIndex,readClass, mismatches);
						 * readClass.setFirstRead(addNode); startY =
						 * (int)(((readClass.sample.getIndex()+1)*Draw.drawVariables.
						 * sampleHeight-MainPane.drawCanvas.bottom-(BaseVariables.readHeight+2)));
						 * addNode.setPrev(null);
						 * addNode.setNext(readClass.getHeadAndTail().get(0)[headnode]);
						 * readClass.getHeadAndTail().get(0)[headnode].setPrev(addNode); lastAdded =
						 * addNode; } else { ReadNode addNode = new ReadNode(samRecord,
						 * readClass.sample.getComplete(), chrom, readClass.sample,
						 * splitIndex,readClass, mismatches); startY =
						 * (int)(((readClass.sample.getIndex()+1)*Draw.drawVariables.
						 * sampleHeight-MainPane.drawCanvas.bottom-(BaseVariables.readHeight+2)));
						 * addNode.setPrev(lastAdded); addNode.setNext(lastAdded.getNext());
						 * lastAdded.setNext(addNode); lastAdded = addNode;
						 * readClass.getHeadAndTail().get(0)[headnode].setPrev(addNode);
						 * addNode.setNext(readClass.getHeadAndTail().get(0)[headnode]); } } else {
						 */
						try {							
							if (readClass.getReads().isEmpty() || samRecord.getUnclippedStart() > readClass.getReadEnd()) {
								readSam(chrom, readClass, samRecord, mismatches, 0, isDiscordant);
							} else {
								readSamLeft(chrom, readClass, samRecord, mismatches, isDiscordant);
							}
						} catch (final Exception e) {
							System.out.println(samRecord + " " + currentread);
							e.printStackTrace();
							break;
						}
						// }
						// }
						// }
						/*
						 * else { continue; }
						 */
					} catch (final Exception e) {
						ErrorLog.addError(e.getStackTrace());
						e.printStackTrace();
						break;
					}
				}
				if (viewLength < Settings.readDrawDistance) {
					if (readClass.searchstart > startpos) {
						readClass.searchstart = startpos; // splitIndex.setMinReadStart(startpos);
					}
					if (readClass.searchend < endpos) {
						readClass.searchend = endpos;
					}
					if (firststart > 0 && readClass.getReadStart() > firststart) {
						readClass.setReadStart(firststart);
					}
					if (readClass.getReadEnd() < laststart) {
						readClass.setReadEnd(laststart);
					}
				}

				/*
				 * if(left) {
				 * 
				 * MainPane.drawCanvas.loadBarSample = 0; MainPane.drawCanvas.loadbarAll =0;
				 * setLevels(lastAdded, readClass.sample, readClass); lastAdded = null;
				 * 
				 * }
				 */
			}
		} catch (final Exception ex) {
			ErrorLog.addError(ex.getStackTrace());
			ex.printStackTrace();
			return null;
		}
		return null;
	}

	static double[][] coverageArrayAdd(final int addlength, final double[][] coverageArray, final Reads readClass) {

		final double[][] coverages = new double[(int) (coverageArray.length + (addlength) + 1000)][8];

		// pointer = coverages.length-1;
		for (int i = 0; i < coverageArray.length; i++) {

			coverages[i][0] = coverageArray[i][0];
			coverages[i][1] = coverageArray[i][1];
			coverages[i][2] = coverageArray[i][2];
			coverages[i][3] = coverageArray[i][3];
			coverages[i][4] = coverageArray[i][4];
			coverages[i][5] = coverageArray[i][5];
			coverages[i][6] = coverageArray[i][6];
			coverages[i][7] = coverageArray[i][7];

		}
		MainPane.drawCanvas.loadbarAll = 0;
		MainPane.drawCanvas.loadBarSample = 0;
		readClass.setCoverageEnd(readClass.getCoverageStart() + coverages.length);
		readClass.setCoverages(coverages);
		return coverages;

	}

	static double[][] coverageArrayAddStart(final int addpos, final int endpos, final double[][] coverageArray,
			final Reads readClass) {
		final int length = endpos - addpos;
		if (length < 0) {
			return null;
		}
		final double[][] coverages = new double[length][8];

		int pointer = coverages.length - 1;

		for (int i = coverageArray.length - 1; i >= 0; i--) {

			coverages[pointer][0] = coverageArray[i][0];
			coverages[pointer][1] = coverageArray[i][1];
			coverages[pointer][2] = coverageArray[i][2];
			coverages[pointer][3] = coverageArray[i][3];
			coverages[pointer][4] = coverageArray[i][4];
			coverages[pointer][5] = coverageArray[i][5];
			coverages[pointer][6] = coverageArray[i][6];
			coverages[pointer][7] = coverageArray[i][7];
			pointer--;

		}
		MainPane.drawCanvas.loadbarAll = 0;
		MainPane.drawCanvas.loadBarSample = 0;
		readClass.setCoverageStart(addpos);
		readClass.setCoverages(coverages);
		return coverages;

	}

	public static VariantCall[][] coverageArrayAdd(final int addlength, final VariantCall[][] coverageArray, final Reads readClass) {

		final VariantCall[][] coverages = new VariantCall[(int) (coverageArray.length + (addlength) + 1000)][8];

		// pointer = coverages.length-1;
		for (int i = 0; i < coverageArray.length; i++) {

			coverages[i][0] = coverageArray[i][0];
			coverages[i][1] = coverageArray[i][1];
			coverages[i][2] = coverageArray[i][2];
			coverages[i][3] = coverageArray[i][3];
			coverages[i][4] = coverageArray[i][4];
			coverages[i][5] = coverageArray[i][5];
			coverages[i][6] = coverageArray[i][6];
			coverages[i][7] = coverageArray[i][7];

		}
		// MainPane.drawCanvas.loadbarAll = 0;
		// MainPane.drawCanvas.loadBarSample= 0;
		readClass.setCoverageEnd(readClass.getCoverageStart() + coverages.length);
		// readClass.setCoverages(coverages);
		return coverages;

	}

	java.util.ArrayList<java.util.Map.Entry<Integer, Byte>> getMismatches(final SAMRecord samRecord,
			final Reads readClass, final double[][] coverageArray, final SplitClass split) {

		if (readClass == null) {
			sample.resetreadHash();
		}
		java.util.ArrayList<java.util.Map.Entry<Integer, Byte>> mismatches = null;
		final String MD = samRecord.getStringAttribute("MD");
		final String SA = samRecord.getStringAttribute("SA");
		if (samRecord.getReadBases().length == 0) {
			return null;
		}
		try {
			
			if (MD == null) {

				if (readClass.sample.MD) {
					readClass.sample.MD = false;
				}
			}

			if ((readClass.sample.MD || MD != null) && (samRecord.getCigarString().contains("S") && Settings.softClips == 0)/*
												 * && ((!samRecord.getCigarString().contains("S") && SA == null) || SA
												 * !=null)
												 */) {

				if (!readClass.sample.MD) {
					readClass.sample.MD = true;
				}

				java.util.ArrayList<java.util.Map.Entry<Integer, Integer>> insertions = null;
				int softstart = 0;
				if (samRecord.getCigarLength() > 1) {

					readstart = samRecord.getUnclippedStart();
					if (readClass.getCoverageStart() > readstart) {
						return null;
					}
					readpos = 0;
					for (int i = 0; i < samRecord.getCigarLength(); i++) {
						element = samRecord.getCigar().getCigarElement(i);
						if (element.getOperator().compareTo(CigarOperator.MATCH_OR_MISMATCH) == 0) {
							for (int r = readpos; r < readpos + element.getLength(); r++) {
								try {
									coverageArray[((readstart + r) - readClass.getCoverageStart())][0]++;
								} catch (final Exception e) {
									// TODO error
								}
								try {
									if (coverageArray[((readstart + r) - readClass.getCoverageStart())][0] > readClass.getMaxcoverage()) {
										readClass.setMaxcoverage(coverageArray[((readstart + r) - readClass.getCoverageStart())][0]);

									}
								} catch (final Exception e) {
									// TODO error
								}
								mispos++;
							}

							readpos += element.getLength();
						} else if (element.getOperator().compareTo(CigarOperator.DELETION) == 0) {
							for (int d = 0; d < element.getLength(); d++) {

								readClass.getCoverages()[(readstart + readpos + d) - readClass.getCoverageStart()][MainPane.baseMap.get((byte) 'D')]++;
							}
							readpos += element.getLength();
						} else if (element.getOperator().compareTo(CigarOperator.INSERTION) == 0) {
							if (insertions == null) {
								insertions = new java.util.ArrayList<java.util.Map.Entry<Integer, Integer>>();
							}
							final java.util.Map.Entry<Integer, Integer> ins = new java.util.AbstractMap.SimpleEntry<>(readpos, element.getLength());
							insertions.add(ins);
							readClass.getCoverages()[(readstart + readpos) - readClass.getCoverageStart()][MainPane.baseMap.get((byte) 'I')]++;
							mispos += element.getLength();

						} else if (element.getOperator().compareTo(CigarOperator.SOFT_CLIP) == 0) {
							if (i == 0 && SA == null) {
								softstart = element.getLength();
							}

							if (Settings.softClips == 1) {
								for (int r = readpos; r < readpos + element.getLength(); r++) {
									if (SA == null) {
										coverageArray[((readstart + r) - readClass.getCoverageStart())][0]++;
									}
									mispos++;
								}
								readpos += element.getLength();

							} else {
								if (i == 0) {
									readstart = samRecord.getAlignmentStart();
									mispos += element.getLength();
								}
							}

						} else if (element.getOperator().compareTo(CigarOperator.HARD_CLIP) == 0) {
							if (i == 0) {
								readstart = samRecord.getAlignmentStart();
							}
						} else if (element.getOperator().compareTo(CigarOperator.SKIPPED_REGION) == 0) {
							readpos += element.getLength();
						}
					}
				} else {
					readstart = samRecord.getUnclippedStart();

					for (int i = 0; i < samRecord.getReadLength(); i++) {
						try {
							if (readClass.getCoverageStart() > readstart) {
								break;
							}
							coverageArray[(readstart + i) - readClass.getCoverageStart()][0]++;

							if (coverageArray[(samRecord.getUnclippedStart() - readClass.getCoverageStart()) + i][0] > readClass.getMaxcoverage()) {
								readClass.setMaxcoverage(coverageArray[(samRecord.getUnclippedStart() - readClass.getCoverageStart()) + i][0]);
							}
						} catch (final Exception e) {
							ErrorLog.addError(e.getStackTrace());
							e.printStackTrace();
							FileRead.cancelreadread = true;
							break;
						}
					}
				}
				
				if (MD != null) {
					try {
						Integer.parseInt(MD);
						return null;
					} catch (final Exception ex) {

						readstart = samRecord.getAlignmentStart() - softstart;
						
						final char[] chars = MD.toCharArray();
						final String bases = samRecord.getReadString();
						final String qualities = samRecord.getBaseQualityString();

						if (SA == null) {
							softstart = 0;
						}

						int readpos = softstart;
						int mispos = softstart;

						int add = 0;
						int digitpointer = 0, insertionpointer = 0;
						boolean first = true;

						for (int i = 0; i < chars.length; i++) {
							try {
								if (insertions != null) {
									if (insertionpointer < insertions.size()
											&& insertions.get(insertionpointer).getKey() <= readpos) {
										while (insertionpointer < insertions.size()
												&& insertions.get(insertionpointer).getKey() <= readpos) {
											mispos += insertions.get(insertionpointer).getValue();
											insertionpointer++;
										}
									}
								}
								if (Character.isDigit(chars[i])) {
									digitpointer = i + 1;
									while (digitpointer < chars.length && Character.isDigit(chars[digitpointer])) {
										digitpointer++;
									}

									if (digitpointer == chars.length) {

										if ((mispos + Integer.parseInt(MD.substring(i, digitpointer))) > samRecord
												.getReadLength()) {
											if (!first) {
												break;
											}
											first = false;
											readpos = 0;
											mispos = 0;
											i = -1;
											digitpointer = 0;
											insertionpointer = 0;
											mismatches.clear();
											continue;
										}

										break;
									} else {
										add = Integer.parseInt(MD.substring(i, digitpointer));
										readpos += add;
										mispos += add;
									}
									i = digitpointer - 1;
								} else if (chars[i] != '^') {

									if (mismatches == null) {
										mismatches = new java.util.ArrayList<java.util.Map.Entry<Integer, Byte>>();
									}
									
									readClass.getCoverages()[(readstart + readpos)	- readClass.getCoverageStart()][MainPane.baseMap.get((byte) bases.charAt(mispos))]++;

									if (samRecord.getBaseQualityString().length() != 1 && (int) qualities.charAt(mispos) - readClass.sample.phred < Settings.baseQ) {

										final java.util.Map.Entry<Integer, Byte> mismatch = new java.util.AbstractMap.SimpleEntry<>(readpos, (byte) Character.toLowerCase(bases.charAt(mispos)));
										mismatches.add(mismatch);
									} else {

										final java.util.Map.Entry<Integer, Byte> mismatch = new java.util.AbstractMap.SimpleEntry<>(readpos, (byte) bases.charAt(mispos));
										mismatches.add(mismatch);
									}
									mispos++;
									readpos++;
								} else {

									digitpointer = i + 1;

									while (!Character.isDigit(chars[digitpointer])) {
										digitpointer++;
										readpos++;
									}

									i = digitpointer - 1;
								}
							} catch (final Exception exc) {
								if (!first) {
									break;
								}
								first = false;
								readpos = 0;
								mispos = 0;
								i = -1;
								digitpointer = 0;
								insertionpointer = 0;
								mismatches.clear();
							}
						}
					}
				}
			} else {
				/*
				 * if(split.getReference() == null) {
				 * 
				 * readClass.setReference(new ReferenceSeq(splitIndex.chrom,start-500, end+500,
				 * MainPane.referenceFile)); }
				 */
				// timecounter = 0;
				/*
				 * while(splitIndex.getReadReference() == null) { Thread.sleep(100);
				 * timecounter++; if(timecounter > 20) { break; } } Thread.sleep(0);
				 */
				// if(splitIndex.getReadReference() == null) {
				// splitIndex.setReference(new
				// ReferenceSeq(splitIndex.chrom,startpos-searchwindow-1, endpos+searchwindow
				// +200, MainPane.referenceFile));
				// return null;
				// }
				
				if (samRecord.getCigarLength() > 1) {
					readstart = samRecord.getUnclippedStart();
					readpos = 0;
					mispos = 0;
					/*
					 * if(readClass.getCoverageStart()>readstart) { return null; }
					 */
					if (mismatches == null) {
						mismatches = new java.util.ArrayList<java.util.Map.Entry<Integer, Byte>>();
					}

					for (int i = 0; i < samRecord.getCigarLength(); i++) {
						element = samRecord.getCigar().getCigarElement(i);
						if (element.getOperator().compareTo(CigarOperator.MATCH_OR_MISMATCH) == 0) {
							for (int r = readpos; r < readpos + element.getLength(); r++) {
								if (((readstart + r) - split.getReadReference().getStartPos() - 1) > split.getReadReference().getSeq().length - 1) {
									split.getReadReference().append(samRecord.getUnclippedEnd() + 1000);
								}
								if (((readstart + r) - split.getReadReference().getStartPos() - 1) < 0) {
									split.getReadReference().appendToStart(samRecord.getUnclippedStart() - 1000);
								}
								try {
									Byte readMismatch = samRecord.getReadBases()[mispos];
									Byte refByte = split.getReadReference().getSeq()[((readstart + r) - split.getReadReference().getStartPos() - 1)];
									Byte refByteUppercase = (byte)(refByte - 32);
									
									if (readMismatch != refByte && readMismatch != refByteUppercase) {
										
										readClass.getCoverages()[(readstart + r) - readClass.getCoverageStart()][MainPane.baseMap.get((byte) samRecord.getReadBases()[mispos])]++;

										if (samRecord.getBaseQualityString().length() != 1 && (int) samRecord.getBaseQualityString().charAt(mispos) - readClass.sample.phred < Settings.baseQ) {

											final java.util.Map.Entry<Integer, Byte> mismatch = new java.util.AbstractMap.SimpleEntry<>(r, (byte) Character.toLowerCase((char) samRecord.getReadBases()[mispos]));
											mismatches.add(mismatch);
										} else {
											final java.util.Map.Entry<Integer, Byte> mismatch = new java.util.AbstractMap.SimpleEntry<>(r, samRecord.getReadBases()[mispos]);
											mismatches.add(mismatch);
										}
									}
								} catch (final Exception e) {
									System.out.println(samRecord.getReadBases().length +" ju");
									e.printStackTrace();
								}
								try {
									int covIndex = ((readstart + r) - readClass.getCoverageStart());
									if(covIndex < 0) continue;
									if(covIndex > coverageArray.length - 1) break;
									coverageArray[covIndex][0]++;

									if (coverageArray[covIndex][0] > readClass.getMaxcoverage()) {
										readClass.setMaxcoverage(coverageArray[covIndex][0]);
									}
								} catch (final Exception e) {
									// TODO error
									e.printStackTrace();
								}
								mispos++;
							}

							readpos += element.getLength();
						} else if (element.getOperator().compareTo(CigarOperator.DELETION) == 0) {
							// readWidth+=element.getLength();
							for (int d = 0; d < element.getLength(); d++) {
								int covIndex = (readstart + readpos + d)- readClass.getCoverageStart();
								if (covIndex < 0) continue; 
								if (covIndex > readClass.getCoverages().length -1) break;
								
								readClass.getCoverages()[covIndex][MainPane.baseMap.get((byte) 'D')]++;

							}
							readpos += element.getLength();
						} else if (element.getOperator().compareTo(CigarOperator.INSERTION) == 0) {
							int covIndex = (readstart + readpos) - readClass.getCoverageStart();
							if (covIndex < 0) continue; 
							if (covIndex > readClass.getCoverages().length -1) break;
							
							readClass.getCoverages()[covIndex][MainPane.baseMap.get((byte) 'I')]++;
							mispos += element.getLength();

						} else if (element.getOperator().compareTo(CigarOperator.SOFT_CLIP) == 0) {
							if (Settings.softClips == 1) {

								for (int r = readpos; r < readpos + element.getLength(); r++) {
									if (((readstart + r) - split.getReadReference().getStartPos()- 1) > split.getReadReference().getSeq().length - 1) {
										split.getReadReference().append(samRecord.getUnclippedEnd() + 1000);
									}
									if (((readstart + r) - splitIndex.getReadReference().getStartPos() - 1) < 0) {
										split.getReadReference().appendToStart(samRecord.getUnclippedStart() - 1000);
									}
									/*
									 * if(((readstart+r)-readClass.reference.getStartPos()-1) < 0) { continue; }
									 */
									if (samRecord.getReadBases()[mispos] != split.getReadReference()
											.getSeq()[((readstart + r) - split.getReadReference().getStartPos() - 1)]) {
										if (SA == null) {

											readClass.getCoverages()[(readstart + r)
													- readClass.getCoverageStart()][MainPane.baseMap
															.get(samRecord.getReadBases()[mispos])]++;
										}
										if (mismatches == null) {
											mismatches = new java.util.ArrayList<java.util.Map.Entry<Integer, Byte>>();
										}
										final java.util.Map.Entry<Integer, Byte> mismatch = new java.util.AbstractMap.SimpleEntry<>(
												r, samRecord.getReadBases()[mispos]);
										mismatches.add(mismatch);
									}
									if (SA == null) {
										coverageArray[((readstart + r) - readClass.getCoverageStart())][0]++;
									}
									mispos++;
								}
								readpos += element.getLength();

							} else {
								if (i == 0) {

									readpos += element.getLength();
									mispos += element.getLength();

								}
							}

						} else if (element.getOperator().compareTo(CigarOperator.HARD_CLIP) == 0) {
							if (i == 0) {
								readstart = samRecord.getAlignmentStart();
							}
						} else if (element.getOperator().compareTo(CigarOperator.SKIPPED_REGION) == 0) {
							readpos += element.getLength();
							// this.readWidth+=element.getLength();
						}
					}
				} else {
					readstart = samRecord.getUnclippedStart();
					/*
					 * if(readClass.getCoverageStart()>readstart) { return null; }
					 */
					if ((samRecord.getUnclippedEnd() - split.getReadReference().getStartPos() - 1) > split.getReadReference().getSeq().length - 1) {
						split.getReadReference().append(samRecord.getUnclippedEnd() + 100);
					}
					if ((samRecord.getUnclippedStart() - split.getReadReference().getStartPos() - 1) < 0) {
						split.getReadReference().appendToStart(samRecord.getUnclippedStart() - 100);
					}

					for (int r = 0; r < samRecord.getReadLength(); r++) {
						try {
							Byte readMismatch = samRecord.getReadBases()[r];
							Byte refByte = split.getReadReference().getSeq()[((readstart + r) - split.getReadReference().getStartPos() - 1)];
							Byte refByteUppercase = (byte)(refByte - 32);
									
							if (readMismatch != refByte && readMismatch != refByteUppercase) {					

								if ((readstart + r) - readClass.getCoverageStart() < readClass.getCoverages().length - 1) {
									readClass.getCoverages()[(readstart + r)	- readClass.getCoverageStart()][MainPane.baseMap.get(samRecord.getReadBases()[r])]++;
									if (mismatches == null) {
										mismatches = new java.util.ArrayList<java.util.Map.Entry<Integer, Byte>>();
									}
									final java.util.Map.Entry<Integer, Byte> mismatch = new java.util.AbstractMap.SimpleEntry<>(r, samRecord.getReadBases()[r]);
									mismatches.add(mismatch);
								}
							}
							try {

								coverageArray[(readstart + r) - readClass.getCoverageStart()][0]++;
							} catch (final Exception e) {
								// TODO error
								// e.printStackTrace();
								continue;
							}
							if (coverageArray[(samRecord.getUnclippedStart() - readClass.getCoverageStart())
									+ r][0] > readClass.getMaxcoverage()) {
								readClass.setMaxcoverage(
										coverageArray[(samRecord.getUnclippedStart() - readClass.getCoverageStart())
												+ r][0]);
							}
						} catch (final Exception e) {

							ErrorLog.addError(e.getStackTrace());
							e.printStackTrace();
							return mismatches;

						}
					}

				}
			}
		} catch (final Exception e) {

			e.printStackTrace();
			return null;
		}
		return mismatches;
	}

	/*
	 * public class ReadAdder extends SwingWorker<String, Object> {
	 * 
	 * boolean end = false; String chrom; Reads readClass; ReadBuffer buffer; public
	 * ReadAdder(String chrom, Reads readClass, ReadBuffer buffer) { this.chrom =
	 * chrom; this.readClass = readClass; this.buffer = buffer; }
	 * 
	 * protected String doInBackground() {
	 * 
	 * while(true) { try {
	 * 
	 * if(buffer.prev != null ) { readSam(chrom, readClass,buffer.record); //
	 * buffer.prev.next = null; buffer.prev = null; } if(buffer.next != null) {
	 * buffer = buffer.next; }
	 * 
	 * if(end && buffer.next == null) {
	 * 
	 * buffer.prev = null; break; } } catch(Exception e) {
	 * 
	 * e.printStackTrace(); end = true; break; }
	 * 
	 * } Draw.updateReads = true; buffer.prev = null; buffer = null;
	 * 
	 * return ""; }
	 * 
	 * }
	 */
	public static boolean isTrackFile(final String file) {

		if (file.toLowerCase().endsWith(".bed") || file.toLowerCase().endsWith(".bed.gz")) {
			return true;
		} else if (file.toLowerCase().endsWith(".bedgraph.gz")) {
			return true;
		} else if (file.toLowerCase().endsWith("gff.gz")) {
			return true;
		} else if (file.toLowerCase().endsWith("gff3.gz")) {
			return true;
		} else if (file.toLowerCase().endsWith(".bigwig")) {
			return true;
		} else if (file.toLowerCase().endsWith(".bw")) {
			return true;
		} else if (file.toLowerCase().endsWith(".bigbed")) {
			return true;
		} else if (file.toLowerCase().endsWith(".bb")) {
			return true;
		} else if (file.toLowerCase().endsWith(".tsv.gz")) {
			return true;
		} else if (file.toLowerCase().endsWith(".tsv.bgz")) {
			return true;
		}
		return false;
	}

	public static boolean isTrackFile(final File file) {

		if (file.getName().toLowerCase().endsWith(".bed") || file.getName().toLowerCase().endsWith(".bed.gz")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".bedgraph.gz")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith("gff.gz")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith("gff3.gz")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".bigwig")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".bw")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".bigbed")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".bb")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".tsv.gz")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".tsv.bgz")) {
			return true;
		} else if (file.getName().toLowerCase().endsWith(".txt")) {
			try {
				final BufferedReader reader = new BufferedReader(new FileReader(file));
				String line, gene;
				//final int counter = 0;
				int founds = 0;

				while ((line = reader.readLine()) != null) {
					/*
					 * counter++; if(counter > 1000) { break; }
					 */
					gene = line.replace(" ", "");
					if (MenuBar.searchTable.containsKey(gene.toUpperCase())
							|| MainPane.geneIDMap.containsKey(gene.toUpperCase())) {

						founds++;
					}
				}

				reader.close();
				/* if (counter > 1000) {
					MainPane.showError("Please give less than 1000 genes in a txt-file.", "Error.");
					return false;
				} else  */if (founds == 0) {
					MainPane.showError("No genes could be identified from the " + file.getName(), "Error.");
					return false;
				} else {
					return true;
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}

		}
		return false;

	}

	public void readBED(final File[] files) {

		try {

			File bedfile;
			Loader.setLoading("Loading tracks");
			int addedfiles = 0;
			for (int i = 0; i < files.length; i++) {

				bedfile = files[i];
				if (!isTrackFile(bedfile)) {
					continue;
				}
				if (!checkIndex(files[i])) {
					if (files[i].getName().endsWith(".tsv.gz") || files[i].getName().endsWith(".tsv.bgz")) {

						MainPane.showError("No index file for the TSV file. Use Tools -> BED converter.", "Error");
						continue;
					}
					if (JOptionPane.showConfirmDialog(MainPane.drawScroll,
							"No index file found. Do you want to create one?", "Indexing?", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {

						Loader.loadingtext = "Indexing " + files[i].getName();
						if (files[i].getName().endsWith(".gz")) {

							MethodLibrary.createBEDIndex(files[i]);
						} else {
							MethodLibrary.createBEDIndex2(files[i]);
						}
					}
				}

				final BedTrack addTrack = new BedTrack(bedfile, MainPane.bedCanvas.bedTrack.size());
				if (!checkIndex(files[i])) {
					MainPane.showError("Index file not found. Using the file may be slow.", "Note");
					addTrack.indexFile = false;
				}
				if (bedfile.getName().endsWith("tsv.gz") || bedfile.getName().endsWith("tsv.bgz")) {
					addTrack.getZerobased().setSelected(false);
					addTrack.iszerobased = 1;
					addTrack.getSelector().frame.setVisible(true);
				}
				MainPane.bedCanvas.bedTrack.add(addTrack);
				MainPane.bedCanvas.trackDivider.add(0.0);
				addTrack.minvalue = 0;

				if (bedfile.length() / 1048576 < Settings.settings.get("bigFile")) {
					addTrack.small = true;
					MainPane.bedCanvas.getBEDfeatures(addTrack, 1, Draw.getSplits().get(0).chromEnd);
				}
				if (bedfile.getName().toLowerCase().endsWith(".bedgraph")
						|| bedfile.getName().toLowerCase().endsWith(".bedgraph.gz")) {

					MainPane.bedCanvas.pressGraph(addTrack);
					addTrack.getSelectorButton().setVisible(true);
				} else if (bedfile.getName().toLowerCase().endsWith("bigwig")
						|| bedfile.getName().toLowerCase().endsWith("bw")) {
					addTrack.bigWig = true;
					addTrack.small = true;
					MainPane.bedCanvas.pressGraph(addTrack);
					addTrack.getSelectorButton().setVisible(false);
					MainPane.bedCanvas.getBEDfeatures(addTrack, (int) Draw.getSplits().get(0).start,
							(int) Draw.getSplits().get(0).end);
				} else {
					setBedTrack(addTrack);
				}
				addedfiles++;
				setTable(addTrack);

			}
			Loader.ready("Loading tracks");
			if (MainPane.bedCanvas.bedTrack.size() == 0) {
				return;
			}
			if (!MainPane.trackPane.isVisible()) {
				MainPane.trackPane.setVisible(true);
				MainPane.varpane.setDividerSize(3);
				if (addedfiles < 5) {
					MainPane.varpane.setResizeWeight(addedfiles * 0.1);
					MainPane.trackPane.setDividerLocation(addedfiles * 0.1);
					MainPane.varpane.setDividerLocation(addedfiles * 0.1);
				} else {
					MainPane.varpane.setResizeWeight(0.8);
					MainPane.trackPane.setDividerLocation(0.8);
					/*
					 * MainPane.bedCanvas.setPreferredSize(new Dimension(MainPane.drawWidth,
					 * MainPane.bedCanvas.bedTrack.size()*30 )); MainPane.bedCanvas.bufImage =
					 * MethodLibrary.toCompatibleImage(new
					 * BufferedImage((int)MainPane.screenSize.getWidth(),
					 * (int)MainPane.bedCanvas.bedTrack.size()*30, BufferedImage.TYPE_INT_ARGB));
					 * MainPane.bedCanvas.buf = (Graphics2D)MainPane.bedCanvas.bufImage.getGraphics();
					 * 
					 * MainPane.bedCanvas.nodeImage = MethodLibrary.toCompatibleImage(new
					 * BufferedImage((int)MainPane.screenSize.getWidth(),
					 * (int)MainPane.bedCanvas.bedTrack.size()*30, BufferedImage.TYPE_INT_ARGB));
					 * MainPane.bedCanvas.nodebuf = (Graphics2D)MainPane.bedCanvas.nodeImage.getGraphics();
					 */
					MainPane.varpane.setDividerLocation(0.8);
				}
				MainPane.trackPane.revalidate();
				MainPane.varpane.revalidate();

			} else {
				MainPane.trackPane.setDividerLocation(MainPane.trackPane.getDividerLocation() + 70);
				MainPane.varpane.setDividerLocation(MainPane.varpane.getDividerLocation() + 70);
				MainPane.varpane.revalidate();
				MainPane.trackPane.revalidate();

				if (MainPane.controlScroll.isVisible()) {
					MainPane.trackPane.setDividerSize(3);
				}
			}
			MainPane.bedScroll.setVisible(true);
			MainPane.bedCanvas.setVisible(true);
			for (int i = 0; i < MainPane.bedCanvas.trackDivider.size(); i++) {
				MainPane.bedCanvas.trackDivider.set(i,
						((i + 1) * (MainPane.varpane.getDividerLocation() / (double) MainPane.bedCanvas.trackDivider.size())));
			}
			MainPane.trackPane.setDividerLocation((int) ((MainPane.bedCanvas.trackDivider.size())
					* (MainPane.varpane.getDividerLocation() / (double) MainPane.bedCanvas.trackDivider.size())));
		} catch (final Exception e) {
			e.printStackTrace();
			ErrorLog.addError(e.getStackTrace());
		}

	}

	public static void setBedTrack(final BedTrack addTrack) {
		try {
			/*
			 * if(!addTrack.file.getName().endsWith(".gz")) { return; }
			 */
			if (addTrack.getBBfileReader() != null) {
				return;
			}
			String name = "";
			if (addTrack.file != null) {
				name = addTrack.file.getName().toLowerCase();
			} else {
				name = addTrack.url.toString().toLowerCase();
			}
			if (name.endsWith(".bw") || name.endsWith(".bigwig") || name.endsWith(".bb") || name.endsWith(".bigbed")) {
				return;
			}
			InputStream in = null;
			String[] split;
			BufferedReader reader = null;
			GZIPInputStream gzip = null;
			FileReader freader = null;
			if (addTrack.file != null) {
				if (addTrack.file.getName().endsWith(".gz") || addTrack.file.getName().endsWith(".bgz")) {
					gzip = new GZIPInputStream(new FileInputStream(addTrack.file));
					reader = new BufferedReader(new InputStreamReader(gzip));
				} else {
					freader = new FileReader(addTrack.file);
					reader = new BufferedReader(freader);
				}
			} else {
				in = addTrack.url.openStream();
				gzip = new GZIPInputStream(in);
				reader = new BufferedReader(new InputStreamReader(gzip));
			}

			int count = 0;
			if (name.endsWith(".gff.gz") || name.endsWith(".gff3.gz")) {
				addTrack.iszerobased = 1;
				addTrack.getZerobased().setSelected(false);
				String line;
				while (count < 10) {
					line = reader.readLine();
					if (line == null) {
						break;
					}
					if (line.startsWith("#")) {
						continue;
					}

					split = line.split("\\t");

					if (split.length > 5) {
						if (!Double.isNaN(Double.parseDouble(split[5]))) {
							addTrack.hasvalues = true;
						}

					}
					if (MainPane.SELEXhash.containsKey(split[2].replace(".pfm", ""))) {
						addTrack.selex = true;
						addTrack.getAffinityBox().setVisible(true);

					}
					count++;
				}
			} else if (name.endsWith(".bed.gz") || name.endsWith(".bed")) {
				String line;
				while (count < 10) {
					line = reader.readLine();
					if (line == null) {
						break;
					}
					if ((line.startsWith("#") || line.startsWith("track"))) {
						continue;
					}
					split = line.split("\\t");

					if (split.length > 4) {
						try {
							if (!Double.isNaN(Double.parseDouble(split[4]))) {
								addTrack.hasvalues = true;
							}
						} catch (final Exception e) {

						}

					}
					if (split.length > 3 && MainPane.SELEXhash.containsKey(split[3])) {
						addTrack.selex = true;
						addTrack.getAffinityBox().setVisible(true);
					}
					count++;
				}

			} else if (name.endsWith(".tsv.gz") || name.endsWith(".tsv.bgz")) {
				if (addTrack.valuecolumn != null) {
					String line;
					while (count < 10) {
						line = reader.readLine();
						if (line == null) {
							break;
						}
						if (line.startsWith("#")) {
							continue;
						}

						split = line.split("\\t");

						if (!Double.isNaN(Double.parseDouble(split[addTrack.valuecolumn]))) {

							addTrack.hasvalues = true;
							break;
						}
						count++;
					}
				}
			}

			if (addTrack.getBBfileReader() == null && !addTrack.hasvalues) {

				addTrack.getLimitField().setVisible(false);
			} else {
				addTrack.getLimitField().setVisible(true);
			}
			if (gzip != null) {
				gzip.close();
			}

			if (freader != null) {
				freader.close();
			}
			if (in != null) {
				in.close();
			}
			reader.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static String getInfoValue(final HashMap<String, String> map, final String key) {

		if (map.containsKey(key)) {
			return map.get(key);
		} else {
			return "-";
		}

	}

	static HashMap<String, String> makeHash(final String[] line) {
		if (line == null || line.length < 9) {
			return null;
		}
		final HashMap<String, String> hash = new HashMap<String, String>();

		final String[] infosplit = line[8].split(";");
		hash.put("seqid", line[0].replace("chr", ""));
		hash.put("source", line[1]);
		hash.put("type", line[2]);
		hash.put("start", "" + (Integer.parseInt(line[3]) - 1));
		hash.put("end", line[4]);
		hash.put("score", line[5]);
		hash.put("strand", line[6]);
		if (line[7].equals("2")) {
			line[7] = "1";
		} else if (line[7].equals("1")) {
			line[7] = "2";
		}

		hash.put("phase", line[7]);

		String[] split;
		for (int i = 0; i < infosplit.length; i++) {
			if (infosplit[i].contains("=")) {
				split = infosplit[i].split("=");
				hash.put(split[0].toLowerCase(), split[1]);
			} else {
				split = infosplit[i].trim().split(" ");

				hash.put(split[0].toLowerCase().replaceAll("\"", "").replace("name", "symbol"),
						split[1].replaceAll("\"", ""));
			}

		}
		return hash;
	}

	public static void readGTF(final File infile, final String outfile, final SAMSequenceDictionary dict) {
		BufferedReader reader = null;
		GZIPInputStream gzip = null;
		FileReader freader = null;
		String line = "", chrom = "-1";
		HashMap<String, String> lineHash;
		final HashMap<String, Gene> genes = new HashMap<String, Gene>();
		final HashMap<String, Transcript> transcripts = new HashMap<String, Transcript>();
		Gene addgene;
		// Boolean found = false;
		Transcript addtranscript;

		try {

			if (infile.getName().endsWith(".gz")) {
				gzip = new GZIPInputStream(new FileInputStream(infile));
				reader = new BufferedReader(new InputStreamReader(gzip));
			} else {
				freader = new FileReader(infile);
				reader = new BufferedReader(freader);
			}
			// line = reader.readLine();
			while ((line = reader.readLine()) != null) {

				if (line.startsWith("#")) {
					continue;
				}
				/*
				 * if(!line.contains("Rp1h")) { if(found) { break; } continue; } found = true;
				 */

				lineHash = makeHash(line.split("\t"));
				chrom = lineHash.get("seqid");

				if (!genes.containsKey(lineHash.get("gene_id"))) {
					/*
					 * if(genes.size() > 1) { break; }
					 */
					final Gene gene = new Gene(chrom, lineHash, true);

					genes.put(lineHash.get("gene_id"), gene);
					if (lineHash.get("transcript_id") == null) {
						continue;
					}
					// continue;
				}
				if (!transcripts.containsKey(lineHash.get("transcript_id"))) {

					addgene = genes.get(lineHash.get("gene_id"));
					transcripts.put(getInfoValue(lineHash, "transcript_id"), new Transcript(lineHash, addgene));
					if (lineHash.get("type").equals("exon")) {
						addtranscript = transcripts.get(getInfoValue(lineHash, "transcript_id"));
						addtranscript.addExon(lineHash, addtranscript);
					}
					if (addgene.getDescription().equals("-")) {
						if (lineHash.containsKey("gene_symbol")) {
							addgene.setDescription(lineHash.get("gene_symbol"));
						}
					}
					continue;
				}
				if (transcripts.containsKey(lineHash.get("transcript_id"))) {
					if (lineHash.get("type").contains("UTR")) {
						continue;
					}
					addtranscript = transcripts.get(lineHash.get("transcript_id"));
					addtranscript.addExon(lineHash, addtranscript);

					continue;
				}

			}

		} catch (final Exception e) {
			System.out.println(line);
			e.printStackTrace();
			System.exit(0);
		}
		try {

			Transcript transcript;
			Gene gene;
			StringBuffer exStarts, exEnds, exPhases;
			final Iterator<Map.Entry<String, Gene>> it = genes.entrySet().iterator();
			final ArrayList<String[]> geneArray = new ArrayList<String[]>();

			while (it.hasNext()) {
				final Map.Entry<String, Gene> pair = (Map.Entry<String, Gene>) it.next();
				gene = pair.getValue();

				for (int i = 0; i < gene.getTranscripts().size(); i++) {
					transcript = gene.getTranscripts().get(i);
					exStarts = new StringBuffer("");
					exEnds = new StringBuffer("");
					exPhases = new StringBuffer("");
					for (int e = 0; e < transcript.exonArray.size(); e++) {
						exStarts.append(transcript.exonArray.get(e).getStart() + ",");
						exEnds.append(transcript.exonArray.get(e).getEnd() + ",");
						exPhases.append(transcript.exonArray.get(e).getStartPhase() + ",");
					}

					final String[] row = { gene.getChrom(), "" + transcript.getStart(), "" + transcript.getEnd(),
							gene.getName(), "" + transcript.exonArray.size(), MethodLibrary.getStrand(gene.getStrand()),
							gene.getID(), transcript.getENST(), transcript.getUniprot(), "-", transcript.getBiotype(),
							"" + transcript.getCodingStart(), "" + transcript.getCodingEnd(), exStarts.toString(),
							exEnds.toString(), exPhases.toString(), transcript.getDescription() };
					if (transcript.getCodingEnd() == -1) {
						row[11] = "" + gene.getEnd();
						row[12] = "" + gene.getStart();
					}

					geneArray.add(row);
				}

				it.remove();
			}

			final gffSorter gffsorter = new gffSorter();
			Collections.sort(geneArray, gffsorter);

			if (outfile != null) {
				MethodLibrary.blockCompressAndIndex(geneArray, outfile, false, dict);
			}

			geneArray.clear();
			if (freader != null) {
				freader.close();
			}
			reader.close();

			if (gzip != null) {
				gzip.close();
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	public static void readGFF(final File infile, final String outfile, final SAMSequenceDictionary dict) {
		BufferedReader reader = null;
		GZIPInputStream gzip = null;
		FileReader freader = null;
		String line = "", chrom = "-1";
		HashMap<String, String> lineHash;
		final HashMap<String, Gene> genes = new HashMap<String, Gene>();
		final HashMap<String, Transcript> transcripts = new HashMap<String, Transcript>();
		Gene addgene;
		Transcript addtranscript;
		
		try {

			if (infile.getName().endsWith(".gz")) {
				gzip = new GZIPInputStream(new FileInputStream(infile));
				reader = new BufferedReader(new InputStreamReader(gzip));
			} else {
				freader = new FileReader(infile);
				reader = new BufferedReader(freader);
			}
			// line = reader.readLine();
		
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("#")) continue;				
			
				lineHash = makeHash(line.split("\t"));
				if (lineHash.get("type").startsWith("region")) {
					if (line.contains("unlocalized")) {
						chrom = "unlocalized";
					} else if (lineHash.get("chromosome") != null) {
						chrom = lineHash.get("chromosome").replace("chr", "");
					} else if (lineHash.get("name") != null) {
						chrom = lineHash.get("name").replace("chr", "");
					}
					continue;
				}

				if (!lineHash.containsKey("parent")) {
					/*
					 * if(!lineHash.get("type").contains("gene")) {
					 * 
					 * continue; }
					 */

					final Gene gene = new Gene(chrom, lineHash);

					genes.put(getInfoValue(lineHash, "id"), gene);

					continue;
				}
				if (genes.containsKey(lineHash.get("parent"))) {

					addgene = genes.get(lineHash.get("parent"));
					/* if (lineHash.containsKey("tag")) {
						if (!lineHash.get("tag").contains("MANE")) continue;
					} else continue; */
					transcripts.put(getInfoValue(lineHash, "id"), new Transcript(lineHash, addgene));
					if (lineHash.get("type").equals("exon")) {
						addtranscript = transcripts.get(getInfoValue(lineHash, "id"));
						addtranscript.addExon(lineHash, addtranscript);
					}
					if (addgene.getDescription().equals("-")) {
						if (lineHash.containsKey("product")) {
							addgene.setDescription(lineHash.get("product"));
						}
					}
					continue;
				}
				if (transcripts.containsKey(lineHash.get("parent"))) {

					addtranscript = transcripts.get(lineHash.get("parent"));
					addtranscript.addExon(lineHash, addtranscript);
					continue;
				}

			}

		} catch (final Exception e) {
			System.out.println(line);
			e.printStackTrace();
			System.exit(0);
		}
		try {

			Transcript transcript;
			Gene gene;
			StringBuffer exStarts, exEnds, exPhases;
			final Iterator<Map.Entry<String, Gene>> it = genes.entrySet().iterator();
			final ArrayList<String[]> geneArray = new ArrayList<String[]>();

			while (it.hasNext()) {
				final Map.Entry<String, Gene> pair = (Map.Entry<String, Gene>) it.next();
				gene = pair.getValue();

				for (int i = 0; i < gene.getTranscripts().size(); i++) {
					transcript = gene.getTranscripts().get(i);
					exStarts = new StringBuffer("");
					exEnds = new StringBuffer("");
					exPhases = new StringBuffer("");
					for (int e = 0; e < transcript.exonArray.size(); e++) {
						exStarts.append(transcript.exonArray.get(e).getStart() + ",");
						exEnds.append(transcript.exonArray.get(e).getEnd() + ",");
						exPhases.append(transcript.exonArray.get(e).getStartPhase() + ",");
					}
					
					final String[] row = { gene.getChrom(), "" + transcript.getStart(), "" + transcript.getEnd(),
							gene.getName(), "" + transcript.exonArray.size(), MethodLibrary.getStrand(gene.getStrand()),
							gene.getID(), transcript.getENST(), transcript.getUniprot(), "-", transcript.getBiotype(),
							"" + transcript.getCodingStart(), "" + transcript.getCodingEnd(), exStarts.toString(),
							exEnds.toString(), exPhases.toString(), transcript.getDescription() };
					geneArray.add(row);
				}

				it.remove();
			}

			final gffSorter gffsorter = new gffSorter();
			Collections.sort(geneArray, gffsorter);

			if (outfile != null) {
				MethodLibrary.blockCompressAndIndex(geneArray, outfile, false, dict);
			}

			geneArray.clear();
			if (freader != null) {
				freader.close();
			}
			reader.close();

			if (gzip != null) {
				gzip.close();
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	public static class gffSorter implements Comparator<String[]> {

		public int compare(final String[] o1, final String[] o2) {

			if (o1[0].compareTo(o2[0]) == 0) {
				if (Integer.parseInt(o1[1]) < Integer.parseInt(o2[1])) {
					return -1;

				} else if (Integer.parseInt(o1[1]) > Integer.parseInt(o2[1])) {
					return 1;
				} else {
					return 0;
				}

			} else {
				return o1[0].compareTo(o2[0]);
			}

		}
	}

	public static class geneSorter implements Comparator<Gene> {

		public int compare(final Gene o1, final Gene o2) {

			if (o1.getChrom().compareTo(o2.getChrom()) == 0) {
				if (o1.getStart() < o2.getStart()) {
					return -1;
				} else if (o1.getStart() > o2.getStart()) {
					return 1;
				} else {
					return 0;
				}
			} else {
				return -o1.getChrom().compareTo(o2.getChrom());
			}
		}
	}

	public void readBED(final String url, final String index, final boolean small) {

		try {

			if (!MainPane.trackPane.isVisible()) {
				MainPane.trackPane.setVisible(true);
				MainPane.varpane.setDividerSize(3);
				MainPane.varpane.setResizeWeight(0.1);
				MainPane.varpane.setDividerLocation(0.1);

			} else {
				MainPane.varpane.setDividerLocation(MainPane.varpane.getDividerLocation() + 100);
				MainPane.varpane.revalidate();
				MainPane.trackPane.setDividerLocation(MainPane.varpane.getDividerLocation() / 2);

				if (MainPane.controlScroll.isVisible()) {
					MainPane.trackPane.setDividerSize(3);
				}
			}
			MainPane.bedScroll.setVisible(true);
			MainPane.bedCanvas.setVisible(true);
			BedTrack addTrack = null;
			if (index.equals("nan")) {

				addTrack = new BedTrack(new URL(url), null, MainPane.bedCanvas.bedTrack.size());

			} else {
				addTrack = new BedTrack(new URL(url), new URL(index), MainPane.bedCanvas.bedTrack.size());
			}

			// MainPane.bedCanvas.getBEDfeatures(addTrack, 1,
			// Draw.getSplits().get(0).chromEnd);
			if (addTrack != null) {
				if (url.toLowerCase().endsWith("tsv.gz") || url.toLowerCase().endsWith("tsv.bgz")) {
					addTrack.getZerobased().setSelected(false);
					addTrack.iszerobased = 1;
					addTrack.getSelector().frame.setVisible(true);
				}
				addTrack.small = small;
				MainPane.bedCanvas.trackDivider.add(0.0);
				MainPane.bedCanvas.bedTrack.add(addTrack);
				setTable(addTrack);
				if (url.toLowerCase().endsWith(".bedgraph")) {
					MainPane.bedCanvas.pressGraph(addTrack);
				}
				if (url.toLowerCase().endsWith("bigwig") || url.toLowerCase().endsWith("bw")) {
					addTrack.small = true;
					addTrack.bigWig = true;
					addTrack.graph = true;
					// MainPane.bedCanvas.getBEDfeatures(addTrack,
					// (int)Draw.getSplits().get(0).start,
					// (int)Draw.getSplits().get(0).end);
				}
				setBedTrack(addTrack);
				if (addTrack.small) {
					// Loader.setLoading("loading");
					MainPane.bedCanvas.getBEDfeatures(addTrack, 1, Draw.getSplits().get(0).chromEnd);

					// Loader.ready("loading");
				}

			}

		} catch (final Exception e) {
			e.printStackTrace();
			ErrorLog.addError(e.getStackTrace());
		}
	}

	public static void setTable(final BedTrack track) {

		final JScrollPane addpane = new JScrollPane();
		addpane.setPreferredSize(new Dimension(500, 400));
		final BedTable add = new BedTable((int) MainPane.screenSize.width, (int) MainPane.screenSize.height, addpane, track);

		addpane.getViewport().add(add);
		addpane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(final AdjustmentEvent event) {
				if (VariantHandler.tabs.getSelectedIndex() > 2) {
					VariantHandler.tables.get(VariantHandler.tabs.getSelectedIndex() - 3).repaint();
				}

			}
		});

		VariantHandler.tables.add(add);
		VariantHandler.tablescrolls.add(addpane);
		track.setTable(add);
		add.resizeTable(VariantHandler.tableScroll.getViewport().getWidth());
		if (track.file != null) {
			/*
			 * if(track.file.getName().length() > 10) {
			 * VariantHandler.tabs.add(track.file.getName().substring(0, 10) +"...",
			 * addpane); add.setName(track.file.getName().substring(0, 10) +"..."); } else {
			 */
			VariantHandler.tabs.add(track.file.getName().replace(".pfm", ""), addpane);
			add.setName(track.file.getName());
			// }
		} else {
			/*
			 * if(FilenameUtils.getName(track.url.getFile()).length() > 10) {
			 * VariantHandler.tabs.add(FilenameUtils.getName(track.url.getFile()).substring(
			 * 0, 10) +"...", addpane);
			 * add.setName(FilenameUtils.getName(track.url.getFile()).substring(0, 10)
			 * +"..."); } else {
			 */
			VariantHandler.tabs.add(FilenameUtils.getName(track.url.getFile()), addpane);
			add.setName(FilenameUtils.getName(track.url.getFile()));
			// }

		}
		// VariantHandler.clusterTable.addHeaderColumn(track);
		MethodLibrary.addHeaderColumns(track);
		VariantHandler.tabs.revalidate();
	}

	public static void removeTable(final BedTrack track) {
		try {

			if (VariantHandler.tables.indexOf(track.getTable()) > -1) {

				VariantHandler.tabs.remove(track.getTable().tablescroll);
				VariantHandler.tablescrolls.remove(track.getTable().tablescroll);
				VariantHandler.tables.remove(track.getTable());
				VariantHandler.tabs.revalidate();
				VariantHandler.tabs.repaint();

			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * int binarySearch(ArrayList<int[]> list, int value, int middle) {
	 * 
	 * if(middle == 0) {
	 * 
	 * return 0; } if(middle == list.size()-1) { return list.size()-1; }
	 * if(list.get(middle-1)[1] <= value) {
	 * 
	 * middle = middle/2; return binarySearch(list, value, middle); }
	 * if(list.get(middle-1)[1] <= value && list.get(middle)[1] >= value) { return
	 * middle-1; } if(list.get(middle-1)[1] > value ) {
	 * 
	 * return binarySearch(list, value, middle); }
	 * 
	 * return -1; }
	 */
	int searchIndex(final ArrayList<int[]> list, final int value, final int low, final int high) {

		middle = (high + low) / 2;
		if (middle == list.size() - 1) {

			return list.size();
		}

		if (list.get(middle)[1] <= value && list.get(middle + 1)[1] >= value) {
			return middle + 1;
		}
		if (list.get(middle)[1] > value) {

			return searchIndex(list, value, low, middle);
		}
		if (list.get(middle)[1] < value) {

			return searchIndex(list, value, middle, high);
		}
		return -1;

	}

	void readSam(final String chrom, final Reads readClass, final SAMRecord samRecordBuffer, final java.util.ArrayList<java.util.Map.Entry<Integer, Byte>> mismatches, final int level, boolean isDiscordant) {
		try {

			if (readClass.getReads().isEmpty()) {

				addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom, readClass.sample,	splitIndex, readClass, mismatches, isDiscordant);
				readClass.setFirstRead(addNode);
				startY = (int) (((readClass.sample.getIndex() + 1) * Draw.drawVariables.sampleHeight	- MainPane.drawCanvas.bottom - (BaseVariables.readHeight + 2)));
				addNode.setRectBounds((int) ((addNode.getPosition() - start) * pixel), startY,	(int) (samRecordBuffer.getReadLength() * pixel), BaseVariables.readHeight);
				readClass.getReads().add(addNode);
				final ReadNode[] addList = new ReadNode[2];
				addList[headnode] = addNode;
				addList[tailnode] = addNode;
				readClass.getHeadAndTail().add(addList);
				readClass.setLastRead(addNode);

			} else {

				mundane = null;
				found = false;
				final ArrayList<ReadNode> mundanes = new ArrayList<ReadNode>();

				if (samRecordBuffer.getCigarLength() > 1) {
					if (samRecordBuffer.getCigar().getCigarElement(0).getOperator().compareTo(CigarOperator.HARD_CLIP) == 0) {
						searchPos = samRecordBuffer.getAlignmentStart();
					} else {
						searchPos = samRecordBuffer.getUnclippedStart();
					}
				} else {
					searchPos = samRecordBuffer.getUnclippedStart();
				}

				for (int i = level; i < readClass.getHeadAndTail().size(); i++) {
					if (i > Settings.readDepthLimit) {
						found = true;
						mundane = null;
						addNode = null;
						break;
					}
					if (mundanes.size() > 0) {
						addNode = mundanes.get(0);
						addNode.setNext(null);
						addNode.setPrev(null);
						searchPos = addNode.getPosition();
						isDiscordant = false;

					}
					if (!isDiscordant) {
						if (readClass.getHeadAndTail().get(i)[tailnode].getPosition()	+ readClass.getHeadAndTail().get(i)[tailnode].getWidth() + 2 < searchPos) { // .getPosition())
																										
							try {
								if (mundanes.size() == 0) {
									xpos = (int) ((searchPos - start) * pixel);
									addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom,
											readClass.sample, splitIndex, readClass, mismatches, isDiscordant);
									readClass.setLastRead(addNode);
								} else {
									mundanes.remove(0);
								}

								startY = (int) (((readClass.sample.getIndex() + 1)	* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom	- ((i + 1) * (BaseVariables.readHeight + 2))));
								addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),	BaseVariables.readHeight);
								addNode.setPrev(readClass.getHeadAndTail().get(i)[tailnode]);
								readClass.getHeadAndTail().get(i)[tailnode].setNext(addNode);
								readClass.getHeadAndTail().get(i)[tailnode] = addNode;
								found = true;
								if (mundanes.size() == 0) break;								

							} catch (final Exception e) {
								ErrorLog.addError(e.getStackTrace());
								e.printStackTrace();
							}
						}

						continue;
					} else {

						if (readClass.getHeadAndTail().get(i)[tailnode].getPosition()
								+ readClass.getHeadAndTail().get(i)[tailnode].getWidth() + 2 < searchPos) {

							xpos = (int) ((searchPos - start) * pixel);
							addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom,
									readClass.sample, splitIndex, readClass, mismatches, isDiscordant);
							startY = (int) (((readClass.sample.getIndex() + 1)
									* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom
									- ((i + 1) * (BaseVariables.readHeight + 2))));
							addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),
									BaseVariables.readHeight);
							addNode.setPrev(readClass.getHeadAndTail().get(i)[tailnode]);
							readClass.getHeadAndTail().get(i)[tailnode].setNext(addNode);
							readClass.getHeadAndTail().get(i)[tailnode] = addNode;
							readClass.setLastRead(addNode);
							found = true;
							break;
						} else if (!readClass.getHeadAndTail().get(i)[tailnode].isDiscordant()) {
							xpos = (int) ((searchPos - start) * pixel);
							addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom,
									readClass.sample, splitIndex, readClass, mismatches, isDiscordant);
							ReadNode tempmundane = readClass.getHeadAndTail().get(i)[tailnode];
							mundane = tempmundane;

							while (tempmundane.getPosition() + tempmundane.getWidth() + 2 >= searchPos) {
								mundanes.add(tempmundane);

								mundane = tempmundane;
								tempmundane = tempmundane.getPrev();

								if (tempmundane == null) {
									break;
								}
								if (tempmundane.isDiscordant()) {
									break;
								}
							}
							tempmundane = null;
							startY = (int) (((readClass.sample.getIndex() + 1)
									* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom
									- ((i + 1) * (BaseVariables.readHeight + 2))));
							addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),
									BaseVariables.readHeight);

							if (mundane.getPrev() != null) {
								addNode.setPrev(mundane.getPrev());
								mundane.getPrev().setNext(addNode);
							}
							if (readClass.getHeadAndTail().get(i)[headnode].equals(mundane)) {
								readClass.getHeadAndTail().get(i)[headnode] = addNode;
							}
							readClass.getHeadAndTail().get(i)[tailnode] = addNode;
							found = false;
							readClass.setLastRead(addNode);

							try {
								readClass.getReads().set(i, addNode);
							} catch (final Exception e) {
								System.out
										.println(readClass.getHeadAndTail().size() + " " + readClass.getReads().size());
							}

							// addNode = mundane;
							// searchPos = addNode.getPosition();
							// isDiscordant = false;

							continue;
						}

					}

				}

				if (!found) {

					if (mundanes.size() == 0) {
						xpos = (int) ((searchPos - start) * pixel);
						addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom, readClass.sample,
								splitIndex, readClass, mismatches, isDiscordant);
						final ReadNode[] addList = new ReadNode[2];
						addList[headnode] = addNode;
						addList[tailnode] = addNode;
						addNode.setPrev(null);
						addNode.setNext(null);
						readClass.getHeadAndTail().add(addList);
						readClass.getReads().add(addNode);
						readClass.setLastRead(addNode);
						startY = (int) (((readClass.sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
								- MainPane.drawCanvas.bottom
								- ((readClass.getReads().size()) * (BaseVariables.readHeight + 2))));
						addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel), BaseVariables.readHeight);
					} else {
						while (mundanes.size() > 0) {
							addNode = mundanes.get(0);
							xpos = addNode.getRect().x;

							final ReadNode[] addList = new ReadNode[2];
							addList[headnode] = addNode;
							addList[tailnode] = addNode;
							addNode.setPrev(null);
							addNode.setNext(null);
							readClass.getHeadAndTail().add(addList);
							readClass.getReads().add(addNode);
							readClass.setLastRead(addNode);
							startY = (int) (((readClass.sample.getIndex() + 1)
									* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom
									- ((readClass.getReads().size()) * (BaseVariables.readHeight + 2))));
							addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),
									BaseVariables.readHeight);
							mundanes.remove(0);
						}
					}
				}
			}

		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();

		}

	}

	void readSamLeft(final String chrom, final Reads readClass, final SAMRecord samRecordBuffer,
			final java.util.ArrayList<java.util.Map.Entry<Integer, Byte>> mismatches, boolean isDiscordant) {
		try {

			mundane = null;
			found = false;

			// isDiscordant = MethodLibrary.isDiscordant(samRecordBuffer,
			// readClass.sample.getComplete());

			if (samRecordBuffer.getCigarLength() > 1) {
				if (samRecordBuffer.getCigar().getCigarElement(0).getOperator()
						.compareTo(CigarOperator.HARD_CLIP) == 0) {
					searchPos = samRecordBuffer.getAlignmentEnd();
				} else {
					searchPos = samRecordBuffer.getUnclippedEnd();
				}
			} else {
				searchPos = samRecordBuffer.getUnclippedEnd();
			}

			for (int i = 0; i < readClass.getHeadAndTail().size(); i++) {
				if (i > Settings.readDepthLimit) {
					found = true;
					mundane = null;
					addNode = null;
					break;
				}

				if (readClass.getHeadAndTail().get(i)[headnode].getPrev() != null
						&& readClass.getHeadAndTail().get(i)[headnode].getPrev().getPosition() > searchPos) {
					ReadNode tempnode;
					while (readClass.getHeadAndTail().get(i)[headnode].getPrev() != null) {
						tempnode = readClass.getHeadAndTail().get(i)[headnode].getPrev();
						readClass.getHeadAndTail().get(i)[headnode] = tempnode;
					}

					tempnode = null;

				}
				if (!isDiscordant) {
					if (readClass.getHeadAndTail().get(i)[headnode].getPosition() - 2 > searchPos) {

						if (readClass.getHeadAndTail().get(i)[headnode].getPrev() != null
								&& readClass.getHeadAndTail().get(i)[headnode].getPrev().getPosition()
										+ readClass.getHeadAndTail().get(i)[headnode].getPrev().getWidth() > searchPos
												- samRecordBuffer.getReadLength()) {
							continue;
						}

						try {

							if (mundane == null) {
								xpos = (int) ((searchPos - start) * pixel);
								addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom,
										readClass.sample, splitIndex, readClass, mismatches, isDiscordant);

							}

							startY = (int) (((readClass.sample.getIndex() + 1)
									* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom
									- ((i + 1) * (BaseVariables.readHeight + 2))));
							addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),
									BaseVariables.readHeight);
							addNode.setPrev(readClass.getHeadAndTail().get(i)[headnode].getPrev());

							readClass.getHeadAndTail().get(i)[headnode].setPrev(addNode);
							addNode.setNext(readClass.getHeadAndTail().get(i)[headnode]);
							if (addNode.getPrev() != null) {
								addNode.getPrev().setNext(addNode);
							}

							found = true;
							break;

						} catch (final Exception e) {
							ErrorLog.addError(e.getStackTrace());
							e.printStackTrace();
						}

					} else if (readClass.getHeadAndTail().get(i)[tailnode].getPosition()
							+ readClass.getHeadAndTail().get(i)[tailnode].getWidth() + 2 < searchPos) {

						readSam(chrom, readClass, samRecord, mismatches, i, isDiscordant);
						return;
					}
					continue;
				} else {

					if (readClass.getHeadAndTail().get(i)[headnode].getPosition() - 2 > searchPos) {

						if (readClass.getHeadAndTail().get(i)[headnode].getPrev() != null
								&& readClass.getHeadAndTail().get(i)[headnode].getPrev().getPosition()
										+ readClass.getHeadAndTail().get(i)[headnode].getPrev().getWidth() < searchPos
												- samRecordBuffer.getReadLength() - 2) {

							xpos = (int) ((searchPos - samRecordBuffer.getReadLength() - start) * pixel);
							addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom,
									readClass.sample, splitIndex, readClass, mismatches, isDiscordant);

							startY = (int) (((readClass.sample.getIndex() + 1)
									* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom
									- ((i + 1) * (BaseVariables.readHeight + 2))));
							addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),
									BaseVariables.readHeight);
							addNode.setPrev(readClass.getHeadAndTail().get(i)[headnode].getPrev());
							readClass.getHeadAndTail().get(i)[headnode].setPrev(addNode);
							addNode.setNext(readClass.getHeadAndTail().get(i)[headnode]);
							if (addNode.getPrev() != null) {
								addNode.getPrev().setNext(addNode);
							}
							// readClass.setLastRead(addNode);
							found = true;

							break;
						}

						else if (readClass.getHeadAndTail().get(i)[headnode].getPrev() != null
								&& !readClass.getHeadAndTail().get(i)[headnode].getPrev().isDiscordant()) {

							xpos = (int) ((searchPos - samRecordBuffer.getReadLength() - start) * pixel);
							addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom,
									readClass.sample, splitIndex, readClass, mismatches, isDiscordant);

							mundane = readClass.getHeadAndTail().get(i)[headnode].getPrev();

							startY = (int) (((readClass.sample.getIndex() + 1)
									* Draw.drawVariables.sampleHeight - MainPane.drawCanvas.bottom
									- ((i + 1) * (BaseVariables.readHeight + 2))));
							addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel),
									BaseVariables.readHeight);

							if (mundane.getPrev() != null) {
								addNode.setPrev(mundane.getPrev());
								mundane.getPrev().setNext(addNode);
							}
							addNode.setNext(readClass.getHeadAndTail().get(i)[headnode]);
							readClass.getHeadAndTail().get(i)[headnode].setPrev(addNode);

							// readClass.getHeadAndTail().get(i)[headnode] = addNode;
							found = false;
							// readClass.setLastRead(addNode);

							try {
								readClass.getReads().set(i, addNode);
							} catch (final Exception e) {
								System.out
										.println(readClass.getHeadAndTail().size() + " " + readClass.getReads().size());
							}
							addNode = mundane;
							searchPos = addNode.getPosition() + addNode.getWidth();
							isDiscordant = false;
							addNode.setNext(null);
							addNode.setPrev(null);

							continue;
						}

					} else if (readClass.getHeadAndTail().get(i)[tailnode].getPosition()
							+ readClass.getHeadAndTail().get(i)[tailnode].getWidth() + 2 < searchPos) {

						readSam(chrom, readClass, samRecord, mismatches, i, isDiscordant);
						return;
					} else {

						// TILANNE ETT� DISCORDANTTI TULEE HEADNODEN P��LLE JA HEADI PIT�� SIIRT�� YL�S

						xpos = (int) ((searchPos - samRecordBuffer.getReadLength() - start) * pixel);
						addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom, readClass.sample,
								splitIndex, readClass, mismatches, isDiscordant);

						mundane = readClass.getHeadAndTail().get(i)[headnode];
						// mundanes.add(readClass.getHeadAndTail().get(i)[headnode]);
						startY = (int) (((readClass.sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
								- MainPane.drawCanvas.bottom - ((i + 1) * (BaseVariables.readHeight + 2))));
						addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel), BaseVariables.readHeight);
						readClass.getHeadAndTail().get(i)[headnode] = addNode;
						if (mundane.getPrev() != null) {
							addNode.setPrev(mundane.getPrev());
							mundane.getPrev().setNext(addNode);
						}
						addNode.setNext(mundane.getNext());
						if (mundane.getNext() != null) {
							mundane.getNext().setPrev(addNode);
						}
						found = false;
						try {
							readClass.getReads().set(i, addNode);
						} catch (final Exception e) {
							System.out.println(readClass.getHeadAndTail().size() + " " + readClass.getReads().size());
						}
						addNode = mundane;
						searchPos = addNode.getPosition() + addNode.getWidth();
						isDiscordant = false;

						continue;
					}

				}

			}

			if (!found) {

				if (mundane == null) {

					xpos = (int) ((searchPos - start) * pixel);
					addNode = new ReadNode(samRecordBuffer, readClass.sample.getComplete(), chrom, readClass.sample,
							splitIndex, readClass, mismatches, isDiscordant);
				}
				final ReadNode[] addList = new ReadNode[2];
				addList[headnode] = addNode;
				addList[tailnode] = addNode;
				addNode.setPrev(null);
				addNode.setNext(null);
				readClass.getHeadAndTail().add(addList);
				readClass.getReads().add(addNode);
				// readClass.setLastRead(addNode);
				startY = (int) (((readClass.sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
						- MainPane.drawCanvas.bottom - ((readClass.getReads().size()) * (BaseVariables.readHeight + 2))));
				addNode.setRectBounds(xpos, startY, (int) (addNode.getWidth() * pixel), BaseVariables.readHeight);

			}

		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();

		}

	}

	void setLevels(ReadNode node, final Sample sample, final Reads readClass) {
		try {

			while (node != null) {
				mundane = null;
				addNode = node.getPrev();
				found = false;
				xpos = (int) ((node.getPosition() - start) * pixel);

				for (int i = 0; i < readClass.getReads().size(); i++) {
					if (i > Settings.readDepthLimit) {
						if (node.getNext() != null) {
							node.getNext().setPrev(node.getPrev());
							;
						}
						if (node.getPrev() != null) {
							node.getPrev().setNext(node.getNext());
						}
						mundane = null;
						node = null;
						// addNode = null;
						found = true;
						break;
					}

					if (!node.isDiscordant()) {

						if (node.getPosition() + node.getWidth() + 2 < readClass.getHeadAndTail().get(i)[headnode]
								.getPosition()) {
							if (i > 0) {

								startY = (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
										- MainPane.drawCanvas.bottom - ((i + 1) * (BaseVariables.readHeight + 2))));
								node.setRectBounds(xpos, startY, (int) (node.getWidth() * pixel), BaseVariables.readHeight);

								if (mundane == null) {
									if (node.getPrev() != null) {
										node.getPrev().setNext(node.getNext());
									}
									node.getNext().setPrev(node.getPrev());
								}

								node.setNext(readClass.getHeadAndTail().get(i)[headnode]);
								readClass.getHeadAndTail().get(i)[headnode].setPrev(node);
								// node.setPrev(sample.headAndTail.get(i)[headnode].getPrev());

								readClass.getHeadAndTail().get(i)[headnode] = node;
								readClass.getHeadAndTail().get(i)[headnode].setPrev(null);

							} else {

								readClass.getHeadAndTail().get(i)[headnode] = node;

							}

							found = true;
							break;

						}
						continue;

					} else {

						if (node.getPosition() + node.getWidth() + 2 < readClass.getHeadAndTail().get(i)[headnode]
								.getPosition()) {

							if (i > 0) {

								startY = (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
										- MainPane.drawCanvas.bottom - ((i + 1) * (BaseVariables.readHeight + 2))));
								node.setRectBounds(xpos, startY, (int) (node.getWidth() * pixel), BaseVariables.readHeight);
								/*
								 * if(node.getPrev()!=null) { node.getPrev().setNext(node.getNext()); }
								 * 
								 * node.getNext().setPrev(node.getPrev());
								 */
								node.setNext(readClass.getHeadAndTail().get(i)[headnode]);

								readClass.getHeadAndTail().get(i)[headnode].setPrev(node);
								node.setPrev(null);
								readClass.getHeadAndTail().get(i)[headnode] = node;
							} else {

								readClass.getHeadAndTail().get(i)[headnode] = node;
							}

							found = true;
							break;

						} else if (readClass.getHeadAndTail().get(i)[headnode].isDiscordant()) {

							if (i == 0) {
								if (node.getPrev() != null) {
									readClass.getHeadAndTail().get(i)[headnode].setPrev(node.getPrev());
									node.getPrev().setNext(readClass.getHeadAndTail().get(i)[headnode]);

								}
							}

							continue;
						} else {

							mundane = readClass.getHeadAndTail().get(i)[headnode];

							startY = (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
									- MainPane.drawCanvas.bottom - ((i + 1) * (BaseVariables.readHeight + 2))));
							node.setRectBounds(xpos, startY, (int) (node.getWidth() * pixel), BaseVariables.readHeight);
							node.setNext(mundane.getNext());
							if (mundane.getNext() != null) {
								mundane.getNext().setPrev(node);
							}
							mundane.setNext(null);
							mundane.setPrev(null);
							node.setPrev(null);
							readClass.getHeadAndTail().get(i)[headnode] = node;
							readClass.getReads().set(i, node);
							node = mundane;
							found = false;
							continue;

						}
					}
				}
				if (!found) {

					final ReadNode[] addList = new ReadNode[2];
					addList[headnode] = node;
					addList[tailnode] = node;
					readClass.getHeadAndTail().add(addList);

					if (node.getPrev() != null) {
						node.getPrev().setNext(node.getNext());

					}
					if (node.getNext() != null) {
						node.getNext().setPrev(node.getPrev());
					}
					node.setNext(null);
					node.setPrev(null);
					readClass.getReads().add(node);
					startY = (int) (((sample.getIndex() + 1) * Draw.drawVariables.sampleHeight
							- MainPane.drawCanvas.bottom - ((readClass.getReads().size()) * (BaseVariables.readHeight + 2))));
					node.setRectBounds(xpos, startY, (int) (node.getWidth() * pixel), BaseVariables.readHeight);

				}
				node = addNode;

			}

		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
		mundane = null;

	}

	public static void removeNonListBeds(BedNode node, final int topos) {

		BedNode tempNode = null;
		if (node.equals(node.getTrack().getHead())) {
			node = node.getNext();
		}
		while (node != null) {
			if (node.getPosition() >= topos) {

				node = null;
				break;
			}
			if (!node.inVarlist) {
				if (node.getPosition() + node.getLength() + 1 < topos) {
					tempNode = node.getNext();
					node.removeNode();
					node = tempNode;
				} else {

					node = node.getNext();
				}
				continue;
			} else {

				node = node.getNext();
			}

		}

		tempNode = null;

	}

	
	public static void removeBedLinks() {

		for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
			if (MainPane.bedCanvas.bedTrack.get(i).used) {
				BedNode node = MainPane.bedCanvas.bedTrack.get(i).getHead();
				BedNode tempnode;
				try {
					while (node != null) {
						if (node.varnodes != null) {
							for (int n = 0; n < node.varnodes.size(); n++) {
								if (!node.varnodes.get(n).hideNode()) continue;
								
								node.varnodes.remove(n);
								n--;
								if (n == -1) break;
							}
						}
						tempnode = node.getNext();
						if (!bigcalc) {
							node.putPrev(null);
							node.putNext(null);
						}
						node = tempnode;
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}

		}

	}


	public static void main(String args[]) {
		/* System.out.println("start");
		readGFF(new File("C:\\LocalData\\rkataine\\chm13v2.0_RefSeq_Liftoff_v5.1.gff3.gz"), "", null);
		System.out.println("end"); */
	}
}