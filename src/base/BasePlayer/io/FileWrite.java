package base.BasePlayer.io;

import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.AminoTable;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.Control;
import base.BasePlayer.control.ControlFile;
import base.BasePlayer.genome.Gene;
import base.BasePlayer.sample.Sample;
import base.BasePlayer.sample.SampleNode;
import base.BasePlayer.variants.VarCalculations;
import base.BasePlayer.variants.VarNode;
import htsjdk.samtools.util.BlockCompressedOutputStream;
import htsjdk.tribble.Feature;

public class FileWrite {
	public static BufferedWriter output, sigOutput;
	public static BlockCompressedOutputStream outputgz =null;
	public static File outFile = null;
  public static void writeToFile(final VarNode vardraw, final BufferedWriter output, final BlockCompressedOutputStream outputgz) {

		if (VariantHandler.table.genearray.size() > 0) {

			if (VariantHandler.tsv.isSelected() || VariantHandler.compactTsv.isSelected()) {

				for (int i = 0; i < VariantHandler.table.genearray.size(); i++) {
					if (vardraw == null || vardraw.getPosition() > VariantHandler.table.genearray.get(i).getEnd()) {

						writeTranscriptToFile(VariantHandler.table.genearray.get(i), output);

						if (VariantHandler.allChroms.isSelected() || FileRead.bigcalc) {
							for (int s = 0; s < VariantHandler.table.genearray.get(i).varnodes.size(); s++) {
								if (VariantHandler.table.genearray.get(i).varnodes.get(s).getExons() != null) {
									for (int e = 0; e < VariantHandler.table.genearray.get(i).varnodes.get(s).getExons()
											.size(); e++) {
										if (VariantHandler.table.genearray.get(i).varnodes.get(s).getExons().get(e)
												.getTranscript().getGene()
												.equals(VariantHandler.table.genearray.get(i))) {
											VariantHandler.table.genearray.get(i).varnodes.get(s).getExons().remove(e);
											e--;
										}
									}
								}
								if (VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts() != null) {
									for (int e = 0; e < VariantHandler.table.genearray.get(i).varnodes.get(s)
											.getTranscripts().size(); e++) {
										if (VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts()
												.get(e).getGene().equals(VariantHandler.table.genearray.get(i))) {
											VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts()
													.remove(e);
											e--;
										}

									}
								}
							}
						}
						VariantHandler.table.genearray.get(i).samples.clear();
						VariantHandler.table.genearray.get(i).varnodes.clear();
						VariantHandler.table.genearray.remove(i);
						i--;
					}
				}
			} else if (VariantHandler.vcf.isSelected() || VariantHandler.oncodrive.isSelected()) {
				
				if (MainPane.projectValues.variantHandlerValues.compareGene > 1) {

				} else {

					writeNodeToFile(vardraw, vardraw.getChrom(), output, outputgz);
					VarCalculations.lastWriteVar = vardraw;
				}

				for (int i = 0; i < VariantHandler.table.genearray.size(); i++) {
					if (vardraw == null || VariantHandler.table.genearray.get(i).getEnd() < vardraw.getPosition()) {

						if (VariantHandler.allChroms.isSelected() || FileRead.bigcalc) {
							for (int s = 0; s < VariantHandler.table.genearray.get(i).varnodes.size(); s++) {
								if (VariantHandler.table.genearray.get(i).varnodes.get(s).getExons() != null) {
									for (int e = 0; e < VariantHandler.table.genearray.get(i).varnodes.get(s).getExons()
											.size(); e++) {
										if (VariantHandler.table.genearray.get(i).varnodes.get(s).getExons().get(e)
												.getTranscript().getGene().equals(VariantHandler.table.genearray.get(i))) {
											VariantHandler.table.genearray.get(i).varnodes.get(s).getExons().remove(e);
											e--;
										}
									}
								}
								if (VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts() != null) {
									for (int e = 0; e < VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts().size(); e++) {
										if (VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts().get(e).getGene().equals(VariantHandler.table.genearray.get(i))) {
											VariantHandler.table.genearray.get(i).varnodes.get(s).getTranscripts().remove(e);
											e--;
										}
									}
								}
							}
						}
						if (MainPane.projectValues.variantHandlerValues.compareGene == 1) {
							VariantHandler.table.genearray.get(i).samples.clear();
						}
						VariantHandler.table.genearray.get(i).varnodes.clear();
						VariantHandler.table.genearray.remove(i);
						i--;
					}
				}

				if (MainPane.projectValues.variantHandlerValues.compareGene > 1) {

					if (VariantHandler.table.genearray.size() == 0) {

						flushVars(vardraw);
					}
				}
			}

		}
	}

	public static void flushVars(final VarNode vardraw) {
		boolean found = false;
		if (VariantHandler.vcf.isSelected() || VariantHandler.oncodrive.isSelected()) {
			if (VarCalculations.lastWriteVar != null) {
				if (VarCalculations.lastWriteVar.getNext() == null) {
					found = false;

					if (VarCalculations.lastWriteVar.isInGene()) {
						if (VarCalculations.lastWriteVar.getExons() != null) {
							for (int i = 0; i < VarCalculations.lastWriteVar.getExons().size(); i++) {
								if (VarCalculations.lastWriteVar.getExons().get(i).getTranscript().getGene().samples
										.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
									found = true;
								} else {
									VarCalculations.lastWriteVar.getExons().remove(i);
									i--;
								}
							}
						} else {
							for (int i = 0; i < VarCalculations.lastWriteVar.getTranscripts().size(); i++) {
								if (VarCalculations.lastWriteVar.getTranscripts().get(i).getGene().samples
										.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
									found = true;
								} else {
									VarCalculations.lastWriteVar.getTranscripts().remove(i);
									i--;
								}
							}
						}
					}
					if (found) {
						writeNodeToFile(VarCalculations.lastWriteVar, VarCalculations.lastWriteVar.getChrom(), output, outputgz);
					}
				} else {
					VarCalculations.lastWriteVar = VarCalculations.lastWriteVar.getNext();
				}
			}
			if (vardraw == null) {

				if (VariantHandler.table.genearray.size() > 0) {

					while (VarCalculations.lastWriteVar != null) {

						found = false;
						if (VarCalculations.lastWriteVar.isInGene()) {
							if (VarCalculations.lastWriteVar.getExons() != null) {
								for (int i = 0; i < VarCalculations.lastWriteVar.getExons().size(); i++) {
									if (VarCalculations.lastWriteVar.getExons().get(i).getTranscript().getGene().samples
											.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {

										found = true;

									} else {
										VarCalculations.lastWriteVar.getExons().remove(i);
										i--;
									}
								}
							} else {
								for (int i = 0; i < VarCalculations.lastWriteVar.getTranscripts().size(); i++) {
									if (VarCalculations.lastWriteVar.getTranscripts().get(i).getGene().samples
											.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
										found = true;

									}

									else {
										VarCalculations.lastWriteVar.getTranscripts().remove(i);
										i--;
									}
								}
							}
						}
						if (found) {

							writeNodeToFile(VarCalculations.lastWriteVar, VarCalculations.lastWriteVar.getChrom(), output, outputgz);

						}

						VarCalculations.lastWriteVar = VarCalculations.lastWriteVar.getNext();
					}
				}
			} else {

				while (!VarCalculations.lastWriteVar.equals(vardraw)) {

					found = false;
					if (VarCalculations.lastWriteVar.isInGene()) {
						if (VarCalculations.lastWriteVar.getExons() != null) {
							for (int i = 0; i < VarCalculations.lastWriteVar.getExons().size(); i++) {
								if (VarCalculations.lastWriteVar.getExons().get(i).getTranscript().getGene().samples
										.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {

									found = true;
								} else {
									VarCalculations.lastWriteVar.getExons().remove(i);
									i--;
								}
							}
						} else {
							for (int i = 0; i < VarCalculations.lastWriteVar.getTranscripts().size(); i++) {
								if (VarCalculations.lastWriteVar.getTranscripts().get(i).getGene().samples
										.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
									found = true;
								} else {
									VarCalculations.lastWriteVar.getTranscripts().remove(i);
									i--;
								}
							}
						}
					}
					if (found) {

						writeNodeToFile(VarCalculations.lastWriteVar, VarCalculations.lastWriteVar.getChrom(), output, outputgz);

					}

					VarCalculations.lastWriteVar = VarCalculations.lastWriteVar.getNext();
				}

			}

		} else {
			if (vardraw == null && VariantHandler.table.genearray.size() > 0) {
				for (int i = 0; i < VariantHandler.table.genearray.size(); i++) {
					writeTranscriptToFile(VariantHandler.table.genearray.get(i), output);
				}
				VariantHandler.table.genearray.clear();
			}
		}
	}
public static void writeNodeToFile(final VarNode node, final String chrom, final BufferedWriter output,	final BlockCompressedOutputStream outputgz) {
		try {
			if (!node.inVarList) return;
			if (VariantHandler.vcf.isSelected()) {
				final StringBuffer info = new StringBuffer("");
				final StringBuffer alts = new StringBuffer("");
				final StringBuffer refs = new StringBuffer("");			
				final StringBuffer sampleinfos = new StringBuffer("");
				double avgquality = 0;
				int samplecount = 0, var = -1;
				String rscode = ".";
				String ref = MainPane.getBase.get(node.getRefBase());
				final int AN = MainPane.varsamples * 2;
				int AC = 0, allelenro = 1;
				final StringBuffer ACs = new StringBuffer("");
				final StringBuffer AFs = new StringBuffer("");
				double AF = 0.0;
				String sampleinfo = null;			
				final HashMap<Integer, String> samplehash = new HashMap<Integer, String>();
				boolean set = false, found = false;

				for (Map.Entry<String, ArrayList<SampleNode>> entry: node.vars.entrySet()) {
					var++;
					if (node.hideNodeVar(entry)) continue;
					
					AF = 0;
					AC = 0;
					avgquality = 0;
					if (node.vars.size() == 1) {
						if (node.indel && entry.getKey().length() > 1) {
							final String[] result = MethodLibrary.makeIndelColumns(chrom, node.getPosition(), ref,entry.getKey());
							ref = result[0];								
							alts.append(result[1] + ",");
						} else {
							alts.append(entry.getKey() + ",");
						}
					} else {

						if (node.indel) {
							if (!set) {
								final String[] result = MethodLibrary.makeMultiAlt(chrom, node.getPosition(), ref, node);
								set = true;
								ref = result[0];
													
								alts.append(result[1]);
							}
							allelenro = var + 1;

						} else {
							if (refs.length() == 0)	refs.append(ref + ",");							
							if (alts.length() > 0) allelenro++;
							
							alts.append(entry.getKey() + ",");
						}
					}					
				
					for (int s = 0; s < entry.getValue().size(); s++) {
						SampleNode sampleNode = entry.getValue().get(s);
						Sample sample = sampleNode.getSample();
						int sampleIndex = sample.getMultiIndex();

						if (!sampleNode.hideVar(entry.getKey().length() > 1)) {
							found = true;
							if (samplehash.containsKey(sample.getMultiIndex())) { // multiallelic							
								AC++;
								String[] sampleInfo = samplehash.get(sampleIndex).split(":");
								String newGT = sampleInfo[0] + "/" + allelenro;
								String ADs = sampleInfo[2];
								
								if (!sampleNode.hasNoref()) {								
									String[] oldCoverages = ADs.split(",");
									oldCoverages[0] = "" +(Integer.parseInt(oldCoverages[0]) - sampleNode.getCalls());
									ADs = oldCoverages[0];
									
									for (int i = 1; i < oldCoverages.length; i++) {
										ADs += "," + oldCoverages[i];
									}									
								}
								ADs += "," +sampleNode.getCalls();
								
								sampleinfo = newGT + ":" + sampleNode.getCoverage()	+ ":" + ADs + ":" + sampleInfo[3] +"," +sampleNode.getGQString();
								samplehash.put(sampleIndex, sampleinfo);
							} else if (sampleNode.isHomozygous()) {
								AC += 2;
								String ADs = "" +(sampleNode.getCoverage() - sampleNode.getCalls()) +"," +sampleNode.getCalls();								
								sampleinfo = allelenro + "/" + allelenro + ":"	+sampleNode.getCoverage() + ":" + ADs + ":"	+ sampleNode.getGQString();
								samplehash.put(sampleIndex, sampleinfo);
							} else {
								AC++;								
								String ADs = sampleNode.hasNoref() ? "0," +sampleNode.getCalls() : "" +(sampleNode.getCoverage() - sampleNode.getCalls()) +"," +sampleNode.getCalls();
								
								String newGT = sampleNode.hasNoref() ? "" : "0/";
								
								sampleinfo = newGT + allelenro + ":" + sampleNode.getCoverage() + ":" + ADs + ":" + sampleNode.getGQString();								
								samplehash.put(sampleIndex, sampleinfo);
							}
							samplecount++;
							if (sampleNode.getQuality() != null) avgquality += sampleNode.getQuality();							
						}
					}
					AF = MethodLibrary.round(AC / (double) AN, 5);
					ACs.append(AC + ",");
					AFs.append(AF + ",");
				}

				if (!found) return;
				
				if (node.rscode != null) rscode = node.rscode;
				
				for (int i = 0; i < MainPane.varsamples; i++) {
					if (samplehash.containsKey(i)) sampleinfos.append("\t" + samplehash.get(i));
					else sampleinfos.append("\t" + "0/0");
				}

				alts.deleteCharAt(alts.length() - 1);
				AFs.deleteCharAt(AFs.length() - 1);
				ACs.deleteCharAt(ACs.length() - 1);
				info.append("AN=" + AN + ";AC=" + ACs + ";AF=" + AFs);

				if (outputgz != null) {

					final String writeline = chrom + "\t" + (node.getPosition() + 1) + "\t" + rscode + "\t" + ref + "\t" + alts + "\t" + MethodLibrary.round(avgquality / (double) samplecount, 2) + "\tPASS\t"
							+ info + "\t" + VariantHandler.format + sampleinfos + MainPane.lineseparator;
					
					outputgz.write(writeline.getBytes());
						
					try {
						final Feature vcf = VariantHandler.vcfCodec.decode(writeline);
						
						FileRead.indexCreator.addFeature(vcf, FileRead.filepointer);
						FileRead.filepointer = outputgz.getFilePointer();
					
					
					} catch(Exception e) {
						e.printStackTrace();
						System.out.println(node.getPosition() +" " + alts);
					}					
				} else if (output != null) {

					output.write(chrom + "\t" + (node.getPosition() + 1) + "\t" + rscode + "\t" + ref + "\t" + alts
							+ "\t" + MethodLibrary.round(avgquality / (double) samplecount, 2) + "\t.\t" + info + "\t"
							+ VariantHandler.format + sampleinfos + MainPane.lineseparator);
				} else {
					// System.out.println(chrom +"\t" +(node.getPosition()+1) +"\t" +rscode +"\t"
					// +ref +"\t" +alts +"\t" +MethodLibrary.round(avgquality/(double)samplecount,2)
					// +"\t.\t" +info +"\t" +format +sampleinfos);
				}
			} else if (VariantHandler.oncodrive.isSelected()) {
				String[] result = null;
				for (Map.Entry<String, ArrayList<SampleNode>> entry: node.vars.entrySet()) {
					if (node.hideNodeVar(entry)) continue;
					
					if (node.indel && entry.getKey().length() > 1) 
						result = MethodLibrary.makeIndelColumns(node.getChrom(), node.getPosition(), MainPane.getBase.get(node.getRefBase()), entry.getKey());
					
					for (int i = 0; i < entry.getValue().size(); i++) {

						if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) continue;						

						try {
							if (output != null) {

								if (node.indel && entry.getKey().length() > 1) {
									if (result[0].length() > 1) {
										output.write(node.getChrom() + "\t" + (node.getPosition() + 2) + "\t"+ result[0].substring(1) + "\t-\t" + entry.getValue().get(i).getSample().getName() + MainPane.lineseparator);
									} else {
										output.write(node.getChrom() + "\t" + (node.getPosition() + 2) + "\t-\t" + result[1].substring(1) + "\t" + entry.getValue().get(i).getSample().getName() + MainPane.lineseparator);
									}									
								} else {

									output.write(node.getChrom() + "\t" + (node.getPosition() + 1) + "\t" + MainPane.getBase.get(node.getRefBase()) + "\t" + entry.getKey() + "\t"	+ entry.getValue().get(i).getSample().getName() + MainPane.lineseparator);

								}

							}

						} catch (final Exception ex) {
							ex.printStackTrace();
							ErrorLog.addError(ex.getStackTrace());
						}
					}
				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeTranscriptToFile(final Gene gene, final BufferedWriter output) {
		try {
			VarNode node = null;
			SampleNode varnode;
			String[] row;
			StringBuffer[] bedarray;
			String rscode, aminochange, transcripts, exons;
			StringBuffer strand = new StringBuffer("");
			int casefreq = 0;
			String genotype = "", biotype = ".", geneID = "", description = "";
			StringBuffer controls = new StringBuffer(""), tracks = new StringBuffer("");
			String clusters = "";
			final HashMap<ControlFile, SampleNode> temphash = new HashMap<ControlFile, SampleNode>();
			AminoTable table = VariantHandler.table;
			table.getAminos(gene);
			StringBuffer samples = new StringBuffer("");
			StringBuffer genotypes = new StringBuffer("");
			StringBuffer qualities = new StringBuffer("");
			StringBuffer GQualities = new StringBuffer("");
			StringBuffer fractions = new StringBuffer("");
			double fraction = 0;
			
			if (VariantHandler.geneTsv.isSelected()) {
				output.write(gene.getName() + "\t" + gene.mutations + "\t" + gene.samples.size() + "\t");
				for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
					if (!gene.samples.contains(Getter.getInstance().getSampleList().get(i))) { output.write("0\t"); continue; }
					String aminos ="";
					for (int s = 0; s < table.aminoarray.size(); s++) {
						row = table.aminoarray.get(s).getRow();
						node = table.aminoarray.get(s).getNode();
						for (Map.Entry<String, ArrayList<SampleNode>> entry: node.vars.entrySet()) {
							if (node.hideNodeVar(entry)) {
								continue;
							}
							if (!entry.getKey().equals(row[5])) {
								continue;
							}
							
							for (int e = 0; e < entry.getValue().size(); e++) {

								if (entry.getValue().get(e).hideVar(entry.getKey().length() > 1)) {
									continue;
								}
	
								varnode = entry.getValue().get(e);
								if (varnode.getSample().equals(Getter.getInstance().getSampleList().get(i))) {
									aminos += MethodLibrary.aminoEffect(row[3]) +";";
									//System.out.println(varnode.getSample().getName() +" " +MethodLibrary.aminoEffect(row[3]));									
								}
							}
							
						}
					}
					output.write(aminos.substring(0, aminos.length()-1) +"\t");
				}
				output.write(MainPane.lineseparator);
				/* for (int s = 0; s < table.aminoarray.size(); s++) {
					for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
						if (Getter.getInstance().getSampleList().get(i).calledvariants || (Getter.getInstance().getSampleList().get(i).getTabixFile() != null || Getter.getInstance().getSampleList().get(i).multipart) && !Getter.getInstance().getSampleList().get(i).removed) {
							
							if (gene.samples.contains(Getter.getInstance().getSampleList().get(i))) {							
								output.write("1\t");
							} else {
								output.write("0\t");
							}
						}
					}
					output.write(MainPane.lineseparator);
				} */
				MainPane.drawCanvas.loadBarSample = (int) ((gene.getStart() / (double) Draw.getSplits().get(0).chromEnd) * 100);
				varnode = null;
				node = null;
				return;
			}
			for (int s = 0; s < table.aminoarray.size(); s++) {
				row = table.aminoarray.get(s).getRow();
				node = table.aminoarray.get(s).getNode();
				strand = new StringBuffer("");
				if (node.isRscode() != null) {
					rscode = node.isRscode();
				} else {
					rscode = "N/A";
				}

				if (row[8].contains("troni")) {
					exons = "Intronic";
				} else if (row[8].contains("geni")) {
					exons = "Intergenic";
				} else {
					if (row[8].length() == 1) {
						exons = "Exon " + row[8];
					} else if (row[8].length() > 1) {
						exons = "Exons " + row[8];
					} else {
						exons = "";
					}
				}
				if (gene.intergenic) {
					if (node.getTranscripts().size() == 2) {
						geneID = gene.getID() + ";" + node.getTranscripts().get(1).getGene().getID();
						description = gene.getDescription() + ";"
								+ node.getTranscripts().get(1).getGene().getDescription();
						strand.append((gene.getStrand()) ? "+;" : "-;");
						strand.append((node.getTranscripts().get(1).getGene().getStrand()) ? "+" : "-");
					} else {
						geneID = gene.getID();
						description = gene.getDescription();
					}
				} else {
					if (!gene.getStrand()) {
						strand.append("-");
					} else {
						strand.append("+");
					}
					geneID = gene.getID();
					description = gene.getDescription();
				}
				transcripts = row[6];
				biotype = row[7];

				clusters = "";

				if (MainPane.projectValues.variantHandlerValues.commonVariantsMin > 1 && VariantHandler.clusterSize > 0) {
					if (node.clusterNode != null) {
						clusters = node.clusterNode.ID + "\t" + node.clusterNode.nodecount + "\t"
								+ node.clusterNode.width + "\t"
								+ MethodLibrary.round(node.clusterNode.nodecount / (double) node.clusterNode.width, 2)
								+ "\t";
					} else {
						// System.out.println(node.getPosition());
						// continue;
					}
				}

				for (Map.Entry<String, ArrayList<SampleNode>> entry: node.vars.entrySet()) {
					if (node.hideNodeVar(entry)) {
						continue;
					}
					if (!entry.getKey().equals(row[5])) {
						continue;
					}

					if (exons.length() > 0) {
						aminochange = row[3];
					} else {
						aminochange = "N/A";
					}

					if (MainPane.bedCanvas.bedOn) {

						tracks = new StringBuffer("");

						bedarray = MethodLibrary.makeTrackArray(node, entry.getKey(), false);
						if (bedarray != null) {
							for (int b = 0; b < bedarray.length; b++) {
								if (!MainPane.bedCanvas.bedTrack.get(b).intersect) {
									continue;
								}
								if (bedarray[b] != null) {
									tracks.append(bedarray[b].toString() + "\t");
								} else {
									tracks.append("-\t");
								}
							}
						} else {
							for (int i = 0; i < MainPane.bedCanvas.bedTrack.size(); i++) {
								if (MainPane.bedCanvas.bedTrack.get(i).intersect) {
									tracks.append("\t");
								}
							}
						}
					}

					if (Control.controlData.controlsOn && entry.getValue().size() > 0) {

						casefreq = 0;
						controls = new StringBuffer("");
						temphash.clear();
						for (int e = entry.getValue().size() - 1; e > -1; e--) {

							if (entry.getValue().get(e).alleles == null) {
								if (entry.getValue().get(e).isHomozygous()) {
									casefreq += 2;
								} else {
									casefreq++;
								}
							} else {
								if (!entry.getValue().get(e).getControlSample().controlOn) {
									continue;
								}
								temphash.put(entry.getValue().get(e).getControlSample(), entry.getValue().get(e));

							}
						}
						ArrayList<ControlFile> controlarray = VariantHandler.controlarray;
						for (int i = 0; i < controlarray.size(); i++) {
							if (temphash.containsKey(controlarray.get(i))) {
								// AF
								controls.append(
										MethodLibrary
												.round(temphash.get(controlarray.get(i)).alleles
														/ (double) temphash.get(controlarray.get(i)).allelenumber, 5)
												+ "\t");
								// OR
								controls.append(MethodLibrary
										.round((casefreq / (double) (MainPane.varsamples * 2 - casefreq))
												/ (temphash.get(controlarray.get(i)).alleles
														/ (double) (temphash.get(controlarray.get(i)).allelenumber
																- temphash.get(controlarray.get(i)).alleles)),
												5)
										+ " (p="
										+ MethodLibrary.round(VariantHandler.table.fe
												.getRightTailedP(casefreq, MainPane.varsamples * 2 - casefreq,
														temphash.get(controlarray.get(i)).alleles,
														temphash.get(controlarray.get(i)).allelenumber
																- temphash.get(controlarray.get(i)).alleles),
												12)
										+ ")\t");
							} else {
								controls.append("N/A\tN/A\t");
							}
						}
					}

					if (VariantHandler.compactTsv.isSelected()) {
						samples = new StringBuffer("");
						genotypes = new StringBuffer("");
						qualities = new StringBuffer("");
						GQualities = new StringBuffer("");
						fractions = new StringBuffer("");
						StringBuffer filters = new StringBuffer("");
						for (int i = 0; i < entry.getValue().size(); i++) {

							if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) continue;

							varnode = entry.getValue().get(i);

							if (varnode.isHomozygous()) {
								genotypes.append("Hom(" + varnode.getCalls() + "/" + varnode.getCoverage() + ");");
							} else {
								genotypes.append("Het(" + varnode.getCalls() + "/" + varnode.getCoverage() + ");");
							}
							fraction = MethodLibrary.round(varnode.getCalls() / (double) varnode.getCoverage(), 2);
							fractions.append(fraction + ";");
							samples.append(varnode.getSample().getName() + ";");
							qualities.append(varnode.getQuality() + ";");
							GQualities.append(varnode.getGQString() + ";");
							filters.append(varnode.filterField +";");
						}

						genotypes.deleteCharAt(genotypes.length() - 1);
						qualities.deleteCharAt(qualities.length() - 1);
						samples.deleteCharAt(samples.length() - 1);
						GQualities.deleteCharAt(GQualities.length() - 1);
						fractions.deleteCharAt(fractions.length() - 1);
						filters.deleteCharAt(filters.length() - 1);
						if (output != null) {
							String region = exons;
							if (exons.length() > 0 && aminochange.equals("N/A")) region = "UTR";
							if (exons.length() == 0 && node.getTranscripts() != null && node.isInGene()) region = "Intronic";
							if (exons.length() == 0 && !node.isInGene()) region = "Intergenic";
							
							output.write(samples + "\t" + row[0] + "\t" + row[1] + "\t" + gene.samples.size()
									+ "\t" + geneID + "\t" + transcripts + "\t" + biotype + "\t" + row[2] + "\t"
									+ strand + "\t" + region + "\t" + aminochange + "\t"
									+ MainPane.getBase.get(node.getRefBase()) + "->" + entry.getKey() + "\t"
									+ genotypes + "\t" + fractions + "\t" + qualities + "\t" + GQualities + "\t" +filters + "\t"
									+ rscode + "\t" + clusters + controls + tracks + description
									+ MainPane.lineseparator);
								
						}

					}

					else if (VariantHandler.tsv.isSelected()) {
						for (int i = 0; i < entry.getValue().size(); i++) {

							if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) {
								continue;
							}

							varnode = entry.getValue().get(i);

							if (varnode.isHomozygous()) {
								genotype = "Hom(" + varnode.getCalls() + "/" + varnode.getCoverage() + ")";
							} else {
								genotype = "Het(" + varnode.getCalls() + "/" + varnode.getCoverage() + ")";
							}
							fraction = MethodLibrary.round(varnode.getCalls() / (double) varnode.getCoverage(), 2);

							try {
								if (output != null) {
									String region = exons;
									
									if (exons.length() > 0 && aminochange.equals("N/A")) region = "UTR";
									if (exons.length() == 0 && node.getTranscripts() != null && node.isInGene()) region = "Intronic";
									if (exons.length() == 0 && !node.isInGene()) region = "Intergenic";

									output.write(varnode.getSample().getName() + "\t" + row[0] + "\t" + row[1]
												+ "\t" + gene.samples.size() + "\t" + geneID + "\t" + transcripts
												+ "\t" + biotype + "\t" + row[2] + "\t" + strand + "\t" + region
												+ "\t" + aminochange + "\t" + MainPane.getBase.get(node.getRefBase())
												+ "->" + entry.getKey() + "\t" + genotype + "\t" + fraction + "\t"
												+ varnode.getQuality() + "\t" + varnode.getGQString() + "\t" +varnode.filterField +"\t"
												+ rscode + "\t" + clusters + controls + tracks + description
												+ MainPane.lineseparator);
																			
								}
							} catch (final Exception ex) {
								ex.printStackTrace();
								ErrorLog.addError(ex.getStackTrace());
							}
						}
					} else if (VariantHandler.oncodrive.isSelected()) {
						String[] result = null;
						
						if (node.indel && entry.getKey().length() > 1) {
							result = MethodLibrary.makeIndelColumns(node.getChrom(), node.getPosition(), MainPane.getBase.get(node.getRefBase()), entry.getKey());
						}
						for (int i = 0; i < entry.getValue().size(); i++) {

							if (entry.getValue().get(i).hideVar(entry.getKey().length() > 1)) {
								continue;
							}

							try {
								if (output != null) {
									if (node.indel && entry.getKey().length() > 1) {
										if (result[0].length() > 1) {
											output.write(node.getChrom() + "\t" + (node.getPosition() + 1) + "\t"	+ result[0].substring(1) + "\t-\t" 	+ entry.getValue().get(i).getSample().getName()
													+ MainPane.lineseparator);
										} else {
											output.write(node.getChrom() + "\t" + (node.getPosition() + 1) + "\t-\t" + result[1].substring(1) + "\t" + entry.getValue().get(i).getSample().getName()
													+ MainPane.lineseparator);
										}
									} else {
										output.write(node.getChrom() + "\t" + (node.getPosition() + 1) + "\t" + MainPane.getBase.get(node.getRefBase()) + "\t" + entry.getKey() + "\t"
												+ entry.getValue().get(i).getSample().getName()
												+ MainPane.lineseparator);
									}
								}
							} catch (final Exception ex) {
								ex.printStackTrace();
								ErrorLog.addError(ex.getStackTrace());
							}
						}
						
					}
				}
			}
			MainPane.drawCanvas.loadBarSample = (int) ((gene.getStart() / (double) Draw.getSplits().get(0).chromEnd) * 100);
			varnode = null;
			node = null;
		} catch (final Exception exc) {
			JOptionPane.showMessageDialog(MainPane.chromDraw, exc.getMessage() + " - there were problem writing variants for " + gene.getName(), "Error", JOptionPane.ERROR_MESSAGE);
			exc.printStackTrace();
			ErrorLog.addError(exc.getStackTrace());
		}

	}
	/* String getWorstChange() {
		String worst = ""
	} */
	public static void writeGeneListToVCF(final BufferedWriter output, final BlockCompressedOutputStream outputgz) {
		boolean found = false;
		//VarNode node = Getter.getInstance().getVariantHead().getNextVisible();
		VarNode node = null; //VariantHandler.table.genearray.get(0).varnodes.get(0);
		if (VariantHandler.table.genearray.size() > 0 && VariantHandler.table.genearray.get(0).varnodes != null	&& VariantHandler.table.genearray.get(0).varnodes.size() > 0) {
			node = VariantHandler.table.genearray.get(0).varnodes.get(0);
		}
		
		while (node != null) {
			if (VariantHandler.vcf.isSelected() || MainPane.projectValues.variantHandlerValues.compareGene < 2) {
				writeNodeToFile(node, node.getChrom(), null, outputgz);
				
				node = node.getNextVisible();
				continue;
			}
			
			found = false;
			if (node.isInGene()) {
				if (node.getExons() != null) {
					for (int i = 0; i < node.getExons().size(); i++) {
						if (node.getExons().get(i).getTranscript().getGene().samples.size() >= VariantHandler.geneSlider
								.getValue()) {
							found = true;
						} else {
							node.getExons().remove(i);
							i--;
						}
					}
				} else {
					for (int i = 0; i < node.getTranscripts().size(); i++) {
						if (node.getTranscripts().get(i).getGene().samples.size() >= MainPane.projectValues.variantHandlerValues.compareGene) {
							found = true;
						} else {
							node.getTranscripts().remove(i);
							i--;
						}
					}
				}
			}
			if (found) {
				writeNodeToFile(node, node.getChrom(), null, outputgz);
			}
			
			node = node.getNextVisible();
		}
		node = null;

	}
}
