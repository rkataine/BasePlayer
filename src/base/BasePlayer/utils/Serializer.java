/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
//import java.io.Serializable;
import java.util.ArrayList;

import base.BasePlayer.Getter;
import base.BasePlayer.Main;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.Control;
import base.BasePlayer.sample.Sample;

public class Serializer {
	
	
	public void serialize(File outfile) {
	
		try {
			FileOutputStream fout = new FileOutputStream(outfile);
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			ArrayList<Sample> sampleTemp = new ArrayList<Sample>();
			for(int i = 0; i<Getter.getInstance().getSampleList().size(); i++) {
				if(!Getter.getInstance().getSampleList().get(i).multipart) {
					sampleTemp.add(Getter.getInstance().getSampleList().get(i));
				}
			}
			oos.writeObject(sampleTemp);
			sampleTemp =null;
			oos.writeObject(Draw.getSplits());
			Draw.drawVariables.scrollbarpos = MainPane.drawScroll.getVerticalScrollBar().getValue();
			Draw.drawVariables.projectFile = outfile;
			Draw.drawVariables.projectName = outfile.getName().substring(0, outfile.getName().indexOf(".ses"));
			Main.frame.setTitle("BasePlayer - Project: " +Draw.drawVariables.projectName);
			
			oos.writeObject(Draw.drawVariables);
			oos.writeObject(Control.controlData);
			oos.writeObject(MainPane.bedCanvas.bedTrack);
			
			oos.writeObject(Settings.settings);
			//VariantHandler.saveValues();
			oos.writeObject(VariantHandler.variantSettings);
			
			oos.flush();
			oos.close();
				
			
		}
		catch(Exception e) { 
			e.printStackTrace();
		}
	}
}
