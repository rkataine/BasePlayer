package base.BasePlayer.utils;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import base.BasePlayer.Getter;
import base.BasePlayer.Main;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.io.FileRead;

public class ScreenPhotos extends SwingWorker<String, Object> {

  protected String doInBackground() {

    process();

    return "";
  }

  void process() {
    try {
      final int readDistance = Settings.settings.get("coverageDrawDistance");
      System.out.println(MainPane.samples);
      //final InputStream in = null;
      String[] split;
      BufferedReader reader = null;
      GZIPInputStream gzip = null;
      FileReader freader = null;
      final File trackfile = MainPane.bedCanvas.bedTrack.get(0).file;

      if (trackfile != null) {
        if (trackfile.getName().endsWith(".gz") || trackfile.getName().endsWith(".bgz")) {
          gzip = new GZIPInputStream(new FileInputStream(trackfile));
          reader = new BufferedReader(new InputStreamReader(gzip));
        } else {
          freader = new FileReader(trackfile);
          reader = new BufferedReader(freader);
        }
      } else {
        return;
      }
      String line;
      int start, end = 0;
      String chr = "1";
      while ((line = reader.readLine()) != null) {
        split = line.split("\t");
        try {
          chr = split[0];
          start = Integer.parseInt(split[1]);
          end = Integer.parseInt(split[2]);
        } catch (final Exception ex) {
          continue;
        }
        FileRead.search = true;
        MainPane.nothread = true;
        if (end - start > Settings.settings.get("coverageDrawDistance")) {
          Settings.settings.put("coverageDrawDistance", (end - start) + 100);
          //Settings.coverageDrawDistance = (end - start) + 100;
        }
        MainPane.drawCanvas.gotoPos(chr, start, end);
        MainPane.drawCanvas.clearReads(Draw.getSplits().get(0));

        for (int i = 0; i < MainPane.samples; i++) {

          Draw.drawVariables.setVisibleStart.accept(i);
          Draw.updateReads = true;

          FileRead.readsLoaded = false;

          Draw.setScrollbar((int) (i * Draw.drawVariables.sampleHeight));
          while (!FileRead.readsLoaded) {
            try {
              Thread.sleep(1000);
            } catch (final InterruptedException ex) {
              // do stuff
            }

          }
          while (Getter.getInstance().loading()) {
            try {
              Thread.sleep(1000);
            } catch (final InterruptedException ex) {
              // do stuff
            }
          }
          Thread.sleep(1000);
          try {
            final BufferedImage image = new Robot().createScreenCapture(new Rectangle(Main.frame.getX() + 10,
            Main.frame.getY() + 5, Main.frame.getBounds().width - 18, Main.frame.getBounds().height - 13));
            ImageIO.write(image, "png",
                new File(split[3] + "_" + base.BasePlayer.Getter.getInstance().getSampleList().get(i).getName() + ".png"));
          } catch (final Exception ex) {
            ex.printStackTrace();
          }
        }

      }
      Settings.settings.put("coverageDrawDistance", readDistance);
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }

}
