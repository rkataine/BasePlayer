package base.BasePlayer.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.SwingWorker;

import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.BedTrack;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.io.FileRead;
import base.BasePlayer.reads.ReadNode;
import base.BasePlayer.sample.Sample;
import htsjdk.samtools.SAMRecord;

public class Seqfetcher extends SwingWorker<String, Object> {
  File file, outfile;
  BedTrack track;

  public Seqfetcher(final File file, final File outfile) {
    this.file = file;
    this.outfile = outfile;
    Settings.softClips = 1;
  }

  void fetchSeq() {
    BufferedReader reader = null;
    BufferedWriter writer = null;
    try {
      reader = new BufferedReader(new FileReader(file));
      writer = new BufferedWriter(new FileWriter(outfile));
      String line, chrom, position, sample;
      String[] splitter, positionsplit;
      final FileRead readreader = new FileRead();
      /*
       * while(!reader.readLine().split("\\s+")[0].equals("19:52486706")) {
       * 
       * }
       */
      boolean firsterror = true;
      final StringBuffer nonfounds = new StringBuffer("");
      while ((line = reader.readLine()) != null) {
        if (!Getter.getInstance().loading()) {
          writer.close();
          break;
        }
        splitter = line.split("\\s+");

        positionsplit = splitter[0].split(":");
        chrom = positionsplit[0];
        position = positionsplit[1];
        sample = splitter[1];
        VariantHandler.hideIndels.setSelected(true);
        VariantHandler.hideSNVs.setSelected(true);

        if (!MainPane.zoomtopos(chrom, position, sample)) {
          if (firsterror) {
            MainPane.showError(
                "Sample: " + sample
                    + " not found in opened samples.\nCheck error log for more missing files.",
                "Error");
            ErrorLog.addError(sample + " not found.");
            nonfounds.append(sample);
            firsterror = false;
          } else {
            if (!nonfounds.toString().contains(sample)) {
              nonfounds.append(sample);
              ErrorLog.addError(sample + " not found.");
            }
          }
          continue;
        }

        final SplitClass split = Draw.getSplits().get(0);
        final Sample readsample = base.BasePlayer.Getter.getInstance().getSampleList().get(Draw.drawVariables.getVisibleStart.get());
        ReadNode read;

        final int centerpos = Integer.parseInt(position);

        int minpos = Integer.MAX_VALUE, maxpos = 0;
        final ArrayList<Object[]> readlist = new ArrayList<Object[]>();
        readreader.splitIndex = split;
        if (readsample.getreadHash().get(split) == null) {
          readsample.resetreadHash();
        }
        readreader.getReads(chrom, centerpos - 100, centerpos + 100, readsample.getreadHash().get(split),
            split);

        if (readsample.getreadHash().get(split) == null) {
          readsample.resetreadHash();
        }
        for (int i = 0; i < readsample.getreadHash().get(split).getReads().size(); i++) {

          read = readsample.getreadHash().get(split).getReads().get(i);
          do {
            if (read.getPosition() < centerpos && read.getPosition() + read.getWidth() > centerpos) {
              if (read.getMismatches() != null && read.getMismatches().size() > 10) {
                if (read.getPosition() < minpos) {
                  minpos = read.getPosition();
                }
                if (read.getPosition() + read.getWidth() > maxpos) {
                  maxpos = read.getPosition() + read.getWidth();
                }
                final SAMRecord readsam = MainPane.fileReader.getRead(
                  MenuBar.chromosomeDropdown.getSelectedItem().toString(), read.getPosition(),
                    read.getPosition() + read.getWidth(), read.getName(),
                    readsample.getreadHash().get(split));
                try {
                  final Object[] adder = { read.getPosition(), readsam.getReadString() };
                  readlist.add(adder);
                } catch (final Exception e) {
                  continue;
                }

              }
            }

          } while ((read = read.getNext()) != null);
        }
        if (maxpos - minpos < 10) {
          final String error = ">" + readsample.getName() + "|BP="
              + MenuBar.chromosomeDropdown.getSelectedItem().toString() + ":"
              + MethodLibrary.formatNumber(centerpos)
              + " (Breakpoint mismatches not found))\nNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN";

          writer.write(error + "\n");
          continue;
        }
        final int[][] matrix = new int[5][maxpos - minpos];

        for (int i = 0; i < 5; i++) {
          for (int j = 0; j < maxpos - minpos; j++) {
            matrix[i][j] = 0;
          }
        }
        for (int i = 0; i < readlist.size(); i++) {
          for (int j = 0; j < readlist.get(i)[1].toString().length(); j++) {
            if ((int) readlist.get(i)[0] - minpos + j >= matrix[0].length) {
              break;
            }
            matrix[MainPane.baseMap.get((byte) (readlist.get(i)[1].toString().charAt(j)))
                - 1][(int) readlist.get(i)[0] - minpos + j]++;
          }
        }
        final StringBuffer buffer = new StringBuffer("");
        read = null;
        
        buffer.append(">" + readsample.getName() + "|BP=" + MenuBar.chromosomeDropdown.getSelectedItem().toString()
            + ":" + MethodLibrary.formatNumber(centerpos) + " (LeftPosition="
            + MenuBar.chromosomeDropdown.getSelectedItem().toString() + ":" + MethodLibrary.formatNumber(minpos)
            + ")\n");
        int max = 0, maxindex = 0;
        final String[] bases = { "A", "C", "G", "T" };
        final StringBuffer fasta = new StringBuffer("");
        for (int j = 0; j < maxpos - minpos; j++) {
          max = 0;
          maxindex = 0;
          for (int i = 0; i < 4; i++) {
            if (matrix[i][j] > max) {
              max = matrix[i][j];
              maxindex = i;
            } else if (max > 0 && matrix[i][j] == max) {
              max = 0;
              maxindex = -1;
            }
          }
          if (maxindex > -1) {
            fasta.append(bases[maxindex]);
          } else {
            fasta.append("N");
          }
        }
        buffer.append(fasta.toString());
        // System.out.println(buffer.toString());
        writer.write(buffer.toString() + "\n");

      }
      reader.close();
      writer.close();
      MainPane.nothread = false;
      MainPane.noreadthread = false;
      FileRead.search = false;
      Draw.variantcalculator = false;
      
      MainPane.chromDraw.updateExons = true;
      MainPane.chromDraw.repaint();

      MainPane.showError("Fasta file ready!", "Note");
    } catch (final Exception e) {
      try {
        if (reader != null && writer != null) {
          reader.close();
          writer.close();
        }
      } catch (final Exception ex) {

      }
      MainPane.nothread = false;
      MainPane.noreadthread = false;
      FileRead.search = false;
      MainPane.chromDraw.updateExons = true;
      MainPane.chromDraw.repaint();
      e.printStackTrace();
    }
  }

  protected String doInBackground() {
    Loader.setLoading("Writing fasta");
    fetchSeq();
    Loader.ready("Writing fasta");
    return "";
  }
/* public static void getConsSeq() {

		final Sample readsample = drawCanvas.sampleList.get(Draw.drawVariables.getVisibleStart.get());
		final SplitClass split = Draw.getSplits().get(0);
		ReadNode read;
		final int centerpos = chromDraw.getPosition((int) (drawCanvas.getDrawWidth() / 2.0 + split.pixel / 2),
				split);
		int minpos = Integer.MAX_VALUE, maxpos = 0;
		final ArrayList<Object[]> readlist = new ArrayList<Object[]>();

		for (int i = 0; i < readsample.getreadHash().get(split).getReads().size(); i++) {

			read = readsample.getreadHash().get(split).getReads().get(i);
			do {
				if (read.getPosition() < centerpos && read.getPosition() + read.getWidth() > centerpos) {
					if (read.getMismatches() != null && read.getMismatches().size() > 10) {
						if (read.getPosition() < minpos) {
							minpos = read.getPosition();
						}
						if (read.getPosition() + read.getWidth() > maxpos) {
							maxpos = read.getPosition() + read.getWidth();
						}
						final SAMRecord readsam = fileReader.getRead(
							MenuBar.chromosomeDropdown.getSelectedItem().toString(), read.getPosition(),
								read.getPosition() + read.getWidth(), read.getName(),
								readsample.getreadHash().get(split));

						final Object[] adder = { read.getPosition(), readsam.getReadString() };
						readlist.add(adder);
					}
				}
			} while ((read = read.getNext()) != null);
		}

		final int[][] matrix = new int[5][maxpos - minpos];

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < maxpos - minpos; j++) {
				matrix[i][j] = 0;
			}
		}
		for (int i = 0; i < readlist.size(); i++) {
			for (int j = 0; j < readlist.get(i)[1].toString().length(); j++) {

				matrix[baseMap.get((byte) (readlist.get(i)[1].toString().charAt(j))) - 1][(int) readlist.get(i)[0]
						- minpos + j]++;
			}
		}
		final StringBuffer buffer = new StringBuffer("");

		read = null;

		buffer.append(">" + readsample.getName() + "|BP=" + MenuBar.chromosomeDropdown.getSelectedItem().toString() + ":"
				+ centerpos + " (LeftPosition=" + MenuBar.chromosomeDropdown.getSelectedItem().toString() + ":"
				+ MethodLibrary.formatNumber(minpos) + ")\n");
		int max = 0, maxindex = 0;
		final String[] bases = { "A", "C", "G", "T" };
		final StringBuffer fasta = new StringBuffer("");
		for (int j = 0; j < maxpos - minpos; j++) {
			max = 0;
			maxindex = 0;
			for (int i = 0; i < 4; i++) {
				if (matrix[i][j] > max) {
					max = matrix[i][j];
					maxindex = i;
				} else if (max > 0 && matrix[i][j] == max) {
					max = 0;
					maxindex = -1;
				}
			}
			if (maxindex > -1) {
				fasta.append(bases[maxindex]);
			} else {
				fasta.append("N");
			}
		}
		buffer.append(fasta.toString());

		System.out.println(buffer.toString());
	} */
}