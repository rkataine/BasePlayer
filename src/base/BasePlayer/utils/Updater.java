package base.BasePlayer.utils;

import java.awt.*;
import java.util.ArrayList;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.net.URLConnection;
import javax.swing.*;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.Main;
import base.BasePlayer.ProxySettings;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.*;
import base.BasePlayer.GUI.MenuBar;
public class Updater extends SwingWorker<String, Object> {
  private static Type type;
	private static String address;
	private static int port;
  private class DefaultTrustManager implements X509TrustManager {

    @Override
    public void checkClientTrusted(final X509Certificate[] arg0, final String arg1)
        throws CertificateException {
    }

    @Override
    public void checkServerTrusted(final X509Certificate[] arg0, final String arg1)
        throws CertificateException {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }
  }

  public void downloadFile(final String fileURL, final String saveDir) throws IOException {

    System.out.println("Updating file from: " + fileURL + "\nSaving to: " + saveDir);
    System.out.println("Opening connection...");
    try {
      final URL url = new URL(fileURL);

      // HttpsURLConnection httpConn = (HttpsURLConnection) url.openConnection();
      // httpConn.connect();
      final SSLContext ctx = SSLContext.getInstance("TLS");
      ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
      SSLContext.setDefault(ctx);
      HttpsURLConnection httpCon = null;
      Proxy proxy = null;
      if (ProxySettings.useProxy.isSelected()) {
        final String res = setProxyVariables();
        if (res.equals("")) {
          final InetSocketAddress proxyInet = new InetSocketAddress(address, port);
          proxy = new Proxy(type, proxyInet);
          httpCon = (HttpsURLConnection) url.openConnection(proxy);
        }
      } else {
        httpCon = (HttpsURLConnection) url.openConnection();
      }
      httpCon.setHostnameVerifier(new HostnameVerifier() {
        @Override
        public boolean verify(final String arg0, final SSLSession arg1) {
          return true;
        }
      });
      try {
        httpCon.connect();
      } catch (final Exception e) {

        if (ProxySettings.useProxy.isSelected()) {
          MainPane.showError("Could not connect to internet to download updates.\n"
              + "Proxy settings are not correct. Go to Tools -> Settings -> Proxy.\n"
              + "You can download updates manually from https://baseplayer.fi/update/\n"
              + "Download BasePlayer.jar to your BasePlayer directory and overwrite the old one.",
              "Error");

        } else {
          MainPane.showError("Could not connect to internet to download updates.\n"
              + "If you are behind proxy server, Go to Tools -> Settings -> Proxy.\n"
              + "You can download updates manually from https://baseplayer.fi/update/\n"
              + "Download BasePlayer.jar to your BasePlayer directory and overwrite the old one.",
              "Error");

        }
        return;
      }
      final int responseCode = httpCon.getResponseCode();
      final int BUFFER_SIZE = 4096;
      System.out.println("Ready.");
      // always check HTTP response code first
      if (responseCode == HttpsURLConnection.HTTP_OK) {
        String fileName = "";
        final String disposition = httpCon.getHeaderField("Content-Disposition");
        // String contentType = httpConn.getContentType();
        final int contentLength = httpCon.getContentLength();

        if (disposition != null) {
          // extracts file name from header field
          final int index = disposition.indexOf("filename=");
          if (index > 0) {
            fileName = disposition.substring(index + 10, disposition.length() - 1);
          }
        } else {
          // extracts file name from URL
          fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());
        }

        // opens input stream from the HTTP connection
        final InputStream inputStream = httpCon.getInputStream();
        final String saveFilePath = saveDir + File.separator + fileName + "_temp";
        final File testFile = new File(saveDir + File.separator + "_test");
        // opens an output stream to save into file
        if (!testFile.mkdir()) {
          MainPane.showError(
              "Could not update BasePlayer. No writing permissions in BasePlayer folder.\nStart BasePlayer as adminstrator and press update again.\n"
                  + "Alternatively, give writing permissions to your BasePlayer folder.",
              "Error");
          testFile.delete();
          return;
        }
        testFile.delete();
        final FileOutputStream outputStream = new FileOutputStream(saveFilePath);

        int bytesRead = -1;
        final byte[] buffer = new byte[BUFFER_SIZE];
        while ((bytesRead = inputStream.read(buffer)) != -1) {
          outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.close();
        inputStream.close();
        final File tempfile = new File(saveFilePath),
            newfile = new File(saveDir + File.separator + fileName);

        if (tempfile.length() == contentLength) {

          final InputStream in = new FileInputStream(tempfile);

          // For Append the file.
          // OutputStream out = new FileOutputStream(f2,true);

          // For Overwrite the file.
          final OutputStream out = new FileOutputStream(newfile);

          final byte[] buf = new byte[1024];
          int len;
          while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
          }
          in.close();
          out.close();

          newfile.setLastModified(httpCon.getLastModified());
          tempfile.delete();
          if (!fileName.contains("Launcher")) {
            MainPane.showError("BasePlayer updated. Please restart program to apply changes.", "Note");
          }
          MenuBar.update.setEnabled(false);
        } else {
          MainPane.showError("BasePlayer could not be updated.", "Note");
        }

        System.out.println("File downloaded");
      } else {
        System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        ErrorLog.addError("No file to download. Server replied HTTP code: " + responseCode);
      }
      httpCon.disconnect();
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }

  protected String doInBackground() {

    try {
      Loader.setLoading("Updating BasePlayer... (downloading BasePlayer.jar from https://baseplayer.fi/update/");

      downloadFile("https://baseplayer.fi/storage/update/dev/BasePlayer.jar", MainPane.userDir);
      if (MainPane.updatelauncher) {
        downloadFile("https://baseplayer.fi/update/Launcher.jar", MainPane.userDir);
      }
      /*
       * if(!new File(userDir+"/genomes/ensembl.txt").exists()) { downloadFile(
       * "https://www.cs.helsinki.fi/u/rkataine/BasePlayer/update/ensembl.txt",
       * userDir+"/genomes/"); }
       */
      Loader.ready("Updating BasePlayer... (downloading BasePlayer.jar from https://baseplayer.fi/update/");
      /*
       * if(homefile.length() > 0) { File replacefile = new File(userDir
       * +"/BasePlayer.jar"); FileUtils.copyFile(homefile, replacefile);
       * FileUtils.forceDelete(homefile); } else {
       * 
       * JOptionPane.showMessageDialog(
       * chromDraw,"BasePlayer couldn't be updated. Please try again.", "Note",
       * JOptionPane.INFORMATION_MESSAGE); return ""; } /* file = new
       * URL("https://www.cs.helsinki.fi/u/rkataine/Rikurator/update/Launcher.jar");
       * homefile = new File(userDir +"/Launcher.jar");
       * 
       * FileUtils.copyURLToFile(file, homefile);
       */

    } catch (final Exception e) {
      Loader.ready("Updating BasePlayer... (downloading BasePlayer.jar from https://baseplayer.fi/update/");
      e.printStackTrace();
      ErrorLog.addError(e.getStackTrace());
      MainPane.showError(e.getMessage(), "Error");

    }
    return "";
  }
  public static String setProxyVariables() {

		if (ProxySettings.proxytypes.getSelectedItem().toString().contains("HTTP")) {
			type = Proxy.Type.HTTP;
		} else if (ProxySettings.proxytypes.getSelectedItem().toString().contains("SOCKS")) {
			type = Proxy.Type.SOCKS;
		} else {
			type = Proxy.Type.DIRECT;
		}

		if (ProxySettings.hostField.getText().trim().equals("Proxy host")
				|| ProxySettings.hostField.getText().trim().equals("")) {
			return "Set proxy host in settings";
		} else {
			address = ProxySettings.hostField.getText().trim();
		}

		if (ProxySettings.portField.getText().trim().equals("Port")
				|| ProxySettings.portField.getText().trim().equals("")) {
			return "Set proxy port in settings";
		} else {
			final String porttest = ProxySettings.portField.getText().trim();
			try {
				port = Integer.parseInt(porttest);
			} catch (final Exception e) {
				return "Set proxy port in settings (number)";
			}
		}
		return "";
	}
  public class CheckUpdates extends SwingWorker<String, Object> {
		private class DefaultTrustManager implements X509TrustManager {

			@Override
			public void checkClientTrusted(final X509Certificate[] arg0, final String arg1)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(final X509Certificate[] arg0, final String arg1)
					throws CertificateException {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		}

		protected String doInBackground() {
			try {

				final SSLContext ctx = SSLContext.getInstance("TLS");
				ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
				SSLContext.setDefault(ctx);
				URL file = new URL("https://baseplayer.fi/storage/update/dev/BasePlayer.jar");
				HttpsURLConnection httpCon = null;
				Proxy proxy = null;

				if (ProxySettings.useProxy.isSelected()) {

					final String res = setProxyVariables();

					if (res.equals("")) {
						final InetSocketAddress proxyInet = new InetSocketAddress(address, port);
						proxy = new Proxy(type, proxyInet);
						httpCon = (HttpsURLConnection) file.openConnection(proxy);
					}
				} else {
					httpCon = (HttpsURLConnection) file.openConnection();
				}
				try {
					httpCon.connect();
				} catch (final Exception e) {

					if (ProxySettings.useProxy.isSelected()) {
						MainPane.showError("Could not connect to internet to check updates.\n"
								+ "Proxy settings are not correct. Go to Tools -> Settings -> Proxy.\n"
								+ "You can download updates manually from https://baseplayer.fi/update/\n"
								+ "Download BasePlayer.jar to your BasePlayer directory and overwrite the old one.",
								"Error");
						return "";
					} else {
						MainPane.showError("Could not connect to internet to check updates.\n"
								+ "If you are behind proxy server, Go to Tools -> Settings -> Proxy.\n"
								+ "You can download updates manually from https://baseplayer.fi/update/\n"
								+ "Download BasePlayer.jar to your BasePlayer directory and overwrite the old one.",
								"Error");
						return "";
					}
				}
				httpCon.setHostnameVerifier(new HostnameVerifier() {
					@Override
					public boolean verify(final String arg0, final SSLSession arg1) {
						return true;
					}
				});
				File homefile = new File(MainPane.userDir + "/BasePlayer.jar");

				if (httpCon.getLastModified() != homefile.lastModified()) {
					// putMessage("Updates available. Please click File->Update to get the most
					// recent version.");
					MenuBar.update.setVisible(true);
					MenuBar.update.setEnabled(true);
					MenuBar.update.setForeground(Color.green);

				} else {
					// putMessage("BasePlayer is up-to-date.");
					MenuBar.update.setEnabled(false);
					MenuBar.update.setVisible(false);
				}
				httpCon.disconnect();
				file = new URL("https://baseplayer.fi/update/Launcher.jar");
				homefile = new File(MainPane.userDir + "/Launcher.jar");
				if (ProxySettings.useProxy.isSelected() && proxy != null) {
					httpCon = (HttpsURLConnection) file.openConnection(proxy);
				} else {
					httpCon = (HttpsURLConnection) file.openConnection();
				}

				httpCon.setHostnameVerifier(new HostnameVerifier() {
					@Override
					public boolean verify(final String arg0, final SSLSession arg1) {
						return true;
					}
				});
				httpCon.connect();
				if (!homefile.exists() || httpCon.getLastModified() != homefile.lastModified()) {
					MainPane.updatelauncher = true;
				} else {
					MainPane.updatelauncher = false;
				}

				httpCon.disconnect();

			} catch (final Exception e) {
				MainPane.showError(e.getMessage(), "Error");

				MenuBar.update.setEnabled(false);
			}
			try {
				BufferedReader selexReader = new BufferedReader(
						new InputStreamReader(Main.class.getResourceAsStream("SELEX/TFbinding_PFMs.txt")));
				String line, factor;
				String[] split, matrix, values;
				int[][] selexmatrix;
				while ((line = selexReader.readLine()) != null) {
					split = line.split("\\t");
					factor = split[0].replace(".pfm", "");
					matrix = split[1].split(";");
					values = matrix[0].split(",");
					selexmatrix = new int[4][values.length];
					for (int i = 0; i < 4; i++) {
						values = matrix[i].split(",");
						for (int j = 0; j < values.length; j++) {
							selexmatrix[i][j] = Integer.parseInt(values[j]);

						}
					}
					MainPane.factorNames.put(factor, factor);
					MainPane.SELEXhash.put(factor, selexmatrix);
				}
				selexReader.close();

				String id;
				boolean first = true;
				int linepointer = 0;
				if (new File(MainPane.userDir + "/additions/motifs/").exists()) {
					final File[] files = new File(MainPane.userDir + "/additions/motifs/").listFiles();
					for (final File file : files) {
						selexReader = new BufferedReader(new FileReader(file));

						while ((line = selexReader.readLine()) != null) {
							try {
								if (line.startsWith(" ")) {
									continue;
								}
								if (line.startsWith(">")) {
									first = true;
									split = line.split("\\s+");
									id = split[0].substring(1);
									factor = split[1];

									MainPane.factorNames.put(id, factor);

									linepointer = 0;
									line = selexReader.readLine();
									selexmatrix = null;

									while (line.length() > 1 && !line.startsWith(" ")) {

										split = line.substring(line.indexOf("[") + 1, line.indexOf("]")).trim()
												.split("\\s+");

										if (first) {
											first = false;
											selexmatrix = new int[4][split.length];
										}

										for (int j = 0; j < split.length; j++) {
											selexmatrix[linepointer][j] = Integer.parseInt(split[j]);
										}
										linepointer++;
										line = selexReader.readLine();
									}
									MainPane.SELEXhash.put(id, selexmatrix);
								}
							} catch (final Exception e) {

								e.printStackTrace();
							}
						}
						selexReader.close();
					}
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
			return "";
		}
	}
  public static String checkFile(final URL url, final ArrayList<String> others) throws IOException {

		URLConnection httpCon = null;
		final String fileURL = url.getPath();

		Proxy proxy = null;
		if (ProxySettings.useProxy.isSelected()) {
			final String res = setProxyVariables();
			if (res.equals("")) {
				final InetSocketAddress proxyInet = new InetSocketAddress(address, port);
				proxy = new Proxy(type, proxyInet);
				httpCon = (URLConnection) url.openConnection(proxy);
			}
		} else {
			httpCon = (URLConnection) url.openConnection();
		}
		try {
			httpCon.connect();
		} catch (final Exception e) {
			if (ProxySettings.useProxy.isSelected()) {
				MainPane.showError("Could not connect to internet to download genomes.\n"
						+ "Proxy settings are not correct. Go to Tools -> Settings -> Proxy.", "Error");
			} else {
				MainPane.showError("Could not connect to internet to download genomes.\n"
						+ "If you are behind proxy server, Go to Tools -> Settings -> Proxy.", "Error");
			}
			return null;
		}
		String fileName = "";

		final String disposition = httpCon.getHeaderField("Content-Disposition");

		if (disposition != null) {
			// extracts file name from header field
			final int index = disposition.indexOf("filename=");
			if (index > 0) {
				fileName = disposition.substring(index + 10, disposition.length() - 1);
			}
		} else {
			// extracts file name from URL
			fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1);
		}
		InputStream inputStream = null;

		// opens input stream from the HTTP connection
		try {
			inputStream = httpCon.getInputStream();

		} catch (final Exception e) {

			if (fileName.endsWith(".gff3.gz")) {
				final String urldir = fileURL.substring(0, fileURL.lastIndexOf("/") + 1);
				fileName = getNewFile(url.getHost(), urldir, fileName);

				if (!others.contains(fileName.replace(".gff3.gz", ""))) {

					return fileName;
				} else {
					return "";
				}

			}
		}

		if (inputStream == null) {
			return fileName;
		} else {
			inputStream.close();
			if (!others.contains(fileName.replace(".gff3.gz", ""))) {
				return fileName;
			}
			return "";
		}
	}
  public static String downloadFile(URL url, String saveDir, final int size) throws IOException {
		URLConnection httpCon = null;
		Proxy proxy = null;
		if (ProxySettings.useProxy.isSelected()) {
			final String res = setProxyVariables();
			if (res.equals("")) {
				final InetSocketAddress proxyInet = new InetSocketAddress(address, port);
				proxy = new Proxy(type, proxyInet);
				httpCon = (URLConnection) url.openConnection(proxy);
			}
		} else {
			httpCon = (URLConnection) url.openConnection();
		}

		try {
			httpCon.connect();
		} catch (final Exception e) {
			if (ProxySettings.useProxy.isSelected()) {
				MainPane.showError("Could not connect to internet to download genomes.\n"
						+ "Proxy settings are not correct. Go to Tools -> Settings -> Proxy.\n"
						+ "You can download genome files manually from Ensembl by selecting the genome and pressing \"Get file links.\" button below.\n"
						+ "Download files with web browser and add them manually. See online manual for more instructions.",
						"Error");
			} else {
				MainPane.showError("Could not connect to internet to download genomes.\n"
						+ "If you are behind proxy server, Go to Tools -> Settings -> Proxy.\n"
						+ "You can download genome files manually from Ensembl by selecting the genome and pressing \"Get file links.\" button below.\n"
						+ "Download files with web browser and add them manually. See online manual for more instructions.",
						"Error");
			}
			return null;
		}

		final String fileURL = url.getPath();

		final int BUFFER_SIZE = 4096;
		// always check HTTP response code first
		String fileName = "";
		final String disposition = httpCon.getHeaderField("Content-Disposition");

		if (disposition != null) {
			// extracts file name from header field
			final int index = disposition.indexOf("filename=");
			if (index > 0) {
				fileName = disposition.substring(index + 10, disposition.length() - 1);
			}
		} else {
			// extracts file name from URL
			fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1);
		}
		InputStream inputStream = null;

		// opens input stream from the HTTP connection
		try {
			inputStream = httpCon.getInputStream();
		} catch (final Exception e) {
			if (fileName.endsWith(".gff3.gz")) {
				final String urldir = fileURL.substring(0, fileURL.lastIndexOf("/") + 1);
				fileName = getNewFile(url.getHost(), urldir, fileName);
				url = new URL(url.getProtocol() + "://" + url.getHost() + "/" + urldir + "/" + fileName);
				httpCon = (URLConnection) url.openConnection();
				inputStream = httpCon.getInputStream();

			}
		}

		if (inputStream == null) {
			return "";
		}
		if (fileName.endsWith(".gff3.gz")) {
			saveDir = saveDir += "/" + fileName + "/";
			new File(saveDir).mkdirs();
		}
		final String saveFilePath = saveDir + File.separator + fileName;

		// opens an output stream to save into file
		final FileOutputStream outputStream = new FileOutputStream(saveFilePath);
		long downloaded = 0;
		int bytesRead = -1, counter = 0;
		String loading = "";
		final byte[] buffer = new byte[BUFFER_SIZE];
		if (MainPane.drawCanvas != null) {
			loading = Loader.loadingtext;
			Loader.loadingtext = loading + " 0MB";
		}
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
			downloaded += buffer.length;
			if (MainPane.drawCanvas != null) {
				counter++;
				if (counter == 100) {

					Loader.loadingtext = loading + " " + (downloaded / 1048576) + "/~" + (size / 1048576)
							+ "MB";
					MainPane.drawCanvas.loadBarSample = (int) (downloaded / (double) size * 100);
					MainPane.drawCanvas.loadbarAll = (int) (downloaded / (double) size * 100);
					if (MainPane.drawCanvas.loadBarSample > 100) {
						MainPane.drawCanvas.loadBarSample = 100;
						MainPane.drawCanvas.loadbarAll = 100;
					}
					counter = 0;
				}
			}
		}
		if (MainPane.drawCanvas != null) {
			MainPane.drawCanvas.loadBarSample = 0;
			MainPane.drawCanvas.loadbarAll = 0;
		}
		outputStream.close();
		inputStream.close();
		return fileName;
	}

	static String getNewFile(final String server, final String folder, final String oldfile) {
		String minfile = "";
		try {

			final String filename = oldfile;
			final FTPClient f = new FTPClient();
			f.connect(server);
			f.enterLocalPassiveMode();
			f.login("anonymous", "");
			final FTPFile[] files = f.listFiles(folder);
			int left = 0, right = filename.length() - 1, distance = 0;
			int mindistance = 100;

			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().endsWith(".gff3.gz")) {
					distance = 0;
					right = Math.min(filename.length(), files[i].getName().length());
					left = 0;

					while (left < right) {
						if (filename.charAt(left) != files[i].getName().charAt(left)) {
							distance++;
						}
						left++;
					}
					distance += Math.abs(filename.length() - files[i].getName().length());
					if (distance < mindistance) {
						mindistance = distance;
						minfile = files[i].getName();
					}
				}
			}
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
		return minfile;
	}
}