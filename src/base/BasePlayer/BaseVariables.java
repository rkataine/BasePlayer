package base.BasePlayer;
import java.awt.Font;

public class BaseVariables {
    public static Integer defaultFontSize = 11;
    public static Integer readHeight = 0;
    public static Font readfont = new Font("SansSerif", Font.PLAIN, BaseVariables.defaultFontSize);
    public static Font seqFont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
    public static Font loadingFont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize*2);
    public static Font defaultFont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize);
    public static Font biggerFont = new Font("SansSerif", Font.BOLD, BaseVariables.defaultFontSize+2);
    public static String variantCountLabel = "Variant count on screen: ";
}