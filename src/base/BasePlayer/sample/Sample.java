/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.sample;

import htsjdk.samtools.util.BlockCompressedInputStream;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JOptionPane;

import base.BasePlayer.Getter;
import base.BasePlayer.Setter;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.reads.ReadNode;
import base.BasePlayer.reads.Reads;
import base.BasePlayer.variants.VarNode;

public class Sample {

	private transient HashMap<SplitClass, Reads> readHash;
	private transient Float maxCoverage = 0F;
	public static int selectedSampleIndex = -1;
	public static Sample selectedSample = null; 
	public static Sample hoverSample = null;
	public boolean katri = false, reading = false, multiVCF = false, multipart = false, SV = false, MD = false, CRAM = false, calledvariants = false, chrSet = false, annotation = false, annoTrack = false, intersect = false;
	public Boolean affected = false, female;
	private int index;
	private String sampleName, tabixfile;
	private transient BlockCompressedInputStream inputStream;
	private transient BufferedReader vcfreader;
	public Sample mother, father, multiVCFparent;
	public int parents = 0;
	public ArrayList<Sample> children;
	//public ArrayList<String[]> translocations;
	public Color familyColor;	
	public int infolocation = 0, varcount = 0, homozygotes = 0, heterozygotes = 0, indels = 0, snvs=0, ins=0, coding = 0, syn= 0,nonsyn=0,missense=0, splice=0,nonsense=0,fshift=0,inframe=0, basequalsum = 0, basequals = 0,
	prepixel = 0, maxCoveragecaller = 0, phred = 33, sitioRate = 0, versioRate = 0;
	public long coverages = 0, vcfEndPos = 0;
	public File samFile;
	public Integer longestRead = 0,somaticColumn = null, samePosCount = 1;
	public String chr = "", vcfchr = "", readString = "No BAM/CRAM";
	private transient HashMap<String, ArrayList<ReadNode>> mates;
	public double callrates = 0.0;
	public double[] mutationTypes;
	public ArrayList<String[]> breakpoints;

	private transient Boolean complete;
	
	public Sample(String sampleName, int index, String tabixfile) {		
		if(tabixfile != null) {
			this.tabixfile = tabixfile;		
			try {
				setInputStream();
				//inputStream = new BlockCompressedInputStream(new File(this.tabixfile));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		this.sampleName = sampleName;
		this.index = index;
	
	}
	public void setTabixFile(String file) {
		this.tabixfile = file;
	}
	public void resetreadHash() {
		if(samFile != null) {
			if(samFile.getName().toLowerCase().endsWith(".cram")) {
				this.CRAM = true;
			}
		}
		if(readHash == null) {
			readHash = new HashMap<SplitClass, Reads>();
		}
		for(int i = 0; i<Draw.getSplits().size(); i++) {
			Reads reads = new Reads();
			reads.sample = this;
			readHash.put(Draw.getSplits().get(i), reads);
		}
		

	}
/*	ArrayList<Reads> getreadHash() {
		return this.readHash;
	}*/
	
	public float getMaxCoverage() {
		if(maxCoverage == null) return 0F;		
		return this.maxCoverage;
	}
	public void setMaxCoverage(float value) {
		this.maxCoverage = value;
	}
	public Boolean getComplete() {
		
		return this.complete;
	}
	public void setcomplete(boolean value) {
		this.complete = value;
	}
	public BlockCompressedInputStream getVCFInput() {
		return this.inputStream;
	}
	public BufferedReader getIDXInput() {
		return this.vcfreader;
	}
	public void setInputStream() {
		try {
		if(this.tabixfile.endsWith(".gz")) {
			this.inputStream = new BlockCompressedInputStream(new File(this.tabixfile));
		}
		else {
			this.vcfreader= new BufferedReader(new FileReader(this.tabixfile));
		}
		}
		catch(Exception e) {
			
			this.inputStream =null;
			e.printStackTrace();
			JOptionPane.showMessageDialog(MainPane.chromDraw, e.getMessage() , "Error", JOptionPane.ERROR_MESSAGE);
			
		}
	}
	public BufferedReader getVCFReader() {
		return this.vcfreader;
	}
	
	public HashMap<SplitClass, Reads> getreadHash() {
		return this.readHash;
	}
	public HashMap<String, ArrayList<ReadNode>> getMates() {
		return this.mates;
	}
	
	public void setMates() {
		this.mates = new HashMap<String, ArrayList<ReadNode>>();
	}
	public int getMultiIndex() {
		if(this.multipart) {
			return (short)(this.index-1);
		}
		else {
			return this.index;
		}
	}
	public int getIndex() {
		return this.index;
	}
	public void setIndex(int index) {
		this.index = index;
	}	
	public String getName() {
		return this.sampleName;
	}
	public void setName(String name) {
		this.sampleName = name;
	}
	public String getTabixFile() {
		return this.tabixfile;
	}
	public Map<String, String> toJSON() {
		Map<String, String> json = new HashMap<String, String>();
		json.put("name", this.sampleName);
		
		if (this.tabixfile != null) {
			json.put("vcf", this.tabixfile.replace("\\", "/"));
		}
		
		if (this.samFile != null) {
			json.put("sam", this.samFile.getAbsolutePath().replace("\\", "/"));
		}

		return json;
	}
	public void removeSample() {
		if (this.multiVCF) {

			int remindex = this.getIndex() + 1;
			Sample remsample;
			for (int i = remindex; i < MainPane.samples; i++) {
				remsample = Getter.getInstance().getSampleList().get(i);
				if (!remsample.multipart) break;				
				VarNode node = Getter.getInstance().getVariantHead().getNext();

				while (node != null) {
					node.removeSample(remsample);
					node = node.getNext();
				}
				if (remsample.samFile != null) {
					MainPane.readsamples--;
					remsample.samFile = null;
				}

				if (!this.annotation) MainPane.varsamples--;				
				Getter.getInstance().getSampleList().remove(remsample);
				MainPane.samples--;
				node = null;
				i--;
			}
			remsample = null;
			MainPane.samples--;
			Getter.getInstance().getSampleList().remove(this);

			/* if (MainPane.samples > 0) {
				if (this.getIndex() < MainPane.samples) {
					this.selectedSample = Getter.getInstance().getSampleList().get(removeSample.getIndex());
					this.Sample.selectedSampleIndex = removeSample.getIndex();
				} else {
					this.selectedSample = Getter.getInstance().getSampleList().get(removeSample.getIndex() - 1);
					this.Sample.selectedSampleIndex = removeSample.getIndex() - 1;
				}
			}
			removeSample = null;
			this.removeSample = null; */
		} else {
			VarNode node = Getter.getInstance().getVariantHead().getNext();

			while (node != null) {
				node.removeSample(this);
				node = node.getNext();
			}
			MainPane.drawCanvas.selectedIndex = 0;
			node = null;
			if (this.calledvariants) {
				if (!this.annotation) {
					MainPane.varsamples--;
				}
			}
			if (this.samFile != null) {

				MainPane.readsamples--;

				this.samFile = null;
			}
			if (this.getTabixFile() != null) {
				if (!this.annotation) {
					MainPane.varsamples--;
				}
			}
			Getter.getInstance().getSampleList().remove(this);
			if (this.annotation) {
				boolean found = false;
				for (int i = 0; i < Getter.getInstance().getSampleList().size(); i++) {
					if (Getter.getInstance().getSampleList().get(i).annotation) {
						found = true;
						break;
					}
				}
				MainPane.drawCanvas.annotationOn = found;
			}

			MainPane.samples--;
			/* if (MainPane.samples > 0) {
				if (this.getIndex() < MainPane.samples) {
					this.selectedSample = Getter.getInstance().getSampleList().get(removeSample.getIndex());
					this.Sample.selectedSampleIndex = removeSample.getIndex();
				} else {
					this.selectedSample = Getter.getInstance().getSampleList().get(removeSample.getIndex() - 1);
					this.Sample.selectedSampleIndex = removeSample.getIndex() - 1;
				}
			} */
		}
		VariantHandler.commonSlider.setMaximum(Math.max(MainPane.varsamples, 1));
		VariantHandler.geneSlider.setMaximum(Math.max(MainPane.varsamples, 1));
		if (MainPane.projectValues.variantHandlerValues.commonVariantsMax> MainPane.varsamples) {
			VariantHandler.commonSlider.setUpperValue(MainPane.varsamples);

		}
		if (MainPane.projectValues.variantHandlerValues.commonVariantsMin > MainPane.varsamples) {
			VariantHandler.commonSlider.setValue(Math.max(MainPane.varsamples, 1));
		}
		if (MainPane.projectValues.variantHandlerValues.compareGene > MainPane.varsamples) {
			VariantHandler.geneSlider.setValue(Math.max(MainPane.varsamples, 1));
		}
		checkSampleIndices();
		MainPane.chromDraw.vardraw = null;
		
		MainPane.drawCanvas.resizeCanvas(MainPane.drawCanvas.getWidth(), MainPane.drawScroll.getViewport().getHeight());
		
		Setter.getInstance().setUpdateScreen();
	}
	void checkSampleIndices() {
		AtomicInteger index = new AtomicInteger(0);
		Getter.getInstance().getSampleList().forEach(sample -> {
			sample.setIndex(index.getAndIncrement());
		});
	}
}
