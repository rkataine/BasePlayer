/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.sample;
import java.util.Arrays;
import java.util.HashMap;

import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.control.ControlFile;
import base.BasePlayer.io.FileRead;

public class SampleNode {
//	private String variation;

//	private static final long serialVersionUID = 1L;
	private final Integer coverage, calls;
	private final Float quality, gq;
	public float heightValue = 0;
	private final Sample sample;
	private final ControlFile controlSample;
	private final boolean genotype;
	private final boolean noref;
	public Integer alleles, allelenumber;
	public boolean common = false;
	public boolean inheritance = false;
	public Integer length = null;
	public String filterField = null;
	//HashMap<String, Float> advQualities;
	
	public SampleNode(int alleles, int allelenumber, ControlFile sample) {
		this.alleles = alleles;
		this.allelenumber = allelenumber;
		this.sample = null;
		this.controlSample = sample;
		this.quality = null;
		this.gq = null;
		this.coverage = null;
		this.calls = null;
		this.genotype = false;
		this.noref = false;
	}
	public SampleNode(int[] coverages, String[] genotype, int genotypeIndex, Float quality, Float gq, HashMap<String, Float> advquals, String filterField, Sample sample) {
		this.sample = sample;
		if (genotype == null) {	this.coverage = 100; this.calls = 50; this.quality = quality;this.controlSample = null;	this.genotype = false;this.noref = false;this.gq = null; } 
		else {
			this.genotype = genotype[0].equals(genotype[1]);
			if (!genotype[0].equals("0") && !this.genotype) this.noref = true;
			else noref = false;				
			
			genotypeIndex += 1;
			//this.advQualities = advquals;
			this.controlSample = null;
			/*if(coverage > Short.MAX_VALUE) {
				this.calls = (Float)(calls/(double)coverage * Short.MAX_VALUE);
				this.coverage = Short.MAX_VALUE;
				
			}
			else {*/
			this.filterField = filterField;
			this.coverage = Arrays.stream(coverages).sum();
			this.calls = coverages[genotypeIndex];
			
			this.gq = gq;
			this.quality = quality;
			
		}
		switch(Settings.selectedVarDraw) {	
		
			case 0: {	
				
				if(sample.getMaxCoverage() < coverage) {					
					sample.setMaxCoverage((float)(coverage));					
				}	
				this.heightValue = coverage;
				break;
			}
			case 1: {	
				if(sample.getMaxCoverage() < calls/(float)coverage) {
					
					sample.setMaxCoverage(calls/(float)coverage);
					
				}	
				this.heightValue = calls/(float)coverage;
				break;
			}
			case 2: {	
				if(quality != null) {
					if(sample.getMaxCoverage() < quality) {
						
						sample.setMaxCoverage(quality);
						if(VariantHandler.maxSlideValue < quality) {
							VariantHandler.maxSlideValue = quality;
						}
					}	
					this.heightValue = quality;
				}
				break;
			}
			case 3: {	
				if(gq != null) {
					if(sample.getMaxCoverage() < gq) {
						
						sample.setMaxCoverage(gq);
						if(VariantHandler.maxSlideValue < gq) {
							VariantHandler.maxSlideValue = gq;
						}
					}	
					this.heightValue = gq;
				}
				
				break;
			}
			case 4: {	
				if(sample.getMaxCoverage() < calls) {
					
					sample.setMaxCoverage((float)(calls));
					
				}	
				this.heightValue = calls;
				break;
			}
		}
	}
	/*public String getVariation() {
		return " ";
	//	return this.variation;
	}*/
	public boolean hasNoref() { return this.noref; }
	public void addAlleles(int add) { this.alleles += add; }
	public int getCoverage() { return this.coverage; }
	public int getCalls() { return this.calls; }
	public Float getQuality() { return this.quality; }
	public Float getGQ() { return this.gq; }
	public String getGQString() {
		if(this.gq == null) return ".";
		else return ""+this.gq;		
	}
	public Sample getSample() {	return this.sample; }
	public ControlFile getControlSample() {	return this.controlSample; }
	public Double getAlleleFraction() {	return this.calls/(double)(this.coverage); }
	public boolean isHomozygous() {	return this.genotype; }

	public String toString() {
		return getSample() +"\n"
				+"coverage: " +getCoverage() +"\n"
				+"calls: " +getCalls() +"\n"
				+"quality: " +getQuality() +"\n"
				+"GQ: " +getGQ() +"\n";
	}
	public boolean hideVar(final boolean indel) {
		if (this.alleles != null) return true;	

		if (MainPane.drawCanvas.annotationOn) {
			if (this.getSample().annotation) return false;
			
			if (!FileRead.readFiles && (MainPane.projectValues.variantHandlerValues.commonVariantsMin >= 1	|| MainPane.projectValues.variantHandlerValues.commonVariantsMax < MainPane.varsamples)) {
				if (!this.common) return true;				
			}
		}

		if (!this.getSample().annoTrack) {
		
			if (this.getQuality() != null && MainPane.projectValues.variantHandlerValues.variantQuality > this.getQuality()) {
				return true;
			}
			if (this.getGQ() != null && MainPane.projectValues.variantHandlerValues.variantGQ > this.getGQ()) {
				return true;
			}
			if (MainPane.projectValues.variantHandlerValues.variantCoverage > this.getCoverage()) {
				return true;
			}
			if (MainPane.projectValues.variantHandlerValues.variantMaxCoverage < this.getCoverage()) {
				return true;
			}
			if (MainPane.projectValues.variantHandlerValues.variantCalls / 100.0 > this.getAlleleFraction()	|| this.getAlleleFraction() == 0.0) {
				return true;
			}
			if (MainPane.projectValues.variantHandlerValues.variantMaxCalls != 100) {
				if (MainPane.projectValues.variantHandlerValues.variantMaxCalls / 100.0 < this.getAlleleFraction()) return true;				
			}
			if (VariantHandler.readSlider.getValue() > this.getCalls()) return true;
			if (VariantHandler.hideHomos.isSelected()) if (this.isHomozygous()) return true;
		}
		return false;
	}
	public boolean hideVarCommon(final boolean indel) {
		
		if (this.alleles != null) return true;
		
		if (this.getQuality() != null && MainPane.projectValues.variantHandlerValues.variantQuality > this.getQuality()) {
			return true;
		}
		if (this.getGQ() != null && MainPane.projectValues.variantHandlerValues.variantGQ > this.getGQ()) {
			return true;
		}
		
		if (MainPane.projectValues.variantHandlerValues.variantCoverage > this.getCoverage()) {
			return true;
		}
	
		if (MainPane.projectValues.variantHandlerValues.variantMaxCoverage < this.getCoverage()) {
			return true;
		}
	
		if (MainPane.projectValues.variantHandlerValues.variantCalls / 100.0 > this.getAlleleFraction()) {
			return true;
		}
		
		if (MainPane.projectValues.variantHandlerValues.variantMaxCalls != 100) {
			if (MainPane.projectValues.variantHandlerValues.variantMaxCalls / 100.0 < this.getAlleleFraction()) {
				return true;
			}
		}
		
	
		if (VariantHandler.hideHomos.isSelected()) {
			if (this.isHomozygous()) {
				return true;
			}
		}
		return false;
	}

}
