package base.BasePlayer;

import java.util.List;
import java.util.Map;

import base.BasePlayer.control.ControlFile;

public class ProjectValues {
  public String projectFile;

	//genome and annotation files
  public List<Map<String, String>> samples;
	public List<ControlFile.ControlJSON> controls;
	public List<Map<String, String>> tracks;
  public VariantHandlerValues variantHandlerValues = new VariantHandlerValues();

    public class VariantHandlerValues {
		//variant tab
    public int variantQuality = 0, variantGQ = 0, variantReads = 1, variantCalls = 1, variantMaxCalls = 100, variantMaxCoverage = 1500, variantCoverage = 1;
    
		//compare tab
		public int compareGene = 1, commonVariantsMin = 1, commonVariantsMax = 1, clusterSize = 0;
		
		//hide tab
		public boolean hideNoncoding = false, hideRscoded = false, hideSNVs = false, hideIndels = false, hideHomozygotes = false, freeze = false;
		
		//annotate settings
		public boolean nonSynonymous = false, truncs = false, windowcalc = false, intronic = false, intergenic = false, utr = false, 
		allChroms = false, onlySel = false, onlyStats = false, contexts = false;
		
		//output settings
		public boolean writeFile = false, tsv = false, compact = false, 
		vcf = false, genetsv = false, oncodrive = false;
		
		//TODO: inheritance & advanced settings
    }
    
}