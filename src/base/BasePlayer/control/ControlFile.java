/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.control;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JPopupMenu;

import base.BasePlayer.Getter;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.variants.BreakPoint;

public class ControlFile implements Serializable, ActionListener {
	
	private static final long serialVersionUID = 1L;
	public boolean controlled = false;
	public String tabixfile;
	String sampleName, chr = "";
	public short index;
	public double alleleFreq = 0.01;
	public boolean SV = false, isControlFile = true;
	public Rectangle alleleBox = new Rectangle();
	public Rectangle playbox = new Rectangle();
	public JCheckBox remOverlaps;
	private transient JPopupMenu menu;
	public Polygon playTriangle = new Polygon();
	
	public boolean controlOn = false;
	public StringBuffer alleletext = new StringBuffer("0.01");
	public ArrayList<BreakPoint> breakpoints;
	
	public ControlFile(String sampleName, short index, String tabixfile) {
		this.tabixfile = tabixfile;
		this.sampleName = sampleName;
		this.index = index;		
		setMenu();
	}

	public int getFirstBreak(String chr) {
		for (int i = 0; i < this.breakpoints.size(); i++) if (this.breakpoints.get(i).getChr().equals(chr)) return i;
		return -1;
	}
	public String getTabixFile() { return this.tabixfile; }	
	public short getIndex() { return this.index; }
	public void setIndex(short index) { this.index = index; }	
	public String getName() { return this.sampleName; }
	void addMenu(ArrayList<String> populations) {
		for(String population : populations) menu.add(new JCheckBox(population, true));		
	}
	ArrayList<String> readMenu() {
		ArrayList<String> unselected = new ArrayList<String>();
		for (int i = 1; i < menu.getComponentCount(); i++) {
			JCheckBox check = (JCheckBox)menu.getComponent(i);
			if (!check.isSelected()) unselected.add(check.getText());					
		}
		return unselected;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == remOverlaps) {	Control.dismissControl(Getter.getInstance().getVariantHead(), this); }
	}
	public void setMenu() {
		if(remOverlaps == null) remOverlaps = new JCheckBox("Overlap indels");
		
		menu = new JPopupMenu("Options");
		menu.add(remOverlaps);
		remOverlaps.addActionListener(this);
		for(int c = 0 ; c<getPopupMenu().getComponentCount(); c++) getPopupMenu().getComponent(c).setFont(MainPane.menuFont);		
	}
	public JPopupMenu getPopupMenu() {	return this.menu; }
	
	public class ControlJSON {
		public String name, vcf;
		public double alleleFreq;
	}
	public ControlJSON toJSON() {
		ControlJSON json = new ControlJSON();
		json.name = this.sampleName;
		json.vcf = this.tabixfile.replace("\\", "/");
		json.alleleFreq = this.alleleFreq;
		return json;
	}
}
