/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.control;
import htsjdk.tribble.readers.TabixReader;

import javax.swing.*;

import base.BasePlayer.variants.VarMaster;
import base.BasePlayer.variants.VarNode;
import base.BasePlayer.variants.VcfReader;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.Setter;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.Launcher;
import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.MenuBar;
import base.BasePlayer.GUI.modals.VariantHandler;
import base.BasePlayer.sample.SampleNode;
import base.BasePlayer.variants.BreakPoint;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.zip.GZIPInputStream;
public class Control {
	
  static String path = Launcher.ctrldir;
	static String offsetString ="";
	static boolean hold = false;
	static int row = 1;		
	static int runIndex = -1;
	static boolean isCancel;	
	private static TabixReader tabixreader;	
	//private 
	public static ControlData controlData = new ControlData();

	private static int allelenumber;
	private static String[] infosplit;
	private static String[] coverages;	
	
public Control() {	
	    
}

public static void applyControl() {
	try {
		
		for(int c = 0; c<controlData.fileArray.size(); c++) {
			if(!Getter.getInstance().loading()) break;
			ControlFile controlFile = controlData.fileArray.get(c);
			if(!controlFile.controlOn || controlFile.controlled) continue;
		
			MainPane.drawCanvas.loadbarAll = (int)((c/(double)(controlData.fileArray.size()))*100);
			try {
				tabixreader = new TabixReader(controlFile.getTabixFile());				
			}
			catch(Exception tab) {
				if (tab.toString().contains(".tbi")) {
					MainPane.showError("Could not find index file (.tbi) for " +controlFile.getTabixFile(), "Note");
				} else {
					MainPane.showError("Could not find " +controlFile.getTabixFile(), "Note");
				}	
			}
			Loader.loadingtext = "Applying control " +controlFile.getName();				
		
			TabixReader.Iterator iterator=null; 		
			VarMaster.getInstance().setCurrent(Getter.getInstance().getVariantHead().getNext());
			    
			if (Getter.getInstance().getVariantHead().getNext() == null) return;			

			iterator = tabixreader.query(controlFile.chr +MenuBar.chromosomeDropdown.getSelectedItem().toString() +":" +(VarMaster.getInstance().getCurrent().getPosition()-1));			
			
			if (iterator == null) continue;	
			if (controlFile.SV) {
				controlBreakPoints(iterator, Getter.getInstance().getVariantHead().getNext(), controlFile);
			} else if(!controlFile.remOverlaps.isSelected()) {
				useVCFstrict(iterator, Getter.getInstance().getVariantHead().getNext(), controlFile);				  
			}
			else {
				useVCFoverlap(iterator, Getter.getInstance().getVariantHead().getNext(), controlFile);
			}			
			controlFile.controlled = true;	 
		}
	
		MainPane.drawCanvas.repaint();
		VariantHandler.table.repaint();
	}
	catch(Exception e) {
		ErrorLog.addError(e.getStackTrace());
		e.printStackTrace();
	}	
}

private static void controlBreakPoints(TabixReader.Iterator iterator, VarNode current, ControlFile controlFile) {
	String line;
	int flankLength = 1000;
	try {
		while((line = iterator.next()) != null) {			
			if(!Getter.getInstance().loading()) break;
			
			String[] split = line.split("\t");
			int start = Integer.parseInt(split[1]) - flankLength;
			int end = start + flankLength*2;

			while(current != null && current.getPosition() < start) current = current.getNext();	
			if(current == null) break;
			if(current.getPosition() > end) continue;
			
			current.getPair().removeNode();
			current = current.remove();			
		}
		/* int breakIndex = controlFile.getFirstBreak(chr);
		current = Getter.getInstance().getVariantHead().getNext();
		for (int i = breakIndex; i < controlFile.breakpoints.size(); i++) {
			BreakPoint breakpoint = controlFile.breakpoints.get(i);
			int start = breakpoint.getPos() - flankLength;
			int end = start + flankLength*2;
			while(current != null && current.getPosition() < start) current = current.getNext();	
			if(current == null) break;
			if(current.getPosition() > end) continue;
			current.getPair().removeNode();
			current = current.remove();		

			if (!breakpoint.getChr().equals(chr)) break;
		} */
		current = null;
	} catch(Exception e) { e.printStackTrace(); }
}

static void useVCFoverlap(TabixReader.Iterator iterator, VarNode current, ControlFile controlFile) {
	try {
		String line;
		String[] split, varlist = null;
		int position, count = 0, ref= 0, alt = 0;
		ArrayList<SampleNode> samplelist = null;
		int alleles = 0, baselength = 0;		 
		
		while((line = iterator.next()) != null) {
			if(!Getter.getInstance().loading()) break;
			split = line.split("\t");
			baselength = MethodLibrary.getControlBaseLength(split[3], split[4], 0);	
			position = Integer.parseInt(split[1])-1;

			while(current != null && current.getPosition() < position) current = current.getNext();		 
			
			if(current == null) break;
			
			if(position+baselength < current.getPosition()) continue;
		
			if(current.getPosition() >= position && current.getPosition() <= position+baselength) {
				varlist = VcfReader.parseALT(split[3], split[4], null);
				
				current.controlled = true;
				
				for(Map.Entry<String, ArrayList<SampleNode>> sampleNodes : current.vars.entrySet()) {
					count++;
					for(int t = 0; t<varlist.length; t++) {
						if(sampleNodes.getKey().equals(varlist[t]) || baselength > 1) {										
							if(controlFile.isControlFile) {
								alleles = Integer.parseInt(MethodLibrary.getValueFromInfo("AC", split[7], false).split(",")[t]);											
								allelenumber = Integer.parseInt(MethodLibrary.getValueFromInfo("AN", split[7], true));											
							} else {
								infosplit = split[split.length-1].split(":");		    							 
								coverages = infosplit[split[8].indexOf("AD")/3].split(","); // TODO 
								ref = Integer.parseInt(coverages[0]);
								alt = Integer.parseInt(coverages[1]);
								
								alleles = alt;				 	
								allelenumber = ref+alt;
							}
							if(alleles > 0) {
								samplelist = sampleNodes.getValue();		    				
								samplelist.add(new SampleNode(alleles,allelenumber,controlFile));	
							}										
						}	    					
					}
					continue;
				}	    		 
			}
			
			if(count % 10 == 0) {
				MainPane.drawCanvas.loadBarSample = (int)((current.getPosition()/(double)(Draw.getSplits().get(0).chromEnd))*100);     
				Setter.getInstance().setUpdateScreen();
			}
		}
		   
			samplelist = null;			
		   	current = null;
		}
		catch(Exception e) {			
			e.printStackTrace();
		}
}

static void useVCFstrict(TabixReader.Iterator iterator, VarNode current, ControlFile controlFile) {
	try {
		String line;
		String[] split, varlist = null;
		int position, count = 0;
		ArrayList<SampleNode> samplelist = null;
		int alleles = 0;
		
	   while((line = iterator.next()) != null) {	 
			 
			if(!Getter.getInstance().loading()) break;
			
			
			split = line.split("\t");	    	
			position = Integer.parseInt(split[1])-1;
			
			while(current != null && current.getPosition() < position) current = current.getNext();		    		 
			
			if(current == null) break;
			
			if(position < current.getPosition()) continue;	    	
	    	
	    	 if(current.getPosition() == position) {
	    		 varlist = VcfReader.parseALT(split[3], split[4], null);
					 
	    		 current.controlled = true;	    		 
					 
	    		 for(Map.Entry<String, ArrayList<SampleNode>> sampleNode : current.vars.entrySet()) {
	    			count++;
					for(int t = 0; t<varlist.length; t++) {
						String controlvar = varlist[t];
						
						if(sampleNode.getKey().equals(controlvar)) {
						
							if(controlFile.isControlFile) {
								allelenumber = Integer.parseInt(MethodLibrary.getValueFromInfo("AN", split[7], true));									 
								alleles = Integer.parseInt(MethodLibrary.getValueFromInfo("AC", split[7], false).split(",")[t]);
							} else {
								infosplit = split[split.length-1].split(":");
								if(infosplit[0].length() > 2 ) { 
									if(infosplit[0].charAt(0) == infosplit[0].charAt(2)) alleles = 2;								
									else alleles = 1;								
								}
								
								allelenumber = 2;						 
							}								
							if(alleles > 0) {
								samplelist = sampleNode.getValue();	    				
								samplelist.add(new SampleNode(alleles,allelenumber,controlFile));	    
							}
							continue;
						}
					}	    			
	    		 }		 
	    	 }
	    	 
			if(count % 10 == 0) {
				MainPane.drawCanvas.loadBarSample = (int)((current.getPosition()/(double)(Draw.getSplits().get(0).chromEnd))*100);     
				Setter.getInstance().setUpdateScreen();
			}			
		}
	   	if(samplelist != null) {
			samplelist = null;
		}
	   current = null;
	}
	catch(Exception e) {
		
		e.printStackTrace();
	}
}
public static class runner extends SwingWorker<String, Object> {		
	protected String doInBackground() {
		Loader.setLoading("Controlling");
		applyControl();
		
		Loader.ready("Controlling");
		return "";
	}	
}

public static void addFiles(File[] filestemp) {
  	try {
		for(int i=0;i<filestemp.length; i++) {	
			if(!filestemp[i].getName().endsWith(".vcf.gz")) continue;

			ControlFile addSample = new ControlFile(filestemp[i].getName(), (short)(controlData.fileArray.size()), filestemp[i].getCanonicalPath());			
			controlData.fileArray.add(addSample);		
			MethodLibrary.addHeaderColumns(addSample);			
			MainPane.controlDraw.trackDivider.add(0.0);
			ArrayList<String> populations = new ArrayList<String>();
			try {				
				GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(filestemp[i]));
				BufferedReader reader = new BufferedReader(new InputStreamReader(gzip));				
				String line = reader.readLine(), population = "";
				int idindex = 0, dotindex = 0;
				while(line.startsWith("#")) {
					if(line.contains("##INFO")) {
						if(line.contains("ID=")) {
							idindex = line.indexOf("ID=");
							dotindex = line.indexOf(",");
							if(idindex > 0 && dotindex > 0) {
								if(line.substring(idindex, dotindex).contains("AC")) {
									population = line.substring(idindex+3, dotindex).replace("AC", "").replace("_", "");
									if(population.length() == 0) population = "ALL";
									
									populations.add(population);			
								}
							}								
						}
					}
					line = reader.readLine();
				}
				addSample.isControlFile = line.contains("AN=");
				if(line.contains("SVTYPE")) addSample.SV = true;
				if(line.startsWith("chr")) addSample.chr = "chr";

				if (addSample.SV) {
					addSample.breakpoints = new ArrayList<BreakPoint>();
					
					while(line != null) {							
						String[] split = line.trim().split("\t");
						String SVtype = MethodLibrary.getValueFromInfo("SVTYPE", split[7], false);
						if (SVtype.equals("BND") || SVtype.equals("TRA")) addSample.breakpoints.add(new BreakPoint(split));
						line = reader.readLine();						
					}
					addSample.breakpoints.sort(Comparator.comparing(BreakPoint::getChr).thenComparingInt(BreakPoint::getPos));		
				}				
				
				addSample.addMenu(populations);
				reader.close();					
				gzip.close();
				//controlData.sampleCount +=samplecount;	
			}
			catch(Exception e){
				ErrorLog.addError(e.getStackTrace());
				e.printStackTrace();
			}					
		}

	
		if(controlData.fileArray.size() > 0 ) {	
			if(MainPane.trackPane.isVisible() && Getter.getInstance().getControlList().size() == 0) {
				MainPane.trackPane.setDividerLocation(MainPane.varpane.getDividerLocation());
			}
			for(int i = 0 ; i<MainPane.controlDraw.trackDivider.size(); i++) {
				MainPane.controlDraw.trackDivider.set(i, ((i+1)*(MainPane.varpane.getDividerLocation()/(double)MainPane.controlDraw.trackDivider.size())));
			}	
			if(!MainPane.trackPane.isVisible()) {
				MainPane.trackPane.setVisible(true);	
				MainPane.varpane.setDividerSize(2);		  
				MainPane.varpane.setDividerLocation(controlData.fileArray.size()*80);		  
			}
			else {				
				MainPane.varpane.setDividerLocation(MainPane.varpane.getDividerLocation()+80);
				
				if(MainPane.bedScroll.isVisible()) MainPane.trackPane.setDividerSize(2); }
			}
			
			MainPane.controlScroll.setVisible(true);
			MainPane.controlDraw.setVisible(true);
			hold = false;
		
 
 	} catch(Exception e) {
  		ErrorLog.addError(e.getStackTrace());
  		e.printStackTrace();
  	}
}
static void clearControls() {
	 VarNode current = Getter.getInstance().getVariantHead().getNext();
	 while(current != null) {
		 if(current.controlled) {
			 for(ArrayList<SampleNode> sampleList : current.vars.values()) {
					
					for(int i = sampleList.size()-1; i> -1;i-- ) {
						if(sampleList.get(i).alleles == null) {
							
							break;
						}
						
						sampleList.remove(i);
					
					}							
			 }
			 current.controlled = false;
		 }
		
		 current = current.getNext();
	 }
	 controlData.fileArray.clear();
//	 controlData.sampleArray.clear();	
	 //controlData.total = 0;
	 //controlData.sampleCount = 0;
	 current = null;
}

public static void dismissControls(VarNode head) {
	VarNode current = head;

	while(current != null) {
		 if(current.controlled) {
			 for(ArrayList<SampleNode> sampleList : current.vars.values()) {
					
					for(int i = sampleList.size()-1; i> -1;i-- ) {
						if(sampleList.get(i).alleles == null) {
							break;
						}
						sampleList.remove(i);
						
					}							
			 }
			 current.controlled = false;
		 }
		
		 current = current.getNext();
	 }
	for(int i = 0 ; i<controlData.fileArray.size(); i++) {
		controlData.fileArray.get(i).controlled = false;
	}
	
	current = null;
	head = null;
}
static void dismissControl(VarNode head, ControlFile sample) {
	VarNode current = head;
	sample.controlOn = false;
	MainPane.controlDraw.repaint();
	sample.controlled = false;
	while(current != null) {
		 if(current.controlled) {
			 for(ArrayList<SampleNode> sampleList : current.vars.values()) {
					
					for(int i = sampleList.size()-1; i> -1;i-- ) {
						if(sampleList.get(i).alleles == null) {
							break;
						}
						if(sampleList.get(i).getControlSample().equals(sample)) {
							sampleList.remove(i);
						}
					}							
			 }
			
		 }
		
		 current = current.getNext();
	 }
	for(int i = 0 ; i<controlData.fileArray.size(); i++) {
		controlData.fileArray.get(i).controlled = false;
	}
	
	Control.applyControl();

	current = null;
	head = null;
}
static void control(VarNode head) {
	try {
	//	Control.controlData.alleleThreshold = Double.parseDouble(allelebox.getText());
	}
	catch(Exception ec) {
		ec.printStackTrace();
		ErrorLog.addError(ec.getStackTrace());
	}
	try {
		
		runner runner = new runner();
	    runner.execute();
	     
	}
	catch(Exception ex) {
		ex.printStackTrace();
		ErrorLog.addError(ex.getStackTrace());
	}
}


static class MyFilter extends javax.swing.filechooser.FileFilter {
	public boolean accept(File file) { 
			if (file.isDirectory()) {
				return true;
		    }			
			
			else if(file.getName().matches(".*.ctrl.gz")) {
				return true;
			}
		/*	else if(file.getName().matches(".*.vcf")) {
				return true;
			}*/
			else if(file.getName().matches(".*.vcf.gz")) {
				return true;
			}
			else {
				return false;
			}		
	}
	
	public String getDescription() { return "*.ctrl.gz, *.vcf.gz"; }
	}


	

}


