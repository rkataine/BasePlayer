/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.control;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import base.BasePlayer.GUI.MainPane;


public class ControlData implements Serializable {
	private static final long serialVersionUID = 1L;
	public java.util.List<ControlFile> fileArray = Collections.synchronizedList(new ArrayList<ControlFile>());
	
	public boolean controlsOn = false;
	
	public void loadData() {
		if (MainPane.projectValues.controls.size() == 0) return;

		for (int i = 0; i < fileArray.size(); i++) {
			fileArray.get(i).alleleFreq = MainPane.projectValues.controls.get(i).alleleFreq;
			fileArray.get(i).alleletext = new StringBuffer(String.valueOf(MainPane.projectValues.controls.get(i).alleleFreq));
		}
	}
	
}
