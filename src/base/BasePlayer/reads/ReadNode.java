/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.reads;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Map.Entry;

import base.BasePlayer.BaseConstants;
import base.BasePlayer.BaseVariables;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.sample.Sample;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import htsjdk.samtools.Cigar;
import htsjdk.samtools.CigarElement;
import htsjdk.samtools.CigarOperator;
import htsjdk.samtools.SAMRecord;

public class ReadNode {
		static int arrowLength = 5;
		public static ReadNode selected = null;
    public static ReadNode clicked = null;
		private final int position, startoffset, matepos;
		private Color color;
		private int insertsize;
		private final Integer readWidth, realWidth;
		private final short quality;
		java.awt.Rectangle rect;
		private final String readName;
		private final boolean forward, isDiscordant, primary, mateforward, unmapped, first;
		private CigarElement element;
		private String mateChrom;
		ArrayList<Entry<Integer, Byte>> mismatchArray;
		private Entry<Integer, Byte> mismatchentry;
		public SplitClass split;
		ReadNode prev, next;
		public int yposition = 0;
		public ArrayList<ReadNode> mates;
		public final String SA;
		private final java.util.ArrayList<java.util.Map.Entry<Integer,Byte>> mismatches;
		private final Cigar cigar;
		public static String[] clickedReadInfo = new String[11];
		public static  SAread saReads = null;

		public ReadNode() {
			this.primary = true; this.unmapped = false;	this.position = -1;	this.rect = null;	this.readWidth = 0;	this.realWidth = 0; this.readName = null;	this.quality = 0;	this.insertsize = 0;
			this.forward = false;	SA = null;	this.matepos = -1;	this.mateChrom = null;	this.mateforward = false;	this.cigar = null;	this.isDiscordant = false; this.first = true;
			this.mismatches = null;	this.startoffset = 0;	this.color = Color.gray;
		}

		public ReadNode(final SAMRecord record, final boolean cg, final String chrom, final Sample sample, final SplitClass split, final Reads readClass, final java.util.ArrayList<java.util.Map.Entry<Integer, Byte>> mismatches, final boolean isDiscordant) {	
			
			this.rect = new java.awt.Rectangle();
			this.primary = !record.getSupplementaryAlignmentFlag();
			this.readName = record.getReadName();			
			this.unmapped = record.getReadUnmappedFlag();
			this.quality = this.unmapped ? -1 : (short) record.getMappingQuality();
			this.mismatches = mismatches;
			this.insertsize = record.getInferredInsertSize() == 0 ? record.getReadLength() : record.getInferredInsertSize(); 
			this.forward = !record.getReadNegativeStrandFlag();
			SA = record.getStringAttribute("SA");
			this.isDiscordant = SA != null ? true : isDiscordant;
			this.split = split;
			this.first = record.getReadPairedFlag() ? record.getFirstOfPairFlag() : true;
			int endpos;
			
			if (record.getCigarString().contains("H") || (SA != null)) {
				this.position = record.getAlignmentStart();
				endpos = record.getAlignmentEnd();
				this.startoffset = record.getAlignmentStart() - record.getUnclippedStart();
			} else {
				this.position = record.getUnclippedStart();
				endpos = record.getUnclippedEnd();
				this.startoffset = 0;
			}
			if (this.unmapped) endpos = this.position + record.getReadLength();
			this.readWidth = endpos - this.position + 1;
			this.realWidth = record.getUnclippedEnd() - record.getUnclippedStart() + 1;
			// TODO MT & M
			if (record.getReadPairedFlag() && (!record.getMateUnmappedFlag() || record.getReadUnmappedFlag() && record.getMateAlignmentStart() > 0)) {
				if (record.getMateReferenceName().contains("chr")) {
					if (record.getMateReferenceName().contains("M")) {
						this.mateChrom = record.getMateReferenceName().replace("chr", "") + "T";
					} else {
						this.mateChrom = record.getMateReferenceName().replace("chr", "");
					}
				} else {
					this.mateChrom = record.getMateReferenceName();
				}
				this.mateforward = !record.getMateNegativeStrandFlag();
				this.matepos = record.getMateAlignmentStart();

				try {
					if (this.isDiscordant && sample.getMates() != null && sample.getMates().size() > 0) {
						this.split = split;
						if (sample.getMates().containsKey(record.getReadName())) {
							this.mates = sample.getMates().get(record.getReadName());
							this.mates.add(this);
						}
					}
				} catch (final Exception e) {
					ErrorLog.addError(e.getStackTrace());
					e.printStackTrace();
				}
			} else if (SA != null) {
				//this.mateChrom = null;
				this.mateforward = false;
				this.matepos = -1;
				if (sample.getMates() != null) {
					if (sample.getMates().containsKey(record.getReadName())) {
						this.split = split;
						this.mates = sample.getMates().get(record.getReadName());
						this.mates.add(this);
					}
				}
			}	else {
				//this.mateChrom = null;
				this.mateforward = false;
				this.matepos = -1;
			}
			
			if (record.getCigarLength() > 1) this.cigar = record.getCigar();
			else cigar = null;
		
			if (this.isDiscordant()) this.color = getColor(chrom);
			else {
				if (this.getMappingQuality() > -1 && this.getMappingQuality() < Settings.mappingQ) this.color = BaseConstants.dark;
				else if (this.isForward()) this.color = BaseConstants.forwardColor;
				else this.color = BaseConstants.reverseColor;				
			}
			if (SA != null) {				
				String firstSplitChrom = SA.split(",")[0].replace("chr", "");
				if (!firstSplitChrom.equals(chrom)) this.color = getChromColor(firstSplitChrom);
			}
				
		}
		//METHODS
		public void setRectBounds(final int x, final int y, final int width, final int height) {	this.rect.setBounds(x, y, width, height);	}
		public java.awt.Rectangle getRect() {	return this.rect;	}
		Color getColor() {	return this.color; }
		public int getPosition() {	return this.position;	}
		public int getStartOffset() { return this.startoffset; }
		public int getWidth() {	return this.readWidth;	}
		int getRealWidth() { return this.realWidth; }
		public String getName() {	return this.readName;	}
		public int getMatePos() {	return this.matepos;	}
		public String getMateChrom() {	return this.mateChrom;	}
		int getInsertSize() {	return this.insertsize;	}
		boolean isForward() {	return this.forward;	}
		boolean isMateForward() {	return this.mateforward;	}
		public ReadNode getPrev() {	return this.prev;	}
		public ReadNode getNext() {	return this.next;	}
		public ArrayList<ReadNode> getMates() {	return this.mates; }
		boolean getPrimary() { return this.primary;	}
		void addMate(final ReadNode mate) {	this.mates.add(mate);	}
		public void setPrev(final ReadNode node) {	this.prev = node;	}
		public void setNext(final ReadNode node) {	this.next = node;	}	
		public boolean isDiscordant() {	return this.isDiscordant;	}
		boolean isUnmapped() {	return this.unmapped;	}	
		Cigar getCigar() { return this.cigar;	}
		public java.util.ArrayList<java.util.Map.Entry<Integer,Byte>> getMismatches() {	return this.mismatches;	}
		Short getMappingQuality() {	return this.quality; }

		public void draw(Graphics2D buf) {	
			this.split.getReadBuffer().setColor(this.getColor());		
			final Rectangle drawRect = this.getRect();
			final int ypos = drawRect.y;
			int clipLength = 0;
			int zoomOffset = this.isForward() ? 0 : arrowLength;
			// if zoomed out, remove arrows and show reads as lines
			if (this.split.pixel < 0.1) this.setRectBounds((int)drawRect.getX(), (int) drawRect.y, (int) (this.getWidth() * this.split.pixel) + arrowLength, BaseVariables.readHeight);
			else drawTriangles(false, buf);			

			if (this.getCigar() == null || this.split.pixel < 0.1) {
				this.split.getReadBuffer().fillRect((int) drawRect.getX() + zoomOffset, ypos, (int) drawRect.getWidth() - arrowLength, (int) drawRect.getHeight());
				
				/* if (this.getMatePos() == -1 && this.SA == null) {
					this.split.getReadBuffer().setColor(Color.white);
					this.split.getReadBuffer().drawRect((int) drawRect.getX() + zoomOffset, ypos, (int) drawRect.getWidth() - arrowLength, (int) drawRect.getHeight());
				} */
			
				if (this.isUnmapped()) {
					this.split.getReadBuffer().setColor(Color.red);
					this.split.getReadBuffer().drawRect((int) drawRect.getX() + zoomOffset, ypos, (int) drawRect.getWidth() - arrowLength, (int) drawRect.getHeight());
				}
			} else {	
				int thispos = this.getPosition();
				int insertion = 0;
				
				for (int c = 0; c < this.getCigar().numCigarElements(); c++) {
	
					element = this.getCigar().getCigarElement(c);
					if (element.getOperator().compareTo(CigarOperator.HARD_CLIP) == 0) continue;

					if (element.getOperator().compareTo(CigarOperator.MATCH_OR_MISMATCH) == 0 || element.getOperator().name().equals("=")) {
						int insertPos = (int) ((thispos + insertion - this.split.start) * this.split.pixel);
						int insertLength = (int) ((element.getLength() - insertion) * this.split.pixel) - 10;
						if ((c == 0 || (c == 1 && this.getCigar().getCigarElement(0).getOperator().compareTo(CigarOperator.HARD_CLIP) == 0)) && !this.isForward()) 
							this.split.getReadBuffer().fillRect(insertPos + 10, ypos,	insertLength,(int) drawRect.getHeight());
						else if (c == this.getCigar().numCigarElements() - 1 && this.isForward()) 
							this.split.getReadBuffer().fillRect(insertPos, ypos,	insertLength,	(int) drawRect.getHeight());
						else if ((c == this.getCigar().numCigarElements() - 2 && this.getCigar().getCigarElement(c + 1).getOperator().compareTo(CigarOperator.HARD_CLIP) == 0) && this.isForward())
							this.split.getReadBuffer().fillRect(insertPos, ypos, insertLength,	(int) drawRect.getHeight());					
				 		else this.split.getReadBuffer().fillRect(insertPos, ypos,	insertLength + 10,	(int) drawRect.getHeight());
	
						if (insertion > 0) insertion = 0;
						thispos += element.getLength();
					} else if (element.getOperator().compareTo(CigarOperator.DELETION) == 0) {
						int delPos = (int) ((thispos - this.split.start) * this.split.pixel);
						int delLength = (int) ((thispos + element.getLength() - this.split.start) * this.split.pixel);
						this.split.getReadBuffer().setColor(Color.black);
						
						if (element.getLength() * this.split.pixel > 5) {							
							this.split.getReadBuffer().drawLine(delPos, ypos + BaseVariables.readHeight / 2, delLength,	(int) drawRect.getY() + BaseVariables.readHeight / 2);	
						} else {							
							this.split.getReadBuffer().fillRect(delPos,	ypos, (int) (element.getLength() * this.split.pixel + 1), BaseVariables.readHeight);						
						}
						thispos += element.getLength();
						this.split.getReadBuffer().setColor(this.getColor());
					} else if (element.getOperator().compareTo(CigarOperator.INSERTION) == 0) {
						if (this.split.pixel > 5) {
							this.split.getReadBuffer().setColor(Color.white);
							this.split.getReadBuffer().fillRect((int) ((thispos - this.split.start) * this.split.pixel),	ypos, (int) (this.split.pixel + 1), BaseVariables.readHeight);
							this.split.getReadBuffer().setColor(Color.black);
							this.split.getReadBuffer().drawString("I" + element.getLength(), (int) ((thispos - this.split.start) * this.split.pixel) + 2, ypos + BaseVariables.readHeight);
							this.split.getReadBuffer().setColor(this.getColor());
						} else {
							this.split.getReadBuffer().setColor(Color.white);
							this.split.getReadBuffer().fillRect((int) ((thispos - this.split.start) * this.split.pixel),	ypos, (int) (element.getLength() * this.split.pixel + 1), BaseVariables.readHeight);
							this.split.getReadBuffer().setColor(this.getColor());
						}
	
						insertion = 1;
					} else if (element.getOperator().compareTo(CigarOperator.SOFT_CLIP) == 0) {
						if (this.SA == null) {							
							int startpos = (int) ((thispos + insertion - this.split.start) * this.split.pixel) - 1;
							if (!this.isForward() && c == 0) startpos += 11;
							int readwidth = (int) ((element.getLength() - insertion) * this.split.pixel);
							//TODO check readwidth (was -8 if start or end)
							this.split.getReadBuffer().fillRect(startpos, ypos,	readwidth,	(int) drawRect.getHeight());
							this.split.getReadBuffer().setColor(BaseConstants.softColor);
							this.split.getReadBuffer().fillRect(startpos, ypos,	readwidth,	(int) drawRect.getHeight());
								
							this.split.getReadBuffer().setColor(this.getColor());
							thispos += element.getLength();	
						} else {
							this.split.getReadBuffer().setColor(Color.white);
							if (c == 0) {
								clipLength = element.getLength();
								this.split.getReadBuffer().fillRect((int) ((thispos + insertion - this.split.start) * this.split.pixel) - 1, ypos - 2, 2, BaseVariables.readHeight + 4);
							} else this.split.getReadBuffer().fillRect((int) ((thispos + insertion - this.split.start) * this.split.pixel), ypos - 2, 2,	BaseVariables.readHeight + 4);
							this.split.getReadBuffer().setColor(this.getColor());	
						}
	
						if (insertion > 0) insertion = 0;
					} else if (element.getOperator().compareTo(CigarOperator.SKIPPED_REGION) == 0) {
						this.split.getReadBuffer().setColor(Color.black);
						this.split.getReadBuffer().drawLine((int) ((thispos - this.split.start) * this.split.pixel), ypos + BaseVariables.readHeight / 2, (int) ((thispos + element.getLength() - this.split.start) * this.split.pixel), (int) drawRect.getY() + BaseVariables.readHeight / 2);
						this.split.getReadBuffer().setColor(this.getColor());
						thispos += element.getLength();
					}
				}	
			}
			if (this.split.viewLength < 400 && this.getMismatches() != null) {
				if (this.split.getReadBuffer().getFont().getSize() != BaseVariables.readfont.getSize()) this.split.getReadBuffer().setFont(BaseVariables.readfont);
	
				mismatchArray = this.getMismatches();

				for (final Entry<Integer, Byte> entry : mismatches) {	
					if (!this.isForward()) {
						if (Character.isLowerCase((char) (byte) entry.getValue())) this.split.getReadBuffer().setColor(BaseConstants.reverseTextLow);
						else this.split.getReadBuffer().setColor(BaseConstants.reverseText);						
					} else {	
						if (Character.isLowerCase((char) (byte) entry.getValue())) this.split.getReadBuffer().setColor(BaseConstants.forwardTextLow);
						else this.split.getReadBuffer().setColor(BaseConstants.forwardText);
					}
					int x = (int) ((((this.getPosition() - clipLength + entry.getKey()) - this.split.start) * this.split.pixel) + this.split.pixel / 3);
					
					if (!MainPane.getBase.containsKey(entry.getValue())) this.split.getReadBuffer().setColor(Color.black);	
					this.split.getReadBuffer().drawString(MainPane.getBase.get(entry.getValue()), x, ypos + BaseVariables.readHeight);					
				}	
			} else if (this.split.viewLength < Settings.readDrawDistance) {	
				if (this.getMismatches() != null) {
					mismatchArray = this.getMismatches();
					for (int m = 0; m < mismatchArray.size(); m++) {
						mismatchentry = mismatchArray.get(m);
						if (!this.isForward()) {
							if (Character.isLowerCase((char) (byte) mismatchentry.getValue())) this.split.getReadBuffer().setColor(BaseConstants.reverseTextLow);
							else this.split.getReadBuffer().setColor(BaseConstants.reverseText);
						} else {	
							if (Character.isLowerCase((char) (byte) mismatchentry.getValue())) this.split.getReadBuffer().setColor(BaseConstants.forwardTextLow);
							else this.split.getReadBuffer().setColor(BaseConstants.forwardText);							
						}
	
						if ((char) (byte) mismatchentry.getValue() == ('I')) this.split.getReadBuffer().setColor(Color.black);
						
						int x = (int) (((this.getPosition() + mismatchentry.getKey()) - this.split.start) * this.split.pixel);
						this.split.getReadBuffer().fillRect(x, ypos, (int) (this.split.pixel + 1), BaseVariables.readHeight);	
					}
				}
			}
			/* if (this.getMatePos() == -1 && this.SA == null) {
				
				this.split.getReadBuffer().setColor(Color.white);
				this.split.getReadBuffer().drawRect(this.getRect().x, this.getRect().y, this.getRect().width,	this.getRect().height);
			} */
			if (this.isUnmapped()) {
				this.split.getReadBuffer().setColor(Color.red);
				this.split.getReadBuffer().drawRect((int) drawRect.getX() + zoomOffset, ypos, (int) drawRect.getWidth() - arrowLength, (int) drawRect.getHeight());
			}
		}

		public void drawClickedRead(Graphics2D buf) {
			if (clicked == null) return;
			Rectangle clickedRect = ReadNode.clicked.getRect();
			
			BasicStroke strongStroke = BaseConstants.strongStroke;
			Sample clickedReadSample = MainPane.drawCanvas.clickedReadSample;
			SAread saReads = ReadNode.saReads;
			int readwheel = MainPane.drawCanvas.readwheel;
			/* int readtextWidth = MainPane.drawCanvas.readtextWidth;
			String[] clickedReadInfo = MainPane.drawCanvas.clickedReadInfo; */
			ReadNode hoverMate = MainPane.drawCanvas.hoverMate;
			Boolean mouseDrag = MainPane.drawCanvas.mouseDrag;
			int drawWidth = Draw.getDrawWidth();
			Rectangle clickedMateRect = MainPane.drawCanvas.clickedMateRect;
			SplitClass selectedSplit = MainPane.drawCanvas.selectedSplit;
			Rectangle testRect = null;
			/* String[] hoverMateInfo = MainPane.drawCanvas.hoverMateInfo;
			int preread, thisread; */
			//int selectedIndex = MainPane.drawCanvas.selectedIndex;
			//QuadCurve2D curve = new QuadCurve2D.Double(0,0,40,40,0,0);

			buf.setStroke(strongStroke);
			
			if (clickedReadSample.getreadHash() == null	|| clickedReadSample.getreadHash().get(ReadNode.clicked.split) == null) {
				ReadNode.clicked = null;
				saReads = null;
				return;
			}
			if (readwheel != clickedReadSample.getreadHash().get(ReadNode.clicked.split).readwheel) readwheel = clickedReadSample.getreadHash().get(ReadNode.clicked.split).readwheel;			

			buf.setFont(BaseVariables.defaultFont);			
			//readtextWidth = buf.getFontMetrics().stringWidth(clickedReadInfo[12]);
			
			if (hoverMate != null) {
				if (hoverMate.equals(ReadNode.clicked)) buf.setColor(Color.white);
				else buf.setColor(Color.orange);				
			} else buf.setColor(Color.orange);
			
			if (mouseDrag) ReadNode.clicked.setRectBounds((int) ((ReadNode.clicked.getPosition() - ReadNode.clicked.split.start) * ReadNode.clicked.split.pixel),	ReadNode.clicked.getRect().y, (int) (ReadNode.clicked.getWidth() * ReadNode.clicked.split.pixel),	BaseVariables.readHeight);
			int width = Math.min(clickedRect.width - 3, MainPane.drawWidth);
			
			if (ReadNode.clicked.isForward()) 
				buf.drawRect(clickedRect.x + ReadNode.clicked.split.offset - 3, clickedRect.y - 3, width,	clickedRect.height + 5);
			else {
				buf.drawRect(clickedRect.x + ReadNode.clicked.split.offset + 2, clickedRect.y - 3,	width, clickedRect.height + 5);
				
			}
			/* if (clickedRect.x < 5 && clickedRect.x + clickedRect.width <= drawWidth) buf.drawRect(4 + ReadNode.clicked.split.offset, clickedRect.y - 3, clickedRect.width + ReadNode.clicked.getRect().x - 4, clickedRect.height + 5);
			 	else if (clickedRect.x < 5 && clickedRect.x + clickedRect.width > drawWidth) buf.drawRect(4 + ReadNode.clicked.split.offset, clickedRect.y - 3, drawWidth - 8, clickedRect.height + 5);
				else if (clickedRect.x + clickedRect.width > drawWidth) {
				if (ReadNode.clicked.isForward()) buf.drawRect(clickedRect.x + ReadNode.clicked.split.offset - 3, clickedRect.y - 3,	clickedRect.width + (drawWidth - (clickedRect.x + clickedRect.width)) - 3,	clickedRect.height + 5);
				else buf.drawRect(clickedRect.x + ReadNode.clicked.split.offset + 2, clickedRect.y - 3,	clickedRect.width + (drawWidth - (clickedRect.x + clickedRect.width)) - 3, clickedRect.height + 5);
			} else {
				if (ReadNode.clicked.isForward()) buf.drawRect(clickedRect.x + ReadNode.clicked.split.offset - 3, clickedRect.y - 3, clickedRect.width + 1,	clickedRect.height + 5);
				else buf.drawRect(clickedRect.x + ReadNode.clicked.split.offset + 2, clickedRect.y - 3, clickedRect.width,	clickedRect.height + 5);
			} */

			ReadNode.clicked.drawTriangles(true, buf);			
			
			buf.setStroke(strongStroke);			
			buf.setColor(Color.orange);

			if (ReadNode.clicked.getMates() != null) {
				for (int i = 0; i < ReadNode.clicked.getMates().size(); i++) {
					ReadNode mate = ReadNode.clicked.getMates().get(i);
					if (mate.equals(this)) continue;
					if (Draw.getSplits().indexOf(mate.split) < 0) {
						ReadNode.clicked.getMates().remove(i);
						i--;
						continue;
					}

					if (hoverMate != null) {
						if (hoverMate.equals(mate)) buf.setColor(Color.white);
						else buf.setColor(Color.orange);						
					} else buf.setColor(Color.orange);
					
					clickedMateRect = mate.getRect();
					
					if (mouseDrag) //TODO
						mate.setRectBounds((int) ((mate.getPosition()	- mate.split.start)	* mate.split.pixel),	clickedMateRect.y,	(int) (mate.getWidth()	* mate.split.pixel),	BaseVariables.readHeight);
					
					mate.drawTriangles(true, buf);

					if (mate.split != null) {
						if (clickedMateRect.width < 2) {
							if (mate.split.equals(selectedSplit))	
								buf.drawRect(clickedMateRect.x + mate.split.offset,	mate.yposition - 3, 2, BaseVariables.readHeight);	
						} else {
							mate.setRectBounds((int) ((mate.getPosition()	- mate.split.start)	* mate.split.pixel),
							mate.rect.y,	(int) (mate.getWidth() * mate.split.pixel),	BaseVariables.readHeight);

							if (clickedMateRect.x < 5 && clickedMateRect.x + clickedMateRect.width <= drawWidth) 
								buf.drawRect(4 + mate.split.offset,	mate.yposition - 3,	clickedMateRect.width + clickedMateRect.x - 4, clickedMateRect.height + 5);
							else if (clickedMateRect.x < 5 && clickedMateRect.x + clickedMateRect.width > drawWidth) 
								buf.drawRect(4 + mate.split.offset,	mate.yposition - 3, drawWidth - 8,	clickedMateRect.height + 5);

							else if (clickedMateRect.x + clickedMateRect.width > drawWidth) 
								buf.drawRect(clickedMateRect.x + mate.split.offset,	mate.yposition - 3,	clickedMateRect.width	+ (drawWidth - (clickedMateRect.x + clickedMateRect.width)) - 4, clickedMateRect.height + 5);
							else {
								if (ReadNode.clicked.isForward()) 
									buf.drawRect(clickedMateRect.x + mate.split.offset - 3,	mate.yposition - 3, clickedMateRect.width + 1,	clickedMateRect.height + 5);
								else 
									buf.drawRect(clickedMateRect.x + mate.split.offset + 2,	mate.yposition - 3, clickedMateRect.width, clickedMateRect.height + 5);
							}
						}

						testRect = clickedMateRect;
						testRect.setBounds(testRect.x, mate.yposition, testRect.width,	testRect.height);

						if (MainPane.drawCanvas.readRect.intersects(testRect)	&& mate.split.equals(selectedSplit)) {
							if (hoverMate == null || !hoverMate.equals(mate)) {
								hoverMate = mate;
								if (!ReadNode.clicked.equals(mate)) {
									//hoverMateInfo = hoverMate.createReadInfo();
									if (saReads != null && saReads.reads != null) {
										for (int r = 0; r < saReads.reads.size(); r++) {
											if (saReads.reads.get(r)[SAread.SARead] == null) {
												if ((int) saReads.reads.get(r)[SAread.pos] == hoverMate.getPosition()) {
													saReads.reads.get(r)[SAread.SARead] = hoverMate;
													break;
												}
											}
										}
									}
								}
							}
							//if (!ReadNode.clicked.equals(mate)) {
								//drawInfoBox(hoverMateInfo, readtextWidth + 30, hoverMate,	buf.getFontMetrics().stringWidth(hoverMateInfo[12]));
								buf.setColor(Color.orange);
								buf.setStroke(strongStroke);
							//}
						}

						if (i > 0) {
							/* if (ReadNode.clicked.getMates().get(i - 1).split.equals(mate.split)) {
								if (ReadNode.clicked.getMates().get(i - 1).getRect().x	+ ReadNode.clicked.getMates().get(i - 1).getRect().width > mate.getRect().x) {
									preread = ReadNode.clicked.getMates().get(i - 1).getRect().x	+ ReadNode.clicked.getMates().get(i - 1).split.offset;
									thisread = mate.getRect().x	+ mate.split.offset;
								} else {
									preread = ReadNode.clicked.getMates().get(i - 1).getRect().x + ReadNode.clicked.getMates().get(i - 1).getRect().width	+ ReadNode.clicked.getMates().get(i - 1).split.offset;
									thisread = mate.getRect().x	+ mate.split.offset;
								}
							} else {
								preread = ReadNode.clicked.getMates().get(i - 1).getRect().x	+ ReadNode.clicked.getMates().get(i - 1).getRect().width	+ ReadNode.clicked.getMates().get(i - 1).split.offset;
								thisread = mate.getRect().x	+ mate.split.offset;
							} */

							/* if (!ReadNode.clicked.getMates().get(i - 1).split.equals(mate.split)) {
								if (Getter.getInstance().getSampleList().get(selectedIndex).getreadHash().get(ReadNode.clicked.getMates().get(i - 1).split) == null) {
									ReadNode.clicked.getMates().remove(i - 1);
									continue;
								}
								//curve.setCurve(preread, ReadNode.clicked.getMates().get(i - 1).yposition,	Math.abs(thisread - preread) / 2 + preread,	ReadNode.clicked.getMates().get(i - 1).yposition - 100, thisread,	mate.yposition);
							} */ //else curve.setCurve(preread, ReadNode.clicked.getMates().get(i - 1).yposition,	Math.abs(thisread - preread) / 2 + preread,	mate.yposition - 100, thisread,	mate.yposition);
			
							//buf.setColor(Color.white);
							//buf.setStroke(BaseConstants.doubledashed);
							//buf.draw(curve);
							buf.setColor(Color.orange);
							buf.setStroke(strongStroke);
						}
					}
				}
			}		
		buf.setStroke(BaseConstants.basicStroke);
	}
		Color getColor(final String chrom) {
			if (this.getMateChrom() != null && !this.getMateChrom().equals(chrom)) return getChromColor(this.getMateChrom());	
	
			if (this.getMatePos() != -1) {			
				if (this.isDiscordant() && this.isForward() == this.isMateForward()) return Color.blue;	
				if (Math.abs(this.getInsertSize()) > Settings.insertSize) {					
					if (this.getInsertSize() < 0 && this.isForward()) return Color.gray;
					if (this.getInsertSize() > 0 && !this.isForward()) return Color.gray;
					else return Color.green;					
				}				
			}
			return Color.lightGray;
		}
		public void drawTriangles(boolean selected, Graphics2D buf) {
			if (selected) {	
				buf.setColor(Color.white);
				int x = this.isForward() ? (int) this.getRect().x + split.offset + this.getRect().width + 2 : (int) this.getRect().x + split.offset - 2;
				buf.fillPolygon(Draw.makeTriangle(x, this.getRect().y-2, arrowLength*2, this.getRect().height+4, this.isForward()));
				return;
			} else {
				this.split.getReadBuffer().setColor(this.getColor());
				int x = this.isForward() ? (int) this.getRect().x + this.getRect().width - arrowLength : (int) this.getRect().x + arrowLength;
				this.split.getReadBuffer().fillPolygon(Draw.makeTriangle(x, this.getRect().y, arrowLength, this.getRect().height, this.isForward()));
			}
		}
		static String mapToNumber(final String chrom) { // TODO create better mapper and move to methodlibrary
			if (chrom.equals("I")) return "1"; if (chrom.equals("II")) return "2"; if (chrom.equals("III")) return "3";
			if (chrom.equals("IV")) return "4"; if (chrom.equals("V")) return "5"; if (chrom.equals("VI")) return "6";
			if (chrom.equals("VII")) return "7"; if (chrom.equals("VIII")) return "8"; if (chrom.equals("IX")) return "9";
			if (chrom.equals("X")) return "10"; if (chrom.equals("XI")) return "11"; if (chrom.equals("XII")) return "12";
			if (chrom.equals("XIII")) return "13"; if (chrom.equals("XIV")) return "14"; if (chrom.equals("XV")) return "15";
			if (chrom.equals("XVI")) return "16";
			return "0";
		}
		static Color getChromColor(String chrom) {
			if (Character.isLetter(chrom.charAt(0)) && !chrom.equals("X")) chrom = mapToNumber(chrom);
			if (BaseConstants.CHROM_COLORS.containsKey(chrom)) return BaseConstants.CHROM_COLORS.get(chrom);
			else return Color.cyan;			
		}

		public String[] createReadInfo() {
			final String[] temp = new String[12];
			temp[0] = "" + this.getName();		
			temp[1] = "Position: chr" + this.split.chrom + ":" + MethodLibrary.formatNumber(this.getPosition())	+ (this.isForward() ? " (+)" : " (-)");
						
			temp[2] = "Mapping quality: " + this.getMappingQuality();
			temp[3] = "Insert size: " + MethodLibrary.formatNumber(this.getInsertSize()) + "bp";
			if (this.getCigar() == null) {
				temp[4] = "Cigar: " + this.getWidth() + "M";
				temp[5] = "Length: " + this.getWidth() + "bp";
			} else {
				if (this.getCigar().toString().length() > 50) 
					temp[4] = "Cigar: " + this.getCigar().toString().substring(0, 20) + " ... " + this.getCigar().toString().substring(this.getCigar().toString().length() - 20, this.getCigar().toString().length());
				else 
					temp[4] = "Cigar: " + this.getCigar().toString();	
				
				temp[5] = "Length: " + this.getWidth() + "bp";
			}
			temp[8] = this.first ? "First in pair" : "Second in pair";	
			if (this.getMatePos() == -1) {	
				temp[6] = "No mate";
				temp[7] = "Primary alignment: " + this.getPrimary();
						
			} else {			
				temp[6] = "Mate position: chr" + this.getMateChrom() + ":" + MethodLibrary.formatNumber(this.getMatePos()) + (ReadNode.clicked.isMateForward() ? " (+)" : " (-)");				
				temp[7] = "Primary alignment: " + this.getPrimary();			
			}
			temp[9] = temp[10] = "";
			if (this.SA != null) {
				final String[] SAs = this.SA.split(";");
				temp[9] = "Split position(s): ";
				for (int i = 0; i < SAs.length; i++) {
					final String[] sa = SAs[i].split(",");
					sa[0] = sa[0].replace("chr", "");
					temp[9] += "chr" + sa[0] + ":" + MethodLibrary.formatNumber(Integer.parseInt(sa[1])) +"(" +sa[2] +")";
					if (i < SAs.length - 1) temp[9] += ", ";					
				}
				temp[7] = "Primary alignment: " + this.getPrimary();
				//temp[10] = "Double click to view the splitted read.";
			}
			temp[11] = this.isUnmapped() ? "Unmapped" : "";
			
			
			return temp;	
		}
		void drawInfoBox(final String[] readinfo, final int boxoffset, final ReadNode read, int boxwidth) {
			
			//MethodLibrary.showReadMenu(MainPane.drawCanvas, read);
			/* if (boxwidth < 320)	boxwidth = 320;
			Graphics2D buf = MainPane.drawCanvas.buf;
			SAread saReads = MainPane.drawCanvas.saReads;
			Rectangle saReadRect = new Rectangle(); // = saReads != null ? saReads.read.getRect() : null;
		
			int boxheight;
			// TODO SCALE
			buf.setColor(BaseConstants.infoBoxColor);
			
			buf.fillRect(10 + MainPane.sidebarWidth + boxoffset, 10, boxwidth + 10, (readinfo.length) * 21 + (saReads == null || boxoffset > 100 ? 0 : saReads.reads.size() * 21));	
			buf.drawRect(10 + MainPane.sidebarWidth + boxoffset, 10, boxwidth + 10, (readinfo.length) * 21 + (saReads == null || boxoffset > 100 ? 0 : saReads.reads.size() * 21));
			boxheight = (readinfo.length) * 21 + 10 + (saReads == null || boxoffset > 100 ? 0 : saReads.reads.size() * 21);
		
			buf.setStroke(BaseConstants.dashed);
			//TODO clean next drawing
			if (read.getRect().x > Draw.getDrawWidth() || read.getRect().x < 0) {
	
			} else if (read.getRect().x > 0) {
				if (read.getRect().x + read.split.offset - MainPane.sidebarWidth > boxoffset + boxwidth + 20) 
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset,	read.getRect().y, boxwidth + boxoffset + 20 + MainPane.sidebarWidth, 10);
				else 
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset,	read.getRect().y, boxwidth + boxoffset + 20 + MainPane.sidebarWidth, boxheight);
				
				if (read.getRect().y > boxheight) 
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset,	read.getRect().y, 10 + boxoffset + MainPane.sidebarWidth, boxheight);
				else 
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset,	read.getRect().y, boxwidth + 20 + boxoffset + MainPane.sidebarWidth, boxheight);
				
			} else {
				if (read.getRect().x + read.getRect().width + read.split.offset - MainPane.sidebarWidth > boxoffset + boxwidth	+ 20)
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset + read.getRect().width,	read.getRect().y, boxwidth + boxoffset + 20 + MainPane.sidebarWidth, 10);
				else
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset	+ read.getRect().width,	read.getRect().y, boxwidth + boxoffset + 20 + MainPane.sidebarWidth, boxheight);
				
				if (clicked.getRect().y > boxheight)
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset	+ read.getRect().width,	read.getRect().y, 20 + boxoffset + MainPane.sidebarWidth, boxheight);
				else 
					buf.drawLine((int) ((read.getPosition() - read.split.start) * read.split.pixel) + read.split.offset	+ read.getRect().width,	read.getRect().y, boxwidth + boxoffset + 20 + MainPane.sidebarWidth, boxheight);
			}
			buf.setStroke(BaseConstants.basicStroke);
			buf.setColor(Color.black);

			for (int i = 0; i < readinfo.length - 1; i++) buf.drawString(readinfo[i], 15 + MainPane.sidebarWidth + boxoffset, 25 + i * 20);
			
				if (saReads != null && boxoffset < 100) {
					buf.setColor(Color.lightGray);	
					buf.fillRect(15 + MainPane.sidebarWidth + boxoffset, readinfo.length * 20 - 20, boxwidth - 10, 15);
					buf.fillPolygon(Draw.makeTriangle(15 + MainPane.sidebarWidth + boxoffset + (boxwidth - 10),	readinfo.length * 20 - 23, 10, 21, true));
					buf.setColor(Color.black);
					buf.drawRect(15 + MainPane.sidebarWidth + boxoffset, readinfo.length * 20 - 20, boxwidth - 10, 15);
					buf.drawPolygon(Draw.makeTriangle(15 + MainPane.sidebarWidth + boxoffset + (boxwidth - 10),	readinfo.length * 20 - 23, 10, 21, true));
					buf.drawString("Fragment length: " + saReads.readlength + "bp", 15 + MainPane.sidebarWidth + boxoffset + 5,	readinfo.length * 20 - 8);

					for (int i = 0; i < saReads.reads.size(); i++) {
						buf.setColor(ReadNode.getChromColor((String) (saReads.reads.get(i)[SAread.chrom])));
						
						if ((boolean) saReads.reads.get(i)[SAread.forward]) {
							saReadRect.setBounds(15 + MainPane.sidebarWidth + boxoffset	+ (int) ((double) saReads.reads.get(i)[SAread.relativepos] * boxwidth),	readinfo.length * 20 + (6 + i * 21), (int) ((double) saReads.reads.get(i)[SAread.relativelength] * boxwidth), 15);
							buf.fillRect(saReadRect.x, saReadRect.y, saReadRect.width - 5, 15);
							buf.fillPolygon(Draw.makeTriangle(saReadRect.x + saReadRect.width - 5, saReadRect.y - 3, 5, 21, true));
							if (saReads.reads.get(i)[SAread.SARead] != null) {
								if (MainPane.drawCanvas.hoverMate != null && saReads.reads.get(i)[SAread.SARead].equals(MainPane.drawCanvas.hoverMate)) {	
									buf.setColor(Color.white);
									buf.setStroke(BaseConstants.doubleStroke);
								} else if (saReads.reads.get(i)[SAread.SARead].equals(saReads.read)) {
									buf.setColor(Color.orange);
									buf.setStroke(BaseConstants.doubleStroke);	
								}	else {
									buf.setColor(Color.black);
									buf.setStroke(BaseConstants.basicStroke);
								}
								buf.drawRect(saReadRect.x, saReadRect.y, saReadRect.width - 5, 15);
								buf.drawPolygon(Draw.makeTriangle(saReadRect.x + saReadRect.width - 5, saReadRect.y - 3, 5, 21, true));
								buf.setStroke(BaseConstants.basicStroke);
							} else {
								buf.setColor(Color.black);
								buf.drawRect(saReadRect.x, saReadRect.y, saReadRect.width - 5, 15);
								buf.drawPolygon(Draw.makeTriangle(saReadRect.x + saReadRect.width - 5, saReadRect.y - 3, 5, 21, true));
							}
							saReadRect.setBounds(15 + boxoffset + (int) ((double) saReads.reads.get(i)[SAread.relativepos] * boxwidth),	saReadRect.y, saReadRect.width, saReadRect.height);
		
							if (MainPane.drawCanvas.readRect.intersects(saReadRect)) {
								if (MainPane.drawCanvas.hoverMate == null || !MainPane.drawCanvas.hoverMate.equals((ReadNode) saReads.reads.get(i)[SAread.SARead])) {
									MainPane.drawCanvas.hoverMate = (ReadNode) saReads.reads.get(i)[SAread.SARead];
									MainPane.drawCanvas.hoverMate.getRect().setBounds(15 + boxoffset + (int) ((double) saReads.reads.get(i)[SAread.relativepos]	* boxwidth),	saReadRect.y, saReadRect.width, saReadRect.height);
								}
		
								if (MainPane.drawCanvas.hoverMate == null && ReadNode.clicked.getMates() != null) {
									for (int m = 0; m < ReadNode.clicked.getMates().size(); m++) {
										for (int r = 0; r < saReads.reads.size(); r++) {
											if ((int) saReads.reads.get(r)[SAread.pos] == ReadNode.clicked.getMates().get(m).getPosition()) {
												saReads.reads.get(r)[SAread.SARead] = ReadNode.clicked.getMates().get(m);
												break;
											}
										}
									}
								}
							}
						} else {
							saReadRect.setBounds(20 + MainPane.sidebarWidth + boxoffset	+ (int) ((double) saReads.reads.get(i)[SAread.relativepos] * boxwidth),	readinfo.length * 20 + (6 + i * 21),(int) ((double) saReads.reads.get(i)[SAread.relativelength] * boxwidth), 15);
		
							buf.fillRect(saReadRect.x, saReadRect.y, saReadRect.width - 5, 15);
							buf.fillPolygon(Draw.makeTriangle(saReadRect.x, saReadRect.y - 3, 5, 21, false));
		
							if (saReads.reads.get(i)[SAread.SARead] != null) {	
								if (MainPane.drawCanvas.hoverMate != null && saReads.reads.get(i)[SAread.SARead].equals(MainPane.drawCanvas.hoverMate)) {	
									buf.setColor(Color.white);
									buf.setStroke(BaseConstants.doubleStroke);
								} else if (saReads.reads.get(i)[SAread.SARead].equals(saReads.read)) {
									buf.setColor(Color.orange);
									buf.setStroke(BaseConstants.doubleStroke);	
								}	else {
									buf.setColor(Color.black);
									buf.setStroke(BaseConstants.basicStroke);
								}
		
								buf.drawRect(saReadRect.x, saReadRect.y, saReadRect.width - 5, 15);
								buf.drawPolygon(Draw.makeTriangle(saReadRect.x, saReadRect.y - 3, 5, 21, false));
								buf.setStroke(BaseConstants.basicStroke);
							} else {
								buf.setColor(Color.black);
								buf.drawRect(saReadRect.x, saReadRect.y, saReadRect.width - 5, 15);
								buf.drawPolygon(Draw.makeTriangle(saReadRect.x, saReadRect.y - 3, 5, 21, false));
							}
							saReadRect.setBounds(15 + boxoffset + (int) ((double) saReads.reads.get(i)[SAread.relativepos] * boxwidth),	saReadRect.y, saReadRect.width, saReadRect.height);
		
							if (MainPane.drawCanvas.readRect.intersects(saReadRect)) {
								if (MainPane.drawCanvas.hoverMate == null || !MainPane.drawCanvas.hoverMate.equals((ReadNode) saReads.reads.get(i)[SAread.SARead])) {
									MainPane.drawCanvas.hoverMate = (ReadNode) saReads.reads.get(i)[SAread.SARead];
									MainPane.drawCanvas.hoverRect.setBounds(15 + boxoffset	+ (int) ((double) saReads.reads.get(i)[SAread.relativepos]	* boxwidth),	saReadRect.y, saReadRect.width, saReadRect.height);
								}
		
								if (MainPane.drawCanvas.hoverMate == null && ReadNode.clicked.getMates() != null) {
									for (int m = 0; m < ReadNode.clicked.getMates().size(); m++) {
										for (int r = 0; r < saReads.reads.size(); r++) {
											if ((int) saReads.reads.get(r)[SAread.pos] == ReadNode.clicked.getMates().get(m).getPosition()) {
												saReads.reads.get(r)[SAread.SARead] = ReadNode.clicked.getMates().get(m);
												break;
											}
										}
									}
								}
							}
						}
					}
				} */
		}
}
