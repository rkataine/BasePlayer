/* Author: Riku Katainen @ University of Helsinki
 * 
 * Tumor Genomics Group (http://research.med.helsinki.fi/gsb/aaltonen/) 	
 * Contact: riku.katainen@helsinki.fi / help@baseplayer.fi
 * 
 * LICENSE: 
 * 
 * GNU AFFERO GENERAL PUBLIC LICENSE
 * Version 3, 19 November 2007
 *  
 */
package base.BasePlayer.reads;

import java.awt.Rectangle;
import java.util.ArrayList;

import base.BasePlayer.BaseVariables;
import base.BasePlayer.ErrorLog;
import base.BasePlayer.Getter;
import base.BasePlayer.MethodLibrary;
import base.BasePlayer.GUI.Draw;
import base.BasePlayer.GUI.MainPane;
import base.BasePlayer.GUI.SplitClass;
import base.BasePlayer.GUI.modals.Settings;
import base.BasePlayer.sample.Sample;

import java.awt.Color;
import java.awt.Graphics2D;

public class Reads implements Cloneable {
	public Sample sample;
	final Color[] colorArray = { Color.green, Color.cyan, Color.orange, Color.red, Color.gray, Color.white, Color.white };
	final String[] baseArray = { "A", "C", "G", "T", "N", "INS", "DEL" };
	private transient double maxcoverage = 0.0;
	private transient double[][] coverages;
	private transient ArrayList<ReadNode> reads = new ArrayList<ReadNode>();
	private transient ArrayList<ReadNode[]> headAndTail = new ArrayList<ReadNode[]>();
	private transient int runs = 1;
	private transient ReadNode firstRead, lastRead;
	private transient int maxReadSize = 0;
	public int startpos, endpos;
	public int readwheel = 0;
	private transient Rectangle scrollbar = new Rectangle(), scroller = new Rectangle();
	private transient Integer readstart=Integer.MAX_VALUE, readend=0, coveragestart = Integer.MAX_VALUE, coverageend = 0;
	public int searchstart = Integer.MAX_VALUE;
  public int searchend = 0;
	private transient boolean readScroll = false;
	int coveragebottom;
	public boolean loading = false, nodraw = false;
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	public int getRuns() {return this.runs;}
	public void setRuns(final int n) {this.runs = n;}
	public Rectangle getScrollBar() {return this.scrollbar;}
	public Rectangle getScroller() {return this.scroller;}
	public boolean isReadScroll() {return this.readScroll;}
	public void setReadScroll(final boolean value) {this.readScroll = value;}
	public void setMaxcoverage(final double coverage) {	this.maxcoverage = coverage;}
	public double getMaxcoverage() {
		if (Settings.fixedCoverage > 0) {
			return Settings.fixedCoverage;
		} else {
			return this.maxcoverage;
		}
	}

	public int getReadSize() { return this.maxReadSize; }
	public void setReadSize(final int max) {	this.maxReadSize = max; }
	public double[][] getCoverages() {return this.coverages;	}
	public void setCoverages(final double[][] coverages) {	this.coverages = coverages;	}
	public Integer getReadStart() {	return readstart;	}
	public int getCoverageStart() {	return this.coveragestart;}
	public int getCoverageEnd() {	return this.coverageend;}
	public void setCoverageStart(final int start) {	this.coveragestart = start;	}
	public void setCoverageEnd(final int end) {	this.coverageend = end;	}
	public int getReadEnd() {	return readend;}
	public void setReadStart(final int value) {	this.readstart = value;	}
	public void setReadEnd(final int value) {	this.readend = value;	}
	public ReadNode getFirstRead() {return this.firstRead;}
	public ReadNode getLastRead() {	return this.lastRead;	}
	public void setFirstRead(final ReadNode read) {	this.firstRead = read;}
	public void setLastRead(final ReadNode read) {this.lastRead = read;	}

	public void resetReads() {
		this.reads = new ArrayList<ReadNode>();
		this.headAndTail = new ArrayList<ReadNode[]>();
	}

	public ArrayList<ReadNode> getReads() {	return this.reads;}
	public ArrayList<ReadNode[]> getHeadAndTail() {	return this.headAndTail;}
	public void drawCoverage(final SplitClass split) {

		if (split.viewLength <= Settings.settings.get("coverageDrawDistance") && Draw.drawVariables.sampleHeight > Settings.coverageWindowHeight) {
			try {				
				if (Settings.onlyCoverage.isSelected() || split.getDivider() == null || (split.getDivider() != 1 && split.viewLength > Settings.readDrawDistance)) split.setDivider(1.0);
				else if (split.getDivider() != 5.0 && split.viewLength <= Settings.readDrawDistance) split.setDivider(5.0);
			
				if (Settings.onlyCoverage.isSelected()) split.getReadBuffer().setColor(Draw.backColor);
				else split.getReadBuffer().setColor(Color.gray);
				int drawY = sample.getIndex() - Draw.drawVariables.getVisibleStart.get();

				split.getReadBuffer().fillRect(0, (int) (drawY * Draw.drawVariables.sampleHeight),	Draw.getDrawWidth(), (int) (Draw.drawVariables.sampleHeight / split.getDivider()));
				split.getReadBuffer().setColor(Color.black);
				coveragebottom = (int) ((drawY * Draw.drawVariables.sampleHeight) + (Draw.drawVariables.sampleHeight / split.getDivider()));
				split.getReadBuffer().fillRect(0, coveragebottom, Draw.getDrawWidth(), 2);

				if (split.getDivider() != 1) {
					split.getReadBuffer().setColor(Draw.backTransparent);
					split.getReadBuffer().fillRect(0, (int) ((sample.getIndex()) * Draw.drawVariables.sampleHeight),
							Draw.getDrawWidth(), (int) (Draw.drawVariables.sampleHeight / split.getDivider()) - 2);
				}

				if (this.getCoverages() != null) {

					split.getReadBuffer().setColor(Color.black);

					if (split.viewLength > Settings.readDrawDistance) {
						
						if ((MainPane.drawCanvas.lineZoomer && !split.equals(MainPane.drawCanvas.selectedSplit)) || !MainPane.drawCanvas.lineZoomer) {

							for (int j = (int) ((this.getCoverageStart() - split.start) * split.pixel); j < this.getCoverages().length; j++) {
								if (j < 0)	continue;
								if ((int) ((this.getCoverageStart() - split.start) * split.pixel) + j > MainPane.drawCanvas.getWidth()) break;
								
								split.getReadBuffer().fillRect(j + (int) ((this.getCoverageStart() - split.start) * split.pixel),	coveragebottom- (int) ((this.getCoverages()[j][0] / (double) this.getMaxcoverage())
								* (Draw.drawVariables.sampleHeight / split.getDivider())), 1, (int) ((this.getCoverages()[j][0] / (double) this.getMaxcoverage())	* (Draw.drawVariables.sampleHeight / split.getDivider())));
							}
						}
					} else {						
						MainPane.drawCanvas.coveragevariant = -1;
						MainPane.drawCanvas.overlap = false;					
						drawCoverages(split);
					}
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		} else {
			if (!split.clearedReads) {
				split.clearedCoverages = true;
				
				if (this.getCoverageStart() != 0) {
					this.setCoverages(null);
					this.setCoverageEnd(0);
					this.setCoverageStart(Integer.MAX_VALUE);
				}
			}
		}
	}

	void drawCoverages(final SplitClass split) {
		final double coveragePanelHeight = Draw.drawVariables.sampleHeight / split.getDivider();
		int prepixel = -1;
		int maxcovtemp = 0;
		if (this.getCoverageStart() == Integer.MAX_VALUE) return; //TODO miksi readien refresh tuo aluksi tähän?
		for (int j = (int) (split.start - this.getCoverageStart()); j < this.getCoverages().length; j++) {
			if (j < 0)	continue;
			if (j + this.getCoverageStart() > split.end) break;

			final int coverage = (int) this.getCoverages()[j][0];

			if (maxcovtemp < coverage) maxcovtemp = coverage;

			if (prepixel < 0 || prepixel < (int) ((j * split.pixel)	+ ((this.getCoverageStart() - split.start) * split.pixel))) {
				final int x = (int) ((j * split.pixel) + ((this.getCoverageStart() - split.start) * split.pixel));
				final int y = (int) (coveragebottom	- (coverage / (double) this.getMaxcoverage() * coveragePanelHeight) + 1);
				final int width = (int) split.pixel + 1;
				final int height = (int) (coverage / (double) this.getMaxcoverage() * coveragePanelHeight);
				split.getReadBuffer().fillRect(x, y, width, height);
				prepixel = (int) ((j * split.pixel) + ((this.getCoverageStart() - split.start) * split.pixel));
			}

			for (int i = 1; i < this.getCoverages()[j].length; i++) {
				final int allelicCount = (int) this.getCoverages()[j][i];

				if (this.getCoverages()[j][i] >= Settings.coverageAlleleFreq) {
					split.getReadBuffer().setColor(colorArray[i - 1]);
					final int coveragevariantheight = (int) (allelicCount / (double) this.getMaxcoverage() * coveragePanelHeight);

					final int x = (int) (((j) * split.pixel) + ((this.getCoverageStart() - split.start) * split.pixel));
					final int y = coveragebottom - coveragevariantheight;
					final int width = (int) split.pixel + 1;
					int height = (int) (allelicCount / (double) this.getMaxcoverage() * coveragePanelHeight);
					if (height < 1) height = 1;
					// don't draw if DEL
					if (i != 7) {
						split.getReadBuffer().fillRect(x, y, width, height);
					}

					split.getReadBuffer().setColor(Color.black);
				}
			}
		}
		
		if ((int) this.getMaxcoverage() != maxcovtemp) {
			this.setMaxcoverage(maxcovtemp);
		}
	}

	public void drawCoverageMouseHover(final SplitClass split, Graphics2D drawBuffer) {
		if (this.getCoverages() == null) return;
		final int moveY = MainPane.drawCanvas.moveY - Draw.scrollValue;
		
		final int moveX = MainPane.drawCanvas.moveX;
		
		if (moveY >= coveragebottom) return;
		if (split.viewLength > Settings.settings.get("coverageDrawDistance")) return;
		if (moveX <= split.offset) return;

		final double coveragePanelHeight = Draw.drawVariables.sampleHeight / split.getDivider();
		final double sampleDivider = this.sample.getIndex() * Draw.drawVariables.sampleHeight;
		
		if (MainPane.drawCanvas.moveY <= sampleDivider || MainPane.drawCanvas.moveY >= sampleDivider + coveragePanelHeight) return;
		
		final int mousePosition = MainPane.chromDraw.getPosition(moveX-split.offset, split);
		final int coverageArrayPosition = (int) (mousePosition - this.getCoverageStart());
		if (coverageArrayPosition < 0 || coverageArrayPosition >= this.getCoverages().length) return;
		if (split.viewLength < Settings.readDrawDistance) {
			ReadNode.selected = null;
			final int overlapCoverage = (int)this.getCoverages()[coverageArrayPosition][0];
			String baseChange = "";
			Color overlapColor = Color.black;
			String overlapText = "";
			
			final int boxheight = BaseVariables.defaultFontSize * 3;
			final int boxwidth = (int) (MainPane.chromDraw.bounds * 11	+ (Math.log10(overlapCoverage) * MainPane.chromDraw.bounds));
			final int yposition = moveY - boxheight < (sampleDivider - Draw.scrollValue) ? (int) (sampleDivider - Draw.scrollValue) : moveY - boxheight;
			drawBuffer.setColor(Color.black);
			drawBuffer.fillRect(moveX + 20, yposition, boxwidth, boxheight);
			drawBuffer.setColor(Color.white);
			drawBuffer.drawString("Coverage: " + overlapCoverage, moveX + 26, yposition + BaseVariables.defaultFontSize);			
			int baseCounter = 0;

			for (int i = 1; i < 8; i++) {
				final int allelicCount = (int) this.getCoverages()[coverageArrayPosition][i];
				if (allelicCount < Settings.coverageAlleleFreq) continue;					
				baseCounter++;
				baseChange = baseArray[i - 1];
				overlapColor = colorArray[i - 1];
				overlapText = baseChange + " : " + allelicCount + " (" + (int) (MethodLibrary.round(allelicCount / (double)overlapCoverage, 2) * 100) + "%)\n";
				drawBuffer.setColor(overlapColor);
				drawBuffer.drawString(overlapText, moveX + 26, (int)(yposition + BaseVariables.defaultFontSize * baseCounter) +BaseVariables.defaultFontSize);		
			}

			
			
			// TODO parempi ratkaisu näihin jos on skrollannut alempiin näytteisiin
			
			
			
			
		} else {
			final int coverageAmount = (int) ((sampleDivider + (coveragePanelHeight - 2) - moveY) * (this.getMaxcoverage() * split.pixel / coveragePanelHeight) + 1);
			drawBuffer.setColor(Color.white);
			drawBuffer.drawString("" + coverageAmount, moveX + 20, moveY + 10);
		}
	}

	public void mouseIntersectRead(final SplitClass split) {
		try {
			if (MainPane.drawCanvas.selectedSplit != split) return;
			if (Getter.getInstance().getSampleList().get(MainPane.drawCanvas.selectedIndex) != this.sample) return; // TODO: korjaa tämmöset
			if (MainPane.drawCanvas.moveY < coveragebottom) return;
			if (split.viewLength > Settings.readDrawDistance) return;

			final int readLevel = ((int) (((Draw.drawVariables.sampleHeight) * (MainPane.drawCanvas.selectedIndex + 1)) - (MainPane.drawCanvas.moveY)) + this.readwheel) / (BaseVariables.readHeight + 2);
			if (readLevel >= this.reads.size() || readLevel < 0) { ReadNode.selected = null; return; }
			ReadNode currentRead = this.reads.get(readLevel);
			boolean foundread = false;
			while (currentRead != null) {
				try {
					if (currentRead.getPosition() > (int) (split.start + MainPane.drawCanvas.mouseRect.x / split.pixel)) {
						break;
					}

					if (currentRead.getPosition() + currentRead.getWidth() < split.start) {
						currentRead = currentRead.getNext();
						continue;
					}
					if (currentRead.getRect().x + currentRead.getRect().width < MainPane.drawCanvas.mouseRect.x) {
						currentRead = currentRead.getNext();
						continue;
					}

					foundread = true;
					
					if (base.BasePlayer.reads.ReadNode.selected == null) {
						ReadNode.selected = currentRead;
						
					} else if (!ReadNode.selected.equals(currentRead)) {
						ReadNode.selected = currentRead;
					}
					//currentRead.draw();
					break;
				} catch (final Exception ex) {
					ErrorLog.addError(ex.getStackTrace());
					ex.printStackTrace();
					break;
				}
			}

			if (!foundread) {
				ReadNode.selected = null;
			}
			
		} catch (final Exception e) {
			ErrorLog.addError(e.getStackTrace());
			e.printStackTrace();
		}
	}
	void moveReadUp(final ReadNode moveread, final SplitClass split, final Sample readsample, final int startrow) {
		boolean found = false;

		for (int j = startrow; j < this.getReads().size(); j++) {

			try {

				ReadNode read = this.getReads().get(j);

				while (read != null) {

					if (read.getNext() != null) {
						if (read.getPosition() + read.getWidth() < moveread.getPosition()
								&& read.getNext().getPosition() > moveread.getPosition() + moveread.getWidth()) {
							read.getNext().setPrev(moveread);
							moveread.setNext(read.getNext());
							read.setNext(moveread);

							moveread.setPrev(read);

							found = true;
							break;
						}
					}
					if (read.getPosition() > moveread.getPosition() + moveread.getWidth()) {
						break;
					}

					read = read.getNext();

				}
				if (found) {
					break;
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	void moveReadToBottom(final ReadNode moveread, final SplitClass split, final Sample readsample, final int readlevel,
			final int addlevel, final ArrayList<ReadNode> removed) {
		ReadNode.clicked = null;
		ReadNode.selected = null;

		if (readlevel == addlevel) {
			return;
		}
		removeRead(moveread);

		ReadNode read, nextRead;

		try {
		
			read = this.getReads().get(addlevel);
		
			while (read != null) {

				if (read.getPosition() + read.getWidth() < moveread.getPosition()) {
					read = read.getNext();
					continue;
				}
				if (read.getPosition() > moveread.getPosition() + moveread.getWidth()) {
					break;
				}

				if (read.equals(this.getReads().get(addlevel))) {
					this.getReads().set(addlevel, this.getHeadAndTail().get(addlevel)[0]);

				}
				nextRead = read.getNext();
				if (nextRead == null) {
					System.out.println(addlevel);
				}
				removed.add(read);

				removeRead(read);
				read = nextRead;

			}

			if (read != null) {
				if (read.getPrev() != null) {
					read.getPrev().setNext(moveread);
					moveread.setPrev(read.getPrev());
				} else {
					moveread.setPrev(null);
				}
				read.setPrev(moveread);
				moveread.setNext(read);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	void removeRead(final ReadNode removeread) {
		if (removeread.getPrev() != null) {
			if (removeread.getNext() != null) {
				removeread.getPrev().setNext(removeread.getNext());
			} else {
				removeread.getPrev().setNext(null);
			}
		}
		if (removeread.getNext() != null) {
			if (removeread.getPrev() != null) {
				removeread.getNext().setPrev(removeread.getPrev());
			} else {
				removeread.getNext().setPrev(null);
			}
		}
		removeread.setNext(null);
		removeread.setPrev(null);
	}
	public void groupMismatchReads(final int position, final SplitClass split, final Sample readsample, final Reads readClass) {
		if (readClass == null || readClass.getReads() == null) return;
		int addlevel = 0;

		ArrayList<ReadNode> removedReads = new ArrayList<ReadNode>();
		for (int j = 0; j < readClass.getReads().size(); j++) {
			try {
				ReadNode read = readClass.getReads().get(j);
				boolean moved = false;
				while (read != null) {
					if (read.getPosition() + read.getWidth() < position) {
						read = read.getNext();
						continue;
					}
					if (read.getPosition() > position) break;
					if (read.getMismatches() == null) {
						read = read.getNext();
						continue;
					}

					for (int i = 0; i < read.getMismatches().size(); i++) {
						if (read.getPosition() + read.getMismatches().get(i).getKey() == position) {
							readClass.moveReadToBottom(read, split, readsample, j, addlevel, removedReads);
							addlevel++;
							moved = true;
							break;
						}
					}
					if (moved) break;
					read = read.getNext();
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < removedReads.size(); i++) {
			readClass.moveReadUp(removedReads.get(i), split, readsample, addlevel);
		}
		removedReads = null;
	}
}


