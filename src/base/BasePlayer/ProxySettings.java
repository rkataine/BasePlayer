package base.BasePlayer;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import base.BasePlayer.GUI.MainPane;

public class ProxySettings extends JPanel {
	
	private static final long serialVersionUID = 1L;
	public static JCheckBox useProxy;
	static String host, port, username, password;
	public static JTextField hostField;
  public static JTextField portField;
  static JTextField userField;
  static JTextField passField;
	static String[] types = {"HTTP", "SOCKS", "DIRECT"};
	public static JComboBox<String> proxytypes;	
	static JButton save;	
	
	public ProxySettings() {
		super(new GridBagLayout());
		useProxy = new JCheckBox("Use proxy");
		proxytypes = new JComboBox<String>(types);
		hostField = new JTextField("Proxy host");
		portField = new JTextField("Port");
		save = new JButton("Save");
		//userField = new JTextField("Username");
		//passField = new JTextField("Pass");
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTHWEST;		
		c.insets = new Insets(2,0,0,30);
		c.gridx = 0;
		c.weightx = 1;
		
		c.gridy = 0;
		c.gridwidth = 1;	
		add(useProxy,c);
		c.gridy++;
		add(proxytypes,c);
		c.gridy++;
		add(hostField,c);
		c.gridy++;
		add(portField,c);
		c.gridy++;
		c.fill = GridBagConstraints.NONE;
		
		add(save,c);
		save.setPreferredSize(MainPane.buttonDimension);
		c.gridy++;
		c.weighty = 1;
		add(new JLabel(),c);
		//add(userField);
		//add(passField);
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(useProxy.isSelected()) {
					MainPane.writeToConfig("isProxy=true");
				}
				else {
					MainPane.writeToConfig("isProxy=false");
				}
				MainPane.writeToConfig("proxyHost=" +hostField.getText());
				MainPane.writeToConfig("proxyPort=" +portField.getText());
				MainPane.writeToConfig("proxyType=" +proxytypes.getSelectedItem().toString());
				
			}
			
		});
	}

	
	
}
