package base.BasePlayer;

import base.BasePlayer.GUI.Loader;
import base.BasePlayer.GUI.MainPane;

public class Setter {
  private static Setter instance = null;

  private Setter() {
      // TODO Auto-generated constructor stub
  }

  public static Setter getInstance() {
    if (instance == null) {
        instance = new Setter();
    }
    return instance;
  }

  public void setLoading(boolean loading, String text) {
    Loader.isLoading = loading;
    Loader.loadingtext = text;
  }
  public void setUpdateScreen() {
    MainPane.drawCanvas.updateScreen = true;
    MainPane.drawCanvas.repaint();
  }
}
